//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <malloc.h>
#include "OcheredO.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)

#define ABS(X) (((X) > 0) ? (X) : (-(X)))


//���������� ������ �������
int OCHERED_O1::Add1(ODIN_BORT_1 *NewOB1)
{
  int i;
  double DD;
  unsigned long Nom1,Nom2;


  if(First<0||Last<0)
  {
    First=Last=0;
    OB1[Last]=NewOB1;
    if(OB1[Last]->Status2&PRIVYAZAN_OTSCHET_STATUS2)
    {
      OB1[Last]->Status&=~LOJNY_STATUS;
    }else{
      OB1[Last]->Status|=LOJNY_STATUS;
    }
    return 1;
  }
  if(Last+1==First)
  {
    return 0;  //������������
  }

  Last++;
  if(Last==200)
  {
    Last=0;
  }
  OB1[Last]=NewOB1;


//����� ����������� �������, ���� ��������� �� ����� � ���� � �������
  for(i=First;;)
  {
     if(OB1[i]->SektorNomer+1<OB1[Last]->SektorNomer)
     {
        First++;
        if(First==200)First=0;
        i++;
        if(i>=200)i=0;
        if(i==Last)break;
        continue;
     }
     if(OB1[i]->Azmt>OB1[Last]->Azmt)
     {
       if(OB1[Last]->Azmt+360.0-OB1[i]->Azmt>DAzmt)
       {
         First++;
         if(First==200)First=0;
         i++;
         if(i>=200)i=0;
         if(i==Last)break;
         continue;
       }
     }else{
       if(OB1[Last]->Azmt-OB1[i]->Azmt>DAzmt)
       {
         First++;
         if(First==200)First=0;
         i++;
         if(i>=200)i=0;
         if(i==Last)break;
         continue;
       }
     }

     if(i!=Last)
     {
//���� �������� ������� ���� Last �� ���������
       DD=ABS(OB1[Last]->Dlnst-OB1[i]->Dlnst);
    //���� ��������� ������ ���������
       if(DD<=DDaln)
       {
     //�������� ��� ��� �������
          if(OB1[Last]->Status2&PRIVYAZAN_OTSCHET_STATUS2)
          {
            if(OB1[i]->Status2&PRIVYAZAN_OTSCHET_STATUS2)
            {
//��� ��� ������� ���������. ������, ��� ��� �������� � ���������������� ��������
               OB1[i]->Status|=BLIZKO_STATUS;
               OB1[Last]->Status|=BLIZKO_STATUS;
            }else{
//���� ������ � ���������������� ��������, ������ ������ ������ �� ��������...
//���� ���������, ����� �� ������ ������ ��� �� ����� ��� � ������... ���� ��, ��
//������ �� �������� ���������� �������
              if(OB1[i]->NomBort==OB1[Last]->NomBort)
              {
                 OB1[i]->Status|=DROBLENIE_STATUS;
                 OB1[Last]->Status|=DROBLENIE_STATUS;
                 OB1[i]->Status&=~LOJNY_STATUS;
                 if(OB1[i]->Status&3==3&&OB1[Last]->Status&3==2||
                 OB1[i]->Status&3==2&&OB1[Last]->Status&3==3)
                 {
                    OB1[i]->Status2|=DUBL_S_PS_STATUS2;
                    OB1[Last]->Status2|=DUBL_S_PS_STATUS2;
                 }
              }
            }
          }else{    //��������� ������ �� �������
            if(OB1[i]->Status2&PRIVYAZAN_OTSCHET_STATUS2)
            {
              if(OB1[i]->NomBort==OB1[Last]->NomBort)
              {
                 OB1[Last]->Status|=DROBLENIE_STATUS;
                 OB1[i]->Status|=DROBLENIE_STATUS;
                 OB1[Last]->Status&=~LOJNY_STATUS;
                 if(OB1[i]->Status&3==3&&OB1[Last]->Status&3==2||
                 OB1[i]->Status&3==2&&OB1[Last]->Status&3==3)
                 {
                    OB1[i]->Status2|=DUBL_S_PS_STATUS2;
                    OB1[Last]->Status2|=DUBL_S_PS_STATUS2;
                 }
              }
            }
          }
       }
     }

     i++;
     if(i>=200)i=0;
     if(i==Last)break;
  }
//������ ��������� ������ ������ ��� ���
  if(OB1[Last]->Status2&PRIVYAZAN_OTSCHET_STATUS2)
  {
    OB1[Last]->Status&=~LOJNY_STATUS;
  }else{
    if(OB1[Last]->Status&DROBLENIE_STATUS)
    {
       OB1[Last]->Status&=~LOJNY_STATUS;
    }else{
       OB1[Last]->Status|=LOJNY_STATUS;
    }
  }



  return 1;
}




int OCHERED_O1::AddP(ODIN_BORT_1 *NewOB1,
                          PARA_OTSCHETOV_OB1 **POOB1,
                          int *N_of_POOB1
                         )
{
  int i;
  double DD;
  unsigned long Nom1,Nom2;

  NewOB1->Status&=~BLIZKO_STATUS;
  if(First<0||Last<0)
  {
    First=Last=0;
    OB1[Last]=NewOB1;
    return 1;
  }
  if(Last+1==First)
  {
    return 0;  //������������
  }
//��������� ��������� ������
  Last++;
  if(Last==200)
  {
    Last=0;
  }
  OB1[Last]=NewOB1;

  //����� ����������� �������, ���� ��������� �� ����� � ���� � �������
  for(i=First;;)
  {
     if(OB1[i]->SektorNomer+1<OB1[Last]->SektorNomer)
     {
        First++;
        if(First==200)First=0;
        i++;
        if(i>=200)i=0;
        if(i==Last)break;
        continue;
     }


     if(OB1[i]->Azmt>340&&OB1[Last]->Azmt<20)
     {
       if(OB1[Last]->Azmt+360.0-OB1[i]->Azmt>DAzmt)
       {
         First++;
         if(First==200)First=0;
         i++;
         if(i>=200)i=0;
         if(i==Last)break;
         continue;
       }
     }else if(OB1[i]->Azmt<=OB1[Last]->Azmt)
     {
       if(OB1[Last]->Azmt-OB1[i]->Azmt>DAzmt)
       {
         First++;
         if(First==200)First=0;
         i++;
         if(i>=200)i=0;
         if(i==Last)break;
         continue;
       }
     }else{  //��� �� ����������� �������, ��� OB1[i]->Azmt>340&&OB1[Last]->Azmt<20
       First++;
       if(First==200)First=0;
       i++;
       if(i>=200)i=0;
       if(i==Last)break;
       continue;
     }

     if(i!=Last)
     {
//���� �������� ������� ���� Last �� ���������
       DD=ABS(OB1[Last]->Dlnst-OB1[i]->Dlnst);
    //���� ��������� ������ ���������
       if(DD<=DDaln)
       {
     //�������� ��� ��� �������
          *POOB1=(PARA_OTSCHETOV_OB1 *)realloc(*POOB1,sizeof(PARA_OTSCHETOV_OB1)*
                                     ((*N_of_POOB1)+1));
          (*POOB1)[(*N_of_POOB1)].OB1_1=OB1[i]; (*POOB1)[(*N_of_POOB1)].OB1_2=OB1[Last];
           OB1[i]->Status|=BLIZKO_STATUS;
           OB1[Last]->Status|=BLIZKO_STATUS;
         (*N_of_POOB1)++;
       }
     }
     i++;
     if(i>=200)i=0;
     if(i==Last)break;
  }
  return 1;
}


int OCHERED_O1::AddS(ODIN_BORT_1 *NewOB1,
                PARA_OTSCHETOV_OB1 **POOB1,
                int *N_of_POOB1)
{
  int i;
  double DD;
  unsigned long Nom1,Nom2;
  NewOB1->Status&=~BLIZKO_STATUS;

  if(First<0||Last<0)
  {
    First=Last=0;
    OB1[Last]=NewOB1;
    return 1;
  }
  if(Last+1==First)
  {
    return 0;  //������������
  }
//��������� ��������� ������
  Last++;
  if(Last==200)
  {
    Last=0;
  }
  OB1[Last]=NewOB1;

  //����� ����������� �������, ���� ��������� �� ����� � ���� � �������
  for(i=First;;)
  {
     if(OB1[i]->SektorNomer+1<OB1[Last]->SektorNomer)
     {
        First++;
        if(First==200)First=0;
        i++;
        if(i>=200)i=0;
        if(i==Last)break;
        continue;
     }
     if(OB1[i]->Azmt>OB1[Last]->Azmt&&OB1[i]->Azmt>350&&OB1[Last]->Azmt<10)
     {
       if(OB1[Last]->Azmt+360.0-OB1[i]->Azmt>DAzmt)
       {
         First++;
         if(First==200)First=0;
         i++;
         if(i>=200)i=0;
         if(i==Last)break;
         continue;
       }
     }else if(OB1[i]->Azmt<=OB1[Last]->Azmt)
     {
       if(ABS(OB1[Last]->Azmt-OB1[i]->Azmt)>DAzmt)
       {
         First++;
         if(First==200)First=0;
         i++;
         if(i>=200)i=0;
         if(i==Last)break;
         continue;
       }
     }else{
         First++;
         if(First==200)First=0;
         i++;
         if(i>=200)i=0;
         if(i==Last)break;
         continue;
     }

     if(i!=Last)
     {
//���� �������� ������� ���� Last �� ���������
       DD=ABS(OB1[Last]->Dlnst-OB1[i]->Dlnst);
    //���� ��������� ������ ���������
       if(DD<=DDaln)
       {
     //��������, ����� ��� ������� �� ����� �� ������� ������ � �� ���� �� ����������� �� ������ ��� ������
     //� ������ �������� �� ������ ���� �����
          if(OB1[i]->OprVysota!=0&&(OB1[i]->OprVysota==OB1[i]->IstVysota)&&
             !(OB1[i]->Status&LOJNY_NOMER_STATUS)&&OB1[i]->NomBort>0&&
        OB1[Last]->OprVysota!=0&&(OB1[Last]->OprVysota==OB1[Last]->IstVysota)&&
        !(OB1[Last]->Status&LOJNY_NOMER_STATUS)&&OB1[Last]->NomBort>0&&
          OB1[i]->NomBort!=OB1[Last]->NomBort)
          {

             *POOB1=(PARA_OTSCHETOV_OB1 *)realloc(*POOB1,sizeof(PARA_OTSCHETOV_OB1)*
                                     ((*N_of_POOB1)+1));
            (*POOB1)[(*N_of_POOB1)].OB1_1=OB1[i]; (*POOB1)[(*N_of_POOB1)].OB1_2=OB1[Last];
            OB1[i]->Status|=BLIZKO_STATUS;
            OB1[Last]->Status|=BLIZKO_STATUS;
            (*N_of_POOB1)++;
          }  
       }
     }
     i++;
     if(i>=200)i=0;
     if(i==Last)break;
  }
  return 1;
}





//��������� ������...
//���� ��� ������� �������
//����� ��� ���������� �������
//���������� 2, ���� ������������ P � PS �������
int OCHERED_O1::Add2(ODIN_BORT_2 *NewOB1,
           long NomA,long Indx,
           long *NomA_P, long *Indx_P, //������ ����� NomA_P, Indx_P
           long *NomA_S, long *Indx_S, //������ ������� S
           long *NomA_PS, long *Indx_PS)
{
/*
  int i;
  double D1,D2;
  double A1,A2;

  if(First<0||Last<0)
  {
    First=Last=0;
    OB2[Last].NomA=NomA; OB2[Last].Indx=Indx;
    if(NewOB1->Status&REJIM_UVD_STATUS) OB2[Last].Rejim=1;
    else(NewOB1->Status&REJIM_RBS_STATUS) OB2[Last].Rejim=2;
    else OB2[Last].Rejim=0;
    OB2[Last].Kanal=(NewOB1->Status&3);
    OB2[Last].IsPrivyazan=((NewOB1->Status2&PRIVYAZAN_OTSCHET_STATUS2)!=0);
    OB2[Last].Azmt=NewOB1->Azmt;     OB2[Last].Dlnst=NewOB1->Dlnst;
    OB2[Last].Time=NewOB1->Time;
    OB2[Last].SektorNomer=NewOB1->SektorNomer;
    OB2[Last].Rasm=false;

    return 1;
  }
  

  Last++;
  if(Last==200)Last=0;
  OB2[Last].NomA=NomA; OB2[Last].Indx=Indx;
  if(NewOB1->Status&REJIM_UVD_STATUS) OB2[Last].Rejim=1;
  else(NewOB1->Status&REJIM_RBS_STATUS) OB2[Last].Rejim=2;
  else OB2[Last].Rejim=0;
  OB2[Last].Kanal=(NewOB1->Status&3);
  OB2[Last].IsPrivyazan=((NewOB1->Status2&PRIVYAZAN_OTSCHET_STATUS2)!=0);
  OB2[Last].Azmt=NewOB1->Azmt;     OB2[Last].Dlnst=NewOB1->Dlnst;
  OB2[Last].Time=NewOB1->Time;
  OB2[Last].SektorNomer=NewOB1->SektorNomer;
  OB2[Last].Rasm=false;



  return 1;
  */
}




