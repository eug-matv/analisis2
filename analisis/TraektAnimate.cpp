//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <alloc.h>
#include <stdio.h>
#include "Tools.h"
#include "TraektAnimate.h"
#include "Traekt.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
TColor PColAnim[51];
TColor SColAnim[51];
TColor PSColAnim[51];


extern TCGauge *GlobalCGauge1;





//������ �������
struct ODIN_DAT_ANIM2
{
  ODIN_BORT_1 *OB;
  int X,Y;  //���������� �� ������, ������ ���������� ������� ��������� �����
};


//�������� ������� ������� ��������





static ODIN_BORT_1 **OB1=NULL;
static ODIN_DAT_ANIM2 *ODA2;
static long N_of_OtschAnim=0; //����� �������� ��������
static long TekFirstODA2=0;



static long *NSAnim=NULL;
static int N_of_NSAnim=0;

static long TekIndxNSAnim;


int OBZORY_1::InizialAnim(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm
)
{

  unsigned char R1,G1,B1,R2,G2,B2,Rp,Gp,Bp;
  double Rk,Gk,Bk;
  long Nom,Indx,i;
  extern TCGauge *GlobalCGauge1;


  TekIndxNSAnim=0;
  PColAnim[0]=P_Col;
  SColAnim[0]=S_Col;
  PSColAnim[0]=PS_Col;

//�������� �����
  R1=GetRValue(P_Col); G1=GetGValue(P_Col);  B1=GetBValue(P_Col);
  R2=GetRValue(OblastCol); G2=GetGValue(OblastCol); B2=GetBValue(OblastCol);
  Rk=(R2-R1)/(DlinaHvosta+1.0); Gk=(G2-G1)/(DlinaHvosta+1.0);
  Bk=(B2-B1)/(DlinaHvosta+1.0);

  for(i=1;i<=DlinaHvosta;i++)
  {
    Rp=R1+Rk*i; Gp=G1+Gk*i; Bp=B1+Bk*i;
    PColAnim[i]=(TColor)RGB(Rp,Gp,Bp);
  }

  R1=GetRValue(S_Col); G1=GetGValue(S_Col);  B1=GetBValue(S_Col);
  Rk=(R2-R1)/(DlinaHvosta+1.0); Gk=(G2-G1)/(DlinaHvosta+1.0);
  Bk=(B2-B1)/(DlinaHvosta+1.0);

  for(i=1;i<=DlinaHvosta;i++)
  {
    Rp=R1+Rk*i; Gp=G1+Gk*i; Bp=B1+Bk*i;
    SColAnim[i]=(TColor)RGB(Rp,Gp,Bp);
  }

  R1=GetRValue(PS_Col); G1=GetGValue(PS_Col);  B1=GetBValue(PS_Col);
  Rk=(R2-R1)/(DlinaHvosta+1.0); Gk=(G2-G1)/(DlinaHvosta+1.0);
  Bk=(B2-B1)/(DlinaHvosta+1.0);

  for(i=1;i<=DlinaHvosta;i++)
  {
    Rp=R1+Rk*i; Gp=G1+Gk*i; Bp=B1+Bk*i;
    PSColAnim[i]=(TColor)RGB(Rp,Gp,Bp);
  }

  long TekSmesh;
  long MaxN_of_OAnim=0,MaxN_of_NSAnim=0;
  TekSmesh=FirstAbs;

  while(TekSmesh>=0)
  {
    Nom=TekSmesh%362; Indx=TekSmesh/362;
    if(Nom<360&&(o_b_1[Nom][Indx].Status&VISIBLE_STATUS))
    {
      if(MaxN_of_OAnim==N_of_OtschAnim)
      {
        MaxN_of_OAnim+=100;
        ODA2=(ODIN_DAT_ANIM2 *)realloc(ODA2,sizeof(ODIN_DAT_ANIM2)*MaxN_of_OAnim);
      }
      ODA2[N_of_OtschAnim].OB=o_b_1[Nom]+Indx;
      if(N_of_NSAnim==0||
        ODA2[N_of_OtschAnim].OB->SektorNomer!=NSAnim[N_of_NSAnim-1])
      {
//������� ����� ������� � �����
         if(N_of_NSAnim==MaxN_of_NSAnim)
         {
           MaxN_of_NSAnim+=100;
           NSAnim=(long*)realloc(NSAnim,sizeof(long)*MaxN_of_NSAnim);
         }
         NSAnim[N_of_NSAnim]=ODA2[N_of_OtschAnim].OB->SektorNomer;
         N_of_NSAnim++;
      }


      N_of_OtschAnim++;
    }
    TekSmesh=o_b_1[Nom][Indx].AbsNext;
  };
  VychislenieKoordinatAnim(Image,LeftKm,BottomKm,RightKm,TopKm);
  TekFirstODA2=0;

  GlobalCGauge1->MinValue=0;
  GlobalCGauge1->MaxValue=N_of_OtschAnim;
  GlobalCGauge1->Progress=0;

  GlobalCGauge1->Visible=true;
  return 1;
}


int OBZORY_1::VychislenieKoordinatAnim(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm)
{
  long i;
  double KoefX,KoefY;   //�����������
  long LeftM=LeftKm*1000,
       BottomM=BottomKm*1000,
       RightM=RightKm*1000,
       TopM=TopKm*1000;
  double Xf,Yf;


  KoefX=(Image->Width-1)/((double)(RightKm-LeftKm));
  KoefY=(Image->Height-1)/((double)(BottomKm-TopKm));

  for(i=0;i<N_of_OtschAnim;i++)
  {
    if(ODA2[i].OB->X<RightM&&ODA2[i].OB->X>LeftM&&
       ODA2[i].OB->Y<TopM&&ODA2[i].OB->Y>BottomM)
    {
//������� �����
        Xf=ODA2[i].OB->X/1000.0;
        Yf=ODA2[i].OB->Y/1000.0;
        ODA2[i].X=Okrugl((Xf-LeftKm)*KoefX);
        ODA2[i].Y=Okrugl((Yf-TopKm)*KoefY);
    }else{
      ODA2[i].X=-1;
      ODA2[i].Y=-1;
    }  
  }
  return 1;
}






int OBZORY_1::VyvodAllDataAnim(long TekSektorNomer,TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm)
{
  long TekMinSektor,TekMaxSektor,MinSektor,MaxSektor;
  long i,j,k;
  long Kanal;
  long NomBort,Dlina;
  int XVis,YVis;
  char TimeOut[20];
  int Ch,Ch1,Mn,Sk;
  AnsiString TextNomBort,TextVysota;
  double fCurTime=-1;
//������� ������ ��������, �������

    TekMinSektor=TekSektorNomer-3;
    TekMaxSektor=TekSektorNomer;



//�������� �� ����������� �����
  if(IsHvost)
  {
//������� ������
     MinSektor=TekMinSektor-4*DlinaHvosta;
     MaxSektor=TekMaxSektor-4*DlinaHvosta;
     if(TekFirstODA2<0)TekFirstODA2=0;
     if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;
     if(ODA2[TekFirstODA2].OB->SektorNomer<MinSektor)
     {
       while(ODA2[TekFirstODA2].OB->SektorNomer<MinSektor)
       {
         TekFirstODA2++;
         if(TekFirstODA2>=N_of_OtschAnim){TekFirstODA2=N_of_OtschAnim-1;break;};
       };
       if(ODA2[TekFirstODA2].OB->SektorNomer>MinSektor)TekFirstODA2--;
     }else // if(ODA2[TekFirstODA2].OB->SektorNomer>=Sektora[0])
     {
       while(ODA2[TekFirstODA2].OB->SektorNomer>=MinSektor)
       {
         TekFirstODA2--;
         if(TekFirstODA2<=0)
         {
           TekFirstODA2=0;
           break;
         }  
       }

     }
     if(TekFirstODA2<0)TekFirstODA2=0;
     if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;

     k=TekFirstODA2;

//������� ���� ������...
     Image->Picture->Bitmap->Canvas->Pen->Mode=pmCopy;
     Image->Picture->Bitmap->Canvas->Pen->Style=psSolid;
     Image->Picture->Bitmap->Canvas->Brush->Style=bsSolid;
//     Image->Picture
     for(i=DlinaHvosta;i>=1;i--)
     {
       while(k<N_of_OtschAnim&&ODA2[k].OB->SektorNomer<=MaxSektor)
       {

         if(ODA2[k].OB->SektorNomer>=MinSektor&&ODA2[k].X!=-1)
         {
//������� ����� ������
           Kanal=ODA2[k].OB->Status&3;
           switch(Kanal)
           {
            case 1:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               Image->Picture->Bitmap->Canvas->Pen->Color=RS_P_Col;
               Image->Picture->Bitmap->Canvas->Brush->Color=RS_P_Col;
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PColAnim[i];
               Image->Picture->Bitmap->Canvas->Brush->Color=PColAnim[i];
             }
             break;
        case 2:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=SColAnim[i];
               Image->Picture->Bitmap->Canvas->Brush->Color=SColAnim[i];
             }
             break;
        case 3:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PSColAnim[i];
               Image->Picture->Bitmap->Canvas->Brush->Color=PSColAnim[i];
             }
             break;
           };
           XVis=ODA2[k].X; YVis=ODA2[k].Y;
           if(DiametrMetok==1)
           {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
           }else if(DiametrMetok==2)
           {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
           }else{
             Image->Picture->Bitmap->Canvas->Brush->Color=
               Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
               XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
           }
         }
         k++;
       };
       MinSektor+=4;
       MaxSektor+=4;
     }
  }

  if(TekFirstODA2<0)TekFirstODA2=0;
  if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;
  if(ODA2[TekFirstODA2].OB->SektorNomer<TekMinSektor)
  {
     while(ODA2[TekFirstODA2].OB->SektorNomer<TekMinSektor)
     {
         TekFirstODA2++;
         if(TekFirstODA2>=N_of_OtschAnim){TekFirstODA2=N_of_OtschAnim-1;break;};
     };
     if(ODA2[TekFirstODA2].OB->SektorNomer>TekMinSektor)TekFirstODA2--;
  }else // if(ODA2[TekFirstODA2].OB->SektorNomer>=TekMinSektor)
  {
     while(ODA2[TekFirstODA2].OB->SektorNomer>=TekMinSektor)
     {
         TekFirstODA2--;
         if(TekFirstODA2<=0)
         {
           TekFirstODA2=0;
           break;
         }  
     }
  }
  if(TekFirstODA2<0)TekFirstODA2=0;
  if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;
  k=TekFirstODA2;
  while(k<N_of_OtschAnim&&ODA2[k].OB->SektorNomer<=TekMaxSektor)
  {

    if(ODA2[k].OB->SektorNomer>=TekMinSektor&&ODA2[k].X!=-1)
    {
//������� ����� ������
      Kanal=ODA2[k].OB->Status&3;
      switch(Kanal)
      {
        case 1:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               Image->Picture->Bitmap->Canvas->Pen->Color=RS_P_Col;
               Image->Picture->Bitmap->Canvas->Brush->Color=RS_P_Col;
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PColAnim[0];
               Image->Picture->Bitmap->Canvas->Brush->Color=PColAnim[0];
              }
              break;
        case 2:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=SColAnim[0];
               Image->Picture->Bitmap->Canvas->Brush->Color=SColAnim[0];
             }
              break;
        case 3:
                           if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PSColAnim[0];
               Image->Picture->Bitmap->Canvas->Brush->Color=PSColAnim[0];
             }
              break;
      };
      XVis=ODA2[k].X; YVis=ODA2[k].Y;
      if(DiametrMetok==1)
      {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
      }else if(DiametrMetok==2)
      {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
       }else{
             Image->Picture->Bitmap->Canvas->Brush->Color=
               Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
               XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);

       }

    }
    fCurTime=ODA2[k].OB->Time;
    k++;
  };

//� ������ ����� �������� ������ � ������� � ������ �����, ���� �������� ��������

//���� ��������� ����� ��� ��������� ������ ��� ��������� �����
  if(IsAnimNomer||IsAnimVysota||IsAnimTime)
  {
    Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
    Image->Picture->Bitmap->Canvas->Font->Color=Text_Col;
    Image->Picture->Bitmap->Canvas->Font->Name="MS Sans Serif";
    Image->Picture->Bitmap->Canvas->Font->Size=7;

    k=TekFirstODA2;
    while(k<N_of_OtschAnim&&ODA2[k].OB->SektorNomer<=TekMaxSektor)
    {
       if(ODA2[k].OB->SektorNomer>=TekMinSektor&&ODA2[k].X!=-1)
       {
          XVis=ODA2[k].X; YVis=ODA2[k].Y;
          if(IsAnimNomer)
          {
NomBort=otis[ODA2[k].OB->IndxBrt].ulNumber&0x00FFFFFF;
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x02000000)
             {
                 if(NomBort==0)
                 {
                    TextNomBort=String((int)NomBort)+String("(r)");
                 }else{
                    TextNomBort=String((int)NomBort);
                 }
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x01000000)
             {
                 if(NomBort<10000)
                 {
                     TextNomBort=String((int)NomBort)+String("(u)");
                 }else{
                     TextNomBort=String((int)NomBort);
                 }
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0)
             {
                  TextNomBort=String("PC �")+String((int)otis[ODA2[k].OB->IndxBrt].trassa_indx);
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x12000000)
             {
                  TextNomBort=String((int)NomBort)+String("(RM_RBS)");
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x11000000)
             {
                 TextNomBort=String((int)NomBort)+String("(RM_UVD)");
             }
             Dlina=Image->Picture->Bitmap->Canvas->TextWidth(TextNomBort);
             if(XVis+Dlina>Image->Width)
             {
                XVis=Image->Width-Dlina;
             }
             if(otis[ODA2[k].OB->IndxBrt].ulNumber<0x14000000)
             {
 //������� ������
              Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis,TextNomBort);
             }
             YVis+=10;
          }             
          if(IsAnimVysota)
          {
             XVis=ODA2[k].X;
             TextVysota=IntToStr((int)ODA2[k].OB->OprVysota);
             Dlina=Image->Picture->Bitmap->Canvas->TextWidth(TextVysota);
             if(XVis+Dlina>Image->Width)
             {
                XVis=Image->Width-Dlina;
             }
             Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis,TextVysota);
             YVis+=10;
          }
          if(IsAnimTime)
          {
               XVis=ODA2[k].X;
               Ch1=(int)(ODA2[k].OB->Time);
               Ch=Ch1%24;
               Mn=(int)((ODA2[k].OB->Time-Ch1)*60);
               Sk=(int)((60*(ODA2[k].OB->Time-Ch1)-Mn)*60);
               sprintf(TimeOut,"%02d:%02d:%02d",Ch,Mn,Sk);
               Dlina=strlen(TimeOut);
               if(XVis+Dlina>Image->Width)
               {
                 XVis=Image->Width-Dlina;
               }
               Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis,
                   String(TimeOut));
          }
       }
       k++;
    };
  }

  if(fCurTime>=0)
  {
      Ch1=(int)(fCurTime);
      Ch=Ch1%24;
      Mn=(int)((fCurTime-Ch1)*60);
      Sk=(int)((60*(fCurTime-Ch1)-Mn)*60);
      sprintf(cCurTimeAnim,"%02d:%02d:%02d",Ch,Mn,Sk);
  }else{
      cCurTimeAnim[0]=0;
  }

  GlobalCGauge1->Progress=k;
  return 1;
}


//����� ������ �����
int OBZORY_1::VyvodAllDataAnimNazad(long TekSektorNomer,TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm)
{
  long TekMinSektor,TekMaxSektor,MinSektor,MaxSektor;
  long i,j,k;
  long Kanal;
  long NomBort,Dlina;
  int XVis,YVis;
  char TimeOut[20];
  int Ch,Ch1,Mn,Sk;
  AnsiString TextNomBort,TextVysota;
    double fCurTime=-1;
//������� ������ ��������, �������

    TekMinSektor=TekSektorNomer;
    TekMaxSektor=TekSektorNomer+3;



//�������� �� ����������� �����
  if(IsHvost)
  {
//������� ������
     MinSektor=TekMinSektor+4*DlinaHvosta;
     MaxSektor=TekMaxSektor+4*DlinaHvosta;
     if(TekFirstODA2<0)TekFirstODA2=0;
     if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;
     if(ODA2[TekFirstODA2].OB->SektorNomer>MaxSektor)
     {
       while(ODA2[TekFirstODA2].OB->SektorNomer>MaxSektor)
       {
         TekFirstODA2--;
         if(TekFirstODA2<0){TekFirstODA2=0;break;};
       };
       if(ODA2[TekFirstODA2].OB->SektorNomer<MaxSektor)TekFirstODA2++;
     }else // if(ODA2[TekFirstODA2].OB->SektorNomer>=Sektora[0])
     {
       while(ODA2[TekFirstODA2].OB->SektorNomer<=MaxSektor)
       {
         TekFirstODA2++;
         if(TekFirstODA2>=N_of_OtschAnim-1)
         {
           TekFirstODA2=N_of_OtschAnim-1;
           break;
         }
       }

     }

     if(TekFirstODA2<0)TekFirstODA2=0;
     if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;

     k=TekFirstODA2;

//������� ���� ������...
     Image->Picture->Bitmap->Canvas->Pen->Mode=pmCopy;
     Image->Picture->Bitmap->Canvas->Pen->Style=psSolid;
     Image->Picture->Bitmap->Canvas->Brush->Style=bsSolid;
//     Image->Picture
     for(i=DlinaHvosta;i>=1;i--)
     {
       while(k>=0&&ODA2[k].OB->SektorNomer>=MinSektor)
       {

         if(ODA2[k].OB->SektorNomer<=MaxSektor&&ODA2[k].X!=-1)
         {
//������� ����� ������
           Kanal=ODA2[k].OB->Status&3;
           switch(Kanal)
           {
            case 1:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               Image->Picture->Bitmap->Canvas->Pen->Color=RS_P_Col;
               Image->Picture->Bitmap->Canvas->Brush->Color=RS_P_Col;
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PColAnim[i];
               Image->Picture->Bitmap->Canvas->Brush->Color=PColAnim[i];
             }
             break;
            case 2:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=SColAnim[i];
               Image->Picture->Bitmap->Canvas->Brush->Color=SColAnim[i];
             }
             break;
            case 3:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PSColAnim[i];
               Image->Picture->Bitmap->Canvas->Brush->Color=PSColAnim[i];
             }
             break;
           };
           XVis=ODA2[k].X; YVis=ODA2[k].Y;
           if(DiametrMetok==1)
           {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
           }else if(DiametrMetok==2)
           {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
           }else{
             Image->Picture->Bitmap->Canvas->Brush->Color=
               Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
               XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
           }
         }
         k--;
       };
       MinSektor-=4;
       MaxSektor-=4;
     }
  }

  if(TekFirstODA2<0)TekFirstODA2=0;
  if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;
  if(ODA2[TekFirstODA2].OB->SektorNomer>TekMaxSektor)
  {
     while(ODA2[TekFirstODA2].OB->SektorNomer>TekMaxSektor)
     {
         TekFirstODA2--;
         if(TekFirstODA2<0){TekFirstODA2=0;break;};
     };
     if(ODA2[TekFirstODA2].OB->SektorNomer<TekMaxSektor)TekFirstODA2++;
  }else // if(ODA2[TekFirstODA2].OB->SektorNomer>=TekMinSektor)
  {
     while(ODA2[TekFirstODA2].OB->SektorNomer<=TekMaxSektor)
     {
         TekFirstODA2++;
         if(TekFirstODA2>=N_of_OtschAnim-1)
         {
           TekFirstODA2=N_of_OtschAnim-1;
           break;
         }
     }
  }
  if(TekFirstODA2<0)TekFirstODA2=0;
  if(TekFirstODA2>=N_of_OtschAnim)TekFirstODA2=N_of_OtschAnim-1;

  k=TekFirstODA2;
  while(k>=0&&ODA2[k].OB->SektorNomer>=TekMinSektor)
  {

    if(ODA2[k].OB->SektorNomer<=TekMaxSektor&&ODA2[k].X!=-1)
    {
//������� ����� ������
      Kanal=ODA2[k].OB->Status&3;
      switch(Kanal)
      {

        case 1:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               Image->Picture->Bitmap->Canvas->Pen->Color=RS_P_Col;
               Image->Picture->Bitmap->Canvas->Brush->Color=RS_P_Col;
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PColAnim[0];
               Image->Picture->Bitmap->Canvas->Brush->Color=PColAnim[0];
              }
              break;
        case 2:
             if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=SColAnim[0];
               Image->Picture->Bitmap->Canvas->Brush->Color=SColAnim[0];
             }
              break;
        case 3:
                           if(ODA2[k].OB->Status&BLIZKO_STATUS)
             {
               if(ODA2[k].OB->Status&REJIM_UVD_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
               }else if(ODA2[k].OB->Status&REJIM_RBS_STATUS)
               {
                 Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
                 Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;
               }
             }else{
               Image->Picture->Bitmap->Canvas->Pen->Color=PSColAnim[0];
               Image->Picture->Bitmap->Canvas->Brush->Color=PSColAnim[0];
             }
              break;

      };
      XVis=ODA2[k].X; YVis=ODA2[k].Y;
      if(DiametrMetok==1)
      {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
      }else if(DiametrMetok==2)
      {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
       }else{
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
               XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
             Image->Picture->Bitmap->Canvas->Brush->Color=
               Image->Picture->Bitmap->Canvas->Pen->Color;

       }
    }
    fCurTime=ODA2[k].OB->Time;
    k--;
  };

//� ������ ����� �������� ������ � ������� � ������ �����, ���� �������� ��������

//���� ��������� ����� ��� ��������� ������ ��� ��������� �����
  if(IsAnimNomer||IsAnimVysota||IsAnimTime)
  {
    Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
    Image->Picture->Bitmap->Canvas->Font->Color=Text_Col;
    Image->Picture->Bitmap->Canvas->Font->Name="MS Sans Serif";
    Image->Picture->Bitmap->Canvas->Font->Size=7;

    k=TekFirstODA2;
    while(k>=0&&ODA2[k].OB->SektorNomer>=TekMinSektor)
    {

       if(ODA2[k].OB->SektorNomer<=TekMaxSektor&&ODA2[k].X!=-1)
       {
          XVis=ODA2[k].X; YVis=ODA2[k].Y;
          if(IsAnimNomer)
          {
             NomBort=otis[ODA2[k].OB->IndxBrt].ulNumber&0x00FFFFFF;
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x02000000)
             {
                 if(NomBort==0)
                 {
                    TextNomBort=String((int)NomBort)+String("(r)");
                 }else{
                    TextNomBort=String((int)NomBort);
                 }
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x01000000)
             {
                 if(NomBort<10000)
                 {
                     TextNomBort=String((int)NomBort)+String("(u)");
                 }else{
                     TextNomBort=String((int)NomBort);
                 }
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0)
             {
                  TextNomBort=String("PC �")+String((int)otis[ODA2[k].OB->IndxBrt].trassa_indx);
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x12000000)
             {
                  TextNomBort=String((int)NomBort)+String("(RM_RBS)");
             }else
             if((otis[ODA2[k].OB->IndxBrt].ulNumber&0xFF000000)==0x11000000)
             {
                 TextNomBort=String((int)NomBort)+String("(RM_UVD)");
             }
             Dlina=Image->Picture->Bitmap->Canvas->TextWidth(TextNomBort);
             if(XVis+Dlina>Image->Width)
             {
                XVis=Image->Width-Dlina;
             }
             if(otis[ODA2[k].OB->IndxBrt].ulNumber<0x14000000)
             {
 //������� ������
              Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis,TextNomBort);
             }
              YVis+=10;
          }

          if(IsAnimVysota)
          {
             XVis=ODA2[k].X;
             TextVysota=IntToStr((int)ODA2[k].OB->OprVysota);
             Dlina=Image->Picture->Bitmap->Canvas->TextWidth(TextVysota);
             if(XVis+Dlina>Image->Width)
             {
                XVis=Image->Width-Dlina;
             }
             Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis,TextVysota);
             YVis+=10;
          }
          if(IsAnimTime)
          {
               XVis=ODA2[k].X;
               Ch1=(int)(ODA2[k].OB->Time);
               Ch=Ch1%24;
               Mn=(int)((ODA2[k].OB->Time-Ch1)*60);
               Sk=(int)((60*(ODA2[k].OB->Time-Ch1)-Mn)*60);
               sprintf(TimeOut,"%02d:%02d:%02d",Ch,Mn,Sk);
               Dlina=strlen(TimeOut);
               if(XVis+Dlina>Image->Width)
               {
                 XVis=Image->Width-Dlina;
               }
               Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis,
                   String(TimeOut));
          } //if(IsAnimTime)

       } //if(ODA2[k].OB
       k--;
    }//while(

  }

  if(fCurTime>=0)
  {
      Ch1=(int)(fCurTime);
      Ch=Ch1%24;
      Mn=(int)((fCurTime-Ch1)*60);
      Sk=(int)((60*(fCurTime-Ch1)-Mn)*60);
      sprintf(cCurTimeAnim,"%02d:%02d:%02d",Ch,Mn,Sk);
  }else{
      cCurTimeAnim[0]=0;
  }
  GlobalCGauge1->Progress=k;
  return 1;
}







int OBZORY_1::NextNSAnim(long DSektor)
{
  long TekSektor;
  //���������� ����� �������
  TekSektor=NSAnim[TekIndxNSAnim];
  TekSektor+=DSektor;
  for(long i=TekIndxNSAnim;i<N_of_NSAnim;i++)
  {
    if(NSAnim[i]>=TekSektor)
    {
      TekIndxNSAnim=i;
      return TekIndxNSAnim;
    }
  }
  TekIndxNSAnim=N_of_NSAnim-1;
  return TekIndxNSAnim;
}


int OBZORY_1::PrevNSAnim(long DSektor)
{
  long TekSektor;
  //���������� ����� �������
  TekSektor=NSAnim[TekIndxNSAnim];
  TekSektor-=DSektor;
  for(long i=TekIndxNSAnim;i>=0;i--)
  {
    if(NSAnim[i]<=TekSektor)
    {
      TekIndxNSAnim=i;
      return TekIndxNSAnim;
    }
  }
  TekIndxNSAnim=0;
  return TekIndxNSAnim;
}



int OBZORY_1::VyvodAllDataAnimTekSektor(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm)
{
 return VyvodAllDataAnim(NSAnim[TekIndxNSAnim],Image,LeftKm,BottomKm,RightKm,TopKm);
}                  


int OBZORY_1::VyvodAllDataAnimTekSektorNazad(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm)
{
 return VyvodAllDataAnimNazad(NSAnim[TekIndxNSAnim],Image,LeftKm,BottomKm,RightKm,TopKm);
}                  




int OBZORY_1::StopAnim(void)
{
  if(ODA2)
  {
    free(ODA2);
    ODA2=NULL;
  }

  if(NSAnim)
  {
    free(NSAnim);
    NSAnim=NULL;
  }
  N_of_OtschAnim=0;
  N_of_NSAnim=0;
  GlobalCGauge1->Visible=false;
  return 1;
}




int OBZORY_1::ShowDatasAnim1(TStrings *Strings,bool Nazad)
{
  long i;
  long MinNomerSektora,MaxNomerSektora;
  if(TekIndxNSAnim<0||TekIndxNSAnim>=N_of_NSAnim)return 0;

  if(Nazad)
  {
    if(IsHvost)
    {
      MaxNomerSektora=NSAnim[TekIndxNSAnim]+4*DlinaHvosta+3;
      MinNomerSektora=NSAnim[TekIndxNSAnim];
    }else{
      MaxNomerSektora=NSAnim[TekIndxNSAnim]+3;
      MinNomerSektora=NSAnim[TekIndxNSAnim];
    }
    Strings->Clear();
    for(i=N_of_OtschAnim-1;i>=0;i--)
    {
      if(ODA2[i].OB->SektorNomer>=MinNomerSektora&&
         ODA2[i].OB->SektorNomer<=MaxNomerSektora)
      {
        Strings->Add(GetStrokaFrom2(ODA2[i].OB));
      }
    }
  }else{
    if(IsHvost)
    {
      MaxNomerSektora=NSAnim[TekIndxNSAnim];
      MinNomerSektora=NSAnim[TekIndxNSAnim]-4*DlinaHvosta-3;
    }else{
      MaxNomerSektora=NSAnim[TekIndxNSAnim];
      MinNomerSektora=NSAnim[TekIndxNSAnim]-3;
    }
    Strings->Clear();
    for(i=0;i<N_of_OtschAnim;i++)
    {
      if(ODA2[i].OB->SektorNomer>=MinNomerSektora&&
         ODA2[i].OB->SektorNomer<=MaxNomerSektora)
      {
        Strings->Add(GetStrokaFrom2(ODA2[i].OB));
      }
    }

  }


  return 1;
}



int OBZORY_1::ShowDatasAnim(TStrings *Strings, bool Nazad,   //��������
    long Left, long Top, long Right, long Bottom)
{
  long i;
  long MinNomerSektora,MaxNomerSektora;
  if(TekIndxNSAnim<0||TekIndxNSAnim>=N_of_NSAnim)return 0;


  if(Nazad)
  {
    if(IsHvost)
    {
      MaxNomerSektora=NSAnim[TekIndxNSAnim]+4*DlinaHvosta+3;
      MinNomerSektora=NSAnim[TekIndxNSAnim];
    }else{
      MaxNomerSektora=NSAnim[TekIndxNSAnim]+3;
      MinNomerSektora=NSAnim[TekIndxNSAnim];
    }
    Strings->Clear();
    for(i=N_of_OtschAnim-1;i>=0;i--)
    {
      if(ODA2[i].OB->SektorNomer>=MinNomerSektora&&
         ODA2[i].OB->SektorNomer<=MaxNomerSektora)
      {
        if(ODA2[i].X>=0&&ODA2[i].X>=Left&&ODA2[i].X<=Right&&
           ODA2[i].Y>=Top&&ODA2[i].Y<=Bottom)
        {
           Strings->Add(GetStrokaFrom2(ODA2[i].OB));
        }
      }
    }
  }else{
    if(IsHvost)
    {
      MaxNomerSektora=NSAnim[TekIndxNSAnim];
      MinNomerSektora=NSAnim[TekIndxNSAnim]-4*DlinaHvosta-3;
    }else{
      MaxNomerSektora=NSAnim[TekIndxNSAnim];
      MinNomerSektora=NSAnim[TekIndxNSAnim]-3;
    }
    Strings->Clear();
    for(i=0;i<N_of_OtschAnim;i++)
    {
      if(ODA2[i].OB->SektorNomer>=MinNomerSektora&&
         ODA2[i].OB->SektorNomer<=MaxNomerSektora)
      {
        if(ODA2[i].X>=0&&ODA2[i].X>=Left&&ODA2[i].X<=Right&&
           ODA2[i].Y>=Top&&ODA2[i].Y<=Bottom)
        {
           Strings->Add(GetStrokaFrom2(ODA2[i].OB));
        }
      }
    }

  }
  return 1;
}    


