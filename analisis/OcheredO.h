//---------------------------------------------------------------------------
#ifndef OcheredOH
#include "Traekt.h"
#define OcheredOH
//---------------------------------------------------------------------------
/*������� ��������. 
OCHERED_O1 ��������� ���� �������. */
struct ODIN_BORT_2
{
  long NomA,Indx;  //����� �������, ������
  long NomBort;    //����� �����
  long Rejim;      //����� 2 RBS, 1 ���. 0 - ���������
  long Kanal;      //1 ��������� 2 ��������� 3 PS
  bool IsPrivyazan;  //�������� �� ������?
  double Azmt,  Dlnst;  //�������� �������� � ����������
  double Time;     //����� � �����
  long SektorNomer;
  bool Rasm;        //���������� ��� ������ ������

};






struct OCHERED_O1
{
  ODIN_BORT_2 OB2[200];
  ODIN_BORT_1 *OB1[200]; //������ ����������
  double DAzmt,DDaln; //�������� ��������
  int First,Last;  //���� ������ ���, ����� First � Last = -1

  bool IsInPlot;  //������� �� ��� ����

  OCHERED_O1(double DA,double DD)
  {
    First=-1; Last=-1; DAzmt=DA; DDaln=DD;
  };

//���������� �������� - ��� ��� ��������� ������ ���������� ������
//���������� 1
  int Add1(ODIN_BORT_1 *NewOB1);

//���������� �������� ��� ������ �������� ���������� ������
//���� ����� ��������, ����������� � ���������������� ��������
  int AddP(ODIN_BORT_1 *NewOB1,
                PARA_OTSCHETOV_OB1 **POOB1,
                int *N_of_POOB1);

//���������� �������� ���������� ������ ��� ������ ���� ������
  int AddS(ODIN_BORT_1 *NewOB1,
                PARA_OTSCHETOV_OB1 **POOB1,
                int *N_of_POOB1);



//��������� ������...
//���� ��� ������� �������
//����� ��� ���������� �������
//���������� 2, ���� ������������ P � PS �������
  int Add2(ODIN_BORT_2 *NewOB1,
           long NomA,long Indx,
           long *NomA_P, long *Indx_P, //������ ����� NomA_P, Indx_P
           long *NomA_S, long *Indx_S, //������ ������� S
           long *NomA_PS, long *Indx_PS);



  void Dodelat1(void);  //��� ������


};


#endif
