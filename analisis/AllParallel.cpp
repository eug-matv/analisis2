/*������ �������� � ����� AllParallel.h*/

//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop


#include "AllParallel.h"
#include "GlavOkno.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
//   Important: Methods and properties of objects in VCL can only be
//   used in a method called using Synchronize, for example:
//
//      Synchronize(UpdateCaption);
//
//   where UpdateCaption could look like:
//
//      void __fastcall TAllParallel::UpdateCaption()
//      {
//        Form1->Caption = "Updated in a thread";
//      }
//---------------------------------------------------------------------------

__fastcall TAllParallel::TAllParallel(int Zadacha)
        : TThread(true)
{

  CurZadacha=Zadacha;
  if(Zadacha==1) //�������� ��������������� ���������� - ���, ��� ����� ������� �����������
  {
     FreeOnTerminate=true;  //�����������������
     OnTerminate=MyOnTerminate1;

     FGlavOkno->Nachalo1();
  //�������� ����
     Resume();
  }


  if(Zadacha==3)
  {

  }


}
//---------------------------------------------------------------------------
void __fastcall TAllParallel::Execute()
{
        //---- Place thread code here ----
  if(CurZadacha==1) //�������� ��������������� ���������� - ���, ��� ����� ������� �����������
  {

     FGlavOkno->LoadFiles();
  }


}

//---------------------------------------------------------------------------



void __fastcall TAllParallel::MyOnTerminate1(TObject *Sender)
{
  if(CurZadacha==1)
  {
    FGlavOkno->Konec1();
  }

  if(CurZadacha==3)
  {
    FGlavOkno->KonecPrivyazki();
  }
}
