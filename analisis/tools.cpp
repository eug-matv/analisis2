//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include  <errno.h>
#include "tools.h"
#include "CSPIN.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)



long Okrugl(double Chislo1)
{
 double Chislo2,Chislo3;

 Chislo2=floor(Chislo1);
 Chislo3=Chislo1-Chislo2;
 if(Chislo3<0.5)return (long)Chislo2;
 return ((long)Chislo2+1);
}

int SravnStr(const void *First, const void *Second)
{

 return (strcmp(*((char**)First),*((char**)Second)));
}


char *QSearchStr(char *Strka, char **Strks,int N_of_Strks)
{
 void *Ret;
 Ret=bsearch(&Strka,Strks,N_of_Strks,sizeof(char*),SravnStr);
 if(Ret==NULL)return NULL;
 return (*(char**)Ret);
}




//��������� ������� � ���� ��������
long MilliSeconds(int NomerSutok,
                  int Chasy,
                  int Minuty,
                  double Secundy)
{
 //������� ��������� ��������
 long MS;
 long MS1;
 MS=(long)(floor(Secundy*1000.0));
 MS1=1000*(NomerSutok*24*3600+Chasy*3600+Minuty*60);
 return (MS1+MS);
}


long Minuts(int NomerSutok,
                  int Chasy,
                  int Minuty)
{
 return (NomerSutok*24*60+Chasy*60+Minuty);
}


long MilliSecondsFromMandS(long Minuty,double Secundy)
{
 return ((long)(floor(Secundy*1000.0))+60000*Minuty);
}

void GetHM_MSFormMilliSeconds(long Time,int &H,int &M,  int &S, int &MS)
{
 long Time1;
 Time1=Time%86400000L;
 H=Time1/3600000L;

 Time1%=3600000L;
 M=Time1/60000;

 Time1%=60000;
 S=Time1/1000;

 MS=Time1%1000;


}


/** ��������� ��� Edit � ����� **/
void SaveFormEdits(TForm *Form, AnsiString FileName)
{
 FILE *fp;
 int i;
 fp=fopen(FileName.c_str(),"w");
 if(!fp)return;
 for(i=0;i<Form->ComponentCount;i++)
 {
  if(Form->Components[i]->ClassNameIs("TEdit"))
  {
  //�������� ��������
   fprintf(fp,"%s ",(TEdit*)(Form->Components[i])->Name.c_str());
   if(((TEdit*)(Form->Components[i]))->Text.Length()==0)
   {
    fputs("null", fp);
   }else{
    fputs(((TEdit*)(Form->Components[i]))->Text.c_str(), fp);
   };
   fprintf(fp,"\n");
  }

  if(Form->Components[i]->ClassNameIs("TCSpinEdit"))
  {
  //�������� ��������
   fprintf(fp,"%s ",(TEdit*)(Form->Components[i])->Name.c_str());
   fputs(((TCSpinEdit*)(Form->Components[i]))->Text.c_str(), fp);
   fprintf(fp,"\n");
  }

 }

 fclose(fp);
}


/** ������� ��� Edit �� �����**/
void LoadFormEdits(TForm *Form, AnsiString FileName)
{
 FILE *fp;
 int i;
 char EN[255],*RetC;
 fp=fopen(FileName.c_str(),"r");
 if(!fp)return;
 for(i=0;i<Form->ComponentCount;i++)
 {
  if(Form->Components[i]->ClassNameIs("TEdit"))
  {

   fscanf(fp,"%s ",&EN);
   if(strcmp(EN,((TForm*)Form->Components[i])->Name.c_str())==0)
   {
    RetC=fgets(EN,254,fp);
    if(RetC)
    {
     if(strcmp(EN,"null\n")==0)
     {
      ((TEdit*)Form->Components[i])->Text="";
     }else{

      ((TEdit*)Form->Components[i])->Text=String(EN).Trim();
     }
    }
   }
  }
  if(Form->Components[i]->ClassNameIs("TCSpinEdit"))
  {
   fscanf(fp,"%s ",&EN);
   if(strcmp(EN,((TForm*)Form->Components[i])->Name.c_str())==0)
   {
    RetC=fgets(EN,254,fp);
    if(RetC)
    {
     ((TCSpinEdit*)Form->Components[i])->Text=String(EN).Trim();
    }
   }
  }
 }
 fclose(fp);
}

//���������� � ���� ������ ���� TCheckBox
void SaveFormCBandRB(TForm *Form, AnsiString FileName)
{
 FILE *fp;
 int i;
 fp=fopen(FileName.c_str(),"w");
 if(!fp)return;
 for(i=0;i<Form->ComponentCount;i++)
 {
  if(Form->Components[i]->ClassNameIs("TCheckBox"))
  {
  //�������� ��������
   fprintf(fp,"%s ",(TCheckBox*)(Form->Components[i])->Name.c_str());
   if(((TCheckBox*)(Form->Components[i]))->Checked)
   {
    fputs("true", fp);
   }else{
    fputs("false", fp);
   };
   fprintf(fp,"\n");
  }

  if(Form->Components[i]->ClassNameIs("TRadioButton"))
  {
  //�������� ��������
   fprintf(fp,"%s ",(TRadioButton*)(Form->Components[i])->Name.c_str());
   if(((TRadioButton*)(Form->Components[i]))->Checked)
   {
    fputs("true", fp);
   }else{
    fputs("false", fp);
   }
   fprintf(fp,"\n");
  }
 }
 fclose(fp);
}

/** ������� ��� Edit �� �����**/
void LoadFormCBandRB(TForm *Form, AnsiString FileName)
{
 FILE *fp;
 int i;
 char EN[255],*RetC;
 fp=fopen(FileName.c_str(),"r");
 if(!fp)return;
 for(i=0;i<Form->ComponentCount;i++)
 {
  if(Form->Components[i]->ClassNameIs("TCheckBox"))
  {

   fscanf(fp,"%s ",&EN);
   if(strcmp(EN,((TCheckBox*)Form->Components[i])->Name.c_str())==0)
   {
    RetC=fgets(EN,254,fp);
    if(RetC)
    {
     if(EN[0]=='t'&&EN[1]=='r'&&EN[2]=='u'&&EN[3]=='e')
     {
      ((TCheckBox*)Form->Components[i])->Checked=true;
     }else{

      ((TCheckBox*)Form->Components[i])->Checked=false;
     }
    }
   }
  }

  if(Form->Components[i]->ClassNameIs("TRadioButton"))
  {
   fscanf(fp,"%s ",&EN);
   if(strcmp(EN,((TRadioButton*)Form->Components[i])->Name.c_str())==0)
   {
    RetC=fgets(EN,254,fp);
    if(RetC)
    {
     if(EN[0]=='t'&&EN[1]=='r'&&EN[2]=='u'&&EN[3]=='e')
     {
      ((TRadioButton*)Form->Components[i])->Checked=true;
     }else{

      ((TRadioButton*)Form->Components[i])->Checked=false;
     }
    }
   }
  }
 }

 fclose(fp);
}



/************����������� ��������� �� ��� ����************/

//���� ���� ��������� ���������� 1, ���� UNIX ���������� 2,

//���� ������ ������� ���������� 0
int IsTextFile(AnsiString FileName)
{
 FILE *fp;
 int errno_d;
 int i;
 char c,cp=' ';
//������� � �������� ������
 fp=fopen(FileName.c_str(),"rb");
 if(fp==NULL)
 {
        errno_d=errno;
                MessageBox(NULL, String(errno_d).c_str() ,FileName.c_str(),MB_OK);
        return 0;
 }
 for(i=0; i<10000;i++)
 {
  if(feof(fp))
  {
   fclose(fp);
   return 1;
  }
  c=(char)fgetc(fp);
  if(c=='\n')
  {
   if(cp=='\r')
   {
    fclose(fp);
    return 1;
   }
   fclose(fp);
   return 2;
  }
  cp=c;
 }
 fclose(fp);
 return 1;
}


/*�������� ����� ��� ����������, ���� �� ��������� � ��� ��������� ���� �� ��������*/
FILE *MyFOpenR(AnsiString FileName)
{
 int Ret;
 FILE *fp;
 Ret=IsTextFile(FileName);
 if(Ret==0)return NULL;
 if(Ret==1)
 {
  fp=fopen(FileName.c_str(),"r");
  return fp;
 }
 fp=fopen(FileName.c_str(),"rb");
 return fp;
}



double RastMejduObjektami(double Asimut1, double Dalnost1,
                          double Asimut2, double Dalnost2)
{
 double DX;
 double DY;
 double DDaln;
 DX=cos(Asimut1*M_PI/180.0)*Dalnost1-cos(Asimut2*M_PI/180.0)*Dalnost2;
 DY=sin(Asimut1*M_PI/180.0)*Dalnost1-sin(Asimut2*M_PI/180.0)*Dalnost2;
 DDaln=DX*DX+DY*DY;
 return sqrt(DDaln);
}








///��������� ����� ����� ��� ����
AnsiString FileNameWithoutPath(AnsiString FN)
{

 int Length;
 int i;
 AnsiString FN1;
 FN1=FN.Trim();
 if((Length=FN1.Length())==0)return String("");
 for(i=Length;i>=1;i--)
 {
  if(FN1[i]=='\\')
  {
   if(i==Length)return String("");
   return FN1.SubString(i+1, Length-i);
  }
 }
 return FN1;

}

/*��������� ������� ����� �� ������. ����� ����� � ������ ���� ��������� ���� ���������,
���� ��������, ���� ������ � �������*/
int GetMassivZelyhChiselFromStrka(char *Strka,     //������
                                  long *Numbers,    //������
                                  long *N_of_Numbers,
                                  long MaxN_of_Numbers
                                  )//���������� �������
{
  int N=0,M=0;
  int i,Length=strlen(Strka);

  for(i=0;i<=Length;i++)
  {
    if(N==MaxN_of_Numbers)break;
    if(Strka[i]>=0x30&&Strka[i]<=0x39)
    {
      if(M==0)
      {
        Numbers[N]=(long)(Strka[i])-0x30;
        M++;
      }else{
        Numbers[N]=Numbers[N]*10+(long)(Strka[i])-0x30;
        M++;
      }
    }else{
      if(M!=0)
      {
        M=0;
        N++;
      }
    }
  }
  *N_of_Numbers=N;
  return 1;
}





//��������� ���� ����� � ��������� ������� � ��������� �� �����
int SravnDoubleRazryad(double Chislo1,double Chislo2,double Umnoj)
{
 long ChI1,ChI2;
 ChI1=Okrugl(Chislo1*Umnoj);
 ChI2=Okrugl(Chislo2*Umnoj);
 if(ChI1>ChI2)return 1;
 if(ChI1<ChI2)return -1;
 return 0;
}



//����������� ������ ����� ���� TStringList � TStrings
int StrngLstToStrngs(TStringList *SL, TStrings *Ss)
{
 int i;
 if(!Ss||!SL)return 0;
 Ss->Clear();
 for(i=0;i<SL->Count;i++)
 {
  Ss->Add(SL->Strings[i]);
 }
 return 1;
}




//���������� ����� ����� ���� int
int sort_function_int( const void *a, const void *b)
{
 return (*((int*)a) - *((int*)b));
}

void QSortInt(int *Massiv, int N_of_El)
{
 qsort(Massiv, N_of_El, sizeof(int),sort_function_int);
}




void GetTimeFromDouble(double FloatTime, int &Sutki,
                      int &H, int &M, double &S)
{
  double fM,fS;
  Sutki=0;
  while(FloatTime<0)
  {
    FloatTime+=24.0;
    Sutki--;
  }
  H=(int)FloatTime;
  fM=FloatTime-H;
  while(H>=24)
  {
    Sutki++;
    H-=24;
  }
  fM=fM*60;
  M=(int)(fM);
  fS=fM-M;
  S=fS*60.0;
}


//��������� ������� �� �������� �����
AnsiString   GetTimeFromDoubleToString(double FloatTime)
{
  int Sutki,H,M;
  double S;
  char  Strka[50];
  AnsiString RetS;
  GetTimeFromDouble(FloatTime,Sutki,H,M,S);
  sprintf(Strka,"%02d:%02d:%06.03lf",H,M,S);
  RetS=Strka;
  return RetS;
}





/*����������� ���� ��������� M �� N ����������*/
int FirstSochetaniy(long *Nmrs,long N, long M)
{
  int i;
  if(M>=N)return 0;
  for(i=0;i<M;i++)
  {
    Nmrs[i]=i;
  }
  return 1;
}



int NextSochetaniy(long *Nmrs,long N, long M,
                   long *UbratVydel, long *SdelatVydel,
                   long *N_of_Smen)
{
  int i,j;
  if(M>=N)return 0;          //������
  if(Nmrs[0]==N-M)return 2;  //��� ����������� ��� ��������� M �� N
  if(Nmrs[M-1]<N-1)          //���� Nmrs[M-1]<N-1
  {                          //����� ��������� ��������� ������� �� 1
    UbratVydel[0]=Nmrs[M-1]; //����� ������ ���������
    Nmrs[M-1]++;             //������� �������
    SdelatVydel[0]=Nmrs[M-1]; //����� ����� �������
    *N_of_Smen=1;            //������� ������� ���������
    return 1;
  }else{
    for(i=1;i<M;i++)
    {
      if(Nmrs[i]==N-M+i)    //���� ������ �������� �������
      {
        UbratVydel[0]=Nmrs[i-1];  //������ ���������
        Nmrs[i-1]++;              //�������� ������ ���� ������
        SdelatVydel[0]=Nmrs[i-1]; //��������, ��� ���������� ��������
//���������� ��� ����������� �������� ����� ����� ����
        for(j=i;j<M;j++)
        {
           UbratVydel[j-i+1]=Nmrs[j];  //��������� ��� ���� ���������� ���������
           Nmrs[j]=Nmrs[i-1]+j-i+1;    //����������� ������� ����� ����� ����
           SdelatVydel[j-i+1]=Nmrs[j]; //��������� ��� ����� �����.
        }
        *N_of_Smen=M-i+1;   //������� ���� ���������
        return 1;
      }
    }
  }

  return 1;
}




int NextSochetaniy2(long *Nmrs,long N, long M, long M1,
                   long *UbratVydel, long *SdelatVydel,
                   long *N_of_Smen)
{
  int i;

  if(Nmrs[M1]==N-M+M1)return 2;

  UbratVydel[0]=Nmrs[M1];
  Nmrs[M1]++;
  SdelatVydel[0]=Nmrs[M1];


  *N_of_Smen=1;
  for(i=M1+1;i<M;i++)
  {
     UbratVydel[*N_of_Smen]=Nmrs[i];
     Nmrs[i]=Nmrs[M1]+(*N_of_Smen);
     SdelatVydel[*N_of_Smen]=Nmrs[i];
     (*N_of_Smen)++;
  }
  return 1;
}




/*��������� GetTochkiNaGranice. ��������� ��������� ����������� ������ � �����.
��� ���������� ���� ������������ �������� ������ ���� �����.
*/

int GetTochkiNaGranicePriam(double X1,double Y1,double X2, double Y2, //���������� �����q
                       long X1r, long Y1r, long X2r, long Y2r,  //���������� ���������������
                       long *X1ret,long *Y1ret, long *X2ret, long *Y2ret) //���������� ������������
{
  double Kx,Ky; //����������� �������
  double Bx,By; //��������� ����
  long Y,X;
  double Vrem;

  *X1ret=-10000; *Y1ret=-10000; *X2ret=-10000; *Y2ret=-10000;
//���� Y/X<=1, �� �������� Y(X)... ����� X �� Y
  if(ABS(X2-X1)>=1&&ABS(Y2-Y1)>=1)
  {
     if(X1>X2)
     {
       Vrem=X1; X1=X2;X2=Vrem;
       Vrem=Y1; Y1=Y2;Y2=Vrem;
     }
     Ky=(Y2-Y1)/(X2-X1);
     Kx=(X2-X1)/(Y2-Y1);
     By=Y2-Ky*X2;
     Bx=X2-Kx*Y2;

//������ ����������� � X1r
     X=X1r;
     Y=Okrugl(Ky*X1r+By);
     if(Y>=Y1r&&Y<=Y2r)
     {
       *X1ret=X; *Y1ret=Y;
     }else{
       if(Ky<0)Y=Y2r; else Y=Y1r;
       X=Okrugl(Kx*Y+Bx);
       if(X>=X1r&&X<=X2r)
       {
         *X1ret=X; *Y1ret=Y;
       }else{
         return 0;
       }
     }
     X=X2r;
     Y=Okrugl(Ky*X2r+By);
     if(Y>=Y1r&&Y<=Y2r)
     {
       *X2ret=X; *Y2ret=Y;
     }else{
       Y=Y2r;
       if(Ky<0)Y=Y1r; else Y=Y2r;
       X=Okrugl(Kx*Y+Bx);
       if(X>=X1r&&X<=X2r)
       {
         *X2ret=X; *Y2ret=Y;
       }else{
         return 0;
       }
     }
  }else if(ABS(X1-X2)<1)  //������������ �����
  {
      if((X1+X2)/2>=X1r&&(X1+X2)/2<=X2r)
      {
         *X1ret=*X2ret=Okrugl((X1+X2)/2); *Y1ret=Y1r; *Y2ret=Y2r;
      }else{
        return 0;
      }
  }else{
      if((Y1+Y2)/2>=Y1r&&(Y1+Y2)/2<=Y2r)
      {
         *Y1ret=*Y2ret=Okrugl((Y1+Y2)/2); *X1ret=X1r; *X2ret=X2r;
      }else{
        return 0;
      }

  }
  return 1;
}

/*��������� ���������� ����� �����*/
AnsiString GetTempFileInTempPath(void)
{
  AnsiString FN,PF;
  int i;
  int Ret;
  char PathTemp[2000];

//������� ��������� ����
  Ret=GetTempPath(1999,PathTemp);
  if(Ret==0)return "";
  PF=PathTemp;
  if(PF[PF.Length()]=='\\')
  {
    PF=PF.SubString(1,PF.Length()-1);
  }

//���������� ��� �����
  for(i=0;i<1000;i++)
  {
    FN=PF+String("\\Anls")+IntToStr(i)+String(".tmp");
    if(!FileExists(FN))break;
  }

  return FN;
}


/*��������� ������� ��������*/
double toolsGetDAzimuth(double Azmt1, double Azmt2)
{
    if(Azmt1>Azmt2)
    {
        if(Azmt1-Azmt2>180.0)
        {
                return (Azmt1-Azmt2-360.0);
        }
        return Azmt1-Azmt2;
    }

    if(Azmt1-Azmt2<-180.0)
    {
       return (Azmt1-Azmt2+360.0);
    }
    return Azmt1-Azmt2;
}

double toolsGetMidAzimuth(double Azmt1, double koef1, double Azmt2)
{
      
}

/*��������� ��������, ������� ������� ������ �� dfTime1 �� dfTime2 � �����.
�������, ��� ������ 12 ����� ������ �� �����
*/
double toolsGetDTime(   double dfTime1,
                        double dfTime2)
{
        if(dfTime2>=dfTime1)
        {
                if(dfTime2-dfTime1>12.0)
                {
                        return (dfTime2-dfTime1-24.0);
                }
                return (dfTime2-dfTime1);
        }
//dfTime1>dfTime2
        if(dfTime1-dfTime2>12.0)
        {
                return (dfTime2-dfTime1+24.0);
        }
        return (dfTime2-dfTime1);
}
