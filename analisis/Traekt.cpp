//�������� �������� � Traekt.h
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <IniFiles.hpp>
#include <alloc.h>
#include <mem.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "RabsBort.h"
#include "tools.h"
#include "tools_get_nomer_obzora.h"
#include "NotPriamZone.h"
#include "Traekt.h"


#define ABS(X) (((X) > 0) ? (X) : (-(X)))

//---------------------------------------------------------------------------
#pragma package(smart_init)


AnsiString PutKProgramme;  //���� � ���������
AnsiString PutKTemp;       //���� � ����� ��������� ������
AnsiString NastroikiCfg;  //���� �� ����� �����������
AnsiString CurPathCfg;  //���� �� ����� �����������

AnsiString CurPath;    //������� ����



TCGauge *GlobalCGauge1=NULL;


//��������� ������������� ������� ��� ���������� ������� ������ �������
long PromFindNextObzorForTraekt(long LastObzor,
                               double TO,
                               short LastNomerAzmt,
                               short NomerAzmt,
                               double LastT,
                               double T,
                               int *StatusNext) //1 - ���� �� ���������,
                                                //0 - ���� �� �� ��������� � �����-�� ������ ���������
{

  double dT;  //�� ������� ������� � � ��������� ������
  long PrirostAzimut, NewAzmtLong;
  long NewAzmt,      //����� �������������� �������� ������� � ��������
        LeftNewAzmt,  // NewAzmt - 45 �������� � ������ �������� ����� 0
        RightNewAzmt;//NewAzmt + 45 �������� � ������ �������� ����� 0 (360)
  long DO;
  *StatusNext=1;
  dT=T-LastT;

//���������, �� ������� ������ �������� �����
  PrirostAzimut=(long)(dT/TO*360);   //��������� ������� � ��������
  NewAzmtLong=LastNomerAzmt+PrirostAzimut; //����� ������
  DO=NewAzmtLong/360;       //�� ������� ������� ������ ���� �������
  NewAzmt=(short)(NewAzmtLong%360);  //�������������� ��������� ������
  LeftNewAzmt=NewAzmt-45;   //������� �������������� ����������� (������ � �����)
  if(LeftNewAzmt<0)LeftNewAzmt+=360;
  RightNewAzmt=NewAzmt+45;
  if(RightNewAzmt>360)RightNewAzmt-=360;

//������� �������� ��� �������� �� �������� ����� 360 �������� ����������
  if(LeftNewAzmt<RightNewAzmt)
  {
 //�������� �������� �� ���� ����� � ���� ��������
      if(NomerAzmt>RightNewAzmt||NomerAzmt<LeftNewAzmt)
      {
        *StatusNext=0;
      }
      return (LastObzor+DO);
  }

//� ��� ������� �������, ���� LeftNewAzmt>RightNewAzmt
  if(NewAzmt>LeftNewAzmt) //������ NewAzmt<
  {
    if(NomerAzmt<LeftNewAzmt&&NomerAzmt>RightNewAzmt)
    {
       *StatusNext=0;
    }
    if(NomerAzmt<=RightNewAzmt)
    {
      DO++;
    }
  }else{            //������ NewAzmt<RightNewAzmt
    if(NomerAzmt>=LeftNewAzmt)
    {
      DO--;
    }
  }
  return (LastObzor+DO);
}


/*���������� �������� ��������*/
bool PromAsimutDiapazonForTraekt(double Azmt,double Asmt1,double Asmt2)
{
//
 if(Asmt1>Asmt2)
 {
  if(Azmt<Asmt1&&Azmt>Asmt2)
  {
   return false;
  }
  return true;
 }else{
  if(Azmt<=Asmt2&&Azmt>=Asmt1)
  {
   return true;
  }
 }
 return false;
}






OBZORY_1::OBZORY_1()
{
  int Ret;
  fps=NULL;
  N_of_fps=0;
  o_b_1=NULL;
  N_of_o_b_1=NULL;
  MaxN_of_o_b_1=NULL;
  POOB1_p=POOB1_uvd=POOB1_rbs=NULL;
  N_of_POOB1_p=N_of_POOB1_uvd=N_of_POOB1_rbs=0;
  IsWasSearchingRS_P=false;  IsWasSearchingRS_VK=false;
  curIndxOfOpenF=-1;
  curOpenF=NULL;
  FirstAbs=LastAbs=-1;
  otis=(struct ONE_TRASSA_INFO  *)malloc(sizeof(struct ONE_TRASSA_INFO));
  approx_interval=1.0/60.0;             //������ ����� ������
  period_for_out_trass=1.0/60.0;        //����� ������ ������, ������ ���� ������ �����

  Ret=LoadOption();
  if(Ret==0)
  {
    DiametrMetok=1;
    TO=4.58/3600;
    is180=0;
    IsInPlot=false;
    IsVisNumber=true;
    IsVisTime=false;
    IsCheckTime=false;
    IsCheckNumber=true;
    IsCheckVys=false;
    IskluchVys=false;
    IsCheckAzmt=false;
    IsCheckDlnst=false;
    KanalP=KanalS=KanalPS=true;
    RejimUVD=true; RejimRBS=true;
    VydelyatSevera=true;
    IsCheckSignalPK=false;
    dfMinSignalPK=1000.0;
    Napravlenie=0; //��� �������� �� �����������...
    RSA_RBS1=RSA_UVD1=4.5;  //� ��������
    RSD_RBS1=RSD_UVD1=0.4;  //� ����������
    RSA_P1=3.5;  //� ��������
    RSD_P1=0.4;  //� ����������
    RSA_RBS2=RSA_UVD2=4.5;  //� ��������
    RSD_RBS2=RSD_UVD2=0.4;  //� ����������
    RSA_P2=3.5;  //� ��������
    RSD_P2=0.4;  //� ����������
    IsAnimNomer=true;
    IsAnimVysota=true;
    IsAnimTime=false;
    IsHvost=false;
    DlinaHvosta=2;
    Dlnst1=Dlnst2=Azmt11=Azmt12=Azmt21=Azmt22=-1;
    VysotaRLS=0;
    IsLojnyyInfoVyvod=true;
    IsIstinnyRightDopData=false;
    OblastCol=clBlack;  //������ ���� �� ���������
    Kolca1Col=(TColor)RGB(0,0xF0,0);
    Kolca2Col=(TColor)RGB(0,0x80,0);
    Rad90Col=(TColor)RGB(0,0xF0,0);
    Rad30Col=(TColor)RGB(0,0x80,0);
    RamkaCol=clWhite;
    Text_Col=clWhite;
    P_Col=clBlue;
    S_Col=clYellow;
    PS_Col=clLime;
    RS_P_Col=clRed;
    RS_UVD_Col=clFuchsia;
    RS_RBS_Col=(TColor)0x000000C0;
    N_of_NomBortKO_UVD=N_of_NomBortKO_RBS=2;
    NomBortKO_UVD[0]=10001;    NomBortKO_UVD[1]=55555;
    NomBortKO_RBS[0]=1;        NomBortKO_RBS[1]=5555;
    dRazmerOknaDlyaZonyObzora=10.0;
//���������
    cPechatPK=0;
    cPechatVK=225;
    cPechatObjed=140;
    isSetMaxDalnGOST=false;
    iMaxDalnGOST_PK=150;
    iMaxDalnGOST_VK=350;


  }
  RSA_UVD2=RSA_UVD1; RSA_RBS2=RSA_RBS1; RSA_P2=RSA_P1;
  RSD_UVD2=RSD_UVD1; RSD_RBS2=RSD_RBS1; RSD_P2=RSD_P1;



  DopDVys=150;
  ChisloO=NULL;


  Time1=Time2=-1;





  ChisloOtschetov=0;
  ChisloFilterOtschetov=0;
  ChisloVydelOtschetov=0;
  ChisloVydelOtschetovP=0;
  ChisloVydelOtschetovS=0;
  ChisloVydelOtschetovPS=0;
  ChisloBlizkihOtschetovP=0;
  ChisloBlizkihOtschetovS_UVD=0;
  ChisloBlizkihOtschetovS_RBS=0;
  IsWasPrivyazka=false;



  FindRadialTraekt=false;
  FindForP_PS=false;
  MaxMinD=30;
  MinMaxD=300;

  Pereskoki=NULL;
  PrevPereskoki=NULL;
  N_of_Pereskokov=0;




};



static void tempSaveOneOption(TIniFile *ini, char *name, int data)
{
        ini->WriteInteger(String("NASTROIKI"),String(name), data);
}

static void tempSaveOneOption(TIniFile *ini, char *name, double data)
{
        char strka[100];
        sprintf(strka,"%lf",data);
        ini->WriteString(String("NASTROIKI"),String(name), String(strka));
        
}


static void tempSaveOneOption(TIniFile *ini, char *name, long data)
{
        ini->WriteInteger(String("NASTROIKI"),String(name), (int)data);
}


static void tempSaveOneOption(TIniFile *ini, char *name,  unsigned long data)
{
        char strka[100];
        sprintf(strka,"%08lX",data);
        ini->WriteString(String("NASTROIKI"),String(name), String(strka));
}

static void tempSaveOneOption(TIniFile *ini, char *name,  char *strka)
{
        ini->WriteString(String("NASTROIKI"),String(name), String(strka));
}




int OBZORY_1::SaveOption(void) //���������� �������� � ���� - ���������� 1, ���� ������
                        //����� 0
{
   FILE *fp;
   int i;

   TIniFile *ini;
   ini=new TIniFile(NastroikiCfg);
   tempSaveOneOption(ini,"DiametrMetok",DiametrMetok);
   tempSaveOneOption(ini,"PustAORL_1AS",1);
   tempSaveOneOption(ini,"TO",(double)TO);
   tempSaveOneOption(ini,"is180", (int)(is180!=0));
   tempSaveOneOption(ini,"IsInPlot",(int)IsInPlot);
   tempSaveOneOption(ini,"IsVisNumber",(IsVisNumber ? 1 : 0));
   tempSaveOneOption(ini,"IsVisTime",(IsVisTime ? 1 : 0));
   tempSaveOneOption(ini,"IsCheckTime",(IsCheckTime ? 1 : 0));
   tempSaveOneOption(ini,"IsCheckNumber",(IsCheckNumber ? 1 : 0));
   tempSaveOneOption(ini,"IsCheckVys",(IsCheckVys ? 1 : 0));
   tempSaveOneOption(ini,"IskluchVys",(IskluchVys ? 1 : 0));
   tempSaveOneOption(ini,"IsCheckAzmt",(IsCheckAzmt ? 1 : 0));
   tempSaveOneOption(ini,"IsCheckDlnst",(IsCheckDlnst ? 1 : 0));
   tempSaveOneOption(ini,"KanalP",(KanalP ? 1 : 0));
   tempSaveOneOption(ini,"KanalS",(KanalS ? 1 : 0));
   tempSaveOneOption(ini,"KanalPS",(KanalPS ? 1 : 0));
   tempSaveOneOption(ini,"IsCheckSignalPK",(IsCheckSignalPK ? 1 : 0));
   tempSaveOneOption(ini, "dfMinSignalPK",dfMinSignalPK);

   tempSaveOneOption(ini,"VydelyatSevera", (VydelyatSevera ? 1 : 0));
   tempSaveOneOption(ini,"RSA_RBS1", RSA_RBS1);
   tempSaveOneOption(ini,"RSA_UVD1", RSA_UVD1);
   tempSaveOneOption(ini,"RSA_P1", RSA_P1);
   tempSaveOneOption(ini,"RSD_RBS1", RSD_RBS1);
   tempSaveOneOption(ini,"RSD_UVD1", RSD_UVD1);
   tempSaveOneOption(ini,"RSD_P1", RSD_P1);
   tempSaveOneOption(ini,"RSA_RBS2", RSA_RBS2);
   tempSaveOneOption(ini,"RSA_UVD2", RSA_UVD2);
   tempSaveOneOption(ini,"RSA_P2", RSA_P2);
   tempSaveOneOption(ini,"RSD_RBS2", RSD_RBS2);
   tempSaveOneOption(ini,"RSD_UVD2", RSD_UVD2);
   tempSaveOneOption(ini,"RSD_P2", RSD_P2);
   tempSaveOneOption(ini,"IsAnimNomer",(IsAnimNomer ? 1 : 0));
   tempSaveOneOption(ini,"IsAnimVysota",(IsAnimVysota ? 1 : 0));
   tempSaveOneOption(ini,"IsAnimTime",(IsAnimTime ? 1 : 0));
   tempSaveOneOption(ini,"IsHvost",(IsHvost ? 1 : 0));
   tempSaveOneOption(ini,"DlinaHvosta", DlinaHvosta);
   tempSaveOneOption(ini,"Dlnst1", Dlnst1);
   tempSaveOneOption(ini,"Dlnst2", Dlnst2);
   tempSaveOneOption(ini,"Azmt11", Azmt11);
   tempSaveOneOption(ini,"Azmt12", Azmt12);
   tempSaveOneOption(ini,"Azmt21", Azmt21);
   tempSaveOneOption(ini,"Azmt22", Azmt22);
   tempSaveOneOption(ini,"VysotaRLS",VysotaRLS);
   tempSaveOneOption(ini,"Vys1",Vys1);
   tempSaveOneOption(ini,"Vys2",Vys2);
   tempSaveOneOption(ini,"IsIstinnyRightDopData",(IsIstinnyRightDopData ? 1 : 0));
   tempSaveOneOption(ini,"IsLojnyyInfoVyvod",(IsLojnyyInfoVyvod ? 1 : 0));
   tempSaveOneOption(ini,"RejimUVD",(RejimUVD ? 1 : 0));
   tempSaveOneOption(ini,"RejimRBS",(RejimRBS ? 1 : 0));
   tempSaveOneOption(ini,"OblastCol",(ULONG)OblastCol);
   tempSaveOneOption(ini,"Kolca1Col",(ULONG)Kolca1Col);
   tempSaveOneOption(ini,"Kolca2Col",(ULONG)Kolca2Col);
   tempSaveOneOption(ini,"Rad90Col",(ULONG)Rad90Col);
   tempSaveOneOption(ini,"Rad30Col",(ULONG)Rad30Col);
   tempSaveOneOption(ini,"RamkaCol",(ULONG)RamkaCol);
   tempSaveOneOption(ini,"P_Col",(ULONG)P_Col);
   tempSaveOneOption(ini,"S_Col",(ULONG)S_Col);
   tempSaveOneOption(ini,"PS_Col",(ULONG)PS_Col);
   tempSaveOneOption(ini,"RS_UVD_Col",(ULONG)RS_UVD_Col);
   tempSaveOneOption(ini,"RS_RBS_Col",(ULONG)RS_RBS_Col);
   tempSaveOneOption(ini,"RS_P_Col",(ULONG)RS_P_Col);
   tempSaveOneOption(ini,"Text_Col",(ULONG)Text_Col);
   tempSaveOneOption(ini,"MestoIspytaniy",MestoIspytaniy);

/*
   fprintf(fp,"NomBortKO_UVD ");
   for(i=0;i<N_of_NomBortKO_UVD;i++)
   {
      if(i==0)
      {
        fprintf(fp,"%d",NomBortKO_UVD[i]);
      }else{
        fprintf(fp,", %d",NomBortKO_UVD[i]);
      }
   }
   fprintf(fp," \n");


   fprintf(fp,"NomBortKO_RBS ");
   for(i=0;i<N_of_NomBortKO_RBS;i++)
   {
      if(i==0)
      {
        fprintf(fp,"%d",NomBortKO_RBS[i]);
      }else{
        fprintf(fp,", %d",NomBortKO_RBS[i]);
      }
   }
   fprintf(fp," \n");
*/
   tempSaveOneOption(ini,"cPechatPK",cPechatPK%256);
   tempSaveOneOption(ini,"cPechatVK",cPechatVK%256);
   tempSaveOneOption(ini,"cPechatObjed",cPechatObjed%256);

   if(isSetMaxDalnGOST)
        tempSaveOneOption(ini,"isSetMaxDalnGOST",1);
   else
        tempSaveOneOption(ini,"isSetMaxDalnGOST",0);

   tempSaveOneOption(ini, "iMaxDalnGOST_PK",iMaxDalnGOST_PK);
   tempSaveOneOption(ini, "iMaxDalnGOST_VK",iMaxDalnGOST_VK);

   delete ini;

   return 1;
}



static int tempLoadOneOption(TIniFile *ini, char *name, int &data)
{
        int  iRez;
        iRez=ini->ReadInteger(String("NASTROIKI"),String(name), -123456789);
        if(iRez==-123456789)
        {
                return 0;
        }
        data=iRez;
        return 1;
}


static int tempLoadOneOption(TIniFile *ini, char *name, long &data)
{
        int  iRez;
        iRez=ini->ReadInteger(String("NASTROIKI"),String(name), -123456789);
        if(iRez==-123456789)
        {
                return 0;
        }
        data=(long)iRez;
        return 1;
}


static int tempLoadOneOption(TIniFile *ini, char *name, double &data)
{
        int iRet;
        AnsiString asRez;
        double dbl;
        asRez=ini->ReadString(String("NASTROIKI"),String(name), String("hui"));
        if(asRez==String("hui"))
        {
                return 0;
        }
        iRet=sscanf(asRez.c_str(),"%lf",&dbl);
        if(iRet!=1)
        {
                return 0;
        }
        data=dbl;

        return 1;
}


static int tempLoadOneOption(TIniFile *ini, char *name, unsigned long &data)
{
        int iRet;
        AnsiString asRez;
        unsigned long ul;
        asRez=ini->ReadString(String("NASTROIKI"),String(name), String("hui"));
        if(asRez==String("hui"))
        {
                return 0;
        }
        iRet=sscanf(asRez.c_str(),"%X",&ul);
        if(iRet!=1)
        {
                return 0;
        }
        data=ul;

        return 1;
}


static int tempLoadOneOption(TIniFile *ini, char *name, TColor &data)
{
        unsigned long ul;
        if(tempLoadOneOption(ini,name,ul))
        {
                data=(TColor)ul;
                return 1;
        }
        return 0;
}

static int tempLoadOneOption(TIniFile *ini, char *name, char *data, int sz)
{
        int iRet;
        AnsiString asRez;

        asRez=ini->ReadString(String("NASTROIKI"),String(name), String("hui"));
        if(asRez==String("hui"))
        {
                return 0;
        }

        strncpy(data,asRez.c_str(),sz-1);
        data[sz-1]=0;
        return 1;
}


static int tempLoadOneOption(TIniFile *ini, char *name, bool &data)
{
        int n;
        if(tempLoadOneOption(ini,name,n))
        {
                data=(n!=0);
                return 1;
        }
        return 0;

}





int OBZORY_1::LoadOption(void) //���������� �������� � ���� - ���������� 1, ���� ������
                        //����� 0
{
   FILE *fp;
   int i;
   int Nom;

   TIniFile *ini;
   ini=new TIniFile(NastroikiCfg);
   int iRet;

   iRet=1;

iRet&=tempLoadOneOption(ini,"DiametrMetok",DiametrMetok);
iRet&=tempLoadOneOption(ini,"TO", TO);
iRet&=tempLoadOneOption(ini,"is180", is180);
iRet&=tempLoadOneOption(ini,"IsInPlot",IsInPlot);
iRet&=tempLoadOneOption(ini,"IsVisNumber", IsVisNumber);
iRet&=tempLoadOneOption(ini,"IsVisTime",IsVisTime);
iRet&=tempLoadOneOption(ini,"IsCheckTime",IsCheckTime);
iRet&=tempLoadOneOption(ini,"IsCheckNumber", IsCheckNumber);
iRet&=tempLoadOneOption(ini,"IsCheckVys", IsCheckVys);
iRet&=tempLoadOneOption(ini,"IskluchVys", IskluchVys);
iRet&=tempLoadOneOption(ini,"IsCheckAzmt", IsCheckAzmt);
iRet&=tempLoadOneOption(ini,"IsCheckDlnst", IsCheckDlnst);
iRet&=tempLoadOneOption(ini,"KanalP",KanalP);
iRet&=tempLoadOneOption(ini,"KanalS", KanalS);
iRet&=tempLoadOneOption(ini, "KanalPS",KanalPS);
iRet&=tempLoadOneOption(ini,"IsCheckSignalPK",IsCheckSignalPK);
iRet&=tempLoadOneOption(ini, "dfMinSignalPK",dfMinSignalPK);

iRet&=tempLoadOneOption(ini, "VydelyatSevera", VydelyatSevera);
iRet&=tempLoadOneOption(ini,"RSA_RBS1", RSA_RBS1);
iRet&=tempLoadOneOption(ini,"RSA_UVD1", RSA_UVD1);
iRet&=tempLoadOneOption(ini,"RSA_P1", RSA_P1);
iRet&=tempLoadOneOption(ini,"RSD_RBS1", RSD_RBS1);
iRet&=tempLoadOneOption(ini,"RSD_UVD1", RSD_UVD1);
iRet&=tempLoadOneOption(ini,"RSD_P1", RSD_P1);
iRet&=tempLoadOneOption(ini,"RSA_RBS2", RSA_RBS2);
iRet&=tempLoadOneOption(ini,"RSA_UVD2", RSA_UVD2);
iRet&=tempLoadOneOption(ini,"RSA_P2", RSA_P2);
iRet&=tempLoadOneOption(ini,"RSD_RBS2", RSD_RBS2);
iRet&=tempLoadOneOption(ini,"RSD_UVD2", RSD_UVD2);
iRet&=tempLoadOneOption(ini,"RSD_P2", RSD_P2);
iRet&=tempLoadOneOption(ini,"IsAnimNomer", IsAnimNomer);
iRet&=tempLoadOneOption(ini,"IsAnimVysota", IsAnimVysota);
iRet&=tempLoadOneOption(ini,"IsAnimTime", IsAnimTime);
iRet&=tempLoadOneOption(ini,"IsHvost",IsHvost);
iRet&=tempLoadOneOption(ini,"DlinaHvosta", DlinaHvosta);
iRet&=tempLoadOneOption(ini,"Dlnst1", Dlnst1);
iRet&=tempLoadOneOption(ini,"Dlnst2", Dlnst2);
iRet&=tempLoadOneOption(ini,"Azmt11", Azmt11);
iRet&=tempLoadOneOption(ini,"Azmt12", Azmt12);
iRet&=tempLoadOneOption(ini,"Azmt21", Azmt21);
iRet&=tempLoadOneOption(ini,"Azmt22", Azmt22);
iRet&=tempLoadOneOption(ini,"VysotaRLS",VysotaRLS);
iRet&=tempLoadOneOption(ini,"Vys1",Vys1);
iRet&=tempLoadOneOption(ini,"Vys2",Vys2);
iRet&=tempLoadOneOption(ini,"IsIstinnyRightDopData",IsIstinnyRightDopData);
iRet&=tempLoadOneOption(ini,"IsLojnyyInfoVyvod",IsLojnyyInfoVyvod);
iRet&=tempLoadOneOption(ini,"RejimUVD",RejimUVD);
iRet&=tempLoadOneOption(ini,"RejimRBS",RejimRBS);
iRet&=tempLoadOneOption(ini,"OblastCol",OblastCol);
iRet&=tempLoadOneOption(ini,"Kolca1Col",Kolca1Col);
iRet&=tempLoadOneOption(ini,"Kolca2Col",Kolca2Col);
iRet&=tempLoadOneOption(ini,"Rad90Col",Rad90Col);
iRet&=tempLoadOneOption(ini,"Rad30Col",Rad30Col);
iRet&=tempLoadOneOption(ini,"RamkaCol",RamkaCol);
iRet&=tempLoadOneOption(ini,"P_Col",P_Col);
iRet&=tempLoadOneOption(ini,"S_Col",S_Col);
iRet&=tempLoadOneOption(ini,"PS_Col",PS_Col);
iRet&=tempLoadOneOption(ini,"RS_UVD_Col",RS_UVD_Col);
iRet&=tempLoadOneOption(ini,"RS_RBS_Col",RS_RBS_Col);
iRet&=tempLoadOneOption(ini,"RS_P_Col",RS_P_Col);
iRet&=tempLoadOneOption(ini,"Text_Col",Text_Col);
iRet&=tempLoadOneOption(ini,"MestoIspytaniy",MestoIspytaniy, sizeof(MestoIspytaniy));

/*
   fprintf(fp,"NomBortKO_UVD ");
   for(i=0;i<N_of_NomBortKO_UVD;i++)
   {
      if(i==0)
      {
        fprintf(fp,"%d",NomBortKO_UVD[i]);
      }else{
        fprintf(fp,", %d",NomBortKO_UVD[i]);
      }
   }
   fprintf(fp," \n");


   fprintf(fp,"NomBortKO_RBS ");
   for(i=0;i<N_of_NomBortKO_RBS;i++)
   {
      if(i==0)
      {
        fprintf(fp,"%d",NomBortKO_RBS[i]);
      }else{
        fprintf(fp,", %d",NomBortKO_RBS[i]);
      }
   }
   fprintf(fp," \n");
*/
iRet&=tempLoadOneOption(ini,"cPechatPK",cPechatPK);


iRet&=tempLoadOneOption(ini,"cPechatVK",cPechatVK);
iRet&=tempLoadOneOption(ini,"cPechatObjed",cPechatObjed);

    isSetMaxDalnGOST=false;               //���� ����������� ��� ����������� ������
    iMaxDalnGOST_PK=160;
    iMaxDalnGOST_VK=350;

    //������ � ����� ������������� �������������� ������ ���� ���� ������, ������ 10
    dRazmerOknaDlyaZonyObzora=10.0;



/*
   if(isSetMaxDalnGOST)
        tempSaveOneOption(ini,"isSetMaxDalnGOST",1);
   else
        tempSaveOneOption(ini,"isSetMaxDalnGOST",0);

   tempSaveOneOption(ini, "iMaxDalnGOST_PK",iMaxDalnGOST_PK);
   tempSaveOneOption(ini, "iMaxDalnGOST_VK",iMaxDalnGOST_VK);
  */


   delete ini;

   if(!iRet)
   {
        return 0;
   }

   return 1;
}



AnsiString OBZORY_1::GetStrokaFrom(long NomerA,long Indx)
{
  AnsiString RetStrka="";
  char StrkaNom[6];
  char Strka[500];

  int Dlina,i;
  FILE *fp;


  if(NomerA<0||NomerA>360)return RetStrka;
  if(Indx<0||Indx>=N_of_o_b_1[NomerA])return RetStrka;

  if(curOpenF)
  {
        if(curIndxOfOpenF==o_b_1[NomerA][Indx].NFile)
        {
           fp=curOpenF;
        }else{
           fclose(curOpenF);
           curOpenF=NULL;
        }
  }
  if(!curOpenF)
  {
        fp=(curOpenF=MyFOpenR(fps[o_b_1[NomerA][Indx].NFile]));
        curIndxOfOpenF=o_b_1[NomerA][Indx].NFile;
  }


  fseek(fp,o_b_1[NomerA][Indx].Smesh,0);

//� ������ ������� ������
  fgets(Strka,499, fp);

  if(NomerA==360)
  {
    return String(Strka);
  }

//������ �������� ������������ ������

  if(Strka[3]=='3')
  {
//    RetStrka=String(Strka).SubString(1,117);
      RetStrka=String(Strka).TrimRight();
      Dlina=RetStrka.Length();
      for(i=Dlina;i>=117;i--)
      {

        if(RetStrka[i]=='/')
        {
          RetStrka=String(Strka).SubString(1,i-1);
        }
      }
  }else{
//    RetStrka=String(Strka).SubString(1,103);
      RetStrka=String(Strka).TrimRight();
      Dlina=RetStrka.Length();
      for(i=Dlina;i>=103;i--)
      {
        if(RetStrka[i]=='/')
        {
          RetStrka=String(Strka).SubString(1,i-1);
        }
      }


  }


  if(IsLojnyyInfoVyvod&&(
      (o_b_1[NomerA][Indx].Status&LOJNY_NOMER_STATUS)||
      (o_b_1[NomerA][Indx].Status&DROBLENIE_STATUS)||
      (o_b_1[NomerA][Indx].Status&LOJNY_VYSOTA_STATUS)||
       (o_b_1[NomerA][Indx].Status&LOJNY_STATUS)||
       (o_b_1[NomerA][Indx].Status2&NE_PODVIJEN_STATUS2)||
        o_b_1[NomerA][Indx].OprVysota==0)
    )
  {
    RetStrka=RetStrka+String("  //");
  }





  if(o_b_1[NomerA][Indx].Status&LOJNY_NOMER_STATUS)
  {
    if(IsLojnyyInfoVyvod)
    {
      if(o_b_1[NomerA][Indx].Status&DROBLENIE_STATUS)
      {
        RetStrka=TrimRight(RetStrka)+String(" Spl.pl.!");
      }else if(o_b_1[NomerA][Indx].Status&LOJNY_STATUS)
      {
        RetStrka=TrimRight(RetStrka)+String(" False pl.!");
      }else if(o_b_1[NomerA][Indx].Status2&NE_PODVIJEN_STATUS2)
      {
        RetStrka=TrimRight(RetStrka)+String(" Not movable!");
      }else{
    //������� � �����
        RetStrka=TrimRight(RetStrka)+String(" TN: ")+
             String( (int)((otis[o_b_1[NomerA][Indx].IndxBrt].ulNumber)&0x00FFFFFF))+
             String(" DN: ")+
             String( (int)(o_b_1[NomerA][Indx].NomBort));
      }
    }


    if(IsIstinnyRightDopData)
    {
       if(((o_b_1[NomerA][Indx].Status&(DROBLENIE_STATUS|LOJNY_STATUS))==0)&&
         ((o_b_1[NomerA][Indx].Status2&NE_PODVIJEN_STATUS2)==0))
       {
//���������� �����
          sprintf(StrkaNom,"%5d",
            (int)((otis[o_b_1[NomerA][Indx].IndxBrt].ulNumber)&0x00FFFFFF));
          if(RetStrka[40]=='|')
          {
             for( int i=0;i<5;i++)RetStrka[41+i]=StrkaNom[i];
          }else{
             for( int i=0;i<5;i++)RetStrka[40+i]=StrkaNom[i];
          }
       }
    }
  }

  if(o_b_1[NomerA][Indx].Status&LOJNY_VYSOTA_STATUS||
     o_b_1[NomerA][Indx].OprVysota==0||
     (((o_b_1[NomerA][Indx].Status&3)==1)&&(((otis[o_b_1[NomerA][Indx].IndxBrt].ulNumber)&0xFF000000)==0x01000000)||
                                            ((otis[o_b_1[NomerA][Indx].IndxBrt].ulNumber)&0xFF000000)==0x02000000))

  {
    if(IsLojnyyInfoVyvod&&((o_b_1[NomerA][Indx].Status&(DROBLENIE_STATUS|LOJNY_STATUS))!=0)&&
      ((o_b_1[NomerA][Indx].Status2&NE_PODVIJEN_STATUS2)==0))
    {
      RetStrka=TrimRight(RetStrka)+String(" TA: ")+
      String((int)(o_b_1[NomerA][Indx].IstVysota))+String("  DA: ")+
      String((int)(o_b_1[NomerA][Indx].OprVysota));
    }
    if(IsIstinnyRightDopData)
    {
       if((o_b_1[NomerA][Indx].Status&(DROBLENIE_STATUS|LOJNY_STATUS))!=0
            &&((o_b_1[NomerA][Indx].Status2&NE_PODVIJEN_STATUS2)==0))
       {
          sprintf(StrkaNom,"%7d",
          (int)(o_b_1[NomerA][Indx].IstVysota));
          if(RetStrka[46]=='|')
          {
             for( int i=0;i<7;i++)RetStrka[47+i]=StrkaNom[i];
          }else{
             for( int i=0;i<7;i++)RetStrka[46+i]=StrkaNom[i];
          }

       }
    }
  }
  return (RetStrka+String("\n"));

}


AnsiString OBZORY_1::GetStrokaFrom2(ODIN_BORT_1 *OB1)
{
  AnsiString RetStrka="";
  char StrkaNom[6];
  char Strka[500];
  int Dlina,i;
  FILE *fp;
  if(curOpenF)
  {
        if(curIndxOfOpenF==OB1->NFile)
        {
           fp=curOpenF;
        }else{
           fclose(curOpenF);
           curOpenF=NULL;
        }
  }
  if(!curOpenF)
  {
        fp=(curOpenF=MyFOpenR(fps[OB1->NFile]));
        curIndxOfOpenF=OB1->NFile;
  }

  fseek(fp,OB1->Smesh,0);
//� ������ ������� ������
  fgets(Strka,499, fp);

//������ �������� ������������ ������
  if(Strka[3]=='3')
  {
//    RetStrka=String(Strka).SubString(1,117);
      RetStrka=String(Strka).TrimRight();
      Dlina=RetStrka.Length();
      for(i=Dlina;i>=117;i--)
      {

        if(RetStrka[i]=='/')
        {
          RetStrka=String(Strka).SubString(1,i-1);
        }
      }
  }else{
//    RetStrka=String(Strka).SubString(1,103);
      RetStrka=String(Strka).TrimRight();
      Dlina=RetStrka.Length();
      for(i=Dlina;i>=103;i--)
      {
        if(RetStrka[i]=='/')
        {
          RetStrka=String(Strka).SubString(1,i-1);
        }
      }
  }


  if(IsLojnyyInfoVyvod&&(
      (OB1->Status&LOJNY_NOMER_STATUS)||
      (OB1->Status&DROBLENIE_STATUS)||
      (OB1->Status&LOJNY_VYSOTA_STATUS)||
      (OB1->Status2&NE_PODVIJEN_STATUS2)||
      (OB1->Status&LOJNY_STATUS)||
       OB1->OprVysota==0)
    )
  {
    RetStrka=RetStrka+String("  //");
  }



  if(OB1->Status&LOJNY_NOMER_STATUS)
  {
    if(IsLojnyyInfoVyvod)
    {
      if(OB1->Status&DROBLENIE_STATUS)
      {
        RetStrka=TrimRight(RetStrka)+String(" Spl.plot!");
      }else if(OB1->Status&LOJNY_STATUS)
      {
        RetStrka=TrimRight(RetStrka)+String(" False pl.!");
      }else if(OB1->Status2&NE_PODVIJEN_STATUS2)
      {
        RetStrka=TrimRight(RetStrka)+String(" Not movable!");
      }else{
    //������� � �����
        RetStrka=TrimRight(RetStrka)+String(" TN: ")+
             String( (int)((otis[OB1->IndxBrt].ulNumber)&0x00FFFFFF))+
             String(" DN: ")+
             String( (int)(OB1->NomBort));
      }
    }


    if(IsIstinnyRightDopData)
    {
       if((OB1->Status&(DROBLENIE_STATUS|LOJNY_STATUS))==0&&
           (OB1->Status2&NE_PODVIJEN_STATUS2)==0)
       {
//���������� �����
          sprintf(StrkaNom,"%5d",
            (int)((otis[OB1->IndxBrt].ulNumber)&0x00FFFFFF));
          if(RetStrka[40]=='|')
          {
             for( int i=0;i<5;i++)RetStrka[41+i]=StrkaNom[i];
          }else{
             for( int i=0;i<5;i++)RetStrka[40+i]=StrkaNom[i];
          }
       }
    }
  }

  if(OB1->Status&LOJNY_VYSOTA_STATUS||
     OB1->OprVysota==0||
     (((OB1->Status&3)==1)&&((((otis[OB1->IndxBrt].ulNumber)&0xFF000000)==0x01000000))||
                             (((otis[OB1->IndxBrt].ulNumber)&0xFF000000)==0x02000000)))
  {
    if(IsLojnyyInfoVyvod&&
      (OB1->Status2&NE_PODVIJEN_STATUS2)==0)
    {
      RetStrka=TrimRight(RetStrka)+String(" TA: ")+
      String((int)(OB1->IstVysota))+String("  DA: ")+
      String((int)(OB1->OprVysota));
    }
    if(IsIstinnyRightDopData)
    {
       if((OB1->Status&(DROBLENIE_STATUS|LOJNY_STATUS))==0&&
           (OB1->Status2&NE_PODVIJEN_STATUS2)==0)
       {
      sprintf(StrkaNom,"%7d",
          (int)(OB1->IstVysota));
          if(RetStrka[46]=='|')
          {
             for( int i=0;i<7;i++)RetStrka[47+i]=StrkaNom[i];
          }else{
             for( int i=0;i<7;i++)RetStrka[46+i]=StrkaNom[i];
          }
       }
    }
  }
  return (RetStrka+String("\n"));
}


/*������ ������� �������� ��� ���������� � ������ �� ��������������.*/

int OBZORY_1::NewFiles(AnsiString *fn, int N_of_fn,double TempObzoraVSekundah)
{
  int i;

//�������� ��� �������������� ����������  
  FreeAll();
  IsWasPrivyazka=false;
  LastNomerObzora=-1;

  TO=TempObzoraVSekundah/3600;
//  fps=(FILE**)malloc(sizeof(FILE*)*N_of_fp);
  fps=new AnsiString[N_of_fn];
  N_of_fps=N_of_fn;
 // memcpy(fps, fp,sizeof(FILE*)*N_of_fp);
  for(i=0;i<N_of_fps;i++)fps[i]=fn[i];

  o_b_1=(ODIN_BORT_1**)malloc( sizeof(ODIN_BORT_1*)*362);
  N_of_o_b_1=(long*)malloc(sizeof(long)*362);
  MaxN_of_o_b_1=(long*)malloc(sizeof(long)*362);
  if(o_b_1==NULL)
  {
//    return;
  }
  for(i=0;i<362;i++)
  {
    MaxN_of_o_b_1[i]=N_of_o_b_1[i]=0;
    o_b_1[i]=NULL;
  }


  LastTime=0;
  LastChas=0;
  LastMinuta=0;
  NomerSutok=0;
  PromFTime=-1;

  LastVisual=-1;
  return 1;
}




OBZORY_1::~OBZORY_1()
{
//  SaveOption();
  FreeAll();
}



int OBZORY_1::Add(int NomFile, //����� �����
                  const DANN &Dan   //������ �� ������ �� �����
                   ) 
{
  unsigned short Status;
  double Time;
  int Nomer;
  unsigned long NewNumber;  //����� �����
  int i;
  int nm1,indx1;
  int iRet;

  long i_p;

  if(NomFile>=N_of_fps||NomFile<0)return 0; //�����
  Time=Dan.H+Dan.M/60.0+Dan.S/3600.0;
  if(PromFTime<0)
  {
    PromFTime=Dan.H+Dan.M/60.0+Dan.S/3600.0;
    NomerSutok=0;
    CLSutkiFirst=    NomerSutok; CLChasyFirst=Dan.H; CLMinutyFirst=Dan.M;
    CLSekundyFirst=(int)(Dan.S);
  }

  //������� ������ ������
  //��������� ����� ��� ��� �� �����
    if(Dan.P=='N')
    {
      Nomer=360;
    }else{
      Nomer=Dan.Asimut;
      if(Nomer>=360)Nomer-=360;
    }



  CLChasyCur=Dan.H;
  CLMinutyCur=Dan.M;


  CLSekundyCur=(int)Dan.S;

//��������� �� ������� ���� �������� ����� ������


//��������, �������� �������� ��������� ������ ��� �������
  if(N_of_o_b_1[Nomer]==MaxN_of_o_b_1[Nomer])
  {
//�������� ������ �� 1000 ��������
     MaxN_of_o_b_1[Nomer]+=1000;
     o_b_1[Nomer]=(ODIN_BORT_1*)realloc(o_b_1[Nomer],sizeof(ODIN_BORT_1)*
                                        MaxN_of_o_b_1[Nomer]);
  }

//������ ������� �������
  o_b_1[Nomer][N_of_o_b_1[Nomer]].NFile=(short)NomFile;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].Smesh=Dan.Smesh;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].IstVysota=o_b_1[Nomer][N_of_o_b_1[Nomer]].OprVysota=(short)Dan.Vysota;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].ParnOtschet1=-1;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].ParnOtschet2=-1;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].ParnForDubl=-1;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].Time=Time;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].Prev=
     o_b_1[Nomer][N_of_o_b_1[Nomer]].Next=-1;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].poryadk_nomer=(++last_poryadk_nomer);
  if(LastAbs<0)
  {
       FirstAbs=LastAbs=N_of_o_b_1[Nomer]*362+Nomer;
       o_b_1[Nomer][N_of_o_b_1[Nomer]].AbsPrev=
                o_b_1[Nomer][N_of_o_b_1[Nomer]].AbsPrev=-1;
  }else{
       nm1=LastAbs%362;
       indx1=LastAbs/362;
       o_b_1[Nomer][N_of_o_b_1[Nomer]].AbsPrev=LastAbs;
       LastAbs=N_of_o_b_1[Nomer]*362+Nomer;
       o_b_1[nm1][indx1].AbsNext=LastAbs;
       o_b_1[Nomer][N_of_o_b_1[Nomer]].AbsNext=-1;
  }



   o_b_1[Nomer][N_of_o_b_1[Nomer]].NomBort=atoi(Dan.NomBort);



  if(Nomer<360)
  {
    o_b_1[Nomer][N_of_o_b_1[Nomer]].Azmt=Dan.Asimut;
    if(o_b_1[Nomer][N_of_o_b_1[Nomer]].Azmt>=360)o_b_1[Nomer][N_of_o_b_1[Nomer]].Azmt-=360;
    o_b_1[Nomer][N_of_o_b_1[Nomer]].Dlnst=Dan.Dalnost;
    o_b_1[Nomer][N_of_o_b_1[Nomer]].dfSignalPK=Dan.dfSignalPK;

  }



  if(Nomer==360) //���� ��� �����
  {
    Status=0;
  }else{
    if(Dan.T_K[0]=='P')
    {
      if(Dan.T_K[1]=='S')
      {
        Status=KANAL_P_STATUS|KANAL_S_STATUS;
      }else{
        Status=KANAL_P_STATUS;
      }
    }else{
  //����?
      Status=KANAL_S_STATUS;
    }
  }

  if(Status==2||Status==3)
  {
    if(Dan.T_Bort[0]=='I') //����� RBS
    {
      Status|=REJIM_RBS_STATUS;
    }else{
      Status|=REJIM_UVD_STATUS;
    }
    if(Dan.OtnVys=='A')
    {
      Status|=ABS_VYSOTA_STATUS;
    }/*else{
//      Status|=ABS_VYSOTA_STATUS;

    }   */

  }


  o_b_1[Nomer][N_of_o_b_1[Nomer]].Status=Status;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].Status2=0;


  o_b_1[Nomer][N_of_o_b_1[Nomer]].IndxBrt=-1;
  if(Nomer==360)
  {
    N_of_o_b_1[Nomer]++;
    return 1;
  }

  long Xpr,Ypr;
//  o_b_1[Nomer][N_of_o_b_1[Nomer]].X=(long)(Dan.Dalnost*sin(Dan.Asimut/180*M_PI)*1000);
//  o_b_1[Nomer][N_of_o_b_1[Nomer]].Y=(long)(Dan.Dalnost*cos(Dan.Asimut/180*M_PI)*1000);
  Xpr=(long)(Dan.Dalnost*sin(Dan.Asimut/180*M_PI)*1000);
  Ypr=(long)(Dan.Dalnost*cos(Dan.Asimut/180*M_PI)*1000);

  if(Xpr-100<Dan.X&&Ypr-100<Dan.Y&&Xpr+100>Dan.X&&Ypr+100>Dan.Y)
  {
    o_b_1[Nomer][N_of_o_b_1[Nomer]].X=Dan.X;
    o_b_1[Nomer][N_of_o_b_1[Nomer]].Y=Dan.Y;
  }else{
    o_b_1[Nomer][N_of_o_b_1[Nomer]].X=Xpr;
    o_b_1[Nomer][N_of_o_b_1[Nomer]].Y=Ypr;
  }
  o_b_1[Nomer][N_of_o_b_1[Nomer]].NextVisual=-1;
  o_b_1[Nomer][N_of_o_b_1[Nomer]].IndxVtv=0;


  if(Nomer!=360)
  {
//��� �� ����� - ������� ������� ������
      if(LastNomerObzora<0)
      {
         o_b_1[Nomer][N_of_o_b_1[Nomer]].nomer_obzora=1;
      }else{
        o_b_1[Nomer][N_of_o_b_1[Nomer]].nomer_obzora=
                LastNomerObzora+
                tgnoGetIzmenenieObzora(TO,is180,
                LastTimeForNomerObzora,LastAzmtFromNomerObzora, LastKanal>1,

                o_b_1[Nomer][N_of_o_b_1[Nomer]].Time,
                o_b_1[Nomer][N_of_o_b_1[Nomer]].Azmt,
                (o_b_1[Nomer][N_of_o_b_1[Nomer]].Status&3)>1
                );
      }
      o_b_1[Nomer][N_of_o_b_1[Nomer]].SektorNomer=
                tgnoGetNomer90Sektora(is180,
                  o_b_1[Nomer][N_of_o_b_1[Nomer]].nomer_obzora,
                  o_b_1[Nomer][N_of_o_b_1[Nomer]].Azmt,
                  (o_b_1[Nomer][N_of_o_b_1[Nomer]].Status&3)>1);
      LastAzmtFromNomerObzora=o_b_1[Nomer][N_of_o_b_1[Nomer]].Azmt;
      LastTimeForNomerObzora=o_b_1[Nomer][N_of_o_b_1[Nomer]].Time;
      LastNomerObzora=o_b_1[Nomer][N_of_o_b_1[Nomer]].nomer_obzora;
      LastKanal=(int)(o_b_1[Nomer][N_of_o_b_1[Nomer]].Status&3);
  }

  N_of_o_b_1[Nomer]++;

//� ������ ���� ������� ��� ������� � ������ ������������� �������
  if(Nomer<360)
  {
           for(i=1;i<n_of_otis;i++)
           {
             NewNumber=otis[i].ulNumber&0x00FFFFFF;
             if(NewNumber==o_b_1[Nomer][N_of_o_b_1[Nomer]-1].NomBort)
             {
                tpAddToOtis(Nomer,N_of_o_b_1[Nomer]-1,i);
                break;
             }
           }
           if(i==n_of_otis)
           {
             NewNumber=(unsigned long)(o_b_1[Nomer][N_of_o_b_1[Nomer]-1].NomBort);
             if(o_b_1[Nomer][N_of_o_b_1[Nomer]-1].Status&REJIM_UVD_STATUS)
             {
                NewNumber|=0x01000000;
             }else
             if(o_b_1[Nomer][N_of_o_b_1[Nomer]-1].Status&REJIM_RBS_STATUS)
             {
                NewNumber|=0x02000000;
             }
             iRet=tpNewTrassa(NewNumber);
             if(iRet>0)
             {
                tpAddToOtis(Nomer,N_of_o_b_1[Nomer]-1,iRet);
             }
           }

  }
  return 1;
}


int SravnDataODIN_BORT_1( const void * A1,const void * A2)
{
  ODIN_BORT_1 *ob11=(ODIN_BORT_1*)A1;
  ODIN_BORT_1 *ob12=(ODIN_BORT_1*)A2;

  if(ob11->Time>ob12->Time)return 1;
  if(ob11->Time<ob12->Time)return (-1);
  return 0;
}







int OBZORY_1::ClearSelection(void)
{
  long Tek;
  long Indx,Nomer;
  long Indx1,Nomer1;
  long NextV,PrevV;

  Tek=FirstVisual;
  if(Tek==-1)return 0;
  while(Tek>=0)
  {
    Indx=Tek/362; Nomer=Tek%362;

//��������, �� ��������� �� ������ � ������� ���������
    if(o_b_1[Nomer][Indx].Status&V_OBLASTI_VYDELENIA_STATUS)
    {

     //���� ������� ������ �����
       o_b_1[Nomer][Indx].Status&=~V_OBLASTI_VYDELENIA_STATUS;
       o_b_1[Nomer][Indx].Status&=~VISIBLE_STATUS;
//       o_b_1[Nomer][Indx].Status&=~BLIZKO_STATUS;
       if(Tek==FirstVisual)
       {
         if(Tek==LastVisual)
         {
           o_b_1[Nomer][Indx].NextVisual=-1;
           FirstVisual=LastVisual=-1;
           break;
         }else{
           NextV=o_b_1[Nomer][Indx].NextVisual;
           o_b_1[Nomer][Indx].NextVisual=-1;
           FirstVisual=NextV;
           PrevV=-1;
           Tek=NextV;
         }
       }else{  //��� �� ������ ������� ����� ����������
         if(Tek==LastVisual)
         {
           LastVisual=PrevV;
           Nomer=LastVisual%362; Indx=LastVisual/362;
           o_b_1[Nomer][Indx].NextVisual=-1;
           break;
         }else{
           Indx1=PrevV/362;Nomer1=PrevV%362;
           NextV=o_b_1[Nomer][Indx].NextVisual;
           o_b_1[Nomer1][Indx1].NextVisual=NextV;
           o_b_1[Nomer][Indx].NextVisual=-1;
           Tek=NextV;
         }
       }
    }else{
       PrevV=Tek;
       Tek=o_b_1[Nomer][Indx].NextVisual;
    }
  };

//����������� ����� ��������
  ChisloVydelOtschetov=0;
  ChisloFilterOtschetov=0;
  ChisloVydelOtschetovP=0;
  ChisloVydelOtschetovPS=0;
  ChisloVydelOtschetovS=0;
  ChisloBlizkihOtschetovP=0;
  ChisloBlizkihOtschetovS_UVD=0;
  ChisloBlizkihOtschetovS_RBS=0;

  Tek=FirstVisual;

  while(Tek>=0)
  {
    Indx=Tek/362; Nomer=Tek%362;
    if(Nomer!=360)
    {
       ChisloFilterOtschetov++;
       if(o_b_1[Nomer][Indx].Status&BLIZKO_STATUS)
       {
         if(o_b_1[Nomer][Indx].Status&REJIM_UVD_STATUS)
         {
             ChisloBlizkihOtschetovS_UVD++;
         }else if(o_b_1[Nomer][Indx].Status&REJIM_RBS_STATUS)
         {
             ChisloBlizkihOtschetovS_RBS++;
         }else{
             ChisloBlizkihOtschetovP++;
         }
       }
    }
    Tek=o_b_1[Nomer][Indx].NextVisual;
  };
  return 1;
}

int OBZORY_1::ClearNotSelection(void)
{
  long Tek;
  long Indx,Nomer;
  long Indx1,Nomer1;
  long NextV,PrevV;

  Tek=FirstVisual;
  if(Tek==-1)return 0;
  while(Tek>=0)
  {
    Indx=Tek/362; Nomer=Tek%362;

//��������, �� ��������� �� ������ � ������� ���������
    if(!(o_b_1[Nomer][Indx].Status&V_OBLASTI_VYDELENIA_STATUS)&&Nomer<360)
    {

     //���� ������� ������ �����
       o_b_1[Nomer][Indx].Status&=~V_OBLASTI_VYDELENIA_STATUS;
       o_b_1[Nomer][Indx].Status&=~VISIBLE_STATUS;
//       o_b_1[Nomer][Indx].Status&=~BLIZKO_STATUS;
       if(Tek==FirstVisual)
       {
         if(Tek==LastVisual)
         {
           o_b_1[Nomer][Indx].NextVisual=-1;
           FirstVisual=LastVisual=-1;
           break;
         }else{
           NextV=o_b_1[Nomer][Indx].NextVisual;
           o_b_1[Nomer][Indx].NextVisual=-1;
           FirstVisual=NextV;
           PrevV=-1;
           Tek=NextV;
         }
       }else{  //��� �� ������ ������� ����� ����������
         if(Tek==LastVisual)
         {
           LastVisual=PrevV;
           Nomer=LastVisual%362; Indx=LastVisual/362;
           o_b_1[Nomer][Indx].NextVisual=-1;
           break;
         }else{
           Indx1=PrevV/362;Nomer1=PrevV%362;
           NextV=o_b_1[Nomer][Indx].NextVisual;
           o_b_1[Nomer1][Indx1].NextVisual=NextV;
           o_b_1[Nomer][Indx].NextVisual=-1;
           Tek=NextV;
         }
       }
    }else{
       PrevV=Tek;
       Tek=o_b_1[Nomer][Indx].NextVisual;
    }
  };

//������� �����.


//����������� ����� ��������
  ChisloVydelOtschetov=0;
  ChisloFilterOtschetov=0;
  ChisloVydelOtschetovP=0;
  ChisloVydelOtschetovPS=0;
  ChisloVydelOtschetovS=0;
  ChisloBlizkihOtschetovP=0;
  ChisloBlizkihOtschetovS_UVD=0;
  ChisloBlizkihOtschetovS_RBS=0;

  Tek=FirstVisual;

  while(Tek>=0)
  {
    Indx=Tek/362; Nomer=Tek%362;
    if(Nomer!=360)
    {
       ChisloFilterOtschetov++;
       if(o_b_1[Nomer][Indx].Status&BLIZKO_STATUS)
       {
         if(o_b_1[Nomer][Indx].Status&REJIM_UVD_STATUS)
         {
             ChisloBlizkihOtschetovS_UVD++;
         }else if(o_b_1[Nomer][Indx].Status&REJIM_RBS_STATUS)
         {
             ChisloBlizkihOtschetovS_RBS++;
         }else{
             ChisloBlizkihOtschetovP++;
         }
       }
    }
    Tek=o_b_1[Nomer][Indx].NextVisual;
  };
  return 1;
}









void OBZORY_1::FreeAll(void)
{
  int i;
  FreeBlizkieForVK();
  FreeBlizkieForP();

  if(o_b_1)
  {
    for(i=0;i<362;i++)
    {
      if(o_b_1[i])
      {
        free(o_b_1[i]);
      }
    }
    free(o_b_1);
    o_b_1=NULL;
  }
  if(MaxN_of_o_b_1)
  {
    free(MaxN_of_o_b_1);
    MaxN_of_o_b_1=NULL;
  }
  if(N_of_o_b_1)
  {
    free(N_of_o_b_1);
    N_of_o_b_1=NULL;
  }
  if(fps)
  {
    delete []fps;
    fps=NULL;
  }

  if(N_of_Pereskokov)
  {
    free(Pereskoki);Pereskoki=NULL;
    free(PrevPereskoki); PrevPereskoki=NULL;
    N_of_Pereskokov=0;
  }

  if(curOpenF)
  {
        fclose(curOpenF);
        curOpenF=NULL;
  }

  if(work_otis)
  {
        free(work_otis);
        work_otis=NULL;
        n_of_work_otis=0;
        max_n_of_work_otis=0;
  }
  if(sort_otis)
  {
        free(sort_otis);
        sort_otis=NULL;
        n_of_sort_otis=0;
  }


  if(otis)
  {
        for(i=0;i<n_of_otis;i++)
        {
                if(otis[i].vetv)
                {
                        free(otis[i].vetv);
                }
        }
        free(otis);
        otis=NULL;
        n_of_otis=0;
        max_n_of_otis=0;
  }

  ChisloBlizkihOtschetovP=0;
  ChisloBlizkihOtschetovS_RBS=0;
  ChisloBlizkihOtschetovS_UVD=0;
  ChisloOtschetov=0;

  LastTime=0;
  LastNomer=0;
  LastChas=0;
  LastMinuta=0;
  NomerSutok=0;
  LastNomerObzora=-1;
  N_of_fps=0;
  FirstVisual=-1;
  LastVisual=-1;
  curIndxOfOpenF=-1;
  FirstAbs=LastAbs=-1;
  nomer_for_99999_0=0;
  FreeBlizkieForP();
  FreeBlizkieForVK();


}









int OBZORY_1::MakeFilter(void)
{
  long i,indx,nomer,j;
  long i1,i2,i3;
  long indxBrt, indxVtv;
  int isVisible;      

  ChisloFilterOtschetov=0;

  LastVisual=FirstVisual=-1;

  if(FirstAbs<0)
  {
        return 0;
  }

  if(FindRadialTraekt)
  {
        for(i=1;i<n_of_otis;i++)
        {
                if(otis[i].ulNumber>0x02FFFFFF)
                {
                        continue;
                }
                for(j=0;j<otis[i].n_of_vetv;j++)
                {
                        otis[i].vetv[j].min_dlnst_vis=100000.0;
                        otis[i].vetv[j].max_dlnst_vis=-100000.0;
                        otis[i].vetv[j].isView=0;
                }

        }
  }


  i=FirstAbs;

  //������������

    while(i>=0)
    {
        nomer=i%362;
        indx=i/362;
      //������� ������ ��� �� ����������
        o_b_1[nomer][indx].Status&=~V_OBLASTI_VYDELENIA_STATUS;
        ProveritForVisibleOdinOtschet(nomer,indx);
        if(FindRadialTraekt)
        {
                if(o_b_1[nomer][indx].Status&VISIBLE_STATUS)
                {
                        indxBrt=o_b_1[nomer][indx].IndxBrt;
                        if(indxBrt<=0||indxBrt>=n_of_otis)
                        {
                                int debug=1;
                        }
                        if(indxBrt<=0||
                           o_b_1[nomer][indx].IndxVtv<=0||
                           otis[o_b_1[nomer][indx].IndxBrt].ulNumber>0x02FFFFFF)
                        {
                          o_b_1[nomer][indx].Status&=~VISIBLE_STATUS;
                        }else{
                          if((FindForP_PS&&(o_b_1[nomer][indx].Status&KANAL_P_STATUS))||
                             (!FindForP_PS&&(o_b_1[nomer][indx].Status&KANAL_S_STATUS)))
                          {
                             indxVtv=(o_b_1[nomer][indx].IndxVtv&0xFFF)-1;
                             if(indxVtv>=0)
                             {
                                if(o_b_1[nomer][indx].Dlnst>
                                  otis[indxBrt].vetv[indxVtv].max_dlnst_vis)
                                {
                                  otis[indxBrt].vetv[indxVtv].max_dlnst_vis=
                                      o_b_1[nomer][indx].Dlnst;
                                }
                                if(o_b_1[nomer][indx].Dlnst<
                                   otis[indxBrt].vetv[indxVtv].min_dlnst_vis)
                                {
                                   otis[indxBrt].vetv[indxVtv].min_dlnst_vis=
                                               o_b_1[nomer][indx].Dlnst;
                                }
                             }

                             indxVtv=((o_b_1[nomer][indx].IndxVtv&0xFFF000)>>12)-1;
                             if(indxVtv>=0)
                             {
                                if(o_b_1[nomer][indx].Dlnst>
                                   otis[indxBrt].vetv[indxVtv].max_dlnst_vis)
                                {
                                   otis[indxBrt].vetv[indxVtv].max_dlnst_vis=
                                                o_b_1[nomer][indx].Dlnst;
                                }
                                if(o_b_1[nomer][indx].Dlnst<
                                   otis[indxBrt].vetv[indxVtv].min_dlnst_vis)
                                {
                                   otis[indxBrt].vetv[indxVtv].min_dlnst_vis=
                                               o_b_1[nomer][indx].Dlnst;
                                }

                             }
                           }
                        }
                }
        }

        i=o_b_1[nomer][indx].AbsNext;
    }

    if(FindRadialTraekt)
    {
       for(i=1;i<n_of_otis;i++)
       {

          for(j=0;j<otis[i].n_of_vetv;j++)
          {
             if(otis[i].vetv[j].min_dlnst_vis<=MaxMinD&&
                otis[i].vetv[j].max_dlnst_vis>=MinMaxD)
             {
                otis[i].vetv[j].isView=1;
             }else{
                otis[i].vetv[j].isView=0;
             }
          }
       }
       i=GetFirstFilterOB(false);
       while(i>=0)
       {
           if(i%362==360)
           {
                i=GetNextFilterOB(i,false);
                continue;
           }

           if(o_b_1[i%362][i/362].IndxBrt<=0)
           {
               o_b_1[i%362][i/362].Status&=~VISIBLE_STATUS;
               i=GetNextFilterOB(i,false);
               continue;
           }
           indxBrt=o_b_1[i%362][i/362].IndxBrt;
           indxVtv=(o_b_1[i%362][i/362].IndxVtv&0x000FFF)-1;
           isVisible=0;
           if(indxVtv>=0)
           {
                if(otis[indxBrt].vetv[indxVtv].isView)
                {
                   isVisible=1;
                }
           }
           if(!isVisible)
           {
               indxVtv=((o_b_1[i%362][i/362].IndxVtv&0xFFF000)>>12)-1;
               if(indxVtv>=0)
               {
                   if(otis[indxBrt].vetv[indxVtv].isView)
                   {
                      isVisible=1;
                   }
               }
           }
           if(!isVisible)
           {
               o_b_1[i%362][i/362].Status&=~VISIBLE_STATUS;
           }
           i=GetNextFilterOB(i,false);
       }
    }


    i1=GetFirstFilterOB(false);
    if(i1>=0)
    {
        i2=GetNextFilterOB(i1, false);
        if(i2>=0)
        {
            i3=GetNextFilterOB(i2,false);
            while(i3>=0)
            {
                if(((i1%362)==360)&&((i2%362)==360)&&((i3%362)==360))
                {
                    o_b_1[i2%362][i2/362].Status&=~VISIBLE_STATUS;
                }
                i1=i2;
                i2=i3;
                i3=GetNextFilterOB(i3,false);
            }
        }
    }

  return 1;
}


//������� ������ �� �����
int OBZORY_1::VyvodNaEkran(TImage *Image,
                   double LeftKm, double BottomKm,
                   double RightKm, double TopKm,
                   TCGauge *CGauge)
{
  int i;
  long TekIndx;          //������� �������

  long *IndxBlizkie=NULL;
  int N_of_IndxBlizkie=0;
  long *PervNomBortIndx;   //������ ������� ��������� ����� �� ������ -
                      //��� ������ ������� ������ � �������
  long N_of_IndxPervNomBortIndx=0; //��� ����������� ������� ���������

  long LeftM=LeftKm*1000,
       BottomM=BottomKm*1000,
       RightM=RightKm*1000,
       TopM=TopKm*1000;



  long Nomer,Index;  //����� � ������ �������� �������

  long XVis,YVis;
  double KoefX,KoefY;

  long IndxBrt;  //������ �����
  unsigned long NomBort; //����� �����
  double Xf,Yf;
  AnsiString TextNomBort;
  int Dlina;
  int Ch,Ch1,Mn,Sk;  //
  char TimeOut[10];
  long CGaugeOtch=0;

  if(ChisloOtschetov==0)return 0;
  CGauge->MinValue=0;
  CGauge->MaxValue=ChisloFilterOtschetov;

  PervNomBortIndx=(long*)malloc(n_of_otis*sizeof(long));

//������� ������� -1 ��� �������� ������� PervNomBortIndx
  for(i=0;i<n_of_otis;i++)
  {
    PervNomBortIndx[i]=-1;
  }

  KoefX=(Image->Width-1)/((double)(RightKm-LeftKm));
  KoefY=(Image->Height-1)/((double)(BottomKm-TopKm));


  TekIndx=GetFirstFilterOB(false);

//  Image->Picture->Bitmap->Canvas->Pen->Mode=pmXor;
  Image->Picture->Bitmap->Canvas->Pen->Mode=pmCopy;
  Image->Picture->Bitmap->Canvas->Pen->Style=psSolid;
  Image->Picture->Bitmap->Canvas->Brush->Color=Text_Col;
  Image->Picture->Bitmap->Canvas->Brush->Style=bsSolid;
  while(TekIndx>=0)
  {
/*
    if(Thread->Terminated) //���� �����
    {
      if(IndxBlizkie)free(IndxBlizkie);
      if(PervNomBortIndx)free(PervNomBortIndx);
      if(IndxPervNomBortIndx)free(IndxPervNomBortIndx);
      return -2;
    }
*/
    CGaugeOtch++;
    if(CGaugeOtch%1000==0)
    {
      CGauge->Progress=CGaugeOtch;
    }

    Nomer=TekIndx%362;
    if(Nomer==360)
    {
        TekIndx=GetNextFilterOB(TekIndx,false);
        continue;
    }

    Index=TekIndx/362;
    IndxBrt=o_b_1[Nomer][Index].IndxBrt;
    if(IndxBrt<0||IndxBrt>=n_of_otis)
    {
       TekIndx=GetNextFilterOB(TekIndx,false);
       continue;
    }




//��������, ����� �� ������� ��� �����
    if(o_b_1[Nomer][Index].X<RightM&&o_b_1[Nomer][Index].X>LeftM&&
       o_b_1[Nomer][Index].Y<TopM&&o_b_1[Nomer][Index].Y>BottomM)
    {
//������� �����
      Xf=o_b_1[Nomer][Index].X/1000.0;
      Yf=o_b_1[Nomer][Index].Y/1000.0;
      XVis=Okrugl((Xf-LeftKm)*KoefX);
      YVis=Okrugl((Yf-TopKm)*KoefY);

//�������� �������� �� ���� ������ ������� � ����������� ������ ��������
      if(o_b_1[Nomer][Index].Status&BLIZKO_STATUS)
      {
        IndxBlizkie=(long*)realloc(IndxBlizkie,sizeof(long)*(N_of_IndxBlizkie+1));
        IndxBlizkie[N_of_IndxBlizkie]=Index*362+Nomer;
        N_of_IndxBlizkie++;
      }

//��������� ����
      if(o_b_1[Nomer][Index].Status&KANAL_P_STATUS)
      {
        if(o_b_1[Nomer][Index].Status&KANAL_S_STATUS)
        {
//������������ ����� �������� ��� �������� ����
            Image->Picture->Bitmap->Canvas->Pen->Color=PS_Col;
            Image->Picture->Bitmap->Canvas->Brush->Color=PS_Col;
        }else{
 //���������� ��� ����� ����
            Image->Picture->Bitmap->Canvas->Pen->Color=P_Col;
            Image->Picture->Bitmap->Canvas->Brush->Color=P_Col;
        }
      }else{
        if(o_b_1[Nomer][Index].Status&KANAL_S_STATUS)
        {
 //���������� ������� ������
            Image->Picture->Bitmap->Canvas->Pen->Color=S_Col;
            Image->Picture->Bitmap->Canvas->Brush->Color=S_Col;
        }else{

           TekIndx=GetNextFilterOB(TekIndx,false);
           continue;
        }
      }

      if(DiametrMetok==1)
      {
         Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
         Image->Picture->Bitmap->Canvas->Pen->Color;
      }else if(DiametrMetok==2)
      {
          Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
          Image->Picture->Bitmap->Canvas->Pen->Color;
          Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
          Image->Picture->Bitmap->Canvas->Pen->Color;
          Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
          Image->Picture->Bitmap->Canvas->Pen->Color;
          Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
          Image->Picture->Bitmap->Canvas->Pen->Color;
      }else{
          Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
               XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
      }




      if(IsVisNumber)
      {
         if(PervNomBortIndx[IndxBrt]==-1)
         {
//����� ������� ���� �����
           PervNomBortIndx[IndxBrt]=Index*362+Nomer;
         }
      }
    }
    TekIndx=GetNextFilterOB(TekIndx,false);
  }


//������ ������� ������� ����������� � ���������������� ��������
//������� ������� ���������� ������ ������� ��������� � ���������������� �������� ����
//�� �����
  if(IsWasSearchingRS_P)
  {
    Image->Picture->Bitmap->Canvas->Pen->Color=RS_P_Col;
    Image->Picture->Bitmap->Canvas->Brush->Color=RS_P_Col;

    for(i=0;i<N_of_POOB1_p;i++)
    {
       if((POOB1_p[i].OB1_1->Status&VISIBLE_STATUS)&&
          (POOB1_p[i].OB1_2->Status&VISIBLE_STATUS))
       {
//������� ��� �������
          Xf=POOB1_p[i].OB1_1->X/1000.0;
          Yf=POOB1_p[i].OB1_1->Y/1000.0;
          XVis=Okrugl((Xf-LeftKm)*KoefX);
          YVis=Okrugl((Yf-TopKm)*KoefY);
          if(DiametrMetok==1)
          {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
          }else if(DiametrMetok==2)
          {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
         }else{
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
                  XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
         }
          Xf=POOB1_p[i].OB1_2->X/1000.0;
          Yf=POOB1_p[i].OB1_2->Y/1000.0;
          XVis=Okrugl((Xf-LeftKm)*KoefX);
          YVis=Okrugl((Yf-TopKm)*KoefY);
          if(DiametrMetok==1)
          {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
          }else if(DiametrMetok==2)
          {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
         }else{
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
                  XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
         }

       }
    }
  }

//������� ������� ���������� ������, ����������� � ���������������� �������� ��� ���
  if(IsWasSearchingRS_VK)
  {
    Image->Picture->Bitmap->Canvas->Pen->Color=RS_UVD_Col;
    Image->Picture->Bitmap->Canvas->Brush->Color=RS_UVD_Col;
    for(i=0;i<N_of_POOB1_uvd;i++)
    {
       if((POOB1_uvd[i].OB1_1->Status&VISIBLE_STATUS)&&
          (POOB1_uvd[i].OB1_2->Status&VISIBLE_STATUS))
       {
//������� ��� �������
          Xf=POOB1_uvd[i].OB1_1->X/1000.0;
          Yf=POOB1_uvd[i].OB1_1->Y/1000.0;
          XVis=Okrugl((Xf-LeftKm)*KoefX);
          YVis=Okrugl((Yf-TopKm)*KoefY);
          if(DiametrMetok==1)
          {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
          }else if(DiametrMetok==2)
          {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
          }else{
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
                  XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
          }
          Xf=POOB1_uvd[i].OB1_2->X/1000.0;
          Yf=POOB1_uvd[i].OB1_2->Y/1000.0;
          XVis=Okrugl((Xf-LeftKm)*KoefX);
          YVis=Okrugl((Yf-TopKm)*KoefY);
          if(DiametrMetok==1)
          {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
          }else if(DiametrMetok==2)
          {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
         }else{
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
                  XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
         }
       }
    }
    Image->Picture->Bitmap->Canvas->Pen->Color=RS_RBS_Col;
    Image->Picture->Bitmap->Canvas->Brush->Color=RS_RBS_Col;

    for(i=0;i<N_of_POOB1_rbs;i++)
    {
       if((POOB1_rbs[i].OB1_1->Status&VISIBLE_STATUS)&&
          (POOB1_rbs[i].OB1_2->Status&VISIBLE_STATUS))
       {
//������� ��� �������
          Xf=POOB1_rbs[i].OB1_1->X/1000.0;
          Yf=POOB1_rbs[i].OB1_1->Y/1000.0;
          XVis=Okrugl((Xf-LeftKm)*KoefX);
          YVis=Okrugl((Yf-TopKm)*KoefY);
          if(DiametrMetok==1)
          {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
          }else if(DiametrMetok==2)
          {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
          }else{
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
                  XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
          }
          Xf=POOB1_rbs[i].OB1_2->X/1000.0;
          Yf=POOB1_rbs[i].OB1_2->Y/1000.0;
          XVis=Okrugl((Xf-LeftKm)*KoefX);
          YVis=Okrugl((Yf-TopKm)*KoefY);
          if(DiametrMetok==1)
          {
            Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
            Image->Picture->Bitmap->Canvas->Pen->Color;
          }else if(DiametrMetok==2)
          {
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
             Image->Picture->Bitmap->Canvas->Pixels[XVis+1][YVis+1]=
             Image->Picture->Bitmap->Canvas->Pen->Color;
         }else{
             Image->Picture->Bitmap->Canvas->Ellipse(XVis-DiametrMetok/2,YVis-DiametrMetok/2,
                  XVis-DiametrMetok/2+DiametrMetok,YVis-DiametrMetok/2+DiametrMetok);
         }
       }
    }
  }




//������ �� ������� �������
  if(IsVisNumber)  //������������ �� ����� �����
  {
    Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
    Image->Picture->Bitmap->Canvas->Font->Color=Text_Col;
    Image->Picture->Bitmap->Canvas->Font->Name="MS Sans Serif";
    Image->Picture->Bitmap->Canvas->Font->Size=7;

    for(i=0;i<n_of_otis;i++)
    {
  /*
      if(Thread->Terminated) //���� �����
      {
        if(IndxBlizkie)free(IndxBlizkie);
        if(PervNomBortIndx)free(PervNomBortIndx);
        if(IndxPervNomBortIndx)free(IndxPervNomBortIndx);
        return -2;
      }
   */
      if(PervNomBortIndx[i]<0)continue;
      Nomer=PervNomBortIndx[i]%362;
      Index=PervNomBortIndx[i]/362;
      Xf=Okrugl(o_b_1[Nomer][Index].X/1000.0);
      Yf=Okrugl(o_b_1[Nomer][Index].Y/1000.0);
      XVis=Okrugl((Xf-LeftKm)*KoefX);
      YVis=Okrugl((Yf-TopKm)*KoefY);
//���������� ������ � �������
      NomBort=otis[i].ulNumber&0x00FFFFFF;
      if((otis[i].ulNumber&0xFF000000)==0x02000000)
      {
          if(NomBort==0)
          {
            TextNomBort=String((int)NomBort)+String("(r)");
          }else{
            TextNomBort=String((int)NomBort);
          }
      }else
      if((otis[i].ulNumber&0xFF000000)==0x01000000)
      {
          if(NomBort<10000)
          {
            TextNomBort=String((int)NomBort)+String("(u)");
          }else{
            TextNomBort=String((int)NomBort);
          }
      }else
      if((otis[i].ulNumber&0xFF000000)==0)
      {
          TextNomBort=String("PC �")+String((int)otis[i].trassa_indx);
      }else
      if((otis[i].ulNumber&0xFF000000)==0x12000000)
      {
        TextNomBort=String((int)NomBort)+String("(RM_RBS)");
      }else
      if((otis[i].ulNumber&0xFF000000)==0x11000000)
      {
        TextNomBort=String((int)NomBort)+String("(RM_UVD)");
      }
      Dlina=Image->Picture->Bitmap->Canvas->TextWidth(TextNomBort);
      if(XVis+Dlina>Image->Width)
      {
        XVis=Image->Width-Dlina;
      }
      if(otis[i].ulNumber<0x14000000)
      {
 //������� ������
          Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis,TextNomBort);

 //� ������ ������� �����, ���� ����������
          if(IsVisTime)
          {
             Ch1=(int)(o_b_1[Nomer][Index].Time);
             Ch=Ch1%24;
             Mn=(int)((o_b_1[Nomer][Index].Time-Ch1)*60);
             Sk=(int)((60*(o_b_1[Nomer][Index].Time-Ch1)-Mn)*60);
             sprintf(TimeOut,"%02d:%02d:%02d",Ch,Mn,Sk);
             Image->Picture->Bitmap->Canvas->TextOut(XVis,YVis+20,TimeOut);
          }
      }
    }
  }



  if(IndxBlizkie)free(IndxBlizkie);
  if(PervNomBortIndx)free(PervNomBortIndx);
  return 1;

}



int OBZORY_1::LoadFiles(AnsiString *file_names, int N_of_fn,double TempObzoraVSekundah)
{

  int i;
  int Ret;
  FILE *fp;
  DANN Dan;
  double Time;
  short NFile;  //����� ����� ��� ������������� ��� ������ OCHERED_FOR_OBZORY_1
  if(N_of_fn==0)return 0;
  long PromSizeOfPrevFiles=0;
  if(curOpenF)
  {
        fclose(curOpenF);
        curOpenF=NULL;
  }




  ChisloOtschetov=0;
  ChisloFilterOtschetov=0;
  ChisloVydelOtschetov=0;
  ChisloVydelOtschetovP=0;
  ChisloVydelOtschetovS=0;
  ChisloVydelOtschetovPS=0;
  ChisloBlizkihOtschetovP=0;
  ChisloBlizkihOtschetovS_UVD=0;
  ChisloBlizkihOtschetovS_RBS=0;
  last_poryadk_nomer=0;
  LastNomerObzora=-1;

  CLSutkiFirst=0; CLChasyFirst=0; CLMinutyFirst=0; CLSekundyFirst=0;
  CLSutkiCur=0; CLChasyCur=0; CLMinutyCur=0; CLSekundyCur=0;

//������� ��������� ����� ������
  NewFiles(file_names,N_of_fn,TempObzoraVSekundah);

  //��������, � �� ���������� �� ��� ���������� Analisis �����
  isMakingByAnalisisFiles=0;
  for(i=0;i<N_of_fps;i++)
  {
        char temp_test_strka[22];
        fp=MyFOpenR(fps[i]);
        fseek(fp,0,0);
  //���� ���������� ����� ����� ***ANALISIS MAKING***
        if(fread(temp_test_strka,21, 1,fp))
        {
                temp_test_strka[21]=0;
                if(!strcmp("***ANALISIS MAKING***",temp_test_strka))
                {
                        isMakingByAnalisisFiles=1;
                }
        }
        fclose(fp);
        if(isMakingByAnalisisFiles)break;
  }

//�������� ����� �������� ������ - ��� ������ ������������� �������
  tpNewTrassa(0xFFFFFFFF);




  for(i=0;i<N_of_fps;i++)
  {
//������� �� ������ �����
      fp=MyFOpenR(fps[i]);
      fseek(fp,0,0);
      while(!feof(fp))
      {
        Ret=LoadDan(fp,&Dan);
        if(Ret<1)continue;
        Add(i,Dan);
      };
      GlobalGaugeCurTraekt++;
      Application->ProcessMessages();
      fclose(fp);
  }

//������ ���������� ����������� �������� � ������������ ����� ����������������� ������
  int NaidLi=0;
  for(i=0;i<361;i++)
  {
    if(MaxN_of_o_b_1[i]>N_of_o_b_1[i])
    {
      o_b_1[i]=(ODIN_BORT_1*)realloc(o_b_1[i],N_of_o_b_1[i]*sizeof(ODIN_BORT_1));
      MaxN_of_o_b_1[i]=N_of_o_b_1[i];
    }
    if(N_of_o_b_1[i]>0)NaidLi=1;

    if(i==360)
    {
      if(NaidLi==0&&N_of_o_b_1[i]>0)
      {
        NaidLi=2;
      }
    }
  }


  if(!NaidLi)return (-1); //�� ������� ������

  if(NaidLi==2)return (-2);


//������ ��� ������ ������� ��� P, S � PS
  FindVseParnyeOtschetyNomBort();

  ChisloOtschetov=last_poryadk_nomer;

  return 1;
}



int OBZORY_1::LoadData(AnsiString *file_names,
                                int N_of_fn,
                                double TempObzoraVSekundah)
{
        int iRet;

        GlobalGaugeCurTraekt=0;
        GlobalGaugeMaxTraekt=N_of_fn+500;

        iRet=LoadFiles(file_names,N_of_fn, TempObzoraVSekundah);
        if(iRet<=0)
        {
                return (-10000+iRet);
        }
        return 1;
}




/**/
int OBZORY_1::TempSaveAllDataAndFreeMemory(FILE *fp)
{
  long i;
  extern TCGauge *GlobalCGauge1;

  for(i=0;i<=360;i++)
  {
    if(i%15==0)
    {
      GlobalCGauge1->Progress=GlobalCGauge1->Progress+1;
    }

    if(N_of_o_b_1[i]>0)
    {
      fwrite(o_b_1[i], sizeof(ODIN_BORT_1),N_of_o_b_1[i],fp);
      free(o_b_1[i]);
    }

  }
  return 1;
}

int OBZORY_1::TempRestoreAllData(FILE *fp)
{
  long i;
  for(i=0;i<=360;i++)
  {
    if(N_of_o_b_1[i]>0)
    {
      o_b_1[i]=(ODIN_BORT_1*)malloc(sizeof(ODIN_BORT_1)*N_of_o_b_1[i]);
      fread(o_b_1[i], sizeof(ODIN_BORT_1),N_of_o_b_1[i],fp);
    }
  }
  return 1;
}





//�������
int OBZORY_1::ProveritForVisibleOdinOtschet(long NomerAzmt,
                                    long Indx)
{
  int IndxBrt=-1;
  int i;
  bool RetB;
  long Indx1,Nomer1;
  unsigned long NomBort;

  if(NomerAzmt<0||NomerAzmt>360)return 0;
  if(Indx<0||Indx>=N_of_o_b_1[NomerAzmt])return 0;

//� ������ ��������...
//1 �� ������, ���� ����������
  if(NomerAzmt==360)
  {
    if(IsCheckTime)
    {
       if(o_b_1[NomerAzmt][Indx].Time<Time1||o_b_1[NomerAzmt][Indx].Time>Time2)
       {
         o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
         return 1;
       }

    }

    o_b_1[NomerAzmt][Indx].Status|=VISIBLE_STATUS;

    if(LastVisual<0)
    {
      FirstVisual=Indx*362+NomerAzmt;
      LastVisual=FirstVisual;
      o_b_1[NomerAzmt][Indx].NextVisual=-1;
    }else{
      Nomer1=LastVisual%362;
      Indx1=LastVisual/362;
      LastVisual=Indx*362+NomerAzmt;
      o_b_1[Nomer1][Indx1].NextVisual=LastVisual;
      o_b_1[NomerAzmt][Indx].NextVisual=-1;
    }
    return 1;
  }

  if(!KanalP&&!KanalS&&!KanalPS)
  {
    o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
    return 1;
  }

  if(!KanalPS)
  {
     if(o_b_1[NomerAzmt][Indx].Status&KANAL_P_STATUS&&
          o_b_1[NomerAzmt][Indx].Status&KANAL_S_STATUS)
     {
       o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
        return 1;
     }
  }

  if(!KanalP)
  {
     if(o_b_1[NomerAzmt][Indx].Status&KANAL_P_STATUS&&
      !(o_b_1[NomerAzmt][Indx].Status&KANAL_S_STATUS))
    {
       o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
       return 1;
    }
  }
  if(!KanalS)
  {
     if((o_b_1[NomerAzmt][Indx].Status&3)==2)
     {
       o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
       return 1;
     }
  }

  if(!RejimUVD&&o_b_1[NomerAzmt][Indx].Status&REJIM_UVD_STATUS)
  {
       o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
        return 1;
  }
  if(!RejimRBS&&o_b_1[NomerAzmt][Indx].Status&REJIM_RBS_STATUS)
  {
       o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
        return 1;
  }


  if(IsCheckNumber)
  {
//������ ������ �����, ������� ���������� ���������

    IndxBrt=o_b_1[NomerAzmt][Indx].IndxBrt;

    if(!otis[IndxBrt].isSelected)
    {
      o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
      return 1;
    }
  }

//������ �������� �����
  if(IsCheckTime)
  {
    if(o_b_1[NomerAzmt][Indx].Time<Time1||o_b_1[NomerAzmt][Indx].Time>Time2)
    {
      o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
      return 1;
    }
  }

  if(IsCheckVys)
  {
    if(IskluchVys)
    {
       if(o_b_1[NomerAzmt][Indx].IstVysota>=Vys1&&
          o_b_1[NomerAzmt][Indx].IstVysota<=Vys2)
       {
         o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
         return 1;
       }
    }else{
        if(o_b_1[NomerAzmt][Indx].IstVysota<Vys1||
          o_b_1[NomerAzmt][Indx].IstVysota>Vys2)
       {
         o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
         return 1;
       }
    }
  }

//�������� ��������
  if(IsCheckAzmt)
  {
    if(Azmt11>=-0.002&&Azmt12>=-0.002)
    {
       if(Azmt21>=-0.002&&Azmt22>=-0.002)
       {
//���� ��������� � �� Azmt21 � �� Azmt22
         RetB=PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt11,Azmt12)||
              PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt21,Azmt22);
         if(!RetB)
         {
           o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
           return 1;
         }
       }else{
           RetB=PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt11,Azmt12);
         if(!RetB)
         {
           o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
           return 1;
         }
       }
    }else{
         if(Azmt21>=-0.002&&Azmt22>=-0.002)
       {
//���� ��������� � �� Azmt21 � �� Azmt22
         RetB=PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt21,Azmt22);
         if(!RetB)
         {
           o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
           return 1;
         }
       }
    }
  }

//�������� ���������
  if(IsCheckDlnst)
  {
     if(o_b_1[NomerAzmt][Indx].Dlnst>Dlnst2||
        o_b_1[NomerAzmt][Indx].Dlnst<Dlnst1)
     {
        o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
        return 1;
     }
  }

//�������� �����������
//� ����� �� ������ ��������� �� ����������� ������������ ���
  if(IsWasPrivyazka)
  {
    if(Napravlenie==1&&(o_b_1[NomerAzmt][Indx].Status2&3)!=0&&
       (o_b_1[NomerAzmt][Indx].Status2&3)!=1)
    {
      o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
      return 1;
    }
    if(Napravlenie==2&&(o_b_1[NomerAzmt][Indx].Status2&3)!=0&&
       (o_b_1[NomerAzmt][Indx].Status2&3)!=2)
    {
      o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
      return 1;
    }

  }

//��������� �������� �������
  if(IsCheckSignalPK)
  {
        if((o_b_1[NomerAzmt][Indx].Status&KANAL_P_STATUS)&&
           !(o_b_1[NomerAzmt][Indx].Status&KANAL_S_STATUS))
        {
            if(o_b_1[NomerAzmt][Indx].dfSignalPK<dfMinSignalPK)
            {
                  o_b_1[NomerAzmt][Indx].Status&=~(VISIBLE_STATUS);
                  return 1;
            }
        }
  }



  o_b_1[NomerAzmt][Indx].Status|=VISIBLE_STATUS;
//  o_b_1[NomerAzmt][Indx].Status&=~BLIZKO_STATUS;
  ChisloFilterOtschetov++;  //�������� ����� ������������� ��������

  if(LastVisual<0)
  {
    FirstVisual=Indx*362+NomerAzmt;
    LastVisual=FirstVisual;
    o_b_1[NomerAzmt][Indx].NextVisual=-1;
  }else{
    Nomer1=LastVisual%362;
    Indx1=LastVisual/362;
    LastVisual=Indx*362+NomerAzmt;
    o_b_1[Nomer1][Indx1].NextVisual=LastVisual;
    o_b_1[NomerAzmt][Indx].NextVisual=-1;
  }
  return 1;
}

/*�������� ������*/
int OBZORY_1::ProveritNastroikiOdinOtschet(long NomerAzmt,
                                    long Indx)
{
  int IndxBrt=-1;

  int i;
  bool RetB;
  unsigned long NomBort;

  if(NomerAzmt<0||NomerAzmt>360)return 0;
  if(Indx<0||Indx>=N_of_o_b_1[NomerAzmt])return 0;

//� ������ ��������...
//1 �� ������, ���� ����������
  if(NomerAzmt==360)
  {
    if(IsCheckTime)
    {
       if(o_b_1[NomerAzmt][Indx].Time<Time1||o_b_1[NomerAzmt][Indx].Time>Time2)
       {
         return 0;
       }

    }
    return 1;
  }

  if(!KanalP&&!KanalS&&!KanalPS)
  {
    return 0;
  }

  if(!KanalPS&&
    o_b_1[NomerAzmt][Indx].Status&KANAL_P_STATUS&&
    o_b_1[NomerAzmt][Indx].Status&KANAL_S_STATUS)
  {
     return 0;
  }

  if(!KanalP&&
      o_b_1[NomerAzmt][Indx].Status&KANAL_P_STATUS&&
      !(o_b_1[NomerAzmt][Indx].Status&KANAL_S_STATUS))
  {
     return 0;
  }

  if(!KanalS&&
      !(o_b_1[NomerAzmt][Indx].Status&KANAL_P_STATUS)&&
        o_b_1[NomerAzmt][Indx].Status&KANAL_S_STATUS)
  {
     return 0;
  }


  if(!RejimUVD&&o_b_1[NomerAzmt][Indx].Status&REJIM_UVD_STATUS)
  {
     return 0;
  }
  if(!RejimRBS&&o_b_1[NomerAzmt][Indx].Status&REJIM_RBS_STATUS)
  {
     return 0;
  }



  if(IsCheckNumber)
  {
//������ ������ �����, ������� ���������� ���������
    IndxBrt=o_b_1[NomerAzmt][Indx].IndxBrt;

    if(!otis[IndxBrt].isSelected)
    {
      return 0;
    }
  }

//������ �������� �����
  if(IsCheckTime)
  {
    if(o_b_1[NomerAzmt][Indx].Time<Time1||o_b_1[NomerAzmt][Indx].Time>Time2)
    {
      return 0;
    }
  }

  if(IsCheckVys)
  {
    if(IskluchVys)
    {
       if(o_b_1[NomerAzmt][Indx].IstVysota>Vys1&&
          o_b_1[NomerAzmt][Indx].IstVysota<Vys2)
       {
         return 0;
       }
    }else{
        if(o_b_1[NomerAzmt][Indx].IstVysota<Vys1||
          o_b_1[NomerAzmt][Indx].IstVysota>Vys2)
       {
         return 0;
       }
    }
  }

//�������� ��������
  if(IsCheckAzmt)
  {
    if(Azmt11>=-0.002&&Azmt12>=-0.002)
    {
       if(Azmt21>=-0.002&&Azmt22>=-0.002)
       {
//���� ��������� � �� Azmt21 � �� Azmt22
         RetB=PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt11,Azmt12)||
              PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt21,Azmt22);
         if(!RetB)
         {
           return 0;
         }
       }else{
           RetB=PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt11,Azmt12);
         if(!RetB)
         {
           return 0;
         }
       }
    }else{
         if(Azmt21>=-0.002&&Azmt22>=-0.002)
       {
//���� ��������� � �� Azmt21 � �� Azmt22
         RetB=PromAsimutDiapazonForTraekt(o_b_1[NomerAzmt][Indx].Azmt,
                                          Azmt21,Azmt22);
         if(!RetB)
         {
           return 0;
         }
       }
    }
  }

//�������� ���������
  if(IsCheckDlnst)
  {
     if(o_b_1[NomerAzmt][Indx].Dlnst>Dlnst2||
        o_b_1[NomerAzmt][Indx].Dlnst<Dlnst1)
     {
        return 0;
     }
  }


  //��������� �������� �������
  if(IsCheckSignalPK)
  {
        if((o_b_1[NomerAzmt][Indx].Status&KANAL_P_STATUS)&&
           !(o_b_1[NomerAzmt][Indx].Status&KANAL_S_STATUS))
        {
            if(o_b_1[NomerAzmt][Indx].dfSignalPK<dfMinSignalPK)
            {
                   return 0;
            }
        }
  }

  return 1;
}




//���������� ��������� � ������� � ������
int OBZORY_1::GetTimeAndSutki(int Sutki1,int Chasy1,int Minuty1,int Sekundy1,
                              int Sutki2,int Chasy2,int Minuty2,int Sekundy2)
{
  if(Sutki1>Sutki2)return 0;  //����� ����������� �����������
  if(Sutki1==Sutki2)
  {
    if(Chasy1>Chasy2)
    {
      return 0;
    }
    if(Chasy1==Chasy2)
    {
       if(Minuty1>Minuty2)
       {
         return 0;
       }
       if(Minuty1==Minuty2)
       {
         if(Sekundy1>Sekundy2)
         {
           return 0;
         }
       }
    }
  }

  Time1=24*Sutki1+Chasy1+Minuty1/60.0+Sekundy1/3600.0-0.00001;
  Time2=24*Sutki2+Chasy2+Minuty2/60.0+Sekundy2/3600.0+0.00001;
  return 1;
}



//�������� �������� ���������� � ������� ���������
//������ ������ � ��� ��������
//������� ���������� �����, ������� ��������
int OBZORY_1::VydelennayaOblast(double *Xv, double *Yv, int N_of_v //���������� ����� � �� �����
                                                        //��������������, ��� ����� ���������
                               )
{


  long TekVisual,i;
  double Xf,Yf;
  long Indx,Nomer;
  int Ret;
  long i1,i2,i3;

  ChisloVydelOtschetov=0;
  ChisloVydelOtschetovP=0;
  ChisloVydelOtschetovS=0;
  ChisloVydelOtschetovPS=0;

//�������� ��������������� �������
  MakeNotPriamOblast();
  for(i=0;i<N_of_v;i++)
  {
    AddTochkaVNotPriamOblast(Xv[i],Yv[i]);
  }
  DodelatNotPriamOblast();
//������ ���� ��������� ��� �����
  TekVisual=GetFirstFilterOB(false);
  while(TekVisual>=0)
  {
     Indx=TekVisual/362;
     Nomer=TekVisual%362;
     if(Nomer==360)
     {
        o_b_1[Nomer][Indx].Status|=V_OBLASTI_VYDELENIA_STATUS;
     }else{
        Xf=o_b_1[Nomer][Indx].X/1000.0;
        Yf=o_b_1[Nomer][Indx].Y/1000.0;

 //������ ��������, ����������� �� � � Y ���������� �������
        Ret=IsVnutriNotPriamOblast(Xf,Yf);
        if(Ret) //����������� ���������� �������
        {
//������� ��� ������������� ���������� �������
          if(Nomer<360)
          {
            o_b_1[Nomer][Indx].Status|=V_OBLASTI_VYDELENIA_STATUS;
            ChisloVydelOtschetov++;
            if(o_b_1[Nomer][Indx].Status&(KANAL_P_STATUS|KANAL_S_STATUS))
            {
              ChisloVydelOtschetovPS++;
            }
            else if(o_b_1[Nomer][Indx].Status&KANAL_P_STATUS)
            {
              ChisloVydelOtschetovP++;
            }else if(o_b_1[Nomer][Indx].Status&KANAL_S_STATUS)
            {
              ChisloVydelOtschetovS++;
            }
          }
        }else{
//������� ��� ��������������� ���������� �������
                o_b_1[Nomer][Indx].Status&=~V_OBLASTI_VYDELENIA_STATUS;
        }
     }
     TekVisual=GetNextFilterOB(TekVisual,false);
  };
  FreeNotPriamOblast();


  i1=GetFirstFilterOB(true);
    if(i1>=0)
    {
        i2=GetNextFilterOB(i1, true);
        if(i2>=0)
        {
            i3=GetNextFilterOB(i2,true);
            while(i3>=0)
            {
                if(((i1%362)==360)&&((i2%362)==360)&&((i3%362)==360))
                {
                    o_b_1[i2%362][i2/362].Status&=~V_OBLASTI_VYDELENIA_STATUS;
                }
                i1=i2;
                i2=i3;
                i3=GetNextFilterOB(i3,true);
            }
        }
    }

  return 1;
}








/*��������, ���� �� ������ � ������ �������, ������� ����������� �����
���������� 1, ���� ��� ���� ����� 0.*/
int OBZORY_1::ProverkaIndexBortSektor(long Sektor1,
                                      long Sektor2,
                                      long IndxBrt,
                                      bool IsP, bool IsS, bool IsPS)
{


  return 0;
}





//����� ������� � ������ �������� ����������� �������
//���������� ����� ��������
int OBZORY_1::FindBlizkieOtschety(
                           double AzmtO,      //��������� ������
                           double DlnstO,     //��������� ���������
                           long NomerObzoraO, //����� ������
                           long TekIndexesO[], //������ ������� ��������� ��������
                                              //������ TekIndexesO �� ����� 360,
                                              //����� ��������� ������ ����� ���� ������
                                              //TekIndexesO=0, ����� ����� �������� ������� � 0 ��������
                           double dAzmt, double dDlnst, //������������ ������� �������� � ���������� � �������� � ��
                           bool FindKanalP,   //��������� ������ ���������� ������
                           bool FindKanalS,   //��������� ������ ���������� ������
                           bool FindKanalPS,  //��������� ��������� ������
                           bool IsS_PS_ToUVD, //��������� ������ ���, ���� ��������� �����
                           bool IsS_PS_ToRBS, //��������� ������ RBS, ���� ��������� �����
                           long OprNomBort,    //� ������������ ��������� OprNomBort 
                           short  *NomerABlizk, long *IndxABlizk,
                           int MaxN_of_A) //������ ������...
{
  long SektorNomerO,SektorNomerO1,SektorNomerO2;    //����� 90-�� �������
                                                //����� �� 1 ������ - ����� �� 1 ������
  int AzmtI;
  int N_of_A=0;
  double Azmt1,Azmt2;
  double Dlnst1,Dlnst2;
  int Azmt1I,Azmt2I;
  int i1,i2;
  long *TekIndexesO1,TekIndexesO2[360];
  bool IsWasAzmt1I=false,IsWasAzmt2I=false;

  if(AzmtO<0)return (-1);             //���� ������ ������������� - �����
  if(AzmtO>=360.01)return (-1);       //���� ������ ������ ��� ����� 360 �������� - �����
  if(DlnstO<0)return (-1);   //���� ��������� �������������
  Azmt1=AzmtO-dAzmt;         //����� ������� �������� Azmt1 � Azmt2
  if(Azmt1<0)Azmt1+=360.0;
  Azmt2=AzmtO+dAzmt;
  if(Azmt2>=360)Azmt2-=360;

//� ��� ������������� �������
  if(TekIndexesO)
  {
    TekIndexesO1=TekIndexesO;
   }else{
    TekIndexesO1=TekIndexesO2;
  }

//������ ��������� ����������
  Dlnst1=DlnstO-dDlnst; Dlnst2=DlnstO+dDlnst;
  AzmtI=(int)AzmtO;  Azmt1I=(int)Azmt1; Azmt2I=(int)Azmt2;
  if(AzmtI<90)
  {
    SektorNomerO=4*NomerObzoraO;
  }else if(AzmtI<180)
  {
    SektorNomerO=4*NomerObzoraO+1;
  }else if(AzmtI<270)
  {
    SektorNomerO=4*NomerObzoraO+2;
  }else{ //AzmtI>=270 && AzmtI<360
    SektorNomerO=4*NomerObzoraO+3;
  }
  SektorNomerO1=SektorNomerO-1;
  SektorNomerO2=SektorNomerO+1;

//�������� ��� ������� � �������� AzmtI, ������� ���� � ���� ����� �����...

  i1=AzmtI;
  i2=AzmtI+1;
  if(i2==360)i2=0;
  if(AzmtI==Azmt2I)IsWasAzmt2I=true;
  for(;;)
  {


      if(!IsWasAzmt1I)N_of_A=FindBlizkieOtschetyAzmt(Azmt1,Azmt2,Dlnst1,Dlnst2,i1,
        FindKanalP, FindKanalS, FindKanalPS, IsS_PS_ToUVD, IsS_PS_ToRBS,OprNomBort,
        TekIndexesO1, SektorNomerO1, SektorNomerO2, NomerABlizk, IndxABlizk, N_of_A,
        MaxN_of_A);
      if(i1==Azmt1I)IsWasAzmt1I=true;

      if(!IsWasAzmt2I)N_of_A=FindBlizkieOtschetyAzmt(Azmt1,Azmt2,Dlnst1,Dlnst2,i2,
       FindKanalP, FindKanalS, FindKanalPS, IsS_PS_ToUVD, IsS_PS_ToRBS,OprNomBort,
       TekIndexesO1, SektorNomerO1, SektorNomerO2, NomerABlizk, IndxABlizk, N_of_A,
       MaxN_of_A);
      if(i2==Azmt2I)IsWasAzmt2I=true;

      if(IsWasAzmt1I&&IsWasAzmt2I)break;
      i1--; if(i1<0)i1=359;
      i2++; if(i2==360)i2=0;
  }

  return N_of_A;
}

/*���������, ���������� �� FindBlizkieOtschety*/
//������� ��� ������� ������� ����� ������� A>=RasmAzmtI � A<RasmAzmtI+1

int OBZORY_1::FindBlizkieOtschetyAzmt(
                          double Azmt1,  double Azmt2,  //��������� ��������
                          double Dlnst1, double Dlnst2, //��������� ����������
                          short RasmAzmtI, //�� ��������������� �������
                          bool FindKanalP,   //��������� ������ ���������� ������
                          bool FindKanalS,   //��������� ������ ���������� ������
                          bool FindKanalPS,  //��������� ��������� ������
                          bool IsS_PS_ToUVD, //��������� ������ ���, ���� ��������� �����
                          bool IsS_PS_ToRBS, //��������� ������ RBS, ���� ��������� �����
                          long OprNomBort,    //� ������������ ��������� OprNomBort
                          long TekIndexesO[], //������ ������� ��������� ��������

                                              //������ TekIndexesO �� ����� 360,
                                              //����� ��������� ������ ����� ���� ������
                                              //TekIndexesO=0, ����� ����� �������� ������� � 0 ��������
                          long SektorNomerO1, long SektorNomerO2, //�������� �������� �������
                          short  *NomerABlizk, long *IndxABlizk, //������� �������
                          int N_of_A, //������� ��� ��������
                          int MaxN_of_A) //������������ ����� ��������
{
  int i;
  bool RetB;
  if(N_of_o_b_1[RasmAzmtI]>0)
  {
//��������, ��� ��������� ���������
    if(TekIndexesO[RasmAzmtI]<0)i=0;
    else
    if(TekIndexesO[RasmAzmtI]>=N_of_o_b_1[RasmAzmtI])i=N_of_o_b_1[RasmAzmtI]-1;
    else i=TekIndexesO[RasmAzmtI];
   while(i<N_of_o_b_1[RasmAzmtI]-1)
    {
       i++;
    }



         if(o_b_1[RasmAzmtI][i].SektorNomer<SektorNomerO1) //����� ������� ������� ��� ������
    {
       i++;

          while(i<N_of_o_b_1[RasmAzmtI]&&o_b_1[RasmAzmtI][i].SektorNomer<=SektorNomerO2)
       {
        if(o_b_1[RasmAzmtI][i].SektorNomer<SektorNomerO1){i++;continue; }

       //��������, � ������������� �� ������ ���� ��������
          RetB=TestForFindBlizkieOtschety(Azmt1,Azmt2,Dlnst1,Dlnst2,
                                          FindKanalP,FindKanalS,FindKanalPS,
                                          IsS_PS_ToUVD,IsS_PS_ToRBS,OprNomBort,RasmAzmtI,i);
          if(RetB) //����� ������ � ��������� ������� � ����� ��������� �������� �� ������ � ���� ������
          {
        //������� ������ ������
            if(N_of_A<MaxN_of_A)
            {
              NomerABlizk[N_of_A]=RasmAzmtI;IndxABlizk[N_of_A]=i; N_of_A++;
            }
        //matv_tmp    i++;continue;
          }else{
        //matv_tmp   i++;continue;
          }
       };
       TekIndexesO[RasmAzmtI]=i; //�������
    }else{
       while(i>=0 ) //matv_tmp &&o_b_1[RasmAzmtI][i].SektorNomer111>=SektorNomerO1)
       {
         //matv_tmp if(o_b_1[RasmAzmtI][i].SektorNomer111>SektorNomerO2){i--;continue; }

       //��������, � ������������� �� ������ ���� ��������
          RetB=TestForFindBlizkieOtschety(Azmt1,Azmt2,Dlnst1,Dlnst2,
                                          FindKanalP,FindKanalS,FindKanalPS,
                                          IsS_PS_ToUVD,IsS_PS_ToRBS,OprNomBort,RasmAzmtI,i);
          if(RetB) //����� ������ � ��������� ������� � ����� ��������� �������� �� ������ � ���� ������
          {
        //������� ������ ������
            if(N_of_A<MaxN_of_A)
            {
              NomerABlizk[N_of_A]=RasmAzmtI;IndxABlizk[N_of_A]=i; N_of_A++;
            }
            i--;continue;
          }else{
           i--;continue;
          }
       };
       TekIndexesO[RasmAzmtI]=i;
    }
  }
  return N_of_A;
}



//�������, ������� ���������� �� ��������� FindBlizkieOtschety
//���������� true, ���� ������ NomerA � IndxA ����������� ���� ����������
bool OBZORY_1::TestForFindBlizkieOtschety(
                          double Azmt1,  double Azmt2,  //��������� ��������
                          double Dlnst1, double Dlnst2, //��������� ����������
                          bool FindKanalP,   //��������� ������ ���������� ������
                          bool FindKanalS,   //��������� ������ ���������� ������
                          bool FindKanalPS,  //��������� ��������� ������
                          bool IsS_PS_ToUVD, //��������� ������ ���, ���� ��������� �����
                          bool IsS_PS_ToRBS, //��������� ������ RBS, ���� ��������� �����
                          long OprNomBort,    //� ������������ ��������� OprNomBort
                          short NomerA,      //����� �������
                          long IndxA)       //������
{
  //������ �������� �������� �� ������ ������ �� �������
  if(Azmt1>Azmt2) //�������� �� ��������
  {
    if(o_b_1[NomerA][IndxA].Azmt<Azmt2&&o_b_1[NomerA][IndxA].Azmt>Azmt1)
    {
      return false;
    }
  }else{
    if(o_b_1[NomerA][IndxA].Azmt>Azmt2||o_b_1[NomerA][IndxA].Azmt<Azmt1)
    {
      return false;
    }
  }

//������ �������� �� ���������
  if(o_b_1[NomerA][IndxA].Dlnst<Dlnst1||o_b_1[NomerA][IndxA].Dlnst>Dlnst2)
  {
      return false;
  }

//������ �������� ������
  if(!FindKanalP)       //��������� ����� �� ���������������
  {
//�������� �������� �� ������ ��������� �������
    if((o_b_1[NomerA][IndxA].Status&3)==1) //� ������ ���������� ������
    {
      return false;
    }
  }


  if(!FindKanalS)      //��������� ����� �� ���������������,
  {
    if((o_b_1[NomerA][IndxA].Status&3)==2) // � ������ ���������� ������
    {
      return false;
    }
  }

  if(!FindKanalPS)
  {
    if((o_b_1[NomerA][IndxA].Status&3)==3)
    {
      return false;
    }
  }

  if((o_b_1[NomerA][IndxA].Status&3)>1)
  {
      if(!IsS_PS_ToUVD&&o_b_1[NomerA][IndxA].Status&REJIM_UVD_STATUS)
      {
        return false;
      }
      if(!IsS_PS_ToRBS&&o_b_1[NomerA][IndxA].Status&REJIM_RBS_STATUS)
      {
        return false;
      }
  }
  if(OprNomBort>=0)
  {
     if(o_b_1[NomerA][IndxA].NomBort!=OprNomBort)
     {
       return false;
     }
  }

  return true; //���������� ��� ��� ���������
}

int OBZORY_1::OzenkaMinMaxAndMaxMin(void)
{
  int i;
  double MinS,MaxS,MinP,MaxP;
  long TekSmesh,NomA,IndxO,TekSmesh1,NomA1,IndxO1;
  long PrevNomA,PrevIndxO, FirstOtsch;
  bool NadoIskluchit;


  if(!FindRadialTraekt||!IsWasPrivyazka)return 0;


//������� ��������� �� ��� �������� ������� ����� ���������� �� ���
  for(i=1;i<n_of_sort_otis;i++)
  {
    if(IsCheckNumber && !(sort_otis[i]->isSelected))continue;
    if(otis[i].lFirstIndex<0||
       (otis[i].ulNumber&0xFF000000)>0x02000000)
    {
        continue;
    }   

    MinS=10000;
    MaxS=0;
    MinP=10000;
    MaxP=0;
    PrevNomA=-1;
    PrevIndxO=-1;
    FirstOtsch=    TekSmesh;
    while(TekSmesh>=0)
    {
        NomA=TekSmesh%362;       IndxO=TekSmesh/362;
        if((o_b_1[NomA][IndxO].Status2&3)==3)
        {
          o_b_1[NomA][IndxO].Status&=~(VISIBLE_STATUS);
          TekSmesh=o_b_1[NomA][IndxO].Next;
          continue;
        }

        if(o_b_1[NomA][IndxO].Status2&LETIT_NA_RLS_STATUS2)
        {
          TekSmesh=o_b_1[NomA][IndxO].Next;
          continue;
        }


//��������, � ����� ������ ����� 100 �������
        if(PrevNomA>=0&&
         (o_b_1[NomA][IndxO].Time-o_b_1[PrevNomA][PrevIndxO].Time>=100*TO||
          o_b_1[NomA][IndxO].Time-o_b_1[PrevNomA][PrevIndxO].Time<-TO/4))
        {
//��������, � ������ �� �������� ��� �����
          if(FindForP_PS)
          {
             if(MinP<=MaxMinD&&MaxP>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
          }else{
             if(MinS<=MaxMinD&&MaxS>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
          }
          if(NadoIskluchit)
          {
            TekSmesh1=FirstOtsch;
            while(TekSmesh!=TekSmesh1)
            {
              NomA1=TekSmesh1%362; IndxO1=TekSmesh1/362;
              if(o_b_1[NomA1][IndxO1].Status2&LETIT_NA_RLS_STATUS2)
              {
                  TekSmesh1=o_b_1[NomA1][IndxO1].Next;
                  continue;
              }

              o_b_1[NomA1][IndxO1].Status&=~(VISIBLE_STATUS);
              TekSmesh1=o_b_1[NomA1][IndxO1].Next;
            }
          }
          FirstOtsch=TekSmesh;
          MinS=10000;MaxS=0;MinP=10000;MaxP=0;
        }


        if(!(o_b_1[NomA][IndxO].Status&VISIBLE_STATUS)||
          !(o_b_1[NomA][IndxO].Status2&PRIVYAZAN_OTSCHET_STATUS2))
        {
          o_b_1[NomA][IndxO].Status&=~VISIBLE_STATUS;
          PrevNomA=NomA; PrevIndxO=IndxO;
          TekSmesh=o_b_1[NomA][IndxO].Next;
          continue;
        }




        if(o_b_1[NomA][IndxO].Status&KANAL_P_STATUS)
        {
            if(MinP>o_b_1[NomA][IndxO].Dlnst)
            {
              MinP=o_b_1[NomA][IndxO].Dlnst;
            }
            if(MaxP<o_b_1[NomA][IndxO].Dlnst)
            {
              MaxP=o_b_1[NomA][IndxO].Dlnst;
            }
        }
        if(o_b_1[NomA][IndxO].Status&KANAL_S_STATUS)
        {
            if(MinS>o_b_1[NomA][IndxO].Dlnst)
            {
              MinS=o_b_1[NomA][IndxO].Dlnst;
            }
            if(MaxS<o_b_1[NomA][IndxO].Dlnst)
            {
              MaxS=o_b_1[NomA][IndxO].Dlnst;
            }
        }
        PrevNomA=NomA; PrevIndxO=IndxO;
        TekSmesh=o_b_1[NomA][IndxO].Next;
    };
    if(FindForP_PS)
    {
           if(MinP<=MaxMinD&&MaxP>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
    }else{
             if(MinS<=MaxMinD&&MaxS>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
    }
    if(NadoIskluchit)
    {
          TekSmesh1=FirstOtsch;
          while(TekSmesh1>=0)
          {
              NomA1=TekSmesh1%362; IndxO1=TekSmesh1/362;
              if(o_b_1[NomA1][IndxO1].Status2&LETIT_NA_RLS_STATUS2)
              {
                  TekSmesh1=o_b_1[NomA1][IndxO1].Next;
                  continue;
              }
              o_b_1[NomA1][IndxO1].Status&=~(VISIBLE_STATUS);
              TekSmesh1=o_b_1[NomA1][IndxO1].Next;
          }
    }

//������ ����������, ������� ����� �� ��� - �� ���� �� �� �������������
    MinS=10000;
    MaxS=0;
    MinP=10000;
    MaxP=0;
    PrevNomA=-1;
    PrevIndxO=-1;
    FirstOtsch=    TekSmesh;
    while(TekSmesh>=0)
    {
        NomA=TekSmesh%362;       IndxO=TekSmesh/362;

        if(o_b_1[NomA][IndxO].Status2&LETIT_OT_RLS_STATUS2)
        {
          TekSmesh=o_b_1[NomA][IndxO].Next;
          continue;
        }


//��������, � ����� ������ ����� 100 �������
        if(PrevNomA>=0&&
         (o_b_1[NomA][IndxO].Time-o_b_1[PrevNomA][PrevIndxO].Time>=100*TO||
          o_b_1[NomA][IndxO].Time-o_b_1[PrevNomA][PrevIndxO].Time<-TO/4))
        {
//��������, � ������ �� �������� ��� �����
          if(FindForP_PS)
          {
             if(MinP<=MaxMinD&&MaxP>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
          }else{
             if(MinS<=MaxMinD&&MaxS>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
          }
          if(NadoIskluchit)
          {
            TekSmesh1=FirstOtsch;
            while(TekSmesh!=TekSmesh1)
            {
              NomA1=TekSmesh1%362; IndxO1=TekSmesh1/362;
              if(o_b_1[NomA1][IndxO1].Status2&LETIT_OT_RLS_STATUS2)
              {
                  TekSmesh1=o_b_1[NomA1][IndxO1].Next;
                  continue;
              }

              o_b_1[NomA1][IndxO1].Status&=~(VISIBLE_STATUS);
              TekSmesh1=o_b_1[NomA1][IndxO1].Next;
            }
          }
          FirstOtsch=TekSmesh;
          MinS=10000;MaxS=0;MinP=10000;MaxP=0;
        }


        if(!(o_b_1[NomA][IndxO].Status&VISIBLE_STATUS)||
          !(o_b_1[NomA][IndxO].Status2&PRIVYAZAN_OTSCHET_STATUS2))
        {
          o_b_1[NomA][IndxO].Status&=~VISIBLE_STATUS;
          PrevNomA=NomA; PrevIndxO=IndxO;
          TekSmesh=o_b_1[NomA][IndxO].Next;
          continue;
        }




        if(o_b_1[NomA][IndxO].Status&KANAL_P_STATUS)
        {
            if(MinP>o_b_1[NomA][IndxO].Dlnst)
            {
              MinP=o_b_1[NomA][IndxO].Dlnst;
            }
            if(MaxP<o_b_1[NomA][IndxO].Dlnst)
            {
              MaxP=o_b_1[NomA][IndxO].Dlnst;
            }
        }
        if(o_b_1[NomA][IndxO].Status&KANAL_S_STATUS)
        {
            if(MinS>o_b_1[NomA][IndxO].Dlnst)
            {
              MinS=o_b_1[NomA][IndxO].Dlnst;
            }
            if(MaxS<o_b_1[NomA][IndxO].Dlnst)
            {
              MaxS=o_b_1[NomA][IndxO].Dlnst;
            }
        }
        PrevNomA=NomA; PrevIndxO=IndxO;
        TekSmesh=o_b_1[NomA][IndxO].Next;
    };
    if(FindForP_PS)
    {
           if(MinP<=MaxMinD&&MaxP>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
    }else{
             if(MinS<=MaxMinD&&MaxS>=MinMaxD)NadoIskluchit=false;else NadoIskluchit=true;
    }
    if(NadoIskluchit)
    {
          TekSmesh1=FirstOtsch;
          while(TekSmesh1>=0)
          {
              NomA1=TekSmesh1%362; IndxO1=TekSmesh1/362;
              if(o_b_1[NomA1][IndxO1].Status2&LETIT_OT_RLS_STATUS2)
              {
                  TekSmesh1=o_b_1[NomA1][IndxO1].Next;
                  continue;
              }
              o_b_1[NomA1][IndxO1].Status&=~(VISIBLE_STATUS);
              TekSmesh1=o_b_1[NomA1][IndxO1].Next;
          }
    }
  }



  ChisloFilterOtschetov=0;
//�������� �� �������� � ��������� ��, ������� �� ������������
  PrevNomA=-1,PrevIndxO=-1;
  TekSmesh=FirstVisual;
   while(TekSmesh>=0)
   {

     NomA=TekSmesh%362; IndxO=TekSmesh/362;
     if(!(o_b_1[NomA][IndxO].Status&VISIBLE_STATUS))
     {
       if(TekSmesh==FirstVisual)
       {
         FirstVisual=o_b_1[NomA][IndxO].NextVisual;
         TekSmesh=o_b_1[NomA][IndxO].NextVisual;
         o_b_1[NomA][IndxO].NextVisual=-1;
         continue;
       }else if(TekSmesh==LastVisual)
       {
         o_b_1[PrevNomA][PrevIndxO].NextVisual=o_b_1[NomA][IndxO].NextVisual=-1;
         LastVisual=PrevNomA+PrevIndxO*362;

         break;
       }else{
         o_b_1[PrevNomA][PrevIndxO].NextVisual=o_b_1[NomA][IndxO].NextVisual;
         TekSmesh=o_b_1[NomA][IndxO].NextVisual;
         o_b_1[NomA][IndxO].NextVisual=-1;
         continue;
       }
     }else{
         ChisloFilterOtschetov++;
         TekSmesh=o_b_1[NomA][IndxO].NextVisual;
         PrevNomA=NomA; PrevIndxO=IndxO;
         continue;
     }
   }
   return 1;
}


//����� ������ �������� ��� �������
//����� �������� ����������� ��������
int OBZORY_1::FindVseParnyeOtschetyNomBort(void)  //������ �����
{
//�������... ���� ���� ������ � ����� ������ PS, �� �������� ����� ������ ���� S
  long Indexes[362];
  long i,k;
  long TekSektor;
  int Ret;
  long indx1,nomer1, indx2, nomer2, indx3, nomer3;
  long naidS;
  double azmt,dlnst,azmt_a;
  double d_azmt,d_azmt_a;
  ChisloPSOtschetov=0;
  ChisloPSOtschetov_Privyaz=0;
  ChisloPOtschetov=0;
  ChisloSOtschetov=0;

  i=FirstAbs;
  while(i>=0)
  {
       indx1=i/362;
       nomer1=i%362;
       if(indx1==1&&nomer1==0)
       {

                int h1, m1;

                double s1, tmp;
                h1=(int)(o_b_1[nomer1][indx1].Time);
                tmp=(o_b_1[nomer1][indx1].Time-h1)*60.0;
                m1=(int)(tmp);
                s1=(tmp-m1)*60.0;
                int debug=1;
       }

       if((o_b_1[nomer1][indx1].Status&KANAL_P_STATUS)&&
          (o_b_1[nomer1][indx1].Status&KANAL_S_STATUS))
       {
           naidS=-1;

//���� �����  ������� S
           k=o_b_1[nomer1][indx1].AbsPrev;
           while(k>=0)
           {
               indx2=k/362;
               nomer2=k%362;
               if(ABS(o_b_1[nomer1][indx1].Time-o_b_1[nomer2][indx2].Time)>TO/5)
               {
                        break;
               }

               d_azmt=ABS(o_b_1[nomer2][indx2].Azmt-o_b_1[nomer1][indx1].Azmt);
               if(d_azmt>180)d_azmt=ABS(360-d_azmt);
               if((o_b_1[nomer2][indx2].Status&KANAL_S_STATUS)&&
                  (o_b_1[nomer2][indx2].NomBort==o_b_1[nomer1][indx1].NomBort)&&
                  (o_b_1[nomer2][indx2].OprVysota==o_b_1[nomer1][indx1].OprVysota)&&
                  ((o_b_1[nomer2][indx2].Status&REJIM_UVD_STATUS)==
                  (o_b_1[nomer1][indx1].Status&REJIM_UVD_STATUS))&&
                  ((o_b_1[nomer2][indx2].Status&REJIM_RBS_STATUS)==
                  (o_b_1[nomer1][indx1].Status&REJIM_RBS_STATUS))&&
                (d_azmt<4.0)&&
                (ABS(o_b_1[nomer2][indx2].Dlnst-o_b_1[nomer1][indx1].Dlnst)<1.0)
                  )
               {
                        naidS=k;
                        break;
               }
               k=o_b_1[nomer2][indx2].AbsPrev;
           }

           if(naidS<0)
           {
                k=o_b_1[nomer1][indx1].AbsNext;
                while(k>=0)
                {
                        indx2=k/362;
                        nomer2=k%362;
                        if(ABS(o_b_1[nomer1][indx1].Time-o_b_1[nomer2][indx2].Time)>TO/5)
                        {
                                break;
                        }
                        d_azmt=ABS(o_b_1[nomer2][indx2].Azmt-o_b_1[nomer1][indx1].Azmt);
                        if(d_azmt>180)d_azmt=ABS(360-d_azmt);

                        if((o_b_1[nomer2][indx2].Status&KANAL_S_STATUS)&&
                          (o_b_1[nomer2][indx2].NomBort==o_b_1[nomer1][indx1].NomBort)&&
                          (o_b_1[nomer2][indx2].OprVysota==o_b_1[nomer1][indx1].OprVysota)&&
                          ((o_b_1[nomer2][indx2].Status&REJIM_UVD_STATUS)==
                          (o_b_1[nomer1][indx1].Status&REJIM_UVD_STATUS))&&
                          ((o_b_1[nomer2][indx2].Status&REJIM_RBS_STATUS)==
                          (o_b_1[nomer1][indx1].Status&REJIM_RBS_STATUS))&&
                         (d_azmt<4.0)&&
                        (ABS(o_b_1[nomer2][indx2].Dlnst-o_b_1[nomer1][indx1].Dlnst)<1.0)
                        )
                       {
                                naidS=k;
                                break;
                       }
                       k=o_b_1[nomer2][indx2].AbsNext;
                }
           }


           if(naidS>=0)
           {
//������� ������� S
                k=o_b_1[nomer1][indx1].AbsPrev;
                while(k>=0)
                {
                    indx3=k/362;
                    nomer3=k%362;
                    if(ABS(o_b_1[nomer1][indx1].Time-o_b_1[nomer3][indx3].Time)>TO/5)
                    {
                        break;
                    }
                    if(o_b_1[nomer3][indx3].Status&KANAL_P_STATUS)
                    {
//�������� ������ � ���������
                        if(ABS(o_b_1[nomer3][indx3].Azmt-o_b_1[nomer2][indx2].Azmt)<180.0)
                        {
                             azmt=o_b_1[nomer3][indx3].Azmt*0.4+
                                  o_b_1[nomer2][indx2].Azmt*0.6;
                             azmt_a=azmt;
                        }else{
                            if(o_b_1[nomer3][indx3].Azmt<o_b_1[nomer2][indx2].Azmt)
                            {
                               azmt=(o_b_1[nomer3][indx3].Azmt+360.0)*0.4+
                                     o_b_1[nomer2][indx2].Azmt*0.6;
                               if(azmt>360.0)azmt-=360.0;
                               azmt_a=(o_b_1[nomer2][indx2].Azmt-360.0)*0.6+
                                     o_b_1[nomer3][indx3].Azmt*0.4;

                               if(azmt_a<0)azmt_a+=360.0;
                            }else{ //o_b_1[nomer3][indx3].Azmt>o_b_1[nomer2][indx2].Azmt

                               azmt=(o_b_1[nomer3][indx3].Azmt)*0.4+
                                     (o_b_1[nomer2][indx2].Azmt+360.0)*0.6;
                               if(azmt>360.0)azmt-=360.0;
                               azmt_a=(o_b_1[nomer2][indx2].Azmt)*0.6+
                                     (o_b_1[nomer3][indx3].Azmt-360.0)*0.4;
                               if(azmt_a<0)azmt_a+=360.0;
                            }
                        }

                        d_azmt=ABS(azmt-o_b_1[nomer1][indx1].Azmt);
                        if(d_azmt>180)d_azmt=ABS(d_azmt-360.0);
                        d_azmt_a=ABS(azmt_a-o_b_1[nomer1][indx1].Azmt);
                        if(d_azmt_a>180)d_azmt_a=ABS(d_azmt_a-360.0);



                        dlnst=o_b_1[nomer3][indx3].Dlnst*0.4+
                             o_b_1[nomer2][indx2].Dlnst*0.6;

                        if((d_azmt<0.05||d_azmt_a<0.05) &&
                           ABS(dlnst-o_b_1[nomer1][indx1].Dlnst)<0.05)
                        {
                                o_b_1[nomer1][indx1].ParnOtschet1=naidS;
                                o_b_1[nomer1][indx1].ParnOtschet2=k;
                                o_b_1[nomer2][indx2].ParnOtschet1=i;
                                o_b_1[nomer2][indx2].ParnOtschet2=k;
                                o_b_1[nomer3][indx3].ParnOtschet1=i;
                                o_b_1[nomer3][indx3].ParnOtschet2=naidS;


                                break;
                        }
                    }
                    k=o_b_1[nomer3][indx3].AbsPrev;
                }
           }

       }
       i=o_b_1[nomer1][indx1].AbsNext;
  }




  return 1;
}




























