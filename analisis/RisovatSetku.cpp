//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include <stdio.h>
#include "RisovatSetku.h"
#include "tools.h"

//;#define  VYVOD_SETKA_DALN

int GetTochkiNaGranicePriam(double X1,double Y1,double X2, double Y2, //���������� �����q
                       long X1r, long Y1r, long X2r, long Y2r,  //���������� ���������������
                       long *X1ret,long *Y1ret, long *X2ret, long *Y2ret); //���������� ������������


//---------------------------------------------------------------------------
#pragma package(smart_init)
/*���������� ���������� ��� ������� ���������� ���������*/
  TColor SetkaOblastCol=clBlack;  //���� �������
  TColor SetkaKolca1Col=(TColor)RGB(0,0xF0,0);  //���� ����� �������� (����� 50 ��)
  TColor SetkaKolca2Col=(TColor)RGB(0,0x80,0);  //����� �������������
  TColor SetkaRad90Col=(TColor)RGB(0,0xF0,0);   //����� ���.����� 90 � 180 ��������
  TColor SetkaRad30Col=(TColor)RGB(0,0x80,0);   //����� ���.����� 30 � �.�.


//  TColor SetkaOblastColPrint=clBlack;  //���� �������
  TColor SetkaKolca1ColPrn=(TColor)RGB(0x60,0x60,0x60);  //���� ����� �������� (����� 50 ��)
  TColor SetkaKolca2ColPrn=(TColor)RGB(0x60,0x60,0x60);  //����� �������������
  TColor SetkaRad90ColPrn=(TColor)RGB(0x60,0x60,0x60);   //����� ���.����� 90 � 180 ��������
  TColor SetkaRad30ColPrn=(TColor)RGB(0x60,0x60,0x60);   //����� ���.����� 30 � �.�.





double PromMaxRisovatSetku4(double D[4])
{
  double Ret=D[0];
  for(int i=1;i<4;i++)
  {
    if(D[i]>Ret)Ret=D[i];
  }
  return Ret;
}

double PromMinRisovatSetku4(double D[4])
{
  double Ret=D[0];
  for(int i=1;i<4;i++)
  {
    if(D[i]<Ret)Ret=D[i];
  }
  return Ret;
}



int VyvodSetkaANLSS(
                TImage *Image, //������ ���� TImage
                double Left, double Bottom,    //���������� ������� ������ ���� ������ ���
                double Right, double Top //���������� ������� �������� ����
               )
{
  long X1,X2,Y1,Y2,X0,Y0;
  double KoefX,KoefY;
//  long Okrugl(double Chislo1);
  double Radius,Xf1,Yf1,Xf2,Yf2,Xf1a,Yf1a,Xf2a,Yf2a;
  int Ret;



  KoefX=(Image->Width-1)/((double)(Right-Left));
  KoefY=(Image->Height-1)/((double)(Bottom-Top));


  Image->Picture->Bitmap->Width=Image->Width;
  Image->Picture->Bitmap->Height=Image->Height;
  Image->Picture->Bitmap->Canvas->Brush->Style=bsSolid;
  Image->Picture->Bitmap->Canvas->Brush->Color=SetkaOblastCol;
  Image->Picture->Bitmap->Canvas->Pen->Style=psSolid;
  Image->Picture->Bitmap->Canvas->Pen->Color=SetkaOblastCol;
  Image->Picture->Bitmap->Canvas->Pen->Mode=pmCopy;
  Image->Picture->Bitmap->Canvas->Rectangle(0,0,Image->Width,Image->Height);
  if(Left==-400 && Top==400&&
     Right==400&& Bottom==-400||
     Left==-200 && Top==200&&
     Right==200&& Bottom==-200||
     Left==-100 && Top==100&&
     Right==100&& Bottom==-100)
  {
 //�������� ����� ������� ���������� ����������� � �������������
     Radius=Top;

     X0=Okrugl((-Left)*KoefX); Y0=Okrugl((-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad90Col;
     Image->Picture->Bitmap->Canvas->MoveTo(X0,0);
     Image->Picture->Bitmap->Canvas->LineTo(X0,Image->Height);
     Image->Picture->Bitmap->Canvas->MoveTo(0,Y0);
     Image->Picture->Bitmap->Canvas->LineTo(Image->Width,Y0);

//������ �������� ������������ �� �����
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Image->Picture->Bitmap->Canvas->Pen->Style=psDash;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=-Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Xf1=-Radius*cos(M_PI/3.0); Yf1=-Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Image->Picture->Bitmap->Canvas->Pen->Style=psDash;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=-Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Xf1=-Radius*cos(M_PI/3.0); Yf1=Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=-Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

//������ ���� ���������� ����������
//�� 200 � 400 ��
     Image->Picture->Bitmap->Canvas->Pen->Style=psSolid;
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaKolca1Col;
     Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
     Xf1=-Radius/2; Yf1=Radius/2; Xf2=Radius/2; Yf2=-Radius/2;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);
     Xf1=-Radius; Yf1=Radius; Xf2=Radius; Yf2=-Radius;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);

     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaKolca2Col;
     Xf1=-Radius/4; Yf1=Radius/4; Xf2=Radius/4; Yf2=-Radius/4;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);
     Xf1=-Radius*3/4; Yf1=Radius*3/4; Xf2=Radius*3/4; Yf2=-Radius*3/4;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);

#ifdef VYVOD_SETKA_DALN
//������� ������ � ����������
     Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
     Image->Picture->Bitmap->Canvas->Font->Color=(TColor)RGB(0xFF,0xE0,0xE0);;
     Image->Picture->Bitmap->Canvas->Font->Name="Arial";
     Image->Picture->Bitmap->Canvas->Font->Size=8;

     AnsiString DlnstStrk;
     for(int i=1;i<=4;i++)
     {
        Image->Picture->Bitmap->Canvas->Pen->Color=(TColor)RGB(0,0xF0,0);
        X1=Okrugl((-Radius*i/4-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/4);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/4));

        X1=Okrugl((Radius*i/4-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/4);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/4));


        Y1=Okrugl((Radius*i/4-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/4);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/4));

        Y1=Okrugl((-Radius*i/4-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/4);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/4));
     }

#endif

  }else if(Left==-50 && Top==50&&
     Right==50&& Bottom==-50)
  {
     Radius=Top;

     X0=Okrugl((-Left)*KoefX); Y0=Okrugl((-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad90Col;
     Image->Picture->Bitmap->Canvas->MoveTo(X0,0);
     Image->Picture->Bitmap->Canvas->LineTo(X0,Image->Height);
     Image->Picture->Bitmap->Canvas->MoveTo(0,Y0);
     Image->Picture->Bitmap->Canvas->LineTo(Image->Width,Y0);

//������ �������� ������������ �� �����
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Image->Picture->Bitmap->Canvas->Pen->Style=psDash;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=-Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Xf1=-Radius*cos(M_PI/3.0); Yf1=-Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

     Image->Picture->Bitmap->Canvas->Pen->Style=psDash;
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=-Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;
     Xf1=-Radius*cos(M_PI/3.0); Yf1=Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=-Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
     Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);

//������ ���� ���������� ����������
//�� 50 ��
     Image->Picture->Bitmap->Canvas->Pen->Style=psSolid;
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaKolca1Col;
     Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
     Xf1=-Radius; Yf1=Radius; Xf2=Radius; Yf2=-Radius;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);

//������ ����� 10 ��
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaKolca2Col;
     Xf1=-Radius/5; Yf1=Radius/5; Xf2=Radius/5; Yf2=-Radius/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);

     Xf1=-Radius*2/5; Yf1=Radius*2/5; Xf2=Radius*2/5; Yf2=-Radius*2/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);

     Xf1=-Radius*3/5; Yf1=Radius*3/5; Xf2=Radius*3/5; Yf2=-Radius*3/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);

     Xf1=-Radius*4/5; Yf1=Radius*4/5; Xf2=Radius*4/5; Yf2=-Radius*4/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);


#ifdef VYVOD_SETKA_DALN
//������� ������ � ����������
     Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
     Image->Picture->Bitmap->Canvas->Font->Color=(TColor)RGB(0xE0,0xE0,0xE0);;
     Image->Picture->Bitmap->Canvas->Font->Name="Arial";
     Image->Picture->Bitmap->Canvas->Font->Size=8;


     AnsiString DlnstStrk;
     for(int i=1;i<=5;i++)
     {
        Image->Picture->Bitmap->Canvas->Pen->Color=(TColor)RGB(0,0xFF,0);
        X1=Okrugl((-Radius*i/5-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/5);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/5));

        X1=Okrugl((Radius*i/5-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/5);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/5));


        Y1=Okrugl((Radius*i/5-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/5);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/5));

        Y1=Okrugl((-Radius*i/5-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/5);
        X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(Radius*i/5));
     }
#endif

  }else{
     double Rads[4];
     long MinD,MaxD,DD;
//����� ������ ������������ �������
 //��������� ���������� ��� � �������� ������������ ������ �������� ������ ����



//��������� ������������ � ����������� ������� ����������� � ��
     Rads[0]=sqrt(Left*Left+Top*Top);
     Rads[1]=sqrt(Right*Right+Top*Top);
     Rads[2]=sqrt(Right*Right+Bottom*Bottom);
     Rads[3]=sqrt(Left*Left+Bottom*Bottom);

     //������ ������� ����� ���������
//������� �������������� � ������������ �����
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad90Col;
     Xf1=0; Xf2=0; Yf1=400; Yf2=-400;

     Xf1a=(Xf1-Left)*KoefX;     Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;      Yf2a=(Yf2-Top)*KoefY;
     if(Xf1a>=0&&Xf1a<Image->Width)
     {
       X1=Okrugl(Xf1a); X2=Okrugl(Xf2a); Y1=Okrugl(Yf1a); Y2=Okrugl(Yf2a);
       Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
       Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);
     }

     Xf1=-400; Xf2=400; Yf1=0; Yf2=0;
     Xf1a=(Xf1-Left)*KoefX;     Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;      Yf2a=(Yf2-Top)*KoefY;
     if(Yf1a>=0&&Yf1a<Image->Height)
     {
       X1=Okrugl(Xf1a); X2=Okrugl(Xf2a); Y1=Okrugl(Yf1a); Y2=Okrugl(Yf2a);
       Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
       Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);
     }

//������� ��� ��������� �����
     Image->Picture->Bitmap->Canvas->Pen->Style=psDash;
     Image->Picture->Bitmap->Canvas->Pen->Color=SetkaRad30Col;

     Xf1=10*cos(M_PI/6.0);
     Yf1=10*sin(M_PI/6.0);
     Xf2=-10*cos(M_PI/6.0);
     Yf2=-10*sin(M_PI/6.0);
     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Image->Width-1,Image->Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
        Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);
     }

     Xf1=10*cos(M_PI/3.0);
     Yf1=10*sin(M_PI/3.0);
     Xf2=-10*cos(M_PI/3.0);
     Yf2=-10*sin(M_PI/3.0);

     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Image->Width-1,Image->Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
        Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);
     }

     Xf1=-10*cos(M_PI/6.0);
     Yf1=10*sin(M_PI/6.0);
     Xf2=10*cos(M_PI/6.0);
     Yf2=-10*sin(M_PI/6.0);
     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Image->Width-1,Image->Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
        Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);
     }

     Xf1=-10*cos(M_PI/3.0);
     Yf1=10*sin(M_PI/3.0);
     Xf2=10*cos(M_PI/3.0);
     Yf2=-10*sin(M_PI/3.0);
     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Image->Width-1,Image->Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Image->Picture->Bitmap->Canvas->MoveTo(X1,Y1);
        Image->Picture->Bitmap->Canvas->LineTo(X2,Y2);
     }


     if (Left<0&&Right>0)
     {
       if(Bottom<0&&Top>0)
       {
         MinD=10;
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }else{
         if(Top<0)
         {
           MinD=-Top;
         }else{
           MinD=Bottom;
         }
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }
     }else{
       if(Bottom<0&&Top>0)
       {
         if(Right<0)
         {
           MinD=-Right;
         }else{
           MinD=Left;
         }
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }else{
         MinD=(long)PromMinRisovatSetku4(Rads);
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }
     }
//������ ��������� ����������� � ������������ �������
     while(MinD<10||MinD%10!=0)
     {
       MinD++;
     };
     while(MaxD%10!=0)
     {
       MaxD--;
     }
//�������� ������
     Image->Picture->Bitmap->Canvas->Pen->Style=psSolid;
     Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
     for(DD=MinD;DD<=MaxD&&DD<=400;DD+=10)
     {
        if(DD%50==0)
        {
          Image->Picture->Bitmap->Canvas->Pen->Color=SetkaKolca1Col;
        }else{
          Image->Picture->Bitmap->Canvas->Pen->Color=SetkaKolca2Col;
        }
//��������
//��������� � �������
        X1=Okrugl((-DD-Left)*KoefX); Y1=Okrugl((DD-Top)*KoefY);
        X2=Okrugl((DD-Left)*KoefX)+1; Y2=Okrugl((-DD-Top)*KoefY)+1;
        Image->Picture->Bitmap->Canvas->Ellipse(X1,Y1,X2,Y2);

//�������� ������ ������
#ifdef VYVOD_SETKA_DALN
 //������� ������ � ����������
       if(KoefX<3&&DD%100!=0)continue;
       if(KoefX<6&&DD%50!=0)continue;

       Image->Picture->Bitmap->Canvas->Brush->Style=bsClear;
       Image->Picture->Bitmap->Canvas->Font->Color=(TColor)RGB(0xE0,0xE0,0xE0);;
       Image->Picture->Bitmap->Canvas->Font->Name="Arial";
       Image->Picture->Bitmap->Canvas->Font->Size=8;

      //������� ������ � ��������� �� ������
  //�������� ����� �� ���������� �������� �� ���� ������������ �����
        X1=Okrugl((-Left)*KoefX); Y1=Okrugl((DD-Top)*KoefY);

        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
//������� �����
          X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
          Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
          Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������
        Xf1=-DD*sin(M_PI/6.0);
        Yf1=DD*cos(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }

        Xf1=+DD*sin(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
              Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }


//��������, ����� �� ���������� �� ����, ������������ ������
        X1=Okrugl((DD-Left)*KoefX); Y1=Okrugl((-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
//������� �����
          X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
          Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
          Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������
        Xf1=DD*sin(M_PI/3.0);
        Yf1=DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
              Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }

        Yf1=-DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
              Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }

//�������� ����� �� ���������� ������ �� ������ ������������ ����
        X1=Okrugl((-Left)*KoefX); Y1=Okrugl((-DD-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
//������� �����
          X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
          Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
          Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������

        Xf1=DD*sin(M_PI/6.0);
        Yf1=-DD*cos(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
        Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }

        Xf1=-DD*sin(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
              Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }


        //��������, ����� �� ���������� �� ����, ������������ �����
        X1=Okrugl((-DD-Left)*KoefX); Y1=Okrugl((-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
//������� �����
          X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
          Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
          Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������
        Xf1=-DD*sin(M_PI/3.0);
        Yf1=-DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
            Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
           X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
           Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
           Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }

        Yf1=+DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
        {
          X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
          Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
          Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
        }

//������ ����������� ������ ����� � ������� �������� ����
        Yf1=Top;
        if(DD*DD>Yf1*Yf1)
        {
          Xf1=sqrt(DD*DD-Yf1*Yf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
             Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
          X1=Okrugl((-Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
             Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
        }

//������ ����������� � ����� ��������
        Xf1=Left;
        if(DD*DD-Xf1*Xf1>0)
        {
          Yf1=sqrt(DD*DD-Xf1*Xf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
            Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
            Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((-Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
        }

//������ ����������� � ������ ��������
        Yf1=Bottom;
        if(DD*DD>Yf1*Yf1)
        {
          Xf1=sqrt(DD*DD-Yf1*Yf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
             Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))+2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
          X1=Okrugl((-Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
             Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))/2;
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))+2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
        }

//������ ����������� � ����� ��������
        Xf1=Right;
        if(DD*DD-Xf1*Xf1>0)
        {
          Yf1=sqrt(DD*DD-Xf1*Xf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
            Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
            X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))+2;
            Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((-Yf1-Top)*KoefY);
          if(X1<Image->Picture->Bitmap->Width&&X1>=0&&
           Y1<Image->Picture->Bitmap->Height&&Y1>=0)
          {
             Y1-=Image->Picture->Bitmap->Canvas->TextHeight(String(DD))/2;
             X1-=Image->Picture->Bitmap->Canvas->TextWidth(String(DD))+2;
             Image->Picture->Bitmap->Canvas->TextOut(X1,Y1,String(DD));
          }
        }



#endif


     }



  }
  return 1;
}



//���������� ����� ��� ������� ���� TPrinter
//��� ����� ������� ��� ����� ���� �������� ����� BeginDoc

int VyvodSetkaPrinter(
      TCanvas *Canvas,       //
      int Width,int Height,  //������� ��������
      int Xo1, int Yo1,         //������ �������� ������
      long Left, long Bottom,    //���������� ������� ������ ���� ������ ���
      long Right, long Top //���������� ������� �������� ����
      )
{
  int Ret;
  long X1,X2,Y1,Y2,X0,Y0;
  double KoefX,KoefY;
//  long Okrugl(double Chislo1);
  double Radius,Xf1,Yf1,Xf2,Yf2,Xf1a,Yf1a,Xf2a,Yf2a;


  double PopravKoef=Width/768.0; //������������ ����������� ��� ������
  long Width1,Width2,Width05;
  Width1=Okrugl(PopravKoef);
  Width2=Okrugl(2*PopravKoef);
  Width05=Okrugl(PopravKoef/2); if(Width05<1)Width05=1;


  KoefX=(Width-1)/((double)(Right-Left));
  KoefY=(Height-1)/((double)(Bottom-Top));


  Canvas->Brush->Style=bsSolid;
  Canvas->Brush->Color=clWhite;
  Canvas->Pen->Style=psSolid;
  Canvas->Pen->Color=clWhite;
  Canvas->Pen->Mode=pmCopy;
  Canvas->Rectangle(Xo1,Yo1,Xo1+Width,Yo1+Height);
  if(Left==-400 && Top==400&&
     Right==400&& Bottom==-400||
     Left==-200 && Top==200&&
     Right==200&& Bottom==-200||
     Left==-100 && Top==100&&
     Right==100&& Bottom==-100)
  {
 //�������� ����� ������� ���������� ����������� � �������������
     Radius=Top;

     X0=Okrugl((-Left)*KoefX); Y0=Okrugl((-Top)*KoefY);
     Canvas->Pen->Color=SetkaRad90ColPrn;
     Canvas->Pen->Width=Width1;
     Canvas->MoveTo(X0+Xo1,0+Yo1);
     Canvas->LineTo(X0+Xo1,Height+Yo1);
     Canvas->MoveTo(0+Xo1,Y0+Yo1);
     Canvas->LineTo(Width+Xo1,Y0+Yo1);


     Canvas->Pen->Color=SetkaRad30ColPrn;

//������ �������� ������������ �� �����
     Canvas->Pen->Width=Width05;
     Canvas->Pen->Style=psDash;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=-Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);

     Xf1=-Radius*cos(M_PI/3.0); Yf1=-Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);

     Canvas->Pen->Style=psDash;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=-Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);

     Xf1=-Radius*cos(M_PI/3.0); Yf1=Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=-Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);
     Canvas->Pen->Width=1;

//������ ���� ���������� ����������
//�� 200 � 400 ��
     Canvas->Pen->Style=psSolid;
     Canvas->Pen->Color=SetkaKolca1ColPrn;
     Canvas->Brush->Style=bsClear;
     Canvas->Pen->Width=Width1;
     Xf1=-Radius/2; Yf1=Radius/2; Xf2=Radius/2; Yf2=-Radius/2;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);
     Xf1=-Radius; Yf1=Radius; Xf2=Radius; Yf2=-Radius;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);

     Canvas->Pen->Width=Width05;
     Canvas->Pen->Color=SetkaKolca2ColPrn;
     Xf1=-Radius/4; Yf1=Radius/4; Xf2=Radius/4; Yf2=-Radius/4;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);
     Xf1=-Radius*3/4; Yf1=Radius*3/4; Xf2=Radius*3/4; Yf2=-Radius*3/4;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);
     Canvas->Pen->Width=1;

//������� ������ � ����������
     Canvas->Brush->Style=bsClear;
     Canvas->Font->Color=(TColor)RGB(90,90,90);
     Canvas->Pen->Color=(TColor)RGB(90,90,90);
     Canvas->Font->Name="Arial";
     Canvas->Font->Size=7;

     AnsiString DlnstStrk;
     for(int i=1;i<=4;i++)
     {
        X1=Okrugl((-Radius*i/4-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/4);
        X1-=Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/4));

        X1=Okrugl((Radius*i/4-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/4);
        X1-=Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/4));


        Y1=Okrugl((Radius*i/4-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/4);
        X1-=Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/4));

        Y1=Okrugl((-Radius*i/4-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/4);
        X1-=Canvas->TextWidth(String(Radius*i/4))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/4))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/4));
     }



  }else if(Left==-50 && Top==50&&
     Right==50&& Bottom==-50)
  {
     Radius=Top;
     X0=Okrugl((-Left)*KoefX); Y0=Okrugl((-Top)*KoefY);
     Canvas->Pen->Color=SetkaRad90ColPrn;
     Canvas->Pen->Width=Width1;
     Canvas->Pen->Color=(TColor)RGB(20,20,20);
     Canvas->MoveTo(X0+Xo1,0+Yo1);
     Canvas->LineTo(X0+Xo1,Height+Yo1);
     Canvas->MoveTo(0+Xo1,Y0+Yo1);
     Canvas->LineTo(Width+Xo1,Y0+Yo1);

//������ �������� ������������ �� �����
     Canvas->Pen->Color=SetkaRad30ColPrn;
     Canvas->Pen->Width=Width05;

     Canvas->Pen->Style=psDash;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=-Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);

     Xf1=-Radius*cos(M_PI/3.0); Yf1=-Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);

     Canvas->Pen->Style=psDash;
     Xf1=-Radius*cos(M_PI/6.0); Yf1=Radius*sin(M_PI/6.0);
     Xf2=Radius*cos(M_PI/6.0);  Yf2=-Radius*sin(M_PI/6.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);

     Xf1=-Radius*cos(M_PI/3.0); Yf1=Radius*sin(M_PI/3.0);
     Xf2=Radius*cos(M_PI/3.0);  Yf2=-Radius*sin(M_PI/3.0);
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX); Y2=Okrugl((Yf2-Top)*KoefY);
     Canvas->MoveTo(X1+Xo1,Y1+Yo1);
     Canvas->LineTo(X2+Xo1,Y2+Yo1);

//������ ���� ���������� ����������
//�� 50 ��
     Canvas->Pen->Style=psSolid;
     Canvas->Pen->Color=SetkaKolca1ColPrn;
     Canvas->Pen->Width=Width1;
     Canvas->Brush->Style=bsClear;
     Xf1=-Radius; Yf1=Radius; Xf2=Radius; Yf2=-Radius;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);

//������ ����� 10 ��
     Canvas->Pen->Color=SetkaKolca1ColPrn;
     Canvas->Pen->Width=Width05;
     Xf1=-Radius/5; Yf1=Radius/5; Xf2=Radius/5; Yf2=-Radius/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);

     Xf1=-Radius*2/5; Yf1=Radius*2/5; Xf2=Radius*2/5; Yf2=-Radius*2/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);

     Xf1=-Radius*3/5; Yf1=Radius*3/5; Xf2=Radius*3/5; Yf2=-Radius*3/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);

     Xf1=-Radius*4/5; Yf1=Radius*4/5; Xf2=Radius*4/5; Yf2=-Radius*4/5;
     X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
     X2=Okrugl((Xf2-Left)*KoefX)+1; Y2=Okrugl((Yf2-Top)*KoefY)+1;
     Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);
     Canvas->Pen->Width=1;


//������� ������ � ����������
     Canvas->Brush->Style=bsClear;
     Canvas->Font->Color=(TColor)RGB(90,90,90);
     Canvas->Pen->Color=(TColor)RGB(90,90,90);
     Canvas->Font->Name="Arial";
     Canvas->Font->Size=7;


     AnsiString DlnstStrk;
     for(int i=1;i<=5;i++)
     {
        X1=Okrugl((-Radius*i/5-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/5);
        X1-=Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/5));

        X1=Okrugl((Radius*i/5-Left)*KoefX); Y1=Okrugl(-Top*KoefY);
        DlnstStrk=String(Radius*i/5);
        X1-=Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/5));


        Y1=Okrugl((Radius*i/5-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/5);
        X1-=Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/5));

        Y1=Okrugl((-Radius*i/5-Top)*KoefY); X1=Okrugl(-Left*KoefX);
        DlnstStrk=String(Radius*i/5);
        X1-=Canvas->TextWidth(String(Radius*i/5))/2;
        Y1-=Canvas->TextHeight(String(Radius*i/5))/2;
  //������� ��������
        Canvas->TextOut(X1+Xo1,Y1+Yo1,String(Radius*i/5));
     }
  }else{
     double Rads[4];
     long MinD,MaxD,DD;

     Canvas->Pen->Style=psSolid;
     Canvas->Pen->Color=SetkaRad90ColPrn;
     Canvas->Pen->Width=Width1;
  //������� �������������� � ������������ �����
     Xf1=0; Xf2=0; Yf1=-400; Yf2=400;
     Xf1a=(Xf1-Left)*KoefX;     Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;      Yf2a=(Yf2-Top)*KoefY;
     if(Xf1a>=0&&Xf1a<Width)
     {
       X1=Okrugl(Xf1a); X2=Okrugl(Xf2a); Y1=0; Y2=Height-1;
       Canvas->MoveTo(X1+Xo1,Y1+Yo1);
       Canvas->LineTo(X2+Xo1,Y2+Yo1);
     }

     Xf1=-400; Xf2=400; Yf1=0; Yf2=0;
     Xf1a=(Xf1-Left)*KoefX;     Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;      Yf2a=(Yf2-Top)*KoefY;
     if(Yf1a>=0&&Yf1a<Height)
     {
       X1=0; X2=Width-1; Y1=Okrugl(Yf1a); Y2=Okrugl(Yf2a);
       Canvas->MoveTo(X1+Xo1,Y1+Yo1);
       Canvas->LineTo(X2+Xo1,Y2+Yo1);
     }

//������� ��� ��������� �����
     Canvas->Pen->Style=psDash;
     Canvas->Pen->Width=Width05;
     Canvas->Pen->Color=SetkaRad90ColPrn;
     Xf1=10*cos(M_PI/6.0);
     Yf1=10*sin(M_PI/6.0);
     Xf2=-10*cos(M_PI/6.0);
     Yf2=-10*sin(M_PI/6.0);
     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Width-1,Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Canvas->MoveTo(X1+Xo1,Y1+Yo1);
        Canvas->LineTo(X2+Xo1,Y2+Yo1);
     }

     Xf1=10*cos(M_PI/3.0);
     Yf1=10*sin(M_PI/3.0);
     Xf2=-10*cos(M_PI/3.0);
     Yf2=-10*sin(M_PI/3.0);
     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Width-1,Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Canvas->MoveTo(X1+Xo1,Y1+Yo1);
        Canvas->LineTo(X2+Xo1,Y2+Yo1);
     }

     Xf1=-10*cos(M_PI/6.0);
     Yf1=10*sin(M_PI/6.0);
     Xf2=10*cos(M_PI/6.0);
     Yf2=-10*sin(M_PI/6.0);
     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Width-1,Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Canvas->MoveTo(X1+Xo1,Y1+Yo1);
        Canvas->LineTo(X2+Xo1,Y2+Yo1);
     }

     Xf1=-10*cos(M_PI/3.0);
     Yf1=10*sin(M_PI/3.0);
     Xf2=10*cos(M_PI/3.0);
     Yf2=-10*sin(M_PI/3.0);
     Xf1a=(Xf1-Left)*KoefX; Xf2a=(Xf2-Left)*KoefX;
     Yf1a=(Yf1-Top)*KoefY;  Yf2a=(Yf2-Top)*KoefY;
//�������� ���������� ����� ����� �� ������
     Ret=GetTochkiNaGranicePriam(Xf1a,Yf1a,Xf2a,Yf2a,0,0,Width-1,Height-1,
                                 &X1,&Y1,&X2,&Y2);
     if(Ret==1)
     {
        Canvas->MoveTo(X1+Xo1,Y1+Yo1);
        Canvas->LineTo(X2+Xo1,Y2+Yo1);
     }


//��������� ������������ � ����������� ������� ����������� � ��
     Rads[0]=sqrt(Left*Left+Top*Top);
     Rads[1]=sqrt(Right*Right+Top*Top);
     Rads[2]=sqrt(Right*Right+Bottom*Bottom);
     Rads[3]=sqrt(Left*Left+Bottom*Bottom);

     if (Left<0&&Right>0)
     {
       if(Bottom<0&&Top>0)
       {
         MinD=10;
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }else{
         if(Top<0)
         {
           MinD=-Top;
         }else{
           MinD=Bottom;
         }
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }
     }else{
       if(Bottom<0&&Top>0)
       {
         if(Right<0)
         {
           MinD=-Right;
         }else{
           MinD=Left;
         }
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }else{
         MinD=(long)PromMinRisovatSetku4(Rads);
         MaxD=(long)PromMaxRisovatSetku4(Rads);
       }
     }
//������ ��������� ����������� � ������������ �������
     while(MinD<10||MinD%10!=0)
     {
       MinD++;
     };
     while(MaxD%10!=0)
     {
       MaxD--;
     }
//�������� ������
     Canvas->Pen->Style=psSolid;
     Canvas->Brush->Style=bsClear;
     for(DD=MinD;DD<=MaxD&&DD<=400;DD+=10)
     {
        if(DD%50==0)
        {
          Canvas->Pen->Width=Width1;
          Canvas->Pen->Color=SetkaKolca1ColPrn;
        }else{
          Canvas->Pen->Width=Width05;
          Canvas->Pen->Color=SetkaKolca2ColPrn;
        }
//��������
//��������� � �������
        X1=Okrugl((-DD-Left)*KoefX); Y1=Okrugl((DD-Top)*KoefY);
        X2=Okrugl((DD-Left)*KoefX)+1; Y2=Okrugl((-DD-Top)*KoefY)+1;
        Canvas->Ellipse(X1+Xo1,Y1+Yo1,X2+Xo1,Y2+Yo1);
        Canvas->Pen->Width=1;
//�������� ������ ������
 //������� ������ � ����������
       if(KoefX<3*PopravKoef&&DD%100!=0)continue;
       if(KoefX<6*PopravKoef&&DD%50!=0)continue;

       Canvas->Brush->Style=bsClear;
       Canvas->Font->Color=(TColor)RGB(90,90,90);
       Canvas->Pen->Color=(TColor)RGB(90,90,90);
       Canvas->Font->Name="Arial";
       Canvas->Font->Size=7;

      //������� ������ � ��������� �� ������
  //�������� ����� �� ���������� �������� �� ���� ������������ �����
        X1=Okrugl((-Left)*KoefX); Y1=Okrugl((DD-Top)*KoefY);

        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
//������� �����
          X1-=Canvas->TextWidth(String(DD))/2;
          Y1-=Canvas->TextHeight(String(DD))/2;
          Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������

        Xf1=-DD*sin(M_PI/6.0);
        Yf1=DD*cos(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))/2;
//             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }

        Xf1=+DD*sin(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))/2;
//             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }


//��������, ����� �� ���������� �� ����, ������������ ������
        X1=Okrugl((DD-Left)*KoefX); Y1=Okrugl((-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
//������� �����
          X1-=Canvas->TextWidth(String(DD))/2;
          Y1-=Canvas->TextHeight(String(DD))/2;
          Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������
        Xf1=DD*sin(M_PI/3.0);
        Yf1=DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))/2;
//             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }

        Yf1=-DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))/2;
//             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }

//�������� ����� �� ���������� ������ �� ������ ������������ ����
        X1=Okrugl((-Left)*KoefX); Y1=Okrugl((-DD-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
//������� �����
          X1-=Canvas->TextWidth(String(DD))/2;
          Y1-=Canvas->TextHeight(String(DD))/2;
          Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������

        Xf1=DD*sin(M_PI/6.0);
        Yf1=-DD*cos(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))/2;
//             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }

        Xf1=-DD*sin(M_PI/6.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))/2;
//             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }


        //��������, ����� �� ���������� �� ����, ������������ �����
        X1=Okrugl((-DD-Left)*KoefX); Y1=Okrugl((-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
//������� �����
          X1-=Canvas->TextWidth(String(DD))/2;
          Y1-=Canvas->TextHeight(String(DD))/2;
          Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }
//������� ����� ������� �������� -30 � +30 ��������
        Xf1=-DD*sin(M_PI/3.0);
        Yf1=-DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
           X1-=Canvas->TextWidth(String(DD))/2;
           Y1-=Canvas->TextHeight(String(DD))/2;
//           Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }

        Yf1=+DD*cos(M_PI/3.0);
        X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
        if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
        {
          X1-=Canvas->TextWidth(String(DD))/2;
          Y1-=Canvas->TextHeight(String(DD))/2;
//          Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
        }

//������ ����������� ������ ����� � ������� �������� ����
        Yf1=Top;
        if(DD*DD>Yf1*Yf1)
        {
          Xf1=sqrt(DD*DD-Yf1*Yf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
             Y1<Height&&Y1>=0)
          {
             X1-=Canvas->TextWidth(String(DD))/2;
             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
          X1=Okrugl((-Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
             Y1<Height&&Y1>=0)
          {
             X1-=Canvas->TextWidth(String(DD))/2;
             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
        }

//������ ����������� � ����� ��������
        Xf1=Left;
        if(DD*DD-Xf1*Xf1>0)
        {
          Yf1=sqrt(DD*DD-Xf1*Xf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
             Y1<Height&&Y1>=0)
          {
            Y1-=Canvas->TextHeight(String(DD))/2;
            Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((-Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
          {
             Y1-=Canvas->TextHeight(String(DD))/2;
             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
        }

//������ ����������� � ������ ��������        
        Yf1=Bottom;
        if(DD*DD>Yf1*Yf1)
        {
          Xf1=sqrt(DD*DD-Yf1*Yf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
             Y1<Height&&Y1>=0)
          {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))+2;
             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
          X1=Okrugl((-Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
             Y1<Height&&Y1>=0)
          {
             X1-=Canvas->TextWidth(String(DD))/2;
             Y1-=Canvas->TextHeight(String(DD))+2;
             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
        }

//������ ����������� � ����� ��������
        Xf1=Right;
        if(DD*DD-Xf1*Xf1>0)
        {
          Yf1=sqrt(DD*DD-Xf1*Xf1);
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
          {
            Y1-=Canvas->TextHeight(String(DD))/2;
            X1-=Canvas->TextWidth(String(DD))+2;
            Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
          X1=Okrugl((Xf1-Left)*KoefX); Y1=Okrugl((-Yf1-Top)*KoefY);
          if(X1<Width&&X1>=0&&
           Y1<Height&&Y1>=0)
          {
             Y1-=Canvas->TextHeight(String(DD))/2;
             X1-=Canvas->TextWidth(String(DD))+2;
             Canvas->TextOut(X1+Xo1,Y1+Yo1,String(DD));
          }
        }
     }
  }
  return 1;
}



