//---------------------------------------------------------------------------


#pragma hdrstop

#include "traekt_zavyazka.h"
#include "tools.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


int tzDebugCount1=0;
int tzDebugCount2=0;


/*������� �������� ������*/
//������� �������� ������
int OBZORY_1::tzZavyazatTrassu(long smesh1, long smesh2)
{
        int nomer1, indx1, nomer2, indx2;
        int isOdinKanal=0;
        int isOdinRejim=0;
        double skorost;
        double dT,dD,dfV;
        unsigned long ulNumber;
        nomer1=smesh1%362;
        nomer2=smesh2%362;
        indx1=smesh1/362;
        indx2=smesh2/362;


        dT=o_b_1[nomer2][indx2].Time-o_b_1[nomer1][indx1].Time;
        if(dT<0)dT+=24;
        if(dT>=TO+TO/8.0)
        {
            return (-1);
        }


        if(dT<=TO-TO/8.0)
        {
            return 0;
        }



        if(IsInPlot)
        {
           if(((o_b_1[nomer1][indx1].Status&KANAL_S_STATUS)&&
              (o_b_1[nomer2][indx2].Status&KANAL_S_STATUS)&&
              !(o_b_1[nomer1][indx1].Status&KANAL_P_STATUS)&&
              !(o_b_1[nomer2][indx2].Status&KANAL_P_STATUS))||
              ((o_b_1[nomer1][indx1].Status&KANAL_P_STATUS)&&
               (o_b_1[nomer2][indx2].Status&KANAL_P_STATUS)&&
              !(o_b_1[nomer1][indx1].Status&KANAL_S_STATUS)&&
              !(o_b_1[nomer2][indx2].Status&KANAL_S_STATUS)))
            {
                isOdinKanal=1;
            }
        }else{
             if(((o_b_1[nomer1][indx1].Status&KANAL_S_STATUS)&&
              (o_b_1[nomer2][indx2].Status&KANAL_S_STATUS))||
              ((o_b_1[nomer1][indx1].Status&KANAL_P_STATUS)&&
               (o_b_1[nomer2][indx2].Status&KANAL_P_STATUS)&&
              !(o_b_1[nomer1][indx1].Status&KANAL_S_STATUS)&&
              !(o_b_1[nomer2][indx2].Status&KANAL_S_STATUS)))
            {
                isOdinKanal=1;
            }
        }

        if(isOdinKanal)
        {
                if(o_b_1[nomer1][indx1].Status&KANAL_S_STATUS)
                {
                        if(((o_b_1[nomer1][indx1].Status&REJIM_UVD_STATUS)&&
                           (o_b_1[nomer2][indx2].Status&REJIM_UVD_STATUS))||
                           ((o_b_1[nomer1][indx1].Status&REJIM_RBS_STATUS)&&
                           (o_b_1[nomer2][indx2].Status&REJIM_RBS_STATUS)))
                        {
                            isOdinRejim=1;
                            if(o_b_1[nomer1][indx1].Status&REJIM_UVD_STATUS)
                            {
                                ulNumber=((unsigned long)(o_b_1[nomer2][indx2].NomBort))&
                                        0x01000000;
                            }else{
                                ulNumber=((unsigned long)(o_b_1[nomer2][indx2].NomBort))&
                                        0x02000000;
                            }
                        }

                }else{
                    isOdinRejim=1;
                    ulNumber=(unsigned long)(o_b_1[nomer2][indx2].NomBort);
                }
        }

//��������:
//1) ������ ������ ���� �����,
//2) ������ ������ ���� ���������
//3) ������ ������ ���� ���������

        if(o_b_1[nomer1][indx1].NomBort==o_b_1[nomer2][indx2].NomBort&&
          isOdinKanal&&isOdinRejim)
        {
               tzDebugCount1++;
                 dD=RastMejduObjektami(o_b_1[nomer2][indx2].Azmt,
                                        o_b_1[nomer2][indx2].Dlnst,
                           o_b_1[nomer1][indx1].Azmt,
                                        o_b_1[nomer1][indx1].Dlnst);
                 dfV=dD/dT;
                 if(dfV>=MIN_SKOROST&&dfV<=MAX_SKOROST)
                 {
                         tzDebugCount2++;
                       return tzMakeTrassu(smesh1%362,smesh1/362,
                                        smesh2%362,smesh2/362);
                 }
        }
        return 0;
}


//�������� ������ - ���������� �� tpZavyazatTrassu
int OBZORY_1::tzMakeTrassu(long nomer1, long indx1,
                   long nomer2, long indx2 )
{
    unsigned long NewNomer;
    int i_t;

    NewNomer=o_b_1[nomer1][indx1].NomBort;
    if(NewNomer==99999)
    {
         int debug=1;
    }
    if(o_b_1[nomer1][indx1].Status&REJIM_UVD_STATUS)
    {
        NewNomer|=0x01000000;
    }else
    if(o_b_1[nomer1][indx1].Status&REJIM_RBS_STATUS)
    {
        if(NewNomer==99999)
        {
                int debug=1;
        }
        NewNomer|=0x02000000;
    }
    if(NewNomer==99999||NewNomer==0)
    {
        i_t=tpNewTrassa((unsigned long)(99000+nomer_for_99999_0));
    }else{

        if(NewNomer==99999)
        {
             int debug=1;
        }
        i_t=tpNewTrassa(NewNomer);
    }
    if(i_t<0)return i_t;
    if(NewNomer==99999||NewNomer==0)
    {
       nomer_for_99999_0=(nomer_for_99999_0+1)%999;
    }
    tpAddToOtisWithParnye(nomer1,indx1,i_t);
    tpAddToOtisWithParnye(nomer2,indx2,i_t);
    return 1;
}




int  OBZORY_1::tzTryZavyazatTrassu(int isPonly,
                       long smesh)
{
    long smesh1;
    int iRet;

    if(isPonly==0&&(o_b_1[smesh%362][smesh/362].Status&0x03)==1)
    {
        return 0;
    }
    smesh1=tnGetPrevMainOtshet(smesh);
    while(smesh1>=0)
    {
        iRet=tzZavyazatTrassu(smesh1,smesh);
        if(iRet)break;
        smesh1=tnGetPrevMainOtshet(smesh1);
    }
    if(smesh1<0||iRet<0)return 0;
    if(n_of_work_otis==max_n_of_work_otis)
    {
        max_n_of_work_otis+=100;
        work_otis=(ONE_TRASSA_INFO**)realloc(work_otis,
                        sizeof(ONE_TRASSA_INFO*)*max_n_of_work_otis);
    }
    work_otis[n_of_work_otis]=&(otis[n_of_otis-1]);
    n_of_work_otis++;
    return 1;

}
