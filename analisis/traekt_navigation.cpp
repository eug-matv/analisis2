//---------------------------------------------------------------------------


#pragma hdrstop

#include "traekt_navigation.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)



//��������� ������������� �������
//��������� ������� �������, ������� �� ��������
long OBZORY_1::tnGetFirstNotPrivyazan(
                                int isPonly,
                                long *nomer, long *indx)
{
  long  smesh;
  smesh=tnGetFirstMainOtshet(0);
  while(smesh>=0)
  {
        if((o_b_1[smesh%362][smesh/362].Status&3)==2)
        {
                if(!isPonly)
                {
                        break;
                }
        }else{
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                        {
                                if(!isPonly)
                                {
                                        break;
                                }
                        }else{
                                break;
                        }
                }
        }
        smesh=tnGetNextMainOtshet(smesh);
  };

   if(smesh>=0)
   {

        tpNextNP=tnGetNextMainOtshet(smesh);
        tpPrevNP=tnGetPrevMainOtshet(smesh);
        if(nomer)(*nomer)=smesh%362;
        if(indx)(*indx)=smesh/362;
   }
   return smesh;
}


long OBZORY_1::tnGetLastNotPrivyazan(
                                int isPonly,
                                long *nomer, long *indx)
{
  long  smesh;
  smesh=tnGetLastMainOtshet(0);
  while(smesh>=0)
  {
        if((o_b_1[smesh%362][smesh/362].Status&3)==2)
        {
                if(!isPonly)
                {
                        break;
                }
        }else{
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                        {
                                if(!isPonly)
                                {
                                        break;
                                }
                        }else{
                                break;
                        }
                }
        }
        smesh=tnGetPrevMainOtshet(smesh);
  };

   if(smesh>=0)
   {
          tpNextNP=tnGetNextMainOtshet(smesh);
          tpPrevNP=tnGetPrevMainOtshet(smesh);
          if(nomer)(*nomer)=smesh%362;
          if(indx)(*indx)=smesh/362;
   }
   return smesh;

}

//��������� ���������� �������, ������� �� ��������
long OBZORY_1::tnGetNextNotPrivyazan(int isPonly, long *nomer, long *indx)
{
  long  smesh;
  smesh=tpNextNP;
   while(smesh>=0)
  {
        if((o_b_1[smesh%362][smesh/362].Status&3)==2)
        {
                if(!isPonly)
                {
                        break;
                }
        }else{
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                        {
                                if(!isPonly)
                                {
                                        break;
                                }
                        }else{
                                break;
                        }
                }
        }
        smesh=tnGetNextMainOtshet(smesh);
  };


   if(smesh>=0)
   {
          tpNextNP=tnGetNextMainOtshet(smesh);
          tpPrevNP=tnGetPrevMainOtshet(smesh);
          if(nomer)(*nomer)=smesh%362;
          if(indx)(*indx)=smesh/362;
   }
   return smesh;

}

//��������� ���������� �������, ������� �� ��������
long OBZORY_1::tnGetPrevNotPrivyazan(int isPonly, long *nomer, long *indx)
{
  long  smesh;
  smesh=tpPrevNP;
  while(smesh>=0)
  {
        if((o_b_1[smesh%362][smesh/362].Status&3)==2)
        {
                if(!isPonly)
                {
                        break;
                }
        }else{
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                        {
                                if(!isPonly)
                                {
                                        break;
                                }
                        }else{
                                break;
                        }
                }
        }
        smesh=tnGetPrevMainOtshet(smesh);
  };

   if(smesh>=0)
   {

          tpNextNP=tnGetNextMainOtshet(smesh);
          tpPrevNP=tnGetPrevMainOtshet(smesh);
          if(nomer)(*nomer)=smesh%362;
          if(indx)(*indx)=smesh/362;
   }
   return smesh;
}

//��������� ���������� �������, ������� �� ��������
long OBZORY_1::tnGetNextMainOtshet(long _smesh,
                                   long *nomer, long *indx)
{
     long smesh;
     smesh=o_b_1[_smesh%362][_smesh/362].Next;
     while(smesh>=0)
     {
          if((o_b_1[smesh%362][smesh/362].Status&3)==2)
          {
                break;
          }
          if((o_b_1[smesh%362][smesh/362].Status&3)==1||
             (o_b_1[smesh%362][smesh/362].Status&3)==3)
          {
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        break;
                }
          }
          smesh=o_b_1[smesh%362][smesh/362].Next;
     }

     if(smesh>=0)
     {
          if(nomer)(*nomer)=smesh%362;
          if(indx)(*indx)=smesh/362;
     }
     return smesh;
}

//��������� ����������� �������, ������� �� ��������
  long OBZORY_1::tnGetPrevMainOtshet(long _smesh,
        long *nomer, long *indx)
{
    long smesh;
    smesh=o_b_1[_smesh%362][_smesh/362].Prev;
     while(smesh>=0)
     {
          if((o_b_1[smesh%362][smesh/362].Status&3)==2)
          {
                break;
          }
          if((o_b_1[smesh%362][smesh/362].Status&3)==1||
             (o_b_1[smesh%362][smesh/362].Status&3)==3)
          {
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        break;
                }
          }
          smesh=o_b_1[smesh%362][smesh/362].Prev;
     }

     if(smesh>=0)
     {
          if(nomer)(*nomer)=smesh%362;
          if(indx)(*indx)=smesh/362;
     }
     return smesh;

}

//��������� ������� ������� ������,
long OBZORY_1::tnGetFirstMainOtshet(int n_trass,
                        long *nomer, long *indx)
{
     long smesh;
     if(n_trass<0||n_trass>=n_of_otis)return 0;

     smesh=otis[n_trass].lFirstIndex;
     while(smesh>=0)
     {                               
          if((o_b_1[smesh%362][smesh/362].Status&3)==2)
          {
                break;
          }
          if((o_b_1[smesh%362][smesh/362].Status&3)==1||
             (o_b_1[smesh%362][smesh/362].Status&3)==3)
          {
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        break;
                }
          }
          smesh=o_b_1[smesh%362][smesh/362].Next;
     }

     if(smesh>=0)
     {
          if(nomer)(*nomer)=smesh%362;
          if(indx)(*indx)=smesh/362;
     }
     return smesh;
}



//��������� ������� ������� ������, ������� �� ���������
long OBZORY_1::tnGetLastMainOtshet(int n_trass,
                        long *nomer, long *indx)
{
     long smesh;
     if(n_trass<0||n_trass>=n_of_otis)return 0;

     smesh=otis[n_trass].lLastIndex;
     while(smesh>=0)
     {
          if((o_b_1[smesh%362][smesh/362].Status&3)==2)
          {
                break;
          }
          if((o_b_1[smesh%362][smesh/362].Status&3)==1||
             (o_b_1[smesh%362][smesh/362].Status&3)==3)
          {
                if(o_b_1[smesh%362][smesh/362].ParnOtschet1<0&&
                   o_b_1[smesh%362][smesh/362].ParnOtschet2<0)
                {
                        break;
                }
          }
          smesh=o_b_1[smesh%362][smesh/362].Prev;
     }

     if(smesh>=0)
     {
          if(nomer)(*nomer)=smesh%362;
          if(indx)(*indx)=smesh/362;
     }
     return smesh;
}




//����� ��������� � ������ ������� ������� ������ trass_n
long OBZORY_1::tnGetMoreAndNearestOtshetTrassy(long smesh, int n_trass)
{
      long smesh1;
      smesh1=o_b_1[smesh%362][smesh/362].AbsNext;
      while(smesh1>=0)
      {
                if((smesh1%362)==0)
                {
                        smesh1=o_b_1[smesh1%362][smesh1/362].AbsNext;
                        continue;
                }
                if(o_b_1[smesh1%362][smesh1/362].IndxBrt==n_trass)
                {
                        if((o_b_1[smesh1%362][smesh1/362].Status%3)==2||
                            o_b_1[smesh1%362][smesh1/362].ParnOtschet1<0||
                            o_b_1[smesh1%362][smesh1/362].ParnOtschet2<0)
                       {
                                break;
                       }
                }
                smesh1=o_b_1[smesh1%362][smesh1/362].AbsNext;
                continue;
      }
      return smesh1;
}


//����� ��������� � ������ ������� ������� ������ trass_n
long OBZORY_1::tnGetLessAndNearestOtshetTrassy(long smesh, int n_trass)
{
      long smesh1;
      smesh1=o_b_1[smesh%362][smesh/362].AbsPrev;
      while(smesh1>=0)
      {
                if((smesh1%362)==0)
                {
                        smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                        continue;
                }
                if(o_b_1[smesh1%362][smesh1/362].IndxBrt==n_trass)
                {
                        if((o_b_1[smesh1%362][smesh1/362].Status%3)==2||
                            o_b_1[smesh1%362][smesh1/362].ParnOtschet1<0||
                            o_b_1[smesh1%362][smesh1/362].ParnOtschet2<0)
                       {
                                break;
                       }
                }
                smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                continue;
      }
      return smesh1;
}


long OBZORY_1::tnGetFirstOtschetKanalAndRejim(int iKanal, //1 - ������ �����, ����� ������ �����
                                      int iRejim, //����� 1 - ���, 2 - RBS
                                      int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        )
{
        long i,j;
        if(iKanal<=0||iKanal>2)return (-1);
        i=FirstAbs;
        if(iKanal==1)
        {
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }
                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }

                if((o_b_1[i%362][i/362].Status&3)==1)
                {
                      return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if(o_b_1[i%362][i/362].ParnOtschet1<0&&
                           o_b_1[i%362][i/362].ParnOtschet2<0)
                        {
                                return i;
                        }
                        else if((j=o_b_1[i%362][i/362].ParnOtschet2)>=0)
                        {
                            if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                            {
                                return i;
                            }
                        }
                }
                i=o_b_1[i%362][i/362].AbsNext;
            };
        }else{
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }

                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }


                if(!(o_b_1[i%362][i/362].Status&KANAL_S_STATUS))
                {
                        i=o_b_1[i%362][i/362].AbsNext;
                        continue;
                }

                if((iRejim==1&&
                       !(o_b_1[i%362][i/362].Status&REJIM_UVD_STATUS))||
                   (iRejim==2&&
                       !(o_b_1[i%362][i/362].Status&REJIM_RBS_STATUS)))
                {
                        i=o_b_1[i%362][i/362].AbsNext;
                        continue;
                }


                if((o_b_1[i%362][i/362].Status&3)==2)
                {
                    return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if((j=o_b_1[i%362][i/362].ParnOtschet1)>=0)
                        {
                           if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
                       (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                           {
                                return i;
                           }
                        }else{
                           return i;
                        }
                }

                i=o_b_1[i%362][i/362].AbsNext;
            };
        }
        return (-1);
}

long OBZORY_1::tnGetLastOtschetKanalAndRejim(int iKanal, //1 - ������ �����, ����� ������ �����
                                      int iRejim, //����� 1 - ���, 2 - RBS
                                      int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        )
{
        long i,j;
        if(iKanal<=0||iKanal>2)return (-1);
        i=LastAbs;
        if(iKanal==1)
        {
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }

                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }
                if((o_b_1[i%362][i/362].Status&3)==1)
                {
                      return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if(o_b_1[i%362][i/362].ParnOtschet1<0&&
                           o_b_1[i%362][i/362].ParnOtschet2<0)
                        {
                                return i;
                        }
                        else if((j=o_b_1[i%362][i/362].ParnOtschet2)>=0)
                        {
                            if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
                                (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                            {
                                return i;
                            }
                        }

                }
                i=o_b_1[i%362][i/362].AbsPrev;
            };
        }else{
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }

                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }


                if(!(o_b_1[i%362][i/362].Status&KANAL_S_STATUS))
                {
                        i=o_b_1[i%362][i/362].AbsPrev;
                        continue;
                }

                if((iRejim==1&&
                       !(o_b_1[i%362][i/362].Status&REJIM_UVD_STATUS))||
                   (iRejim==2&&
                       !(o_b_1[i%362][i/362].Status&REJIM_RBS_STATUS)))
                {
                        i=o_b_1[i%362][i/362].AbsPrev;
                        continue;
                }


                if((o_b_1[i%362][i/362].Status&3)==2)
                {
                    return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if((j=o_b_1[i%362][i/362].ParnOtschet1)>=0)
                        {
                           if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
                       (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                           {
                                return i;
                           }
                        }else{
                           return i;
                        }
                }
                i=o_b_1[i%362][i/362].AbsPrev;
            };
        }
        return (-1);
}

long OBZORY_1::tnGetNextOtschetKanalAndRejim(
                                        long smesh, //�������� �������
                                        int iKanal, //1 - ������ �����, ����� ������ �����
                                        int iRejim,  //����� 1 - ���, 2 - RBS
                                        int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        )
{
   long i,j;
   if(smesh<0)return (-1);
   i=o_b_1[smesh%362][smesh/362].AbsNext;
   if(iKanal==1)
   {
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }

                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }

                if((o_b_1[i%362][i/362].Status&3)==1)
                {
                      return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if(o_b_1[i%362][i/362].ParnOtschet1<0&&
                           o_b_1[i%362][i/362].ParnOtschet2<0)
                        {
                                return i;
                        }
                        else if((j=o_b_1[i%362][i/362].ParnOtschet2)>=0)
                        {
                            if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                            {
                                return i;
                            }
                        }

                }
                i=o_b_1[i%362][i/362].AbsNext;
            };
   }else{
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }

                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsNext;
                     continue;
                }
                if(!(o_b_1[i%362][i/362].Status&KANAL_S_STATUS))
                {
                        i=o_b_1[i%362][i/362].AbsNext;
                        continue;
                }

                if((iRejim==1&&
                       !(o_b_1[i%362][i/362].Status&REJIM_UVD_STATUS))||
                   (iRejim==2&&
                       !(o_b_1[i%362][i/362].Status&REJIM_RBS_STATUS)))
                {
                        i=o_b_1[i%362][i/362].AbsNext;
                        continue;
                }


                if((o_b_1[i%362][i/362].Status&3)==2)
                {
                    return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if((j=o_b_1[i%362][i/362].ParnOtschet1)>=0)
                        {
                           if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
                       (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                           {
                                return i;
                           }
                        }else{
                           return i;
                        }
                }
                i=o_b_1[i%362][i/362].AbsNext;
            };
   }
   return (-1);
}


long OBZORY_1::tnGetPrevOtschetKanalAndRejim(
                                        long smesh, //�������� �������
                                        int iKanal, //1 - ������ �����, ����� ������ �����
                                        int iRejim,  //����� 1 - ���, 2 - RBS
                                        int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        )
{
   long i,j;
   if(smesh<0)return (-1);
   i=o_b_1[smesh%362][smesh/362].AbsPrev;
   if(iKanal==1)
   {
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }
                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }


                if((o_b_1[i%362][i/362].Status&3)==1)
                {
                      return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if(o_b_1[i%362][i/362].ParnOtschet1<0&&
                           o_b_1[i%362][i/362].ParnOtschet2<0)
                        {
                                return i;
                        }
                        else if((j=o_b_1[i%362][i/362].ParnOtschet2)>=0)
                        {
                            if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                            {
                                return i;
                            }
                        }

                }
                i=o_b_1[i%362][i/362].AbsPrev;
            };
   }else{
            while(i>=0)
            {
                if((i%362)==360)
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }

                if((iStatusVis==1&&!(o_b_1[i%362][i/362].Status&VISIBLE_STATUS))||
       (iStatusVis==2&&!(o_b_1[i%362][i/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                {
                     i=o_b_1[i%362][i/362].AbsPrev;
                     continue;
                }


                if(!(o_b_1[i%362][i/362].Status&KANAL_S_STATUS))
                {
                        i=o_b_1[i%362][i/362].AbsPrev;
                        continue;
                }

                if((iRejim==1&&
                       !(o_b_1[i%362][i/362].Status&REJIM_UVD_STATUS))||
                   (iRejim==2&&
                       !(o_b_1[i%362][i/362].Status&REJIM_RBS_STATUS)))
                {
                        i=o_b_1[i%362][i/362].AbsPrev;
                        continue;
                }


                if((o_b_1[i%362][i/362].Status&3)==2)
                {
                    return i;
                }
                if((o_b_1[i%362][i/362].Status&3)==3)
                {
                        if((j=o_b_1[i%362][i/362].ParnOtschet1)>=0)
                        {
                           if((iStatusVis==1&&!(o_b_1[j%362][j/362].Status&VISIBLE_STATUS))||
                       (iStatusVis==2&&!(o_b_1[j%362][j/362].Status&V_OBLASTI_VYDELENIA_STATUS)))
                           {
                                return i;
                           }
                        }else{
                           return i;
                        }
                }
                i=o_b_1[i%362][i/362].AbsPrev;
            };
   }
   return (-1);
}


