//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "AnalisisMain.h"

//---------------------------------------------------------------------------
extern AnsiString PutKProgramme;
extern AnsiString NastroikiCfg;
extern AnsiString PutKTemp;
extern AnsiString CurPathCfg;
extern AnsiString HelpFileName;
int AnalisisMain()
{
  int Ret;
  char CurDir[2000];


  GetCurrentDirectory(1999,CurDir);
  PutKProgramme=CurDir;
  if(PutKProgramme[PutKProgramme.Length()]=='\\')
  {
    PutKProgramme=PutKProgramme.SubString(1,PutKProgramme.Length()-1);
  }


  Ret=GetTempPath(1999,CurDir);
  //Возвращаемое значение
  if(Ret==0)
  {
    MessageBox(NULL, "Not found the path to the temporary file folder", "Error!",MB_OK);
  }

  PutKTemp=CurDir;
  if(PutKTemp[PutKTemp.Length()]=='\\')
  {
    PutKTemp=PutKTemp.SubString(1,PutKTemp.Length()-1);
  }

  HelpFileName=PutKProgramme+String("\\help_html\\Analisis.chm");
  if(!FileExists(HelpFileName))
  {
     AnsiString ErrMes=String("No found file: ")+HelpFileName;
     MessageBox(NULL,ErrMes.c_str(),"Error!",MB_OK);
  }



  NastroikiCfg=PutKProgramme+"\\Nastroiki.ini";
  CurPathCfg=PutKProgramme+"\\CurPath.cfg";

        return 1;
}
//---------------------------------------------------------------------------
