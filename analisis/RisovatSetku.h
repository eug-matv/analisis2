//---------------------------------------------------------------------------
/*� ������ ������ ����������� ��������� ��������� ����� ��� ������� ������������
��������*/

#ifndef RisovatSetkuH
#define RisovatSetkuH
//---------------------------------------------------------------------------
extern TColor SetkaOblastCol;  //���� �������
extern TColor SetkaKolca1Col;  //���� ����� �������� (����� 50 ��)
extern TColor SetkaKolca2Col;  //����� �������������
extern TColor SetkaRad90Col;   //����� ���.����� 90 � 180 ��������
extern TColor SetkaRad30Col;   //����� ���.����� 30 � �.�.



int VyvodSetkaANLSS(
                TImage *Image, //������ ���� TImage
                double Left, double Bottom,    //���������� ������� ������ ���� ������ ���
                double Right, double Top //���������� ������� �������� ����
               );

int VyvodSetkaPrinter(
      TCanvas *Canvas,       //
      int Width,int Height,  //������� ��������
      int Xo1, int Yo1,         //������ �������� ������
      long Left, long Bottom,    //���������� ������� ������ ���� ������ ���
      long Right, long Top //���������� ������� �������� ����
      );


#endif
