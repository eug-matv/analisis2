//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <alloc.h>
#include <stdlib.h>

#include "NotPriamZone.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))

static OBLAST *Oblast=NULL;



OBLAST::OBLAST()
{
  Otrezki=NULL;
  N_of_Otrezki=0;
  PoslX=-2000000000L;
  PoslY=-2000000000L;
  Dodelan=false;
}


int OBLAST::AddPixel(double X, double Y)
{
  if(Dodelan)return 0;
  if(PoslX==-2000000000L&&
     PoslY==-2000000000L)
  {
    PervX=X;
    PervY=Y;
    PoslX=X;
    PoslY=Y;
    MinX=MaxX=X;
    MinY=MaxY=Y;
    return 1;
  }
  Otrezki=(OTREZOK**)realloc(Otrezki, (N_of_Otrezki+1)*sizeof(OTREZOK));
  Otrezki[N_of_Otrezki]=new OTREZOK(PoslX,PoslY,X,Y);
  MinX=MIN(MinX,X);
  MaxX=MAX(MaxX,X);
  MinY=MIN(MinY,Y);
  MaxY=MAX(MaxY,Y);


  N_of_Otrezki++;
  PoslX=X;
  PoslY=Y;
  return 1;
}


int SortOtrezki(const void *a, const void *b)
{

  OTREZOK **o1,**o2;
  o1=(OTREZOK**)a;
  o2=(OTREZOK**)b;
  if((*o1)->X1>(*o2)->X1)return 1;
  if((*o1)->X1<(*o2)->X1)return (-1);
  return 0;
}


int OBLAST::Dodelat(void)
{
   if(!N_of_Otrezki||Dodelan)return 0;

 //�������� ��������� �������
  Otrezki=(OTREZOK**)realloc(Otrezki, (N_of_Otrezki+1)*sizeof(OTREZOK));
  Otrezki[N_of_Otrezki]=new OTREZOK(PoslX,PoslY,PervX,PervY);
  N_of_Otrezki++;
  Dodelan=true;
  Priam.Xl=MinX;
  Priam.Xr=MaxX;
  Priam.Yt=MinY;
  Priam.Yb=MaxY;
  qsort(Otrezki,N_of_Otrezki,sizeof(OTREZOK*),SortOtrezki);
  return 1;
}


//�������������� ����� �������
int OBLAST::IsVnutri(double X, double Y)
{
  int Ret,i;
  int NPlus=0;
//��������, � ����������� �� ����� ��������������
//  Ret=Priam.HasPixel(X,Y);
//  if(!Ret)return 0;


//������ �������� �������������� �����  ������
  for(i=0; i<N_of_Otrezki;i++)
  {
     Ret=Otrezki[i]->HorPolojenie(X);
     if(Ret!=0)continue;

     Ret=Otrezki[i]->VerPolojenie(X,Y);
     if(Ret==0)return 1;
     if(Ret<-100000)continue;
     if(Ret>0)
     {
       NPlus++;
     }

  }
  if(NPlus%2==1)return 1;
  return 0;
}




OBLAST::~OBLAST()
{
  int i;
  for(i=0;i<N_of_Otrezki;i++)
  {
    delete Otrezki[i];
  }
  free(Otrezki);
}




//�������� ��������������� �������
void __stdcall MakeNotPriamOblast(void)
{
/*
  if(Oblast)
  {
    return 0;
  }
  */
  FreeNotPriamOblast();
  Oblast=new OBLAST;
}


//���������� ��������
void __stdcall AddTochkaVNotPriamOblast(double X, double Y)
{
   Oblast->AddPixel(X,Y);
}

//���������� ������ ���������
void __stdcall DodelatNotPriamOblast(void)
{

  Oblast->Dodelat();

}


//������ ��������������� �������
long __stdcall IsVnutriNotPriamOblast(double X, double Y)
{
  if(!Oblast)return 0;
  return (long)(Oblast->IsVnutri(X,Y));
}


void __stdcall FreeNotPriamOblast(void)
{
  if(!Oblast)return ;
  delete Oblast;
  Oblast=NULL;
}



