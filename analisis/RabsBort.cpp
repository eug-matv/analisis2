//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "RabsBort.h"
#include "tools.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)


bool StopBinInTxt;

extern long GlobalTempObzora;    //��������� ������������ ���������� � ������
								 //main






//������� �� ���� |
int SchitZ(FILE *fp)
{
 char N=' ';
 int Ret;
 while(N!='|')
 {
  Ret=fscanf(fp,"%c",&N);
  if(Ret!=1)return 0;
 }
return 1;
}


int SchKK(FILE *fp,char KK[])
{
 char N=' ';
 int k=0;
 int Ret;
 while(N!='|')
 {
  Ret=fscanf(fp, "%c",&N);
  if(Ret!=1)
  {
   return 0;
  }
  if(k>=4)continue;
  KK[k]=N;
  k++;
 }
 return 1;
}



int SchitD4(FILE *fp,char P[])
{
 char N;
 P[0]=P[1]=P[2]=P[3]=' ';
 N=(char)fgetc(fp);
 if(N=='|'||N==EOF)return 0;
 P[0]=N;

 N=(char)fgetc(fp);
 if(N==EOF)return 0;
 if(N=='|')return 1;
 P[1]=N;

 N=(char)fgetc(fp);
 if(N==EOF)return 0;
 if(N=='|')return 1;
 P[2]=N;


 N=(char)fgetc(fp);
 if(N==EOF)return 0;
 if(N=='|')return 1;
 P[3]=N;

 while(N!='|')
 {
  N=(char)fgetc(fp);
  if(N==EOF)
  {
   return 0;
  }
 }
 return 1;
}


int SchitD2(FILE *fp,char P[])
{
 char N;
 P[0]=P[1]=' ';
 N=(char)fgetc(fp);
 if(N=='|'||N==EOF)return 0;
 P[0]=N;

 N=(char)fgetc(fp);
 if(N==EOF)return 0;
 if(N=='|')return 1;
 P[1]=N;

 while(N!='|')
 {
  N=(char)fgetc(fp);
  if(N==EOF)
  {
   return 0;
  }
 }
 return 1;
}

int SchitD1(FILE *fp, char &P)
{
 char N=' ';
 int Naid=0;
 while(N!='|')
 {
  N=(char)fgetc(fp);
  if(N==EOF)return 0;
  if(N!=' '&&N!='\t'&&N!='|')
  {
   if(!Naid)
   {
    P=N;
    Naid=1;
   }
  }
 }

 if(!Naid)P=' ';
 return 1;
}


int SchitatNom(FILE *fp, char Nom[8])
{
 char b[2];
 b[1]=0;
 AnsiString Nom1="";
 while(b[0]!='|')
 {
  b[0]=(char)fgetc(fp);
  if(b[0]==EOF)return 0;
  if(b[0]!='|')
  {
   Nom1=Nom1+AnsiString(b);
  }
 }
 Nom1=Nom1.Trim();
 //��������� ������
 strcpy(Nom,Nom1.c_str());
 return 1;
}

int SchitatTime(FILE *fp, int &Chas, int &Min, double &Sec)
{
 char d1,d2;
 int Ret;

 Ret=fscanf(fp,"%d%c%d%c%lf",&Chas,&d1,&Min,&d2,&Sec);
 if(Ret<5)return 0;
 if(d1!=':'||d2!=':')return 0;
 d1=' ';
 while(d1!='\n'&&d1!=EOF)
 {
  d1=(char)fgetc(fp);
 }
 return 1;
}

int SchitDouble(FILE *fp, double &DP)
{
 int Ret;
 char N=' ';
 Ret=fscanf(fp,"%lf",&DP);
 if(Ret!=1)
 {
  return 0;
 }
 while(N!='|'&&N!=EOF)
 {
  N=(char)fgetc(fp);
 }
 return 1;
}


int SchitInt(FILE *fp, int &IP)
{
 int Ret;
 char N=' ';
 Ret=fscanf(fp,"%d",&IP);
 if(Ret!=1)
 {
  return 0;
 }
 while(N!='|'&&N!=EOF)
 {
  N=(char)fgetc(fp);
 }
 return 1;
}





//���������� ������� �� �����
//����������
/*
int LoadDan(FILE *fp, DANN *Dan)
{
//1
 //������� ������ ��������
 long Smesh;
 char D[1000],*R,d1,d2;
 AnsiString DA;
 int Ret,Dlina,i,k=0;
 Smesh=ftell(fp);
 Ret=SchitInt(fp,(Dan->T_I));
 if(Ret!=1)
 {
  //�������� ��� ����������� ������
  R=fgets(D, 999, fp);
  if(R==NULL)return 0;
  Dlina=strlen(D);
  if(D[Dlina-1]=='\n')
  {
   D[Dlina-1]=0;
   Dlina--;
  }
  DA=AnsiString(D).Trim();

 //������ ���� �������� �����
  Dlina=DA.Length();
  if(Dlina==0)
  {
   return 0;
  }
  if(DA[Dlina]=='|')Dlina--;
  for(i=Dlina;i>=1;i--)
  {
   if(DA[i]=='|')
   {
    k=i+1;
    break;
   }
  }
  if(k==0)
  {
   return 0;
  }
  //������� ������
  DA=DA.SubString(k,Dlina-k+1);
 //������ ��������� �������� �� ������ DA �����
  Ret=sscanf(DA.c_str(),"%d%c%d%c%lf",&(Dan->H),&d1,&(Dan->M),&d2,&(Dan->S));
  if(Ret!=5)return 0;

 // ���������, ��� ��� ������ ����� �����
  Dan->P='N';
  Dan->T_I=45;
  Dan->Nom1=0;
  Dan->T_K[0]=Dan->T_K[1]=' ';
  for(i=0;i<4;i++)Dan->P3[i]=0;
  Dan->P4=' ';
  Dan->Nom2=0;
  Dan->T_Bort[0]=Dan->T_Bort[1]=' ';
  Dan->Nom=0;

  ZeroMemory(Dan->NomBort,8);
  Dan->Vysota=0;
  Dan->X=Dan->Y=0;
  Dan->Asimut=Dan->Dalnost=0.0;
  Dan->Smesh=Smesh;
  return 1;
 }

//2
 //������� ������ ������
 Ret=SchitD1(fp,Dan->P);
 if(Ret!=1)
 {
  return 0;
 }


 if(Dan->P=='N') //�����
 {
 //���� ������ �����

//������ ����� ��������� �����
  d1='|';
  while(d1!='\n'&&!feof(fp))
  {
   if(d1=='|')
   {
    Ret=SchitatTime(fp,Dan->H,Dan->M,Dan->S);  //��������� ������� �����
    if(Ret==1)
    {
     Dan->Nom1=0;
     Dan->T_K[0]=Dan->T_K[1]=' ';
     for(i=0;i<4;i++)Dan->P3[i]=0;
     Dan->P4=' ';
     Dan->Nom2=0;
     Dan->T_Bort[0]=Dan->T_Bort[1]=' ';
     Dan->Nom=0;
     ZeroMemory(Dan->NomBort,8);
     Dan->Vysota=0;
     Dan->X=Dan->Y=0;
     Dan->Asimut=Dan->Dalnost=0.0;
     Dan->Smesh=Smesh;
     return 1;
    }
   }
   d1=(char)fgetc(fp);
  }
 }






//3
 Ret=SchitInt(fp,Dan->Nom1);
 if(Ret!=1)
 {
  return 0;
 }

//4
//������� ��� �����
 Ret=SchitD2(fp,Dan->T_K);
 if(Ret!=1)
 {
  return 0;
 }

//5
 Ret=SchitD4(fp,Dan->P3);
 if(Ret!=1)
 {
  return 0;
 }

//6
 Ret=SchitD1(fp,Dan->P4);
 if(Ret!=1)
 {
  return 0;
 }

//7
 Ret=SchitInt(fp, Dan->Nom2);
 if(Ret!=1)
 {
  return 0;
 }

//8
 Ret=SchitD2(fp,Dan->T_Bort);
 if(Ret!=1)
 {
  return 0;
 }

//9
 Ret=SchitInt(fp,Dan->Nom);
 if(Ret!=1)
 {
  return 0;
 }

//10
 Ret=SchitatNom(fp,Dan->NomBort);
 if(Ret!=1)
 {
  return 0;
 }

//11
 Ret=SchitInt(fp,Dan->Vysota);
 if(Ret!=1)
 {
  return 0;
 }

//12
 Ret=SchitInt(fp,Dan->X);
 if(Ret!=1)
 {
  return 0;
 }

 Ret=SchitInt(fp,Dan->Y);
 if(Ret!=1)
 {
  return 0;
 }


 Ret=SchitDouble(fp,Dan->Asimut);
 if(Ret!=1)
 {
  return 0;
 }

 Ret=SchitDouble(fp,Dan->Dalnost);
 if(Ret!=1)
 {
  return 0;
 }


//������ ����� ��������� �����
 d1='|';
 while(d1!='\n'&&!feof(fp))
 {
  if(d1=='|')
  {
   Ret=SchitatTime(fp,Dan->H,Dan->M,Dan->S);  //��������� ������� �����
   Dan->Smesh=Smesh;
   if(Ret==1)
   {
    return 1;
   }
  }
  d1=(char)fgetc(fp);
 }

 return 0;
}

*/
/*����������: 1, ���� ������, 0 - ���� ������������ ������
              2, ���� �����
*/
int LoadDan(FILE *fp, DANN *Dan)
{
 long Smesh;
 int Ret;
 char D[1000],*R;
 Smesh=ftell(fp);
 R=fgets(D, 999, fp);
 if(R==NULL)return 0;
 Dan->Smesh=Smesh;
 Ret=GetDannFromStroka(D,Dan);
 return Ret;
}



int SaveDannPlot(DANN Dan,FILE *fpOut)
{

 int Dlina;
 int i;
 fprintf(fpOut," %2d |",Dan.T_I);
 fprintf(fpOut," %c |",Dan.P);
 fprintf(fpOut," %2d |",Dan.Nom1);
 fprintf(fpOut,"%c%c|",Dan.T_K[0],Dan.T_K[1]);
 fprintf(fpOut,"%c%c%c%c|",Dan.P3[0],Dan.P3[1],Dan.P3[2],Dan.P3[3]);
 fprintf(fpOut," %c|",Dan.OtnVys);
 fprintf(fpOut," %d |",Dan.Nom2);
 fprintf(fpOut,"%c%c|",Dan.T_Bort[0],Dan.T_Bort[1]);

 if(Dan.Nom>=10)
 {
  fprintf(fpOut," %d |",Dan.Nom);
 }else{
  fprintf(fpOut,"  %d |",Dan.Nom);
 }
 Dlina=strlen(Dan.NomBort);

 for(i=0;i<7-Dlina;i++)
 {
  fprintf(fpOut," ");
 }

 for(i=0;i<Dlina;i++)
 {
  fprintf(fpOut,"%c",Dan.NomBort[i]);
 }
 fprintf(fpOut,"|");

 fprintf(fpOut,"%7d|",Dan.Vysota);
 fprintf(fpOut,"%7d|",Dan.X);
 fprintf(fpOut,"%7d|",Dan.Y);
 fprintf(fpOut," %6.2lf |",Dan.Asimut);
 fprintf(fpOut," %6.2lf |",Dan.Dalnost);
 fprintf(fpOut," %02d:%02d:%06.3lf |\n",Dan.H,Dan.M,Dan.S);
 return 1;
}



AnsiString DanToStr(DANN Dan)
{
 char Strka[200];
 AnsiString AS;

 sprintf(Strka," %2d | %c | %2d |%c%c|%c%c%c%c| %c| %d |%c%c|",
                Dan.T_I,
                      Dan.P,
                           Dan.Nom1,
                                 Dan.T_K[0],Dan.T_K[1],
                                       Dan.P3[0],Dan.P3[1],Dan.P3[2],Dan.P3[3],
                                              Dan.OtnVys,
                                                    Dan.Nom2,
                                                        Dan.T_Bort[0],Dan.T_Bort[1]);
 AS=String(Strka);


 if(Dan.Nom>=10)
 {
  sprintf(Strka," %d |",Dan.Nom);
 }else{
  sprintf(Strka,"  %d |",Dan.Nom);
 }

 AS=AS+String(Strka);
 sprintf(Strka,"%7s|",Dan.NomBort);
 AS=AS+String(Strka);

 sprintf(Strka,"%7d|",Dan.Vysota);
 AS=AS+String(Strka);

 sprintf(Strka,"%7d|",Dan.X);
 AS=AS+String(Strka);

 sprintf(Strka,"%7d|",Dan.Y);
 AS=AS+String(Strka);

 sprintf(Strka," %6.2lf |",Dan.Asimut);
 AS=AS+String(Strka);

 sprintf(Strka," %6.2lf |",Dan.Dalnost);
 AS=AS+String(Strka);

 sprintf(Strka," %02d:%02d:%06.3lf |",Dan.H,Dan.M,Dan.S);
 AS=AS+String(Strka);
 return AS;
}



int SortTime(FILE *fpSch, //������ ��������
             FILE *fpRes) //���� ��������
{
 int N_of_D,i;
 int MaxNofD=1000;
 long TSmesh=0;
 int Ret;
 char Stroka[999];
 fseek(fpSch,0,SEEK_SET);
 OFFSET_TIME *TekOT,*Prom;
 DANN D;
 N_of_D=0;
 TekOT=new OFFSET_TIME [MaxNofD];
 while(!feof(fpSch))
 {
  TSmesh=ftell(fpSch);
  Ret=LoadDan(fpSch,&D);
  if(Ret==1)
  {
   TekOT[N_of_D].offset=TSmesh;
   TekOT[N_of_D].H=D.H;
   TekOT[N_of_D].M=D.M;
   TekOT[N_of_D].S=D.S;
   N_of_D++;
   if(N_of_D==MaxNofD)
   {
    MaxNofD+=1000;
    Prom=new OFFSET_TIME [MaxNofD];
   //��������� ������ �� TekOT � Prom
    CopyMemory(Prom,TekOT,N_of_D*sizeof(OFFSET_TIME));
    delete []TekOT;
    TekOT=Prom;
   }
  }
 }



//������ ������ ������ TekOT ���� ������������� ������� �����
 qsort(TekOT,N_of_D,sizeof(OFFSET_TIME),SravnOff_Time);

//������ ��������������� ���� �������
 for(i=0;i<N_of_D;i++)
 {
  fseek(fpSch,TekOT[i].offset,SEEK_SET);
  fgets(Stroka, 999, fpSch);
  fputs(Stroka,fpRes);
 }
 delete []TekOT;
 return 1;
}

//--------------------------------------------------
//��������� ���� ������� �� �������
//���������� ������� � ��������
int SravnenieTime(DANN Dan1, DANN Dan2)
{
 //�������
 int dH,dM,dIS;
 int Sum1;
 dH=Dan1.H-Dan2.H;
 dM=Dan1.M-Dan2.M;
 dIS=(int)floor(Dan1.S)-(int)floor(Dan2.S);
 Sum1=dH*3600+dM*60+dIS;
 if(Sum1==0)
 {
  if(Dan1.S>Dan2.S)Sum1=1;
  else if(Dan1.S<Dan2.S)Sum1=-1;
  else Sum1=0;
 }
 return Sum1;
}
//-----------------------------------------------------
//��������� �� �������� � �� �������

int SravnenieAsimut(DANN Dan1, DANN Dan2)
{
 //������� ����
 int Ret;
 Ret=SravnenieTime(Dan1,Dan2);
 if(Dan1.Asimut>=Dan2.Asimut) //�������� �� ������
 {
  if(Ret<=-2)return -1;
  return 1;
 }else{
  if(Ret>=2)return 1;


  return  -1;
 }


}



//---------------------------------------------------
//��������� �������
int  _cdecl SravnOff_Time(const void *el1, const void *el2)
{

 int Ret;
 OFFSET_TIME *El1,*El2;
 El1=(OFFSET_TIME*)el1;
 El2=(OFFSET_TIME*)el2;

 if(El1->H > El2->H)Ret=1;
 else if(El1->H < El2->H)Ret=-1;
 else Ret=0;
 if(Ret)return Ret;

 if(El1->M > El2->M)Ret=1;
 else if(El1->M < El2->M)Ret=-1;
 else Ret=0;
 if(Ret)return Ret;

 if(El1->S > El2->S)Ret=1;
 else if(El1->S < El2->S)Ret=-1;
 else Ret=0;
 return Ret;


}




/*

//������� ���������� ������ ��� ��������� ����
int LoadDanCS(FILE *fp,
	      DANN *Dan)
{

//��������� ������� ������
 char dl1=' ';
 int k=0,Ret;
//������ �� ���� ������

//������� ��� ����� '|' �� �����
 ZeroMemory(Dan,sizeof(DANN));
 Dan->Smesh=ftell(fp);
 while(!feof(fp)&&dl1!='\n'&&k<3)
 {
  dl1=(char)fgetc(fp);
  if(dl1=='|')
  {
   k++;
  }
  if(dl1=='N')
  {
   goto NORD;
  }
 }
 Ret=SchitD2(fp,Dan->T_K);
 if(Ret!=1)return 0;
 k=0;
 dl1=' ';
 while(!feof(fp)&&dl1!='\n'&&k<5)
 {
  dl1=(char)fgetc(fp);
  if(dl1=='|')
  {
   k++;
  }
 }

//������� ����� �����
 Ret=SchitatNom(fp,Dan->NomBort);
 if(Ret!=1)return 0;

//������� ������
 Ret=SchitInt(fp,Dan->Vysota);
 if(Ret!=1)return 0;
 k=0;
 dl1=' ';


 while(!feof(fp)&&dl1!='\n'&&k<2)
 {
  dl1=(char)fgetc(fp);
  if(dl1=='|')
  {
   k++;
  }
 }

//������� ������
 Ret=SchitDouble(fp,Dan->Asimut);
 if(Ret!=1)return 0;

 Ret=SchitDouble(fp,Dan->Dalnost);
 if(Ret!=1)return 0;

 Ret=SchitatTime(fp,Dan->H,Dan->M,Dan->S);
 if(Ret!=1)return 0;
 Dan->P='P';
 return 1;

 NORD:
//��������� ������� �����
 dl1=' ';
 while(dl1!='\n'&&!feof(fp))
 {
  dl1=(char)fgetc(fp);
  Dan->NomBort[0]=0;
  Dan->Nom1=0;
  Dan->P3[0]=0;
  
  if(dl1=='|')
  {
   Ret=SchitatTime(fp,Dan->H,Dan->M,Dan->S);
   Dan->P='N';
   Dan->T_I=50;
   if(Ret!=1)return 0;
   return 1;
  }


 }
 return 0;
}
*/

/*�������������� ��������� ����� � ���������*/
int BinInTxt(AnsiString BinFile, AnsiString TxtFile)
{
 FILE *fpB,*fpT;
 int Ret,i;
 bool RetB;
 char C1=' ';
 AnsiString VremFileName="Vr",VremFileNameExt;
 i=0;
 do
 {
  VremFileNameExt=VremFileName+IntToStr(i)+String(".txt");
  RetB=FileExists(VremFileNameExt);
  i++;
 }while(RetB);
//������� ���� � �������� ������
 StopBinInTxt=false;
 fpB=fopen(BinFile.c_str(),"rb");
 if(fpB==NULL)return 0;  //������ �������� �����

//������� ���� �� ����
 fpT=fopen(VremFileNameExt.c_str(),"wb");
 if(fpT==NULL)
 {
  fclose(fpB);
  return (-2);
 }

 while(!feof(fpB))
 {
  C1=(char)fgetc(fpB);
  if(C1=='\r')continue;
  if(C1=='\n')
  {
  //�������� ��� �� ���������� ������ ������� �������
   fputc('\r',fpT);
   fputc('\n',fpT);
   if(StopBinInTxt)return -3;
   continue;
  }
  fputc(C1,fpT);
 }
 fclose(fpB);
 fclose(fpT);

//������ ���������� ���� � �����
 Ret=rename(VremFileNameExt.c_str(),TxtFile.c_str());
 if(Ret!=0) //������
 {
  DeleteFile(TxtFile);
  Ret=rename(VremFileNameExt.c_str(),TxtFile.c_str());
  if(Ret!=0)return (-1);
 }
 return 1;
}






//��������� ������ ���������������� ����� � �� 5 ����
//
int GetIntChilso5(char *Bytes)
{
   int K=0,i;
   int Ret=0;
   int znk=1;
   for(i=0;i<5;i++)
   {
      if(K==0)
      {
        if(Bytes[i]==' ')
        {
           continue;
        }
        if(Bytes[i]=='-')
        {
            K=1;
            znk=-1;
            continue;
        }

        if(Bytes[i]>'9'||Bytes[i]<'0')return (-100000);
        K=1;
        Ret=Ret*10+Bytes[i]-0x30;
      }else{
        if(Bytes[i]>'9'||Bytes[i]<'0')return (-100000);
        Ret=Ret*10+Bytes[i]-0x30;
      }
   }
   return Ret*znk;
}


//���������� �������� �����
//���� ����� ����� 1000000, �� ������
int GetIntChislo7(char *Bytes)
{
   int K=0,i;
   int Ret=0;
   int Poryadok=10;
   for(i=6;i>=0;i--)
   {
     if(K==0)
     {
        if(Bytes[i]>'9'||Bytes[i]<'0')
        {
          return 1000000;
        }
        Ret=Bytes[i]-0x30;

        K=1;
     }else if(K==1)
     {
        if(Bytes[i]==' '||Bytes[i]=='+')
        {
           K=2;
           continue;
        }
        if(Bytes[i]=='-')
        {
           K=2;
           Ret=-Ret;
           continue;
        }
        if(Bytes[i]>'9'||Bytes[i]<'0')
        {
          return 1000000;
        }
        Ret+=Poryadok*(Bytes[i]-0x30);
        Poryadok*=10;
     }else{
        if(Bytes[i]!=' ')
        {
          return 1000000;
        }
     }
   }
   return Ret;
}




//���������� ������
//����� ���� ��������� ������ ������
int GetDannFromStroka(char *Stroka,
                      DANN *Dan)
{
  
  int Length,i,K=0;
  int Sdvg=0, TmNach;
  char *RetStrka;
  Length=strlen(Stroka);
  if(Length<102)return 0;

//�������� ������
  if(Stroka[0]=='N'&&Stroka[1]=='O'&&Stroka[2]=='R'&&Stroka[3]=='D'||
     Stroka[1]>='0'&&Stroka[1]<='9'&&(Stroka[2]=='0'||Stroka[2]=='3')&&
     Stroka[6]=='N'&&Stroka[4]=='|')
  {
   //�������� ����� ������
     if(Stroka[2]=='3')
     {
       if(Length<116)return 0;
       TmNach=102; //������ �������
     }else if(Stroka[0]!='N'||Stroka[1]!='O'||Stroka[2]!='R'||Stroka[3]!='D')
     {
       if(Stroka[2]>'9' || Stroka[2]<'0')return 0;
       TmNach=88;  //������ �������
     }else{
       TmNach=88;  //������ �������
     }
     if(Stroka[87]=='|')
     {
       Sdvg=1;
     }else if(Stroka[86]=='|')
     {
       Sdvg=0;
     }else{
       return 0;
     }

//�������� �����
     if(Stroka[TmNach+Sdvg]==' ')
     {
       Stroka[TmNach+Sdvg]='0';
     }
     if(Stroka[TmNach+Sdvg]>'9'||Stroka[TmNach+Sdvg]<'0'||
        Stroka[TmNach+Sdvg+1]>'9'||Stroka[TmNach+Sdvg+1]<'0')
     {
       return 0;
     }else{
       Dan->H=10*(Stroka[TmNach+Sdvg]-0x30)+(Stroka[TmNach+Sdvg+1]-0x30);
       if(Dan->H>=24)return 0;
     }
     if(Stroka[TmNach+Sdvg+2]!=':')return 0;
     if(Stroka[TmNach+Sdvg+3]>'9'||Stroka[TmNach+Sdvg+3]<'0'||
        Stroka[TmNach+Sdvg+4]>'9'||Stroka[TmNach+Sdvg+4]<'0')
     {
       return 0;
     }else{
       Dan->M=10*(Stroka[TmNach+Sdvg+3]-0x30)+(Stroka[TmNach+Sdvg+4]-0x30);
       if(Dan->M>59)return 0;
     }
     if(Stroka[TmNach+Sdvg+5]!=':')return 0;
     Dan->S=strtod(Stroka+(TmNach+Sdvg+6),&RetStrka);
     if(RetStrka[0]!=' '||
        RetStrka[1]!='|'||
        Dan->S>=59.99999999)return 0;

     Dan->P='N';
     Dan->Asimut=360;
     return 2;
  }
  Dan->P=0;


  if(Stroka[2]=='3')
  {
    if(Length<116)return 0;
    TmNach=102; //������ �������
  }else{
    if(Stroka[2]>'9' || Stroka[2]<'0')return 0;
    TmNach=88;  //������ �������
  }
  if(Stroka[1]>'9' || Stroka[1]<'0')return 0;
  Dan->T_I=Stroka[2]-0x30+(Stroka[1]-0x30)*10;

  if(Stroka[4]!='|')return 0;

  if(Stroka[14]=='|')
  {
    Sdvg=1;
  }else if(Stroka[13]=='|')
  {
    Sdvg=0;
  }else{
    return 0;
  }

  if(Stroka[9]=='R'||
     Stroka[10]=='R'||
     Stroka[11]=='R'||
     Stroka[12]=='R'||
     Stroka[29+Sdvg]=='I'||
     Stroka[30+Sdvg]=='I'
  )
  {
        Dan->T_Bort[0]='I';
  }else{
        Dan->T_Bort[0]=' ';
  }
  if(Stroka[9]=='U'||
        Stroka[10]=='U'||
        Stroka[11]=='U'||
        Stroka[12]=='U'||
        Stroka[29+Sdvg]=='U'||
        Stroka[30+Sdvg]=='U'
        )
  {
        Dan->T_Bort[1]='U';
  }else{
        Dan->T_Bort[1]=' ';
  }




//�������� ����� ���������� �� ������
  if(Stroka[22+Sdvg]=='A'||Stroka[23+Sdvg]=='A')
  {
    Dan->OtnVys='A';
  }else{
    Dan->OtnVys=' ';
  }
  if(Stroka[24+Sdvg]!='|')return 0;


//���������� � ����� ������
  if((Stroka[14+Sdvg]=='P')||(Stroka[15+Sdvg]=='P'))
  {
    Dan->T_K[0]='P';
  }else{
    Dan->T_K[0]=' ';
  }

  if((Stroka[14+Sdvg]=='S')||(Stroka[15+Sdvg]=='S'))
  {
    Dan->T_K[1]='S';
  }else{
    Dan->T_K[1]=' ';
  }


  if(Stroka[16+Sdvg]!='|')return 0;

//���������� � �������� �������
  K=0;
  for(i=0;i<5;i++)
  {
    if(Stroka[39+Sdvg+i]==' ')
    {
      K++;
    }else{
      Dan->NomBort[i-K]=Stroka[39+Sdvg+i];
    }
  }
  Dan->NomBort[5-K]=0;
  if(Stroka[44+Sdvg]!='|')return 0;

//���������� � �������

  Dan->Vysota=GetIntChilso5(Stroka+(47+Sdvg));
  if(Dan->Vysota<-50000||Stroka[52+Sdvg]!='|')return 0;

//���������� � X
  Dan->X=GetIntChislo7(Stroka+(53+Sdvg));
  if(Dan->X==1000000||Stroka[60+Sdvg]!='|')return 0;

//���������� � Y
  Dan->Y=GetIntChislo7(Stroka+(61+Sdvg));
  if(Dan->Y==1000000||Stroka[68+Sdvg]!='|')return 0;

  Dan->Asimut=strtod(Stroka+(69+Sdvg),&RetStrka);
  if(!(RetStrka[0]==' '&&
     RetStrka[1]=='|')&&RetStrka[0]!='|'||
     Dan->Asimut>=500.0)return 0;
  if(Stroka[77+Sdvg]!='|')return 0;

  Dan->Dalnost=strtod(Stroka+(78+Sdvg),&RetStrka);
  if(!(RetStrka[0]==' '&&
     RetStrka[1]=='|')&&RetStrka[0]!='|'||
     Dan->Dalnost>=500.0)return 0;
  if(Stroka[86+Sdvg]!='|')return 0;

//������� �����
  if(Stroka[TmNach+Sdvg]==' ')
  {
     Stroka[TmNach+Sdvg]='0';
  }
  if(Stroka[TmNach+Sdvg]>'9'||Stroka[TmNach+Sdvg]<'0'||
     Stroka[TmNach+Sdvg+1]>'9'||Stroka[TmNach+Sdvg+1]<'0')
  {
    return 0;
  }else{
    Dan->H=10*(Stroka[TmNach+Sdvg]-0x30)+(Stroka[TmNach+Sdvg+1]-0x30);
    if(Dan->H>=24)return 0;
  }
  if(Stroka[TmNach+Sdvg+2]!=':')return 0;
  if(Stroka[TmNach+Sdvg+3]>'9'||Stroka[TmNach+Sdvg+3]<'0'||
     Stroka[TmNach+Sdvg+4]>'9'||Stroka[TmNach+Sdvg+4]<'0')
  {
    return 0;
  }else{
    Dan->M=10*(Stroka[TmNach+Sdvg+3]-0x30)+(Stroka[TmNach+Sdvg+4]-0x30);
    if(Dan->M>59)return 0;
  }
  if(Stroka[TmNach+Sdvg+5]!=':')return 0;
  Dan->S=strtod(Stroka+(TmNach+Sdvg+6),&RetStrka);
  if(RetStrka[0]!=' '||
     RetStrka[1]!='|'||
     Dan->S>=60.00000001)return 0;

  if((Dan->T_K[0]=='P'||Dan->T_K[1]=='P')&&(Dan->T_K[0]!='S'&&Dan->T_K[1]!='S'))
  {
     if(Stroka[TmNach+Sdvg+12]==' '&&Stroka[TmNach+Sdvg+13]=='|')
     {
         if(Stroka[TmNach+Sdvg+14]==' ')
         {
                if(sscanf(Stroka+(TmNach+Sdvg+15),"%lf", &(Dan->dfSignalPK))!=1)
                {
                   Dan->dfSignalPK=0.0;
                }
         }
     }else{
         Dan->dfSignalPK=0.0;
     }
  }else{
     Dan->dfSignalPK=0.0;
  }
//� ������ ��������, ������������� �� ��� ������ ����� ������ ����-1�� ��� ��� �����
/*
  if(CS) //����� ��� ������ �������� ���� - ����-1��
  {
//����� ������ ������ ���������� ������
     if(Dan->T_K[0]=='P'&&Dan->T_K[1]!='S')
     {
       if(Stroka[9]=='A'||Stroka[9]=='M'||
          Stroka[10]=='A'||Stroka[10]=='M'||
          Stroka[11]=='A'||Stroka[11]=='M'||
          Stroka[12]=='A'||Stroka[12]=='M')
       {
          return 1;
       }
     }else{
       if(Stroka[9]=='R'||Stroka[9]=='U'||
          Stroka[10]=='R'||Stroka[10]=='U'||
          Stroka[11]=='R'||Stroka[11]=='U'||
          Stroka[12]=='R'||Stroka[12]=='U')
       {
          return 1;
       }
     }

  }else{
     if(Dan->T_K[0]=='P'&&Dan->T_K[1]!='S')
     {
    //   if(Stroka[29+Sdvg]=='P'||Stroka[30+Sdvg]=='P')
    //   {
         return 1;
   //    }
     }else{
       if(Stroka[29+Sdvg]=='I'||Stroka[30+Sdvg]=='I'||
          Stroka[29+Sdvg]=='U'||Stroka[30+Sdvg]=='U')
       {
         return 1;
       }

     }
  }
*/

/*������� ������*/
  if(Dan->T_K[0]=='S'||Dan->T_K[1]=='S')
  {
        if(Length>=115&&Length<=116&&TmNach==88)
        {
                sscanf(Stroka+103,"%ld%ld",&(Dan->ObnarujNomBort),&(Dan->ObnarujVysota));
        }else if(Length>=129&&Length<=131&&TmNach==102)
        {
                sscanf(Stroka+117,"%ld%ld",&(Dan->ObnarujNomBort),&(Dan->ObnarujVysota));
        }
   }


  return 1;
}


int PromSravnDouble(const void *a, const void *b)
{
  double a1=*((double*)a);
  double b1=*((double*)b);
  if(a1>b1)return 1;
  if(a1<b1)return (-1);
  return 0;

}


//����� ����� ������ �� �������
double FindTempObzoraPoSeveram(FILE *fp)
{
//������� ������� � �������� ��� �������
  int LastM,LastH;
  double LastS;
  double VremyaVSekundah[1000];  //����� � ��������
  double Left,Right;
  int N_of_Ob[1000];
  int N_of_Severov=0;
  int i,j;
  int Ret;
  DANN Dan;
  fseek(fp,0,0);

//���� ������ �����
  while(Ret!=2)
  {
    if(feof(fp))return (-1);
    Ret=LoadDan(fp,&Dan);
  }
  LastM=Dan.M;
  LastH=Dan.H;
  LastS=Dan.S;

//���� ��������� ������
  while(!feof(fp))
  {
    Ret=LoadDan(fp,&Dan);
    if(Ret==2)  //������ �����
    {
//�������� ������� � ��������
       VremyaVSekundah[N_of_Severov]=3600*(Dan.H-LastH)+60*(Dan.M-LastM)+(Dan.S-LastS);
       N_of_Severov++;
       if(N_of_Severov==1000)break;
       LastM=Dan.M;
       LastH=Dan.H;
       LastS=Dan.S;
    }
  };

  if(N_of_Severov<10)return (-1);
//������ ���������� ������������� ������ ������� ������� � ������� ����������
  qsort(VremyaVSekundah, N_of_Severov, sizeof(double),PromSravnDouble);

//� �������������� ������ ����������� ���� ������ �������� ����� ����������� �����
  for(i=0;i<N_of_Severov;i++)
  {
    Left=VremyaVSekundah[i]-0.5;
    Right=VremyaVSekundah[i]+0.5;
    N_of_Ob[i]=0;
    for(j=i;j>=0;j--)
    {
      if(VremyaVSekundah[j]<Left)
      {
        break;
      }
      N_of_Ob[i]++;
    }
    for(j=i+1;j<N_of_Severov;j++)
    {
      if(VremyaVSekundah[j]>Right)
      {
        break;
      }
      N_of_Ob[i]++;
    }
  }

  int MaxN_of_O=-1;
  int i1_max=-1;
  int i2_max=-1;
  for(i=0;i<N_of_Severov;i++)
  {
    if(N_of_Ob[i]>MaxN_of_O)
    {
      i1_max=i;
      i2_max=i;
      MaxN_of_O=N_of_Ob[i];
      continue;
    }
    if(N_of_Ob[i]==MaxN_of_O)
    {
      i2_max=i;
      continue;
    }
  }

  double SumPeriod=0;

  for(i=i1_max;i<=i2_max;i++)
  {
    SumPeriod+=VremyaVSekundah[i];
  }
  return (SumPeriod/(i2_max-i1_max+1));
}







int GetFormatFile(char *FileName)
{
  FILE *fp;
  DANN Dan;
  int Ret=0;
  fp=MyFOpenR(FileName);
  if(fp==NULL)
  {
     return 0;
  }

  while(!feof(fp))
  {
    Ret=LoadDan(fp,&Dan);
    if(Ret==1||Ret==2)
    {
      fclose(fp);
      return 1;
    }
  }
  fclose(fp);
  return (-1);
}

