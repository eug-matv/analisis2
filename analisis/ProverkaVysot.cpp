/*��������� �.�.
� ������ ������ ���������� ��������� ������  OBZORY_1::AnalizVysot, �������
���������� �� ��������� OBZORY_1::FindNeoprNomBort, ������������� � ������
TraektKalman.cpp. ��� ������� ����� ������������ ��������  DopDVys ������ OBZORY_1,
�� �� ������������ � ���������� � ���� ��������� � ���� "���������� �������
�� ������".

�������� ������� ����� �����:

1) ������ ���������� ��������������� ��� ���� ������� ������, ����������,
����������� �������. ������ � AnalizVysot. ��� ������� ����� ���� ����������
������� OV_Mas ������� OTSCHET_VYS, ��������� � ProverkaVysot.h, ����� ���������.
������� ������� ����� �������� � ������.

2) ����� ��������� ������� OV_Mas ���������� ��������� FindLojnyeVysotyAll,
������������� � ������ ������, ������� � �������� �������� ������������ ��������� �����

3) ��������� FindLojnyeVysotyAll � ������� OV_Mas �������� ��� ������� ������
����� 40, ����� ������� ������ ���� ��������� �� ������ �� ����� 15 ��������.
������ �������� ��������������� ������ ������ ������ �� ������� ��� ����������,
�� ���� ������ �� ����� �������, ��� ��� ������� ������ �����������.

4) ��� ������� ���������� ���������� ��������� FindLojnyeVysotyN ���
FindLojnyeVysotyN_Uchityvat0, ������� ��������� ���� ��������� ����������� � ������������������ ��������
��� �������� �� ������. ��� ����� ���������� ��������� PravilnyLiRyadVysot ���
PravilnyLiRyadVysot_Uchityvat0 � ����������� �� FindLojnyeVysotyN ���
FindLojnyeVysotyN_Uchityvat0. ���������� ���������, ���� ���� ��������� � ����� ��� �����,
��� ���������� �� ����� ��� �����. ���������, ��� ���� �� ������� � �� ���������, ���� ������
���������� �� ����� ��� �� MaxPerepadVysotPV �. ����� ��� ���� ���������� ���� ��������

5) ���� ��� ����� ���� ��������� ������������������ �� ����������� ��� �� ������ �����,
�� ��������������� ���������� ������� 1 ������ �� ����������� (OV_Mas[k].IsViewed=false),
����� 2 � �.�., ���� ����������� ����� ���������, ��� ����  ����������
��������� PravilnyLiRyadVysot ��� PravilnyLiRyadVysot_Uchityvat0. ����� ��� �������,
������� ������� ����� "���������" ���� �������, �� �� ���������� �������� ��� ������ �� ������
(OV_Mas[k].IsLojn=true).

6) ����� ���� ������� ��� ������� � ������� �������� ����������� ������� ������ ������� �
������ �� ������ ��������. ��� ����� � ���� ������� FindLojnyeVysotyAll. �������� ������
 �����. ���� ������, � �������� �� ���������� ������ ��� ���������� �� ����� �����
 �������� �� ���� ��� �������, ������� �� ������� ���������� �� ����� ��� �� 100 �������,
�� ������ ����� ������� ��� ������� ������.
���� ������ ���� ������ ������ ��� �����, ������� ���������� ������ �� ������� (����� 100 �������),
�� �������� ���������������� ������� ������������� ������ ����� �������� �������.

7) � ���� ������� OBZORY_1::AnalizVysot. ���������� �������� �������� o_b_1 ������ OBZORY_1
� ������������ � ���������� ��������� ������� OV_Mas. ��� ��� ��������,
������� ����� ������ ������� ��������������� ��� LOJNY_VYSOTA_STATUS ��� ��������
Status, � ����� ���������� �������������� �������� �����.

��� ����� � ���� ������������ //1) ��� //2)  ������������ � ����������� �������

*/


//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <alloc.h>
#include "ProverkaVysot.h"
#include "Traekt.h"
#include "tools.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)


/*������ ��� �������� �����.*/

struct OTSCHET_VYS
{
  long Vysota;      //������������ ������ � ������
  long IstVysota;   //�������� ��� ������������ ������
  long NomerSektora; //����� ������� 90 ��������
  bool IsViewed; //������������� ������ ��� ������� �����
  bool IsLojn;   //������ �� ��� ������
  long Indx,Azmt;     //������ ������� ��� ����������� ����������� �������
  long Kanal;   //1 -P, 2 - S, 3 - PS
  long StatusVysoty;  //������ ������: 0 - �������������, 1 - ����������... ������ ��������������� ��������

  long NomBort;   //����� �����
};


long MaxPerepadVysotPV=150;  //����������� ���������� ������� ����� � ������

//������� ��������� �����
int SravnenieVysot(OTSCHET_VYS *OV1,OTSCHET_VYS *OV2)
{
  double dPerepad;
  long dObzor;
  dObzor=Okrugl((OV2->NomerSektora-OV1->NomerSektora)/4.0);
  if(dObzor>1)
  {
  //  dPerepad=(OV2->Vysota-OV1->Vysota)/((double)dObzor);
    dPerepad=OV2->Vysota-OV1->Vysota;
  }else{
    dPerepad=OV2->Vysota-OV1->Vysota;
  }
  if(dPerepad>MaxPerepadVysotPV)return (-1);
  if(dPerepad<-MaxPerepadVysotPV)return 1;
  return 0;
}




/*���������� ���������� �� ��� ������ � ��������� ��� �� ���������� �������, �������
���������� 1, ���� ���������, 0 - ���� ���� ������*/

int PravilnyLiRyadVysot(OTSCHET_VYS *OV_Mas, int N_of_OV_Mas)
{
 int i,j;
 int State=0,Ret,Ret1=1;
 bool IsWasSravnenie=false;
 for(i=0;i<N_of_OV_Mas-1;i++)
 {
   if(OV_Mas[i].IsViewed==false||OV_Mas[i].Vysota==0||
      OV_Mas[i].Kanal==1||OV_Mas[i].IsLojn)continue;

   for(j=i+1;j<N_of_OV_Mas;j++)
   {
//���� ������ ������ ������� ���������������
     if(OV_Mas[j].IsViewed==false||(OV_Mas[j].Vysota==0)||
     OV_Mas[j].Kanal==1||OV_Mas[j].IsLojn)continue;
     Ret=SravnenieVysot(OV_Mas+i,OV_Mas+j);
     IsWasSravnenie=true;
     if(Ret>0)
     {
       if(State<0)
       {
         Ret1=0;
       }
       State=1;
     }
     if(Ret<0)
     {
       if(State>0)
       {
         Ret1=0;
       }
       State=-1;
     }
     break;
   }
 }
 if(!IsWasSravnenie)return 0;
 return Ret1;
}





//����� ��������� ������ ������
//���� ������ ������ ���� OTSCHET_VYS
//������ ��� ������ ����� 30...
int FindLojnyeVysotyN(OTSCHET_VYS *OV_Mas,
                     int N) //����� ���������
{
   int Ret,M,RetPrav,i,k;
   int NextD;
   long Nmrs[100];  //������ Nms
   long UbratVydel[100],SdelatVydel[100],N_of_Smen;
   bool NaidenyLojnyeVysoty=false;
   for(i=0;i<N;i++)
   {
     OV_Mas[i].IsViewed=true;
     if(OV_Mas[i].Vysota<-500||OV_Mas[i].Vysota>25000)
     {
       OV_Mas[i].IsLojn=true;
     }
   }

//�������� ������ �������
//5)
   Ret=PravilnyLiRyadVysot(OV_Mas,N);
   if(Ret==1)return 1;  //���� �� ���������, �� ����� ����� � ������ �� ���������
   for(M=1;M<N/2;M++)
   {
 //� ��� ���� ��������� �� ����������.

     FirstSochetaniy(Nmrs,N,M);  //�������� � tools.h
     Ret=1;

     while(Ret==1)
     {
       NextD=0;
       for(k=0;k<M;k++)
       {
         if((OV_Mas[Nmrs[k]].Vysota==0)
             ||OV_Mas[Nmrs[k]].IsLojn||
            OV_Mas[Nmrs[k]].Kanal<2)
         {
            NextD=1;  //����� ���������� ������� � �� ������������.
            break;
         }
       }
       if(NextD)
       {
        //������� � ���������� ��������, �� k �� ��������. �������� � tools.h
         Ret=NextSochetaniy2(Nmrs,N,M,k,UbratVydel,SdelatVydel,&N_of_Smen);
         for(i=0;i<N_of_Smen;i++)OV_Mas[UbratVydel[i]].IsViewed=true;
         for(i=0;i<N_of_Smen;i++)OV_Mas[SdelatVydel[i]].IsViewed=false;
         continue;
       }

 //�������� ������������ ���� �� ������ ����������� ��� �������� �� ������.
       RetPrav=PravilnyLiRyadVysot(OV_Mas, N);
       if(RetPrav==1)
       {
          NaidenyLojnyeVysoty=true;
          break;
       }

//�������������� Nmrs. �������� � tools.h
       Ret=NextSochetaniy(Nmrs,N,M,UbratVydel,SdelatVydel,&N_of_Smen);
       if(Ret==2)break;
       for(i=0;i<N_of_Smen;i++)OV_Mas[UbratVydel[i]].IsViewed=true;
       for(i=0;i<N_of_Smen;i++)OV_Mas[SdelatVydel[i]].IsViewed=false;
     }
     if(NaidenyLojnyeVysoty)break;
   }
   if(NaidenyLojnyeVysoty==false)return 0;

//��������� ������ ������
   for(i=0;i<N;i++)
   {
     if((OV_Mas[i].Vysota!=0)&&OV_Mas[i].Kanal>1&&OV_Mas[i].IsViewed==false)
     {
       OV_Mas[i].IsLojn=true;
     }
   }
   return 1;
}


/*���������� ���������� �� ��� ������ � ��������� ��� �� ���������� �������, �������
���������� 1, ���� ���������, 0 - ���� ���� ������*/
//���� ��� ���������
int PravilnyLiRyadVysot_Uchityvat0(OTSCHET_VYS *OV_Mas, int N_of_OV_Mas)
{
 int i,j;
 int State=0,Ret,Ret1=1;
 bool IsWasSravnenie=false;
 for(i=0;i<N_of_OV_Mas-1;i++)
 {
//���� �� ������� ������
   if(OV_Mas[i].IsViewed==false)continue;
   if(OV_Mas[i].Kanal<2)continue;
   for(j=i+1;j<N_of_OV_Mas;j++)
   {
//���� ������ ������ ������� ���������������
     if(OV_Mas[j].IsViewed==false||
     OV_Mas[j].Kanal==1||OV_Mas[j].IsLojn)continue;
     Ret=SravnenieVysot(OV_Mas+i,OV_Mas+j);
     IsWasSravnenie=true;
     if(Ret>0)
     {
       if(State<0)
       {
         Ret1=0;
       }
       State=1;
     }
     if(Ret<0)
     {
       if(State>0)
       {
         Ret1=0;
       }
       State=-1;
     }
     break;
   }
 }
 if(!IsWasSravnenie)return 0;
 return Ret1;
}





//����� ��������� ������ ������
//���� ������ ������ ���� OTSCHET_VYS
//������ ��� ������ ����� 30...
//���� ��� ���������
int FindLojnyeVysotyN_Uchityvat0(OTSCHET_VYS *OV_Mas,
                     int N) //����� ���������
{
   int Ret,M,RetPrav,i,k;
   int NextD;
   long Nmrs[100];  //������ Nms
   long UbratVydel[100],SdelatVydel[100],N_of_Smen;
   bool NaidenyLojnyeVysoty=false;
   for(i=0;i<N;i++)
   {
     OV_Mas[i].IsViewed=true;
     if(OV_Mas[i].Vysota<-500||OV_Mas[i].Vysota>25000)
     {
       OV_Mas[i].IsLojn=true;
     }
   }

//�������� ������ �������
   Ret=PravilnyLiRyadVysot_Uchityvat0(OV_Mas,N);
   if(Ret==1)return 1;
   for(M=1;M<N/2;M++)
   {
     FirstSochetaniy(Nmrs,N,M);
     Ret=1;

     while(Ret==1)
     {
       NextD=0;
       for(k=0;k<M;k++)
       {
         if(OV_Mas[Nmrs[k]].IsLojn||
            OV_Mas[Nmrs[k]].Kanal<2)
         {
            NextD=1;
            break;
         }
       }
       if(NextD)
       {
         Ret=NextSochetaniy2(Nmrs,N,M,k,UbratVydel,SdelatVydel,&N_of_Smen);
         for(i=0;i<N_of_Smen;i++)OV_Mas[UbratVydel[i]].IsViewed=true;
         for(i=0;i<N_of_Smen;i++)OV_Mas[SdelatVydel[i]].IsViewed=false;
         continue;
       }
       RetPrav=PravilnyLiRyadVysot_Uchityvat0(OV_Mas, N);
       if(RetPrav==1)
       {
          NaidenyLojnyeVysoty=true;
          break;
       }
       Ret=NextSochetaniy(Nmrs,N,M,UbratVydel,SdelatVydel,&N_of_Smen);


       if(Ret==2)break;
       for(i=0;i<N_of_Smen;i++)OV_Mas[UbratVydel[i]].IsViewed=true;
       for(i=0;i<N_of_Smen;i++)OV_Mas[SdelatVydel[i]].IsViewed=false;
     }
     if(NaidenyLojnyeVysoty)break;
   }
   if(NaidenyLojnyeVysoty==false)return 0;

//��������� ������ ������
   for(i=0;i<N;i++)
   {
     if(OV_Mas[i].Kanal>1&&OV_Mas[i].IsViewed==false)
     {
       OV_Mas[i].IsLojn=true;
     }
   }
   return 1;
}




//����� ������ �����
int FindLojnyeVysotyAll(OTSCHET_VYS *OV_Mas,
                     int N)
{
  int i=0,i_p,j,Ret,j1;
  bool ShagNazad=false;
  int N_of_NotNull,N_of_O; //�� ������� ������
  double DSektor,dV;
  int N_of_MalyeVysoty;       //��� ������� ����������� ������� ��� ������
  int N_of_AllForMalyeVysoty;



//�� ������ ����� ���� ��� ������ ������
//����� ������ �����
//3)  ���� �������� ����� ����� 30 �������� ��� �������
  while(i<N)
  {

//���� ������, ����� ����� ��������� �������� �����
     N_of_NotNull=0;
     N_of_O=0;
     if(OV_Mas[i].Vysota)
     {
        N_of_NotNull++;
     }
     N_of_O++;

     for(j=i+1;j<N;j++)
     {
       if(OV_Mas[j].NomerSektora-OV_Mas[j-1].NomerSektora>400|| //���� ������ ����� 100 �������
          OV_Mas[j].StatusVysoty!=OV_Mas[j-1].StatusVysoty)    //��� ������ ���� ���, � ����� ���. ��� ��������
       {
//����� 100 �������� ������
          j--;
          break;
       }
       if(OV_Mas[j].Vysota)
       {
         N_of_NotNull++;
       }
       N_of_O++;
       if(N_of_NotNull==15||N_of_O==40)
       {
         break;
       }
     }
     ShagNazad=false;
//��������, � ������� �� �� ����������� ���������� �������� ��� ������� ������
     if(N_of_NotNull<10)
     {
       N_of_NotNull=0;
       N_of_O=0;
       if(j==N)
       {
           j=N-1;     
       }
       j1=j;
       for(j=j;j>=0;j--)
       {
         if(j>0&&(OV_Mas[j].NomerSektora-OV_Mas[j-1].NomerSektora>400||
            OV_Mas[j].StatusVysoty!=OV_Mas[j-1].StatusVysoty))
         {
//����� 100 �������� ������
            i=j;
            break;
         }

         if(OV_Mas[j].Vysota)
         {
           N_of_NotNull++;
         }
         N_of_O++;
         if(N_of_NotNull==15||N_of_O==40)
         {
           i=j;
           break;
         }
       }
       ShagNazad=true;
     }

//� ������ �������� ��
     if(N_of_NotNull>5)
     {
//� ��� ��� ���� ��������� �� ����� �� ������ ������ 2*MaxPerepadVysotPV
        N_of_MalyeVysoty=0;
        N_of_AllForMalyeVysoty=0;
        for(j=i;j<i+N_of_O;j++)
        {
           if((OV_Mas[j].Vysota<2*MaxPerepadVysotPV&&OV_Mas[j].Vysota>0)||
              (OV_Mas[j].Vysota>-2*MaxPerepadVysotPV&&OV_Mas[j].Vysota<0))
           {
             N_of_MalyeVysoty++;
           }
        }
        if(1.0*N_of_MalyeVysoty/N_of_NotNull>0.5)
        {
//4)
           FindLojnyeVysotyN_Uchityvat0(OV_Mas+i,N_of_O);
        }else{
//4)
           FindLojnyeVysotyN(OV_Mas+i,N_of_O);
        }
     }else{
//4)
        FindLojnyeVysotyN_Uchityvat0(OV_Mas+i,N_of_O);
     }
     if(ShagNazad)
     {
       i=j1+1;
     }else{
       i=i+N_of_O/2;
     }
  };



//��� ������� � �������� ������� ���������� �������� ��������
//������ ��� �������� ������� �� ���� ����������...
  int PrevNotNull=-1;
  i_p=0;

//6)  �������� ���������� ������ � �������������� ��� ��������� �� ������ ��������
  for(i=0;i<=N;i++)
  {
    if(i==N||(i>0&&OV_Mas[i].NomerSektora-OV_Mas[i-1].NomerSektora>400)||
       (i>0&&OV_Mas[i].StatusVysoty!=OV_Mas[i-1].StatusVysoty))
    {
 //���� ��� ���������� �������
      if(PrevNotNull>=0)
      {
 //��������� ��������
 //�������� ��� ������� ����� PrevNotNull ������� �� ������ ����� ��������
        if(OV_Mas[PrevNotNull].Vysota<2*MaxPerepadVysotPV)
           for(j=PrevNotNull;j<i;j++)OV_Mas[j].IstVysota=0;
        else
           for(j=PrevNotNull;j<i;j++)OV_Mas[j].IstVysota=OV_Mas[PrevNotNull].Vysota;

      }
      PrevNotNull=-1;
      i_p=i;
     }

     if(i>=N)break;
     if((OV_Mas[i].Vysota)&&OV_Mas[i].IsLojn==false&&OV_Mas[i].Kanal>1)
     {
       if(PrevNotNull<0)
       {
//��������� ������ ��� �������
          if(OV_Mas[i].Vysota<2*MaxPerepadVysotPV)
            for(j=i_p;j<i;j++)OV_Mas[j].IstVysota=0;
          else
            for(j=i_p;j<i;j++)OV_Mas[j].IstVysota=OV_Mas[i].Vysota;

       }else{
         if(i-PrevNotNull>1)
         {
           DSektor=OV_Mas[i].NomerSektora-OV_Mas[PrevNotNull].NomerSektora;
           if(DSektor==0)dV=(OV_Mas[i].Vysota-OV_Mas[PrevNotNull].Vysota);
           else dV=(OV_Mas[i].Vysota-OV_Mas[PrevNotNull].Vysota)/DSektor;

           for(j=PrevNotNull+1;j<i;j++)
           {
             DSektor=OV_Mas[j].NomerSektora-OV_Mas[PrevNotNull].NomerSektora;
             OV_Mas[j].IstVysota=OV_Mas[PrevNotNull].Vysota+Okrugl(dV*DSektor);
           }
         }
       }
       PrevNotNull=i;
     }
  }

  //������ ���������� ����������� � ���������, �� ������� �������������� ������� ���
//�����. ����� ������, � ����, ������� ����� �� ����������� ������. ��� ����������, ����� ������� ��������� ������
//������ �����, ��� �������
//  for(i=0;i<N;)
//  {
//    if(OV_Mas[i].IstVysota<2*MaxPerepadVysotPV)
//    {
//���� ���������� - ��� ����� ��� �������
//      for(j=i+1;j<i+40;j++)
//      {
//��� ����� ���� ������������� �� ������ �������
//
//      }
//    }
//    i++;
//  }


  return 1;
}



//����� �������� ���������� �� ������
//����� ��������� �� ������ �������� � ������ ����� �������������� � ���������� ��������
//��������� ����������� � ������ ProverkaVysot.cpp
int OBZORY_1::AnalizVysot(void)  //������ �����
{
  long MaxN_of_O=100;  //������������ ����� ��������
  long N_of_O,Smesh,NomA,IndxA;
  long NomA1,IndxA1;
  long StatusVysoty=1;
  int last_prev_gauge=GlobalGaugeCurTraekt;


//��������� ���������� �������� ������
  MaxPerepadVysotPV=DopDVys;

  OTSCHET_VYS *OV_Mas;
  OV_Mas=(OTSCHET_VYS *)malloc(sizeof(OTSCHET_VYS)*MaxN_of_O);
  int i,j; //��������� �� ���� ��������
  for(i=0;i<n_of_otis;i++)
  {
   Application->ProcessMessages();     

    if( otis[i].lFirstIndex<0||
        (otis[i].ulNumber&0x00FFFFFF)==0||
       ((otis[i].ulNumber&0xFF000000)!=0x01000000&&
        (otis[i].ulNumber&0xFF000000)!=0x02000000))
    {
       continue;
    }
    N_of_O=0;
    Smesh=otis[i].lFirstIndex;
//1)  ��������� �������� ����� � ������ o_b_1
    if(IsInPlot)
    {
//���� ������� ����, �� ������ ���� ���������
      StatusVysoty=1;
      while(Smesh>=0)
      {
        NomA=Smesh%362; IndxA=Smesh/362;
        if(o_b_1[NomA][IndxA].Status&3==1)
        {
//��������, � ��� �� ������ ��������, ���� ���, �� ������ ������ ����� ��������
          if(o_b_1[NomA][IndxA].ParnOtschet1>=0||o_b_1[NomA][IndxA].ParnOtschet2>=0)
          {
             Smesh=o_b_1[NomA][IndxA].Next;
             continue;
          }
        }
        if(o_b_1[NomA][IndxA].Status&3==3)
        {
          Smesh=o_b_1[NomA][IndxA].Next;
          continue;
        }

//��������� ������
        if(N_of_O==MaxN_of_O)
        {
              MaxN_of_O+=100;
              OV_Mas=(OTSCHET_VYS *)realloc(OV_Mas,sizeof(OTSCHET_VYS)*MaxN_of_O);
        }

//�������� �������� ������

        OV_Mas[N_of_O].Vysota=OV_Mas[N_of_O].IstVysota=o_b_1[NomA][IndxA].OprVysota;
        OV_Mas[N_of_O].NomerSektora=o_b_1[NomA][IndxA].SektorNomer;
        OV_Mas[N_of_O].IsLojn=false; OV_Mas[N_of_O].IsViewed=true;
        OV_Mas[N_of_O].Indx=IndxA; OV_Mas[N_of_O].Azmt=NomA;
        OV_Mas[N_of_O].Kanal=o_b_1[NomA][IndxA].Status&3;
        if(OV_Mas[N_of_O].Vysota>0&&OV_Mas[N_of_O].Kanal>1)
        {
           if(o_b_1[NomA][IndxA].Status&ABS_VYSOTA_STATUS)
           {
             StatusVysoty=1;
           }else{
//             StatusVysoty=0;
           }
        }
        OV_Mas[N_of_O].StatusVysoty=StatusVysoty;
        OV_Mas[N_of_O].NomBort=otis[i].ulNumber&0x00FFFFFF;
        N_of_O++;
        Smesh=o_b_1[NomA][IndxA].Next;
      };
    }else{
      StatusVysoty=1;
      while(Smesh>=0)
      {
        NomA=Smesh%362; IndxA=Smesh/362;

        if(N_of_O==MaxN_of_O)
        {
              MaxN_of_O+=100;
              OV_Mas=(OTSCHET_VYS *)realloc(OV_Mas,sizeof(OTSCHET_VYS)*MaxN_of_O);
        }

//�������� �������� ������

        OV_Mas[N_of_O].Vysota=OV_Mas[N_of_O].IstVysota=o_b_1[NomA][IndxA].OprVysota;
        OV_Mas[N_of_O].NomerSektora=o_b_1[NomA][IndxA].SektorNomer;
        OV_Mas[N_of_O].IsLojn=false; OV_Mas[N_of_O].IsViewed=true;
        OV_Mas[N_of_O].Indx=IndxA; OV_Mas[N_of_O].Azmt=NomA;
        OV_Mas[N_of_O].Kanal=o_b_1[NomA][IndxA].Status&3;
        if(OV_Mas[N_of_O].Vysota>0&&OV_Mas[N_of_O].Kanal>1)
        {
           if(o_b_1[NomA][IndxA].Status&ABS_VYSOTA_STATUS)
           {
             StatusVysoty=1;
           }else{
//             StatusVysoty=0;
           }
        }
//        StatusVysoty=0; //����
        OV_Mas[N_of_O].StatusVysoty=StatusVysoty;
        OV_Mas[N_of_O].NomBort=otis[i].ulNumber&0x00FFFFFF;
        N_of_O++;
        Smesh=o_b_1[NomA][IndxA].Next;
      };
    }

//������ ����� ���������� ��������� ������ ��������� ��������
//2) ����� ���������� ��������
    FindLojnyeVysotyAll(OV_Mas,N_of_O);  //�������� ���������

//�������� ��� �������
//����� ���������� ��� ������

//7) ������������� �������� � ������������ �� ���������� ��������� OV_Mas
    if(IsInPlot)
    {
      for(j=0;j<N_of_O;j++)
      {
        NomA=OV_Mas[j].Azmt; IndxA=OV_Mas[j].Indx;

        if(OV_Mas[j].IstVysota!=0 &&OV_Mas[j].Vysota==0&&
        ABS(OV_Mas[j].IstVysota)<MaxPerepadVysotPV)
        {
           OV_Mas[j].IstVysota=OV_Mas[j].Vysota;
        }
        o_b_1[NomA][IndxA].IstVysota=OV_Mas[j].IstVysota;


        if(OV_Mas[j].IsLojn)o_b_1[NomA][IndxA].Status|=LOJNY_VYSOTA_STATUS;
        if(o_b_1[NomA][IndxA].ParnOtschet1>=0)
        {
          NomA1=o_b_1[NomA][IndxA].ParnOtschet1%362;IndxA1=o_b_1[NomA][IndxA].ParnOtschet1/362;
          o_b_1[NomA1][IndxA1].IstVysota=OV_Mas[j].IstVysota;
          if(OV_Mas[j].IsLojn)o_b_1[NomA1][IndxA1].Status|=LOJNY_VYSOTA_STATUS;
        }
        if(o_b_1[NomA][IndxA].ParnOtschet2>=0)
        {
          NomA1=o_b_1[NomA][IndxA].ParnOtschet2%362;IndxA1=o_b_1[NomA][IndxA].ParnOtschet2/362;
          o_b_1[NomA1][IndxA1].IstVysota=OV_Mas[j].IstVysota;
          if(OV_Mas[j].IsLojn)o_b_1[NomA1][IndxA1].Status|=LOJNY_VYSOTA_STATUS;
        }
      }
    }else{
      for(j=0;j<N_of_O;j++)
      {
        NomA=OV_Mas[j].Azmt; IndxA=OV_Mas[j].Indx;
        if(OV_Mas[j].IstVysota!=0 &&OV_Mas[j].Vysota==0&&
           ABS(OV_Mas[j].IstVysota)<MaxPerepadVysotPV)
        {
           OV_Mas[j].IstVysota=OV_Mas[j].Vysota;
        }
        o_b_1[NomA][IndxA].IstVysota=OV_Mas[j].IstVysota;
        if(OV_Mas[j].IsLojn)o_b_1[NomA][IndxA].Status|=LOJNY_VYSOTA_STATUS;
      }
    }

    GlobalGaugeCurTraekt=last_prev_gauge+
                (int)(100)*(((double)i)/n_of_otis);

    Application->ProcessMessages();

  }

  if(OV_Mas)free(OV_Mas);

  return 1;
}



