object FGlavOkno: TFGlavOkno
  Left = 239
  Top = 0
  Width = 928
  Height = 480
  Caption = 'FGlavOkno'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object RightPanel: TPanel
    Left = 727
    Top = 0
    Width = 191
    Height = 381
    Align = alRight
    BevelOuter = bvNone
    Color = 16770239
    TabOrder = 0
    object GBMashtab: TGroupBox
      Left = 0
      Top = 0
      Width = 191
      Height = 65
      Align = alTop
      Caption = #1052#1072#1089#1096#1090#1072#1073
      TabOrder = 0
      TabStop = True
      object RB_50km: TRadioButton
        Left = 27
        Top = 13
        Width = 49
        Height = 17
        Caption = '50 '#1082#1084
        TabOrder = 0
        TabStop = True
      end
      object RB_100km: TRadioButton
        Left = 27
        Top = 30
        Width = 56
        Height = 17
        Caption = '100 '#1082#1084
        TabOrder = 1
        TabStop = True
      end
      object RB_400km: TRadioButton
        Left = 91
        Top = 30
        Width = 56
        Height = 17
        Caption = '400 '#1082#1084
        Checked = True
        TabOrder = 3
        TabStop = True
      end
      object RB_200km: TRadioButton
        Left = 91
        Top = 13
        Width = 62
        Height = 17
        Caption = '200 '#1082#1084
        TabOrder = 2
        TabStop = True
      end
      object RB_Uvelichenie: TRadioButton
        Left = 27
        Top = 48
        Width = 105
        Height = 15
        Caption = #1059#1074#1077#1083#1080#1095#1077#1085#1080#1077
        TabOrder = 4
        TabStop = True
        Visible = False
      end
    end
    object StaticText1: TStaticText
      Left = 6
      Top = 69
      Width = 168
      Height = 20
      Alignment = taCenter
      BorderStyle = sbsSingle
      Caption = #1040#1085#1072#1083#1080#1079' '#1092#1072#1081#1083#1072' '#1076#1072#1085#1085#1099#1093
      Color = 8421631
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
    object MVolDataSel: TMemo
      Left = 6
      Top = 99
      Width = 181
      Height = 34
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MVolDataSel')
      ReadOnly = True
      TabOrder = 2
    end
    object MVolVydelData: TMemo
      Left = 6
      Top = 134
      Width = 181
      Height = 33
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MVolVydelData')
      ReadOnly = True
      TabOrder = 3
    end
    object MVydelenoSek: TMemo
      Left = 6
      Top = 169
      Width = 181
      Height = 25
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MVydelenoSek')
      ReadOnly = True
      TabOrder = 4
    end
    object MKurs: TMemo
      Left = 6
      Top = 196
      Width = 181
      Height = 25
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MKurs')
      ReadOnly = True
      TabOrder = 5
    end
    object MSredSkorost: TMemo
      Left = 6
      Top = 223
      Width = 181
      Height = 27
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MSredSkorost')
      ReadOnly = True
      TabOrder = 6
    end
    object Panel2: TPanel
      Left = 0
      Top = 175
      Width = 191
      Height = 206
      Align = alBottom
      BevelOuter = bvNone
      Color = 16770239
      TabOrder = 7
      DesignSize = (
        191
        206)
      object Label6: TLabel
        Left = 121
        Top = 134
        Width = 57
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = #1052#1077#1076#1083#1077#1085#1085#1077#1077
      end
      object M_SKO_D: TMemo
        Left = 6
        Top = 2
        Width = 167
        Height = 39
        TabStop = False
        Alignment = taCenter
        Color = 14540253
        Lines.Strings = (
          'M_SKO_D')
        ReadOnly = True
        TabOrder = 0
      end
      object M_SKO_A: TMemo
        Left = 6
        Top = 44
        Width = 167
        Height = 37
        TabStop = False
        Alignment = taCenter
        Color = 14540253
        Lines.Strings = (
          'M_SKO_A')
        ReadOnly = True
        TabOrder = 1
      end
      object BTochnost: TButton
        Left = 7
        Top = 85
        Width = 64
        Height = 20
        Caption = #1058#1086#1095#1085#1086#1089#1090#1100
        TabOrder = 2
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 106
        Width = 105
        Height = 40
        Anchors = [akRight, akBottom]
        Caption = #1055#1086#1088#1103#1076#1086#1082
        TabOrder = 3
        object CSE_Poryadok: TComboBox
          Left = 8
          Top = 13
          Width = 89
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = '1'
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            #1056#1040#1047#1053#1054#1057#1058#1068)
        end
      end
      object BZonaP: TButton
        Left = 117
        Top = 87
        Width = 55
        Height = 21
        Caption = #1047#1086#1085#1072' P'
        TabOrder = 4
      end
      object BZonaS: TButton
        Left = 117
        Top = 114
        Width = 55
        Height = 19
        Caption = #1047#1086#1085#1072' S'
        TabOrder = 5
      end
      object BStart: TButton
        Left = 9
        Top = 171
        Width = 44
        Height = 27
        HelpContext = 4110
        Anchors = [akRight, akBottom]
        Caption = #1057#1090#1072#1088#1090
        TabOrder = 6
      end
      object PBPause: TPanel
        Left = 53
        Top = 171
        Width = 83
        Height = 27
        HelpContext = 4110
        Anchors = [akRight, akBottom]
        BevelWidth = 2
        Caption = #1055#1072#1091#1079#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
      end
      object BEnd: TButton
        Left = 137
        Top = 171
        Width = 47
        Height = 27
        HelpContext = 4110
        Caption = #1057#1090#1086#1087
        TabOrder = 8
      end
      object TBDTimeAnim: TTrackBar
        Left = 12
        Top = 146
        Width = 177
        Height = 20
        Anchors = [akRight, akBottom]
        Max = 25
        Min = 1
        Orientation = trHorizontal
        Frequency = 1
        Position = 12
        SelEnd = 0
        SelStart = 0
        TabOrder = 9
        ThumbLength = 15
        TickMarks = tmBottomRight
        TickStyle = tsAuto
      end
    end
  end
  object NizPanel: TPanel
    Left = 0
    Top = 381
    Width = 918
    Height = 47
    Align = alBottom
    BevelOuter = bvNone
    Color = 16770239
    TabOrder = 1
    DesignSize = (
      918
      47)
    object CGauge1: TCGauge
      Left = 0
      Top = 0
      Width = 728
      Height = 18
      Anchors = [akLeft, akTop, akRight]
      ShowText = False
      ForeColor = clPurple
      Visible = False
    end
    object Label11: TLabel
      Left = 176
      Top = 32
      Width = 38
      Height = 13
      Caption = 'Label11'
    end
    object CBNazad: TCheckBox
      Left = 745
      Top = 8
      Width = 161
      Height = 15
      HelpContext = 4110
      Anchors = [akRight]
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1085#1072#1079#1072#1076
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object PB_Animate: TProgressBar
      Left = 746
      Top = 26
      Width = 174
      Height = 13
      Anchors = [akRight]
      Min = 0
      Max = 100000
      TabOrder = 1
    end
    object StatusBar1: TStatusBar
      Left = 0
      Top = 25
      Width = 918
      Height = 22
      Color = 16770239
      Panels = <
        item
          Width = 50
        end>
      SimplePanel = False
    end
  end
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 727
    Height = 381
    Align = alClient
    BevelOuter = bvNone
    Caption = 'MainPanel'
    TabOrder = 2
    object VerhPanel: TPanel
      Left = 0
      Top = 0
      Width = 727
      Height = 65
      Align = alTop
      BevelOuter = bvNone
      Color = 16565571
      TabOrder = 0
      object GBVremya: TGroupBox
        Left = 97
        Top = 0
        Width = 93
        Height = 65
        Align = alLeft
        Caption = #1042#1088#1077#1084#1103
        TabOrder = 2
        object LCurTime: TLabel
          Left = 8
          Top = 24
          Width = 45
          Height = 13
          Caption = 'LCurTime'
        end
        object Label1: TLabel
          Left = 9
          Top = 15
          Width = 7
          Height = 16
          Caption = #1089
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 3
          Top = 39
          Width = 16
          Height = 16
          Caption = #1087#1086
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DTP1: TDateTimePicker
          Left = 19
          Top = 14
          Width = 71
          Height = 21
          CalAlignment = dtaLeft
          Date = 38691.3821059144
          Time = 38691.3821059144
          Color = clBtnFace
          DateFormat = dfShort
          DateMode = dmUpDown
          Enabled = False
          Kind = dtkTime
          ParseInput = False
          TabOrder = 0
        end
        object DTP2: TDateTimePicker
          Left = 19
          Top = 37
          Width = 70
          Height = 21
          CalAlignment = dtaLeft
          Date = 0.382105914351996
          Time = 0.382105914351996
          Color = clBtnFace
          DateFormat = dfShort
          DateMode = dmUpDown
          Enabled = False
          Kind = dtkTime
          ParseInput = False
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 190
        Top = 0
        Width = 49
        Height = 65
        Align = alLeft
        Caption = #1057#1091#1090#1082#1080
        TabOrder = 3
        Visible = False
        object CSE_Sutki1: TCSpinEdit
          Left = 4
          Top = 13
          Width = 41
          Height = 22
          Color = clBtnFace
          Enabled = False
          MaxValue = 5
          MinValue = 1
          TabOrder = 0
          Value = 1
        end
        object CSE_Sutki2: TCSpinEdit
          Left = 4
          Top = 38
          Width = 41
          Height = 22
          Color = clBtnFace
          Enabled = False
          MaxValue = 5
          MinValue = 1
          TabOrder = 1
          Value = 1
        end
      end
      object GroupBox3: TGroupBox
        Left = 239
        Top = 0
        Width = 106
        Height = 65
        Align = alLeft
        Caption = #1042#1099#1089#1086#1090#1072
        TabOrder = 4
        object Label3: TLabel
          Left = 1
          Top = 16
          Width = 17
          Height = 16
          Caption = #1054#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 2
          Top = 40
          Width = 16
          Height = 16
          Caption = #1076#1086
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 88
          Top = 27
          Width = 11
          Height = 16
          Caption = #1052
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object EVysota1: TEdit
          Left = 19
          Top = 15
          Width = 44
          Height = 21
          TabOrder = 0
          Text = 'EVysota1'
        end
        object CSBVysota1: TCSpinButton
          Left = 64
          Top = 14
          Width = 20
          Height = 22
          DownGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000808000808000
            8080000000008080008080008080008080000080800080800080800000000000
            0000000000808000808000808000008080008080000000000000000000000000
            0000000080800080800000808000000000000000000000000000000000000000
            0000008080000080800080800080800080800080800080800080800080800080
            8000}
          TabOrder = 1
          UpGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000000000000000
            0000000000000000000000000000008080000080800080800000000000000000
            0000000000000000808000808000008080008080008080000000000000000000
            0080800080800080800000808000808000808000808000000000808000808000
            8080008080000080800080800080800080800080800080800080800080800080
            8000}
        end
        object EVysota2: TEdit
          Left = 19
          Top = 39
          Width = 44
          Height = 21
          TabOrder = 2
          Text = 'EVysota1'
        end
        object CSBVysota2: TCSpinButton
          Left = 64
          Top = 38
          Width = 20
          Height = 22
          DownGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000808000808000
            8080000000008080008080008080008080000080800080800080800000000000
            0000000000808000808000808000008080008080000000000000000000000000
            0000000080800080800000808000000000000000000000000000000000000000
            0000008080000080800080800080800080800080800080800080800080800080
            8000}
          TabOrder = 3
          UpGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000000000000000
            0000000000000000000000000000008080000080800080800000000000000000
            0000000000000000808000808000008080008080008080000000000000000000
            0080800080800080800000808000808000808000808000000000808000808000
            8080008080000080800080800080800080800080800080800080800080800080
            8000}
        end
      end
      object Panel1: TPanel
        Left = 684
        Top = 0
        Width = 43
        Height = 65
        Align = alRight
        BevelInner = bvLowered
        BevelOuter = bvNone
        Color = 16565571
        TabOrder = 1
        object B_OK: TButton
          Left = 6
          Top = 19
          Width = 33
          Height = 25
          Caption = 'OK'
          TabOrder = 0
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 97
        Height = 65
        Align = alLeft
        Caption = #1057#1087#1080#1089#1086#1082' '#1073#1086#1088#1090#1086#1074
        TabOrder = 0
        object BSortSpisokBortov: TButton
          Left = 32
          Top = 13
          Width = 50
          Height = 19
          Caption = '1...9'
          TabOrder = 0
        end
        object Button1: TButton
          Left = 4
          Top = 34
          Width = 90
          Height = 26
          Caption = #1057#1084#1086#1090#1088#1077#1090#1100' '#1073#1086#1088#1090#1072
          TabOrder = 1
        end
      end
    end
    object RisovPanel: TPanel
      Left = 4
      Top = 65
      Width = 723
      Height = 316
      Align = alClient
      AutoSize = True
      BevelOuter = bvNone
      Color = clBlack
      TabOrder = 1
      DesignSize = (
        723
        316)
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 723
        Height = 316
        Align = alClient
        ParentShowHint = False
        ShowHint = False
      end
      object Label9: TLabel
        Left = 119
        Top = 307
        Width = 175
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = '- '#1074' '#1085#1077#1087#1086#1089#1088'. '#1073#1083#1080#1079#1086#1089#1090#1080'(P, '#1059#1042#1044', RBS)'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape5: TShape
        Left = 103
        Top = 307
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = 192
      end
      object Shape4: TShape
        Left = 89
        Top = 307
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clFuchsia
      end
      object Shape3: TShape
        Left = 75
        Top = 307
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clRed
      end
      object Label8: TLabel
        Left = 55
        Top = 308
        Width = 13
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = '- S'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape2: TShape
        Left = 38
        Top = 307
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clYellow
      end
      object Label7: TLabel
        Left = 18
        Top = 308
        Width = 13
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = '- P'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape1: TShape
        Left = 2
        Top = 307
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clBlue
      end
      object Label10: TLabel
        Left = 705
        Top = 308
        Width = 20
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = '- PS'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape6: TShape
        Left = 690
        Top = 307
        Width = 15
        Height = 14
        Anchors = [akRight, akBottom]
        Brush.Color = clLime
      end
      object SVydel: TShape
        Left = 120
        Top = 96
        Width = 520
        Height = 93
        Anchors = [akLeft, akTop, akRight, akBottom]
        Brush.Style = bsClear
        Pen.Color = clWhite
        Pen.Mode = pmXor
        Pen.Style = psDashDot
      end
      object PanelStatus: TPanel
        Left = 64
        Top = 120
        Width = 638
        Height = 116
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        Visible = False
        object LPanelStatus: TLabel
          Left = 1
          Top = 1
          Width = 636
          Height = 114
          Align = alClient
          Alignment = taCenter
          Caption = 'LPanelStatus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object LeftPanel: TPanel
      Left = 0
      Top = 65
      Width = 4
      Height = 316
      Align = alLeft
      TabOrder = 2
    end
  end
  object CBSpisokBortov: TListBox
    Left = 0
    Top = 60
    Width = 84
    Height = 21
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ItemHeight = 14
    MultiSelect = True
    ParentFont = False
    PopupMenu = PM_SpisokBortov
    TabOrder = 3
    Visible = False
  end
  object TimerPerezapis: TTimer
    Enabled = False
    Left = 324
    Top = 569
  end
  object TimerAnimate: TTimer
    Enabled = False
    Interval = 100
    Left = 52
    Top = 312
  end
  object Timer2: TTimer
    Left = 188
    Top = 465
  end
  object Timer1: TTimer
    Enabled = False
    Left = 204
    Top = 113
  end
  object SaveDialog1: TSaveDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 76
    Top = 105
  end
  object PM_SpisokBortov: TPopupMenu
    Left = 24
    Top = 48
    object PM_CopyToBuffer: TMenuItem
      Caption = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074' '#1073#1091#1092#1077#1088' '#1076#1072#1085#1085#1099#1077' '#1086' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1093' '#1073#1086#1088#1090#1072#1093
    end
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 20
    Top = 185
  end
  object MainMenu1: TMainMenu
    Left = 84
    Top = 328
    object mnuFile: TMenuItem
      Caption = #1060#1072#1081#1083
      object MListOfFiles: TMenuItem
        Caption = #1057#1087#1080#1089#1086#1082' '#1092#1072#1081#1083#1086#1074
      end
      object N1: TMenuItem
        Caption = #1054#1090#1082#1088#1099#1090#1100
      end
      object mnuSaveAs: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1092#1072#1081#1083' '#1076#1072#1085#1085#1099#1093
      end
      object mnuSaveVydelAs: TMenuItem
        Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1092#1072#1081#1083' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1093' '#1076#1072#1085#1085#1099#1093
      end
      object mnuSetUserInfo: TMenuItem
        Caption = #1042#1074#1077#1089#1090#1080' '#1080#1085#1092#1086' '#1086' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1077
      end
      object mnuSetDocDate: TMenuItem
        Caption = #1042#1074#1077#1089#1090#1080' '#1080#1085#1092#1086' '#1086' '#1076#1072#1090#1077' '#1076#1086#1082#1091#1084#1077#1085#1090#1080#1088#1086#1074#1072#1085#1080#1103
      end
      object mnuSetDannyeInfo: TMenuItem
        Caption = #1042#1074#1077#1089#1090#1080' '#1085#1077#1086#1073#1093#1086#1076#1080#1084#1091#1102' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1076#1072#1085#1085#1099#1093
      end
      object mnuIzvlech: TMenuItem
        Caption = #1048#1079#1074#1083#1077#1095#1100' '#1080#1079' '#1092#1072#1081#1083#1072
        object mnuIzvlechUVD: TMenuItem
          Caption = #1059#1042#1044
        end
        object mnuIzvlechRBS: TMenuItem
          Caption = 'RBS'
        end
        object mnuIzvlechPS_S: TMenuItem
          Caption = 'PS '#1080' S'
        end
        object mnuIzvlechS: TMenuItem
          Caption = 'S'
        end
        object mnuIzvlechP: TMenuItem
          Caption = 'P'
        end
        object mnuIzvlechP_PS: TMenuItem
          Caption = 'P '#1080' PS'
        end
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object mnuSpiskiBort: TMenuItem
        Caption = #1057#1087#1080#1089#1082#1080' '#1073#1086#1088#1090#1086#1074
        object mnu_Load_Selection: TMenuItem
          Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1093' '#1073#1086#1088#1090#1086#1074
        end
        object N9: TMenuItem
          Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1093' '#1073#1086#1088#1090#1086#1074
        end
        object mnuViewVisualBorts: TMenuItem
          Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1086#1090#1086#1073#1088#1072#1078#1072#1077#1084#1099#1093' '#1073#1086#1088#1090#1086#1074
        end
        object mnuSaveVydelList: TMenuItem
          Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1073#1086#1088#1090#1086#1074', '#1082#1086#1090#1086#1088#1099#1077' '#1074' '#1086#1073#1083#1072#1089#1090#1080' '#1074#1099#1076#1077#1083#1077#1085#1080#1103
        end
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object mnuPrint: TMenuItem
        Caption = #1055#1077#1095#1072#1090#1100
        object mnuPrintPicture: TMenuItem
          Caption = #1050#1072#1088#1090#1080#1085#1082#1072' ('#1090#1088#1072#1077#1082#1090#1086#1088#1080#1080')'
        end
        object mnuPrintData1: TMenuItem
          Caption = #1058#1072#1073#1083#1080#1094#1072' '#1076#1072#1085#1085#1099#1093
          Visible = False
        end
        object mnuPrintData: TMenuItem
          Caption = #1058#1072#1073#1083#1080#1094#1072' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1093' '#1076#1072#1085#1085#1099#1093
          Visible = False
        end
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object mnuExit: TMenuItem
        Caption = #1042#1099#1093#1086#1076
      end
    end
    object mnuDates: TMenuItem
      Caption = #1044#1072#1085#1085#1099#1077
      object mnuShowDates: TMenuItem
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      end
      object mnuShowDates1: TMenuItem
        Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1085#1099#1077' '#1076#1072#1085#1085#1099#1077
      end
      object mnuStat: TMenuItem
        Caption = #1057#1090#1072#1090#1080#1089#1090#1080#1082#1072
        Visible = False
      end
      object mnuDop: TMenuItem
        Caption = #1048#1089#1082#1072#1078#1077#1085#1080#1103' '#1087#1086' '#1074#1099#1089#1086#1090#1072#1084' '#1080' '#1085#1086#1084#1077#1088#1072#1084
      end
      object mnuOtmetkiMejduNord: TMenuItem
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1086#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1090#1084#1077#1090#1086#1082' P '#1084#1077#1078#1076#1091' '#1089#1077#1074#1077#1088#1072#1084#1080
      end
      object mnuOtmetkiMejduNordSred: TMenuItem
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1086#1077' '#1089#1088#1077#1076#1085#1077#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1086#1090#1084#1077#1090#1086#1082' P '#1084#1077#1078#1076#1091' '#1089#1077#1074#1077#1088#1072#1084#1080
        ImageIndex = 7
      end
      object mnuMaxLojnS: TMenuItem
        Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1086#1077' '#1095#1080#1089#1083#1086' '#1083#1086#1078#1085#1099#1093' '#1087#1086#1084#1077#1093' '#1079#1072' '#1086#1076#1080#1085' '#1086#1073#1079#1086#1088
        Visible = False
        object mnuMaxLokjnUVD: TMenuItem
          Caption = #1059#1042#1044
        end
        object mnuMaxLokjnRBS: TMenuItem
          Caption = 'RBS'
        end
      end
      object mnuDroblVK: TMenuItem
        Caption = #1055#1086#1080#1089#1082' '#1076#1088#1086#1073#1083#1077#1085#1099#1093' '#1086#1090#1089#1095#1077#1090#1086#1074' '#1074#1090#1086#1088#1080#1095#1085#1086#1075#1086' '#1082#1072#1085#1072#1083#1072
      end
      object mnuFindPObjed: TMenuItem
        Caption = #1042#1077#1088#1086#1103#1090#1085#1086#1089#1090#1100' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103' '#1086#1090#1089#1095#1077#1090#1086#1074' '#1055#1050' '#1080' '#1042#1050
      end
      object mnuProvestiPrivyazku: TMenuItem
        Caption = #1055#1088#1086#1074#1077#1089#1090#1080' '#1087#1088#1080#1074#1103#1079#1082#1091
      end
      object mnuUbratPrivyazku: TMenuItem
        Caption = #1059#1073#1088#1072#1090#1100' '#1087#1088#1080#1074#1103#1079#1082#1091
      end
    end
    object mnuFindRS: TMenuItem
      Caption = #1055#1086#1080#1089#1082' '#1056#1057
      object mnuRS_PK: TMenuItem
        Caption = #1056#1057' '#1087#1086' '#1087#1077#1088#1074#1080#1095#1085#1086#1084#1091' '#1082#1072#1085#1072#1083#1091
        object mnuRS_PK_Option: TMenuItem
          Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
        end
        object mnuRS_PK_FindBlizkie: TMenuItem
          Caption = #1053#1072#1081#1090#1080' '#1073#1083#1080#1079#1082#1080#1077' '#1086#1090#1089#1095#1077#1090#1099
        end
        object mnuRS_PK_VklBorta: TMenuItem
          Caption = #1042#1082#1083#1102#1095#1080#1090#1100' '#1073#1086#1088#1090#1072' '#1089' '#1073#1083#1080#1079#1082#1086' '#1088#1072#1089#1087#1086#1083#1086#1078#1077#1085#1085#1099#1084#1080' '#1086#1090#1089#1095#1077#1090#1072#1084#1080
        end
        object mnuRS_PK_ViewVydel: TMenuItem
          Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1074' '#1074#1099#1076#1077#1083#1077#1085#1085#1086#1081' '#1086#1073#1083#1072#1089#1090#1080
        end
        object mnuRS_PK_Clear: TMenuItem
          Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1084#1072#1089#1089#1080#1074' '#1087#1072#1088
        end
      end
      object mnuRS_VK: TMenuItem
        Caption = #1056#1057' '#1087#1086' '#1074#1090#1086#1088#1080#1095#1085#1086#1084#1091' '#1082#1072#1085#1072#1083#1091
        object mnuRS_VK_Option: TMenuItem
          Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099
        end
        object mnuRS_VK_FindBlizkie: TMenuItem
          Caption = #1053#1072#1081#1090#1080' '#1073#1083#1080#1079#1082#1080#1077' '#1086#1090#1089#1095#1077#1090#1099
        end
        object mnuRS_VK_VklBorta: TMenuItem
          Caption = #1042#1082#1083#1102#1095#1080#1090#1100' '#1073#1086#1088#1090#1072' '#1089' '#1073#1083#1080#1079#1082#1086' '#1088#1072#1089#1087#1086#1083#1086#1078#1077#1085#1085#1099#1084#1080' '#1086#1090#1089#1095#1077#1090#1072#1084#1080
          object mnuRS_UVD_Add_Brt: TMenuItem
            Caption = #1059#1042#1044
          end
          object mnuRS_RBS_Add_Brt: TMenuItem
            Caption = 'RBS'
          end
        end
        object mnuRS_VK_ViewVydel: TMenuItem
          Caption = #1055#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1074' '#1074#1099#1076#1077#1083#1077#1085#1085#1086#1081' '#1086#1073#1083#1072#1089#1090#1080
          object mnuRS_VK_View_UVD: TMenuItem
            Caption = #1059#1042#1044
          end
          object mnuRS_VK_View_RBS: TMenuItem
            Caption = 'RBS'
          end
        end
        object mnuRS_VK_Clear: TMenuItem
          Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1084#1072#1089#1089#1080#1074' '#1087#1072#1088
        end
      end
    end
    object mnuOblast: TMenuItem
      Caption = #1054#1073#1083#1072#1089#1090#1100
      object mnuVnutriOblasti: TMenuItem
        Caption = #1042#1085#1091#1090#1088#1080' '#1087#1088#1103#1084#1086#1091#1075#1086#1083#1100#1085#1086#1081' '#1086#1073#1083#1072#1089#1090#1080
        ShortCut = 16457
      end
      object mnuPredyshMashtab: TMenuItem
        Caption = #1055#1088#1077#1076#1099#1076#1091#1097#1080#1081' '#1084#1072#1089#1096#1090#1072#1073
        ShortCut = 32776
      end
      object mnuNotPriam: TMenuItem
        Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1085#1077#1087#1088#1103#1084#1086#1091#1075#1086#1083#1100#1085#1091#1102' '#1086#1073#1083#1072#1089#1090#1100
        ShortCut = 16463
      end
      object mnuClearSelection: TMenuItem
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1091#1102' '#1086#1073#1083#1072#1089#1090#1100
        ShortCut = 46
      end
      object mnuClearNotSelection: TMenuItem
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1074#1089#1105', '#1082#1088#1086#1084#1077' '#1074#1099#1076#1077#1083#1077#1085#1085#1086#1081' '#1086#1073#1083#1072#1089#1090#1080
        ShortCut = 8238
      end
    end
    object mnuOption: TMenuItem
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
    end
    object mnuHelp: TMenuItem
      Caption = #1055#1086#1084#1086#1097#1100
      object N4: TMenuItem
        Caption = #1055#1086' '#1075#1083#1072#1074#1085#1086#1084#1091' '#1086#1082#1085#1091
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object N2: TMenuItem
        Caption = #1057#1086#1076#1077#1088#1078#1072#1085#1080#1077
      end
      object N3: TMenuItem
        Caption = #1048#1085#1076#1077#1082#1089' '#1089#1087#1088#1072#1074#1086#1095#1085#1086#1075#1086' '#1092#1072#1081#1083#1072
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object N8: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077
      end
    end
  end
end
