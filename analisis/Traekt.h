/*
������, � ������� ���������� �������� ����� OBZORY_1 ��� ������ ��������� "Analisis"
�������� ������ ���� ���� ����� ������������ ������.
*/


//---------------------------------------------------------------------------
#ifndef TraektH
#define TraektH
#include <stdio.h>
#include "RabsBort.h"
#include "CGAUGES.h"
#include "approx.h"


#define MIN_SKOROST 0

#define MAX_SKOROST 2500

//---------------------------------------------------------------------------

//������ ������ ������������ ���� ��� ������ ������� - P
#define KANAL_P_STATUS ((unsigned short)0x1)

//������ ������ ������������ ���� ��� ������ ������� - S
#define KANAL_S_STATUS ((unsigned short)0x2)
//���� ��� ������ PS, �� ������������ ��� ��������

//���� ����� ������ ���
#define REJIM_UVD_STATUS ((unsigned short)0x4)

//���� ����� ������ RBS
#define REJIM_RBS_STATUS ((unsigned short)0x8)


//������ ������� ��������� ����������
#define ABS_VYSOTA_STATUS ((unsigned short)0x20)


//������� ������ ��� ��� (������������� �� ��������� ��������� ����������� ��� ���)
#define VISIBLE_STATUS ((unsigned short)0x100)

//������ ����� ������ ������
#define LOJNY_VYSOTA_STATUS ((unsigned short)0x200)

//������ ����� ������ �����
#define LOJNY_NOMER_STATUS ((unsigned short)0x400)

//������ �������� � ������� ��������� ������
#define V_OBLASTI_VYDELENIA_STATUS ((unsigned short)0x1000)

//������ ����, ��� ������ ��������� � ���������������� �������� � ������� �������
#define BLIZKO_STATUS ((unsigned short)0x2000)


//������ ����, ��� ������ �������� ����������
#define DROBLENIE_STATUS ((unsigned short)0x4000)


//������ ����, ��� ������ �������� ������ ��������
#define LOJNY_STATUS ((unsigned short)0x8000)





#define KANAL_PS_STATUS ((unsigned short)0x3)


//������� ��� ������� 2
//������ ��� �������
#define PRIVYAZAN_OTSCHET_STATUS2 ((unsigned short)0x8)


/*�������� �������� ������ ��� ����*/
//�� ���
#define LETIT_NA_RLS_STATUS2 ((unsigned short)0x01)
//�� ���
#define LETIT_OT_RLS_STATUS2 ((unsigned short)0x02)

//���� �� ����������� ������ PS ��� ������� ������� S ��� ���� ������ S ���
//������� ������� PS
#define DUBL_S_PS_STATUS2 ((unsigned short)0x10)

//�� ��������� ������
#define NE_PODVIJEN_STATUS2 ((unsigned short)0x80)




//��� ��������� �������� ��������, ������������ ������ ��������
#define REZERV1_FOR_ZONA_STATUS2 ((unsigned short)0x400)
#define REZERV2_FOR_ZONA_STATUS2 ((unsigned short)0x800)

//�������� ������ �������
//������������ � ������ OBZORY_1
//��� ������� ������������� �� ODIN_BORT_1 � OBZORY_1 -- o_b_1
//o_b_1 - ��� ��������� ������ ������, ���������� 362 ���������� ��������
//������� �������. ��� ������ ������ o_b_1[i] � �������� i, ��� i=0-359, ���������
//������ ������ ������� ������� ����� � �������� �� i �� i+0.99 ��������. � i=360
//�������� ��� �������. o_b_1[i][j] , ����� j ���������� ������ ������� � ������� o_b_1[i]

//������ ������� �������������� ��������� �������.
//1 ��� ������� - ��������������� ������, ��� ��������� ������ (���������� Next,Prev);
//2 ��� ������ - ��� ����� ������, ������������� ��� ������ ������ (���.NextVisual)
//3 - ��������� ParnOtschet1 � ParnOtschet2 ��� �������� �������� �������
// � ����� ������� P, S � PS ����������� � ������ ������� (��� ��.������).
//���� ��������� ������ ����� ������� o_b_1[i][j] � ������ ������ ���� o_b_1[i1][j1],
//�� ���������� ���� ��� o_b_1[i][j].Next=j1*362+i1. ���������� � ���  NextVisual �
//ParnOtschet1 � ParnOtschet2

struct ODIN_BORT_1
{
  short NFile;           //����� ����� 0,1,2,3... - ��� ������ ��������� �����
                         // ..
  long Smesh;            //����� (��������) ������ � ������ ������� � �����, ������������ ��� ������
  short OprVysota;      //������������ �������� ������ - �� ������� ���� ��������
  short IstVysota;      //������, �� ������� ����������������, ������ ��� ���������� ����
  short IndxBrt;         //������ �����
  short IndxVtv;         //������ �����
  long NomBort;          //����� ����� ������������
  long SektorNomer;     //���� ���� ������ �� 4 ����� �� ��������
                  //��� �������� �� 0 �� 89 �������� �������� ������ 4
                  //��� �������� �� 90 �� 179 �� �������� ������ 4 � ��������1
                  //��� �������� �� 180 �� 269 �������� ������ 4 � ��������2
                  //��� �������� �� 270 � �� 359, � ����� ������ - � �������� 3

  double Azmt,Dlnst;      //������ � ���.�� � ��������� � ��
  double Time;           //����� � ����� - ��� �������� �� ��������� ����� �������������
                         //�� 24 ����

  double dfSignalPK;    //������� ������� ��

  unsigned short Status; //������ ����������. ���� ���������� ��������������� ��������
                  //�������� �������� ���� ���������� ���������, ���������������� _STATUS


  unsigned short Status2; //���������� � ������ 2. ���� ���������� ��������������� ��������
                  //�������� �������� ���� ���������� ���������, ���������������� _STATUS2


  long X,Y;             //���������� X, Y (��� ��� ������ - �� �����) - ��� ������� � �


  long NextVisual;     //��������� ���������� - ����� ������������ ��� ������


  long Next,Prev;      //��������� ���������� �� �����


  long AbsNext, AbsPrev;        //��������� �� ���������� � ������� �������


  long  ParnOtschet1;  //������ ������ ������ - ��������� �� ������ ������, ���� ����� ����
                       //���� ������ ���� S, �� ������ ������ PS.
                       //���� ������ ���� PS, �� ������ S
                       //���� ������ ���� P, �� ������ PS

  long  ParnOtschet2;  //������ ������ ������ - ��������� �� ������ ������, ���� ����� ����
                       //���� ������ ���� S, �� ������ ������ P.
                       //���� ������ ���� PS, �� ������ P
                       //���� ������ ���� P, �� ������ S

  long ParnForDubl;        //��������� ������ ��� �������


  long poryadk_nomer;   //����� � ������� �����������


  long nomer_obzora;    //��� ������� (�� ��� �������)

};




/*������ ��������:
  ������ ������� �������, ������ ������� �������,
  ������ �������� �������.
*/
struct TROIKA_BORT_1
{
  short Nomer1, Nomer2, Nomer3; //������ ��������
  long  Indx1,  Indx2,  Indx3;  //�������
};


/*�������� �����. ��������� ����� ��������.
������ ���������� ����� ���������� � ������� ���������� ���� ����������� ���� ��������
����� ������������ ���.
*/




/*�������� ���������� �������� ���������� ������ ������� � ������-���� �����*/
struct OTNOSHENIE_OTSCHETA_K_BORTU
{
  long Smesh;  //������ � ����� �������
  double Psi;     //�������� ������������
  long IndxBrt;   //������ �����
  bool IsRasm;
};


/*���� ��������. ����� �������������� ��� ������ ����������� �����������*/
struct PARA_OTSCHETOV_OB1
{
  ODIN_BORT_1 *OB1_1;   //��������� �� ������ ������
  ODIN_BORT_1 *OB1_2;   //��������� �� ������ ������
};




//���� �������� ����������� �� ������������ ��������
struct VETV_INFO
{
        long smesh;
        double min_dlnst_vis, max_dlnst_vis;
        int isView;
};


/*�������� ����� ������*/
struct ONE_TRASSA_INFO
{
         unsigned long ulNumber;       //������ ������� ������ Number...
                      //����� ������� ����: 0x02 - RBS, 0x01 - ���, 0x00 - ��������� ����� -- �����������
                      //0x12 - RBS - ����������� ������� RBS, 0x11 - ����������� ������� ���
                      //���� ����� ����� 0x14000000 - ��������� �� �����������
                      //���� ����� ����� 0x15000000 - ��� �� �����������
                      //���� ����� ����� 0x16000000 - RBS �� �����������
                      //���� ����� ����� 0x17000000 - ���������� ������� �� //���� ��� ��������
                      //���� ����� ����� 0x18000000 - ��� ����������  //���� ��� ��������
                      //���� ����� ����� 0x19000000 - RBS ����������  //���� ��� ��������
                      //���� ����� ����� 0xFFFFFFFF - ������, ������������� �� ����������� �������


                      //����� �������������
         long lFirstIndex;       //������, ����������� �� 1 �������
         long lLastIndex;        //������, ����������� �� ��������� �������
         long lCurIndex;         //������� �������
         long lFirstApproxIndex;  //������ ����� ���������, ������� ��������������� ��� �������������
         long lLastApproxIndex; //��������� ����� �������
         int state;              //0 - ����� ������ ����� 10 �������,
                                 //1 - ������ ������ ��� �������� � �� ���������
                                 //-1 - ���������� ������, ��� �� ���������

         int n_of_otmetok;      //����� ������� - �� ������
         int n_of_otmetok_p;  //����� ���� �������
         int n_of_otmetok_ps; //����� ������� ps
         int n_of_otmetok_s;  //����� ������� s
         struct TAPPStateKoef state_koef_koef;
         int trassa_indx;
         double dfLastTestKoef;         //��������� ����������� �����������
         long lLastAddingSmesh;               //��������� �� ��������� �����������
                                        //������� �������� tpAddOtmetkaToTrassa
         long first_poryadk_nomer, last_poryadk_nomer;

//�������� �� ��� ������. 1 - ������ ���������������, 0 - ������ �� ��������������� ��� ������
         int isSelected;


//������� ��� �������, ��� ����������� ��� ������������ ���������
         VETV_INFO *vetv;
         int n_of_vetv;

};







//����� OBZORY_1
/*OBZORY_1 ������������� ��� ����������� �������� �������� � ������� ���������
������.
����������:
 1) �������� ������ �� ������:
   LoadFiles (������ Traekt.cpp)


 2) ����������� �������� ������ �� ����������:
   �� ��������, ���������, ������,  ������� ������, �� ������,
   �� ������� (P,S ��� �����. PS), �� ������� (���, RBS ��� ���), �� ������������.
   ��������� ������������: MakeFilter (�������) --> ProveritForVisibleOdinOtschet
 ����� ������� ������.
    �������, ������� ������������� ���� ��������� ����� ������ VISIBLE_STATUS,
  � ����� ������������ �� ������ NextVisual ���������� ODIN_BORT_1. ������ ������
 FirstVisual.

 3) �������������� ����������� ��������� � ���� ����������� ��� �������� �� ����� � ������.
   ������� ���������:
    a) VyvodNaEkran (Traekt.cpp) ��� ������ �� �����
    �) VyvodDataNaPrinter (Traekt2.cpp) ��� ������ ������ �� ������


 4) ����������� ���������� �������. ��������� VydelennayaOblast.


 5) ������� ���������� ������� � ��, ��� ��� ���������� �������
    ���������: ClearSelection � ClearNotSelection (Traekt.cpp)




������, ��� ���������� ������� ������ OBZORY_1
1) Traekt.cpp - ������� �������� ������
2) Traekt2.cpp - ������, ��� ����������� ������ ���� �������� ����: "����", "������", "����� ��"
3) TraektAnimate.cpp - ���������� �������� ��� ��������
*/

struct OBZORY_1
{
private:
   long last_poryadk_nomer;
   double PromFTime; //�����-�� ������������� �����
   long NomerFTime;  //����� �������, ���������������
   double LastTime;
   short LastNomer;  //��������� �����
   double ErrLastTime;
   short ErrLastNomer;

   int LastChas;   //��������� ���
   int LastMinuta; //��������� ������
   int NomerSutok; //
   long LastNomerObzora;    //��������� ����� ������
   int LastKanal;           //1 - P, 2 - S, 3 -PS
   double LastAzmtFromNomerObzora;
   double LastTimeForNomerObzora;

//������ �������� FILE* ��������
   AnsiString *fps;
   int N_of_fps;  //����� �������� ������


  //����� ����� �������� 362 ��� ������� ������� �� 0 �� 359, � ����� - ��� 360
  ODIN_BORT_1 **o_b_1; //������� ������
  long *N_of_o_b_1;    //�����  ������
  long *MaxN_of_o_b_1;    //����� ������ ������������ (������ 1000)
                     //��� ���������� N_of_o_b_1[i]==MaxN_of_o_b_1[i] ������������� �� 1000

//��� �������������� �����������
  long FirstVisual;  //������ ������ ������� ����� ������������
  long LastVisual;   //��������� ������ ������� ����� ������������



//��� ����������� ��������
  long FirstAbs;                //������ ��������� �������
  long LastAbs;                 //��������� ��������� ��������




//������ ��� �������������
	FILE *curOpenF;
	int curIndxOfOpenF;

//��������� ��� ����������� ��������� ������ ���� ��� ������� ���������
  FILE *fpDataBack;  //��������� ������ ��� ��������
  AnsiString VremFN0;


//���������� � ������.
   struct ONE_TRASSA_INFO  *otis;       //���������� � �������
   int n_of_otis;
   int max_n_of_otis;

   int indx_otis_pk_not_priv;
   int indx_otis_uvd_not_priv;
   int indx_otis_rbs_not_priv;


   int isMakingByAnalisisFiles;                     //���� ��� �����, ������� �������� � ����������
                                //���������� ������ �� ��������


   struct ONE_TRASSA_INFO  **work_otis;   //������� ������ ���������� ��� ������ � ��������
   int n_of_work_otis;
   int max_n_of_work_otis;


   struct ONE_TRASSA_INFO  **sort_otis;   //������� ������ ���������� ��� ������ � ��������
   int n_of_sort_otis;
   int n_next_sort_indx;



   long tpNextNP;
   long tpPrevNP;


   int nomer_for_99999_0;               //������ ��� ���������� ������

protected:
  int ProveritForVisibleOdinOtschet(long NomerAzmt,
                                    long Indx);

  int ProveritNastroikiOdinOtschet(long NomerAzmt,
                                    long Indx);


/*�������������� ���������, ���������� �� �������� MaxPOtmetokZaObzor*/
//����������� ���� ��������� ����� �����������, ���� ������ ������ ��������
  int VyvodObzor(long FirstPerv,
                 FILE *fpOut);  //�������� ����

 /*�������������� ���������, ���������� �� ��������� MaxSredPOtmetokZaObzor*/
  int GetTimesOfObzorFromSmesh(
                        long smesh,    //�������� ������� ��
                        int dObzors,
                        double &time1,
                        double &time2);

//20131023 ������������� ���������, ������������ ������������� � traekt_privyaz.cpp
//���������, ��� ������ ������� ����������� ����� ������

   int testForOneTrass(ODIN_BORT_1 **obs, int n_of_obs);


//������� ������
   int tpNewTrassa(
                unsigned long ulNomer
                   );



//������� �������� ������
  int tzZavyazatTrassu(long indx1, long indx2);





//�������� ������ - ���������� �� tpZavyazatTrassu
  int tzMakeTrassu(long nomer1, long indx1,
                   long nomer2, long indx2 );

//�������� ������ �� otis'�
  int tpRemoveFromOtis(long nomer, long indx);


//���������� ������� � otis
  int tpAddToOtis(long nomer, long indx,
                  int n_trass);


//���������� ������� � otis ������ � �������
   int tpAddToOtisWithParnye(long nomer, long indx,
                  int n_trass);


//���������� ������� � ������ ������, ���� isMakingByAnalisisFiles=1
  int tpAddToOtisIfIsMakingByAnalisisFiles(long nomer, long indx);


  long tpAddOtmetkaToTrassa(int n_trass,   //����� ������
                        long nomer, long indx,     //����� ����������� �������
                        int isRemovingOther);       //



//��������� ������������� �������
//��������� ������� �������, ������� �� ��������
  long tnGetFirstNotPrivyazan(int isPonly,long *nomer=NULL, long *indx=NULL);

//��������� ���������� �������, ������� �� ��������
  long tnGetLastNotPrivyazan(int isPonly, long *nomer=NULL, long *indx=NULL);

//��������� ���������� �������, ������� �� ��������
  long tnGetNextNotPrivyazan(int isPonly, long *nomer=NULL, long *indx=NULL);

//��������� ����������� �������, ������� �� ��������
  long tnGetPrevNotPrivyazan(int isPonly, long *nomer=NULL, long *indx=NULL);



//��������� ���������� �������, ������� �� ��������
  long tnGetNextMainOtshet(long smesh, long *nomer=NULL, long *indx=NULL);

//��������� ����������� �������, ������� �� ��������
  long tnGetPrevMainOtshet(long smesh, long *nomer=NULL, long *indx=NULL);


//��������� ������� �������� ������ ������ (����� ������)
  long tnGetFirstMainOtshet(int n_trass, long *nomer=NULL, long *indx=NULL);


//���������  �������� ������ ������ (����� ������)
  long tnGetLastMainOtshet(int n_trass, long *nomer=NULL, long *indx=NULL);



//����� ��������� � ������ ������� ������� ������ trass_n
  long tnGetMoreAndNearestOtshetTrassy(long smesh, int n_trass);


//����� ��������� � ������ ������� ������� ������ trass_n
  long tnGetLessAndNearestOtshetTrassy(long smesh, int n_trass);









//��������� ������������ ��� �������������
  int tkGetKoefForApprox(int n_trass,
                            int revers_time,    //�������� ������ �������
                            double dfInterval,
                            long nomer,
                            long indx);

//���������  ������������ � �������������
 double tkGetKoef(int n_trass, long nomer, long indx);


 //���������  ������������ � ������������� ��� ��������� ������
 double tkGetKoefPovtor(int n_trass, long nomer, long indx);



//����� ���������������� ���������
int tkFindInterval(
                        int n_trass,
                        long smesh,        //������� ��������  ��������������� ������
                        double dfInterval,
                        long prev_smesh1, long prev_smesh2,
                        long &smesh1, long &smesh2);



int tkFindIntervalPovtor(
                        int n_trass,
                        long smesh,        //������� ��������  ��������������� ������
                        double dfInterval,
                        long &smesh1, long &smesh2);


//�������� � �������� ������, ���� �����
  int tpTestForKillTrassa(int n_trass);


//������ ����������� �� ��������, ������ ������� �������� �� �������� ���������� �����
   int tpObhodPryamoy();


//������ ����������� �� ��������, ������ ������� �������� �� �������� ���������� �����
   int tpObhodPryamoyPonly();



//�������� ����������� �� �������� ��� ������� - ������ ������ ������� ���������� � ���������� �������
   int tpObhodPovtorny();



//������� ������������� ����� � ���������������� ��������� �����
//������ ����� ���������� ��������������� �� tpObhodPovtorny
   int tpSendToNeedNumBort(long smesh);


//������� ���������� ������ ������������ ���������
//������� ����� ���� � ��� �� ����� � ���� � ��� �� ������� � ���������
   int tpFindKO_Borts();

//��������� 15.09.2015
//�������� ��� ������, �� ����� ���������, ����
   int tpResetAllTrassa();






   int  tzTryZavyazatTrassu(int isPonly,
                        long smesh);




//��������� ��������� �� ������ ������� ����� ����� � ��������
//����������: 1, ��� ��������� - � ����� ������, 0 - �� �������������
//-1 - ����� ��������� ������� ������ �����������
   int tbIsBlizkieOtmetki(long smesh1,  //����� ������ �������
                          long smesh2,  //����� ������ �������
                          double dDAzmt_grds, //���������� ������� � �������
                          double dDDaln_km     //���������� ������� ��������� � ��
                          );



/*�������� ���� ������� ����������� � ���������������� �������� �� �������� �������*/
    int tbFindBlizkieOtmetki(
    //������� ������
                             long smesh,               //�������� ������ ������
                             double dDAzmt_grds,       //������� ��������������� ������ � ��������
                             double dDDaln_km,     //������ ������ ��������� � ��
                             int isBackward,       //����� �����
                             int isForward,        //����� ������

//��� �������� ������
                             long *lFoundSmesh,         //������ �������� �������
                             int sz_lFoundSmesh);       //������ ����� �������




//����������� ����������� ��� ������
   int tnaprFindNapravleniyaTrassy(int n_trass);

//����������� ����������� ��� ���� �����
   int tnaprFindNapravleniya(void);


/*����� ��� ����� � ������� ��������� ������� ����� ����� � �� ��������*/
     int   tnbTestForZeroBort(void);


/*��������������� ����� � ��������� ��*/
     int tnbRenumberPKTrass(void);


public:

//���� ������
  double TO;   //���� ������ � �����



//��������� �� ������ � ������ ������ �� 180 ��������
  int is180;

//��� ������ - ������� ���� ��� �������� ����
  bool IsInPlot;  //������� ����? - ������������ �� ���������� ��������...




/*�������� ����������� ���������� ���������� �������� � ����������� ������� ������*/
  bool IsVisNumber;  //���������� ����� ����� ����� ������ ������� �������,
                   //������������ � ������� ������ ����� � ������� ������������
                   //� ������������ �������

  bool IsVisTime;    //���������� ����� ����� ������ ������� �������,
                   //������������ � ������� ������ ����� � ������� ������������
                   //� ������������ �������


//1) ����������� �������� ������ �� ����������:
  bool IsCheckNumber; //��������� ������ ������
  bool IsCheckTime;   //��������� �����
  double Time1,Time2; //�������� �������
  bool IsCheckVys;      //��������� ������
  bool IskluchVys;      //��������� �������� (���� false, �� ��������)
  long Vys1,Vys2;       //�������� � ������
  long DopDVys;         //���������� ��������� ������
  bool IsCheckAzmt;      //��������� �������
  double Azmt11,Azmt12;  //�������� 1 ��������
  double Azmt21,Azmt22;  //�������� 2 ��������
  bool IsCheckDlnst;     //��������� ���������
  double Dlnst1,Dlnst2;  //��������� ����������
  bool KanalP,KanalS,KanalPS; //������ P, S �/��� PS
  bool RejimUVD,RejimRBS;    //������ ��� ��� RBS
         //���� ������� ����� S ��� ����� PS, �� �������� ������
//��������� �� ����������� ������
  long Napravlenie;  //0 - �� ��������� �����������, 1 - ������� �� ��� �����,
                      //2-������� �� ��� �����


  long NomBortKO_UVD[10],N_of_NomBortKO_UVD; //������ ������ ����������� ������� ���
  long NomBortKO_RBS[10],N_of_NomBortKO_RBS; //������ ������ ����������� ������� RBS


  bool IsCheckSignalPK;
  double dfMinSignalPK;



//������ ����������� �����������
  int DiametrMetok;


//�������� ����� ������
  bool VydelyatSevera;


//������� ������ ��� ������������ ������ ���� (��� ����������� ��������)
  long VysotaRLS;   //������ ��������� ��� ������������ ����




//����� ���������
  char MestoIspytaniy[91];


//������ ����������� ���� ��� ������ ��� ������
   double dRazmerOknaDlyaZonyObzora;



//�����
  TColor OblastCol;  //���� �������
  TColor Kolca1Col;  //���� ����� �������� (����� 50 ��)
  TColor Kolca2Col;  //����� �������������
  TColor Rad90Col;   //����� ���.����� 90 � 180 ��������
  TColor Rad30Col;   //����� ���.����� 30 � �.�.
  TColor RamkaCol;   //���� ����� ���������
  TColor P_Col;      //����� ������� ���������� �����
  TColor S_Col;      //����� ������� ���������� ������
  TColor PS_Col;     //����� ������� ������������
  TColor RS_UVD_Col; //����� ������� ����������� � ������. �������� ���
  TColor RS_RBS_Col; //����� �������... RBS
  TColor RS_P_Col;   //����� ������� �� ����. �����
  TColor Text_Col;   //���� ������


/*�������������� ������ ��� �����������*/
  long ChisloOtschetov;    //����� ����� �������� � �����
  long ChisloFilterOtschetov;
  long ChisloVydelOtschetov;    //����� ��������, ������� ������ � ������� ���������
  long ChisloVydelOtschetovP;
  long ChisloVydelOtschetovPS;
  long ChisloVydelOtschetovS;
  long ChisloBlizkihOtschetovP;
  long ChisloBlizkihOtschetovS_UVD;
  long ChisloBlizkihOtschetovS_RBS;


  long ChisloPOtschetov;   //����� �������� ���� P - ���� �� ������������
  long ChisloSOtschetov;   //����� �������� ���� S - ���� �� ������������
  long ChisloPSOtschetov;  //����� �������� ���� PS - ������������ ��� ������� ����� ����������� ��������
  long ChisloPSOtschetov_Privyaz; //����� ����������� ��������
  long ChisloRBSOtschetov; //����� �������� RBS
  long ChisloUVDOtschetov; //����� �������� ���

 //�� ������� �����...
 //������ ������� ����. ������� ����� ����� ������
  long *ChisloO;    //����� ��������
  long *ChisloP;    //����� �������� ���� P
  long *ChisloS;    //����� �������� ���� S
  long *ChisloPS;    //����� �������� ���� PS
  double *PS_k_PS_S; //��������� ����� �������� PS � ����� S � PS
  long *ChisloProp;    //����� ��������, ��� ������������� ����� �� ���� ��������
  long *ChisloDrobl;   //����� ��������� (���������� � ���� �����)
  long *ChisloDubl;    //����� ������������

//����������� ����������� ��� ������������� ����������
  double RSA_RBS1,RSA_UVD1,RSA_P1,RSD_RBS1,RSD_UVD1,RSD_P1;

//����������� �����������
  double RSA_RBS2,RSA_UVD2,RSA_P2,RSD_RBS2,RSD_UVD2,RSD_P2;


//����� ��� ������������ ������
  bool IsAnimNomer;  //���������� ����� ����� ��� ������������
  bool IsAnimVysota; //���������� �������� ������ ��� ������������
  bool IsAnimTime;   //���������� ����� ��� ������������
  bool IsHvost;      //��������� ����� (������� ���������� �������)
  int DlinaHvosta;   //����� ������


/*��� ������ ������ � ����, ��������� ������ � ���� ��������*/
  bool IsLojnyyInfoVyvod;   //�������� ������ ������
  bool IsIstinnyRightDopData; //�������� �������� �������������� ������, ������� ���������� ���������

//��� ������ ���������� ���������
  bool FindRadialTraekt;
  bool FindForP_PS;       //����� ��� ���������� P ��� PS, ���� false, �� ��� ������ S
  double MaxMinD,MinMaxD;



/*��������� ��� �����������, ������� ������ ��� ���������*/
//�������� � ��������� Add
  int CLSutkiFirst, CLChasyFirst, CLMinutyFirst, CLSekundyFirst;
  int CLSutkiCur, CLChasyCur, CLMinutyCur, CLSekundyCur;



//��������� 11.09.2008
  int cPechatPK;        //���� ������ ��
  int cPechatVK;        //���� ������ ��
  int cPechatObjed;     //���� ������ �����

/*���������: 20090921.
����������� �������� ������������ � ����������� ��������� ��� ���������� �������
������ ������. ���� ����, ����� ������� ��� ��� ���������: ������������ ���������
��� �� � ������������ ��������� ��� ��.
*/
   bool isSetMaxDalnGOST; //�������� ������������ ��������
   int iMaxDalnGOST_PK;    //������������ ��������� ��� ��
   int iMaxDalnGOST_VK;    //������������ ��������� ��� ��


/*������ ���������� �������������� ����������*/
  AnsiString FIO_User,Podr_User;  //�������������
  AnsiString Date_Input;  //���� ����� ����������
  AnsiString Dop_Info;    //�������������� ����������

//���������� ��� ������ ����������� �����������
  double *Azmt_A, *Dlnst_A;



//���� �� ��������
  bool IsWasPrivyazka;


//������ ��� �������� ���������� ������
  PARA_OTSCHETOV_OB1 *POOB1_p;
  int N_of_POOB1_p;
  bool IsWasSearchingRS_P;  //��� �� ����� P

//������ ��� �������� ���������� ������ uvd
  PARA_OTSCHETOV_OB1 *POOB1_uvd;
  int N_of_POOB1_uvd;

//������ ��� �������� ���������� ������ rbs
  PARA_OTSCHETOV_OB1 *POOB1_rbs;
  int N_of_POOB1_rbs;

  bool IsWasSearchingRS_VK; //��� �� ����� ��� ���������� ������


/*��� ����������� ������� ��� ��������*/
  char cCurTimeAnim[9];

/*������ � ����������, �� ���� ����� ����������� ����� � ����� ������������� ����*/
  long *Pereskoki;     //��������� �� �������, �� ���� ������� ������� �����
  long *PrevPereskoki;
  int N_of_Pereskokov;  //����� ����������.



/**�������� � ����� � ������� �������� ������������*/
   double approx_interval;              //�������� ������������

/*����� ��� ������ ������*/
   double period_for_out_trass;         //����� ������ ������


//������������ ��������� �� ����
   long GlobalGaugeMaxTraekt;

//������� �������� ������   
   long GlobalGaugeCurTraekt;

//���������� ������ ������� ������� � �������� ����� ������������ ���
/*
  int LeftKm,
      RightKm,
      BottomKm,
      TopKm;
*/
  OBZORY_1();
  ~OBZORY_1();

//�������� �� �������� �����
  long NextOtschetBort(long Nomer, long Indx, //������� �������
                         bool IsP,bool IsS, bool IsPS //����� ������� �������������
                       )
  {
     unsigned long TekKanal;
     long Nxt,Nmr,Ndx;
     bool IsMojno[4];
     IsMojno[0]=false;
     IsMojno[1]=IsP;     IsMojno[2]=IsS; IsMojno[3]=IsPS;



     if(Nomer<0||Nomer>359)return -1;
     if(Indx<0||Indx>=N_of_o_b_1[Nomer])return (-1);

  //���� ������ ������ �� ���� �������������
     Nxt=o_b_1[Nomer][Indx].Next;

     while(Nxt>=0)
     {
       Nmr=Nxt%362; Ndx=Nxt/362;
       TekKanal=o_b_1[Nmr][Ndx].Status&3;
       if(IsMojno[TekKanal])
       {
          break;
       }
       Nxt=o_b_1[Nmr][Ndx].Next;
     };
     return Nxt;
  };





  long PrevOtschetBort(long Nomer, long Indx,
             bool IsP,bool IsS, bool IsPS //����� ������� �������������
                      )
  {
     unsigned long TekKanal;
     long Prv,Nmr,Ndx;
     bool IsMojno[4];
     IsMojno[0]=false;
     IsMojno[1]=IsP;     IsMojno[2]=IsS; IsMojno[3]=IsPS;


     if(Nomer<0||Nomer>359)return -1;
     if(Indx<0||Indx>=N_of_o_b_1[Nomer])return (-1);

   //�������� �� ���������� ������
     Prv=o_b_1[Nomer][Indx].Prev;
     while(Prv>=0)
     {
       Nmr=Prv%362; Ndx=Prv/362;
       TekKanal=o_b_1[Nmr][Ndx].Status&3;
       if(IsMojno[TekKanal])
       {
         break;
       }
       Prv=o_b_1[Nmr][Ndx].Prev;
     };
     return Prv;
  };


  long NextOtschetBortS_ili_P(long Nomer, long Indx)
  {

     return 1;
  };





//�������� �� ������ ������ �����, ������ ���� ���� S �� �� S, ����� �� P
  long FirstOtschetBortS_ili_P(long BrtIndx)
  {

     return 1;
  };


/*������������ ������, ������� ����� ���� �������� � ����, ���� ���� ���� ���*/
  AnsiString GetStrokaFrom(long NomerA,long Indx);
  AnsiString GetStrokaFrom2(ODIN_BORT_1 *OB1);
  AnsiString GetStrokaForIskaj(long NomerA,long Indx);

//����� �����
  int NewFiles(AnsiString *fn, int N_of_fn,double TempObzoraVSekundah);



//������������ ������
  void FreeAll(void);


//�������� ����� ������
  int Add(int NomFile, //����� �����
          const DANN &Dan  //������ �� ������ �� �����
          ); 



//������� ���������� �������
  int ClearSelection(void);//������� ���������� �������

//������� ������������ �������
  int ClearNotSelection(void);//������� ���������� �������


//������� ��������������� ��������
  int MakeFilter(void);

//������� ������� � ����������� ���������� � ������������ ����������
  int OzenkaMinMaxAndMaxMin(void);



//��������� �������� ������... �������� ������� NewFiles, ����� ���������������
//��������� Add ��� ���� ��������, �������� ��� ������� �� �����
  int LoadFiles(AnsiString *file_names, int N_of_fn,double TempObzoraVSekundah);


//������� ������ �� �����
//���� ����� ��� �������, ��
  int VyvodNaEkran(TImage *Image,
                   double LeftKm, double BottomKm,
                   double RightKm, double TopKm,
                   TCGauge *CGauge
                   ); //��� ����� � ������� ����������� ������



//��������� ������ ������
  int ZapolnitSpisok(TStrings *Strings, //������ ����� ������� ���� ���������
                        bool SortNomer);//����������� �� ������� (����� �� ������� ���������)


//���������� ������ � ������� ������
  int GetDataAboutSelectedNumber(TListBox *ListBox, //���� ���, ���������� ������ ������
                                    bool SortNomer);

//�������� ������, ������� ������� traekt_nomborts.cpp
  bool GetSelectedInSortedBorts(int indx_sort_otis); //������ ����� ������� ���� ���������


//�������� ����� ����� �� �������  traekt_nomborts.cpp
   int GetNomerSelectedBort(int indx_sort_otis);

//�������� ����� ���������� ������ traekt_nomborts.cpp
   int GetNumberOfSortedBorts(void);


  int GetTimeAndSutki(int Sutki1,int Chasy1,int Minuty1,int Sekundy1,
                      int Sutki2,int Chasy2,int Minuty2,int Sekundy2);


//�������� �������� ���������� � ������� ���������
//������ ������ � ��� ��������
//������� ���������� �����, ������� ��������

  int VydelennayaOblast(
     double *Xv, double *Yv, int N_of_v //���������� ����� ������������ ���
                       );


/*���������� ������ � ����� � ������������ ������. ��� ���������� ��������.*/
  int TempSaveAllDataAndFreeMemory(FILE *fp);
  int TempRestoreAllData(FILE *fp);





/*��������� ������ �� ��������*/
//��������� ������ � ������ ������ - ���������� ������
  long GetFirstFilterOB(
                        bool IsSelectedOblast //� ������� ���������
                      )
  {
      long Nom,Indx;
      long i;

      if(IsSelectedOblast)
      {
          i=GetFirstFilterOB(false);
          if(i<0)return (-1);
          Nom=i%362;
          Indx=i/362;
          if(o_b_1[Nom][Indx].Status&V_OBLASTI_VYDELENIA_STATUS)
          {
             return i;
          }
          return GetNextFilterOB(i,true);
      }
      i=FirstAbs;
      while(i>=0)
      {
          Nom=i%362;
          Indx=i/362;
          if(o_b_1[Nom][Indx].Status&VISIBLE_STATUS)
          {
                return i;
          }
          i=o_b_1[Nom][Indx].AbsNext;
      }
      return (-1);
  };



  long GetNextFilterOB(
                       long Tek,
                        bool IsSelectedOblast //� ������� ���������
                       )
  {
    long Nom,Indx;
    long i;
    if(IsSelectedOblast)
    {
          i=GetNextFilterOB(Tek, false);
          while(i>=0)
          {
                Nom=i%362;
                Indx=i/362;
                if(o_b_1[Nom][Indx].Status&V_OBLASTI_VYDELENIA_STATUS)
                {
                    return i;
                }
                i=GetNextFilterOB(i,false);
          }
          return (-1);
    }
    Nom=Tek%362;
    Indx=Tek/362;
    i=o_b_1[Nom][Indx].AbsNext;
    while(i>=0)
    {
        Nom=i%362;
        Indx=i/362;
        if(o_b_1[Nom][Indx].Status&VISIBLE_STATUS)
        {
            return i;
        }
        i=o_b_1[Nom][Indx].AbsNext;
    }
    return (-1);

  };

/*��������� ������ �� ��������*/
//��������� ������ � ������ ������ - ���������� ������
  long GetLastFilterOB(
                        bool IsSelectedOblast //� ������� ���������
                      )
  {
      long Nom,Indx;
      long i;

      if(IsSelectedOblast)
      {
          i=GetLastFilterOB(false);
          if(i<0)return (-1);
          Nom=i%362;
          Indx=i/362;
          if(o_b_1[Nom][Indx].Status&V_OBLASTI_VYDELENIA_STATUS)
          {
             return i;
          }
          return GetPrevFilterOB(i,true);
      }
      i=LastAbs;
      while(i>=0)
      {
          Nom=i%362;
          Indx=i/362;
          if(o_b_1[Nom][Indx].Status&VISIBLE_STATUS)
          {
                return i;
          }
          i=o_b_1[Nom][Indx].AbsPrev;
      }
      return (-1);
  };



  long GetPrevFilterOB(
                       long Tek,
                        bool IsSelectedOblast //� ������� ���������
                       )
  {
    long Nom,Indx;
    long i;
    if(IsSelectedOblast)
    {
          i=GetPrevFilterOB(Tek, false);
          while(i>=0)
          {
                Nom=i%362;
                Indx=i/362;
                if(o_b_1[Nom][Indx].Status&V_OBLASTI_VYDELENIA_STATUS)
                {
                    return i;
                }
                i=GetPrevFilterOB(i,false);
          }
          return (-1);
    }
    Nom=Tek%362;
    Indx=Tek/362;
    i=o_b_1[Nom][Indx].AbsPrev;
    while(i>=0)
    {
        Nom=i%362;
        Indx=i/362;
        if(o_b_1[Nom][Indx].Status&VISIBLE_STATUS)
        {
            return i;
        }
        i=o_b_1[Nom][Indx].AbsPrev;
    }
    return (-1);

  };



long GetFirstOB()
{
     return FirstAbs;
};


long GetLastOB()
{
    return LastAbs;
};


long GetNextOB(long Tek)
{
   if(Tek>=0)
   {
        return o_b_1[Tek%362][Tek/362].AbsNext;
   }
   return (-1);
};

long GetPrevOB(long Tek)
{
   if(Tek>=0)
   {
        return o_b_1[Tek%362][Tek/362].AbsPrev;
   }
   return (-1);
};

//��������� ��������� ������� �� ������
long GetOdin_Bort(long Tek,
                ODIN_BORT_1 *OdinBort1,
                long *IndxBortMult100000PlusNomBort=NULL
                        )
{
  int indxBrt;
  unsigned long ulNombort;
  if(Tek<0)return (-1);
  int Nom,Indx;
  Nom=Tek%362;
  Indx=Tek/362;
  if(Indx>=N_of_o_b_1[Nom])
  {
     return (-1);
  }
  indxBrt=o_b_1[Nom][Indx].IndxBrt;
  ulNombort=otis[indxBrt].ulNumber;
  memcpy(OdinBort1,&(o_b_1[Nom][Indx]),sizeof(ODIN_BORT_1));
  if(IndxBortMult100000PlusNomBort)
  {
        *IndxBortMult100000PlusNomBort= (long)((ulNombort&0x00FFFFFF)%100000)+
                                   ((long)(indxBrt%20000))*100000;
  }                                 
  return 1;
};

//����� ������� ������, ���� ����... �������� ������� ������� -
//�������� ���� ������ ���� S, �� ������ ������ ����
   int FindVseParnyeOtschetyNomBort(void);  //������ �����





/*���������� � �������� ������ � ����, ������� ����� � NastroikiCfg*/
  int SaveOption(void); //���������� �������� � ���� - ���������� 1, ���� ������
                        //����� 0

  int LoadOption(void); //���������� �������� � ���� - ���������� 1, ���� ������
                        //����� 0



/*��������, ���� �� ������ � ������ �������, ������� ����������� �����
���������� 1, ���� ��� ���� ����� 0.*/
//kill_matv
  int ProverkaIndexBortSektor(long Sektor1,
                                      long Sektor2,
                                      long IndxBrt,
                                      bool IsP, bool IsS, bool IsPS);
                                      

//����� ������� � ������ �������� ����������� �������
//���������� ����� ��������
  int FindBlizkieOtschety(
                           double AzmtO,      //��������� ������
                           double DlnstO,     //��������� ���������
                           long NomerObzoraO, //����� ������
                           long TekIndexesO[], //������ ������� ��������� ��������
                                              //������ TekIndexesO �� ����� 360,
                                              //����� ��������� ������ ����� ���� ������
                                              //TekIndexesO=0, ����� ����� �������� ������� � 0 ��������
                           double dAzmt, double dDlnst, //������������ ������� �������� � ����������
                           bool FindKanalP,   //��������� ������ ���������� ������
                           bool FindKanalS,   //��������� ������ ���������� ������
                           bool FindKanalPS,  //��������� ��������� ������
                           bool IsS_PS_ToUVD, //��������� ������ ���, ���� ��������� �����
                           bool IsS_PS_ToRBS, //��������� ������ RBS, ���� ��������� �����
                           long OprNomBort,    //� ������������ ��������� OprNomBort
                           short  *NomerABlizk, long *IndxABlizk,
                           int MaxN_of_A);

int FindBlizkieOtschetyAzmt(
                          double Azmt1,  double Azmt2,  //��������� ��������
                          double Dlnst1, double Dlnst2, //��������� ����������
                          short RasmAzmtI, //�� ��������������� �������
                          bool FindKanalP,   //��������� ������ ���������� ������
                          bool FindKanalS,   //��������� ������ ���������� ������
                          bool FindKanalPS,  //��������� ��������� ������
                          bool IsS_PS_ToUVD, //��������� ������ ���, ���� ��������� �����
                          bool IsS_PS_ToRBS, //��������� ������ RBS, ���� ��������� �����
                          long OprNomBort,    //� ������������ ��������� OprNomBort
                          long TekIndexesO[], //������ ������� ��������� ��������
                                              //������ TekIndexesO �� ����� 360,
                                              //����� ��������� ������ ����� ���� ������
                                              //TekIndexesO=0, ����� ����� �������� ������� � 0 ��������
                          long SektorNomerO1, long SektorNomerO2, //�������� �������� �������
                          short  *NomerABlizk, long *IndxABlizk, //������� �������
                          int N_of_A, //������� ��� ��������
                          int MaxN_of_A); //������������ ����� ��������

//�������, ������� ���������� �� ��������� FindBlizkieOtschety
//���������� true, ���� ������ NomerA � IndxA ����������� ���� ����������
  bool TestForFindBlizkieOtschety(
                          double Azmt1,  double Azmt2,  //��������� ��������
                          double Dlnst1, double Dlnst2, //��������� ����������
                          bool FindKanalP,   //��������� ������ ���������� ������
                          bool FindKanalS,   //��������� ������ ���������� ������
                          bool FindKanalPS,  //��������� ��������� ������
                          bool IsS_PS_ToUVD, //��������� ������ ���, ���� ��������� �����
                          bool IsS_PS_ToRBS, //��������� ������ RBS, ���� ��������� �����
                          long OprNomBort,    //� ������������ ��������� OprNomBort
                          short NomerA,      //����� �������
                          long IndxA);       //������

//��������� ��������� ����������� � ������ Zavyazka.cpp
/*��� ������� ����������. ����� ��� ������� ������� �����, ������� ������� �����
 ��� �� DD1 ��, �� ����� ��� �� DD2 ��. ������ ��� ������� ��������� � ��������� ������
 ��������� ���������� ����� ����� ����� ��������.*/
 int FindDopustimyeOtsch(short NomerO,
                         long IndxO,
                         double DD1, double DD2,
                         bool FindKanalP,
                         bool FindKanalS,
                         bool FindKanalPS,
                         short  *NomerNujn, long *IndxNujn,
                         int MaxN_of_Nujn);

//���������, ���������� �� ��������� FindDopustimyeOtsch
 bool IzGoditsyaDopustimye(short NomerO,
                         long IndxO,
                         double DD1, double DD2,
                         bool FindKanalP,
                         bool FindKanalS,
                         bool FindKanalPS,
                         short  NomerNujn, long IndxNujn);


//��������� ������ ���� ���� �������� - ��� �������, ��� 2 ������ ��� ��������
//�������� � ������ ����, ��� ��������� �������� � ���������� �������� � �������
 int FindDopOtsch3(short NomerO1,long IndxO1,
                    short NomerO2,long IndxO2,
                    bool FindKanalP,
                    bool FindKanalS,
                    bool FindKanalPS,
                    short  *NomerNujn, long *IndxNujn,
                    int MaxN_of_Nujn);

 int FindDopOtsch3Nazad(short NomerO1,long IndxO1,
                        short NomerO2,long IndxO2,
                        bool FindKanalP,
                        bool FindKanalS,
                        bool FindKanalPS,
                        short  *NomerNujn, long *IndxNujn,
                        int MaxN_of_Nujn);


//����� ���� ����� ��������
 int FindAllTroikiOtschet(
                          short NomerO,long IndxO, //������ ������
                          bool FindKanalP,
                          bool FindKanalS,
                          bool FindKanalPS,
                          double Skorost1,
                          double Skorost2,
                          TROIKA_BORT_1 *Troik,
                          int MaxN_of_Troik);


//�������� - ����� �� ���� ��������  ������������ ����������
//���������� 1, ���� �����. 0, ���� �� �����
//����� N_of_O �������� ������ �����, ������� ����� ��������
 int ProverkaPary(
                  short NomerO1,long IndxO1, //������ ������
                  short NomerO2,long IndxO2,
                  bool FindKanalP, bool FindKanalS, bool FindKanalPS, //������
                  int N_of_O,        //������� �������� ������ ���� ��������� (������ 6)
                  void *FilterData,  //��������� �� ������ ������
                  short *NomerO_out,long *IndxO_out, //��� ������� ����� ���������
                  long TekIndexesO[], //������ ������� ��������� ��������
                                              //������ TekIndexesO �� ����� 360,
                                              //����� ��������� ������ ����� ���� ������
                  long IndxNomBort
                  );

 int ProverkaParyNazad(
                  short NomerO1,long IndxO1, //������ ������
                  short NomerO2,long IndxO2,
                  bool FindKanalP, bool FindKanalS, bool FindKanalPS, //������
                  int N_of_O,        //������� �������� ������ ���� ��������� (������ 6)
                  void *FilterData,  //��������� �� ������ ������
                  short *NomerO_out,long *IndxO_out, //��� ������� ����� ���������
                  long TekIndexesO[] //������ ������� ��������� ��������
                                              //������ TekIndexesO �� ����� 360,
                                              //����� ��������� ������ ����� ���� ������
                  );



//������� ��������
//���������� 1, ���� �����. 0, ���� �� �����
//2 - ���� ��� ������� - ������� ����� ������ ��������� ���������� �� ���� �������
  int ZavyzkaOtschetov(short NomerO, long IndxO, //������ ������
            bool FindKanalP, bool FindKanalS, bool FindKanalPS, //������
            double MinSkorost,   //����������� �������� ��/�
            double MaxSkorost,   //������������ �������� � ��/�
            int N_of_O,   //������� �������� ������ ���� ��������
            void *FilterData,    //��������� �� ������ �������
            short *NomerO_out,long *IndxO_out, //��� ������� ����� ���������
            long TekIndexesO[], //������ ������� ��������� ��������
            long IndxNomBort
            );

//������� ��������
//���������� 1, ���� �����. 0, ���� �� �����
//2 - ���� ��� ������� - ������� ����� ������ ��������� ���������� �� ���� �������
  int ZavyzkaOtschetovNazad(short NomerO, long IndxO, //������ ������
            bool FindKanalP, bool FindKanalS, bool FindKanalPS, //������
            double MinSkorost,   //����������� �������� ��/�
            double MaxSkorost,   //������������ �������� � ��/�
            int N_of_O,   //������� �������� ������ ���� ��������
            void *FilterData,    //��������� �� ������ �������
            short *NomerO_out,long *IndxO_out, //��� ������� ����� ���������
            long TekIndexesO[] //������ ������� ��������� ��������
            );




 int FindDopustimyeOtschNazad(short NomerO,
                         long IndxO,
                         double DD1, double DD2,
                         bool FindKanalP,
                         bool FindKanalS,
                         bool FindKanalPS,
                         short  *NomerNujn, long *IndxNujn,
                         int MaxN_of_Nujn);

//���������, ���������� �� ��������� FindDopustimyeOtsch
 bool IzGoditsyaDopustimyeNazad(short NomerO,
                         long IndxO,
                         double DD1, double DD2,
                         bool FindKanalP,
                         bool FindKanalS,
                         bool FindKanalPS,
                         short  NomerNujn, long IndxNujn);


//������ �����������. �������� ������ ������ � �� ���������� ��������
  int ZadatNapravleniaIZamenitNomera(bool IsP, bool IsS, bool IsPS);


//������ ����������� �� ���� ����� ������ ��� �������� �����
  int ZadatNapravleniaForPlotIn(void);


//��������� ������������� � ������ Traekt2.cpp
//��������� � ���� ������
  int SaveAs(AnsiString FileNameOut);

//��������� ������������� � ������ Traekt2.cpp
//��������� � ���� ���������� ������
  int SaveVydelAs(AnsiString FileNameOut);



//������� ������ � ����
  int IzvlechDataVFile(AnsiString FileNameOut,
                       int Chto  //1 - ���, 2 - RBS, 3 - PS � S, 4 - S, 5 - P
                       );

  int SaveSpisokBortov(AnsiString FileLst);
  int SaveSpisokVydelBortov(AnsiString FileLst);

  int LoadSpisokBortov(AnsiString FileLst);
  int VydelSVisualSpisokBortov(void);



//������ �������� ������
  int VyvodDataNaPrinter(   TCanvas *Canvas,       //
      int Width,int Height,  //������� ��������
      int Xo1, int Yo1,         //������ �������� ������
      long Left, long Bottom,    //���������� ������� ������ ���� ������ ���
      long Right, long Top); //���������� ������� �������� ����





//����������� ������ � ��������� TStrings
  int ShowDatas(TStrings *Strings1);

//����������� ������ � ��������� TStrings
//�������� ������������ ������
  int ShowDatas1(TStrings *Strings);

/*����� �������� ��� ������ ������� ������ ����������� ��������� � ������� �����*/
//1 - ���������� ���� ������ � ���� �� ����, ��� ������ ����
  AnsiString SaveDataForFindIskaj(AnsiString FN);

//2 - ������� ������ � ���� � ���������������
  int BackupDataForFindIskaj(void);


//3 - ����������� ��� � ������� ��� ��������� ����� - ��� ����� ������ ���� �������
//���������
  int RestoreAllAfterFindIskaj(void);



//������������ ���������� P������ �� �����
  int MaxPOtmetokZaObzor(TStrings *Strings, AnsiString *FN);

//������������ ���������� P������ �� �����
  int MaxSredPOtmetokZaObzor(TStrings *Strings, AnsiString *FN);



//����� �������� ���������� ������, ������� �����������
  int FindDroblOtschet(TStrings *Strings, AnsiString *FN);

//������ ����������� �����������
  int FindVeroyatnostObjedinenie(long *N_of_P_S,
                                 long *N_of_PS);



//����� �������� ���������� ������ ����������� � ���������������� ��������
//��������� ����� ������
  int FindBlizkieForP(void);
  int FreeBlizkieForP(void);
  int VklBlzkieBortaP(void);

 //����� �������� ���������� ������ � ���������� ��������
  int VyvodVydelRS_P(TStrings *Strings, AnsiString *FN);

//����� �������� ���������� ������ ����������� � ���������������� ��������
//��������� ����� ������
  int FindBlizkieForVK(void);
  int FreeBlizkieForVK(void);
  int VklBlzkieBortaUVD(void);
  int VklBlzkieBortaRBS(void);
  int VyvodVydelRS_UVD(TStrings *Strings, AnsiString *FN);
  int VyvodVydelRS_RBS(TStrings *Strings, AnsiString *FN);



//� ������ traekt_privyaz.cpp
//������ �������������� ������ ��� ������� ������
  int MakeNotTraekt(bool IsP,bool IsS, bool IsPS);



/*��� ������� ������ ������ ��� ������ � ��������� ������� ���� PS � P
� ������������ �������� ���� S. ��� ������ ��� ���������� ������,
��� ����������� �� 3 -� �������� ������ �����*/
 int PrivyazatPS_And_P_Po_S(void);

/*��������� ��������� ���� P ��� �������� �����*/
//���������� ������� - ���� ������� ���� P
 int FindPForNomer(int IndexNomerB);

//�������������� ��������� � FindPForNomer
//���������� 1, ���� ��������� ������ ������, ��� 0, ���� ���.
 int FindPSOtschetForS(int IndexNomerB, long NomA, long IndxO,
                        long TekIndexesO[], long *NomRet, long *IndxRet);

//�������� �� ���� �������� ������� ������ � � �����, ������ ��� �������, �������
//�������� ������� ��� �����������. ���������� ������ � ���������� ������
//����� ������ ��� ���������� ��������
 int FindLojnAndDrobl(bool TolkoPoS  //���� true, �� ������������� ������ S-�������
                                      //������� ���� PS �� ��������������� ������ �� ���������

                      );

//�������� ����, ���� �� ������������� ������


//���������� ����� ������� �� �������� ��������
 double ReturnProcentNotTraekt(int IndexB, bool IsP,bool IsS, bool IsPS);




//����� ��������� �� ������ �������� � ������ ����� �������������� � ���������� ��������
//��������� ����������� � ������ ProverkaVysot.cpp
  int AnalizVysot(void);  //������ �����


/*��������� ��� �������� �����������*/
/* ������������� ������ ��������
*/
  int InizialAnim(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm);

/*���������� ��������� ��������� ��� ��������*/
  int VychislenieKoordinatAnim(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm);

  int VyvodAllDataAnim(long TekSektorNomer,TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm);

  int VyvodAllDataAnimNazad(long TekSektorNomer,TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm);

//�������� ������ �� ����� �����������
  int ShowDatasAnim(TStrings *Strings, bool Nazad,   //��������
        long Left, long Top, long Right, long Bottom);

  int ShowDatasAnim1(TStrings *Strings,bool Nazad);


//��������� ����� ������� �������
  int NextNSAnim(long DSektor);
  int PrevNSAnim(long DSektor);

  int VyvodAllDataAnimTekSektor(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm);
  int VyvodAllDataAnimTekSektorNazad(TImage *Image,
                  double LeftKm, double BottomKm,
                  double RightKm, double TopKm);



  int StopAnim(void);

/*�������� ������ � ��������*/
  int LoadData(AnsiString *file_names,
                                int N_of_fn,
                                double TempObzoraVSekundah);

//����� ������� � ������������ ����� ������
  long tnGetFirstOtschetKanalAndRejim(int iKanal, //1 - ������ �����, ����� ������ �����
                                      int iRejim, //����� 1 - ���, 2 - RBS
                                      int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        );

  long tnGetLastOtschetKanalAndRejim(int iKanal, //1 - ������ �����, ����� ������ �����
                                      int iRejim,  //����� 1 - ���, 2 - RBS
                                      int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        );

  long tnGetNextOtschetKanalAndRejim(
                                        long smesh, //�������� �������
                                        int iKanal, //1 - ������ �����, ����� ������ �����
                                        int iRejim,  //����� 1 - ���, 2 - RBS
                                        int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        );

  long tnGetPrevOtschetKanalAndRejim(
                                        long smesh, //�������� �������
                                        int iKanal, //1 - ������ �����, ����� ������ �����
                                        int iRejim,  //����� 1 - ���, 2 - RBS
                                        int iStatusVis //0 - �����, 1 - ������ ������������,
                                                     //2 - � ������� ���������
                                        );


/*�������� ������ - ����� ���������� � ����� traekt_privyaz.cpp*/
     int tpMakePrivyaz(void);

//��������� 02.10.2015
//������ ��������
     int tpKillPrivyaz(void);


};




#endif
