//---------------------------------------------------------------------------


#pragma hdrstop


#include "traekt_blizkie.h"
#include "tools.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


//��������� ��������� �� ������ ������� ����� ����� � ��������
//����������: 1, ��� ��������� - � ����� ������, 0 - �� �������������
//-1 - ������ ������� �� �������
int OBZORY_1::tbIsBlizkieOtmetki(long smesh1,  //����� ������ �������
                          long smesh2,  //����� ������ �������
                          double dDAzmt_grds, //���������� ������� � �������
                          double dDDaln_km     //���������� ������� ��������� � ��
                          )
{
        double dTime;
        double dfDAzmtT;
        double dDalnT;
        dTime=toolsGetDTime(o_b_1[smesh1%362][smesh1/362].Time,
                            o_b_1[smesh2%362][smesh2/362].Time);
        if(dTime>TO/8.0||dTime<-TO/8.0)
        {
                return (-1);
        }
        if(smesh1%362==360||smesh2%362==360)return 0;

        if((o_b_1[smesh1%362][smesh1/362].Status&3)==1)
        {
           if((o_b_1[smesh2%362][smesh2/362].Status&3)==3)
           {
                if(o_b_1[smesh2%362][smesh2/362].ParnOtschet1>=0||
                   o_b_1[smesh2%362][smesh2/362].ParnOtschet2>=0)
                {
                        return 0;
                }
           }else
           if((o_b_1[smesh2%362][smesh2/362].Status&3)==2)
           {
                return 0;
           }
        }else
        if((o_b_1[smesh1%362][smesh1/362].Status&3)==2)
        {
           if((o_b_1[smesh2%362][smesh2/362].Status&3)==1)
           {
                return 0;
           }
           if((o_b_1[smesh2%362][smesh2/362].Status&3)==2)
           {
                if(((o_b_1[smesh1%362][smesh1/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh2%362][smesh2/362].Status&REJIM_RBS_STATUS))||
                   ((o_b_1[smesh2%362][smesh2/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh1%362][smesh1/362].Status&REJIM_RBS_STATUS)))
                {
                      return 0;
                }
           }
           if((o_b_1[smesh2%362][smesh2/362].Status&3)==3)
           {
                if(o_b_1[smesh2%362][smesh2/362].ParnOtschet1>0||
                   o_b_1[smesh2%362][smesh2/362].ParnOtschet2>0)
                {
                      return 0;
                }
                if(((o_b_1[smesh1%362][smesh1/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh2%362][smesh2/362].Status&REJIM_RBS_STATUS))||
                   ((o_b_1[smesh2%362][smesh2/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh1%362][smesh1/362].Status&REJIM_RBS_STATUS)))
                {
                      return 0;
                }
           }
        }else
        if((o_b_1[smesh1%362][smesh1/362].Status&3)==3)
        {
            if(o_b_1[smesh2%362][smesh2/362].ParnOtschet1>0||
                   o_b_1[smesh2%362][smesh2/362].ParnOtschet2>0)
            {
                return 0;
            }
            if((o_b_1[smesh2%362][smesh2/362].Status&3)==2)
            {
                if(((o_b_1[smesh1%362][smesh1/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh2%362][smesh2/362].Status&REJIM_RBS_STATUS))||
                   ((o_b_1[smesh2%362][smesh2/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh1%362][smesh1/362].Status&REJIM_RBS_STATUS)))
                {
                      return 0;
                }
            }
            if((o_b_1[smesh2%362][smesh2/362].Status&3)==3)
            {
                if(o_b_1[smesh2%362][smesh2/362].ParnOtschet1>0||
                   o_b_1[smesh2%362][smesh2/362].ParnOtschet2>0)
                {
                      return 0;
                }
                if(((o_b_1[smesh1%362][smesh1/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh2%362][smesh2/362].Status&REJIM_RBS_STATUS))||
                   ((o_b_1[smesh2%362][smesh2/362].Status&REJIM_UVD_STATUS)&&
                    (o_b_1[smesh1%362][smesh1/362].Status&REJIM_RBS_STATUS)))
                {
                      return 0;
                }
            }
        }
        dfDAzmtT=toolsGetDAzimuth(o_b_1[smesh1%362][smesh1/362].Azmt,
                                  o_b_1[smesh2%362][smesh2/362].Azmt);
        if(ABS(dfDAzmtT)>dDAzmt_grds)return 0;
        dDalnT=o_b_1[smesh1%362][smesh1/362].Dlnst-
               o_b_1[smesh2%362][smesh2/362].Dlnst;
        if(ABS(dDalnT)>dDAzmt_grds)return 0;
        return 1;
}



/*�������� ���� ������� ����������� � ���������������� �������� �� �������� �������*/
int OBZORY_1::tbFindBlizkieOtmetki(
    //������� ������
                             long smesh,               //�������� ������ ������
                             double dDAzmt_grds,       //������� ��������������� ������ � ��������
                             double dDDaln_km,     //������ ������ ��������� � ��
                             int isBackward,       //����� �����
                             int isForward,        //����� ������

//��� �������� ������
                             long *lFoundSmesh,         //������ �������� �������
                             int sz_lFoundSmesh)        //������ ����� �������
{
        int n_of_blzk=0;
        int iRet;
        long tek_smesh;
        if(smesh%362==360)return 0;

        if(isBackward)
        {
            tek_smesh=o_b_1[smesh%362][smesh/362].AbsPrev;
            while(tek_smesh>0)
            {
                 iRet=tbIsBlizkieOtmetki(tek_smesh,smesh,dDAzmt_grds,dDDaln_km);
                 if(iRet==1)
                 {
                        if(n_of_blzk<sz_lFoundSmesh)
                        {
                                if(lFoundSmesh)lFoundSmesh[n_of_blzk]=tek_smesh;
                                n_of_blzk++;
                        }
                 }else
                 if(iRet<0)
                 {
                        break;
                 }
                 tek_smesh=o_b_1[smesh%362][smesh/362].AbsPrev;
            }
        }

        if(isForward)
        {
            tek_smesh=o_b_1[smesh%362][smesh/362].AbsNext;
            while(tek_smesh>0)
            {
                 iRet=tbIsBlizkieOtmetki(tek_smesh,smesh,dDAzmt_grds,dDDaln_km);
                 if(iRet==1)
                 {
                        if(n_of_blzk<sz_lFoundSmesh)
                        {
                                if(lFoundSmesh)lFoundSmesh[n_of_blzk]=tek_smesh;
                                n_of_blzk++;
                        }
                 }else
                 if(iRet<0)
                 {
                        break;
                 }
                 tek_smesh=o_b_1[smesh%362][smesh/362].AbsNext;
            }
        }
        return n_of_blzk;
}

