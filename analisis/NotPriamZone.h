//---------------------------------------------------------------------------
#ifndef NotPriamZoneH
#define NotPriamZoneH
//---------------------------------------------------------------------------
/*������ ��� ��������� ��������������� �������*/

struct OTREZOK
{
  double X1,Y1;  //������ ������
  double X2,Y2;  //������ �������
  OTREZOK(double Xt1, double Yt1,
          double Xt2, double Yt2)
  {

    if(Xt1<=Xt2)
    {
      X1=Xt1;
      X2=Xt2;
      Y1=Yt1;
      Y2=Yt2;
    }else{
      X1=Xt2;
      X2=Xt1;
      Y1=Yt2;
      Y2=Yt1;
    }
  };

  //-1, ���� ����� ����� �������
  //0, ���� ��� �������� ��� ��� ��������
  //1, ������ �������
  int HorPolojenie(double Xt)
  {
    if(Xt<X1)
    {
      return (-1);
    }else if(Xt>=X2){
      return 1;
    }
    return 0;
  };

//-1, ���� ����� ���� �������
//0, ����� ����� ������������� �������������� �� �������
//1, ���� �������
  int VerPolojenie(double Yt)
  {
    if(Y1>Y2)
    {
      if(Yt<Y2)
      {
        return (-1);
      }else if(Yt>Y1)
      {
        return 1;
      }
      return 0;
    }

    if(Yt<Y1)
    {
       return (-1);
    }else if(Yt>Y2){
       return 1;
    }
    return 0;
  };

//�������� Y ���������� � �����
  double GetY(double Xt)
  {
    double K;
    double Ret;
    if(X1==X2)
    {
      if(Xt==X1)
      {
        return 2000000000L;
      }else{
        return (-2000000000L);
      }
    }

    K=(Y2-Y1)/(X2-X1);
    Ret=K*(Xt-X1);
    Ret+=Y1;
    return Ret;
  };



  int VerPolojenie(double Xt,double Yt)
  {
     double Yn;
     if(X1==X2)
     {
       if(Xt==X1)return 0;
       return -1000000;
     }
     Yn=GetY(Xt);
     if(Yn>Yt)return (-1);
     if(Yn<Yt)return 1;
     return 0;
  };

};

struct PRIAM
{
  double Xl,Yt;
  double Xr,Yb;
  PRIAM()
  {
  };
  PRIAM(double X1,double Y1,
        double X2,double Y2)
  {
    if(X1>=X2)
    {
      Xl=X1;
      Xr=X2;
    }else{
      Xl=X2;
      Xr=X1;
    }
    if(Y1>=Y2)
    {
      Yt=Y1;
      Yb=Y2;
    }else{
      Yt=Y2;
      Yb=Y1;
    }
  };

  int HasPixel(double X, double Y)
  {
    if(X>=Xl&&X<=Xr&&
       Y>=Yt&&Y<=Yb)
    {
      return 1;
    }
    return 0;
  };

};



//������� ��������� �� ��������
class OBLAST
{
    double PervX, PervY;
    double PoslX, PoslY;
    bool Dodelan;
    double MinX,MaxX,MinY,MaxY;



    PRIAM Priam;          //�������������
    OTREZOK **Otrezki;    //�������
    int N_of_Otrezki;     //����� ��������

  public:
    OBLAST();
    ~OBLAST();
    int AddPixel(double X, double Y);
    int Dodelat(void);
    int IsVnutri(double X, double Y);

};




//������� ����� ��������������� �������
extern "C" __declspec(dllexport)
 void __stdcall MakeNotPriamOblast(void);



//�������� ����� 
extern "C" __declspec(dllexport)
 void __stdcall AddTochkaVNotPriamOblast(double X, double Y);


//�������� ��������������� �������, ��������
extern "C" __declspec(dllexport)
 void __stdcall DodelatNotPriamOblast(void);


//���������, �������� �� ����� ������ �������
extern "C" __declspec(dllexport)
 long __stdcall IsVnutriNotPriamOblast(double X, double Y);


//�������� ��������������� �������
extern "C" __declspec(dllexport)
 void __stdcall FreeNotPriamOblast(void);




#endif
