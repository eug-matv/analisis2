//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "tools.h"
#include "traekt_nomborts.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)



static int  tnbSortFunctionByNomer(const void *a, const void *b)
{
   ONE_TRASSA_INFO *a1, *b1;
   a1=*((ONE_TRASSA_INFO **)a);
   b1=*((ONE_TRASSA_INFO **)b);
   if(a1->ulNumber<b1->ulNumber)return (-1);
   if(a1->ulNumber>b1->ulNumber)return (1);
   if(a1->first_poryadk_nomer<b1->first_poryadk_nomer)
   {
        return (-1);
   }
   if(a1->first_poryadk_nomer>b1->first_poryadk_nomer)
   {
        return 1;
   }
   return 0;
}

static int  tnbSortFunctionByFirstTime(const void *a, const void *b)
{
   ONE_TRASSA_INFO *a1, *b1;
   a1=*((ONE_TRASSA_INFO **)a);
   b1=*((ONE_TRASSA_INFO **)b);
   if((a1->ulNumber&0xFF000000)>0x12000000||
      (b1->ulNumber&0xFF000000)>0x12000000)return tnbSortFunctionByNomer(a,b);
   if(a1->first_poryadk_nomer<b1->first_poryadk_nomer)return (-1);
   if(a1->first_poryadk_nomer>b1->first_poryadk_nomer)return 1;
   return 0;
}



int OBZORY_1::tnbTestForZeroBort(void)
{
        int i,j;
        long smesh;
        unsigned long nomborts[10],n;
        int n_of_nomborts=0;
        int naid;
        int cnts_nomborts[10];
        int max_n=-1,indx_brt_max=-1;
        for(i=1;i<n_of_otis;i++)
        {
                Application->ProcessMessages();
                if(otis[i].lFirstIndex<0)continue;
                if((otis[i].ulNumber&0xFF000000)==0||
                   (otis[i].ulNumber&0xFF000000)>0x02000000||
                   (otis[i].ulNumber&0xFFFFFF)!=0)continue;
                n_of_nomborts=0;

                if(i==447)
                {
                        int debug=1;
                }
                smesh=tnGetFirstMainOtshet(i);
                while(smesh>=0)
                {
                    n=(unsigned long)(o_b_1[smesh%362][smesh/362].NomBort);
                    if(n!=0&&(o_b_1[smesh%362][smesh/362].Status&KANAL_S_STATUS))
                    {
                        naid=0;
                        for(j=0;j<n_of_nomborts&&j<10;j++)
                        {
                              if(nomborts[j]==n)
                              {
                                naid=1;
                                cnts_nomborts[j]++;
                                break;
                              }
                        }
                        if(!naid)
                        {
                                if(n_of_nomborts<10)
                                {
                                     nomborts[n_of_nomborts]=n;
                                     cnts_nomborts[n_of_nomborts]=1;
                                     n_of_nomborts++;
                                }
                        }

                    }
                    smesh=tnGetNextMainOtshet(smesh);
                }
                max_n=-1;
                indx_brt_max=-1;
                for(j=0;j<n_of_nomborts;j++)
                {
                     if(cnts_nomborts[j]>max_n)
                     {
                        indx_brt_max=j;
                        max_n=cnts_nomborts[j];
                     }
                }
                if(indx_brt_max>=0)
                {
                     otis[i].ulNumber=otis[i].ulNumber|nomborts[indx_brt_max];
                }
        }
        return 1;
}


int OBZORY_1::ZapolnitSpisok(TStrings *Strings, //������ ����� ������� ���� ���������
                     bool SortNomer)   //����������� �� ������� (����� �� ������� ���������)
{
  int i;
  unsigned long NomerOut;
  char RejStr[5];
  double Time;
  long Nomer,Index;
  int Sutki,Chasy,Minuty;
  double Sekundy;
  long ChisloOtschetov;
  int isNumberOut=1;
  char Strka[100];
  if(sort_otis)
  {
     free(sort_otis);
     sort_otis=NULL;
  }
  sort_otis=(ONE_TRASSA_INFO**)malloc((n_of_otis-1)*sizeof(ONE_TRASSA_INFO*));
  n_of_sort_otis=0;
  for(i=1;i<n_of_otis;i++)
  {
        if(otis[i].lFirstIndex<0)continue;
        sort_otis[n_of_sort_otis]=&(otis[i]);
        n_of_sort_otis++;

  }
  if(SortNomer)
  {
        qsort(sort_otis,  n_of_sort_otis, sizeof(ONE_TRASSA_INFO*),
                tnbSortFunctionByNomer);
  }else{
        qsort(sort_otis,  n_of_sort_otis, sizeof(ONE_TRASSA_INFO*),
                tnbSortFunctionByFirstTime);
  }
  Strings->Clear();
  for(i=0;i<n_of_sort_otis;i++)
  {
        NomerOut=sort_otis[i]->ulNumber&0x00FFFFFF;
        if((sort_otis[i]->ulNumber&0xFF000000)==0x0)
         {
                if(IsWasPrivyazka)
                {
                        RejStr[0]='�'; RejStr[1]='�';RejStr[2]='#';RejStr[3]=' ';RejStr[4]=0;
                }else{
                        RejStr[0]=' '; RejStr[1]=' ';RejStr[2]=' ';RejStr[3]=' ';RejStr[4]=0;
                }
        }else
        if((sort_otis[i]->ulNumber&0xFF000000)==0x01000000)
        {
                RejStr[0]='�'; RejStr[1]='�';RejStr[2]='�';RejStr[3]='-';RejStr[4]=0;
        }else
        if((sort_otis[i]->ulNumber&0xFF000000)==0x02000000)
        {
                RejStr[0]='R'; RejStr[1]='B';RejStr[2]='S';RejStr[3]='-';RejStr[4]=0;
        }else
        if((sort_otis[i]->ulNumber&0xFF000000)==0x11000000)
        {
                RejStr[0]='�'; RejStr[1]='�';RejStr[2]='�';RejStr[3]='-';RejStr[4]=0;
        }else
        if((sort_otis[i]->ulNumber&0xFF000000)==0x12000000)
        {
                RejStr[0]='R'; RejStr[1]='�';RejStr[2]='�';RejStr[3]='-';RejStr[4]=0;
        }else{
                isNumberOut=0;
        }

        if(isNumberOut)
        {
                    Time=o_b_1[sort_otis[i]->lFirstIndex%362][sort_otis[i]->lFirstIndex/362].Time;
            GetTimeFromDouble(Time,Sutki,Chasy,Minuty,Sekundy);

             if(!IsWasPrivyazka||sort_otis[i]->ulNumber>=0x01000000)
             {
                sprintf(Strka,"%s%7ld :%02d:%02d:%02d Plots: %d",
                    RejStr, NomerOut, Chasy, Minuty, (long)Sekundy,
                    sort_otis[i]->n_of_otmetok);
             }else{
                sprintf(Strka,"%s%7ld :%02d:%02d:%02d Plots: %d",
                     RejStr,NomerOut%10000, Chasy, Minuty, (long)Sekundy,
                    sort_otis[i]->n_of_otmetok);
             }
        }else
        if(sort_otis[i]->ulNumber==0x16000000)
        {
            sprintf(Strka,"RBS. False marks");
        }else
        if(sort_otis[i]->ulNumber==0x15000000)
        {
            sprintf(Strka,"UVD. False marks");
        }else
        if(sort_otis[i]->ulNumber==0x14000000)
        {
            sprintf(Strka,"PC. False marks");
        }else
        if(sort_otis[i]->ulNumber==0x19000000)
        {
           sprintf(Strka,"RBS. Split marks");
        }else
        if(sort_otis[i]->ulNumber==0x18000000)
        {
           sprintf(Strka,"UVD. Split marks");
        }else
        if(sort_otis[i]->ulNumber==0x17000000)
        {
           sprintf(Strka,"PC. Split marks");
        }
       Strings->Add(Strka);
  }

  return 1;
}








//���������� ������ � ������� ������
//���������� (-1) - ��������� ����������� ���������
//0 ��������� ������
int OBZORY_1::GetDataAboutSelectedNumber(TListBox *ListBox, //���� ���, ���������� ������ ������
                                         bool SortNomer)
{
  int i;
  if(ListBox->Items->Count!=n_of_sort_otis)return -1;
  for(i=0;i<n_of_sort_otis;i++)
  {
        if(ListBox->Selected[i])
        {
            sort_otis[i]->isSelected=1;
        }else{
            sort_otis[i]->isSelected=0;
        }
  }
  return 1;
}


int OBZORY_1::SaveSpisokBortov(AnsiString FileLst)
{
  FILE *fp;
  int i;

//���������� ������ � ������
  fp=fopen(FileLst.c_str(),"w");
  if(fp==NULL)return 0;
  for(i=0;i<n_of_sort_otis;i++)
  {
    if(sort_otis[i]->isSelected)
    {
      fprintf(fp,"%d %lu Air. number: %lu \n",
                                        sort_otis[i]->trassa_indx,
                                        sort_otis[i]->ulNumber,
                                        0x00FFFFFF&(sort_otis[i]->ulNumber));
    }
  }
  fclose(fp);
  return 1;
}


int OBZORY_1::SaveSpisokVydelBortov(AnsiString FileLst)
{
  FILE *fp;
  int i;
  long I_O,I,N;
  long IndxBrt1;  //
  if(n_of_sort_otis<1)return 0;

//���������� ������ � ������
  fp=fopen(FileLst.c_str(),"w");
  if(fp==NULL)return 0;


  bool *IsWasSaved;
  IsWasSaved=new bool [n_of_otis];
  for(i=0;i<n_of_otis;i++)
  {
    IsWasSaved[i]=false;
  }

  I_O=FirstVisual;
  while(I_O>=0)
  {
     I=I_O/362; N=I_O%362;
     if(o_b_1[N][I].Status&V_OBLASTI_VYDELENIA_STATUS&&
        o_b_1[N][I].Status&VISIBLE_STATUS)
     {
        IndxBrt1=o_b_1[N][I].IndxBrt;
        if(!IsWasSaved[IndxBrt1])
        {
          fprintf(fp,"%d %lu Air. number: %lu \n",
               IndxBrt1, otis[IndxBrt1].ulNumber,0x00FFFFFF&otis[IndxBrt1].ulNumber);
          IsWasSaved[IndxBrt1]=true;
        }
     }
     I_O=o_b_1[N][I].NextVisual;
  };

  fclose(fp);
  delete []IsWasSaved;
  return 1;
}


int OBZORY_1::LoadSpisokBortov(AnsiString FileLst)
{
  FILE *fp;
  int i,Ret;
  char RetC;
  int indx_brt;
  unsigned long NomBort;
//���������� ������ � ������
  fp=fopen(FileLst.c_str(),"r");
  if(fp==NULL)return 0;
  for(i=0;i<n_of_otis;i++)otis[i].isSelected=0;
  while(!feof(fp))
  {
    Ret=fscanf(fp,"%d %lu",&indx_brt, &NomBort);
    if(Ret==2)
    {
//���� ����� �����
        if(indx_brt<n_of_otis)
        {
                if(otis[indx_brt].ulNumber==NomBort)
                {
                        otis[indx_brt].isSelected=1;
                }
        }
    }
    do{
      RetC=fgetc(fp);
    }while(!feof(fp)&&RetC!='\n');
  };
  fclose(fp);
  return 1;
}



int OBZORY_1::VydelSVisualSpisokBortov(void)
{
  int i;
  long TekSmesh,Nom,Indx;
  for(i=0;i<n_of_otis;i++)
  {
    otis[i].isSelected=0;
  }
  TekSmesh=GetFirstFilterOB(false);
  while(TekSmesh>=0)
  {
    Nom=TekSmesh%362;
    Indx=TekSmesh/362;
    if(Nom<360)
    {
      otis[o_b_1[Nom][Indx].IndxBrt].isSelected=1;
    }
    TekSmesh=GetNextFilterOB(TekSmesh,false);
  };
  IsCheckNumber=true;
  return 1;
}



//����������� � traekt_nomborts.h
//�������� ������, ������� �������
bool OBZORY_1::GetSelectedInSortedBorts(int indx_sort_otis)
{
        int i;
        if(indx_sort_otis<0||indx_sort_otis>=n_of_sort_otis)
        {
            return false;
        }
        return (bool)(sort_otis[indx_sort_otis]->isSelected);
}


int OBZORY_1::GetNomerSelectedBort(int indx_sort_otis)
{
        if(indx_sort_otis<0||indx_sort_otis>=n_of_sort_otis)return (-1);
        return (int)((sort_otis[indx_sort_otis]->ulNumber)&0x00FFFFFF);
}


//�������� ����� ���������� ������ traekt_nomborts.cpp
int OBZORY_1::GetNumberOfSortedBorts(void)
{
        return n_of_sort_otis;
}



/*��������������� ����� � ��������� ��*/
int OBZORY_1::tnbRenumberPKTrass(void)
{
        int n=1;
        int i;

        for(i=1;i<n_of_otis;i++)
        {
                Application->ProcessMessages();
                if(otis[i].lFirstIndex>=0&&
                   otis[i].ulNumber<=0xFFFFFF)
                {
                   otis[i].ulNumber=90000+n;
                   n=n+1;
                   if(n==10000)n=1;
                }
        }
        return 1;

}

