//---------------------------------------------------------------------------


#pragma hdrstop

#include <stdlib.h>
#include "traekt_napravlenie.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//����������� ����������� ��� ������
int OBZORY_1::tnaprFindNapravleniyaTrassy(int n_trass)
{
        long smesh, smesh_n;
        long parn;
        int last_kuda=0;          //���� ������ ��������� ��� -1 - �� ���,  1 - �� ���

        if(n_trass<=0||n_trass>=n_of_otis)return 0;
        if(otis[n_trass].lFirstIndex<0)return 0;

        if((otis[n_trass].ulNumber&0xFF000000)>0x02000000)
        {
                return 0;
        }


        smesh=tnGetFirstMainOtshet(n_trass);
        otis[n_trass].vetv=
                (VETV_INFO*)realloc(otis[n_trass].vetv,
                        sizeof(VETV_INFO)*
                        (otis[n_trass].n_of_vetv+1));
        otis[n_trass].vetv[otis[n_trass].n_of_vetv].smesh=smesh;
        otis[n_trass].vetv[otis[n_trass].n_of_vetv].smesh=smesh;
        otis[n_trass].n_of_vetv++;

        while(smesh>=0)
        {
           smesh_n=tnGetNextMainOtshet(smesh);
           if(smesh_n>=0)
           {
                if(o_b_1[smesh_n%362][smesh_n/362].Dlnst>
                   o_b_1[smesh%362][smesh/362].Dlnst)
                {
                      o_b_1[smesh%362][smesh/362].Status2|=LETIT_OT_RLS_STATUS2;
                      o_b_1[smesh%362][smesh/362].IndxVtv=
                         (o_b_1[smesh%362][smesh/362].IndxVtv&0xFFF000)|
                                                otis[n_trass].n_of_vetv;

                      if((parn=o_b_1[smesh%362][smesh/362].ParnOtschet1)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_OT_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv=
                             (o_b_1[parn%362][parn/362].IndxVtv&0xFFF000)|
                                otis[n_trass].n_of_vetv;

                      }
                      if((parn=o_b_1[smesh%362][smesh/362].ParnOtschet2)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_OT_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv=
                             (o_b_1[parn%362][parn/362].IndxVtv&0xFFF000)|
                                otis[n_trass].n_of_vetv;
                      }
                      o_b_1[smesh_n%362][smesh_n/362].Status2|=LETIT_OT_RLS_STATUS2;
                      o_b_1[smesh_n%362][smesh_n/362].IndxVtv=
                         (o_b_1[smesh_n%362][smesh_n/362].IndxVtv&0xFFF000)|
                                                otis[n_trass].n_of_vetv;
                      if((parn=o_b_1[smesh_n%362][smesh_n/362].ParnOtschet1)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_OT_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv=
                             (o_b_1[parn%362][parn/362].IndxVtv&0xFFF000)|
                                                otis[n_trass].n_of_vetv;
                      }
                      if((parn=o_b_1[smesh_n%362][smesh_n/362].ParnOtschet2)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_OT_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv=
                             (o_b_1[parn%362][parn/362].IndxVtv&0xFFF000)|
                                                otis[n_trass].n_of_vetv;
                      }
                      if(last_kuda==-1)
                      {

                        otis[n_trass].vetv=
                        (VETV_INFO*)realloc(otis[n_trass].vetv,
                                sizeof(VETV_INFO)*
                                (otis[n_trass].n_of_vetv+1));
                        otis[n_trass].vetv[otis[n_trass].n_of_vetv].smesh=smesh;
                        otis[n_trass].n_of_vetv++;
                      }

                      last_kuda=1;
                }

                if(o_b_1[smesh_n%362][smesh_n/362].Dlnst<
                   o_b_1[smesh%362][smesh/362].Dlnst)
                {
                      o_b_1[smesh%362][smesh/362].Status2|=LETIT_NA_RLS_STATUS2;
                      o_b_1[smesh%362][smesh/362].IndxVtv=
                         (o_b_1[smesh%362][smesh/362].IndxVtv&0x000FFF)|
                                ((otis[n_trass].n_of_vetv)<<12);
                      if((parn=o_b_1[smesh%362][smesh/362].ParnOtschet1)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_NA_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv=
                               (o_b_1[parn%362][parn/362].IndxVtv&0x000FFF)|
                                ((otis[n_trass].n_of_vetv)<<12);
                      }
                      if((parn=o_b_1[smesh%362][smesh/362].ParnOtschet2)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_NA_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv=
                               (o_b_1[parn%362][parn/362].IndxVtv&0x000FFF)|
                                 ((otis[n_trass].n_of_vetv)<<12);
                      }

                      o_b_1[smesh_n%362][smesh_n/362].Status2|=LETIT_NA_RLS_STATUS2;
                      o_b_1[smesh_n%362][smesh_n/362].IndxVtv=
                            (o_b_1[smesh_n%362][smesh_n/362].IndxVtv&0x000FFF)|
                              ((otis[n_trass].n_of_vetv)<<12);

                      if((parn=o_b_1[smesh_n%362][smesh_n/362].ParnOtschet1)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_NA_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv=
                            (o_b_1[parn%362][parn/362].IndxVtv&0x000FFF)|
                               ((otis[n_trass].n_of_vetv)<<12);

                      }
                      if((parn=o_b_1[smesh_n%362][smesh_n/362].ParnOtschet2)>=0)
                      {
                           o_b_1[parn%362][parn/362].Status2|=LETIT_NA_RLS_STATUS2;
                           o_b_1[parn%362][parn/362].IndxVtv+=
                              (o_b_1[parn%362][parn/362].IndxVtv&0x000FFF)|
                                 ((otis[n_trass].n_of_vetv)<<12);
                     }
                      if(last_kuda==1)
                      {
                        otis[n_trass].vetv=
                        (VETV_INFO*)realloc(otis[n_trass].vetv,
                                sizeof(VETV_INFO)*
                                (otis[n_trass].n_of_vetv+1));
                        otis[n_trass].vetv[otis[n_trass].n_of_vetv].smesh=smesh;
                        otis[n_trass].n_of_vetv++;
                      }
                      last_kuda=-1;
                }
           }
           smesh=smesh_n;
        }
        return 1;
}


//����������� ����������� ��� ���� �����
int OBZORY_1::tnaprFindNapravleniya(void)
{
        int i;
        int last_prev_gauge=GlobalGaugeCurTraekt;


        for(i=0;i<n_of_otis;i++)
        {
                tnaprFindNapravleniyaTrassy(i);
                GlobalGaugeCurTraekt=last_prev_gauge+
                (int)(100)*(((double)i)/n_of_otis);
                Application->ProcessMessages();
        }
        return 1;
}
