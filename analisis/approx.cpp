/*�������� ��. � approx.h*/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include <alloc.h>
#include <math.h>
#include "approx.h"
#include "Tools.h"

#define ABS(X) (((X) > 0) ? (X) : (-(X)))




//---------------------------------------------------------------------------
#pragma package(smart_init)



/*���������� ������������ ������� A ��������  Size x Size*/
double Opredelitel_TFC(double **A,
                int Size)
{
//��������� ����������

   int i,Znak=1,j1,j2,k;
   double Summ=0;
   double **B;
   if(Size==1)return A[0][0];
   if(Size==2)
   {
      return (A[0][0]*A[1][1]-A[1][0]*A[0][1]);
   }

   B=(double**)malloc(sizeof(double*)*(Size-1));
   for(i=0;i<Size-1;i++)
   {
     B[i]=(double*)malloc(sizeof(double)*(Size-1));
   }

   for(i=0;i<Size;i++)
   {

     for(j1=0;j1<Size-1;j1++)
     {
       k=0;
       for(j2=0;j2<Size;j2++)
       {
         if(i==j2)
         {
           k=1;
           continue;
         }
         B[j1][j2-k]=A[j1+1][j2];
       }
     }
     Summ+=A[0][i]*Znak*Opredelitel_TFC(B,Size-1);
     Znak*=-1;
   }

   for(i=0;i<Size-1;i++)
   {
      free(B[i]);
   }
   free(B);
   return Summ;
}


int SystemUravn_TFC(double **A, //������ �������������
                    double *d,  //������ ����� ������
                    double *X,  //�������
                    int N_of_X
                    )
{
  double O_A,O_B;
  double **B;
  int i,j,k;

  char DStrka[100];


  O_A=Opredelitel_TFC(A, N_of_X);

  sprintf(DStrka,"O_A=%g",O_A);




  B=(double**)malloc(sizeof(double*)*(N_of_X));
  for(i=0;i<N_of_X;i++)
  {
     B[i]=(double*)malloc(sizeof(double)*(N_of_X));
  }

  for(k=0;k<N_of_X;k++)
  {
    for(i=0;i<N_of_X;i++)
    {
      for(j=0;j<N_of_X;j++)
      {
        if(j==k)
        {
          B[i][j]=d[i];
        }else{
          B[i][j]=A[i][j];
        }
      }
    }
    O_B=Opredelitel_TFC(B,N_of_X);
    X[k]=O_B/O_A;
  }
  for(i=0;i<N_of_X;i++)
  {
    free(B[i]);
  }
  free(B);
  return 1;
}

int Approx1(double *Xs,double *t, int N_of_t,
            double *A0, double *A1)
{

   double **A,d[2],X[2];
   int i;
   int Ret;

   A=(double**)malloc(sizeof(double*)*2);
   A[0]=(double*)malloc(sizeof(double)*2);
   A[1]=(double*)malloc(sizeof(double)*2);
 //�������� ���������
   d[0]=0;
   d[1]=0;
   A[0][0]=0;
   A[0][1]=0;
   A[1][0]=0;
   A[1][1]=0;

   for(i=0;i<N_of_t;i++)
   {
     d[0]+=Xs[i];
     d[1]+=Xs[i]*t[i];
     A[0][0]+=1;
     A[0][1]+=t[i];
     A[1][0]+=t[i];
     A[1][1]+=t[i]*t[i];
   }


 //������ ���� ������ ���������
   Ret=SystemUravn_TFC(A,d,X,2);

   free(A[0]);
   free(A[1]);
   free(A);
   if(Ret==0)return 0;
   *A0=X[0];
   *A1=X[1];
    return 1;
}


//���������� � ������������� �������
double PowApprox(double X, int Stepen)
{
  int i;
  double Ret=1;
  if(X>=-0.0000000001&&X<=0.0000000001)
  {
    return 0.0;
  }

  for(i=1;i<=Stepen;i++)
  {
     Ret*=X;
  }

  X=1.0/X;
  for(i=-1;i>=Stepen;i--)
  {
     Ret*=X;
  }
  return Ret;
}



/*��������� ������������� ����������������� ���������� �� ������ t[i],X[i].
 t - ��� ������ �������� ���������� ������� t[0],t[1],...,t[M],
 X - �������� ������� � ������ t[i]
 M - ��� ����� ���� �����,
 N - ������� ���������������� ��������
 A - ������ ������������� �������� �������� N+1. �[i] - �������� ����. ��� t � ������� i
 ������ ��� ��� ������� ������ ���� �������� ����� ������� ������� GetKooefForApprox_TFC

 ��������� ������ ������� ��������� ������������ ������������� ��� ���� k �� 0 �� N
  C����_��_i_0_N(A[i]*�����_��_j_0_M-1(t[j]�_����(i+k)))=
       �����_��_j_0_M-1 (X[j] * t �_����(k))
 ������ A[i]


 ������� ������������� �������� ���:
   B[k][i]=�����_��_j_0_M-1(t[j]�_����(i+k))
   ��� k=0...N, i=0...N
*/
int   GetKooefForApprox_TFC( double *t,  //�����
                             double *X,  //�������� � ���� ������
                             int M,      //����� �����
                             int N,      //������� ����������������� ����������
                             double *A)  //������������ ������ �������������
                                          //������ N+1 - ������ ������ ���� ��������
{
  double **B,*D,Prom;  //� - ��� ������������ ��� A
  int i,j,p,k;
  B=new double* [N+1];
  D=new double  [N+1];
  for(i=0;i<=N;i++)
  {
    B[i]=new double [N+1];
  }

//������ ������� ������������� � ������� ��������� ������
  for(k=0;k<=N;k++)
  {
    for(i=0;i<=N;i++)
    {
      Prom=0.0;
      for(p=0;p<M;p++)
      {
         Prom+=PowApprox(t[p],k+i);
      }
      B[k][i]=Prom;
    }
    Prom=0;
    for(p=0;p<M;p++)
    {
      Prom+=X[p]*PowApprox(t[p],k);
    }
    D[k]=Prom;
  }

  SystemUravn_TFC(B,D,A,N+1);

  for(i=0;i<=N;i++)
  {
    delete []B[i];
  }
  delete []B;
  delete []D;
  return 1;
}


/*�������� �������� � �������� �����
  A - ������ ������������� �������� N+1, A[i] ������������� ������� i
  N - ������� ��������
  t - �������� � �����
*/
double GetNumberFromPolinom_TFC(double *A,
                                int     N,
                                double  t)
{
  int i;
  double Ret=A[N];
  for(i=N-1;i>=0;i--)
  {
     Ret=Ret*t+A[i];
  }
  return Ret;
}

/*���������� ������� �� ������ t1 � ��������� ������� X1
 ������ ��� X1
*/
int GetMNumbersFromPolinom_TFC(double *A,
                                int N,
                                double *t1,
                                double *X1,
                                int M1)
{
  int i;
  for(i=0;i<M1;i++)
  {
    X1[i]=GetNumberFromPolinom_TFC(A,N,t1[i]);
  }
  return 1;
}


/*
  ������������� �� ������ (t[i], X[i]), ��� i=0...M,
          ������������ ������� N,
  ������������ ���������� ���������� � ������ A, ���� A!=NULL, ����� ��� ���� �� ����������
  ����� �� ������ t1[i] �������� �������� ���������� � ������ X1[i], ��� i=0...M1
  */
int Approximirovat_TFC(
                       double *t,  //�����
                       double *X,  //�������� � ������ t
                       int M,      //����� �����
                       int N,      //������� �������� �� 0 � ������
                       double *A,  //����������� ��������� ����� ���� ����� 0
                       double *t1, //����� t1
                       double *X1, //�������� � ������(��� ����� ���������)
                       int M1      //����� �����
                       )
{
   double *A1;
   int Ret;
   if(A)
   {
     A1=A;
   }else{
     A1=(double*)malloc(sizeof(double)*(N+1));
   }
   Ret=GetKooefForApprox_TFC(t,X,M,N,A1);
   if(!Ret)
   {
     if(!A)
     {
       free(A1);
     }
     return 0;
   }
   GetMNumbersFromPolinom_TFC(A1,N,t1,X1,M1);

   if(!A)
   {
     free(A1);
   }
   return 1;
}



//������������ �������
int NormTime_TFC(double *t,
                 int N,
                 double *t_n)
{
  double Sred_t;
  int i;
  if(N==0||t==NULL||t_n==NULL)return 0;
  Sred_t=(t[0]+t[N-1])/2;
  for(i=0;i<N;i++)
  {
    t_n[i]=t[i]-Sred_t;
  }
  return 1;
}

//��������� �������� �����
double GetSigma_TFC(double *A_izm,
                    double *A_apr,
                    int N)
{
  int i;
  double Rez=0;
  if(N<2)return -1;
  double Sum=0;
  for(i=0;i<N;i++)
  {
    Sum+=(A_izm[i]-A_apr[i])*(A_izm[i]-A_apr[i]);
  }
  Rez=Sum/(N-1);
  Rez=sqrt(Rez);
  return Rez;
}

double GetDispersia_TFC(double *A_izm,
                    double *A_apr,
                    int N)
{
  int i;
  double Rez=0;
  if(N<2)return -1;
  double Sum=0;
  for(i=0;i<N;i++)
  {
    Sum+=(A_izm-A_apr)*(A_izm-A_apr);
  }
  Rez=Sum/(N-1);
  return Rez;
}



//������������
//������ �����
//������������� ������ ��������

int AnalizVysot_TFC(long MaxPerepadVysoty,
                 double *t, //�������
                long *Vys, //������
                int N_of_Vys,
                long *Vys_A)  //������ �������������������
{
  int i;
  double *t1,*t2;
  double A[3];  //�����������
  double *Vs2,*Vs2a;

  int N_of_Vys2,MaxDV1_i;
  double Sigma;
  double MaxDV=MaxPerepadVysoty/1000.0;
  double MaxDV1;
  t1=(double*)malloc(sizeof(double)*N_of_Vys);
  t2=(double*)malloc(sizeof(double)*N_of_Vys);
  Vs2=(double*)malloc(sizeof(double)*N_of_Vys);
  Vs2a=(double*)malloc(sizeof(double)*N_of_Vys);
  NormTime_TFC(t,N_of_Vys,t1);  //��������� �����
  N_of_Vys2=0;
  for(i=0;i<N_of_Vys;i++)
  {
    if(Vys[i])
    {
      Vs2[N_of_Vys2]=Vys[i]/1000.0;
      t2[N_of_Vys2]=t1[i];
      N_of_Vys2++;
    }
  }
  if(N_of_Vys2<5)
  {
    free(Vs2);
    free(Vs2a);
    free(t1);
    free(t2);
    return 0;
  }

  do
  {
  //�������� ������ �������������
    Approximirovat_TFC(t2,Vs2,N_of_Vys2,2,A,t2,Vs2a,N_of_Vys2);

  //������ �������� �����...
    Sigma=GetSigma_TFC(Vs2,Vs2a,N_of_Vys2);
    if(Sigma>MaxDV)
    {
//������ ����� ������ ������������ ������� � ������ ���
       MaxDV1=-1;
       for(i=0;i<N_of_Vys2;i++)
       {
         if(ABS(Vs2[i]-Vs2a[i])>MaxDV1)
         {
           MaxDV1=ABS(Vs2[i]-Vs2a[i]);
           MaxDV1_i=i;
         }
       }
       for(i=MaxDV1_i;i<N_of_Vys2-1;i++)
       {
         Vs2[i]=Vs2[i+1];
         t2[i]=t2[i+1];
       }
       N_of_Vys2--;
    }
    if(N_of_Vys2<5)break;
   }while(Sigma>MaxDV);



   if(N_of_Vys2<5)
   {
     free(Vs2);
     free(Vs2a);
     free(t1);
     free(t2);
     return 0;
   }



//��������� �������� ��������������� �����
  for(i=0;i<N_of_Vys;i++)
  {
     Vys_A[i]=(long)(1000*GetNumberFromPolinom_TFC(A,2,t1[i]));
  }
  free(Vs2);
  free(Vs2a);
  free(t1);
  free(t2);
  return 1;
}



/*������������ �������� �� �������*/
int ApproximatAzimDalnost_TFC(
          double *AzmtIn,  //������� ������ ��������
          double *DlnstIn, //������� ������ ����������
          double *Times,   //����� � �����
          int N_of_Otch,   //����� ��������
          long Stepen,     //������� ���������� �� 0 �� 6
          double *AzmtOut, //�������� ������ ��������, �� ����� 0
          double *DlnstOut,//�������� ������ ����������, �� ����� 0
          double *SKO_Azmt,//��� �������, �� ����� 0
          double *SKO_Dlnst//��� ���������, �� ����� 0
          )
{
  double *X_In, *Y_In; //������� �������  ���������� ���������
  double *X_Out,*Y_Out; //������� �������  ���������� ���������
  double *TimesNorm;
  double *AzmtOut1, *DlnstOut1;
  double dAzmt;
  int i;
  if(N_of_Otch<=0)return 0;
  if(N_of_Otch==1)return (-1);

//������� ��� ��� ������
  X_In=(double*)malloc(sizeof(double)*N_of_Otch);
  Y_In=(double*)malloc(sizeof(double)*N_of_Otch);
  X_Out=(double*)malloc(sizeof(double)*N_of_Otch);
  Y_Out=(double*)malloc(sizeof(double)*N_of_Otch);
  TimesNorm=(double*)malloc(sizeof(double)*N_of_Otch);
  NormTime_TFC(Times,N_of_Otch, TimesNorm);
  if(AzmtOut)
  {
    AzmtOut1=AzmtOut;
  }else{
    AzmtOut1=(double*)malloc(sizeof(double)*N_of_Otch);
  }
  if(DlnstOut)
  {
    DlnstOut1=DlnstOut;
  }else{
    DlnstOut1=(double*)malloc(sizeof(double)*N_of_Otch);
  }


//������� �������� X_In � Y_In
  for(i=0;i<N_of_Otch;i++)
  {
    X_In[i]=DlnstIn[i]*sin(AzmtIn[i]/180.0*M_PI);
    Y_In[i]=DlnstIn[i]*cos(AzmtIn[i]/180.0*M_PI);
  }
//� ��� ���� ��� ����������������
  Approximirovat_TFC(TimesNorm,X_In,N_of_Otch,Stepen,NULL,TimesNorm,X_Out,N_of_Otch);
  Approximirovat_TFC(TimesNorm,Y_In,N_of_Otch,Stepen,NULL,TimesNorm,Y_Out,N_of_Otch);

//�������� �������� AzmtOut � DlnstOut
  for(i=0;i<N_of_Otch;i++)
  {
      if(X_Out[i]<0.00001&&Y_Out[i]<0.00001&&
         X_Out[i]>-0.00001&&Y_Out[i]>-0.00001)
      {
        AzmtOut1[i]=0;
      }else{
        AzmtOut1[i]=atan2(X_Out[i],Y_Out[i])/M_PI*180;
      }
      if(AzmtOut[i]<0)AzmtOut[i]+=360.0;
      DlnstOut1[i]=sqrt(X_Out[i]*X_Out[i]+Y_Out[i]*Y_Out[i]);
  }

//��������� �������� ��� �� �������
  if(SKO_Azmt)
  {
    *SKO_Azmt=0;
    for(i=0;i<N_of_Otch;i++)
    {
      dAzmt=AzmtIn[i]-AzmtOut1[i];
      if(dAzmt>180)dAzmt=360-dAzmt;
      if(dAzmt<-180)dAzmt=360+dAzmt;
      *SKO_Azmt+=dAzmt*dAzmt;
    }
    *SKO_Azmt=sqrt((*SKO_Azmt)/(N_of_Otch-1));
  }

  if(SKO_Dlnst)
  {
    *SKO_Dlnst=0;
    for(i=0;i<N_of_Otch;i++)
    {
      *SKO_Dlnst+=(DlnstOut1[i]-DlnstIn[i])*(DlnstOut1[i]-DlnstIn[i]);
    }
    *SKO_Dlnst=sqrt((*SKO_Dlnst)/(N_of_Otch-1));
  }


  if(AzmtOut==NULL)
  {
    free(AzmtOut1);
  }
  if(DlnstOut==NULL)
  {
    free(DlnstOut1);
  }
  free(X_In);
  free(Y_In);
  free(X_Out);
  free(Y_Out);
  free(TimesNorm);
  return 1;
}

/*������������ �������� �� �������*/
int GetStatesByData_TFC(
          double *AzmtIn,  //������� ������ ��������
          double *DlnstIn, //������� ������ ����������
          double *Times,   //����� � �����
          int N_of_Otch,   //����� ��������
          int stepen,       //������� ����������
      struct TAPPStateKoef *state_koef)
{
         int i;
         double *X_In, *Y_In; //������� �������  ���������� ���������
         double *TimesNorm;
         if(!AzmtIn || !DlnstIn || !Times || N_of_Otch<=stepen||
                stepen<0||stepen>6||!state_koef)return 0;
//������� ��� ��� ������

          X_In=(double*)malloc(sizeof(double)*N_of_Otch);
          Y_In=(double*)malloc(sizeof(double)*N_of_Otch);
          TimesNorm=(double*)malloc(sizeof(double)*N_of_Otch);
          NormTime_TFC(Times,N_of_Otch, TimesNorm);
 //������� �������� X_In � Y_In
          for(i=0;i<N_of_Otch;i++)
          {
            X_In[i]=DlnstIn[i]*sin(AzmtIn[i]/180.0*M_PI);
            Y_In[i]=DlnstIn[i]*cos(AzmtIn[i]/180.0*M_PI);
          }
          GetKooefForApprox_TFC(TimesNorm,X_In,N_of_Otch, stepen,
                state_koef->koef_x);
          GetKooefForApprox_TFC(TimesNorm,Y_In,N_of_Otch, stepen,
                state_koef->koef_y);

           state_koef->stepen=stepen;
           state_koef->sred_t=(Times[0]+Times[N_of_Otch-1])/2.0;
           if(Times[0]<Times[N_of_Otch-1])
           {
                state_koef->min_t=Times[0];
                state_koef->max_t=Times[N_of_Otch-1];
           }else{
                state_koef->max_t=Times[0];
                state_koef->min_t=Times[N_of_Otch-1];
           }


           free(X_In);
           free(Y_In);
           free(TimesNorm);
           return 1;
}


int RaschetAzimuthAndDalnost_TFC(
        struct TAPPStateKoef *state_koef,
                double t,
        double *dfAzmt, double *dfDlnst,
        double *dfX, double *dfY)
{
     double x,y;
     x=GetNumberFromPolinom_TFC(state_koef->koef_x,
                        state_koef->stepen,
                         t-state_koef->sred_t);

     y=GetNumberFromPolinom_TFC(state_koef->koef_y,
                        state_koef->stepen,
                         t-state_koef->sred_t);


      if(x<0.00001&&y<0.00001&&
         x>-0.00001&&y>-0.00001)
      {
        *dfAzmt=0;
      }else{
        *dfAzmt=atan2(x,y)/M_PI*180;
      }
      if((*dfAzmt)<0)(*dfAzmt)+=360.0;
      *dfDlnst=sqrt(x*x+y*y);
      if(dfX)(*dfX)=x;
      if(dfY)(*dfY)=y;

      return 1;

}

