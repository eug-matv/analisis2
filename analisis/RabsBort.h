//---------------------------------------------------------------------------
#ifndef RabsBortH
#define RabsBortH
#include <stdio.h>
#include <vcl.h>
#define ZIFRA(x)  ((x)>=0x30&&(x)<=0x39 ? 1 : 0)

//---------------------------------------------------------------------------
extern bool StopBinInTxt;

struct DANN   //������ ������ ������
{
 int T_I;
 char P;
 int Nom1;
 char T_K[2];
 char P3[4];
 char OtnVys;  //���� 'A', �� ����������� ������ ����, ����� ������������ ���.
 int Nom2;
 char T_Bort[2];
 int Nom;
 char NomBort[8];
 int Vysota;
 int X;
 int Y;
 double Asimut;
 double Dalnost;
 int H;
 int M;
 double S;
 int NomerSutok;
 long Smesh;
 int NomerObsora;

//��������� 17.04.06
 long ObnarujNomBort;
 long ObnarujVysota;

 //������� ������� ��� ��
 double dfSignalPK;
 

};


struct DANN_STRUCT
{
 int LastNomerSutok;
 long DopuskVozvrat;    //���������� ������ � ������������
 DANN Dan;              //������


};




//������ ��� ������� ����
int LoadDan(FILE *fp,DANN *Dan);
//int LoadDanSutki(FILE *fp,DANN *Dan,int NomerSutok1);


//������ ��� ��������� ����
int LoadDanCS(FILE *fp, DANN *Dan);

//
int SaveDannPlot(DANN Dan,FILE *fpOut);
//AnsiString DanToStr(DANN Dan);



/*��� ���������� ����� �� �������*/
 struct OFFSET_TIME
 {
  long offset;

  int H;
  int M;
  double S;
 };

 int SortTime(FILE *fpSch, //������ ��������
             FILE *fpRes); //���� ��������


 //��������� �������
 int _cdecl SravnOff_Time(const void *el1, const void *el2);



//**/
/*�������������� ��������� ����� � ���������*/
int BinInTxt(AnsiString BinFile, AnsiString TxtFile);


/* struct */


int SravnenieAsimut(DANN Dan1, DANN Dan2);

//���������� ������
int GetDannFromStroka(char *Stroka,
                      DANN *Dan);






double FindTempObzoraPoSeveram(FILE *fp);


//���������� 0� ���� �� ����� ������� ����...
//0, ���� �� ������� ���������� ������, ��� 1, ���� �� ��������� 
int GetFormatFile(char *FileName);



#endif

