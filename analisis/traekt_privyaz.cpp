//---------------------------------------------------------------------------


#pragma hdrstop

#include <stdlib.h>
#include "traekt_privyaz.h"
#include "approx.h"
#include "tools.h"


//������ � ������� �������� ������� ����� ������������� ��������
#define TP_PERIOD_APPROX (1.0/60.0)
#define ABS(X) (((X) > 0) ? (X) : (-(X)))
//---------------------------------------------------------------------------



#pragma package(smart_init)

/*�������� ��������� ��������*/

extern int tzDebugCount1;
extern int tzDebugCount2;




int OBZORY_1::tpMakePrivyaz(void)
{
   extern TCGauge *GlobalCGauge1;
   if(IsWasPrivyazka)return 0; //��� ���� ��������
   tpResetAllTrassa();
   GlobalCGauge1->Progress=1;

   tpObhodPryamoy();
   GlobalCGauge1->Progress=2;

   for(int i=1;i<n_of_otis;i++)
   {
        if(otis[i].ulNumber==(0x02000000|99999))
        {
                int debug=1;
        }
   }

   tpObhodPryamoyPonly();
   GlobalCGauge1->Progress=3;
   for(int i=1;i<n_of_otis;i++)
   {
        if(otis[i].ulNumber==(0x02000000|99999))
        {
                int debug=1;
        }
   }


   tpObhodPovtorny();
   GlobalCGauge1->Progress=4;
   for(int i=1;i<n_of_otis;i++)
   {
        if(otis[i].ulNumber==(0x02000000|99999))
        {
                int debug=1;
        }
   }



   tnbTestForZeroBort();
   GlobalCGauge1->Progress=5;
   for(int i=1;i<n_of_otis;i++)
   {
        if(otis[i].ulNumber==(0x02000000|99999))
        {
                int debug=1;
        }
   }



   tpFindKO_Borts();
   GlobalCGauge1->Progress=6;




   AnalizVysot();
   GlobalCGauge1->Progress=7;

   tnaprFindNapravleniya();
   GlobalCGauge1->Progress=8;

   tnbRenumberPKTrass();
   GlobalCGauge1->Progress=9;
   for(int i=0;i<n_of_otis;i++)
   {
        if(otis[i].ulNumber==(0x02000000|99999))
        {
                int debug=1;
        }
   }

   IsWasPrivyazka=true;

   int debug_smesh;
   debug_smesh=FirstAbs;
   while(debug_smesh>=0)
   {
        if((o_b_1[debug_smesh%362][debug_smesh/362].Status&KANAL_P_STATUS)&&
           (o_b_1[debug_smesh%362][debug_smesh/362].Status&KANAL_S_STATUS))
        {
                if(o_b_1[debug_smesh%362][debug_smesh/362].ParnOtschet1<0||
                   o_b_1[debug_smesh%362][debug_smesh/362].ParnOtschet2<0)
                {
                        int debug=1;
                }
        }
        debug_smesh=o_b_1[debug_smesh%362][debug_smesh/362].AbsNext;
   }


   return 1;
}

int OBZORY_1::tpKillPrivyaz(void)
{
   int i;
   int tmp_k=0;
   int iRet;
   unsigned long NewNumber;  //����� �����
   long smesh;
   extern TCGauge *GlobalCGauge1;

   if(!IsWasPrivyazka)return 0; //��� ���� ��������

   tpResetAllTrassa();
   GlobalCGauge1->Progress=1;

   smesh=FirstAbs;

//������� ��� ������ � ������������ �� �� ������� ������
   while(smesh>=0)
   {    if(tmp_k==0)
        {
                Application->ProcessMessages(); //����� �� �����������
        }
        tmp_k=(tmp_k+1)%50;
        if((smesh%362)<360)
        {
           for(i=1;i<n_of_otis;i++)
           {
                NewNumber=(otis[i].ulNumber)&0x00FFFFFF;
                if(NewNumber==o_b_1[smesh%362][smesh/362].NomBort)
                {
                        tpAddToOtis(smesh%362,smesh/362,i);
                        break;
                }
           }
           if(i==n_of_otis)
           {
                NewNumber=(unsigned long)(o_b_1[smesh%362][smesh/362].NomBort);
                if(o_b_1[smesh%362][smesh/362].Status&REJIM_UVD_STATUS)
                {
                        NewNumber|=0x01000000;
                }else
                if(o_b_1[smesh%362][smesh/362].Status&REJIM_RBS_STATUS)
                {
                        NewNumber|=0x02000000;
                }
                iRet=tpNewTrassa(NewNumber);
                if(iRet>0)
                {
                        tpAddToOtis(smesh%362,smesh/362,iRet);
                }
           }

        }
        o_b_1[smesh%362][smesh/362].ParnForDubl=-1;
        smesh=o_b_1[smesh%362][smesh/362].AbsNext;
   }
   GlobalCGauge1->Progress=2;
   IsWasPrivyazka=false;
   return 1;
}



int OBZORY_1::testForOneTrass(ODIN_BORT_1 **obs, int n_of_obs)
{
    double dD;
    double dfV;
    double dT;
    double azmts[100], dlnsts[100];
    int iRet;
    int i;
    if(n_of_obs<2)return 0;
    if(n_of_obs==2)
    {
        dT=ABS(obs[0]->Time-obs[1]->Time);
        if(dT<TO*(3.0/8.0)||
           dT>(1.0/60.0))

        {
                return 0;
        }
        dD=RastMejduObjektami(obs[0]->Azmt,obs[0]->Dlnst,
                              obs[1]->Azmt,obs[1]->Dlnst);

        dfV= ABS(dD/dT);
        if(dfV>300&&dfV<1500)
        {
           return 1;
        }
        return 0;
    }

    for(i=0;i<n_of_obs-1;i++)
    {
        iRet=testForOneTrass(obs+i,2);
        if(!iRet)
        {
            return 0;
        }
    }
    return 1;
}



//������� ������
int OBZORY_1::tpNewTrassa(
                unsigned long ulNomer
               )
{
        int *tmp_indx_brt=NULL;  //��������� ������ �������� ������
        int i;
        if(n_of_otis==max_n_of_otis)
        {
                if(n_of_work_otis>0)
                {
                     tmp_indx_brt=(int*)malloc(sizeof(int)*n_of_work_otis);
                     for(i=0;i<n_of_work_otis;i++)
                     {
                        tmp_indx_brt[i]=work_otis[i]->trassa_indx;
                     }
                }
                max_n_of_otis+=100;
                otis=(struct ONE_TRASSA_INFO*)realloc(otis,
                       sizeof(struct ONE_TRASSA_INFO)*max_n_of_otis);
                if(!otis)
                {
                        if(n_of_work_otis>0)
                        {
                                free(tmp_indx_brt);
                        }
                        return (-1);
                }
                if(n_of_work_otis>0)
                {
                     for(i=0;i<n_of_work_otis;i++)
                     {
                        work_otis[i]=otis+tmp_indx_brt[i];
                     }
                     free(tmp_indx_brt);
                }

        }
        otis[n_of_otis].first_poryadk_nomer=
        otis[n_of_otis].last_poryadk_nomer=-1;
        otis[n_of_otis].ulNumber=ulNomer;
        otis[n_of_otis].lFirstIndex=
                otis[n_of_otis].lLastIndex=
                otis[n_of_otis].lCurIndex=-1;
        otis[n_of_otis].lFirstApproxIndex=
                otis[n_of_otis].lLastApproxIndex=-1;
        otis[n_of_otis].state=0;
        otis[n_of_otis].n_of_otmetok=0;
        otis[n_of_otis].n_of_otmetok_p=0;
        otis[n_of_otis].n_of_otmetok_s=0;
        otis[n_of_otis].n_of_otmetok_ps=0;
        otis[n_of_otis].trassa_indx=n_of_otis;
        otis[n_of_otis].n_of_vetv=0;
        otis[n_of_otis].vetv=NULL;

        n_of_otis++;
        return (n_of_otis-1);

}





//�������� ������ �� otis'�
int OBZORY_1::tpRemoveFromOtis(long nomer, long indx)
{
       int n_trass;
      long tmpCurIndex, tmp, tmp1;

      if(nomer>=360||nomer<0||indx<0)return 0;
      if(o_b_1[nomer][indx].IndxBrt<0)return 1;
      n_trass=o_b_1[nomer][indx].IndxBrt;
      if(o_b_1[nomer][indx].Prev<0)
      {
          if(o_b_1[nomer][indx].Next<0)
          {
                otis[n_trass].lFirstIndex=otis[n_trass].lLastIndex=
                otis[n_trass].lCurIndex=-1;
                otis[n_trass].n_of_otmetok=otis[n_trass].n_of_otmetok_p=
                otis[n_trass].n_of_otmetok_s=otis[n_trass].n_of_otmetok_ps=0;
                otis[n_trass].first_poryadk_nomer=
                        otis[n_trass].last_poryadk_nomer=-1;
          }else{
                otis[n_trass].lFirstIndex=o_b_1[nomer][indx].Next;
                tmpCurIndex=nomer+indx*362;
                if(tmpCurIndex==otis[n_trass].lCurIndex)
                {
                        otis[n_trass].lCurIndex=o_b_1[nomer][indx].Next;
                }
                tmp=otis[n_trass].lFirstIndex=o_b_1[nomer][indx].Next;
                o_b_1[tmp%362][tmp/362].Prev=-1;
                o_b_1[nomer][indx].Next=-1;
                otis[n_trass].first_poryadk_nomer=
                        o_b_1[tmp%362][tmp/362].poryadk_nomer;

          }
      }else{
//o_b_1[nomer][indx].Prev>=0
          if(o_b_1[nomer][indx].Next<0)
          {
               otis[n_trass].lLastIndex=o_b_1[nomer][indx].Prev;
               tmpCurIndex=nomer+indx*362;
               if(tmpCurIndex==otis[n_trass].lCurIndex)
               {
                   otis[n_trass].lCurIndex=o_b_1[nomer][indx].Prev;
               }
               tmp=o_b_1[nomer][indx].Prev;
               o_b_1[tmp%362][tmp/362].Next=-1;
               o_b_1[nomer][indx].Prev=-1;
               otis[n_trass].last_poryadk_nomer=
                        o_b_1[tmp%362][tmp/362].poryadk_nomer;
          }else{

              tmpCurIndex=nomer+indx*362;
              if(tmpCurIndex==otis[n_trass].lCurIndex)
              {
                  otis[n_trass].lCurIndex=o_b_1[nomer][indx].Next;
              }
              tmp=o_b_1[nomer][indx].Prev;
              tmp1=o_b_1[nomer][indx].Next;
              o_b_1[tmp%362][tmp/362].Next=tmp1;
              o_b_1[tmp1%362][tmp1/362].Prev=tmp;
              o_b_1[nomer][indx].Next=o_b_1[nomer][indx].Prev=-1;
          }
      }
      if(o_b_1[nomer][indx].Status&KANAL_P_STATUS)
      {
         if(o_b_1[nomer][indx].Status&KANAL_S_STATUS)
         {
            otis[n_trass].n_of_otmetok_ps--;
         }else{
            otis[n_trass].n_of_otmetok_p--;
         }
      }else{
        if(o_b_1[nomer][indx].Status&KANAL_S_STATUS)
        {
            otis[n_trass].n_of_otmetok_s--;
        }
     }
     otis[n_trass].n_of_otmetok--;
     o_b_1[nomer][indx].Status2&=(~PRIVYAZAN_OTSCHET_STATUS2);
     o_b_1[nomer][indx].IndxBrt=-1;
     return 1;
}



//���������� ������� � otis ��� �������� ������ otis
int OBZORY_1::tpAddToOtis(long nomer, long indx,
                  int n_trass)
{

        long cur_indx;
        long indx1, nomer1, indx2,nomer2;
        int isNaid=0;
        
        if(nomer>=360)return 0;


        if(n_trass<0||n_trass>=n_of_otis)return 0;

        //���� ������� ������ ������� �� ���������� ������
        tpRemoveFromOtis(nomer,indx);


        cur_indx=otis[n_trass].lCurIndex;

        if(cur_indx<0)
        {
                cur_indx=otis[n_trass].lFirstIndex;
                if(cur_indx<0)
                {
                        otis[n_trass].lFirstIndex=otis[n_trass].lLastIndex=
                                otis[n_trass].lCurIndex=indx*362+nomer;
                        o_b_1[nomer][indx].Prev=-1;
                        o_b_1[nomer][indx].Next=-1;
                        o_b_1[nomer][indx].IndxBrt=n_trass;
                        otis[n_trass].first_poryadk_nomer=
                          otis[n_trass].last_poryadk_nomer=
                          o_b_1[nomer][indx].poryadk_nomer;

                }
        }else{

//���� ���������� ���� �������� �������
           indx1=cur_indx/362;
           nomer1=cur_indx%362;

           if(o_b_1[nomer1][indx1].poryadk_nomer<o_b_1[nomer][indx].poryadk_nomer)
           {
//���� ����������� � ������ ������ ��������� ����� �������� - ���� ������ ����� ��������
               if(o_b_1[nomer1][indx1].Next<0)
               {
//���� ������� ������ ��������� � ������ - ���� �������� ����� ����� ����
                   o_b_1[nomer1][indx1].Next=indx*362+nomer;
                   o_b_1[nomer][indx].Prev=cur_indx;
                   o_b_1[nomer][indx].Next=-1;
                   otis[n_trass].lLastIndex=indx*362+nomer;
                   otis[n_trass].last_poryadk_nomer=
                   o_b_1[nomer][indx].poryadk_nomer;
               }else{
//���� ������ ������ �� ���������, �� ���� ������
                   cur_indx=o_b_1[nomer1][indx1].Next;
                   while(cur_indx>=0)
                   {
                       indx2=cur_indx/362;
                       nomer2=cur_indx%362;
                       if(o_b_1[nomer][indx].poryadk_nomer==
                          o_b_1[nomer2][indx2].poryadk_nomer)
                       {
                           return 2; //������� ��� ��������
                       }
                       if(o_b_1[nomer][indx].poryadk_nomer<
                       o_b_1[nomer2][indx2].poryadk_nomer)
                       {
//������� �������
                           o_b_1[nomer][indx].Next=nomer2+indx2*362;
                           o_b_1[nomer][indx].Prev=nomer1+indx1*362;
                           o_b_1[nomer1][indx1].Next=nomer+indx*362;
                           o_b_1[nomer2][indx2].Prev=nomer+indx*362;
                           isNaid=1;
                           break;
                       }
                       nomer1=nomer2;
                       indx1=indx2;
                       cur_indx=o_b_1[nomer1][indx1].Next;
                  }  //while
                  if(!isNaid)
                  {
//���� �� ������ ������ ������� ����� ���������, �� ���
                    o_b_1[nomer1][indx1].Next=indx*362+nomer;
                    o_b_1[nomer][indx].Prev=nomer1+indx1*362;
                    o_b_1[nomer][indx].Next=-1;
                    otis[n_trass].lLastIndex=indx*362+nomer;
                    otis[n_trass].last_poryadk_nomer=
                        o_b_1[nomer][indx].poryadk_nomer;
                  }
               }

           }else{
//(o_b_1[nomer1][indx1].poryadk_nomer>=o_b_1[nomer][indx].poryadk_nomer

              if(o_b_1[nomer1][indx1].poryadk_nomer==o_b_1[nomer][indx].poryadk_nomer)
              {
                 return 2;
              }

//� ��� ��������, ��� o_b_1[nomer1][indx1].poryadk_nomer>o_b_1[nomer][indx].poryadk_nomer
              indx2=indx1;
              nomer2=nomer1;
              cur_indx=o_b_1[nomer1][indx1].Prev;
              while(cur_indx>=0)
              {
                 nomer1=cur_indx%362;
                 indx1=cur_indx/362;
                 if(o_b_1[nomer1][indx1].poryadk_nomer==
                    o_b_1[nomer][indx].poryadk_nomer)
                 {
                    return 2;
                 }
                 if(o_b_1[nomer1][indx1].poryadk_nomer<o_b_1[nomer][indx].poryadk_nomer)
                 {
                    o_b_1[nomer][indx].Next=nomer2+indx2*362;
                    o_b_1[nomer][indx].Prev=nomer1+indx1*362;
                    o_b_1[nomer1][indx1].Next=nomer+indx*362;
                    o_b_1[nomer2][indx2].Prev=nomer+indx*362;
                    isNaid=1;
                    break;
                }
                nomer2=nomer1;
                indx2=indx1;
                cur_indx=o_b_1[nomer2][indx2].Prev;
              }
              if(!isNaid)
              {
                if(nomer2<0||nomer2>360||indx2<0||indx2>=N_of_o_b_1[nomer2])
                {
                        int Debug=1;
                }
                o_b_1[nomer2][indx2].Prev=indx*362+nomer;
                o_b_1[nomer][indx].Next=nomer2+indx2*362;
                o_b_1[nomer][indx].Prev=-1;
                otis[n_trass].lFirstIndex=indx*362+nomer;
                otis[n_trass].first_poryadk_nomer=o_b_1[nomer][indx].poryadk_nomer;
              }
           }
        }

        o_b_1[nomer][indx].IndxBrt=n_trass;
        otis[n_trass].lCurIndex=indx*362+nomer;
        if(n_trass>0)
        {
              o_b_1[nomer][indx].Status2|=PRIVYAZAN_OTSCHET_STATUS2;
        }else{
              o_b_1[nomer][indx].Status2&=(~PRIVYAZAN_OTSCHET_STATUS2);
        }

//���������� ���������� �������
        if(o_b_1[nomer][indx].Status&KANAL_P_STATUS)
        {
            if(o_b_1[nomer][indx].Status&KANAL_S_STATUS)
            {
               otis[n_trass].n_of_otmetok_ps++;
            }else{
               otis[n_trass].n_of_otmetok_p++;
            }
        }else{
           if(o_b_1[nomer][indx].Status&KANAL_S_STATUS)
           {
              otis[n_trass].n_of_otmetok_s++;
           }
        }
        otis[n_trass].n_of_otmetok++;

        o_b_1[nomer][indx].Status&=~LOJNY_STATUS;
        o_b_1[nomer][indx].Status&=~DROBLENIE_STATUS;
        o_b_1[nomer][indx].Status&=~LOJNY_NOMER_STATUS;
        o_b_1[nomer][indx].Status2&=~NE_PODVIJEN_STATUS2;

        if(otis[n_trass].ulNumber==0x14000000||
           otis[n_trass].ulNumber==0x15000000||
           otis[n_trass].ulNumber==0x16000000)
        {
            o_b_1[nomer][indx].Status|=LOJNY_STATUS;
        }else
        if(otis[n_trass].ulNumber==0x17000000||
           otis[n_trass].ulNumber==0x18000000||
           otis[n_trass].ulNumber==0x19000000)
        {
            o_b_1[nomer][indx].Status|=DROBLENIE_STATUS;
        }else{
           if((otis[n_trass].ulNumber&0xFFFFFF)!=
                (unsigned long)o_b_1[nomer][indx].NomBort)
           {
                o_b_1[nomer][indx].Status|=LOJNY_NOMER_STATUS;
                if((otis[n_trass].ulNumber&0xFF000000)==0x11000000||
                   (otis[n_trass].ulNumber&0xFF000000)==0x12000000)
                {
                    o_b_1[nomer][indx].Status2&=~NE_PODVIJEN_STATUS2;
                }
           }

        }



        return 1;
}

//���������� ������� � otis ������ � �������
int OBZORY_1::tpAddToOtisWithParnye(long nomer, long indx,
                  int n_trass)
{
        long indx1,nomer1,indx2,nomer2;
        if(o_b_1[nomer][indx].ParnOtschet1>=0&&
           o_b_1[nomer][indx].ParnOtschet2>=0)
        {
//��������� ������ �������, ���� ��� �� ����������
             nomer1=o_b_1[nomer][indx].ParnOtschet1%362;
             indx1=o_b_1[nomer][indx].ParnOtschet1/362;
             nomer2=o_b_1[nomer][indx].ParnOtschet2%362;
             indx2=o_b_1[nomer][indx].ParnOtschet2/362;
             tpAddToOtis(nomer1,indx1,n_trass);
             tpAddToOtis(nomer2,indx2,n_trass);
        }
        return tpAddToOtis(nomer,indx,n_trass);
}


//���������� ������� � ������ ������, ���� isMakingByAnalisisFiles=1
int OBZORY_1::tpAddToOtisIfIsMakingByAnalisisFiles(long nomer, long indx)
{
        int i;
        unsigned long NewNumber;
        int i_naid=-1;
        if(!isMakingByAnalisisFiles)return 0;
        NewNumber=(unsigned long)o_b_1[nomer][indx].NomBort;
        if(o_b_1[nomer][indx].Status2&REJIM_UVD_STATUS)
        {
                NewNumber|=0x01000000;
        }else
        if(o_b_1[nomer][indx].Status2&REJIM_RBS_STATUS)
        {
                NewNumber|=0x02000000;
        }
        for(i=0;i<n_of_otis;i++)
        {
            if(otis[i].ulNumber==NewNumber)
            {
                tpAddToOtisWithParnye(nomer, indx, i);
                return 1;
            }
        }
        i=tpNewTrassa(NewNumber);
        if(i<0)return i;
        tpAddToOtisWithParnye(nomer, indx, i);
        return 1;
}







//�������� � �������� ������, ���� �����
int OBZORY_1::tpTestForKillTrassa(int n_trass)
{
   long smesh;
   if(n_trass<0||n_trass>=n_of_otis)return 0;
   if(n_trass==75)
   {
        int Debug=1;
   }

   if(otis[n_trass].n_of_otmetok<10)
   {
//��� ������ ��������� � ������������ �������
        while(otis[n_trass].lFirstIndex>=0)
        {
              tpAddToOtis(otis[n_trass].lFirstIndex%362,
                       otis[n_trass].lFirstIndex/362,0);
        }

   }
   return 1;
}


long OBZORY_1::tpAddOtmetkaToTrassa(int n_trass,   //����� ������
                        long nomer, long indx,     //����� ����������� �������
                        int isRemovingOther)       //
{
        long lRetSmesh=-1;
        if(n_trass<1||n_trass>=n_of_otis)return -1;
        if(otis[n_trass].lLastAddingSmesh>=0&&!isRemovingOther)
        {
                return (-2);
        }
        if(otis[n_trass].lLastAddingSmesh>=0)
        {
                lRetSmesh=otis[n_trass].lLastAddingSmesh;
                tpAddToOtisWithParnye(otis[n_trass].lLastAddingSmesh%362,
                                      otis[n_trass].lLastAddingSmesh/362,
                                      0);
                otis[n_trass].lLastAddingSmesh=-1;
        }
        tpAddToOtisWithParnye(nomer, indx, n_trass);
        otis[n_trass].lLastAddingSmesh=nomer+indx*362;
        return lRetSmesh;
}




//������ ����������� �� ��������, ������ ������� �������� �� �������� ���������� �����
int OBZORY_1::tpObhodPryamoy()
{
        int tmp_k=0;                    //��� ���������� �����, ����� �� ������ 10 ������� ������ ����������� �����������
        long smesh,smesh_n;
        double koef,min_koef;
        int debug_k=0;
        int i=0,index_min;
        int tmp_indx;
        int last_prev_gauge=GlobalGaugeCurTraekt;


        n_of_work_otis=0;
        max_n_of_work_otis=100;
        work_otis=
                (ONE_TRASSA_INFO**)realloc(work_otis,
                        sizeof(ONE_TRASSA_INFO*)*max_n_of_work_otis);


        debug_k=0;
//������� ������ ������������� ������
        smesh=tnGetFirstNotPrivyazan(0);
        while(smesh>=0)
        {
                if(tmp_k==0)
                {
                        Application->ProcessMessages();
                }
                tmp_k=(tmp_k+1)%10;
                
                debug_k++;
                min_koef=3000000.0;
                index_min=-1;
                for(i=0;i<n_of_work_otis;)
                {
                        if(n_of_otis>=201)
                        {
                                int Debug=1;
                        }

                        if(o_b_1[smesh%362][smesh/362].NomBort==12162)
                        {
                                int debug=1;
                        }
                        koef=tkGetKoef(work_otis[i]->trassa_indx,
                                        smesh%362,smesh/362);
                        if(koef<0.0)
                        {
                                tmp_indx=work_otis[i]->trassa_indx;
//���� ������ ������ ������ �� ������ ���������������
                                if(i<n_of_work_otis-1)
                                {
                                      work_otis[i]=work_otis[n_of_work_otis-1];
                                }
                                n_of_work_otis--;
//��������� ����� �����������
                                tpTestForKillTrassa(tmp_indx);
                                continue;
                        }
                        if(koef<min_koef)
                        {
                                min_koef=koef;
                                index_min=work_otis[i]->trassa_indx;
                        }
                        i++;
                }
                if(index_min>=0)
                {
                        smesh_n=tpAddOtmetkaToTrassa(index_min,
                        smesh%362, smesh/362,1);
                        otis[index_min].dfLastTestKoef=min_koef;
                        if(smesh_n>=0)
                        {
                                tpNextNP=smesh_n; //���� ��� ��� �������� �� ���� ������
                        }
                                        GlobalGaugeCurTraekt=
                        last_prev_gauge+
                        (int)(100*((double)(o_b_1[smesh%362][smesh/362].poryadk_nomer)/
                        (double)last_poryadk_nomer));
                        Application->ProcessMessages();
                        smesh=tnGetNextNotPrivyazan(0);
                        continue;
                }
//� ��� ���� ���������� �������� ����� ������



                tzTryZavyazatTrassu(0,smesh);
                GlobalGaugeCurTraekt=
                        last_prev_gauge+
                        (int)(100*((double)(o_b_1[smesh%362][smesh/362].poryadk_nomer)/
                        (double)last_poryadk_nomer));

                Application->ProcessMessages();
                smesh=tnGetNextNotPrivyazan(0);
        }
        for(i=0;i<n_of_work_otis;i++)
        {
             tmp_indx=work_otis[i]->trassa_indx;
             tpTestForKillTrassa(tmp_indx);
        }
        if(work_otis)free(work_otis);
        work_otis=NULL;
        max_n_of_work_otis=n_of_work_otis=0;

        return 1;
}






//������ ����������� �� ��������, ������ ������� �������� �� �������� ���������� �����
int OBZORY_1::tpObhodPryamoyPonly()
{
        int tmp_k=0;
        long smesh,smesh_n;
        double koef,min_koef;
        int i=0,index_min;
        int tmp_indx;
        int last_prev_gauge=GlobalGaugeCurTraekt;



        n_of_work_otis=0;
        max_n_of_work_otis=100;
        work_otis=
                (ONE_TRASSA_INFO**)realloc(work_otis,
                        sizeof(ONE_TRASSA_INFO*)*max_n_of_work_otis);
//
        smesh=tnGetFirstNotPrivyazan(1);
        while(smesh>=0)
        {
                if(tmp_k==0)
                {
                        Application->ProcessMessages();
                }
                tmp_k=(tmp_k+1)%10;
                min_koef=3000000.0;
                index_min=-1;
                for(i=0;i<n_of_work_otis;)
                {
                        koef=tkGetKoef(work_otis[i]->trassa_indx,
                                        smesh%362,smesh/362);
                        if(koef<0.0)
                        {
                                tmp_indx=work_otis[i]->trassa_indx;
//���� ������ ������ ������ �� ������ ���������������
                                if(i<n_of_work_otis-1)
                                {
                                      work_otis[i]=work_otis[n_of_work_otis-1];
                                }
                                n_of_work_otis--;
//��������� ����� �����������
                                tpTestForKillTrassa(tmp_indx);
                                continue;
                        }
                        if(koef<min_koef)
                        {
                                min_koef=koef;
                                index_min=work_otis[i]->trassa_indx;
                        }
                        i++;
                }
                if(index_min>=0)
                {
                        smesh_n=tpAddOtmetkaToTrassa(index_min,
                        smesh%362, smesh/362, 1);
                        otis[index_min].dfLastTestKoef=min_koef;
                        if(smesh_n>=0)
                        {
                                tpNextNP=smesh_n; //���� ��� ��� �������� �� ���� ������
                        }
                        GlobalGaugeCurTraekt=
                                last_prev_gauge+
                                (int)(100*((double)(o_b_1[smesh%362][smesh/362].poryadk_nomer)/
                                (double)last_poryadk_nomer));
                        Application->ProcessMessages();
                        smesh=tnGetNextNotPrivyazan(1);
                        continue;
                }
//� ��� ���� ���������� �������� ����� ������
                tzTryZavyazatTrassu(1,smesh);
                GlobalGaugeCurTraekt=
                        last_prev_gauge+
                        (int)(100*((double)(o_b_1[smesh%362][smesh/362].poryadk_nomer)/
                        (double)last_poryadk_nomer));
                Application->ProcessMessages();


                smesh=tnGetNextNotPrivyazan(1);
        }
        for(i=0;i<n_of_work_otis;i++)
        {
             tmp_indx=work_otis[i]->trassa_indx;
             tpTestForKillTrassa(tmp_indx);
        }
        if(work_otis)free(work_otis);
        work_otis=NULL;
        max_n_of_work_otis=n_of_work_otis=0;
        return 1;
}



static int  tpSortFunctionForObhodObratny(const void *a, const void *b)
{
   ONE_TRASSA_INFO *a1, *b1;
   a1=(ONE_TRASSA_INFO *)a;
   b1=(ONE_TRASSA_INFO *)b;
   if(a1->last_poryadk_nomer>b1->last_poryadk_nomer)return (-1);
   return 1;
}



//�������� ����������� �� �������� ��� ������� - ������ ������ ������� ���������� � ���������� �������
int OBZORY_1::tpObhodPovtorny()
{
        int tmp_k=0;
        long smesh,smesh_n;
        double koef,min_koef;
        int i=0,index_min;
        int last_prev_gauge=GlobalGaugeCurTraekt;


//�������� ����� �������� ������
        indx_otis_pk_not_priv=tpNewTrassa(0x14000000);
        indx_otis_uvd_not_priv=tpNewTrassa(0x15000000);
        indx_otis_rbs_not_priv=tpNewTrassa(0x16000000);


        n_of_work_otis=0;
        max_n_of_work_otis=100;
        work_otis=
                (ONE_TRASSA_INFO**)realloc(work_otis,
                        sizeof(ONE_TRASSA_INFO*)*max_n_of_work_otis);
        sort_otis=(ONE_TRASSA_INFO**)malloc(
                        sizeof(ONE_TRASSA_INFO*)*n_of_otis);

         n_of_sort_otis=0;
         n_next_sort_indx=0;
        for(i=1;i<n_of_otis;i++)
        {
           if(otis[i].lFirstIndex>=0)
           {
              sort_otis[n_of_sort_otis]=&(otis[i]);
              n_of_sort_otis++;
           }
        }

        if(n_of_sort_otis==0)
        {
                if(sort_otis)
                {
                        free(sort_otis);
                        sort_otis=NULL;
                }
                if(work_otis)
                {
                        free(work_otis);
                        work_otis=NULL;
                }
                n_of_work_otis=n_of_sort_otis=max_n_of_work_otis=0;
                return 0;
        }

        qsort(sort_otis,  n_of_sort_otis, sizeof(ONE_TRASSA_INFO*),tpSortFunctionForObhodObratny);


        smesh=tnGetLastNotPrivyazan(0);
        while(smesh>=0)
        {
               if(tmp_k==0)
               {
                     Application->ProcessMessages();   
               }
               tmp_k=(tmp_k+1)%10;

               min_koef=3000000.0;
               index_min=-1;
               if((o_b_1[smesh%362][smesh/362].Status&3)==1)
               {
                        int iDebug=1;
               }


//��������� ������ ����� ������ ������� � work_otis
              for(;n_next_sort_indx<n_of_sort_otis;n_next_sort_indx++)
              {
                if(o_b_1[smesh%362][smesh/362].poryadk_nomer<=
                     sort_otis[n_next_sort_indx]->last_poryadk_nomer)
                {
                        if(n_of_work_otis==max_n_of_work_otis)
                        {
                            max_n_of_work_otis+=100;
                            work_otis=
                                (ONE_TRASSA_INFO**)realloc(work_otis,
                                   sizeof(ONE_TRASSA_INFO*)*max_n_of_work_otis);

                        }
                        work_otis[n_of_work_otis]=sort_otis[n_next_sort_indx];
                        n_of_work_otis++;
                }else{
                        break;
                }
              }

              for(i=0;i<n_of_work_otis;)
              {
                   koef=tkGetKoefPovtor(work_otis[i]->trassa_indx,
                                        smesh%362,smesh/362);
                   if(koef<0)
                   {
                   //���� ������ ������ ������ �� ������ ���������������
                        if(i<n_of_work_otis-1)
                        {
                             work_otis[i]=work_otis[n_of_work_otis-1];
                        }
                        n_of_work_otis--;
                        continue;
                   }
                   if(koef<min_koef)
                   {
                        min_koef=koef;
                        index_min=work_otis[i]->trassa_indx;
                   }
                   i++;
              }
              if(index_min>=0)
              {
                      tpAddOtmetkaToTrassa(index_min,
                                   smesh%362, smesh/362,0);
              }else{
                      tpSendToNeedNumBort(smesh);
              }


              GlobalGaugeCurTraekt=
                   last_prev_gauge+100-
                   (int)(100*((double)(o_b_1[smesh%362][smesh/362].poryadk_nomer)/
                   (double)last_poryadk_nomer));
                Application->ProcessMessages();

              smesh=tnGetPrevNotPrivyazan(0);
        }
        if(work_otis)
        {
           free(work_otis);
           work_otis=NULL;
        }
        n_of_work_otis=max_n_of_work_otis=0;
        if(sort_otis)
        {
           free(sort_otis);
           sort_otis=NULL;
        }
        n_of_sort_otis=0;
        return 1;
}

//������� ������������� ����� � ���������������� ��������� �����
//������ ����� ���������� ��������������� �� tpObhodPovtorny
int OBZORY_1::tpSendToNeedNumBort(long smesh)
{
       long smesh1;
       unsigned long ulNomerBorta;
       int isFinding=0;


       if((o_b_1[smesh%362][smesh/362].Status&3)==1)
       {

                 smesh1=o_b_1[smesh%362][smesh/362].AbsPrev;

                 while(smesh1>=0)
                 {

                        if((o_b_1[smesh%362][smesh/362].Time-
                                o_b_1[smesh1%362][smesh1/362].Time)>TO/5)
                        {
                                break;
                        }

                        if(o_b_1[smesh1%362][smesh1/362].Status&KANAL_P_STATUS)
                        {
//������ ������ ���� P, ���� ��������� ������ � ���������
                            if((ABS(toolsGetDAzimuth(o_b_1[smesh1%362][smesh1/362].Azmt,
                                                    o_b_1[smesh%362][smesh/362].Azmt))<RSA_P1)&&
                               (ABS(o_b_1[smesh1%362][smesh1/362].Dlnst-o_b_1[smesh%362][smesh/362].Dlnst)<
                                                                  RSD_P1)
                              )
                            {
 //���� ��������� ����� �����
                                ulNomerBorta=otis[o_b_1[smesh1%362][smesh1/362].IndxBrt].ulNumber;
                                if(((ulNomerBorta&0xFF000000)==0x00000000) ||
                                   ((ulNomerBorta&0xFF000000)==0x01000000) ||
                                   ((ulNomerBorta&0xFF000000)==0x02000000))
                               {
                                     isFinding=1;
                                     break;
                               }

                            }

                        }
                        smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                 }

                 if(!isFinding)
                 {
                        smesh1=o_b_1[smesh%362][smesh/362].AbsNext;

                        while(smesh1>=0)
                        {

                                if((o_b_1[smesh1%362][smesh1/362].Time-
                                o_b_1[smesh%362][smesh/362].Time)>TO/5)
                                {
                                        break;
                                }

                                if(o_b_1[smesh1%362][smesh1/362].Status&KANAL_P_STATUS)
                                {
//������ ������ ���� P, ���� ��������� ������ � ���������
                                        if((ABS(toolsGetDAzimuth(o_b_1[smesh1%362][smesh1/362].Azmt,
                                                    o_b_1[smesh%362][smesh/362].Azmt))<RSA_P1)&&
                                        (ABS(o_b_1[smesh1%362][smesh1/362].Dlnst-o_b_1[smesh%362][smesh/362].Dlnst)<
                                                                  RSD_P1))
                                        {
 //���� ��������� ����� �����
                                                ulNomerBorta=otis[o_b_1[smesh1%362][smesh1/362].IndxBrt].ulNumber;
                                                if(((ulNomerBorta&0xFF000000)==0x00000000) ||
                                                     ((ulNomerBorta&0xFF000000)==0x01000000) ||
                                                     ((ulNomerBorta&0xFF000000)==0x02000000))
                                                {
                                                        isFinding=1;
                                                        break;
                                                }
                                        }

                                }
                                smesh1=o_b_1[smesh1%362][smesh1/362].AbsNext;
                        }
                 }





                tpAddToOtisWithParnye(smesh%362,smesh/362,
                                      indx_otis_pk_not_priv);


       }else{
                if(o_b_1[smesh%362][smesh/362].Status&REJIM_UVD_STATUS)
                {
                        smesh1=o_b_1[smesh%362][smesh/362].AbsPrev;
                        while(smesh1>=0)
                        {
                             if((o_b_1[smesh%362][smesh/362].Time-
                                o_b_1[smesh1%362][smesh1/362].Time)>TO/5)
                             {
                                break;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==1)
                             {
                                 smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                                 continue;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                             {
                                 if(o_b_1[smesh%362][smesh/362].ParnOtschet1>=0)
                                 {
                                        smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                                        continue;
                                 }
                             }
                             ulNomerBorta=otis[o_b_1[smesh1%362][smesh1/362].IndxBrt].ulNumber;
                             if((ulNomerBorta&0xFF000000)==0x01000000)
                             {


                                 if(o_b_1[smesh1%362][smesh1/362].NomBort==
                                    o_b_1[smesh%362][smesh/362].NomBort||
                                    o_b_1[smesh1%362][smesh1/362].OprVysota==
                                    o_b_1[smesh%362][smesh/362].OprVysota)
                                 {
                                      if(
                        (ABS(toolsGetDAzimuth(o_b_1[smesh1%362][smesh1/362].Azmt,
                            o_b_1[smesh%362][smesh/362].Azmt))<RSA_RBS1)&&
         (ABS(o_b_1[smesh1%362][smesh1/362].Dlnst-o_b_1[smesh%362][smesh/362].Dlnst)<
           RSD_RBS1)
                                         )
                                         {
                                                o_b_1[smesh1%362][smesh1/362].ParnForDubl=
                                                        smesh;
                                                isFinding=1;
                                                break;
                                         }
                                 }
                             }
                             smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                        }

                        smesh1=o_b_1[smesh%362][smesh/362].AbsNext;
                        if(!isFinding)while(smesh1>=0)
                        {
                             if((o_b_1[smesh1%362][smesh1/362].Time-
                                o_b_1[smesh%362][smesh/362].Time)>TO/5)
                             {
                                break;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==1)
                             {
                                 smesh1=o_b_1[smesh1%362][smesh1/362].AbsNext;
                                 continue;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                             {
                                 if(o_b_1[smesh%362][smesh/362].ParnOtschet1>=0)
                                 {
                                        smesh1=o_b_1[smesh1%362][smesh1/362].AbsNext;
                                        continue;
                                 }
                             }

                             ulNomerBorta=otis[o_b_1[smesh1%362][smesh1/362].IndxBrt].ulNumber;
                             if((ulNomerBorta&0xFF000000)==0x01000000)
                             {
                                 if(o_b_1[smesh1%362][smesh1/362].NomBort==
                                    o_b_1[smesh%362][smesh/362].NomBort||
                                    o_b_1[smesh1%362][smesh1/362].OprVysota==
                                    o_b_1[smesh%362][smesh/362].OprVysota)
                                 {
                                      if(
                        (ABS(toolsGetDAzimuth(o_b_1[smesh1%362][smesh1/362].Azmt,
                            o_b_1[smesh%362][smesh/362].Azmt))<RSA_RBS1)&&
         (ABS(o_b_1[smesh1%362][smesh1/362].Dlnst-o_b_1[smesh%362][smesh/362].Dlnst)<
           RSD_RBS1)
                                         )
                                         {
                                                o_b_1[smesh1%362][smesh1/362].ParnForDubl=
                                                        smesh;
                                                break;
                                         }
                                 }
                             }
                             smesh1=o_b_1[smesh1%362][smesh1/362].AbsNext;
                        }

                        tpAddToOtisWithParnye(smesh%362,smesh/362,
                                      indx_otis_uvd_not_priv);
                }else
                if(o_b_1[smesh%362][smesh/362].Status&REJIM_RBS_STATUS)
                {
                        smesh1=o_b_1[smesh%362][smesh/362].AbsPrev;
                        while(smesh1>=0)
                        {
                             if((o_b_1[smesh%362][smesh/362].Time-
                                o_b_1[smesh1%362][smesh1/362].Time)>TO/5)
                             {
                                break;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==1)
                             {
                                 smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                                 continue;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                             {
                                 if(o_b_1[smesh%362][smesh/362].ParnOtschet1>=0)
                                 {
                                        smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                                        continue;
                                 }
                             }


                             ulNomerBorta=otis[o_b_1[smesh1%362][smesh1/362].IndxBrt].ulNumber;
                             if((ulNomerBorta&0xFF000000)==0x02000000)
                             {
                                 if(o_b_1[smesh1%362][smesh1/362].NomBort==
                                    o_b_1[smesh%362][smesh/362].NomBort||
                                    o_b_1[smesh1%362][smesh1/362].OprVysota==
                                    o_b_1[smesh%362][smesh/362].OprVysota)
                                 {
                                      if(
                        (ABS(toolsGetDAzimuth(o_b_1[smesh1%362][smesh1/362].Azmt,
                            o_b_1[smesh%362][smesh/362].Azmt))<RSA_RBS1)&&
         (ABS(o_b_1[smesh1%362][smesh1/362].Dlnst-o_b_1[smesh%362][smesh/362].Dlnst)<
           RSD_RBS1)
                                         )
                                         {
                                                o_b_1[smesh1%362][smesh1/362].ParnForDubl=
                                                        smesh;
                                                isFinding=1;
                                                break;    
                                         }
                                 }
                             }
                             smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                        }

                        smesh1=o_b_1[smesh%362][smesh/362].AbsNext;
                        if(!isFinding)while(smesh1>=0)
                        {
                             if((o_b_1[smesh1%362][smesh1/362].Time-
                                o_b_1[smesh%362][smesh/362].Time)>TO/5)
                             {
                                break;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==1)
                             {
                                 smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                                 continue;
                             }

                             if((o_b_1[smesh%362][smesh/362].Status&3)==3)
                             {
                                 if(o_b_1[smesh%362][smesh/362].ParnOtschet1>=0)
                                 {
                                        smesh1=o_b_1[smesh1%362][smesh1/362].AbsPrev;
                                        continue;
                                 }
                             }
                             ulNomerBorta=otis[o_b_1[smesh1%362][smesh1/362].IndxBrt].ulNumber;
                             if((ulNomerBorta&0xFF000000)==0x02000000)
                             {
                                 if(o_b_1[smesh1%362][smesh1/362].NomBort==
                                    o_b_1[smesh%362][smesh/362].NomBort||
                                    o_b_1[smesh1%362][smesh1/362].OprVysota==
                                    o_b_1[smesh%362][smesh/362].OprVysota)
                                 {
                                      if(
                        (ABS(toolsGetDAzimuth(o_b_1[smesh1%362][smesh1/362].Azmt,
                            o_b_1[smesh%362][smesh/362].Azmt))<RSA_RBS1)&&
         (ABS(o_b_1[smesh1%362][smesh1/362].Dlnst-o_b_1[smesh%362][smesh/362].Dlnst)<
           RSD_RBS1)
                                         )
                                         {
                                                o_b_1[smesh1%362][smesh1/362].ParnForDubl=
                                                        smesh;
                                                break;        
                                         }
                                 }
                             }
                             smesh1=o_b_1[smesh1%362][smesh1/362].AbsNext;
                        }


                        tpAddToOtisWithParnye(smesh%362,smesh/362,
                                      indx_otis_rbs_not_priv);
                }else{
                        return 0;
                }
       }
       return 1;
}

//������� ���������� ������ ������������ ���������
//������� ����� ���� � ��� �� ����� � ���� � ��� �� ������� � ���������
int OBZORY_1::tpFindKO_Borts()
{
    int i;
    long smesh, smesh_n;
    double dAzmt,dDaln;
    double dfDopDAzmt, dfDopDDaln;
    for(i=1;i<n_of_otis;i++)
    {
        Application->ProcessMessages();
        if(otis[i].lFirstIndex<0||
           (otis[i].ulNumber&0xFF000000)>0x02000000||
           (otis[i].ulNumber&0xFF000000)==0)
        {
            continue;
        }
        if(otis[i].ulNumber&0xFF000000==0x01000000)
        {
                dfDopDAzmt=RSA_UVD1;
                dfDopDDaln=RSD_UVD1;
        }else{
                dfDopDAzmt=RSA_RBS1;
                dfDopDDaln=RSD_RBS1;
        }

        smesh=tnGetFirstMainOtshet(i);
        smesh_n=tnGetNextMainOtshet(smesh);
        while(smesh_n>=0)
        {
                dAzmt=toolsGetDAzimuth(
                    o_b_1[smesh%362][smesh/362].Azmt,
                    o_b_1[smesh_n%362][smesh_n/362].Azmt);
                if(dAzmt>dfDopDAzmt)
                {
                        break;
                }
                dDaln=o_b_1[smesh_n%362][smesh_n/362].Dlnst-
                      o_b_1[smesh%362][smesh/362].Dlnst;
                if(ABS(dDaln)>dfDopDDaln)
                {
                      break;
                }
                smesh_n=tnGetNextMainOtshet(smesh_n);
        }
        if(smesh_n<0)
        {
//��� ����������� �������
           otis[i].ulNumber|=0x10000000;
           smesh=otis[i].lFirstIndex;
           while(smesh>=0)
           {
                o_b_1[smesh%362][smesh/362].Status2|=NE_PODVIJEN_STATUS2;
                smesh=o_b_1[smesh%362][smesh/362].Next;
           }


        }
    }
    return 1;
}


//��������� 15.09.2015
//�������� ��� ������, �� ����� ���������, ����
int  OBZORY_1::tpResetAllTrassa()
{
     int tmp_k=0;    
     long i;
     long nom, smsh;
     i=FirstAbs;

     while(i>=0)
     {
        if(tmp_k==0)
        {
             Application->ProcessMessages();   
        }
        tmp_k=(tmp_k+1)%10;
//���� �����������
        if((i%362)<360)
        {
                tpAddToOtis(i%362, i/362,0);
        }
        i=o_b_1[i%362][i/362].AbsNext;
     };

     n_of_otis=1; //��������� ����� ����� ������ 1 -- ������ ������ ������
     return 1;
}

