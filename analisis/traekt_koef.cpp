//---------------------------------------------------------------------------


#pragma hdrstop

#include "traekt_koef.h"
#include "tools.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

      //����� ���������������� ���������
int OBZORY_1::tkFindInterval(
                        int n_trass,
                        long smesh,        //������� ��������  ��������������� ������
                        double dfInterval,
                        long prev_smesh1, long prev_smesh2,
                        long &smesh1, long &smesh2)
{

//������� ������ ��������� ������� � smesh
    long tmp_smesh;
    double dTime;

    smesh2=tnGetLastMainOtshet(n_trass);
    while(smesh2>=0&&(o_b_1[smesh%362][smesh/362].poryadk_nomer<=
          o_b_1[smesh2%362][smesh2/362].poryadk_nomer))
    {
        smesh2=tnGetPrevMainOtshet(smesh2);
    }
    dTime=toolsGetDTime(o_b_1[smesh2%362][smesh2/362].Time,
                        o_b_1[smesh%362][smesh/362].Time);
    if(dTime<TO/4.0)
    {
        smesh2=tnGetPrevMainOtshet(smesh2);
    }
    if(smesh2<0)return 0;
    if(smesh2==prev_smesh2)
    {
        smesh1=prev_smesh1;
        return 2;
    }

    smesh1=smesh2;
    tmp_smesh=tnGetPrevMainOtshet(smesh1);
    while(tmp_smesh>=0)
    {
         if(toolsGetDTime(o_b_1[tmp_smesh%362][tmp_smesh/362].Time,
                          o_b_1[smesh2%362][smesh2/362].Time)>dfInterval)
         {
                break;
         }
         smesh1=tmp_smesh;
         tmp_smesh=tnGetPrevMainOtshet(smesh1);
    }

    return 1;
}



int OBZORY_1::tkFindIntervalPovtor(
                        int n_trass,
                        long smesh,        //������� ��������  ��������������� ������
                        double dfInterval,
                        long &smesh1, long &smesh2)
{
   long smesh_t1, smesh_t2;
   long cur_smesh;
   long main_first, main_last;
   double dTime;
   int isStop1, isStop2;
   int isStepPart=0;
   main_first=tnGetFirstMainOtshet(n_trass);
   main_last=tnGetLastMainOtshet(n_trass);
//���� smesh_t1 � smesh_t2
   if(o_b_1[smesh%362][smesh/362].poryadk_nomer<
      o_b_1[main_first%362][main_first/362].poryadk_nomer)
   {
       dTime=toolsGetDTime(o_b_1[smesh%362][smesh/362].Time,
                           o_b_1[main_first%362][main_first/362].Time);
       if(dTime<TO/4.0)
       {
                return 0;
       }
       smesh1=main_first;
       smesh2=smesh1;
       smesh_t2=tnGetNextMainOtshet(smesh2);
       while(smesh_t2>=0&&
             toolsGetDTime(o_b_1[smesh1%362][smesh1/362].Time,
                     o_b_1[smesh_t2%362][smesh_t2/362].Time)<=dfInterval)
       {
                smesh2=smesh_t2;
                smesh_t2=tnGetNextMainOtshet(smesh2);
       }
        otis[n_trass].lCurIndex=smesh1;
   }else
   if(o_b_1[smesh%362][smesh/362].poryadk_nomer>
        o_b_1[main_last%362][main_last/362].poryadk_nomer)
   {
        dTime=toolsGetDTime(o_b_1[main_last%362][main_last/362].Time,
                           o_b_1[smesh%362][smesh/362].Time);
        if(dTime<TO/4.0)
        {
                return 0;
        }
        smesh2=main_last;
        smesh1=smesh2;
        smesh_t1=tnGetPrevMainOtshet(smesh1);
        while(smesh_t1>=0&&
             toolsGetDTime(o_b_1[smesh_t1%362][smesh_t1/362].Time,
                     o_b_1[smesh2%362][smesh2/362].Time)<=dfInterval)
        {
                smesh1=smesh_t1;
                smesh_t1=tnGetPrevMainOtshet(smesh1);
        }
        otis[n_trass].lCurIndex=smesh2;
   }else{
        cur_smesh=otis[n_trass].lCurIndex;
        if(cur_smesh<0)
        {
                cur_smesh=main_first;
        }
        isStop1=isStop2=0;

//���� ��� ������� ������, ������� ������ ���� ����� ������ � �����
        if(o_b_1[smesh%362][smesh/362].poryadk_nomer<
              o_b_1[cur_smesh%362][cur_smesh/362].poryadk_nomer)
        {
                while(cur_smesh>=0&&
                  o_b_1[smesh%362][smesh/362].poryadk_nomer<
                    o_b_1[cur_smesh%362][cur_smesh/362].poryadk_nomer)
                {
                        smesh_t2=cur_smesh;
                        cur_smesh=tnGetPrevMainOtshet(cur_smesh);
                }
                if(cur_smesh<0)
                {
                      smesh_t1=smesh_t2;
                      smesh_t2=tnGetNextMainOtshet(smesh_t1);
                      isStop1=1;
                }else{
                     smesh_t1=cur_smesh;
                }

        }else{
                while(cur_smesh>=0&&
                  o_b_1[smesh%362][smesh/362].poryadk_nomer>
                  o_b_1[cur_smesh%362][cur_smesh/362].poryadk_nomer)
                {
                        smesh_t1=cur_smesh;
                        cur_smesh=tnGetNextMainOtshet(cur_smesh);
                }
                if(cur_smesh<0)
                {
                     smesh_t2=smesh_t1;
                     smesh_t1=tnGetPrevMainOtshet(smesh_t2);
                     isStop2=1;
                }else{
                     smesh_t2=cur_smesh;
                }
        }
        otis[n_trass].lCurIndex=smesh_t2;


        if(isStop1)
        {
                dTime=toolsGetDTime(
                        o_b_1[smesh%362][smesh/362].Time,
                        o_b_1[smesh_t1%362][smesh_t1/362].Time
                           );
                if(dTime<TO/4.0)
                {
                        return 0;
                }
        }else
        if(isStop2)
        {
                dTime=toolsGetDTime(
                       o_b_1[smesh_t2%362][smesh_t2/362].Time,
                        o_b_1[smesh%362][smesh/362].Time
                                   );
                if(dTime<TO/4.0)
                {
                        return 0;
                }
         }else{
                dTime=toolsGetDTime(
                        o_b_1[smesh_t1%362][smesh_t1/362].Time,
                        o_b_1[smesh%362][smesh/362].Time
                                   );
                if(dTime<TO/4.0)
                {
                        return 0;
                }
                dTime=toolsGetDTime(
                        o_b_1[smesh%362][smesh/362].Time,
                        o_b_1[smesh_t2%362][smesh_t2/362].Time
                                   );
                if(dTime<TO/4.0)
                {
                        return 0;
                }
         }

         smesh1=smesh_t1;
         smesh2=smesh_t2;
         while(toolsGetDTime(
                       o_b_1[smesh_t1%362][smesh_t1/362].Time,
                       o_b_1[smesh_t2%362][smesh_t2/362].Time
                            )<=dfInterval)
         {

                if(isStepPart)
                {
                        if(isStop1)
                        {
                              smesh_t2=tnGetNextMainOtshet(smesh_t2);
                        }else{
                              smesh_t1=tnGetPrevMainOtshet(smesh_t1);
                        }
                        isStepPart=0;
                }else{
                        if(isStop2)
                        {
                              smesh_t1=tnGetPrevMainOtshet(smesh_t1);
                        }else{
                              smesh_t2=tnGetNextMainOtshet(smesh_t2);
                        }
                        isStepPart=1;                        
                }

                if(smesh_t1<0)
                {
                        smesh_t1=smesh1;
                        isStop1=1;
                }
                if(smesh_t2<0)
                {
                        smesh_t2=smesh2;
                        isStop2=1;
                }
                if(isStop1&&isStop2)break;
                if(!isStop1)
                {
                        smesh1=smesh_t1;
                }
                if(!isStop2)
                {
                        smesh2=smesh_t2;
                }
         }
         if(smesh1<0)return 0;
   }
   return 1;
}

//��������� ������������ �� �������
//��������� ������������ ��� �������������
  int OBZORY_1::tkGetKoefForApprox(int n_trass,
                            int revers_time,    //�������� ������ �������
                            double dfInterval,
                            long nomer,
                            long indx)
{
     int iRet;
     double dfAzmt[100];
     double dfDlnst[100];
     double dfTimes[100];
     int n;
     long i;
     long smesh1, smesh2;
     int stepen;
//������� ��������� ��������
     if(revers_time)
     {
          iRet=tkFindIntervalPovtor(n_trass,nomer+indx*362,
                                dfInterval, smesh1, smesh2);
     }else{
          iRet=tkFindInterval(n_trass,nomer+indx*362,
                        dfInterval,
                         otis[n_trass].lFirstApproxIndex,
                        otis[n_trass].lLastApproxIndex,
                        smesh1, smesh2);
     }
     if(iRet<=0)return iRet;

     if(smesh1==otis[n_trass].lFirstApproxIndex&&
        smesh2==otis[n_trass].lLastApproxIndex)
     {
        return 1;
     }
//��������������� ����������
     otis[n_trass].dfLastTestKoef=4000000.0;
     otis[n_trass].lLastAddingSmesh=-1;

     n=0;
     i=smesh1;
     while(1)
     {
        dfAzmt[n]=o_b_1[i%362][i/362].Azmt;
        dfDlnst[n]=o_b_1[i%362][i/362].Dlnst;
        dfTimes[n]=o_b_1[i%362][i/362].Time;
        n++;
        if(i==smesh2)
        {
                break;
        }
        i=tnGetNextMainOtshet(i);
     };

     if(n>6)
     {
        stepen=2;
     }else{
        stepen=1;
     }
     iRet=GetStatesByData_TFC(dfAzmt,dfDlnst,dfTimes,n,stepen,
                &(otis[n_trass].state_koef_koef));
     if(iRet<=0)return -100+iRet;
     otis[n_trass].lFirstApproxIndex=smesh1;
     otis[n_trass].lLastApproxIndex=smesh2;
     return 1;
}



double OBZORY_1::tkGetKoef(int n_trass, long nomer, long indx)
{
     int iRet;
     double dTime;
     double dopDAzmt, dopDDaln;
     double dfAzmt, dfDlnst;
     double dfX, dfY;

     double dX, dY, dfKoef;
     if((otis[n_trass].ulNumber&0xFF000000)==0x01000000)
     {
         if(o_b_1[nomer][indx].Status&REJIM_RBS_STATUS)
         {
                return 4000000.0;
         }
         dopDAzmt=RSA_UVD1;
         dopDDaln=RSD_UVD1;
     }else
     if((otis[n_trass].ulNumber&0xFF000000)==0x02000000)
     {
         if(o_b_1[nomer][indx].Status&REJIM_UVD_STATUS)
         {
                return 4000000.0;
         }

         dopDAzmt=RSA_RBS1;
         dopDDaln=RSD_RBS1;
     }else{
         if(o_b_1[nomer][indx].Status&KANAL_S_STATUS)
         {
                return 4000000.0;
         }

         dopDAzmt=RSA_P1;
         dopDDaln=RSD_P1;
     }


     dTime=toolsGetDTime(
      o_b_1[otis[n_trass].lLastIndex%362][otis[n_trass].lLastIndex/362].Time,
      o_b_1[nomer][indx].Time);
     if(dTime>period_for_out_trass)
     {
        return -1.0;
     }



     iRet=tkGetKoefForApprox(n_trass,0,approx_interval,nomer,indx);
     if(iRet<1)
     {
           return 4000000.0;
     }
     RaschetAzimuthAndDalnost_TFC(&(otis[n_trass].state_koef_koef),
                                  o_b_1[nomer][indx].Time,
                                  &dfAzmt, &dfDlnst,
                                  &dfX, &dfY);

     if(ABS(toolsGetDAzimuth(dfAzmt,o_b_1[nomer][indx].Azmt))>dopDAzmt||
        ABS(o_b_1[nomer][indx].Dlnst-dfDlnst)>dopDDaln)
     {


          if(ABS(dfX-o_b_1[nomer][indx].X/1000.0)>dopDDaln||
             ABS(dfY-o_b_1[nomer][indx].Y/1000.0)>dopDDaln)
          {
                return 4000000;
          }


     }
     dX=dfX-o_b_1[nomer][indx].X/1000.0;
     dY=dfY-o_b_1[nomer][indx].Y/1000.0;
     dfKoef=dX*dX+dY*dY;

//���� ������ ��� ��� RBS     
     if(((otis[n_trass].ulNumber&0xFF000000)==0x01000000)||
        ((otis[n_trass].ulNumber&0xFF000000)==0x02000000))
     {
        if(((int)(otis[n_trass].ulNumber)&0x00FFFFFF)!=o_b_1[nomer][indx].NomBort)
        {
                dfKoef+=10000.0;
        }
        if((o_b_1[nomer][indx].Status&0x00000003)==1)
        {
                dfKoef+=10000.0;
        }
     }
//������ ���� �������� �����������  � ������������ ��� ������ ������
     if(dfKoef>=otis[n_trass].dfLastTestKoef)
     {
        return 4000000.0;
     }
     return dfKoef;
}


double OBZORY_1::tkGetKoefPovtor(int n_trass, long nomer, long indx)
{
     int iRet;
     double dTime;
     double dfAzmt, dfDlnst;
     double dopDAzmt, dopDDaln;
     double dfX, dfY;

     double dX, dY, dfKoef;
     if((otis[n_trass].ulNumber&0xFF000000)==0x01000000)
     {
         if(o_b_1[nomer][indx].Status&REJIM_RBS_STATUS)
         {
                return 4000000.0;
         }
         dopDAzmt=RSA_UVD1;
         dopDDaln=RSD_UVD1;
     }else
     if((otis[n_trass].ulNumber&0xFF000000)==0x02000000)
     {
         if(o_b_1[nomer][indx].Status&REJIM_UVD_STATUS)
         {
                return 4000000.0;
         }

         dopDAzmt=RSA_RBS1;
         dopDDaln=RSD_RBS1;
     }else{
         if(o_b_1[nomer][indx].Status&KANAL_S_STATUS)
         {
                return 4000000.0;
         }

         dopDAzmt=RSA_P1;
         dopDDaln=RSD_P1;
     }

     dTime=toolsGetDTime(
      o_b_1[nomer][indx].Time,
      o_b_1[otis[n_trass].lFirstIndex%362][otis[n_trass].lFirstIndex/362].Time
      );
     if(dTime>period_for_out_trass)
     {
        return -1.0;
     }

     iRet=tkGetKoefForApprox(n_trass,1,approx_interval,nomer,indx);
     if(iRet<1)
     {
           return 4000000.0;
     }




     RaschetAzimuthAndDalnost_TFC(&(otis[n_trass].state_koef_koef),
                                  o_b_1[nomer][indx].Time,
                                  &dfAzmt, &dfDlnst,
                                  &dfX, &dfY);

     if(ABS(toolsGetDAzimuth(dfAzmt,o_b_1[nomer][indx].Azmt))>dopDAzmt||
        ABS(o_b_1[nomer][indx].Dlnst-dfDlnst)>dopDDaln)
     {

          return 4000000.0;
     }
     dX=dfX-o_b_1[nomer][indx].X/1000.0;
     dY=dfY-o_b_1[nomer][indx].Y/1000.0;
     dfKoef=dX*dX+dY*dY;

     if(((otis[n_trass].ulNumber&0xFF000000)==0x01000000)||
        ((otis[n_trass].ulNumber&0xFF000000)==0x02000000))
     {
        if(((int)(otis[n_trass].ulNumber)&0x00FFFFFF)!=o_b_1[nomer][indx].NomBort)
        {
                dfKoef+=10000.0;
        }
        if((o_b_1[nomer][indx].Status&0x00000003)==1)
        {
                dfKoef+=10000.0;
        }
     }
//������ ���� �������� �����������  � ������������ ��� ������ ������
     return dfKoef;
}

