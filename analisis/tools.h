//---------------------------------------------------------------------------
#ifndef toolsH
#define toolsH
#include <vcl.h>
#include <stdio.h>
//---------------------------------------------------------------------------
#ifndef ABS
#define ABS(X) (((X) > 0) ? (X) : (-(X)))
#endif


#ifndef MAX
#define MAX(X,Y)  (((X) > (Y)) ? (X) : (Y))
#endif

long Okrugl(double Chislo1);

char *QSearchStr(char *Strka, char **Strks,int N_of_Strks);

long MilliSeconds(int NomerSutok,
                  int Chasy,
                  int Minuty,
                  double Secundy);

long Minuts(int NomerSutok,
            int Chasy,
            int Minuty);

long MilliSecondsFromMandS(long Minuty,double Secundy);
void GetHM_MSFormMilliSeconds(long Time,int &H,int &M,  int &S, int &MS);

void SaveFormEdits(TForm *Form, AnsiString FileName);
void LoadFormEdits(TForm *Form, AnsiString FileName);

void SaveFormCBandRB(TForm *Form, AnsiString FileName);
void LoadFormCBandRB(TForm *Form, AnsiString FileName);

int IsTextFile(AnsiString FileName);
FILE *MyFOpenR(AnsiString FileName);

double RastMejduObjektami(double Asimut1, double Dalnost1,
                          double Asimut2, double Dalnost2);

AnsiString FileNameWithoutPath(AnsiString FN);


/*��������� ������� ����� �� ������. ����� ����� � ������ ���� ��������� ���� ���������,
���� ��������, ���� ������ � �������*/
int GetMassivZelyhChiselFromStrka(char *Strka,     //������
                                  long *Numbers,    //������
                                  long *N_of_Numbers,//���������� ������� (�����)
                                  long MaxN_of_Numbers //������������ ���������� �������
                                  );


int SravnDoubleRazryad(double Chislo1,double Chislo2,double Umnoj);

int StrngLstToStrngs(TStringList *SL, TStrings *Ss);

void QSortInt(int *Massiv, int N_of_El);


//��������� ������� �� �������� �����
AnsiString   GetTimeFromDoubleToString(double FloatTime);

void GetTimeFromDouble(double FloatTime, int &Sutki,
                      int &H, int &M, double &S);



/*����������� ���� ��������� M �� N ����������.
���������: FirstSochetaniy, NextSochetaniy, NextSochetaniy2
������� Nmrs - ��� ������� ��������� ���������. ��������������,
��� ����� ��������� N. ������� ��������� �������� �� 0 �� N-1. ���
���� ������� �������  M �� N, M<N. ������� Nmrs ����������� ���: Nmrs[0]=0,
Nmrs[M-1]=M-1. �� ��������� �����, Nmrs[0]=0, Nmrs[M-2]=M-2; Nmrs[M-1]=M � �.�.
��� ��������.
FirstSochetaniy - ������������� (��. ����).
NextSochetaniy - ����������� ���������� ���������, �������� ����� Nmrs.
NextSochetaniy2 - ����������� ���������� ���������.


*/


/*
FirstSochetaniy ������������� Nmrs
Nmrs - ��� ������ �������� �� ����� M ���������. M ������ ���� ������ N, �� ������ 0
������� ��������� ������ Nmrs � ��������� �� 0 �� M-1 ���������� �� 0 �� M-1, � �� M �� N-1
������.
*/
int FirstSochetaniy(long *Nmrs,long N, long M);

/*NextSochetaniy - ��������� ��������� M �� N.
  UbratVydel - ������ ��������� �������� �������� ��������� (�������).
  SdelatVydel - ������ �������� ����� ���������.
  *N_of_Smen - ������� ��������� ���� ������ �� ������ ������� � ������� �����������.
  UbratVydel ����� �� ��������� ������������ � SdelatVydel.

  ������� ���������� 0 � ������ ������, 1 - ���� �������, 2 - ���� ������� M �� N
  ���������.
*/
int NextSochetaniy(long *Nmrs,long N, long M,
                   long *UbratVydel, long *SdelatVydel,
                   long *N_of_Smen);

/*NextSochetaniy2 - ��������� ��������� M �� N.
  ������ ���������� �������� M - �� �������� � Nmrs �� 1, �� ���� Nmrs[M1]=Nmrs[M1]+1,
  � ��� ��������� ���������������� � ������������

  UbratVydel - ������ ��������� �������� �������� ��������� (�������).
  SdelatVydel - ������ �������� ����� ���������.
  *N_of_Smen - ������� ��������� ���� ������ �� ������ ������� � ������� �����������.
  UbratVydel ����� �� ��������� ������������ � SdelatVydel.

  ������� ���������� 0 � ������ ������, 1 - ���� �������, 2 - ���� ������� M �� N
  ���������.
*/
int NextSochetaniy2(long *Nmrs,long N, long M, long M1,
                   long *UbratVydel, long *SdelatVydel,
                   long *N_of_Smen);


/*��������� GetTochkiNaGranice. ��������� ��������� ����������� ������ � �����.
��� ���������� ���� ������������ �������� ������ ���� �����.
*/



int GetTochkiNaGranicePriam(double X1,double Y1,double X2, double Y2, //���������� �����q
                       long X1r, long Y1r, long X2r, long Y2r,  //���������� ���������������
                       long *X1ret,long *Y1ret, long *X2ret, long *Y2ret); //���������� ������������



/*��������� ���������� ����� ����� � ����� Temp*/
AnsiString GetTempFileInTempPath(void);

/*��������� ��������, ������� ������� ������ �� dfTime1 �� dfTime2 � �����.
�������, ��� ������ 12 ����� ������ �� �����
*/
double toolsGetDTime(   double dfTime1,
                        double dfTime2);


/*��������� �������� �������� � ��������*/
double toolsGetDAzimuth(double Azmt1, double Azmt2);

#endif
