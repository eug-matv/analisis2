//---------------------------------------------------------------------------


#pragma hdrstop
#include <math.h>
#include "tools_get_nomer_obzora.h"
#include "tools.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


int tgnoGetIzmenenieObzora(double to, int is180,  //���� ������ � �����
                           double tm1, double azmt1, int isVtor1,  //����� � ����� � ������
                           double tm2, double azmt2, int isVtor2    //����� � ����� � ������ � ��
                        )
{
        int n1;
        double n;
        if(is180)
        {
                if(isVtor1)
                {
                        azmt1+=180.0;
                        if(azmt1>=360.0)azmt1-=360.0;
                }
                if(isVtor2)
                {
                        azmt2+=180.0;
                        if(azmt2>=360.0)azmt2-=360.0;
                }
        }


        if(tm1>tm2)
        {
                tm2+=24.0;
        }


        n=(tm2-tm1)/to-(azmt2-azmt1)/360.0;
        n1=floor(n);
        if(n-n1<0.5)return n1;
        return (n1+1);
}

int tgnoGetNomer90Sektora(int is180,  //���� ������ � �����
                          int nomer_obzora, //����� ������
                          double azmt, int isVtor  //����� � ����� � ������
                       )
{
        if(is180&&isVtor)
        {
                azmt+=180.0;
                if(azmt>=360.0)azmt-=360.0;
        }

        if(azmt<89.999999)return  nomer_obzora*4;
        if(azmt<179.999999)return (nomer_obzora*4+1);
        if(azmt<269.99999)return (nomer_obzora*4+2);
        return (nomer_obzora*4+3);

}