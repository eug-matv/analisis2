//---------------------------------------------------------------------------


#pragma hdrstop

#include <IniFiles.hpp>


#include "work_ini_form.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)


//#define __SAVE_IN_INI_FILE__  1


int popa()
{
        return 12;
}


int iffSaveFormsInfoInIniFile(TForm *frm,
                              const AnsiString &ini_file_name)
{
        int i,j;
        TIniFile *ini_file;

        if(!frm)return (-1);

        ini_file=new TIniFile(ini_file_name);

        ini_file->WriteString(frm->Name,frm->Name,frm->Caption);
        for(i=0;i<frm->ComponentCount;i++)
        {
            if(i==53)
            {
                int debug=1;
            }
            if(frm->Components[i]->ClassNameIs(String("TLabel")))
            {
                ini_file->WriteString(frm->Name,
                                      ((TLabel*)(frm->Components[i]))->Name,
                                      ((TLabel*)(frm->Components[i]))->Caption);
            }else
            if(frm->Components[i]->ClassNameIs(String("TButton")))
            {
                ini_file->WriteString(frm->Name,
                                      ((TButton*)(frm->Components[i]))->Name,
                                      ((TButton*)(frm->Components[i]))->Caption);
            }else
            if(frm->Components[i]->ClassNameIs(String("TMenuItem")))
            {
                 ini_file->WriteString(frm->Name,
                                      ((TMenuItem*)(frm->Components[i]))->Name,
                                      ((TMenuItem*)(frm->Components[i]))->Caption);
            }else
            if(frm->Components[i]->ClassNameIs(String("TRadioGroup")))
            {
                  TRadioGroup* rg=(TRadioGroup*)(frm->Components[i]);

                  ini_file->WriteString(frm->Name,
                                        rg->Name,
                                        rg->Caption);
                  for(j=0;j<rg->Items->Count;j++)
                  {
                     ini_file->WriteString(frm->Name,
                                        rg->Name+String("__")+IntToStr(1000+j),
                                        rg->Items->Strings[j]);
                  }

            }else
            if(frm->Components[i]->ClassNameIs(String("TGroupBox")))
            {
                  ini_file->WriteString(frm->Name,
                                      ((TGroupBox*)(frm->Components[i]))->Name,
                                      ((TGroupBox*)(frm->Components[i]))->Caption);
            }else
            if(frm->Components[i]->ClassNameIs(String("TCheckBox")))
            {
                  ini_file->WriteString(frm->Name,
                                      ((TCheckBox*)(frm->Components[i]))->Name,
                                      ((TCheckBox*)(frm->Components[i]))->Caption);
            }else
            if(frm->Components[i]->ClassNameIs(String("TRadioButton")))
            {
                  TRadioButton* rbt=(TRadioButton*)(frm->Components[i]);
                  ini_file->WriteString(frm->Name,
                                      rbt->Name,
                                      rbt->Caption);

            }else
            if(frm->Components[i]->ClassNameIs(String("TTabControl")))
            {
                TTabControl *tc=(TTabControl*)(frm->Components[i]);
                for(j=0;j<tc->Tabs->Count;j++)
                {
                     ini_file->WriteString(frm->Name,
                                        tc->Name+String("__")+IntToStr(1000+j),
                                        tc->Tabs->Strings[j]);
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TTabSheet")))
            {
                  ini_file->WriteString(frm->Name,
                                      ((TTabSheet*)(frm->Components[i]))->Name,
                                      ((TTabSheet*)(frm->Components[i]))->Caption);
            }

        }


        delete ini_file;
        return 1;
}




int iffLoadFormsInfoFromIniFile(TForm *frm,
                              const AnsiString &ini_file_name)
{
        int i,j;
        TIniFile *ini_file;
        AnsiString as;

        if(!frm)return (-1);

        ini_file=new TIniFile(ini_file_name);

        ini_file->WriteString(frm->Name,frm->Name,frm->Caption);

        as=ini_file->ReadString(frm->Name,frm->Name,String("19790930"));
        if(as!=String("19790930"))
        {
               frm->Caption=as;
        }

        for(i=0;i<frm->ComponentCount;i++)
        {
            if(frm->Components[i]->ClassNameIs(String("TLabel")))
            {
                as=ini_file->ReadString(frm->Name,
                                        frm->Components[i]->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TLabel*)(frm->Components[i]))->Caption=as;
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TButton")))
            {
                as=ini_file->ReadString(frm->Name,
                                        frm->Components[i]->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TButton*)(frm->Components[i]))->Caption=as;
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TMenuItem")))
            {
                as=ini_file->ReadString(frm->Name,
                                        frm->Components[i]->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TMenuItem*)(frm->Components[i]))->Caption=as;
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TRadioGroup")))
            {
                TRadioGroup* rg=(TRadioGroup*)(frm->Components[i]);
                as=ini_file->ReadString(frm->Name,
                                        rg->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TRadioGroup*)(frm->Components[i]))->Caption=as;
                }
                for(j=0;j<rg->Items->Count;j++)
                {
                    as=ini_file->ReadString(frm->Name,
                                            rg->Name+String("__")+IntToStr(1000+j),
                                            String("19790930"));
                    if(as!=String("19790930"))
                    {
                        rg->Items->Strings[j]=as;
                    }
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TGroupBox")))
            {
                as=ini_file->ReadString(frm->Name,
                                        frm->Components[i]->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TGroupBox*)(frm->Components[i]))->Caption=as;
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TCheckBox")))
            {
                as=ini_file->ReadString(frm->Name,
                                        frm->Components[i]->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TCheckBox*)(frm->Components[i]))->Caption=as;
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TRadioButton")))
            {
                as=ini_file->ReadString(frm->Name,
                                        frm->Components[i]->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TRadioButton*)(frm->Components[i]))->Caption=as;
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TTabControl")))
            {
                TTabControl *tc=(TTabControl*)(frm->Components[i]);
                for(j=0;j<tc->Tabs->Count;j++)
                {
                    as=ini_file->ReadString(frm->Name,
                                            tc->Name+String("__")+IntToStr(1000+j),
                                            String("19790930"));
                    if(as!=String("19790930"))
                    {
                        tc->Tabs->Strings[j]=as;
                    }
                }
            }else
            if(frm->Components[i]->ClassNameIs(String("TTabSheet")))
            {
                as=ini_file->ReadString(frm->Name,
                                        frm->Components[i]->Name,
                                        String("19790930"));
                if(as!=String("19790930"))
                {
                       ((TTabSheet*)(frm->Components[i]))->Caption=as;
                }
            }

        }
        delete ini_file;
        return 1;
}


int iffWorkWithIniFile(TForm *frm,
                        const AnsiString &ini_file_name_without_path)
{
  AnsiString ini_file_name;
  ini_file_name=ExtractFilePath(Application->ExeName)+ini_file_name_without_path;


#ifdef __SAVE_IN_INI_FILE__
    return iffSaveFormsInfoInIniFile(frm, ini_file_name);
#else
    return iffLoadFormsInfoFromIniFile(frm, ini_file_name);
#endif        


}
