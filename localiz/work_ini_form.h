//---------------------------------------------------------------------------

#ifndef work_ini_formH
#define work_ini_formH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include <Forms.hpp>
#include <dstring.h>

int popa();


/*������� iffSaveFormsInfoInIniFile.
��������� ��������
*/
int iffSaveFormsInfoInIniFile(TForm *frm,
                              const AnsiString &ini_file_name
 );

/*������� iffLoadFormsInfoFromIniFile.
��������� ��������
*/

int iffLoadFormsInfoFromIniFile(TForm *frm,
                              const AnsiString &ini_file_name);


int iffWorkWithIniFile(TForm *frm,
                        const AnsiString &ini_file_name_without_path);


#endif
