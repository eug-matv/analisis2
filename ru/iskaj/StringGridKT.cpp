//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <Grids.hpp>
#include <stdio.h>
#include <string.h>
#include "StringGridKT.h"
#include "tools.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)

int SetRowStringGrid(TStringGrid *SG,
                     int N_of_Row,
                     int N_of_Col)
{
   if(N_of_Row<1||N_of_Col<1)return 0;
   SG->RowCount=N_of_Row;
   SG->ColCount=N_of_Col;
   return 1;
}

int SaveStringGridToFile(FILE *fp,  //���� ������ ���� ������ ��� w � ��������� ������
                     TStringGrid *SG)
{
  if(!fp)return 0;
  int i,j;

//�������� ����� ����� � ����� ��������
  fprintf(fp,"%d  %d\n",SG->RowCount, SG->ColCount);

//�������� ����� ������������� ����� � ��������
  fprintf(fp,"%d  %d\n",SG->FixedRows, SG->FixedCols);

  for(i=0;i<SG->RowCount;i++)
  {
    for(j=0;j<SG->ColCount;j++)
    {
       fputs(SG->Cells[j][i].c_str(),fp);
       fprintf(fp,"\n");
    }
  }
  return 1;
}

int LoadStringGridFromFile(FILE *fp,  //���� ������ ���� ������ ��� w � ��������� ������
                     TStringGrid *SG)
{
  if(!fp)return 0;
  int i,j;
  int Ret;
  int ZnchI;
  int Dlina;
  char Strka[500],*RetC;

  Ret=fscanf(fp,"%d",&ZnchI);
  if(!Ret)return 0;
  SG->RowCount=ZnchI;

  Ret=fscanf(fp,"%d",&ZnchI);
  if(!Ret)return 0;
  SG->ColCount=ZnchI;

  Ret=fscanf(fp,"%d",&ZnchI);
  if(!Ret)return 0;
  SG->FixedRows=ZnchI;

  Ret=fscanf(fp,"%d",&ZnchI);
  if(!Ret)return 0;
  SG->FixedCols=ZnchI;

  do
  {
    Ret=getc(fp);
  }while(Ret!='\n'&&!feof(fp));



  for(i=0;i<SG->RowCount;i++)
  {
    for(j=0;j<SG->ColCount;j++)
    {
      RetC=fgets(Strka,499,fp);
      if(!RetC)
      {
        Strka[0]=0;
      }else{
        Dlina=strlen(Strka);
        if(Strka[Dlina-1]=='\n')Strka[Dlina-1]='\0';
      }  
      SG->Cells[j][i]=Strka;
    }
  }
  return 1;
}



//��������������� ��������� ������������ ��� ������ � ������ For631.cpp
//� ��������� OTSCHETS631::ZapolnitMIV_SG
int VyvodSG1(
             TStringGrid *MySG,
             int NStrka,
             int Indx=-1,
             long Nom1=-1,
             long Nom2=-1,
             long Vys=-1,
             double Azmt=-1,
             double Dlnst=-1,
             double Time=-1,
             bool IsklOtch=false,
             bool IsFalseVysota=false,
             bool IsFalseNomer=false,
             int Zona=-1,
             short EVysota=0
            )
{
   int i;

   char Strka[100];
   if(Indx<0)
   {
     MySG->Cells[0][NStrka]=-1;
     if(Indx==-1)
     {
       for(i=1;i<11;i++)       MySG->Cells[i][NStrka]="*********";
     }else{
       for(i=1;i<11;i++)       MySG->Cells[i][NStrka]="";
     }
   }else{
     MySG->Cells[0][NStrka]=IntToStr(Indx);
     MySG->Cells[1][NStrka]=IntToStr((int)Nom1);
     MySG->Cells[2][NStrka]=IntToStr((int)Nom2);
     MySG->Cells[3][NStrka]=IntToStr((int)Vys);

     sprintf(Strka,"%.2lf",Azmt);
     MySG->Cells[4][NStrka]=Strka;

     sprintf(Strka,"%.2lf",Dlnst);
     MySG->Cells[5][NStrka]=Strka;

     MySG->Cells[6][NStrka]=GetTimeFromDoubleToString(Time);
     if(IsklOtch)
     {
        MySG->Cells[7][NStrka]="����.";
     }else{
        MySG->Cells[7][NStrka]="����.";
     }

     if(IsFalseVysota)
     {
        MySG->Cells[8][NStrka]="������";
     }else{
        if(Vys==0)
        {
           MySG->Cells[8][NStrka]="�������";
        }else{
           MySG->Cells[8][NStrka]="������.";
        }
     }

     if(IsFalseNomer)
     {
          MySG->Cells[9][NStrka]="������";
     }else{
          MySG->Cells[9][NStrka]="������.";
     }
     MySG->Cells[10][NStrka]=IntToStr(Zona);
     MySG->Cells[11][NStrka]=IntToStr((int)EVysota);

   }
   return 1;
}



//��������������� ��������� ������������ ��� ������ � ������ VyvodData.cpp
//� ���������
int VyvodSG2(
             TStringGrid *MySG,
             int NStrka,
             int Indx,
             int NomerO,
             long NB1,
             long NB2,
             long Vysota,
             double Azmt,
             double Dlnst,
             double Time,
             bool IsklOtch,
             bool IsFalseVysota,
             bool IsFalseNomer,
             int Zona,         //������� ������ ������
             long OthBort,     //����� ���������� ����� � ������
             double Rast       //���������� �� ����� �����
             )
{
     int i;
     char Strka[100];

     MySG->Cells[0][NStrka]=IntToStr(Indx);
     MySG->Cells[1][NStrka]=IntToStr((int)NomerO);
     MySG->Cells[2][NStrka]=IntToStr((int)NB1);
     MySG->Cells[3][NStrka]=IntToStr((int)NB2);
     MySG->Cells[4][NStrka]=IntToStr((int)Vysota);

     sprintf(Strka,"%.2lf",Azmt);
     MySG->Cells[5][NStrka]=Strka;

     sprintf(Strka,"%.2lf",Dlnst);
     MySG->Cells[6][NStrka]=Strka;

     MySG->Cells[7][NStrka]=GetTimeFromDoubleToString(Time);
     if(IsklOtch)
     {
        MySG->Cells[8][NStrka]="����.";
     }else{
        MySG->Cells[8][NStrka]="����.";
     }

     if(IsFalseVysota)
     {
        MySG->Cells[9][NStrka]="������";
     }else{
        if(Vysota==0)
        {
           MySG->Cells[9][NStrka]="�������";
        }else{
           MySG->Cells[9][NStrka]="������.";
        }
     }

     if(IsFalseNomer)
     {
          MySG->Cells[10][NStrka]="������";
     }else{
          MySG->Cells[10][NStrka]="������.";
     }
     MySG->Cells[11][NStrka]=IntToStr(Zona);
     if(OthBort<0)
     {
       MySG->Cells[12][NStrka]="���";
     }else{
       MySG->Cells[12][NStrka]=IntToStr((int)OthBort);
     }

     if(OthBort<0)
     {
       MySG->Cells[13][NStrka]="���";
     }else{
        sprintf(Strka,"%.3lf",Rast);
       MySG->Cells[13][NStrka]=Strka;

     }


   return 1;
}
