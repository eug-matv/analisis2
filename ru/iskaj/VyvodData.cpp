//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "GlavFFor631.h"
#include "For631.h"
#include "GlobalData631.h"
#include "NewNomer.h"
#include "StringGridKT.h"
#include "VyvodData.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFOData *FOData;
//---------------------------------------------------------------------------
__fastcall TFOData::TFOData(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------

void __fastcall TFOData::MakeShapkaAndRazmery(void)
{
   int i;
   StringGrid1->ColCount=14;
   StringGrid1->RowCount=2;
   StringGrid1->DefaultRowHeight=20;
   StringGrid1->FixedCols=2;
   StringGrid1->ColWidths[0]=1;
   StringGrid1->ColWidths[1]=40;
   StringGrid1->ColWidths[2]=45;
   StringGrid1->ColWidths[3]=45;
   StringGrid1->ColWidths[4]=45;
   StringGrid1->ColWidths[5]=45;
   StringGrid1->ColWidths[6]=45;
   StringGrid1->ColWidths[7]=70;
   StringGrid1->ColWidths[8]=45;
   StringGrid1->ColWidths[9]=45;
   StringGrid1->ColWidths[10]=45;
   StringGrid1->ColWidths[11]=45;
   StringGrid1->ColWidths[12]=45;
   StringGrid1->ColWidths[13]=45;

//������� �������
   StringGrid1->Cells[0][0]="";
   StringGrid1->Cells[1][0]="�";
   StringGrid1->Cells[2][0]="���. �";
   StringGrid1->Cells[3][0]="���. �";
   StringGrid1->Cells[4][0]="������";
   StringGrid1->Cells[5][0]="������";
   StringGrid1->Cells[6][0]="�����.";
   StringGrid1->Cells[7][0]="�����";
   StringGrid1->Cells[8][0]="������";
   StringGrid1->Cells[9][0]="������";
   StringGrid1->Cells[10][0]="�����";
   StringGrid1->Cells[11][0]="��.�.";
   StringGrid1->Cells[12][0]="��.�.";
   StringGrid1->Cells[13][0]="����.";
   StringGrid1->Cells[0][1]="-1";
   for(i=1;i<14;i++)StringGrid1->Cells[i][1]="";
}


void __fastcall TFOData::SdvigUpDownStringGrid1(int Sdvg)
{
  int KIndx;
  int i;
  OTSCHETS631 *O631;
  O631=(OTSCHETS631*)Otschs631;

  if(Sdvg>0)
  {

    KIndx=PervIndx;
    for(i=1;i<=Sdvg;i++)
    {
      KIndx=O631->GlobNextOtschet(KIndx,!(CBIsklBort->Checked),
                                            !(CBIsklOtsch->Checked),
                                            false,false,false);
    }
    if(KIndx==-1)
    {
       PervIndx=O631->GlobLastOtschet(!(CBIsklBort->Checked),
                                      !(CBIsklOtsch->Checked),
                                      false,false,false);
    }else{
       PervIndx=KIndx;
    }
    ZapolnStringGrid1();
    return;
  }
  if(Sdvg<0)
  {
    KIndx=PervIndx;
    for(i=1;i<=-Sdvg;i++)
    {
      KIndx=O631->GlobPrevOtschet(KIndx,!(CBIsklBort->Checked),
                                            !(CBIsklOtsch->Checked),
                                            false,false,false);
    }
    if(KIndx==-1)
    {
       PervIndx=O631->GlobFirstOtschet(!(CBIsklBort->Checked),
                                      !(CBIsklOtsch->Checked),
                                      false,false,false);
    }else{
       PervIndx=KIndx;
    }
    ZapolnStringGrid1();
    return;
  }


}


int __fastcall TFOData::FindOtschetIndx(int Indx)
{
  int TekI,i,N_2,TekI1;
  OTSCHETS631 *O631;
  TGridRect MyRect;
  O631=(OTSCHETS631 *)Otschs631;
  TekI=O631->GlobFirstOtschet(!(CBIsklBort->Checked),
                              !(CBIsklOtsch->Checked),
                               false,false,false);
  TekI1=TekI;
  while(TekI>=0)
  {
     if(TekI==Indx)
     {
       N_2=StringGrid1->Height/44;
       TekI1=TekI;
       for(i=0;i<N_2;i++)
       {
         TekI1=O631->GlobPrevOtschet(TekI1,
                              !(CBIsklBort->Checked),
                              !(CBIsklOtsch->Checked),
                               false,false,false);
         if(TekI1<0)
         {
            TekI1=O631->GlobFirstOtschet(!(CBIsklBort->Checked),
                              !(CBIsklOtsch->Checked),
                               false,false,false);
            break;
         }
       }
       PervIndx=TekI1;
       ZapolnStringGrid1();
       for(i=1;i<StringGrid1->RowCount;i++)
       {
         if(StringGrid1->Cells[0][i].ToInt()==Indx)
         {
           MyRect.Left=0;
           MyRect.Right=12;
           MyRect.Top=i;
           MyRect.Bottom=i;
           StringGrid1->Selection=MyRect;
           break;
         }
       }
       return 1;
     }
     TekI=O631->GlobNextOtschet(TekI, !(CBIsklBort->Checked),
           !(CBIsklOtsch->Checked), false,false,false);
  };
  PervIndx=-1;
  ZapolnStringGrid1();
  return 0;
}


void __fastcall TFOData::ZapolnStringGrid1(void)
{
//������ �������� ������
  int i,Indx;
  int PI;
  int N_of_Pered, N_of_Posle,TekIO;   //�����1
  int N_of_S;            //����� �����
  long NB;
  OTSCHETS631 *O631;

  int OtherIndxBort,Ret;
  double Rast;
  O631=(OTSCHETS631*)Otschs631;
  MakeShapkaAndRazmery();
  if(PervIndx<0||PervIndx>=O631->N_of_OO)
  {
    PervIndx=O631->GlobFirstOtschet(!(CBIsklBort->Checked),
                              !(CBIsklOtsch->Checked),
                              false,false,
                              CBFilterOnly->Checked);
    if(PervIndx<0)
    {
      return;
    }
  }
  if(O631->IsklBort[O631->OO[PervIndx].IndexNomer]&&!(CBIsklBort->Checked)||
     O631->OO[PervIndx].IsklOtch&&!(CBIsklOtsch->Checked))
  {
    PI=O631->GlobNextOtschet(PervIndx,!(CBIsklBort->Checked),
                                            !(CBIsklOtsch->Checked),
                                            false,false,
                                            CBFilterOnly->Checked);
    if(PI<0)
    {
      PI=O631->GlobPrevOtschet(PervIndx,!(CBIsklBort->Checked),
                                        !(CBIsklOtsch->Checked),
                                            false,false,
                                            CBFilterOnly->Checked);
    }
    if(PI<0)return;
    PervIndx=PI;
  }

//������� ��� �������

  N_of_Pered=O631->GetNomerNachalo(PervIndx,
                                   CBIsklBort->Checked,
                                   CBIsklOtsch->Checked,
                                   CBFilterOnly->Checked
                                   );
  N_of_Posle=O631->GetChisloOtschetovPosle(PervIndx,
                                   CBIsklBort->Checked,
                                   CBIsklOtsch->Checked,
                                   CBFilterOnly->Checked);
//��������� ����� ����� � StringGrid1
  N_of_S=StringGrid1->Height/21+3;
  if(N_of_Posle+2<N_of_S)
  {
    StringGrid1->RowCount=N_of_Posle+2;
  }else{
    StringGrid1->RowCount=N_of_S;
  }

  if(SmenaScrollBar1Position)
  {
     ScrollBar1->OnChange=NULL;
     ScrollBar1->Max=10000000;
     ScrollBar1->Position=N_of_Pered;
     ScrollBar1->Min=1;
     ScrollBar1->Max=N_of_Pered+N_of_Posle;
     ScrollBar1->OnChange=ScrollBar1Change;
     PrevScrollBar1Position=ScrollBar1->Position;
  }
  Indx=PervIndx;
  TekIO=N_of_Pered;
//� ������ ����� ��������� ��� ������ ���������
  for(i=1;i<StringGrid1->RowCount;i++)
  {
    Ret=O631->FindBlijOtschetAndRastoyanie(Indx,&OtherIndxBort,&Rast);
    if(Ret!=1)
    {
      OtherIndxBort=-1;
      NB=-1;
    }else{
      NB=O631->NomBort[O631->OO[OtherIndxBort].IndexNomer];
    }

      VyvodSG2(StringGrid1,i,Indx,TekIO,O631->NomBort[O631->OO[Indx].IndexNomer],
             O631->OO[Indx].Nomer,
             O631->OO[Indx].Vysota,
             O631->OO[Indx].Azmt,
             O631->OO[Indx].Dlnst,
             O631->OO[Indx].Time,
             O631->OO[Indx].IsklOtch,
             O631->OO[Indx].IsFalseVysota,
             O631->OO[Indx].Nomer!=O631->NomBort[O631->OO[Indx].IndexNomer],
             O631->OO[Indx].Zona,
             NB,
             Rast);
    Indx=O631->GlobNextOtschet(Indx,!(CBIsklBort->Checked),
                              !(CBIsklOtsch->Checked),false,false,
                               CBFilterOnly->Checked);
    if(Indx<0)break;
    TekIO++;
  }

}




void __fastcall TFOData::ScrollBar1Change(TObject *Sender)
{
    //
//    Timer1->Enabled=true;

   ScrollBar1->OnChange=NULL;
   SmenaScrollBar1Position=false;
   SdvigUpDownStringGrid1(ScrollBar1->Position-PrevScrollBar1Position);
   PrevScrollBar1Position=ScrollBar1->Position;
   ScrollBar1->OnChange=ScrollBar1Change;
   if(IsVnizuKursorScrollBar1)
   {
     StringGrid1->Row=StringGrid1->Height/22;
     IsVnizuKursorScrollBar1=false;
   }
}
//---------------------------------------------------------------------------

void __fastcall TFOData::CBIsklBortClick(TObject *Sender)
{
   SmenaScrollBar1Position=true;
   ZapolnStringGrid1();
}
//---------------------------------------------------------------------------

void __fastcall TFOData::CBIsklOtschClick(TObject *Sender)
{
  SmenaScrollBar1Position=true;
  ZapolnStringGrid1();
}
//---------------------------------------------------------------------------

void __fastcall TFOData::FormShow(TObject *Sender)
{
  HelpFile=FGlav631->HelpFile;
  HelpContext=4283;
  PrevScrollBar1Position=-1;
  ZapolnStringGrid1();
}
//---------------------------------------------------------------------------

void __fastcall TFOData::FormClose(TObject *Sender, TCloseAction &Action)
{
  if(Type==1)
  {
       CloseFODataUVD();
  }else{
       CloseFODataRBS();
  }
}
//---------------------------------------------------------------------------

void __fastcall TFOData::Timer1Timer(TObject *Sender)
{
   ScrollBar1->OnChange=NULL;
   SmenaScrollBar1Position=false;
   SdvigUpDownStringGrid1(ScrollBar1->Position-PrevScrollBar1Position);
   ScrollBar1->OnChange=ScrollBar1Change;

//   ZapolnStringGrid1();
   Timer1->Enabled=false;
   if(IsVnizuKursorScrollBar1)
   {
     StringGrid1->Row=StringGrid1->Height/22;
     IsVnizuKursorScrollBar1=false;
   }

}
//---------------------------------------------------------------------------


void __fastcall TFOData::FormResize(TObject *Sender)
{
  SmenaScrollBar1Position=true;
  ZapolnStringGrid1();
}
//---------------------------------------------------------------------------

int __fastcall TFOData::ShowIndx(int Indx)
{
   OnShow=NULL;
   Show();
   FindOtschetIndx(Indx);
   OnShow=FormShow;
   return 1;
}

void __fastcall TFOData::StringGrid1KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{

   if(Key==VK_UP)
   {
     if(StringGrid1->Row<=1)
     {
         ScrollBar1->Position=ScrollBar1->Position-1;
         Key=0;

     }
   }

   if(Key==VK_DOWN)
   {
     if(StringGrid1->Row>=StringGrid1->Height/22-1)
     {
         IsVnizuKursorScrollBar1=true;
         ScrollBar1->Position=ScrollBar1->Position+1;
         Key=0;

     }
   }


}
//---------------------------------------------------------------------------

void __fastcall TFOData::PopupMenu1Popup(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr->Checked=O631->OO[Indx].IsFalseVysota;
      MRasmatrivat->Checked=!O631->OO[Indx].IsklOtch;
    }else{
      MVysErr->Checked=false;
      MRasmatrivat->Checked=false;
    }
  }else{
    MVysErr->Checked=false;
    MRasmatrivat->Checked=false;
  }



}
//---------------------------------------------------------------------------



void __fastcall TFOData::MVysErrClick(TObject *Sender)
{
  MVysErr->Checked=!MVysErr->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      if(O631->OO[Indx].Vysota==0)return;
      O631->OO[Indx].IsFalseVysota=MVysErr->Checked;

      if(MVysErr->Checked)
      {
        StringGrid1->Cells[9][StringGrid1->Row]="����.";
      }else{
        StringGrid1->Cells[9][StringGrid1->Row]="�����.";
      }
      if(Type==1)
      {
        IzmenFalseVysotaFDOB_UVD();
        IzmenFalseVysotaFIVData_UVD();
      }else{
        IzmenFalseVysotaFDOB_RBS();
        IzmenFalseVysotaFIVData_RBS();
      }
      O631->ZapolnMIV();
    }
  }

}
//---------------------------------------------------------------------------

void __fastcall TFOData::N1Click(TObject *Sender)
{
//
  int IndxBort, Indx;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)
  {
    return;
  }
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  if(Indx<0)return;
  IndxBort=O631->OO[Indx].IndexNomer;
  if(Type==1)
  {
 //���
     OpenFDOB_UVD_Indx(Otschs631,IndxBort,Indx);
  }else{
 //RBS
     OpenFDOB_RBS_Indx(Otschs631,IndxBort,Indx);
  }
}
//---------------------------------------------------------------------------


//�������� ��� ���� ��������� �� ������
int __fastcall TFOData::ObnovIskajVys(void)
{
  int i;
  int Indx;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  for(i=1;i<StringGrid1->RowCount;i++)
  {
    Indx=StringGrid1->Cells[0][i].ToInt();
    if(Indx<0)continue;
    if(O631->OO[Indx].IsFalseVysota)
    {
       StringGrid1->Cells[9][i]="������";

    }else if(O631->OO[Indx].Vysota==0)
    {
       StringGrid1->Cells[9][i]="�������";
    }else{
       StringGrid1->Cells[9][i]="��������";
    }
    if(O631->OO[Indx].Nomer!=O631->NomBort[O631->OO[Indx].IndexNomer])
    {
      StringGrid1->Cells[10][i]="������";
    }else{
      StringGrid1->Cells[10][i]="��������";
    }

//������� �����
    StringGrid1->Cells[2][i]=IntToStr((int)O631->NomBort[O631->OO[Indx].IndexNomer]);

  }


  return 1;
}


void __fastcall TFOData::N3Click(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr2->Checked=O631->OO[Indx].IsFalseVysota;
      MRasmatrivat2->Checked=!O631->OO[Indx].IsklOtch;
    }else{
      MVysErr2->Checked=false;
      MRasmatrivat2->Checked=false;
    }
  }else{
    MVysErr2->Checked=false;
    MRasmatrivat2->Checked=false;
  }

}
//---------------------------------------------------------------------------

void __fastcall TFOData::MVysErr2Click(TObject *Sender)
{
  int i;
  MVysErr2->Checked=!MVysErr2->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      if(O631->OO[Indx].Vysota==0)return;
      O631->OO[Indx].IsFalseVysota=MVysErr2->Checked;

      if(MVysErr2->Checked)
      {
        StringGrid1->Cells[9][StringGrid1->Row]="����.";
      }else{
        StringGrid1->Cells[9][StringGrid1->Row]="�����.";
      }
      if(Type==1)
      {
        IzmenFalseVysotaFDOB_UVD();
        IzmenFalseVysotaFIVData_UVD();
      }else{
        IzmenFalseVysotaFDOB_RBS();
        IzmenFalseVysotaFIVData_RBS();
      }
      O631->ZapolnMIV();
    }
  }
}
//---------------------------------------------------------------------------


void __fastcall TFOData::N2Click(TObject *Sender)
{
//
  int Ret;
  int KodOO;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)return;
  KodOO=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  FNewNomer=new TFNewNomer(NULL);
  FNewNomer->KodOO=KodOO;
  FNewNomer->Otschs631=Otschs631;
  Ret=FNewNomer->Vyzov();
  delete FNewNomer;
  if(Ret==1)
  {
     ObnovIskajVys();
     
//������� ������ �� ������� ����
     if(Type==1)
     {
       IzmenFalseVysotaFDOB_UVD();
       FGlav631->O631_UVD->ZapolnitCheckListBox(FGlav631->CLB_UVD_Borta);
     }else{
       IzmenFalseVysotaFDOB_RBS();
       FGlav631->O631_RBS->ZapolnitCheckListBox(FGlav631->CLB_RBS_Borta);
     }
  }
}
//---------------------------------------------------------------------------




void __fastcall TFOData::MRasmatrivatClick(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
       O631->OO[Indx].IsklOtch=MRasmatrivat->Checked;
       if(O631->OO[Indx].IsklOtch)
       {
          StringGrid1->Cells[8][StringGrid1->Row]="����.";
       }else{
          StringGrid1->Cells[8][StringGrid1->Row]="���.";
       }
       if(Type==1)
       {
          IzmenFalseVysotaFDOB_UVD();
          IzmenFalseVysotaFIVData_UVD();
          FGlav631->O631_UVD->ZapolnitCheckListBox(FGlav631->CLB_UVD_Borta);
       }else{
          IzmenFalseVysotaFDOB_RBS();
          IzmenFalseVysotaFIVData_RBS();
          FGlav631->O631_RBS->ZapolnitCheckListBox(FGlav631->CLB_RBS_Borta);
       }
    }
  }
}
//---------------------------------------------------------------------------




void __fastcall TFOData::MRasmatrivat2Click(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
       O631->OO[Indx].IsklOtch=MRasmatrivat2->Checked;
       if(O631->OO[Indx].IsklOtch)
       {
          StringGrid1->Cells[8][StringGrid1->Row]="����.";
       }else{
          StringGrid1->Cells[8][StringGrid1->Row]="���.";
       }
       if(Type==1)
       {
          IzmenFalseVysotaFDOB_UVD();
          IzmenFalseVysotaFIVData_UVD();
          FGlav631->O631_UVD->ZapolnitCheckListBox(FGlav631->CLB_UVD_Borta);
       }else{
          IzmenFalseVysotaFDOB_RBS();
          IzmenFalseVysotaFIVData_RBS();
          FGlav631->O631_RBS->ZapolnitCheckListBox(FGlav631->CLB_RBS_Borta);
       }
    }
  }

}
//---------------------------------------------------------------------------

void __fastcall TFOData::CBFilterOnlyClick(TObject *Sender)
{
   SmenaScrollBar1Position=true;
   ZapolnStringGrid1();	
}
//---------------------------------------------------------------------------



void __fastcall TFOData::N6Click(TObject *Sender)
{
  if(!FileExists(HelpFile))
  {
    MessageBox(NULL,"���� ������ ��� ������ ������ �� ������������ - ������� ����������� ���������",
           "��������", MB_OK);
    return;
  }

  if(HelpFile.Trim().Length())
  {
    WinHelp(Handle,HelpFile.c_str(),HELP_CONTEXT, HelpContext);
  }		
}
//---------------------------------------------------------------------------



void __fastcall TFOData::FormCreate(TObject *Sender)
{
       iffWorkWithIniFile(this, String("iskaj_vis.ini"));        
}
//---------------------------------------------------------------------------



