//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop


#include "GlavFFor631.h"
#include "IskajNomView.h"
#include "For631.h"
#include "For631_2.h"
#include "GlobalData631.h"
#include "work_ini_form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFIskajNomView *FIskajNomView;
//---------------------------------------------------------------------------
__fastcall TFIskajNomView::TFIskajNomView(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------




void __fastcall TFIskajNomView::VisualizeDataOfBort(void)
{
  OTSCHETS631 *O631;
  O631=(OTSCHETS631*)Otchs631;

  O631->ZapolnitStringGridIskNom(StringGrid1,CBVklIsklBort->Checked,CB_VklIskl->Checked,
                           CB_FilterOnly->Checked);
  if(Type==1)
  {
     LNomer->Caption=String("����� ���. ��������� �� �������");
  }else{
     LNomer->Caption=String("����� RBS. ��������� �� �������");
  }

}
void __fastcall TFIskajNomView::CB_VklIsklClick(TObject *Sender)
{
   VisualizeDataOfBort();
}
//---------------------------------------------------------------------------


void __fastcall TFIskajNomView::FormShow(TObject *Sender)
{
  HelpFile=FGlav631->HelpFile;
  HelpContext=4285;

  VisualizeDataOfBort();
}
//---------------------------------------------------------------------------
int __fastcall TFIskajNomView::FindOtschetIndx(int Indx)
{
  int i;
  int Chislo, Chislo2;
  TGridRect MyRect;

  for(i=1;i<StringGrid1->RowCount;i++)
  {
    Chislo=StringGrid1->Cells[0][i].ToInt();
    if(Chislo<0)break;
    if(Chislo==Indx)
    {
//������� ������
       Chislo2=i-StringGrid1->VisibleRowCount/2;
       if(Chislo2<1)Chislo2=1;
       StringGrid1->TopRow=Chislo2;
       MyRect.Left=1;
       MyRect.Right=13;
       MyRect.Top=i;
       MyRect.Bottom=i;
       StringGrid1->Selection=MyRect;
       return 1;
    }
  }
  return 0;
}

int __fastcall TFIskajNomView::FindOtschetRow(int Row)
{
  int Chislo2;
  TGridRect MyRect;
  if(Row<1||Row>StringGrid1->RowCount)
  {
    return 0;
  }

  Chislo2=Row-StringGrid1->VisibleRowCount/2;
  if(Chislo2<1)Chislo2=1;
  StringGrid1->TopRow=Chislo2;
  MyRect.Left=1;
  MyRect.Right=13;
  MyRect.Top=Row;
  MyRect.Bottom=Row;
  StringGrid1->Selection=MyRect;

}



//---------------------------------------------------------------------------




void __fastcall TFIskajNomView::PopupMenu1Popup(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr->Checked=O631->OO[Indx].IsFalseVysota;
      MRasmatrivat->Checked=!O631->OO[Indx].IsklOtch;
    }else{
      MVysErr->Checked=false;
      MRasmatrivat->Checked=false;
    }
  }else{
    MVysErr->Checked=false;
    MRasmatrivat->Checked=false;
  }

}
//---------------------------------------------------------------------------


void __fastcall TFIskajNomView::MVysErrClick(TObject *Sender)
{
  int i;
  MVysErr->Checked=!MVysErr->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      if(O631->OO[Indx].Vysota==0)return;
      O631->OO[Indx].IsFalseVysota=MVysErr->Checked;
      if(MVysErr->Checked)
      {
        StringGrid1->Cells[10][StringGrid1->Row]="����.";
      }else{
        StringGrid1->Cells[10][StringGrid1->Row]="�����.";
      }
      if(Type==1)
      {
        IzmenFalseVysotaFOData_UVD();
        IzmenFalseVysotaFIVData_UVD();
      }else{
        IzmenFalseVysotaFOData_RBS();
        IzmenFalseVysotaFIVData_RBS();
      }
      O631->ZapolnMIV();
    }
  }
}
//---------------------------------------------------------------------------


void __fastcall TFIskajNomView::N1Click(TObject *Sender)
{
  int IndxBort, Indx;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)
  {
    return;
  }
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  if(Indx<0)return;
  if(Type==1)
  {
    OpenFODataUVD_Indx(Otchs631,Indx);
  }else{
    OpenFODataRBS_Indx(Otchs631,Indx);
  }
}
//---------------------------------------------------------------------------
int __fastcall TFIskajNomView::ObnovIskajVys(void)
{
  int i;
  int Indx;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  for(i=1;i<StringGrid1->RowCount;i++)
  {
    Indx=StringGrid1->Cells[0][i].ToInt();
    if(Indx<0)continue;
    if(O631->OO[Indx].IsFalseVysota)
    {
       StringGrid1->Cells[10][i]="������";

    }else if(O631->OO[Indx].Vysota==0)
    {
       StringGrid1->Cells[10][i]="�������";
    }else{
       StringGrid1->Cells[10][i]="��������";
    }
    if(O631->OO[Indx].Nomer!=O631->NomBort[O631->OO[Indx].IndexNomer])
    {
      if(O631->OO[Indx].Nomer!=0)
      {
        StringGrid1->Cells[11][i]="������";
      }else{
        StringGrid1->Cells[11][i]="�������";
      }
    }else{
      StringGrid1->Cells[11][i]="��������";
    }
//������� �����
    StringGrid1->Cells[2][i]=IntToStr((int)O631->NomBort[O631->OO[Indx].IndexNomer]);

//������ ������� �� ���� ��� �������� �� ������������
    if(O631->OO[Indx].IsklOtch)
    {
      StringGrid1->Cells[9][i]="����.";
    }else{
      StringGrid1->Cells[9][i]="���.";
    }
  }
}



void __fastcall TFIskajNomView::MVysErr2Click(TObject *Sender)
{
  int i;
  MVysErr2->Checked=!MVysErr2->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      if(O631->OO[Indx].Vysota==0)return;
      O631->OO[Indx].IsFalseVysota=MVysErr2->Checked;
      if(MVysErr2->Checked)
      {
        StringGrid1->Cells[10][StringGrid1->Row]="����.";
      }else{
        StringGrid1->Cells[10][StringGrid1->Row]="�����.";
      }
      if(Type==1)
      {
        IzmenFalseVysotaFOData_UVD();
        IzmenFalseVysotaFIVData_UVD();
      }else{
        IzmenFalseVysotaFOData_RBS();
        IzmenFalseVysotaFIVData_RBS();
      }
      O631->ZapolnMIV();
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIskajNomView::N3Click(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr2->Checked=O631->OO[Indx].IsFalseVysota;
      MRasmatrivat2->Checked=!O631->OO[Indx].IsklOtch;
    }else{
      MVysErr2->Checked=false;
      MRasmatrivat2->Checked=false;
    }
  }else{
    MVysErr2->Checked=false;
    MRasmatrivat2->Checked=false;
  }
}
//---------------------------------------------------------------------------





void __fastcall TFIskajNomView::MRasmatrivatClick(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
       O631->OO[Indx].IsklOtch=MRasmatrivat->Checked;
       if(O631->OO[Indx].IsklOtch)
       {
          StringGrid1->Cells[9][StringGrid1->Row]="����.";
       }else{
          StringGrid1->Cells[9][StringGrid1->Row]="���.";
       }
       if(Type==1)
       {
          IzmenFalseVysotaFDOB_UVD();
          IzmenFalseVysotaFIVData_UVD();
          FGlav631->O631_UVD->ZapolnitCheckListBox(FGlav631->CLB_UVD_Borta);
       }else{
          IzmenFalseVysotaFDOB_RBS();
          IzmenFalseVysotaFIVData_RBS();
          FGlav631->O631_RBS->ZapolnitCheckListBox(FGlav631->CLB_RBS_Borta);
       }
    }
  }
}
//---------------------------------------------------------------------------


void __fastcall TFIskajNomView::MRasmatrivat2Click(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
       O631->OO[Indx].IsklOtch=MRasmatrivat2->Checked;
       if(O631->OO[Indx].IsklOtch)
       {
          StringGrid1->Cells[9][StringGrid1->Row]="����.";
       }else{
          StringGrid1->Cells[9][StringGrid1->Row]="���.";
       }
       if(Type==1)
       {
          IzmenFalseVysotaFDOB_UVD();
          IzmenFalseVysotaFIVData_UVD();
          FGlav631->O631_UVD->ZapolnitCheckListBox(FGlav631->CLB_UVD_Borta);
       }else{
          IzmenFalseVysotaFDOB_RBS();
          IzmenFalseVysotaFIVData_RBS();
          FGlav631->O631_RBS->ZapolnitCheckListBox(FGlav631->CLB_RBS_Borta);
       }
    }
  }        
}
//---------------------------------------------------------------------------



void __fastcall TFIskajNomView::CB_FilterOnlyClick(TObject *Sender)
{
    VisualizeDataOfBort();
}
//---------------------------------------------------------------------------


void __fastcall TFIskajNomView::N5Click(TObject *Sender)
{
  if(!FileExists(HelpFile))
  {
    MessageBox(NULL,"���� ������ ��� ������ ������ �� ������������ - ������� ����������� ���������",
           "��������", MB_OK);
    return;
  }

  if(HelpFile.Trim().Length())
  {
    WinHelp(Handle,HelpFile.c_str(),HELP_CONTEXT, HelpContext);
  }
}
//---------------------------------------------------------------------------



void __fastcall TFIskajNomView::N7Click(TObject *Sender)
{
  int IndxBort, Indx;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)
  {
    return;
  }
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  if(Indx<0)return;
  IndxBort=O631->OO[Indx].IndexNomer;
  if(Type==1)
  {
 //���
     OpenFDOB_UVD_Indx(Otchs631,IndxBort,Indx);
  }else{
 //RBS
     OpenFDOB_RBS_Indx(Otchs631,IndxBort,Indx);
  }

}
//---------------------------------------------------------------------------

void __fastcall TFIskajNomView::N8Click(TObject *Sender)
{
  int IndxBort, Indx;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)
  {
    return;
  }
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  if(Indx<0)return;
  IndxBort=O631->OO[Indx].IndexNomer;
  if(Type==1)
  {
 //���
     OpenFDOB_UVD_Indx(Otchs631,IndxBort,Indx);
  }else{
 //RBS
     OpenFDOB_RBS_Indx(Otchs631,IndxBort,Indx);
  }
	
}
//---------------------------------------------------------------------------


void __fastcall TFIskajNomView::FormCreate(TObject *Sender)
{
        iffWorkWithIniFile(this, String("iskaj_vis.ini"));        
}
//---------------------------------------------------------------------------

