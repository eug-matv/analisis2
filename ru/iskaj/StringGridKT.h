//---------------------------------------------------------------------------
#ifndef StringGridKTH
#define StringGridKTH
#include <Grids.hpp>
#include <stdio.h>
//---------------------------------------------------------------------------
int SetRowStringGrid(TStringGrid *SG,
                     int N_of_Row,
                     int N_of_Col);

int LoadStringGridFromFile(FILE *fp,  //���� ������ ���� ������ ��� w � ��������� ������
                     TStringGrid *SG);


int SaveStringGridToFile(FILE *fp,  //���� ������ ���� ������ ��� w � ��������� ������
                     TStringGrid *SG);


//��������������� ��������� ������������ ��� ������ � ������ For631.cpp
//� ��������� OTSCHETS631::ZapolnitMIV_SG
int VyvodSG1(
             TStringGrid *SG,
             int NStrka,
             int Indx,
             long Nom1,
             long Nom2,
             long Vys,
             double Azmt,
             double Dlnst,
             double Time,
             bool IsklOtch,
             bool IsFalseVysota,
             bool IsFalseNomer,
             int Zona,
             short EVysota
            );

//��������������� ��������� ������������ ��� ������ � ������ VyvodData.cpp
//� ���������
int VyvodSG2(
             TStringGrid *MySG,
             int NStrka,
             int Indx,
             int NomerO,
             long NB1,
             long NB2,
             long Vysota,
             double Azmt,
             double Dlnst,
             double Time,
             bool IsklOtch,
             bool IsFalseVysota,
             bool IsFalseNomer,
             int Zona,         //������� ������ ������
             long OthBort,     //����� ���������� ����� � ������
             double Rast       //���������� �� ����� �����
             );

#endif
