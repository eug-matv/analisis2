//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "For631.h"
#include "For631_2.h"
#include "GlavFFor631.h"
#include "GlobalData631.h"
#include "PreviewIskaj.h"
#include "tools.h"
#include "IskajNomView.h"
#include "work_ini_form.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "CGAUGES"
#pragma resource "*.dfm"
TFGlav631 *FGlav631;
TCGauge *GlobalCGauge1Isk=NULL;  //���������� ���������� ��� �����������



long GlobMaxProgress=100;
long GlobTekProgress=0;


  extern bool IsMoveUVD;
  extern int MoveOtkudaUVD;
  extern bool IsMoveRBS;
  extern int MoveOtkudaRBS;

//---------------------------------------------------------------------------
__fastcall TFGlav631::TFGlav631(TComponent* Owner)
	: TForm(Owner)
{
   char Strka[1000];
   extern AnsiString GlobalPathDLL;
   FirstMakeZagruzka=0;
   if(GlobalPathDLL.Length()==0)
   {
     GetCurrentDirectory(999,Strka);
     ProgPath=Strka;
   }else{
     GlobalPathDLL=GlobalPathDLL.Trim();
     ProgPath=GlobalPathDLL;
   }

   ReadMe=ProgPath+String("\\readme.txt");
   NomersBorts=NULL;
   N_of_NomersBorts=0;
   IsModifyed=false;

}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------
void __fastcall TFGlav631::CBAzmt1Click(TObject *Sender)
{
  IsModifyed=true;
  CIzmenaDiapazonov();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::CBAzmt2Click(TObject *Sender)
{
  IsModifyed=true;
  CIzmenaDiapazonov();
}
//---------------------------------------------------------------------------
void __fastcall TFGlav631::CB_DalnClick(TObject *Sender)
{
  IsModifyed=true;
   CIzmenaDiapazonov();
}

//-----------------------------------------------------
void __fastcall TFGlav631::CB_VysotaClick(TObject *Sender)
{
  IsModifyed=true;
  CIzmenaDiapazonov();
}

//--------------------------------------------------
void __fastcall TFGlav631::CIzmenaDiapazonov(void)
{
  if(CBAzmt1->Checked)
  {
     EAzmtMin1->Enabled=true;
     EAzmtMax1->Enabled=true;


  }else{
     EAzmtMin1->Enabled=false;
     EAzmtMax1->Enabled=false;
     CBAzmt2->Checked=false;
  }

  if(CBAzmt2->Checked)
  {
     EAzmtMin2->Enabled=true;
     EAzmtMax2->Enabled=true;
  }else{
     EAzmtMin2->Enabled=false;
     EAzmtMax2->Enabled=false;
  }

  if(CB_Daln->Checked)
  {
    EDalnMin->Enabled=true;
    EDalnMax->Enabled=true;
  }else{
    EDalnMin->Enabled=false;
    EDalnMax->Enabled=false;
  }

  if(CB_Vysota->Checked)
  {
    CSEMinV->Enabled=true;
    CSEMaxV->Enabled=true;
  }else{
    CSEMinV->Enabled=false;
    CSEMaxV->Enabled=false;
  }

}

//---------------------------------------------------------------------------


void __fastcall TFGlav631::FormClick(TObject *Sender)
{
//����

}
//---------------------------------------------------------------------------





void __fastcall TFGlav631::FormCreate(TObject *Sender)
{
     CIzmenaDiapazonov();
     O631_UVD=new OTSCHETS631;
     O631_RBS=new OTSCHETS631;
     FIskajNomView=new TFIskajNomView(NULL);

     iffWorkWithIniFile(this, String("iskaj_vis.ini"));

}
//---------------------------------------------------------------------------







void __fastcall TFGlav631::FormShow(TObject *Sender)
{
  //
 EAzmtMin1->Text="0";EAzmtMin2->Text="0";EAzmtMax1->Text="0";EAzmtMax2->Text="0";
 EDalnMin->Text="0";EDalnMax->Text="0";
 E_TempObzora->Text="5.3";
 E_UVD_Strob->Text="1.5";
 E_UVD_Min->Text="4";
 E_UVD_Max->Text="50";
 E_RBS_Strob->Text="4";
 E_RBS_Min->Text="4";
 E_RBS_Max->Text="50";


  LoadFormEdits(this,ProgPath+String("\\Edits.cfg"));
  LoadFormCBandRB(this,ProgPath+String("\\CBandRB.cfg"));

  if(CSStr.Length()!=0)
  {
    if(CSStr==String("0"))CBCoolSky->Checked=false;else CBCoolSky->Checked=true;
  }
  if(FileNameStr.Length()!=0)
  {
    EFileName->Text=FileNameStr;
  }

  if(TempOStr.Length()!=0)
  {
    E_TempObzora->Text=TempOStr;
  }
  if(FirstMakeZagruzka)
  {

    MRaschetClick(NULL);
    N2->Enabled=false;
    EFileName->Enabled=false;
    FirstMakeZagruzka=0;
    GlobalCGauge1Isk=CGauge1;

  }else{
    GlobalCGauge1Isk=CGauge1;
  }



}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::FormHide(TObject *Sender)
{

  SaveFormEdits(this,ProgPath+String("\\Edits.cfg"));
  SaveFormCBandRB(this,ProgPath+String("\\CBandRB.cfg"));
  GlobalCloseWindows();

}
//---------------------------------------------------------------------------








void __fastcall TFGlav631::IzmenSortUVD(void)
{
   if(RB_Sort_Bort_UVD->Checked)
     {
       O631_UVD->SetDataOSB(1);
     }else
     if(RB_Sort_Time_UVD->Checked)
     {
       O631_UVD->SetDataOSB(2);
     }else
     if(RB_Only_Kolvo_UVD->Checked)
     {
       O631_UVD->SetDataOSB(3,CSEN_of_OtschUVD->Value);
     }
     O631_UVD->ZapolnitCheckListBox(CLB_UVD_Borta);

}
//--------------------------------------------------
void __fastcall TFGlav631::IzmenSortRBS(void)
{
   if(RB_Sort_Bort_RBS->Checked)
     {
       O631_RBS->SetDataOSB(1);
     }else
     if(RB_Sort_Time_RBS->Checked)
     {
       O631_RBS->SetDataOSB(2);
     }else
     if(RB_Only_Kolvo_RBS->Checked)
     {
       O631_RBS->SetDataOSB(3, CSEN_of_OtschRBS->Value);
     }
  O631_RBS->ZapolnitCheckListBox(CLB_RBS_Borta);
}


void __fastcall TFGlav631::RB_Sort_Bort_UVDClick(TObject *Sender)
{
  IzmenSortUVD();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::RB_Sort_Time_UVDClick(TObject *Sender)
{
    IzmenSortUVD();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::RB_Sort_Bort_RBSClick(TObject *Sender)
{
    IzmenSortRBS();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::RB_Sort_Time_RBSClick(TObject *Sender)
{
     IzmenSortRBS();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::CLB_RBS_BortaClickCheck(TObject *Sender)
{
  int i;
  bool OldIskl; 
  if(CLB_RBS_Borta->Items->Count==0)return;
  for(i=0;i<CLB_RBS_Borta->Items->Count;i++)
  {
     O631_RBS->SetIsklBort(O631_RBS->OSB[i].IndexBort,CLB_RBS_Borta->Checked[i]);
  }
  O631_RBS->OtnosKZone();
 // IzmenSortRBS();
}
//---------------------------------------------------------------------------


void __fastcall TFGlav631::CLB_UVD_BortaClickCheck(TObject *Sender)
{
    int i;
  if(CLB_UVD_Borta->Items->Count==0)return;
  for(i=0;i<CLB_UVD_Borta->Items->Count;i++)
  {
    O631_UVD->SetIsklBort(O631_UVD->OSB[i].IndexBort,CLB_UVD_Borta->Checked[i]);
  }
  O631_UVD->OtnosKZone();
 // IzmenSortUVD();

}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::mnuUVD_Borta_1Click(TObject *Sender)
{
   //
   int Nm;
//��������� ����������
   Nm=CLB_UVD_Borta->ItemIndex;
   if(Nm==-1)return;
   Nm=O631_UVD->OSB[Nm].IndexBort;
   OpenFDOB_UVD(O631_UVD,Nm);

}
//---------------------------------------------------------------------------


void __fastcall TFGlav631::mnuRBS_Borta_1Click(TObject *Sender)
{
   int Nm;
//��������� ����������
   Nm=CLB_RBS_Borta->ItemIndex;
   if(Nm==-1)return;
   Nm=O631_RBS->OSB[Nm].IndexBort;
   OpenFDOB_RBS(O631_RBS,Nm);

}
//---------------------------------------------------------------------------











void __fastcall TFGlav631::BViewIskajVUVDClick(TObject *Sender)
{
  SetNewOption();
  OpenFIVDataUVD(O631_UVD);
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::BViewIskajVRBSClick(TObject *Sender)
{
   SetNewOption();
  OpenFIVDataRBS(O631_RBS);
}
//---------------------------------------------------------------------------










void __fastcall TFGlav631::BViewDataUVDClick(TObject *Sender)
{
  SetNewOption();
  OpenFODataUVD(O631_UVD);
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::BViewDataRBSClick(TObject *Sender)
{
   SetNewOption();
   OpenFODataRBS(O631_RBS);
}
//---------------------------------------------------------------------------



/*��� ����� �����*/
//���������� ������
int __fastcall TFGlav631::SaveOFileName(void)
{
  FILE *fp;
  char Strka[100];
  fp=fopen(OFileName.c_str(),"w");
  if(!fp)return 0;
  if(EAzmtMin1->Text.Trim().Length()==0)EAzmtMin1->Text="0";
  if(EAzmtMax1->Text.Trim().Length()==0)EAzmtMax1->Text="0";
//�������� ��� ��������� ��������
  if(CBAzmt1->Checked)
  {
    fprintf(fp, "1 %s %s\n",EAzmtMin1->Text.c_str(),EAzmtMax1->Text.c_str());
  }else{
    fprintf(fp, "0 %s %s\n",EAzmtMin1->Text.c_str(),EAzmtMax1->Text.c_str());
  }
  if(EAzmtMin2->Text.Trim().Length()==0)EAzmtMin2->Text="0";
  if(EAzmtMax2->Text.Trim().Length()==0)EAzmtMax2->Text="0";
  if(CBAzmt2->Checked)
  {
    fprintf(fp, "1 %s %s\n",EAzmtMin2->Text.c_str(),EAzmtMax2->Text.c_str());
  }else{
    fprintf(fp, "0 %s %s\n",EAzmtMin2->Text.c_str(),EAzmtMax2->Text.c_str());
  }
  if(EDalnMin->Text.Trim().Length()==0)EDalnMin->Text="0";
  if(EDalnMin->Text.Trim().Length()==0)EDalnMax->Text="0";
  if(CB_Daln->Checked)
  {
    fprintf(fp, "1 %s %s\n",EDalnMin->Text.c_str(),EDalnMax->Text.c_str());
  }else{
    fprintf(fp, "0 %s %s\n",EDalnMin->Text.c_str(),EDalnMax->Text.c_str());
  }

  if(CB_Vysota->Checked)
  {
    fprintf(fp, "1 %d %d\n",CSEMinV->Value, CSEMaxV->Value);
  }else{
    fprintf(fp, "0 %d %d\n",CSEMinV->Value, CSEMaxV->Value);
  }
  fprintf(fp,"%s\n",E_TempObzora->Text.c_str());
  if(CBCoolSky->Checked)fprintf(fp,"1\n"); else fprintf(fp,"0\n");
  fprintf(fp,"%d\n",CSEMinN_of_O->Value);

//�������� ��� ���
  fprintf(fp,"%s %s %s %d\n",E_UVD_Strob->Text.c_str(),E_UVD_Min->Text.c_str(),
                          E_UVD_Max->Text.c_str(), CSEDopVys_UVD->Value);

//�������� ��� RBS
  fprintf(fp,"%s %s %s %d\n",E_RBS_Strob->Text.c_str(),E_RBS_Min->Text.c_str(),
                          E_RBS_Max->Text.c_str(), CSEDopVys_RBS->Value);
  fclose(fp);
  return 1;
}

int __fastcall TFGlav631::LoadOFileName(void)
{
  int Ret;
  FILE *fp;
  int Chslo,Chslo1,Chslo2;
  char Strka1[100],Strka2[100],Strka3[100];
  fp=fopen(OFileName.c_str(),"r");
  if(!fp)return 0;
  Ret=fscanf(fp,"%d%s%s",&Chslo,Strka1,Strka2);
  if(Ret!=3)return (-1);
  CBAzmt1->Checked=(Chslo==1);
  EAzmtMin1->Text=Strka1;
  EAzmtMax1->Text=Strka2;
  Ret=fscanf(fp,"%d%s%s",&Chslo,Strka1,Strka2);
  if(Ret!=3)return (-1);
  CBAzmt2->Checked=(Chslo==1);
  EAzmtMin2->Text=Strka1;
  EAzmtMax2->Text=Strka2;

//������� ��� ���������
  Ret=fscanf(fp,"%d%s%s",&Chslo,Strka1,Strka2);
  if(Ret!=3)return (-1);
  CB_Daln->Checked=(Chslo==1);
  EDalnMin->Text=Strka1;
  EDalnMax->Text=Strka2;

//������� ��� ������
  Ret=fscanf(fp,"%d%d%d",&Chslo,&Chslo1,&Chslo2);
  if(Ret!=3)return (-1);
  CB_Vysota->Checked=(Chslo==1);
  CSEMinV->Value=Chslo1;
  CSEMaxV->Value=Chslo2;

//������� ���� ������
  Ret=fscanf(fp,"%s",Strka1);
  if(Ret!=1)return(-1);
  E_TempObzora->Text=Strka1;
  Ret=fscanf(fp,"%d",&Chslo);
  if(Ret!=1)return (-1);
  CBCoolSky->Checked=(Chslo==1);
  Ret=fscanf(fp,"%d",&Chslo);
  if(Ret!=1)return (-1);
  CSEMinN_of_O->Value=Chslo;

//������� ������ ���
  Ret=fscanf(fp,"%s%s%s%d",Strka1,Strka2,Strka3,&Chslo);
  if(Ret!=4)return (-1);
  E_UVD_Strob->Text=Strka1;
  E_UVD_Min->Text=Strka2;
  E_UVD_Max->Text=Strka3;
  CSEDopVys_UVD->Value=Chslo;

//������� ������ RBS
  Ret=fscanf(fp,"%s%s%s%d",Strka1,Strka2,Strka3,&Chslo);
  if(Ret!=4)return (-1);
  E_RBS_Strob->Text=Strka1;
  E_RBS_Min->Text=Strka2;
  E_RBS_Max->Text=Strka3;
  CSEDopVys_RBS->Value=Chslo;


  fclose(fp);
  return 1;
}
//----------------------------
void __fastcall TFGlav631::GetDopFileName(void)
{
  AnsiString FileNameBezRashirenia="";
  int i,Dlina=FileName.Length();
  for(i=Dlina;i>=1;i--)
  {
    if(FileName[i]=='.')
    {
      FileNameBezRashirenia=FileName.SubString(1,i-1);
      break;
    }
    if(FileName[i]=='\\')
    {
       FileNameBezRashirenia=FileName;
       break;
    }
  }
  if(FileNameBezRashirenia.Length()==0)
  {
    FileNameBezRashirenia=FileName;
  }
  OFileName=FileNameBezRashirenia+String(".opt");
  UVDOFileName=FileNameBezRashirenia+String(".uo");
  RBSOFileName=FileNameBezRashirenia+String(".ro");

}
//--------------
void __fastcall TFGlav631::MRaschetClick(TObject *Sender)
{
   bool RetB;
   FILE *fp;

   double A1Min,A1Max,A2Min, A2Max,DMin,DMax;
   double TO,Min_R_UVD,Max_R_UVD,Min_R_RBS,Max_R_RBS,Strob_UVD,Strob_RBS;
   long Ch1,Ch2,Indx1; //��� �������� �����
   IsMoveUVD=false;
   IsMoveRBS=false;
   GlobalCGauge1Isk->Visible=true;

   GlobalCloseWindows();

   sscanf(EAzmtMin1->Text.c_str(),"%lf",&A1Min);
   sscanf(EAzmtMax1->Text.c_str(),"%lf",&A1Max);
   sscanf(EAzmtMin2->Text.c_str(),"%lf",&A2Min);
   sscanf(EAzmtMax2->Text.c_str(),"%lf",&A2Max);
   sscanf(EDalnMin->Text.c_str(),"%lf",&DMin);
   sscanf(EDalnMax->Text.c_str(),"%lf",&DMax);
   sscanf(E_TempObzora->Text.c_str(),"%lf",&TO);
   sscanf(E_UVD_Strob->Text.c_str(), "%lf",&Strob_UVD);
   sscanf(E_RBS_Strob->Text.c_str(), "%lf",&Strob_RBS);
   sscanf(E_UVD_Min->Text.c_str(), "%lf", &Min_R_UVD);
   sscanf(E_RBS_Min->Text.c_str(), "%lf", &Min_R_RBS);
   sscanf(E_UVD_Max->Text.c_str(), "%lf", &Max_R_UVD);
   sscanf(E_RBS_Max->Text.c_str(), "%lf", &Max_R_RBS);





   if(FileExists(EFileName->Text))
   {
     FileName=EFileName->Text;
     GetDopFileName();
     fp=MyFOpenR(FileName);
     if(!fp)return;
     O631_UVD->TempObzora=TO/3600.0;
     O631_RBS->TempObzora=TO/3600.0;
     O631_UVD->Strob=Strob_UVD;
     O631_RBS->Strob=Strob_RBS;
     O631_UVD->MinDD=Min_R_UVD;
     O631_RBS->MinDD=Min_R_RBS;
     O631_UVD->MaxDD=Max_R_UVD;
     O631_RBS->MaxDD=Max_R_RBS;
     O631_UVD->MaxPerepadVysoty=CSEDopVys_UVD->Value;
     O631_RBS->MaxPerepadVysoty=CSEDopVys_RBS->Value;
     O631_UVD->MinBN_of_Otsch=CSEMinN_of_O->Value;
     O631_RBS->MinBN_of_Otsch=CSEMinN_of_O->Value;
  //������� ������
     LoadUVDFromFile(fp,
                     CSEMinV->Value,CSEMaxV->Value,CB_Vysota->Checked,
                     A1Min, A1Max, CBAzmt1->Checked,
                     A2Min, A2Max, CBAzmt2->Checked,
                     DMin,  DMax,  CB_Daln->Checked,
                     O631_UVD,O631_RBS,
                     CBCoolSky->Checked);


     if(N_of_NomersBorts>0)
     {
     //  MessageBox(NULL,"1","2",MB_OK);
       O631_UVD->SdelatVseBortaIsklKromeMasiva(NomersBorts,N_of_NomersBorts);
       O631_RBS->SdelatVseBortaIsklKromeMasiva(NomersBorts,N_of_NomersBorts);
     }

     O631_UVD->OtnosKZone();
     O631_RBS->OtnosKZone();
     if(RB_Sort_Bort_UVD->Checked)
     {
       O631_UVD->SetDataOSB(1);
     }else
     if(RB_Sort_Time_UVD->Checked)
     {
       O631_UVD->SetDataOSB(2);
     }else
     if(RB_Only_Kolvo_UVD->Checked)
     {
       O631_UVD->SetDataOSB(3,CSEN_of_OtschUVD->Value);
     }
     O631_UVD->ZapolnitCheckListBox(CLB_UVD_Borta);

     if(RB_Sort_Bort_RBS->Checked)
     {
       O631_RBS->SetDataOSB(1);
     }else
     if(RB_Sort_Time_RBS->Checked)
     {
       O631_RBS->SetDataOSB(2);
     }else
     if(RB_Only_Kolvo_RBS->Checked)
     {
       O631_RBS->SetDataOSB(3,CSEN_of_OtschRBS->Value);
     }
     O631_RBS->ZapolnitCheckListBox(CLB_RBS_Borta);
     O631_UVD->ZapolnMIV();
     O631_RBS->ZapolnMIV();
     fclose(fp);
   }


   GlobalCGauge1Isk=CGauge1;
   CGauge1->Visible=false;
   CGauge1->Progress=0;
   CGauge1->MinValue=0;

   Indx1=O631_UVD->GlobFirstOtschet(false,false,false,false,false);
   if(Indx1>=0)
   {
     Ch1=(long)(O631_UVD->OO[Indx1].Time);
     Indx1=O631_UVD->GlobLastOtschet(false,false,false,false,false);
     Ch2=(long)(O631_UVD->OO[Indx1].Time);
     CGauge1->MaxValue=Ch2-Ch1;
   }else{
     CGauge1->MaxValue=0;
   }

   Indx1=O631_RBS->GlobFirstOtschet(false,false,false,false,false);
   if(Indx1>=0)
   {
     Ch1=(long)(O631_RBS->OO[Indx1].Time);
     Indx1=O631_RBS->GlobLastOtschet(false,false,false,false,false);
     Ch2=(long)(O631_RBS->OO[Indx1].Time);
     CGauge1->MaxValue+=Ch2-Ch1;
   }

}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::N2Click(TObject *Sender)
{
   bool RetB;
   RetB=OpenDialog1->Execute();
   if(RetB)
   {
     EFileName->Text=OpenDialog1->FileName;
   }
}
//---------------------------------------------------------------------------



void __fastcall TFGlav631::N7Click(TObject *Sender)
{
  bool RetB;
  int RetI;
   double A1Min,A1Max,A2Min, A2Max,DMin,DMax;
   double TO,Min_R_UVD,Max_R_UVD,Min_R_RBS,Max_R_RBS,Strob_UVD,Strob_RBS;

  OpenDialog2->Title="��������� ���������";
  OpenDialog2->Filter="����� ����������(*.OPT)|*.opt";
  FileName=EFileName->Text;
  GetDopFileName();
  OpenDialog2->FileName=OFileName;
  RetB=OpenDialog2->Execute();
  if(RetB)
  {
    OFileName=OpenDialog2->FileName;
    RetI=LoadOFileName();
    if(RetI!=1)
    {
      MessageBox(NULL,"�� ������� �������� ����������!",
                 "������!",MB_OK);
      return;
    }
    sscanf(EAzmtMin1->Text.c_str(),"%lf",&A1Min);
    sscanf(EAzmtMax1->Text.c_str(),"%lf",&A1Max);
    sscanf(EAzmtMin2->Text.c_str(),"%lf",&A2Min);
    sscanf(EAzmtMax2->Text.c_str(),"%lf",&A2Max);
    sscanf(EDalnMin->Text.c_str(),"%lf",&DMin);
    sscanf(EDalnMax->Text.c_str(),"%lf",&DMax);
    sscanf(E_TempObzora->Text.c_str(),"%lf",&TO);
    sscanf(E_UVD_Strob->Text.c_str(), "%lf",&Strob_UVD);
    sscanf(E_RBS_Strob->Text.c_str(), "%lf",&Strob_RBS);
    sscanf(E_UVD_Min->Text.c_str(), "%lf", &Min_R_UVD);
    sscanf(E_RBS_Min->Text.c_str(), "%lf", &Min_R_RBS);
    sscanf(E_UVD_Max->Text.c_str(), "%lf", &Max_R_UVD);
    sscanf(E_RBS_Max->Text.c_str(), "%lf", &Max_R_RBS);
    O631_UVD->TempObzora=TO/3600.0;
    O631_RBS->TempObzora=TO/3600.0;
    O631_UVD->Strob=Strob_UVD; O631_RBS->Strob=Strob_RBS;
    O631_UVD->MinDD=Min_R_UVD; O631_RBS->MinDD=Min_R_RBS;
    O631_UVD->MaxDD=Max_R_UVD; O631_RBS->MaxDD=Max_R_RBS;
    O631_UVD->MaxPerepadVysoty=CSEDopVys_UVD->Value;
    O631_RBS->MaxPerepadVysoty=CSEDopVys_RBS->Value;
    O631_UVD->TempObzora=TO/3600.0;O631_RBS->TempObzora=TO/3600.0;
    O631_UVD->Strob=Strob_UVD; O631_RBS->Strob=Strob_RBS;
    O631_UVD->MinDD=Min_R_UVD; O631_RBS->MinDD=Min_R_RBS;
    O631_UVD->MaxDD=Max_R_UVD; O631_RBS->MaxDD=Max_R_RBS;
    O631_UVD->MaxPerepadVysoty=CSEDopVys_UVD->Value;
    O631_RBS->MaxPerepadVysoty=CSEDopVys_RBS->Value;
    O631_UVD->MinBN_of_Otsch=CSEMinN_of_O->Value;
    O631_RBS->MinBN_of_Otsch=CSEMinN_of_O->Value;
    O631_UVD->A11=A1Min;  O631_RBS->A11=A1Min;
    O631_UVD->A12=A1Max;  O631_RBS->A12=A1Max;
    O631_UVD->IsA1=CBAzmt1->Checked;  O631_RBS->IsA1=CBAzmt1->Checked;
    O631_UVD->A21=A2Min;  O631_RBS->A21=A2Min;
    O631_UVD->A22=A2Max;  O631_RBS->A22=A2Max;
    O631_UVD->IsA2=CBAzmt2->Checked;  O631_RBS->IsA2=CBAzmt2->Checked;
    O631_UVD->MinDlnst=DMin;     O631_RBS->MinDlnst=DMin;
    O631_UVD->MaxDlnst=DMax;     O631_RBS->MaxDlnst=DMax;
    O631_UVD->IsDlnst=CB_Daln->Checked; O631_RBS->IsDlnst=CB_Daln->Checked;
    O631_UVD->Vysota1=CSEMinV->Value;   O631_RBS->Vysota1=CSEMinV->Value;
    O631_UVD->Vysota2=CSEMaxV->Value;   O631_RBS->Vysota2=CSEMaxV->Value;
    O631_UVD->IsVysota=CB_Vysota->Checked;O631_RBS->IsVysota=CB_Vysota->Checked;
    O631_UVD->OtnosKZone(); O631_UVD->ZapolnMIV();
    O631_RBS->OtnosKZone(); O631_RBS->ZapolnMIV();
  }




}
//---------------------------------------------------------------------------


void __fastcall TFGlav631::N9Click(TObject *Sender)
{
//
  bool RetB;
  int RetI;
  SaveDialog2->Title="��������� ���������";
  SaveDialog2->Filter="����� ����������(*.OPT)|*.opt";
  FileName=EFileName->Text;
  GetDopFileName();
  SaveDialog2->FileName=OFileName;
  RetB=SaveDialog2->Execute();
  if(RetB)
  {
    OFileName=SaveDialog2->FileName;
    RetI=SaveOFileName();
    if(RetI!=1)
    {
      MessageBox(NULL,"�� ������� ���������� ����������!",
                 "������!",MB_OK);
      return;
    }
  }
}
//---------------------------------------------------------------------------


void __fastcall TFGlav631::BDataOtschetUVDClick(TObject *Sender)
{
   int Nm1,Nm;

//��������� ����������
   Nm1=CLB_UVD_Borta->ItemIndex;
   if(Nm1==-1)return;
   Nm=O631_UVD->OSB[Nm1].IndexBort;
   SetNewOption();
   CLB_UVD_Borta->ItemIndex=Nm1;
   OpenFDOB_UVD(O631_UVD,Nm);


}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::BDataOtschetRBSClick(TObject *Sender)
{
   int Nm1,Nm;

//��������� ����������
   Nm1=CLB_RBS_Borta->ItemIndex;
   if(Nm1==-1)return;
   Nm=O631_RBS->OSB[Nm1].IndexBort;
   SetNewOption();
   CLB_RBS_Borta->ItemIndex=Nm1;
   OpenFDOB_RBS(O631_RBS,Nm);
}
//---------------------------------------------------------------------------









void __fastcall TFGlav631::BIzkajRezRBS1Click(TObject *Sender)
{
  int Nom;
  SetNewOption();
  TButton *Btn=(TButton*)Sender;
  Nom=Btn->Tag;

  if(!(PF_RBS[Nom]))PF_RBS[Nom]=new TPreviewFormIskaj(NULL);



  PF_RBS[Nom]->Hide();

  if(O631_RBS->N_of_OO<=0||O631_RBS->N_of_Borts<=0)
  {
    MessageBox(NULL,"������� ������ ���","������!", MB_OK);
    return;
  }
  O631_RBS->AllDanIskaj();
  O631_RBS->VyvodToStringIskaj(PF_RBS[Nom]->RichEdit1->Lines,Nom,2,FileName,
                      FileNamesPlots, N_of_FileNamesPlots);
  PF_RBS[Nom]->HelpFile=HelpFile;
  PF_RBS[Nom]->HelpContext=10109;
  PF_RBS[Nom]->Show();

}
//---------------------------------------------------------------------------


void __fastcall TFGlav631::BIskajNomUVDClick(TObject *Sender)
{
   SetNewOption();
   FIskajNomView->Hide();
   FIskajNomView->Type=1;
   FIskajNomView->Otchs631=O631_UVD;
   FIskajNomView->Show();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::BIskajNomRBSClick(TObject *Sender)
{
   SetNewOption();
   FIskajNomView->Hide();
   FIskajNomView->Type=2;
   FIskajNomView->Otchs631=O631_RBS;
   FIskajNomView->Show();

}
//---------------------------------------------------------------------------







void __fastcall TFGlav631::FormClose(TObject *Sender, TCloseAction &Action)
{
   int i;
   delete O631_UVD;
   delete O631_RBS;
   delete FIskajNomView;
   for(i=0;i<6;i++)
   {
    if(PF_UVD[i])delete (PF_UVD[i]);
    PF_UVD[i]=NULL;
    if(PF_RBS[i])delete (PF_RBS[i]);
    PF_RBS[i]=NULL;

   }



   if(NomersBorts)
   {
      N_of_NomersBorts=0;
      free(NomersBorts);
      NomersBorts=NULL;
   }

   if(N_of_FileNamesPlots)
   {
     N_of_FileNamesPlots=0; delete []FileNamesPlots;FileNamesPlots=NULL;
   }

}
//---------------------------------------------------------------------------





void __fastcall TFGlav631::BIzkajRezUVD1Click(TObject *Sender)
{
  int Nom;
  SetNewOption();
  TButton *Btn=(TButton*)Sender;

  Nom=Btn->Tag;

  if(!PF_UVD[Nom])PF_UVD[Nom]=new TPreviewFormIskaj(NULL);
  PF_UVD[Nom]->Hide();
  if(O631_UVD->N_of_OO<=0||O631_UVD->N_of_Borts<=0)
  {
    MessageBox(NULL,"������� ������ ���","������!", MB_OK);
    return;
  }
  O631_UVD->AllDanIskaj();
  O631_UVD->VyvodToStringIskaj(PF_UVD[Nom]->RichEdit1->Lines,Nom,1,FileName,
                      FileNamesPlots, N_of_FileNamesPlots);
  PF_UVD[Nom]->HelpFile=HelpFile;
  PF_UVD[Nom]->HelpContext=10109;

  PF_UVD[Nom]->Show();


}
//---------------------------------------------------------------------------




void __fastcall TFGlav631::RB_Only_Kolvo_UVDClick(TObject *Sender)
{
    IzmenSortUVD();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::CSEN_of_OtschUVDChange(TObject *Sender)
{
    if(CSEN_of_OtschUVD->Text.Trim().Length()==0)
    {
      CSEN_of_OtschUVD->Text=0;
    }
    IzmenSortUVD();
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::RB_Only_Kolvo_RBSClick(TObject *Sender)
{
     IzmenSortRBS();        
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::CSEN_of_OtschRBSChange(TObject *Sender)
{
    if(CSEN_of_OtschRBS->Text.Trim().Length()==0)
    {
      CSEN_of_OtschRBS->Text=0;
    }
    IzmenSortRBS();

}
//---------------------------------------------------------------------------




void __fastcall TFGlav631::N19Click(TObject *Sender)
{

  if(!FileExists(HelpFile))
  {
    MessageBox(NULL,"���� ������ ��� ������ ������ �� ������������ - ������� ����������� ���������",
           "��������", MB_OK);
    return;
  }
//  if(HelpFile.Trim().Length())
  {
    WinHelp(Handle,HelpFile.c_str(),HELP_CONTEXT, HelpContext);
  }
}
//---------------------------------------------------------------------------
void __stdcall FindIskaj631(double TempO,char *FMn, long CS,char *HelpF,
                            long *NBs, long N_of_NBs, void *CGauge,
                            char **FileNames, long N_of_FileNames
                            )
{

   char Strka[100];
   int i;

   GlobalCGauge1Isk=(TCGauge*)CGauge;

   FGlav631=new TFGlav631(NULL);
   sprintf(Strka,"%.2lf",TempO);
   FGlav631->TempOStr=Strka;
   FGlav631->FileNameStr=String(FMn).Trim();
   FGlav631->CSStr=String((int)CS);
   FGlav631->HelpFile=HelpF;
   FGlav631->HelpContext=4280;
   FGlav631->FirstMakeZagruzka=1;
   FGlav631->N_of_NomersBorts=N_of_NBs;
   if(FGlav631->NomersBorts)
   {
          FGlav631->NomersBorts=(long*)realloc(FGlav631->NomersBorts,
                                sizeof(long)*N_of_NBs);
    }else{
        FGlav631->NomersBorts=(long*)malloc(sizeof(long)*N_of_NBs);
    }
   for(i=0;i<N_of_NBs;i++)
   {
     // MessageBox(NULL,IntToStr((int)NBs[i]).c_str(),"",MB_OK);
      FGlav631->NomersBorts[i]=NBs[i];
   }

   FGlav631->N_of_FileNamesPlots=N_of_FileNames;
   FGlav631->FileNamesPlots=new AnsiString [FGlav631->N_of_FileNamesPlots];
   for(i=0;i<N_of_FileNames;i++)
   {
     FGlav631->FileNamesPlots[i]=String(FileNames[i]);
   }


   FGlav631->ShowModal();
   delete FGlav631;
}















void __fastcall TFGlav631::CLB_UVD_BortaKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==0x41&&Shift.Contains(ssCtrl))
  {
    for(int i=0;i<CLB_UVD_Borta->Items->Count;i++)
    {
      CLB_UVD_Borta->Checked[i]=true;
    }
    CLB_UVD_BortaClickCheck(NULL);
  }
  
}
//---------------------------------------------------------------------------











void __fastcall TFGlav631::BIskajUVDClick(TObject *Sender)
{
  SetNewOption();
  if(!(PF_UVD[0]))PF_UVD[0]=new TPreviewFormIskaj(NULL);
    if(O631_UVD->N_of_OO<=0||O631_UVD->N_of_Borts<=0)
  {
    MessageBox(NULL,"������� ������ ���","������!", MB_OK);
    return;
  }
  O631_UVD->AllDanIskaj();
  O631_UVD->VyvodToStringIskaj0001(PF_UVD[0]->RichEdit1->Lines,1,FileName,
                      FileNamesPlots, N_of_FileNamesPlots);
  PF_UVD[0]->HelpFile=HelpFile;
  PF_UVD[0]->HelpContext=10109;

  PF_UVD[0]->Show();


}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::BIskajRBSClick(TObject *Sender)
{
  SetNewOption();
  if(!(PF_RBS[0]))PF_RBS[0]=new TPreviewFormIskaj(NULL);
    if(O631_RBS->N_of_OO<=0||O631_RBS->N_of_Borts<=0)
  {
    MessageBox(NULL,"������� ������ ���","������!", MB_OK);
    return;
  }
  O631_RBS->AllDanIskaj();
  O631_RBS->VyvodToStringIskaj0001(PF_RBS[0]->RichEdit1->Lines,2,FileName,
                      FileNamesPlots, N_of_FileNamesPlots);
  PF_RBS[0]->HelpFile=HelpFile;
  PF_RBS[0]->HelpContext=10109;

  PF_RBS[0]->Show();

}
//---------------------------------------------------------------------------











void __fastcall TFGlav631::SetNewOption(void)
{
   double A1Min,A1Max,A2Min, A2Max,DMin,DMax;
   double TO,Min_R_UVD,Max_R_UVD,Min_R_RBS,Max_R_RBS,Strob_UVD,Strob_RBS;
   if(!IsModifyed)return;

   IsMoveUVD=false;
   IsMoveRBS=false;

   GlobalCloseWindows();

   sscanf(EAzmtMin1->Text.c_str(),"%lf",&A1Min);
   sscanf(EAzmtMax1->Text.c_str(),"%lf",&A1Max);
   sscanf(EAzmtMin2->Text.c_str(),"%lf",&A2Min);
   sscanf(EAzmtMax2->Text.c_str(),"%lf",&A2Max);
   sscanf(EDalnMin->Text.c_str(),"%lf",&DMin);
   sscanf(EDalnMax->Text.c_str(),"%lf",&DMax);
   sscanf(E_TempObzora->Text.c_str(),"%lf",&TO);
   sscanf(E_UVD_Strob->Text.c_str(), "%lf",&Strob_UVD);
   sscanf(E_RBS_Strob->Text.c_str(), "%lf",&Strob_RBS);
   sscanf(E_UVD_Min->Text.c_str(), "%lf", &Min_R_UVD);
   sscanf(E_RBS_Min->Text.c_str(), "%lf", &Min_R_RBS);
   sscanf(E_UVD_Max->Text.c_str(), "%lf", &Max_R_UVD);
   sscanf(E_RBS_Max->Text.c_str(), "%lf", &Max_R_RBS);
   O631_UVD->TempObzora=TO/3600.0;
   O631_RBS->TempObzora=TO/3600.0;
   O631_UVD->Strob=Strob_UVD; O631_RBS->Strob=Strob_RBS;
   O631_UVD->MinDD=Min_R_UVD; O631_RBS->MinDD=Min_R_RBS;
   O631_UVD->MaxDD=Max_R_UVD; O631_RBS->MaxDD=Max_R_RBS;
   O631_UVD->MaxPerepadVysoty=CSEDopVys_UVD->Value;
   O631_RBS->MaxPerepadVysoty=CSEDopVys_RBS->Value;
   O631_UVD->TempObzora=TO/3600.0;O631_RBS->TempObzora=TO/3600.0;
   O631_UVD->Strob=Strob_UVD; O631_RBS->Strob=Strob_RBS;
   O631_UVD->MinDD=Min_R_UVD; O631_RBS->MinDD=Min_R_RBS;
   O631_UVD->MaxDD=Max_R_UVD; O631_RBS->MaxDD=Max_R_RBS;
   O631_UVD->MaxPerepadVysoty=CSEDopVys_UVD->Value;
   O631_RBS->MaxPerepadVysoty=CSEDopVys_RBS->Value;
   O631_UVD->MinBN_of_Otsch=CSEMinN_of_O->Value;
   O631_RBS->MinBN_of_Otsch=CSEMinN_of_O->Value;
   O631_UVD->A11=A1Min;  O631_RBS->A11=A1Min;
   O631_UVD->A12=A1Max;  O631_RBS->A12=A1Max;
   O631_UVD->IsA1=CBAzmt1->Checked;  O631_RBS->IsA1=CBAzmt1->Checked;
   O631_UVD->A21=A2Min;  O631_RBS->A21=A2Min;
   O631_UVD->A22=A2Max;  O631_RBS->A22=A2Max;
   O631_UVD->IsA2=CBAzmt2->Checked;  O631_RBS->IsA2=CBAzmt2->Checked;
   O631_UVD->MinDlnst=DMin;     O631_RBS->MinDlnst=DMin;
   O631_UVD->MaxDlnst=DMax;     O631_RBS->MaxDlnst=DMax;
   O631_UVD->IsDlnst=CB_Daln->Checked; O631_RBS->IsDlnst=CB_Daln->Checked;
   O631_UVD->Vysota1=CSEMinV->Value;   O631_RBS->Vysota1=CSEMinV->Value;
   O631_UVD->Vysota2=CSEMaxV->Value;   O631_RBS->Vysota2=CSEMaxV->Value;
   O631_UVD->IsVysota=CB_Vysota->Checked;O631_RBS->IsVysota=CB_Vysota->Checked;
   O631_UVD->OtnosKZone(); O631_UVD->ZapolnMIV();
   O631_RBS->OtnosKZone(); O631_RBS->ZapolnMIV();
   O631_UVD->ZapolnitCheckListBox(CLB_UVD_Borta);
   O631_RBS->ZapolnitCheckListBox(CLB_RBS_Borta);
   IsModifyed=false;
}


















void __fastcall TFGlav631::MNewOptionsClick(TObject *Sender)
{

   SetNewOption();
}
//---------------------------------------------------------------------------





void __fastcall TFGlav631::EAzmtMin1Change(TObject *Sender)
{
    IsModifyed=true;
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::EAzmtMax1Change(TObject *Sender)
{
    IsModifyed=true;
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::EAzmtMin2Change(TObject *Sender)
{
  IsModifyed=true;
        
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::EAzmtMax2Change(TObject *Sender)
{
   IsModifyed=true;
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::EDalnMinChange(TObject *Sender)
{
   IsModifyed=true;
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::EDalnMaxChange(TObject *Sender)
{
  IsModifyed=true;
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::CSEMinVChange(TObject *Sender)
{
    IsModifyed=true;

}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::CSEMaxVChange(TObject *Sender)
{
    IsModifyed=true;
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::E_TempObzoraChange(TObject *Sender)
{
    IsModifyed=true;
}
//---------------------------------------------------------------------------

void __fastcall TFGlav631::CLB_RBS_BortaKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
//������ ����� A.
  if(Key==0x41&&Shift.Contains(ssCtrl))
  {
    for(int i=0;i<CLB_RBS_Borta->Items->Count;i++)
    {
      CLB_RBS_Borta->Checked[i]=true;
    }
    CLB_RBS_BortaClickCheck(NULL);
  }

}
//---------------------------------------------------------------------------


void __fastcall TFGlav631::CSEMinN_of_OChange(TObject *Sender)
{
   IsModifyed=true;
}
//---------------------------------------------------------------------------





