//---------------------------------------------------------------------------
#ifndef GlavFFor631H
#define GlavFFor631H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include "CSPIN.h"
#include <Menus.hpp>
#include <Dialogs.hpp>
#include "CGAUGES.h"
//---------------------------------------------------------------------------
class TFGlav631 : public TForm
{
__published:	// IDE-managed Components
	TPageControl *PageControl1;
	TTabSheet *TS_UVD;
	TTabSheet *TS_RBS;
	TCheckListBox *CLB_UVD_Borta;
	TLabel *Label1;
	TPanel *Panel1;
	TCheckListBox *CLB_RBS_Borta;
	TLabel *Label2;
	TGroupBox *GroupBox1;
	TCheckBox *CBAzmt1;
	TEdit *EAzmtMin1;
	TEdit *EAzmtMax1;
	TLabel *Label4;
	TLabel *Label5;
	TGroupBox *GroupBox2;
	TLabel *Label7;
	TLabel *Label8;
	TCheckBox *CBAzmt2;
	TEdit *EAzmtMin2;
	TEdit *EAzmtMax2;
	TGroupBox *GroupBox3;
	TCheckBox *CB_Daln;
	TEdit *EDalnMin;
	TEdit *EDalnMax;
	TLabel *Label10;
	TLabel *Label11;
	TGroupBox *GroupBox4;
	TCheckBox *CB_Vysota;
	TCSpinEdit *CSEMinV;
	TCSpinEdit *CSEMaxV;
	TLabel *Label14;
	TLabel *Label15;
        TPageControl *PageControl2;
        TTabSheet *TS_UVD_Usl;
        TTabSheet *TS_RBS_Usl;
        TLabel *Label16;
        TEdit *E_UVD_Strob;
        TLabel *Label17;
        TLabel *Label18;
        TLabel *Label19;
        TEdit *E_UVD_Min;
        TLabel *Label20;
        TEdit *E_UVD_Max;
        TLabel *Label21;
        TLabel *Label22;
        TEdit *E_RBS_Strob;
        TLabel *Label23;
        TLabel *Label24;
        TEdit *E_RBS_Min;
        TLabel *Label26;
        TEdit *E_RBS_Max;
        TLabel *Label27;
	TMainMenu *MainMenu1;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *N3;
	TPanel *Panel2;
	TStaticText *StaticText1;
	TLabel *Label28;
	TCSpinEdit *CSEDopVys_UVD;
	TLabel *Label29;
	TCSpinEdit *CSEDopVys_RBS;
	TPanel *Panel3;
	TLabel *Label30;
	TEdit *E_TempObzora;
	TGroupBox *GroupBox5;
	TRadioButton *RB_Sort_Bort_UVD;
	TRadioButton *RB_Sort_Time_UVD;
	TGroupBox *GroupBox6;
	TRadioButton *RB_Sort_Bort_RBS;
	TRadioButton *RB_Sort_Time_RBS;
	TOpenDialog *OpenDialog1;
	TPanel *Panel4;
	TCheckBox *CBCoolSky;
	TPopupMenu *mnuUVD_Borta;
	TPopupMenu *mnuRBS_Borta;
	TMenuItem *mnuUVD_Borta_1;
	TMenuItem *mnuRBS_Borta_1;
	TLabel *Label31;
	TCSpinEdit *CSEMinN_of_O;
	TLabel *Label32;
	TLabel *Label33;
	TButton *BViewIskajVUVD;
	TButton *BViewIskajVRBS;
	TPanel *Panel5;
	TLabel *Label34;
	TEdit *EFileName;
	TButton *BViewDataUVD;
	TButton *BViewDataRBS;
	TMenuItem *MRaschet;
        TMenuItem *N4;
        TMenuItem *N6;
        TMenuItem *N7;
        TMenuItem *N8;
        TMenuItem *N9;
        TMenuItem *N10;
        TMenuItem *RBS1;
        TMenuItem *N11;
        TOpenDialog *OpenDialog2;
        TSaveDialog *SaveDialog2;
        TButton *BDataOtschetUVD;
        TButton *BDataOtschetRBS;
        TMenuItem *N13;
        TMenuItem *N14;
        TMenuItem *N15;
        TMenuItem *N16;
	TGroupBox *GroupBox7;
	TButton *BIzkajRezUVD3;
	TButton *BIzkajRezUVD2;
	TButton *BIzkajRezUVD1;
	TGroupBox *GroupBox8;
	TButton *BIzkajRezRBS3;
	TButton *BIzkajRezRBS2;
	TButton *BIzkajRezRBS1;
        TButton *BIskajNomUVD;
        TButton *BIskajNomRBS;
	TMenuItem *N17;
        TButton *BIzkajRezUVD1_2;
        TButton *BIzkajRezRBS1_2;
        TRadioButton *RB_Only_Kolvo_UVD;
        TCSpinEdit *CSEN_of_OtschUVD;
        TRadioButton *RB_Only_Kolvo_RBS;
        TCSpinEdit *CSEN_of_OtschRBS;
	TMenuItem *N19;
	TSaveDialog *SaveDialog1;
        TButton *BIskajUVD;
        TButton *BIskajRBS;
        TCGauge *CGauge1;
        TPanel *PanelCG;
        TMenuItem *MNewOptions;
        TLabel *Label3;

	void __fastcall CBAzmt1Click(TObject *Sender);
	void __fastcall CBAzmt2Click(TObject *Sender);
	void __fastcall CB_DalnClick(TObject *Sender);
	void __fastcall CB_VysotaClick(TObject *Sender);
	void __fastcall FormClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormHide(TObject *Sender);
	void __fastcall RB_Sort_Bort_UVDClick(TObject *Sender);
	void __fastcall RB_Sort_Time_UVDClick(TObject *Sender);
	void __fastcall RB_Sort_Bort_RBSClick(TObject *Sender);
	void __fastcall RB_Sort_Time_RBSClick(TObject *Sender);
	void __fastcall CLB_RBS_BortaClickCheck(TObject *Sender);
	void __fastcall CLB_UVD_BortaClickCheck(TObject *Sender);
	void __fastcall mnuUVD_Borta_1Click(TObject *Sender);
	void __fastcall mnuRBS_Borta_1Click(TObject *Sender);
	void __fastcall BViewIskajVUVDClick(TObject *Sender);
	void __fastcall BViewIskajVRBSClick(TObject *Sender);
        void __fastcall BViewDataUVDClick(TObject *Sender);
        void __fastcall BViewDataRBSClick(TObject *Sender);
        void __fastcall MRaschetClick(TObject *Sender);
        void __fastcall N2Click(TObject *Sender);
        void __fastcall N7Click(TObject *Sender);
        void __fastcall N9Click(TObject *Sender);
        void __fastcall BDataOtschetUVDClick(TObject *Sender);
        void __fastcall BDataOtschetRBSClick(TObject *Sender);

	void __fastcall BIzkajRezRBS1Click(TObject *Sender);
        void __fastcall BIskajNomUVDClick(TObject *Sender);
        void __fastcall BIskajNomRBSClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall BIzkajRezUVD1Click(TObject *Sender);
        void __fastcall RB_Only_Kolvo_UVDClick(TObject *Sender);
        void __fastcall CSEN_of_OtschUVDChange(TObject *Sender);
        void __fastcall RB_Only_Kolvo_RBSClick(TObject *Sender);
        void __fastcall CSEN_of_OtschRBSChange(TObject *Sender);
	void __fastcall N19Click(TObject *Sender);
	void __fastcall CLB_UVD_BortaKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall BIskajUVDClick(TObject *Sender);
        void __fastcall BIskajRBSClick(TObject *Sender);
        void __fastcall MNewOptionsClick(TObject *Sender);
        void __fastcall EAzmtMin1Change(TObject *Sender);
        void __fastcall EAzmtMax1Change(TObject *Sender);
        void __fastcall EAzmtMin2Change(TObject *Sender);
        void __fastcall EAzmtMax2Change(TObject *Sender);
        void __fastcall EDalnMinChange(TObject *Sender);
        void __fastcall EDalnMaxChange(TObject *Sender);
        void __fastcall CSEMinVChange(TObject *Sender);
        void __fastcall CSEMaxVChange(TObject *Sender);
        void __fastcall E_TempObzoraChange(TObject *Sender);
        void __fastcall CLB_RBS_BortaKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall CSEMinN_of_OChange(TObject *Sender);



private:	// User declarations
    AnsiString FileName;
    AnsiString OFileName;
    AnsiString UVDOFileName;
    AnsiString RBSOFileName;
    bool IsModifyed;


    void __fastcall CIzmenaDiapazonov(void);

    void __fastcall IzmenSortUVD(void);
    void __fastcall IzmenSortRBS(void);
    void __fastcall SetParametryUVD_RBS(void);

    int __fastcall SaveOFileName(void);
    int __fastcall LoadOFileName(void);
    void __fastcall GetDopFileName(void);

    void __fastcall SetNewOption(void);

public:		// User declarations
    struct OTSCHETS631 *O631_UVD;
    struct OTSCHETS631 *O631_RBS;

    AnsiString ProgPath;
    AnsiString ReadMe;

 //��� �����
    AnsiString TempOStr;
    AnsiString FileNameStr;
    AnsiString CSStr;
    int FirstMakeZagruzka;
    long *NomersBorts;
    int N_of_NomersBorts;

//����� ������, � ������� ��������������� �����
    AnsiString *FileNamesPlots; //����� ������
    int N_of_FileNamesPlots;    //����� ���� ������

	__fastcall TFGlav631(TComponent* Owner);

};
//---------------------------------------------------------------------------
extern PACKAGE TFGlav631 *FGlav631;
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport)
void __stdcall FindIskaj631(double TempO,char *FMn, long CS,char *HelpF,
                            long *NBs, long N_of_NBs, void *CGauge,
                            char **FileNames, long N_of_FileNames);
#endif
