//---------------------------------------------------------------------------
#ifndef IskajVysotaDataH
#define IskajVysotaDataH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TFIskajVysotaData : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TStringGrid *StringGrid1;
	TComboBox *CBVidyOtschetov;
	TLabel *Label1;
	TPopupMenu *PopupMenu1;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *N3;
	TMenuItem *MVysErr;
	TMainMenu *MainMenu1;
	TMenuItem *N4;
	TMenuItem *N5;
	TMenuItem *N6;
	TMenuItem *MVysErr2;
	TMenuItem *N8;
	TCheckBox *CBVyvodOnlyOption;
	TCheckBox *CBNotVyvodEsliMalo;
	TMenuItem *N7;
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall CBVidyOtschetovChange(TObject *Sender);
	void __fastcall N1Click(TObject *Sender);
	void __fastcall N2Click(TObject *Sender);
	void __fastcall PopupMenu1Popup(TObject *Sender);
	void __fastcall MVysErrClick(TObject *Sender);
	void __fastcall N4Click(TObject *Sender);
	void __fastcall MVysErr2Click(TObject *Sender);
	void __fastcall CBVyvodOnlyOptionClick(TObject *Sender);
	void __fastcall CBNotVyvodEsliMaloClick(TObject *Sender);
	void __fastcall N7Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFIskajVysotaData(TComponent* Owner);
    void *Otschs631;
    int Type;
    int Zona;

    //���������� ���������� ������ �������
    void __fastcall VisualizeDataOfBort(void);

  //�������� ��� ���� ��������� �� ������
    int __fastcall ObnovIskajVys(void);
};
//---------------------------------------------------------------------------
extern PACKAGE TFIskajVysotaData *FIskajVysotaData;
//---------------------------------------------------------------------------
#endif
