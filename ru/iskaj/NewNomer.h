//---------------------------------------------------------------------------
#ifndef NewNomerH
#define NewNomerH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------


class TFNewNomer : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label1;
	TLabel *LOldNomer;
	TComboBox *ComboBox1;
	TLabel *Label2;
	TButton *Button1;
	TButton *Button2;
	void __fastcall ComboBox1KeyPress(TObject *Sender, char &Key);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFNewNomer(TComponent* Owner);
    int __fastcall Vyzov(void);

    ONE_S_BORT_631 *OSB631;
    int N_of_OSB631;

//������� ����������
    void *Otschs631;  //�������
    int KodOO;        //��� �������, ������� ���������� ��������


//�������� ����������
    int Status;      //������ ���������� 0 - ������, 1 - �������, 2 - ���������,
                     //������ ��� ��� ���� ������ � ������ ������ ��� ��������

    int IndexBort;     //��� ����� � �������� ��� ������� ������            

};
//---------------------------------------------------------------------------
extern PACKAGE TFNewNomer *FNewNomer;
//---------------------------------------------------------------------------
#endif
