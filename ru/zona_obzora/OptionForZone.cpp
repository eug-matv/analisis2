//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "OptionForZone.h"
#include "work_ini_form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFOption *FOption;
//---------------------------------------------------------------------------
__fastcall TFOption::TFOption(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFOption::FormCreate(TObject *Sender)
{
  IsWasVvod=false;
  iffWorkWithIniFile(this, String("zona_obz_vis.ini"));
}
//---------------------------------------------------------------------------
void __fastcall TFOption::Button1Click(TObject *Sender)
{
 IsWasVvod=true;
 Close();
}
//---------------------------------------------------------------------------
void __fastcall TFOption::Button2Click(TObject *Sender)
{
 Close();
}
//---------------------------------------------------------------------------
 