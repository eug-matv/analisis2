//---------------------------------------------------------------------------
#ifndef Unit6H
#define Unit6H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>

//---------------------------------------------------------------------------
class TFDataSOtch : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TLabel *Label1;
        TEdit *Edit1;
        TButton *Button1;
        TStringGrid *StringGrid1;
        TGroupBox *GroupBox1;
        TLabel *Label4;
        TLabel *Label5;
        TEdit *Edit2;
        TEdit *Edit3;
        TButton *Button2;
        TCheckBox *CheckBox1;
        TLabel *LVydelStrok;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *LObnStrok;
        TTimer *Timer1;
        TMainMenu *MainMenu1;
        TMenuItem *N1;
	TMenuItem *N2;
	TSaveDialog *SaveDialog1;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormHide(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall N1Click(TObject *Sender);
	void __fastcall N2Click(TObject *Sender);
private:	// User declarations
       struct OTCHET* VnOtch;
       int N_of_VnOtch;
       double SredSkorost,SredDSkorost;
       double Time1, Time2;
public:		// User declarations
       double SredSkorostObzor;
       bool VisualBort;

        __fastcall TFDataSOtch(TComponent* Owner);
        int __fastcall LoadOtchet(struct OTCHET* Otch,
                                  int N_of_Otch,
                                  int VS,        //�������� ������ �� ��� �� ���������
                                  double dfMaxDalnostOfExist);
};
//---------------------------------------------------------------------------
extern PACKAGE TFDataSOtch *FDataSOtch;
extern PACKAGE TFDataSOtch *FDataSOtchObsh;

int SchitatChislaOtschetovUnit6(
                                 struct OTCHET* Otch,//������ ��������
                                 int N_of_Otch, //����� ��������
                                 int I_O,   //������ ��������
                                 double dfMaxDalnostOfExistO,  //����������� ��������� ������������ ������

/*������������ ��������*/
                                 int *N_of_VO,  //����� ���� �������� � ����
                                 int *N_of_OO, //����� ������������ ��������
                                 double *P,  //����������� �����������
                                 bool Prolet //����������� �����
                               );

//---------------------------------------------------------------------------
#endif
