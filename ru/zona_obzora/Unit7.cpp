//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit7.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFSpisokBortov *FSpisokBortov;
//---------------------------------------------------------------------------
__fastcall TFSpisokBortov::TFSpisokBortov(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
int __fastcall TFSpisokBortov::VybratBort(void)
{
  RetIndex=-1;
  ShowModal();
  return RetIndex;
}

void __fastcall TFSpisokBortov::Button1Click(TObject *Sender)
{
   RetIndex=ListBox1->ItemIndex;
   if(RetIndex<0)return;
   Close();
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokBortov::N1Click(TObject *Sender)
{
  if(!FileExists(HelpFile))
  {
    MessageBox(NULL,"���� ������ ��� ������ ������ �� ������������ - ������� ����������� ���������",
           "��������", MB_OK);
    return;
  }

   WinHelp(Handle,HelpFile.c_str(),HELP_CONTEXT, HelpContext);
}
//---------------------------------------------------------------------------


void __fastcall TFSpisokBortov::FormCreate(TObject *Sender)
{
  iffWorkWithIniFile(this, String("zona_obz_vis.ini"));
}
//---------------------------------------------------------------------------

