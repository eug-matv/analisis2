object FMaxDalnostGOST: TFMaxDalnostGOST
  Left = 345
  Top = 201
  Width = 454
  Height = 306
  BorderIcons = []
  Caption = 'The required maximum range'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 256
    Top = 72
    Width = 16
    Height = 13
    Caption = 'KM'
  end
  object CSpinEdit1: TCSpinEdit
    Left = 128
    Top = 64
    Width = 121
    Height = 22
    Increment = 10
    MaxValue = 410
    MinValue = 10
    TabOrder = 0
    Value = 350
  end
  object BInput: TButton
    Left = 144
    Top = 136
    Width = 97
    Height = 33
    Caption = 'Input'
    TabOrder = 1
    OnClick = BInputClick
  end
end
