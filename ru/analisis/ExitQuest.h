/*���� "����� �� ���������". ������ ���� ������ ������� � ������ �� ���������*/
//---------------------------------------------------------------------------
#ifndef ExitQuestH
#define ExitQuestH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFExitAsk : public TForm
{
__published:	// IDE-managed Components
        TButton *Button1;
        TButton *Button2;
        TButton *Button3;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall Button3Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TFExitAsk(TComponent* Owner);
        int ExitStatus;  //1 ����� �� �� ��������� ��������� ���������
                         //2 ����� � ��������� ��������� ���������
                         //0 - �� ��������
};
//---------------------------------------------------------------------------
extern PACKAGE TFExitAsk *FExitAsk;
//---------------------------------------------------------------------------
#endif
