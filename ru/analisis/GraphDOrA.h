//---------------------------------------------------------------------------
#ifndef GraphDOrAH
#define GraphDOrAH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TFGraphDOrA : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TMenuItem *N1;
        TChart *Chart1;
        TPointSeries *Series1;
        void __fastcall N1Click(TObject *Sender);
private:	// User declaration`s
public:		// User declarations
        struct DANNYE_TOCHNOST *DT1;
        int N_of_DT1;

        __fastcall TFGraphDOrA(TComponent* Owner);
        void __stdcall MakeGraphicAzmt(void);  //��������� ������
        void __stdcall MakeGraphicDlnst(void);  //��������� ������
        void __stdcall ShowGraphicAzmt(struct DANNYE_TOCHNOST *DT,
                                       int N_of_DT);

        void __stdcall ShowGraphicDlnst(struct DANNYE_TOCHNOST *DT,
                                       int N_of_DT);


};
//---------------------------------------------------------------------------
extern PACKAGE TFGraphDOrA *FGraphDOrA_Daln;
extern PACKAGE TFGraphDOrA *FGraphDOrA_Azmt;

//---------------------------------------------------------------------------
#endif
