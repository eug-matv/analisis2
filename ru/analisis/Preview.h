//----------------------------------------------------------------------------
#ifndef PreviewH
#define PreviewH
//----------------------------------------------------------------------------
#include <vcl\ComCtrls.hpp>
#include <vcl\ExtCtrls.hpp>
#include <vcl\Buttons.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Dialogs.hpp>
#include <vcl\Menus.hpp>
#include <vcl\Controls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\Graphics.hpp>
#include <vcl\Classes.hpp>
#include <vcl\Windows.hpp>
#include <vcl\System.hpp>
#include <ActnList.hpp>
#include <ImgList.hpp>
#include <StdActns.hpp>
#include <ToolWin.hpp>
//----------------------------------------------------------------------------
class TPreviewForm : public TForm
{
__published:
	TSaveDialog *SaveDialog;
        TToolBar *ToolBar1;
        TToolButton *ToolButton2;
        TToolButton *ToolButton4;
        TToolButton *ToolButton5;
        TToolButton *ToolButton6;
        TToolButton *ToolButton8;
        TActionList *ActionList1;
        TAction *FileNew1;
        TAction *FileOpen1;
        TAction *FileSave1;
        TAction *FileSaveAs1;
        TAction *FileSend1;
        TAction *FileExit1;
        TEditCut *EditCut1;
        TEditCopy *EditCopy1;
        TEditPaste *EditPaste1;
        TAction *HelpAbout1;
        TStatusBar *StatusBar;
        TImageList *ImageList1;
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *FileSaveAsItem;
        TMenuItem *N1;
        TMenuItem *FileExitItem;
        TMenuItem *CutItem;
        TMenuItem *CopyItem;
        TMenuItem *PasteItem;
        TMenuItem *Help1;
        TFontDialog *FontDialog;
        TMenuItem *N2;
        TMenuItem *FontItem;
        TMenuItem *N3;
        TMenuItem *N4;
        TRichEdit *RichEdit1;
        TPopupMenu *VsplyvMenu1;
        TMenuItem *MVCut;
        TMenuItem *MVCopy;
        TMenuItem *MVPaste;
        TMenuItem *N5;
        TMenuItem *MVFontItem;
        TMenuItem *MDelete;
        TMenuItem *MSelectAll;
        TMenuItem *MVDelete;
        TMenuItem *MVSelectAll;
	void __fastcall FileSaveAs1Execute(TObject *Sender);
	void __fastcall FileExit1Execute(TObject *Sender);
        void __fastcall FontItemClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormHide(TObject *Sender);
        void __fastcall N4Click(TObject *Sender);
        void __fastcall MDeleteClick(TObject *Sender);
        void __fastcall MSelectAllClick(TObject *Sender);
        void __fastcall Help1Click(TObject *Sender);
private:
	AnsiString FFileName;
public:
	virtual __fastcall TPreviewForm(TComponent *Owner);
};
//----------------------------------------------------------------------------
///extern TPreviewForm *PF_UVD[];
///extern TPreviewForm *PF_RBS[];

//----------------------------------------------------------------------------
#endif    
