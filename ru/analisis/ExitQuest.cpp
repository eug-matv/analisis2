/*��. ���� ExitQuest.h*/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "ExitQuest.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFExitAsk *FExitAsk;
//---------------------------------------------------------------------------
__fastcall TFExitAsk::TFExitAsk(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFExitAsk::FormShow(TObject *Sender)
{
   ExitStatus=0;
   ActiveControl=Button1;        
}
//---------------------------------------------------------------------------
void __fastcall TFExitAsk::Button1Click(TObject *Sender)
{
  ExitStatus=1;
  Close();      
}
//---------------------------------------------------------------------------
void __fastcall TFExitAsk::Button2Click(TObject *Sender)
{
  ExitStatus=2;
  Close();

}
//---------------------------------------------------------------------------
void __fastcall TFExitAsk::Button3Click(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------


void __fastcall TFExitAsk::FormCreate(TObject *Sender)
{
    //iffWorkWithIniFile(this, String("analis_lan.ini"));        
}
//---------------------------------------------------------------------------

