//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "ParamForP.h"
#include "work_ini_form.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TF_Param_P *F_Param_P;
//---------------------------------------------------------------------------
__fastcall TF_Param_P::TF_Param_P(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TF_Param_P::FormShow(TObject *Sender)
{
   char Strka[100];
   Canceled=true;
   sprintf(Strka,"%.02lf",RSA);
   E_RSA->Text=Strka;
   sprintf(Strka,"%.0lf",RSD*1000);
   E_RSD->Text=Strka;


}
//---------------------------------------------------------------------------
void __fastcall TF_Param_P::Button1Click(TObject *Sender)
{
  int Ret;
  double Chislo;
  Ret=sscanf(E_RSA->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    //��������� MessageBox(NULL,"����������� ������� �������� ������ ��������!","������!",MB_OK);
    MessageBox(NULL,"You have incorrectly entered the value of the azimuth strobe","Error!",MB_OK);
    return;
  }
  if(Chislo<=0.01)
  {
   //��������� MessageBox(NULL,"������� ��������� �������� ������ ��������!","������!",MB_OK);
    MessageBox(NULL,"Too low strobe azimuths","Error!",MB_OK);
    return;
  }
  if(Chislo>=20.01)
  {
   //���������  MessageBox(NULL,"�������� ������ �������� ������ ���� ������ 20 ��","������!",MB_OK);
    MessageBox(NULL,"Strobe azimuth value must be less than 20","Error!",MB_OK);
    return;
  }
  RSA=Chislo;

  Ret=sscanf(E_RSD->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    //��������� MessageBox(NULL,"����������� ������� �������� ������ ����������!","������!",MB_OK);
    MessageBox(NULL,"����������� ������� �������� ������ ����������!","Error!",MB_OK);
    return;
  }
  if(Chislo<=1)
  {
    MessageBox(NULL,"������� ��������� �������� ������ ����������!","������!",MB_OK);
    return;
  }
  if(Chislo>=5000)
  {
    MessageBox(NULL,"�������� ���������� �������� ������ ���� ������ 5000 �","������!",MB_OK);
    return;
  }
  RSD=Chislo/1000.0;
  Canceled=false;
  Close();
  
}
//---------------------------------------------------------------------------
void __fastcall TF_Param_P::Button2Click(TObject *Sender)
{
    Close();        
}
//---------------------------------------------------------------------------

void __fastcall TF_Param_P::MHelpClick(TObject *Sender)
{
  if(!FileExists(HelpFile))
  {
    AnsiString Err=String("�� ������� ����� ������: ")+HelpFile;
    MessageBox(Handle,Err.c_str(),"������!",MB_OK);
    return;
  }
  WinHelp(Handle,HelpFile.c_str(),HELP_CONTEXT, HelpContext);
        
}
//---------------------------------------------------------------------------

void __fastcall TF_Param_P::FormCreate(TObject *Sender)
{
 //iffWorkWithIniFile(this, String("analis_lan.ini"));        
}
//---------------------------------------------------------------------------

