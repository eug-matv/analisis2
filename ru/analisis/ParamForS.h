//---------------------------------------------------------------------------
#ifndef ParamForSH
#define ParamForSH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TF_Param_S : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TGroupBox *GroupBox2;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *E_RSA_UVD;
        TEdit *E_RSD_UVD;
        TButton *Button1;
        TButton *Button2;
        TEdit *E_RSD_RBS;
        TEdit *E_RSA_RBS;
        TLabel *Label3;
        TLabel *Label4;
        TMainMenu *MainMenu1;
        TMenuItem *MHelp;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall MHelpClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TF_Param_S(TComponent* Owner);
        bool Canceled;
        double RSA_uvd;
        double RSD_uvd;
        double RSA_rbs;
        double RSD_rbs;

};
//---------------------------------------------------------------------------
extern PACKAGE TF_Param_S *F_Param_S;
//---------------------------------------------------------------------------
#endif
