//---------------------------------------------------------------------------
#ifndef ParamForPH
#define ParamForPH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TF_Param_P : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *E_RSA;
        TLabel *Label2;
        TEdit *E_RSD;
        TLabel *Label3;
        TLabel *Label4;
        TButton *Button1;
        TButton *Button2;
        TMainMenu *MainMenu1;
        TMenuItem *MHelp;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall MHelpClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TF_Param_P(TComponent* Owner);
        bool Canceled;
        double RSA;
        double RSD;

};
//---------------------------------------------------------------------------
extern PACKAGE TF_Param_P *F_Param_P;
//---------------------------------------------------------------------------
#endif
