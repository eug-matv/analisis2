//---------------------------------------------------------------------------
#ifndef FUser_InfoH
#define FUser_InfoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFUserInfo : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *Edit1;
        TLabel *Label2;
        TEdit *Edit2;
        TButton *Button1;
        TButton *Button2;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TFUserInfo(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFUserInfo *FUserInfo;
//---------------------------------------------------------------------------
#endif
