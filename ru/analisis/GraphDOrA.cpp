//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "GlavOkno.h"
#include "GraphDOrA.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFGraphDOrA *FGraphDOrA_Daln=NULL;
TFGraphDOrA *FGraphDOrA_Azmt=NULL;
//---------------------------------------------------------------------------
__fastcall TFGraphDOrA::TFGraphDOrA(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __stdcall TFGraphDOrA::MakeGraphicAzmt(void)  //��������� ������
{
  int i,j;
  int PrevObzor;
  Series1->Clear();
  for(i=0;i<N_of_DT1;i++)
  {
    if(i!=0)
    {
       for(j=PrevObzor+1;j<DT1[i].NomerObzora;j++)
       {
         Series1->AddXY(j,0,"",clBlue);
       }
    }
    Series1->AddXY(DT1[i].NomerObzora,DT1[i].Azmt,"",clTeeColor);
    PrevObzor=DT1[i].NomerObzora;
  }
  Chart1->Title->Text->Clear();
  Chart1->Title->Text->Add("Azimuth");
  Chart1->LeftAxis->Title->Caption="Azimuth, �";
  Caption="Azimuth graph";
  Chart1->LeftAxis->Maximum=360;
  Chart1->LeftAxis->Minimum=0;

}

void __stdcall TFGraphDOrA::MakeGraphicDlnst(void)  //��������� ������
{
  int i,j;
  int PrevObzor;
  Series1->Clear();
  for(i=0;i<N_of_DT1;i++)
  {
    if(i!=0)
    {
       for(j=PrevObzor+1;j<DT1[i].NomerObzora;j++)
       {
         Series1->AddXY(j,0,"",clBlue);
       }
    }
    Series1->AddXY(DT1[i].NomerObzora,DT1[i].Dlnst,"",clTeeColor);
    PrevObzor=DT1[i].NomerObzora;
  }
  Chart1->Title->Text->Clear();
  Chart1->Title->Text->Add("Range");
  Chart1->LeftAxis->Title->Caption="Range, km";
  Caption="Range graph";
  Chart1->LeftAxis->Maximum=400;
  Chart1->LeftAxis->Minimum=0;

}


void __stdcall TFGraphDOrA::ShowGraphicAzmt(struct DANNYE_TOCHNOST *DT,
                                       int N_of_DT)
{
  DT1=DT;
  N_of_DT1=N_of_DT;
  MakeGraphicAzmt();
  Show();
}

void __stdcall TFGraphDOrA::ShowGraphicDlnst(struct DANNYE_TOCHNOST *DT,
                                       int N_of_DT)
{

  DT1=DT;
  N_of_DT1=N_of_DT;
  MakeGraphicDlnst();
  Show();
}



void __fastcall TFGraphDOrA::N1Click(TObject *Sender)
{
  if(!FileExists(HelpFile))
  {
    AnsiString Err=String("Help file: ")+HelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }
  WinHelp(Handle,HelpFile.c_str(),HELP_CONTEXT, HelpContext);
}
//---------------------------------------------------------------------------


