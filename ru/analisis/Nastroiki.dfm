�
 TFNASTROIKI 0�;  TPF0TFNastroiki
FNastroikiLeft� TopoWidth�HeightNCaptionSettingsColor	clBtnFaceConstraints.MaxHeightNConstraints.MaxWidth�Constraints.MinHeightNConstraints.MinWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style Menu	MainMenu1OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnHelpFormHelp	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel12Left�TopWidthPHeightCaption   Диаметр меток  TLabelLabel13LeftXTopWidthHeightCaption	   пикс.  TShapeShape5LeftiTop9WidthHeightBrush.Color@ @   TLabelLabel41LeftTopWidth� HeightCaption5   Допустимые отличия по высоте  TLabelLabel42LeftTopWidthHeightCaption   <  TLabelLabel43Left� TopWidth� HeightCaption   Место испытаний:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel44LeftTop5Width� HeightCaption@   Контрольные ответчики УВД, номера:   TLabelLabel45LeftTopMWidth� HeightCaption=   Контрольные ответчики RBS, номера:   TLabelLabel46LeftTopgWidth� HeightCaptionK   Размер сколь. окна для поиска зоны обзора  TLabelLabel47LeftaTophWidthHeightCaption     	TCheckBoxIsVisNumberLeftTopWidth� HeightCaptionShow tail number (and time)TabOrder OnClickIsVisNumberClick  	TCheckBox	IsVisTimeLeftTopWidth� HeightCaption%Show time of the origin of trajectoryTabOrderOnClickIsVisTimeClick  	TGroupBox	GroupBox1LeftTop7Width� Height2CaptionRecords selectionFont.CharsetDEFAULT_CHARSET
Font.Color� Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel3Left� TopZWidth	HeightCaptiontoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4Left� Top\WidthHeightCaptionkmFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftZTopxWidthHeightCaptionfromFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6Left� TopyWidth	HeightCaptiontoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7LeftXTopYWidthHeightCaptionfromFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel8Left� ToppWidthHeightCaptionoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel9LeftZTop� WidthHeightCaptionfromFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel10Left� Top� Width	HeightCaptiontoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel11Left� Top� WidthHeightCaptionoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel51LeftTopWidthHeightCaption)   Мин.уровень сигнала ПКFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  	TCheckBoxIsCheckNumberLeftTopWidth� HeightCaptionBy tail numbersChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder OnClickIsCheckNumberClick  	TCheckBox
IsCheckVysLeftTop6WidthIHeightCaptionBy altitudeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckVysClick  	TCheckBoxIsCheckTimeLeftTop#WidthyHeightCaptionBy timeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckTimeClick  	TCheckBox
IskluchVysLeftUTop5Width� HeightCaption+   Исключить диапазон выс.Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIskluchVysClick  	TCheckBoxIsCheckDlnstLeft	TopMWidth@HeightCaption	By range Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckDlnstClick  TEditDlnst1LeftpTopPWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextDlnst1OnChangeDlnst1Change  TEditDlnst2Left� TopPWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextDlnst2OnChangeDlnst2Change  	TCheckBoxIsCheckAzmtLeft	TopWidthPHeightCaptionBy azimuth Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckAzmtClick  TEditAzmt11LeftpToprWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextAzmt11OnChangeAzmt11Change  TEditAzmt12Left� ToppWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	TextAzmt12OnChangeAzmt12Change  TEditAzmt21LeftpTop� Width-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
TextAzmt21  TEditAzmt22Left� Top� Width-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextAzmt22  	TCheckBoxKanalPLeft
Top� Width?HeightCaption	P channelChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKanalPClick  	TCheckBoxKanalSLeftNTop� WidthCHeightCaption	S channelChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKanalSClick  	TCheckBoxKanalPSLeft� Top� WidthQHeightCaptionCombined PSChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKanalPSClick  TRadioGroupRBNapravleniaLeftTop� WidthwHeightICaption
 DirectionFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	ItemIndex Items.StringsAll readoutsTo radar stationFrom radar station 
ParentFontTabOrderOnClickRBNapravleniaClick  TRadioGroupRGRejimLeft� Top� WidthlHeightICaptionModes (for S and PS)Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style Items.StringsUVD and RBSUVDRBS 
ParentFontTabOrderOnClickRGRejimClick  	TCheckBoxcbIsCheckSignalPKLeftTopWidth� HeightCaption5   Проверять уровень сигнала ПКColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontTabOrderVisible  TEditeMinSignalPKLeft� TopWidthdHeightTabOrderText1e+1Visible   
TCSpinEditDiametrMetokLeft"Top Width1HeightMaxValue
MinValueTabOrderValue  TButtonB_OKLeftyTop�WidthaHeightCaption   TabOrderOnClick	B_OKClick  TButtonBCancelLeft� Top�WidthaHeightCaption   B<5=0TabOrderOnClickBCancelClick  	TGroupBox	GroupBox6LeftTop,WidthaHeightJCaptionQ   Опции анимации (действительны только для ВК)TabOrder 	TCheckBoxIsAnimNomerLeftTopWidth� HeightCaption.   Отображать номера бортовTabOrder   	TCheckBoxIsAnimVysotaLeftTop$WidthyHeightCaption"   Отображать высоту TabOrder  	TCheckBox
IsAnimTimeLeftTop7WidthyHeightCaption   Отображать времяTabOrder  	TGroupBoxgbSledLeft� TopWidth� Height9AlignalRightCaption   !;54TabOrder TLabelLabel1LeftTop#WidthBHeightCaption   Длина следа  	TCheckBoxIsHvostLeftTopWidthiHeightCaption   Выводить следTabOrder OnClickIsHvostClick  
TCSpinEditDlinaHvostaLeftSTopWidth=HeightMaxValue;MinValueTabOrderValue    TButtonB_PoUmolchaniuLeftTop�WidthaHeightCaption   По умолчаниюTabOrderOnClickB_PoUmolchaniuClick  	TGroupBox	GroupBox7LeftTopWidthaHeight3CaptionE   Параметры для вывода и выборки данныхTabOrder 	TCheckBoxVydelyatSeveraLeftTopWidth� HeightCaption(   Выделять также севераTabOrder   	TCheckBoxIsIstinnyRightDopDataLeft� TopWidth� HeightCaption)   Выводить прав.доп.инфоTabOrder  	TCheckBoxIsLojnyyInfoVyvodLeftTopWidth9HeightCaption_   Выводить информацию о неправильных данных отсчетовTabOrder   	TGroupBoxGBMinMaxAndMaxMinLeftTop� WidthaHeightQCaptionF   Для поиска трасс, близких к радиальнымTabOrder	 TLabelLabel26LeftTop:WidthVHeightCaption   Макс.мин.дальн.  TLabelLabel27Left� Top:WidthVHeightCaption   Мин.макс.дальн.  TEditMaxMinDLefthTop5Width1HeightTabOrder TextMaxMinDOnChangeMaxMinDChange  TEditMinMaxDLeft Top5Width1HeightTabOrderTextMinMaxDOnChangeMinMaxDChange  	TCheckBoxFindRadialTraektLeftTopWidthHeightCaptionL   Выделять траектории близкие к радиальнымTabOrderOnClickFindRadialTraektClick  TRadioButtonFindForP_PSLeftTop$WidthyHeightCaption   Отметки P или PSTabOrderOnClickFindForP_PSClick  TRadioButtonFindForS_PSLeft� Top$Width� HeightCaption   Отметки S или PSChecked	TabOrderTabStop	OnClickFindForS_PSClick   	TGroupBoxGBColorsLeftTophWidth� Height� Caption   &25B0TabOrder
 TLabelLabel28LeftTopWidth+HeightCaption   1;0ABL  TShape
SOblastColLeftiTopWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShape
SKolca1ColLeftiTop!WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel30LeftTop WidthZHeightCaption   Кольца основные  TShape
SKolca2ColLeftiTop1WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel29LeftTop0Width=HeightCaption   Кольца доп.  TLabelLabel31LeftTopAWidthXHeightCaption   Рад.линии 90-180  TShape	SRad90ColLeftiTopBWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShape	SRad30ColLeftiTopSWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel32LeftTopRWidthRHeightCaption   Рад.линии 30,60  TShapeS_P_ColLeft� TopWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel33Left� TopWidth6HeightCaption   Отметки P  TLabelLabel34Left� Top Width6HeightCaption   Отметки S  TShapeS_S_ColLeft� Top!WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShapeS_PS_ColLeft� Top1WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel35Left� Top0Width=HeightCaption   Отметки PS  TLabelLabel36Left� TopAWidth=HeightCaption   Близк. УВД  TShapeS_RS_UVD_ColLeft� TopBWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShapeS_RS_RBS_ColLeft� TopSWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel37Left� TopRWidth;HeightCaption   Близк. RBS  TLabelLabel38Left� TopcWidth,HeightCaption   Близк. P  TShape
S_RS_P_ColLeft� TopdWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel39LeftTopcWidth\HeightCaption   Рамка выделения  TShape	SRamkaColLeftiTopdWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel40LeftHTopsWidth>HeightCaption   Цвет текста  TShape
S_Text_ColLeft� TopwWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour   
TCSpinEditDopDVysLeft�TopWidthIHeightMaxValue�MinValue
TabOrderValue�   TEditMestoIspytaniyLeft�TopWidth� HeightTabOrderTextMestoIspytaniy  TEditKO_UVDLeft�Top0Width� HeightTabOrderTextKO_UVD  TEditKO_RBSLeft�TopHWidth� HeightTabOrderTextKO_RBS  TEditE_RazmerOknaLeftTopbWidthKHeightTabOrderTextE_RazmerOkna  	TGroupBox	GroupBox8LeftPTop�Width&HeightICaption>   Оттенки серого отметок при печатиTabOrder TLabelLabel48LeftTop#WidthHeightCaption     TLabelLabel49LefteTop#WidthHeightCaption     TLabelLabel50Left� Top#Width$HeightCaption   Объед.  
TCSpinEdit	CSEPrintOLeft� TopWidth9HeightMaxValue� MinValueTabOrder   
TCSpinEdit
CSEPrintVKLeftwTopWidth9HeightMaxValue� MinValueTabOrder  
TCSpinEdit
CSEPrintPKLeftTopWidth9HeightMaxValue� MinValueTabOrder   	TCheckBoxCBSetMaxDalnGOSTLeftTop}WidthWHeightCaptionf   Задавать максимальную дальность при поиске зоны обзораTabOrder  	TGroupBoxgbVysotaRLSSeeLeft� Top?Width� Height9Caption5   Высота РЛС относительно моряTabOrderVisible TLabelLabel25LeftvTopWidth	HeightCaption     
TCSpinEdit	VysotaRLSLeft TopWidthaHeight	IncrementdTabOrder    	TMainMenu	MainMenu1Left� Top 	TMenuItemmnuHelpCaptionHelpOnClickmnuHelpClick   TColorDialogColorDialog1Ctl3D	CustomColors.StringsColorA=000000ColorB=FFFFFFColorC=00F000ColorD=008000ColorE=FFFF00ColorF=00FFFFColorG=00FF00ColorH=FF00FFColorI=0000C0ColorJ=0000FFColorK=111111ColorL=AABBCCColorM=DDFF11ColorN=ABCDEFColorO=123456ColorP=888888  Left@Top0   