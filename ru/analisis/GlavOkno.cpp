//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "About.h"
#include "approx.h"


#include "Nastroiki.h"

#include "FUser_Info.h"
#include "GlavOkno.h"
#include "SpisokFilis.h"
#include "RisovatSetku.h"
#include "AllParallel.h"
#include "Preview.h"
#include "ExitQuest.h"
#include "ParamForP.h"
#include "ParamForS.h"
#include "GlavFFor631.h"
#include "VydelZona.h"
#include "tools.h"
#include "max_dalnost_gost.h"

#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CGAUGES"
#pragma link "CSPIN"
#pragma resource "*.dfm"
/*���������� ���������� - ��� ��������� ���������. �������� ����� � ���������*/
AnsiString HelpFileName;

static TPreviewForm *FViewVydelData=NULL;
static TPreviewForm *FViewData=NULL;
static TPreviewForm *FMaxPOtmetok=NULL;
static TPreviewForm *FMaxSredPOtmetok=NULL;
static  TPreviewForm *FDublirVK=NULL;
static TPreviewForm *FRS_P=NULL;
static TPreviewForm *FRS_UVD=NULL;
static TPreviewForm *FRS_RBS=NULL;






extern AnsiString CurPath;    //������� ����


#define MAX_N_OF_ZIKLOV 100
#define crMyKrest 5

int ToZvet; //1 � ��������,
            //2 � ��������



double PromColR,PromColG,PromColB,K_R,K_G,K_B;
int ZiklovCol=0;
double PromColR1,PromColG1,PromColB1,K_R1,K_G1,K_B1;


//---------------------------------------------------------------------------

long D_VYS=57;       //��������� �� ������� ������ ����� ������ ���� ������ ������

TFGlavOkno *FGlavOkno;


//---------------------------------------------------------------------------
__fastcall TFGlavOkno::TFGlavOkno(TComponent* Owner)
        : TForm(Owner)
{
 LeftKm=-400;
 RightKm=400;
 BottomKm=-400;
 TopKm=400;
 XNotPriam=NULL;
 YNotPriam=NULL;
 N_NotPriam=0;
 ProletTrassy=false;
}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::FormCanResize(TObject *Sender, int &NewWidth,
      int &NewHeight, bool &Resize)
{

  if(NewWidth>NewHeight+D_VYS)
  {
    NewWidth-=NewWidth-NewHeight-D_VYS;
  }else{
    NewHeight-=NewHeight+D_VYS-NewWidth;
  }


}
//---------------------------------------------------------------------------




void __fastcall TFGlavOkno::FormCreate(TObject *Sender)
{
//��������� ������ ����, ����� ������� ����������� ����� ����������
 int VysotaEkrana,ShirinaEkrana;
 long Ret;


 int DW=Width-ClientWidth,
     DH=Height-ClientHeight;

 //iffWorkWithIniFile(this, AnsiString("analis_lan.ini"));
 N_of_FilesForDelete=0;




 EVysota1->Text=IntToStr((int)Obz1.Vys1);
 EVysota2->Text=IntToStr((int)Obz1.Vys2);

 Screen->Cursors[crMyKrest]=LoadCursor(HInstance,"MyKrest1");
 Image1->Cursor=crMyKrest;
 Cursor=crMyKrest;

 VidOblasti=0;
 NachatoVydelenie=0;

 D_VYS=LeftPanel->Width+RightPanel->Width+DW-
       VerhPanel->Height-NizPanel->Height-DH;

 HDC DC=GetDC(Handle);
 VysotaEkrana=GetDeviceCaps(DC,VERTRES);
 ShirinaEkrana=GetDeviceCaps(DC,HORZRES);
 ReleaseDC(Handle,DC);

 OnCanResize=NULL;
 Top=0;
 Left=0;
 Width=ShirinaEkrana;
 Height=VysotaEkrana-25;
// Height=GetSystemMetrics(SM_CYMAXIMIZED)-4;

 if(Width>Height+D_VYS)
 {
    Width=Height+D_VYS;
 }else{
    Height=Width-D_VYS;
 }


 PromColR=0xBF; PromColG=0xE4;PromColB=0xFF;
 K_R=(double)(0xC0-0xBF)/MAX_N_OF_ZIKLOV;
 K_G=(double)(0xFF-0xE4)/MAX_N_OF_ZIKLOV;
 K_B=(double)(0xC0-0xFF)/MAX_N_OF_ZIKLOV;
 NizPanel->Color=(TColor)RGB((long)PromColR,(long)PromColG,(long)PromColB);
 RightPanel->Color= NizPanel->Color;

 PromColR1=0x43; PromColG1=0xC5;PromColB1=0xFC;
 K_R1=(double)(0x80-0x43)/MAX_N_OF_ZIKLOV;
 K_G1=(double)(0xFF-0xC5)/MAX_N_OF_ZIKLOV;
 K_B1=(double)(0x80-0xFC)/MAX_N_OF_ZIKLOV;
 VerhPanel->Color=(TColor)RGB((long)PromColR1,(long)PromColG1,(long)PromColB1);
 Panel1->Color= VerhPanel->Color;

 NajataPBPause=false;
 Left=(ShirinaEkrana-Width)/2;

 OnCanResize=FormCanResize;
 GlobalState=0;
 SetGlobalState();

  if(Obz1.IsCheckNumber)
  {
     BSortSpisokBortov->Enabled=true;
     CBSpisokBortov->Enabled=true;
     CBSpisokBortov->Color=clWindow;
  }else{
     BSortSpisokBortov->Enabled=false;
     CBSpisokBortov->Enabled=false;
     CBSpisokBortov->Color=clBtnFace;
  }
  if(Obz1.IsCheckTime)
  {
    DTP1->Enabled=true;    DTP2->Enabled=true;
    CSE_Sutki1->Enabled=true; CSE_Sutki2->Enabled=true;
    DTP1->Color=clWindow;    DTP2->Color=clWindow;
    CSE_Sutki1->Color=clWindow; CSE_Sutki2->Color=clWindow;
  }else{
    DTP1->Enabled=false;    DTP2->Enabled=false;
    CSE_Sutki1->Enabled=false; CSE_Sutki2->Enabled=false;
    DTP1->Color=clBtnFace;    DTP2->Color=clBtnFace;
    CSE_Sutki1->Color=clBtnFace; CSE_Sutki2->Color=clBtnFace;
  }

  if(Obz1.IsCheckVys)
  {
    EVysota1->Enabled=true;    EVysota2->Enabled=true;
    EVysota1->ReadOnly=false;   EVysota2->ReadOnly=false;
    EVysota1->Color=clWhite;    EVysota2->Color=clWhite;
    EVysota1->Font->Color=clBlack;    EVysota2->Font->Color=clBlack;
  }else{
    EVysota1->Enabled=true;    EVysota2->Enabled=true;
    EVysota1->ReadOnly=true;   EVysota2->ReadOnly=true;
    EVysota1->Color=clBtnFace;  EVysota2->Color=clBtnFace;
    EVysota1->Font->Color=clBtnFace;    EVysota2->Font->Color=clBtnFace;

  }

  Shape1->Top=RisovPanel->Height-20;
  Label7->Top=RisovPanel->Height-20;
  Shape2->Top=RisovPanel->Height-20;
  Label8->Top=RisovPanel->Height-20;
  Shape3->Top=RisovPanel->Height-20;
  Shape4->Top=RisovPanel->Height-20;
  Shape5->Top=RisovPanel->Height-20;
  Label9->Top=RisovPanel->Height-20;
  Shape6->Left=RisovPanel->Width-35;
  Shape6->Top=RisovPanel->Height-20;
  Label10->Left=RisovPanel->Width-20;
  Label10->Top=RisovPanel->Height-20;


  DToch=NULL;
  N_of_DToch=0;


  N_of_PrevKm=0;

  HelpFile=HelpFileName;


}
//---------------------------------------------------------------------------









void __fastcall TFGlavOkno::CSE_PoryadokKeyPress(TObject *Sender,
      char &Key)
{
     Key=0;
}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::PBPauseMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(GlobalState!=3)return;
  if(NajataPBPause==false)
  {
    PBPause->BevelOuter=bvLowered;
    PBPause->Caption="Continue";
    NajataPBPause=true;
    TimerAnimate->Enabled=false;
    SetGlobalState();
  }else{
    NajataPBPause=false;
  }


}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::PBPauseMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(GlobalState!=3)return;
  if(NajataPBPause==false)
  {
    PBPause->BevelOuter=bvRaised;
    PBPause->Caption="Pause";
    TimerAnimate->Enabled=true;
    SetGlobalState();
  }

}
//---------------------------------------------------------------------------






/*��������� ������� ������ ������� "����" - "������ ������". ���������
���� ������ ������ � ��������� �����.
*/
void __fastcall TFGlavOkno::MListOfFilesClick(TObject *Sender)
{
extern AnsiString CurPathCfg;  //���� �� ����� �����������

extern AnsiString CurPath;    //������� ����


  char Strka[2001],*RetC;
  int Dlina;
 TAllParallel *AllParallel;
  FILE *fp;
  fp=fopen(CurPathCfg.c_str(),"r");
  if(fp)
  {
//������� ������
    RetC=fgets(Strka,2000,fp);
    if(RetC)
    {
       Dlina=strlen(Strka);
       if(Strka[Dlina-1]=='\n')Strka[Dlina-1]=0;
       CurPath=Strka;

    }
    fclose(fp);
  }

//���������� ������-����� TFSpisokFiles � ������  SpisokFilis
  FSpisokFiles->TempObzoraVSekundah=Obz1.TO*3600.0;
  FSpisokFiles->dfAzmtStrob=Obz1.RSA_RBS1;
  FSpisokFiles->dfDlnstStrob=Obz1.RSD_RBS1;
  FSpisokFiles->is180=Obz1.is180;
  FSpisokFiles->isInPlot=Obz1.IsInPlot;
  FSpisokFiles->HelpFile=HelpFileName;
  FSpisokFiles->HelpContext=4051;
  FSpisokFiles->ShowModal();

  if(!FSpisokFiles->Canceled)
  {
    Obz1.RSA_P1=Obz1.RSA_RBS1=Obz1.RSA_UVD1=FSpisokFiles->dfAzmtStrob;
    Obz1.RSD_P1=Obz1.RSD_RBS1=Obz1.RSD_UVD1=FSpisokFiles->dfDlnstStrob;
    Obz1.is180=FSpisokFiles->is180;
    Obz1.IsInPlot=FSpisokFiles->isInPlot;
    //AllParallel=new TAllParallel(1);
    FGlavOkno->Nachalo1();
    FGlavOkno->LoadFiles();
    FGlavOkno->Konec1();
    GlobalState=1;
    N_of_PrevKm=0;
    SetGlobalState();
//    Obz1.Date_Input=InputBox("���� ����������������","������� ���� ���������������� ������",
//                Obz1.Date_Input);
//    Obz1.Dop_Info=InputBox("���� ����������� ����������","������� ����������� ���������� � ������",
//                Obz1.Dop_Info);
  }
  IzFindTochnost=false;
  if(N_of_DToch)
  {
    N_of_DToch=0;
    free(DToch);DToch=NULL;
  }



}
//---------------------------------------------------------------------------










//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::Image1MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   //
  double KoefX,KoefY;
  long Xv,Yv;

  if(GlobalState==0)return;

  if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }


  //��������, ��� ������ ����� ������ ���� ������ ����
   if(Button==mbLeft)
   {
     NachatoVydelenie=1;
     if(VidOblasti!=1)
     {

       if(N_NotPriam)
       {
          FreeNotPriamOblast();
      //����������
          RisovPanel->Color=Obz1.OblastCol;
          SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
          SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
          SetkaRad30Col=Obz1.Rad30Col;
               Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
          Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
          Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
          Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
          Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;
          VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
          Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
       }
       SVydel->Left=X;
       SVydel->Top=Y;
       SVydel->Width=1;
       SVydel->Height=1;
       SVydel->Visible=true;
       if(GlobalState!=3)
       {
         GlobalState=2;
       }
       SetGlobalState();
     }else{ //���� ����� ������ VidOblasti
       if(N_NotPriam==0) //��� ��� �����, ������ ���� ������� �����
       {
         N_NotPriam=1;
         XNotPriam=(double*)realloc(XNotPriam,sizeof(double)*N_NotPriam);
         YNotPriam=(double*)realloc(YNotPriam,sizeof(double)*N_NotPriam);
         KoefX=((double)(RightKm-LeftKm))/(Image1->Width-1);
         KoefY=((double)(BottomKm-TopKm))/(Image1->Height-1);
         XNotPriam[0]=KoefX*X+LeftKm;
         YNotPriam[0]=KoefY*Y+TopKm;
         Xpnp=-1000000;
         Ypnp=-1000000;
       }else{

    //�������� �����, ����������� ��������� ����� � �����
         Image1->Picture->Bitmap->Canvas->Pen->Mode=pmXor;
         Image1->Picture->Bitmap->Canvas->Pen->Color=(TColor)RGB(255,200,0);
         Image1->Picture->Bitmap->Canvas->Pen->Style=psSolid;
         KoefX=(Image1->Width-1)/((double)(RightKm-LeftKm));
         KoefY=(Image1->Height-1)/((double)(BottomKm-TopKm));
         Xv=Okrugl(KoefX*(XNotPriam[N_NotPriam-1]-LeftKm));
         Yv=Okrugl(KoefY*(YNotPriam[N_NotPriam-1]-TopKm));
         Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
         Image1->Picture->Bitmap->Canvas->LineTo(X,Y);
         Xpnp=X;Ypnp=Y;
       }
     }

   }

   if(Button==mbRight)
   {
     if(VidOblasti==1)
     {
       if(N_NotPriam<=2)
       {
         FreeNotPriamOblast();
         VidOblasti=0;


       }else{
     //���� �������� ������ � ��������� �����
         VidOblasti=2;
         Image1->Picture->Bitmap->Canvas->Pen->Mode=pmXor;
         Image1->Picture->Bitmap->Canvas->Pen->Color=(TColor)RGB(255,200,0);
         Image1->Picture->Bitmap->Canvas->Pen->Style=psSolid;
         KoefX=(Image1->Width-1)/((double)(RightKm-LeftKm));
         KoefY=(Image1->Height-1)/((double)(BottomKm-TopKm));
         Xv=Okrugl(KoefX*(XNotPriam[N_NotPriam-1]-LeftKm));
         Yv=Okrugl(KoefY*(YNotPriam[N_NotPriam-1]-TopKm));
         Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
         Xv=Okrugl(KoefX*(XNotPriam[0]-LeftKm));
         Yv=Okrugl(KoefY*(YNotPriam[0]-TopKm));
         Image1->Picture->Bitmap->Canvas->LineTo(Xv,Yv);
         VyzovPosleVydeleniaOblasti();
         GlobalState=1;
         SetGlobalState();
       }
     }else{
     }
   }


}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::Image1MouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
   long Xv,Yv;
   double KoefX,KoefY;
   double Xd,Yd,Azm,Dln;
   char Strka[500];

   KoefX=((double)(RightKm-LeftKm))/(Image1->Width-1);
   KoefY=((double)(BottomKm-TopKm))/(Image1->Height-1);
   Xd=KoefX*X+LeftKm;
   Yd=KoefY*Y+TopKm;
   Dln=sqrt(Xd*Xd+Yd*Yd);
   if(Xd<0.01&&Yd<0.01&&Xd>-0.01&&Yd>-0.01)
   {
       Azm=0;
   }else{
       Azm=atan2(Xd,Yd)/M_PI*180;
   }
   if(Azm<0)Azm+=360;


   sprintf(Strka,
   "Azimuth=%.02lf gr, range=%.02lf km, X=%.02lf km, Y=%.02lf km",
                Azm,Dln,Xd,Yd);

   StatusBar1->Panels->Items[0]->Text=Strka;

   if(GlobalState==0)return;
   if(NachatoVydelenie==1)
   {
      if(VidOblasti==1)
      {
         Image1->Picture->Bitmap->Canvas->Pen->Mode=pmXor;
         Image1->Picture->Bitmap->Canvas->Pen->Color=(TColor)RGB(255,200,0);
         Image1->Picture->Bitmap->Canvas->Pen->Style=psSolid;
         KoefX=(Image1->Width-1)/((double)(RightKm-LeftKm));
         KoefY=(Image1->Height-1)/((double)(BottomKm-TopKm));
         Xv=Okrugl(KoefX*(XNotPriam[N_NotPriam-1]-LeftKm));
         Yv=Okrugl(KoefY*(YNotPriam[N_NotPriam-1]-TopKm));
         if(Xpnp<-100000)
         {
           Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
           Image1->Picture->Bitmap->Canvas->LineTo(X,Y);
         }else{
           Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
           Image1->Picture->Bitmap->Canvas->LineTo(Xpnp,Ypnp);
           Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
           Image1->Picture->Bitmap->Canvas->LineTo(X,Y);
         }
         Xpnp=X;Ypnp=Y;
      }else{
        if(X<SVydel->Left)
        {
          SVydel->Width=1;
        }
        else
        {
          SVydel->Width=X-SVydel->Left+1;
        }

        if(Y<SVydel->Top)
        {
          SVydel->Height=1;
        }
        else
        {
          SVydel->Height=Y-SVydel->Top+1;
        }
      }
   }

//


}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::Image1MouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   long Xv,Yv;
   double KoefX,KoefY;
   if(GlobalState==0)return;

   if(NachatoVydelenie==1)
   {


     if(VidOblasti==1)
     {
         Image1->Picture->Bitmap->Canvas->Pen->Mode=pmXor;
         Image1->Picture->Bitmap->Canvas->Pen->Color=(TColor)RGB(255,200,0);
         Image1->Picture->Bitmap->Canvas->Pen->Style=psSolid;
         KoefX=(Image1->Width-1)/((double)(RightKm-LeftKm));
         KoefY=(Image1->Height-1)/((double)(BottomKm-TopKm));
         Xv=Okrugl(KoefX*(XNotPriam[N_NotPriam-1]-LeftKm));
         Yv=Okrugl(KoefY*(YNotPriam[N_NotPriam-1]-TopKm));
         if(Xpnp<-100000)
         {
           Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
           Image1->Picture->Bitmap->Canvas->LineTo(X,Y);
         }else{
           Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
           Image1->Picture->Bitmap->Canvas->LineTo(Xpnp,Ypnp);
           Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
           Image1->Picture->Bitmap->Canvas->LineTo(X,Y);
         }
     //������� ����� � ��� ������, ������� ��� ���������
         XNotPriam=(double*)realloc(XNotPriam,sizeof(double)*(N_NotPriam+1));
         YNotPriam=(double*)realloc(YNotPriam,sizeof(double)*(N_NotPriam+1));
         KoefX=((double)(RightKm-LeftKm))/(Image1->Width-1);
         KoefY=((double)(BottomKm-TopKm))/(Image1->Height-1);
         XNotPriam[N_NotPriam]=KoefX*X+LeftKm;
         YNotPriam[N_NotPriam]=KoefY*Y+TopKm;
         N_NotPriam++;
     }else{

       if(X<SVydel->Left)
       {
         SVydel->Width=1;
       }
       else
       {
         SVydel->Width=X-SVydel->Left+1;
       }

       if(Y<SVydel->Top)
       {
         SVydel->Height=1;
       }
       else
       {
         SVydel->Height=Y-SVydel->Top+1;
       }
       if(GlobalState!=3)
       {
         VyzovPosleVydeleniaOblasti();
         GlobalState=1;
         SetGlobalState();
       }
     }
     IzFindTochnost=false;
     if(N_of_DToch)
     {
       N_of_DToch=0;
       free(DToch);DToch=NULL;
     }

     NachatoVydelenie=0;
   }
   SetGlobalState();
}
//---------------------------------------------------------------------------




void __fastcall TFGlavOkno::SVydelMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    double KoefX,KoefY;
    double LeftKm1,RightKm1,TopKm1,BottomKm1;
    if(GlobalState==1&&Button==mbRight&&IzFindTochnost)
    {

       KoefX=((double)(RightKm-LeftKm+1))/Image1->Width;
       KoefY=((double)(TopKm-BottomKm+1))/Image1->Height;

      LeftKm1=(SVydel->Left)*KoefX+LeftKm;
      RightKm1=(SVydel->Left+SVydel->Width-1)*KoefX+LeftKm;
      BottomKm1=TopKm-(SVydel->Top+SVydel->Height-1)*KoefY;
      TopKm1=TopKm-(SVydel->Top)*KoefY;
      FVydelZona->HelpFile=HelpFileName;
      FVydelZona->HelpContext=4272;
      FVydelZona->VydelZonaMake(LeftKm1,BottomKm1,RightKm1,TopKm1,DToch,
                                N_of_DToch,
                                KursPolyota);
      return;
    }
   IzFindTochnost=false;
   if(N_of_DToch)
   {
     N_of_DToch=0;
     free(DToch);DToch=NULL;
   }


   Image1MouseDown(NULL,Button,Shift,X+SVydel->Left,Y+SVydel->Top);

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::SVydelMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
    /*
     IzFindTochnost=false;
     if(N_of_DToch)
     {
       N_of_DToch=0;
       free(DToch);DToch=NULL;
     }
     */
     Image1MouseMove(NULL,Shift,X+SVydel->Left,Y+SVydel->Top);

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::SVydelMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
        if(Button==mbRight)
        {


          return;
        }


        IzFindTochnost=false;
        if(N_of_DToch)
        {
          N_of_DToch=0;
          free(DToch);DToch=NULL;
        }

        Image1MouseUp(NULL,Button,Shift,X+SVydel->Left,Y+SVydel->Top);


}
//---------------------------------------------------------------------------


/*������� - ������ ������������� �������*/
void __fastcall TFGlavOkno::mnuVnutriOblastiClick(TObject *Sender)
{
  double LeftKm1,RightKm1, BottomKm1, TopKm1;
  double LeftKm2,RightKm2, BottomKm2, TopKm2;

  double KoefX,KoefY;

//��������� ���������� �����

//��������� ���������� ����� � ����������
  KoefX=((double)(RightKm-LeftKm+1))/Image1->Width;
  KoefY=((double)(TopKm-BottomKm+1))/Image1->Height;
  LeftKm1=(SVydel->Left)*KoefX+LeftKm;
  RightKm1=(SVydel->Left+SVydel->Width-1)*KoefX+LeftKm;
  BottomKm1=TopKm-(SVydel->Top+SVydel->Height-1)*KoefY;
  TopKm1=TopKm-(SVydel->Top)*KoefY;




//�������� ��� ��� ������

  if(LeftKm1<LeftKm)LeftKm1=LeftKm;
  if(BottomKm1<BottomKm)BottomKm1=BottomKm;
  if(RightKm1>RightKm)RightKm1=RightKm;
  if(TopKm1>TopKm)TopKm1=TopKm;
  if(TopKm1-BottomKm1>RightKm1-LeftKm1)
  {
//���� ���������, ����� �������� ���� ����
     LeftKm2=LeftKm1-(TopKm1-BottomKm1-RightKm1+LeftKm1)/2;
     RightKm2=RightKm1+(TopKm1-BottomKm1-RightKm1+LeftKm1)/2;
     LeftKm1=LeftKm2;
     RightKm1=RightKm2;
     if(RightKm1-LeftKm1+1==TopKm1-BottomKm1)
     {
       LeftKm1--;
     }else if(RightKm1-LeftKm1-1==TopKm1-BottomKm1){
       RightKm1--;
     }


  }else if(TopKm1-BottomKm1<RightKm1-LeftKm1)
  {
    TopKm2=TopKm1+(RightKm1-LeftKm1-TopKm1+BottomKm1)/2;
    BottomKm2=BottomKm1-(RightKm1-LeftKm1-TopKm1+BottomKm1)/2;
    TopKm1=TopKm2;
    BottomKm1=BottomKm2;
    if(TopKm1-BottomKm1+1==RightKm1-LeftKm1)
    {
        BottomKm1--;
    }else if(TopKm1-BottomKm1+1==RightKm1-LeftKm1)
    {
       TopKm1++;
    }
  }

  if(TopKm1-BottomKm1<1||RightKm1-LeftKm1<1)
  {
    MessageBox(NULL,"Too little area can not display!",
                    "Error!",MB_OK);
    return;
  }


  if(N_of_PrevKm==20)
  {
    memmove(PrevLeftKm,PrevLeftKm+1,sizeof(double)*19);
    memmove(PrevTopKm,PrevTopKm+1,sizeof(double)*19);
    memmove(PrevRightKm,PrevRightKm+1,sizeof(double)*19);
    memmove(PrevBottomKm,PrevBottomKm+1,sizeof(double)*19);
    N_of_PrevKm--;
  }
  PrevLeftKm[N_of_PrevKm]=LeftKm;
  PrevTopKm[N_of_PrevKm]=TopKm;
  PrevRightKm[N_of_PrevKm]=RightKm;
  PrevBottomKm[N_of_PrevKm]=BottomKm;
  N_of_PrevKm++;

  if(TopKm1-BottomKm1<5)
  {
     TopKm=Okrugl(TopKm1);BottomKm=Okrugl(BottomKm1);LeftKm=Okrugl(LeftKm1);
     RightKm=Okrugl(RightKm1);

  }else{
     TopKm=TopKm1;BottomKm=BottomKm1;LeftKm=LeftKm1;RightKm=RightKm1;
  }
  RB_50km->Checked=false;
  RB_100km->Checked=false;
  RB_200km->Checked=false;
  RB_400km->Checked=false;

  RB_Uvelichenie->Checked=true;
  RB_Uvelichenie->Visible=true;
  RB_Uvelichenie->SetFocus();


  FormResize(NULL); //������� ��������� ��������� �������� ����, �����
  //������������ �������

}
//---------------------------------------------------------------------------



/*��������� ������� ��������� ������� ���� �����*/
void __fastcall TFGlavOkno::FormResize(TObject *Sender)
{
    if(GlobalState==3)  //���� ����� ������������
    {
      RisovPanel->Color=Obz1.OblastCol;
      SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
      SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
      SetkaRad30Col=Obz1.Rad30Col;
//������� �����
      Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
      Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
      Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
      Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
      Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;

//��������� ����� �������� ���������
      Obz1.VychislenieKoordinatAnim(Image1,LeftKm,BottomKm,RightKm,TopKm);

//������� �����
      VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);

      if(NazadAnim)
      {
         Obz1.VyvodAllDataAnimTekSektorNazad(Image1,LeftKm,BottomKm,RightKm,TopKm);
      }else{
         Obz1.VyvodAllDataAnimTekSektor(Image1,LeftKm,BottomKm,RightKm,TopKm);
      }
      return;
    }


  CommandTimer2=2;  //��������� ������� ��� ����, ����� � ���� ����������� �������
                    //���� ������� ����������� �������� ��� �����������
  CGauge1->Progress=0;
  CGauge1->MaxValue=100;
  CGauge1->Visible=true;
  Timer2->Enabled=true;


}

//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::RB_50kmClick(TObject *Sender)
{
//     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
//     Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
//    FreeNotPriamOblast();

    LeftKm=-50;
    RightKm=50;
    BottomKm=-50;
    TopKm=50;
    RB_Uvelichenie->Visible=false;
    FormResize(NULL);
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::RB_200kmClick(TObject *Sender)
{
//     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
//     Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
//     FreeNotPriamOblast();

    LeftKm=-200;
    RightKm=200;
    BottomKm=-200;
    TopKm=200;
    RB_Uvelichenie->Visible=false;
    FormResize(NULL);

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::RB_100kmClick(TObject *Sender)
{
//     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
//     Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
//     FreeNotPriamOblast();

    LeftKm=-100;
    RightKm=100;
    BottomKm=-100;
    TopKm=100;
    RB_Uvelichenie->Visible=false;
    FormResize(NULL);

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::RB_400kmClick(TObject *Sender)
{
//     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
 //    Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
 //    FreeNotPriamOblast();

    LeftKm=-400;
    RightKm=400;
    BottomKm=-400;
    TopKm=400;
    RB_Uvelichenie->Visible=false;

    FormResize(NULL);

}
//---------------------------------------------------------------------------













void __fastcall TFGlavOkno::CBSpisokBortovEnter(TObject *Sender)
{
   int i;
   int MaxDlina=CBSpisokBortov->Canvas->TextWidth("RBS. Split plots");
   CBSpisokBortov->Width=250;
   CBSpisokBortov->Height=300;
   CBSpisokBortov->Visible=true;

   for(i=0;i<CBSpisokBortov->Items->Count;i++)
   {
     MaxDlina=MAX(CBSpisokBortov->Canvas->TextWidth(CBSpisokBortov->Items->Strings[i]),MaxDlina);
   }
   Button1->Visible=false;
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::CBSpisokBortovExit(TObject *Sender)
{
   CBSpisokBortov->Width=80;
   CBSpisokBortov->Height=17;
   CBSpisokBortov->Visible=false;
   Button1->Visible=true;
}
//---------------------------------------------------------------------------





void __fastcall TFGlavOkno::CBSpisokBortovKeyDown(TObject *Sender,
      WORD &Key, TShiftState Shift)
{
    if(CBSpisokBortov->Height>100)
  {
     CBSpisokBortov->Width=80;
     CBSpisokBortov->Height=17;
     CBSpisokBortov->Visible=false;
     Button1->Visible=true;
  }else{
    int i;
    int MaxDlina=CBSpisokBortov->Canvas->TextWidth("RBS. Split plots");
    CBSpisokBortov->Width=250;
    CBSpisokBortov->Height=300;
    CBSpisokBortov->Visible=true;
     Button1->Visible=false;
    for(i=0;i<CBSpisokBortov->Items->Count;i++)
    {
      MaxDlina=MAX(CBSpisokBortov->Canvas->TextWidth(CBSpisokBortov->Items->Strings[i]),MaxDlina);
    }
    CBSpisokBortov->Width=MaxDlina+54;

  }

}
//---------------------------------------------------------------------------
void __fastcall TFGlavOkno::GBVremyaMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }

}
//---------------------------------------------------------------------------
void __fastcall TFGlavOkno::GroupBox2MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }
        
}
//---------------------------------------------------------------------------
void __fastcall TFGlavOkno::GroupBox3MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }
        
}
//---------------------------------------------------------------------------
void __fastcall TFGlavOkno::VerhPanelMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }
        
}
//---------------------------------------------------------------------------
void __fastcall TFGlavOkno::RightPanelMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }
        
}
//---------------------------------------------------------------------------
void __fastcall TFGlavOkno::StatusBar1MouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }
        
}
//---------------------------------------------------------------------------
void __fastcall TFGlavOkno::NizPanelMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }

}
//---------------------------------------------------------------------------




void __fastcall TFGlavOkno::Button1Click(TObject *Sender)
{
/*�������� ������ ������. ���������� ���� ������ � �������� ������
*/
   int i;
   int MaxDlina=CBSpisokBortov->Canvas->TextWidth("RBS. Split plots");
   CBSpisokBortov->Width=250;
   CBSpisokBortov->Height=300;
   CBSpisokBortov->Visible=true;

   for(i=0;i<CBSpisokBortov->Items->Count;i++)
   {
     if(CBSpisokBortov->Canvas->TextWidth(CBSpisokBortov->Items->Strings[i])>MaxDlina)
     {
         MaxDlina=CBSpisokBortov->Canvas->TextWidth(CBSpisokBortov->Items->Strings[i]);
     }
   }
   CBSpisokBortov->Width=250;
   Button1->Visible=false;
}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::BSortSpisokBortovClick(TObject *Sender)
{
/*������������� ����� ���� ������� ������, ���� ���� �������������
�� ������� ��������� ��� ��������*/
  int i;

 //������� ���������� ������� ����� ������, ��� ������ ������
  Obz1.GetDataAboutSelectedNumber(CBSpisokBortov,BSortSpisokBortov->Tag);



  BSortSpisokBortov->Tag=  (BSortSpisokBortov->Tag+1)%2;



  if(BSortSpisokBortov->Tag)     //���� ���� ������������� �� ���������
  {
    BSortSpisokBortov->Caption="<--";

//����������� � Traekt.cpp - ��������� ������ �������� ������ � ������� �� �������
    Obz1.ZapolnitSpisok(CBSpisokBortov->Items,true);
  }else{
    BSortSpisokBortov->Caption="1...9";

//����������� � Traekt.cpp - ��������� ������ �������� ������ � ������� �� ���������
    Obz1.ZapolnitSpisok(CBSpisokBortov->Items,false);
  }
  for(i=0;i<CBSpisokBortov->Items->Count;i++)
  {
    CBSpisokBortov->Selected[i]=Obz1.GetSelectedInSortedBorts(i);
  }

}
//---------------------------------------------------------------------------
   //��������� ��������� ������... ���������, ��� ������������ :(
void __fastcall TFGlavOkno::LoadFiles(void)
{

     Obz1.LoadData(FSpisokFiles->FileNames,
                    FSpisokFiles->N_of_fps,
                    FSpisokFiles->TempObzoraVSekundah);


}







/*������� ��� ����������, ������� �� ����� ����������. ������ ����*/
void __fastcall TFGlavOkno::Nachalo1(void)
{
/*������ ����������� ���� �����*/
    OnResize=NULL;
  //������� ��� �����������, ��� �� ����
    BSortSpisokBortov->Visible=false;
    CBSpisokBortov->Visible=false;
    Button1->Visible=false;
    B_OK->Visible=false;
    GBMashtab->Visible=false;
    BZonaP->Visible=false;
    BZonaS->Visible=false;
    BTochnost->Visible=false;
    BStart->Visible=false;
    PBPause->Visible=false;
    BEnd->Visible=false;
    Image1->Visible=false;
    FGlavOkno->Menu=NULL;
    DTP1->Enabled=true;

    DTP1->Enabled=true;    DTP2->Enabled=true;
    CSE_Sutki1->Enabled=true; CSE_Sutki2->Enabled=true;
    DTP1->Color=clWindow;    DTP2->Color=clWindow;
    CSE_Sutki1->Color=clWindow; CSE_Sutki2->Color=clWindow;
    CGauge1->Progress=0;
    CGauge1->Visible=true;
    Timer1->Enabled=true;

//    FMesOkno=new TFMesOkno(NULL);
//    FMesOkno->Label1->Caption="���������! ���� ��������!" ;
//    FMesOkno->Show();
    LPanelStatus->Caption="Waiting...";
    PanelStatus->Left=0;
    PanelStatus->Top=0;
    PanelStatus->Width=Image1->Width;
    PanelStatus->Height=Image1->Height;
    PanelStatus->Visible=true;



}







void __fastcall TFGlavOkno::Konec1(void)
{
    int H1,M1,S1,H2,M2,S2;
/**/

    Timer1->Enabled=false;
    CGauge1->Visible=false;

    Timer1Timer(NULL);
    if(Obz1.CLSutkiCur!=0)
    {

       double TmPr1,TmPr2;
       TmPr1=Obz1.CLSutkiFirst*24.0+Obz1.CLChasyFirst+Obz1.CLMinutyFirst/60.0+Obz1.CLSekundyFirst/3600.0;
       TmPr2=Obz1.CLSutkiCur*24.0+Obz1.CLChasyCur+Obz1.CLMinutyCur/60.0+Obz1.CLSekundyCur/3600.0;
       if(TmPr2-TmPr1>23+59/60.0+58.9/3600.0)
       {
         Obz1.IsCheckTime=false;
       }
    }


//��� ��������� ����������
    BSortSpisokBortov->Visible=true;
    Button1->Visible=true;
    B_OK->Visible=true;
    GBMashtab->Visible=true;
    BZonaP->Visible=true;
    BZonaS->Visible=true;
    BTochnost->Visible=true;
    BStart->Visible=true;
    PBPause->Visible=true;
    BEnd->Visible=true;
    Image1->Visible=true;
    FGlavOkno->Menu=MainMenu1;
    DTP1->DateTime=TDateTime(
                           (unsigned short)Obz1.CLChasyFirst,
                           (unsigned short)Obz1.CLMinutyFirst,
                           (unsigned short)Obz1.CLSekundyFirst,
                           (unsigned short)0
                           );
   DTP2->DateTime=TDateTime(
                           (unsigned short)Obz1.CLChasyCur,
                           (unsigned short)Obz1.CLMinutyCur,
                           (unsigned short)Obz1.CLSekundyCur,
                           (unsigned short)0
                           );

    if(Obz1.IsCheckTime)
    {
      DTP1->Enabled=true;    DTP2->Enabled=true;
      CSE_Sutki1->Enabled=true; CSE_Sutki2->Enabled=true;
      DTP1->Color=clWindow;    DTP2->Color=clWindow;
      CSE_Sutki1->Color=clWindow; CSE_Sutki2->Color=clWindow;
    }else{
      DTP1->Enabled=false;    DTP2->Enabled=false;
      CSE_Sutki1->Enabled=false; CSE_Sutki2->Enabled=false;
      DTP1->Color=clBtnFace;    DTP2->Color=clBtnFace;
      CSE_Sutki1->Color=clBtnFace; CSE_Sutki2->Color=clBtnFace;
    }
    Obz1.ZapolnitSpisok(CBSpisokBortov->Items,true);
    BSortSpisokBortov->Tag=1;
    BSortSpisokBortov->Caption="<--";

//������ ������� ��� �������...
    MVolDataSel->Text=String("The number of records in files: ")+
                      String(Obz1.ChisloOtschetov);


    GlobalState=1;
    SetGlobalState();

    extern AnsiString CurPathCfg;
    extern AnsiString CurPath;
    FILE *fp;
    char Strka[2001];
    GetCurrentDirectory(2000,Strka);
    CurPath=Strka;
    fp=fopen(CurPathCfg.c_str(),"w");
    if(fp)
    {
//������� ������
      fputs(CurPath.c_str(),fp);
      fclose(fp);
    }

//���� ������� ��������� ������
    if(FSpisokFiles->N_of_fps>1)
    {
      FGlavOkno->Caption="Analisis. Version 2. Open multiple files";
    }else if(FSpisokFiles->N_of_fps==1){
      FGlavOkno->Caption=String("Analisis. Version 2. ")+
                         FSpisokFiles->FileNames[0];

    }
     RisovPanel->Color=Obz1.OblastCol;
     SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
          SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
          SetkaRad30Col=Obz1.Rad30Col;
          Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
          Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
          Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
          Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
          Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;


     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);

    SetGlobalState();
//�������� ������� ������ ������� � ��������� �����
    OnResize=FormResize;

//    FMesOkno->Hide();
//    delete FMesOkno;
    PanelStatus->Visible=false;
      int i;
  if(N_of_FilesForDelete>0)
  {
    for(i=0;i<N_of_FilesForDelete;i++)
    {
      if(FileExists(FilesForDelete[i]))DeleteFile(FilesForDelete[i]);
    }
    N_of_FilesForDelete=0;
  }

//�������� � �� ���� �� ��������� ����� 0 �����
  AnsiString VnimMes;

//
  if(Obz1.N_of_Pereskokov>0)
  {
    VnimMes=String("Were the transitions to the next day \n");
    for(i=0;i<Obz1.N_of_Pereskokov;i++)
    {
      H1=Obz1.Pereskoki[i]/3600; M1=(Obz1.Pereskoki[i]-H1*3600)/60;
      S1=Obz1.Pereskoki[i]-H1*3600-M1*60;
      H2=Obz1.PrevPereskoki[i]/3600; M2=(Obz1.PrevPereskoki[i]-H2*3600)/60;
      S2=Obz1.PrevPereskoki[i]-H2*3600-M2*60;
      sprintf(Strka,"from %02d:%02d:%02d to %02d:%02d:%02d \n", H2,M2,S2,H1,M1,S1);
      VnimMes=VnimMes+String(Strka);
    }
    MessageBox(NULL,VnimMes.c_str(),"Attention!", MB_OK);
  }




}

//��������� �������� ������ ��� ������ ��
void __fastcall TFGlavOkno::Konec2(void)
{
    Timer1->Enabled=false;
    CGauge1->Visible=false;

    Timer1Timer(NULL);
    double TmPr1,TmPr2;
    TmPr1=Obz1.CLSutkiFirst*24.0+Obz1.CLChasyFirst+Obz1.CLMinutyFirst/60.0+Obz1.CLSekundyFirst/3600.0;
    TmPr2=Obz1.CLSutkiCur*24.0+Obz1.CLChasyCur+Obz1.CLMinutyCur/60.0+Obz1.CLSekundyCur/3600.0;
    if(TmPr2-TmPr1>23+59/60.0+58.9/3600.0)
    {
       Obz1.IsCheckTime=false;
    }


//��� ��������� ����������
    BSortSpisokBortov->Visible=true;
    Button1->Visible=true;
    B_OK->Visible=true;
    GBMashtab->Visible=true;
    BZonaP->Visible=true;
    BZonaS->Visible=true;
    BTochnost->Visible=true;
    BStart->Visible=true;
    PBPause->Visible=true;
    BEnd->Visible=true;
    Image1->Visible=true;
    FGlavOkno->Menu=MainMenu1;


}


/*������ �������� - ��� ������ ���������� -- ����������.
����� ������ ��������� ���������� �� ������� TAllParallel::TAllParallel
� ������ AllParallel
*/
void __fastcall TFGlavOkno::NachaloPrivyazki(void)
{
    OnResize=NULL;
  //������� ��� �����������, ��� �� ����
    BSortSpisokBortov->Visible=false;
    CBSpisokBortov->Visible=false;
    Button1->Visible=false;
    B_OK->Visible=false;
    GBMashtab->Visible=false;
    BZonaP->Visible=false;
    BZonaS->Visible=false;
    BTochnost->Visible=false;
    BStart->Visible=false;
    PBPause->Visible=false;
    BEnd->Visible=false;
    Image1->Visible=false;
    FGlavOkno->Menu=NULL;
//    DTP1->Enabled=true;

//    DTP1->Enabled=true;    DTP2->Enabled=true;
//    CSE_Sutki1->Enabled=true; CSE_Sutki2->Enabled=true;
    DTP1->Color=clWindow;    DTP2->Color=clWindow;
    CSE_Sutki1->Color=clWindow; CSE_Sutki2->Color=clWindow;
    CGauge1->Visible=true;
//    Timer1->Enabled=true;
}

/*������ �������� - ��� ������ ���������� -- ����������.
����� ������ ��������� ���������� �� ������� TAllParallel::Execute
� ������ AllParallel.
*/

void __fastcall TFGlavOkno::SamaPrivyazka(void)
{
//��������. �������� ��������� ��� ��������. ����������� � ������ TraektKalman.cpp 
   // Obz1.FindNeoprNomBort();

}

/*������ �������� - ��� ������ ���������� -- ����������.
����� ������ ��������� ���������� �� ������� TAllParallel::Execute
� ������ AllParallel
*/

void __fastcall TFGlavOkno::KonecPrivyazki(void)
{
    Timer1->Enabled=false;
    CGauge1->Visible=false;

    if(Obz1.CLSutkiCur!=0)
    {

       double TmPr1,TmPr2;
       TmPr1=Obz1.CLSutkiFirst*24.0+Obz1.CLChasyFirst+Obz1.CLMinutyFirst/60.0+Obz1.CLSekundyFirst/3600.0;
       TmPr2=Obz1.CLSutkiCur*24.0+Obz1.CLChasyCur+Obz1.CLMinutyCur/60.0+Obz1.CLSekundyCur/3600.0;
       if(TmPr2-TmPr1>23+59/60.0+58.9/3600.0)
       {
         Obz1.IsCheckTime=false;
       }
    }


//��� ��������� ����������
    BSortSpisokBortov->Visible=true;
    Button1->Visible=true;
    B_OK->Visible=true;
    GBMashtab->Visible=true;
    BZonaP->Visible=true;
    BZonaS->Visible=true;
    BTochnost->Visible=true;
    BStart->Visible=true;
    PBPause->Visible=true;
    BEnd->Visible=true;
    Image1->Visible=true;
    FGlavOkno->Menu=MainMenu1;


    Obz1.ZapolnitSpisok(CBSpisokBortov->Items,BSortSpisokBortov->Tag==1);
    SetGlobalState();
    OnResize=FormResize;
    CGauge1->Visible=false;
//    FMesOkno->Hide();
    Application->Title="Analisis";
//    delete FMesOkno;
    PanelStatus->Visible=false;
    B_OKClick(NULL);
}


//----------------------------------------------------------
//��������� ��� ��� ���� ������� � ����������� �� ������ ���������
void __fastcall TFGlavOkno::SetGlobalState(void)
{
   double KoefX,KoefY;
   char Strka[100];
   switch(GlobalState)
   {
     case 0:   //��������� ��������� - ��������� ������ ��������
       DTP1->Visible=true;
       DTP2->Visible=true;
       Label1->Visible=true;
       Label2->Visible=true;
       LCurTime->Visible=false;

 mnuFile->Enabled=true; mnuDates->Enabled=true; mnuFindRS->Enabled=true;
 mnuOblast->Enabled=false;mnuOption->Enabled=true; mnuHelp->Enabled=true;
 GBMashtab->Enabled=true;
 SVydel->Visible=false;

 MListOfFiles->Enabled=true;  mnuSaveAs->Enabled=false; mnuSaveVydelAs->Enabled=false;
 mnuSetUserInfo->Enabled=true; mnuSetDocDate->Enabled=false; mnuSetDannyeInfo->Enabled=false;
 mnuIzvlech->Enabled=false; mnuSpiskiBort->Enabled=false; mnuDates->Enabled=false;
 mnuFindRS->Enabled=false; B_OK->Enabled=false; BTochnost->Enabled=false;
 BStart->Enabled=false; PBPause->Enabled=false;  BEnd->Enabled=false; CBNazad->Enabled=false;
 mnuPrint->Enabled=false; BZonaP->Enabled=false; BZonaS->Enabled=false;
 mnuPredyshMashtab->Enabled=false;
 mnuFindPObjed->Enabled=false;

 //������� ��, ��� �� ��������
    MVolDataSel->Text=String("");
    MVolVydelData->Text=String("");
    MVydelenoSek->Text=String("");
    MKurs->Text=String("");
    MSredSkorost->Text=String("");
    Button1->Enabled=false;


      MVydelenoSek->Text="";
      MKurs->Text="";
      MSredSkorost->Text="";
      M_SKO_D->Text="";
      M_SKO_A->Text="";


     break;
     case 1:   //����� ������� ������ �� � ����� ��������� �������
       DTP1->Visible=true;
       DTP2->Visible=true;
       Label1->Visible=true;
       Label2->Visible=true;
       LCurTime->Visible=false;
 mnuFile->Enabled=true; mnuDates->Enabled=true; mnuFindRS->Enabled=true;
 mnuOblast->Enabled=true;mnuOption->Enabled=true; mnuHelp->Enabled=true;
 GBMashtab->Enabled=true;

 MListOfFiles->Enabled=true;
 mnuSetUserInfo->Enabled=true; mnuSetDocDate->Enabled=true; mnuSetDannyeInfo->Enabled=true;
 mnuSpiskiBort->Enabled=true;
 mnuDates->Enabled=true;
 mnuFindRS->Enabled=true; B_OK->Enabled=true;
 mnuOblast->Enabled=true;
 mnuPrint->Enabled=true;

//�������� ����� �� �������� ��������� ��������
   if(N_of_PrevKm>0)
   {
     mnuPredyshMashtab->Enabled=true;
   }else{
     mnuPredyshMashtab->Enabled=false;
   }

   if(VidOblasti==0)
   {
      mnuVnutriOblasti->Enabled=true;
      KoefX=((double)(RightKm-LeftKm))/(Image1->Width-1);
      KoefY=((double)(BottomKm-TopKm))/(Image1->Height-1);
      Xramki[0]=KoefX*(SVydel->Left)+LeftKm;
      Yramki[0]=KoefY*(SVydel->Top+SVydel->Height-1)+TopKm;

      Xramki[1]=KoefX*(SVydel->Left+SVydel->Width-1)+LeftKm;
      Yramki[1]=KoefY*(SVydel->Top+SVydel->Height-1)+TopKm;

      Xramki[2]=KoefX*(SVydel->Left+SVydel->Width-1)+LeftKm;
      Yramki[2]=KoefY*(SVydel->Top)+TopKm;
      Xramki[3]=KoefX*(SVydel->Left)+LeftKm;
      Yramki[3]=KoefY*(SVydel->Top)+TopKm;

      SVydel->Visible=true;

   }else{
      mnuVnutriOblasti->Enabled=false;
      SVydel->Visible=false;
   }
   if(Obz1.ChisloVydelOtschetov)
   {
     BTochnost->Enabled=true;
     BZonaS->Enabled=true;
     if(Obz1.ChisloVydelOtschetovP+Obz1.ChisloVydelOtschetovPS)
     {
       BZonaP->Enabled=true;
     }else{
       BZonaP->Enabled=false;
     }
     mnuShowDates->Enabled=true;
     mnuSaveVydelAs->Enabled=true;
     mnuPrintData->Enabled=true;
     mnuClearSelection->Enabled=true;
     MVolVydelData->Text=String("Amount of selected data: ")+
                String(Obz1.ChisloVydelOtschetov);

   }else{
     BTochnost->Enabled=false;
     BZonaS->Enabled=false;
     BZonaP->Enabled=false;
     mnuShowDates->Enabled=false;
     mnuSaveVydelAs->Enabled=false;
     mnuPrintData->Enabled=false;
     mnuClearSelection->Enabled=false;
     MVolVydelData->Text="";
   }
   if(Obz1.ChisloFilterOtschetov)
   {
      BStart->Enabled=true; PBPause->Enabled=false;  BEnd->Enabled=false; CBNazad->Enabled=true;
      TBDTimeAnim->Enabled=true;
      mnuSaveAs->Enabled=true;
      mnuShowDates1->Enabled=true;
      mnuPrintData1->Enabled=true;
      mnuPrintPicture->Enabled=true;
      if(Obz1.IsWasPrivyazka&&Obz1.IsInPlot)
      {
        mnuFindPObjed->Enabled=true;
      }else{
        mnuFindPObjed->Enabled=false;
      }

      MVolDataSel->Text=String("Amount of displayed data: ")+
                        String(Obz1.ChisloFilterOtschetov);
      if(Obz1.IsWasPrivyazka)
      {
         mnuDroblVK->Enabled=true;
      }else{
         mnuDroblVK->Enabled=false;
      }
      mnuClearNotSelection->Enabled=true;

   }else{
      BStart->Enabled=false; PBPause->Enabled=false;  BEnd->Enabled=false; CBNazad->Enabled=false;
      TBDTimeAnim->Enabled=false;
      mnuSaveAs->Enabled=false;
      mnuShowDates1->Enabled=false;
      mnuPrintData1->Enabled=false;
      mnuPrintPicture->Enabled=false;
      MVolDataSel->Text=String("Number of records in the file: ")+
                      String(Obz1.ChisloOtschetov);
      mnuDroblVK->Enabled=false;
      mnuClearNotSelection->Enabled=true;
      mnuFindPObjed->Enabled=false;
   }
   if(Obz1.ChisloOtschetov)
   {
     mnuIzvlech->Enabled=true;
     mnuStat->Enabled=true;
     mnuOtmetkiMejduNord->Enabled=true;
     mnuOtmetkiMejduNordSred->Enabled=true;
     mnuFindRS->Enabled=true;
     mnuProvestiPrivyazku->Enabled=true;
   }else{
     mnuIzvlech->Enabled=false;
     mnuStat->Enabled=false;
     mnuDop->Enabled=false;
     mnuOtmetkiMejduNord->Enabled=false;
     mnuOtmetkiMejduNordSred->Enabled=false;
     mnuFindRS->Enabled=false;
     mnuProvestiPrivyazku->Enabled=false;
   }
   if(Obz1.GetNumberOfSortedBorts())
   {
      mnuSpiskiBort->Enabled=true;
   }else{
      mnuSpiskiBort->Enabled=false;
   }

    if(mnuFindRS->Enabled)
    {
 //���������� � ����������� ������������ ���������� ������
      mnuRS_PK_FindBlizkie->Enabled=true;
      if(Obz1.IsWasSearchingRS_P&&Obz1.IsCheckNumber)
      {
         mnuRS_PK_VklBorta->Enabled=true;
      }else{
         mnuRS_PK_VklBorta->Enabled=false;
      }
      if(Obz1.IsWasSearchingRS_P)
      {
        mnuRS_PK_ViewVydel->Enabled=true;
        mnuRS_PK_Clear->Enabled=true;
      }else{
        mnuRS_PK_ViewVydel->Enabled=false;
        mnuRS_PK_Clear->Enabled=false;
      }

//���������� � �� ���������� ������
      mnuRS_VK_FindBlizkie->Enabled=true;
      if(Obz1.IsWasSearchingRS_VK&&Obz1.IsCheckNumber)
      {
         mnuRS_VK_VklBorta->Enabled=true;
      }else{
         mnuRS_VK_VklBorta->Enabled=false;
      }
      if(Obz1.IsWasSearchingRS_VK)
      {
        mnuRS_VK_ViewVydel->Enabled=true;
        mnuRS_VK_Clear->Enabled=true;
      }else{
        mnuRS_VK_ViewVydel->Enabled=false;
        mnuRS_VK_Clear->Enabled=false;
      }
    }




    Button1->Enabled=true;

    if(Obz1.IsWasPrivyazka)
    {
      mnuProvestiPrivyazku->Visible=false;
//      mnuUbratPrivyazku->Visible=;
      mnuUbratPrivyazku->Enabled=true;
      mnuDop->Enabled=true;

    }else{
      mnuProvestiPrivyazku->Visible=true;
      mnuUbratPrivyazku->Visible=false;
      mnuUbratPrivyazku->Enabled=false;
      mnuDop->Enabled=false;
    }

    if(IzFindTochnost)
    {
//��������� ��������
      MVydelenoSek->Visible=true;
      sprintf(Strka,"Selected: %.0lf �",dTimePolyot);
      MVydelenoSek->Text=Strka;


        MKurs->Visible=true;
      if(KursPolyota>=-0.01)
      {
        sprintf(Strka,"Heading: %.0lf gr",KursPolyota);
        MKurs->Text=Strka;
        sprintf(Strka,"Speed: %.0lf km/h",SredSkorostPolyota);
        MSredSkorost->Text=Strka;

      }else{
        sprintf(Strka,"Heading is unknown");
        MKurs->Text=Strka;
        sprintf(Strka,"Speed is inknown");
        MSredSkorost->Text=Strka;
      }

      MSredSkorost->Visible=true;

      if(StepenPolyot)
      {
        M_SKO_D->Visible=true;
        sprintf(Strka,"MSE(R)=%.01lf m",T_SKO_D);
        M_SKO_D->Text=Strka;


        M_SKO_A->Visible=true;
        sprintf(Strka,"MSE(A)=%.01lf min",T_SKO_A);
        M_SKO_A->Text=Strka;
      }else{
    //���� ������� �������
        M_SKO_D->Visible=true;
        M_SKO_D->Lines->Clear();
        sprintf(Strka,"MSE(R)=%.01lf m",T_SKO_D);
        M_SKO_D->Lines->Add(Strka);
        sprintf(Strka,"Average range=%.02lf km",SredD);
        M_SKO_D->Lines->Add(Strka);
        M_SKO_A->Visible=true;
        M_SKO_A->Lines->Clear();
        sprintf(Strka,"MSE(A)=%.01lf min.", T_SKO_A);
        M_SKO_A->Lines->Add(Strka);

        sprintf(Strka,"Aver.azim.=%.02lf gr.",SredA);
        M_SKO_A->Lines->Add(Strka);

        M_SKO_A->Text=Trim(M_SKO_A->Text);
        M_SKO_D->Text=Trim(M_SKO_D->Text);
      }


    }else{
      MVydelenoSek->Text="";
      MKurs->Text="";
      MSredSkorost->Text="";
      M_SKO_D->Text="";
      M_SKO_A->Text="";
    }




     break;

     case 2:   //����� VidOblasti==1
       DTP1->Visible=true;
       DTP2->Visible=true;
       Label1->Visible=true;
       Label2->Visible=true;
       LCurTime->Visible=false;
        mnuFile->Enabled=false; mnuDates->Enabled=false; mnuFindRS->Enabled=false;
        mnuOption->Enabled=false; mnuHelp->Enabled=false;
        GBMashtab->Enabled=false; BTochnost->Enabled=false;   BZonaP->Enabled=false;
        BZonaS->Enabled=false;  B_OK->Enabled=false;
        BStart->Enabled=false; PBPause->Enabled=false;  BEnd->Enabled=false; CBNazad->Enabled=false;
        TBDTimeAnim->Enabled=false;
        mnuVnutriOblasti->Enabled=false;
        mnuClearSelection->Enabled=false;
        mnuPredyshMashtab->Enabled=false;
        mnuNotPriam->Enabled=true;
        mnuFindPObjed->Enabled=false;

     if(VidOblasti==1)
     {
       SVydel->Visible=false;
       mnuOblast->Enabled=true;
     }else{
        SVydel->Visible=true;
        mnuOblast->Enabled=false;
     }
 //������� ��� �����������
     break;

     case 3:   //������������
       mnuDates->Enabled=true;
       if(TimerAnimate->Enabled)
       {
         mnuShowDates->Enabled=false;
         mnuShowDates1->Enabled=false;
       }else{
         mnuShowDates->Enabled=true;
         mnuShowDates1->Enabled=true;
       }
        mnuDop->Enabled=false;
        mnuOtmetkiMejduNord->Enabled=false;
        mnuOtmetkiMejduNordSred->Enabled=false;
        mnuMaxLojnS->Enabled=false;
        mnuDroblVK->Enabled=false;
        mnuProvestiPrivyazku->Enabled=false;
        mnuUbratPrivyazku->Enabled=false;
       mnuFindRS->Enabled=false;
       mnuOblast->Enabled=true;
        mnuVnutriOblasti->Enabled=true;
        mnuNotPriam->Enabled=false;
        mnuClearSelection->Enabled=false;
        mnuClearNotSelection->Enabled=true;
        mnuFindPObjed->Enabled=false;
       if(N_of_PrevKm>0)
       {
         mnuPredyshMashtab->Enabled=true;
       }else{
         mnuPredyshMashtab->Enabled=false;
       }




       mnuOption->Enabled=false;
       B_OK->Enabled=false;
       BTochnost->Enabled=false;
       BZonaP->Enabled=false;
       BZonaS->Enabled=false;
       BStart->Enabled=false;
       PBPause->Enabled=true;
       BEnd->Enabled=true;
       CGauge1->Visible=true;
       DTP1->Visible=false;
       DTP2->Visible=false;
       Label1->Visible=false;
       Label2->Visible=false;
       LCurTime->Visible=true;


     break;

//������� ������ ������ �� �����
     case 4:
       DTP1->Visible=false;
       DTP2->Visible=false;
       Label1->Visible=false;
       Label2->Visible=false;
       LCurTime->Visible=false;
 mnuFile->Enabled=false; mnuDates->Enabled=false; mnuFindRS->Enabled=false;
 mnuOblast->Enabled=false;mnuOption->Enabled=false; mnuHelp->Enabled=false;
 GBMashtab->Enabled=false;
 mnuFindPObjed->Enabled=false;


 MListOfFiles->Enabled=false;
 BTochnost->Enabled=false;
 BZonaP->Enabled=false; BZonaS->Enabled=false;
 BStart->Enabled=false;  PBPause->Enabled=false;
 BEnd->Enabled=false;
 CBNazad->Enabled=false;


     break;
   };
}

//=========================--------------------------------------------

void __fastcall TFGlavOkno::Timer1Timer(TObject *Sender)
{
  //����� ������� ������
   CSE_Sutki1->Value=Obz1.CLSutkiFirst+1;
   CSE_Sutki2->Value=Obz1.CLSutkiCur+1;
   DTP1->DateTime=TDateTime(
                           (unsigned short)Obz1.CLChasyFirst,
                           (unsigned short)Obz1.CLMinutyFirst,
                           (unsigned short)Obz1.CLSekundyFirst,
                           (unsigned short)0
                           );
   DTP2->DateTime=TDateTime(
                           (unsigned short)Obz1.CLChasyCur,
                           (unsigned short)Obz1.CLMinutyCur,
                           (unsigned short)Obz1.CLSekundyCur,
                           (unsigned short)0
                           );


   CGauge1->MinValue=0;

   CGauge1->MaxValue=Obz1.GlobalGaugeMaxTraekt;

   CGauge1->Progress=Obz1.GlobalGaugeCurTraekt;

}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::B_OKClick(TObject *Sender)
{
/*��������� ������ OK. ������ ������� ������������� ������ ��������� �� �����*/
  int Ret;
  double TimeF,TimeL;
  double Tm1,Tm2;


    if(CBSpisokBortov->Height>100)
  {
    CBSpisokBortov->Width=80;
    CBSpisokBortov->Height=17;
    CBSpisokBortov->Visible=false;
    Button1->Visible=true;
  }



  GlobalState=4;     //�� ������ ������ ��������� ��������� 4, ������� ���������
  SetGlobalState();  //����������
//
  IzFindTochnost=false;  //��� ������ ������� ����������
//������� ��� ������ �� ��� � ������������������ ������
  if(N_of_DToch)
  {
    N_of_DToch=0;
    free(DToch);DToch=NULL;
  }


//������� ���������� ������� ����� ������, ��� ������ ������
  Ret=Obz1.GetDataAboutSelectedNumber(CBSpisokBortov,BSortSpisokBortov->Tag);
  if(Ret==-1)
  {
    MessageBox(NULL,"Error! Error!",
                     "Error!",MB_OK);
    GlobalState=1;
     SetGlobalState();

    return;
  }
  if(Ret==0)
  {
    MessageBox(NULL,"No data!", "Error!",MB_OK);
    GlobalState=1;
    SetGlobalState();

    return;
  }

  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;

//������� ����� � �����
  if(Obz1.IsCheckTime)
  {
    DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
    DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
    if(DTP1->DateTime>DTP2->DateTime)
    {
       CSE_Sutki1->Value=1;
       CSE_Sutki2->Value=2;
    }else{
//� ��� ���� ��������� ����� ����� �����
      TimeF=Obz1.CLChasyFirst+Obz1.CLMinutyFirst/60.0+
            Obz1.CLSekundyFirst/3600.0;
      TimeL=Obz1.CLChasyCur+Obz1.CLMinutyCur/60.0+Obz1.CLSekundyCur/3600.0;
      if(TimeF<=TimeL)
      {
         CSE_Sutki1->Value=1;
         CSE_Sutki2->Value=1;
      }else{
//� ��� ������� �� ���� ����� ����� �� �������
         Tm1=H1+M1/60.0+S1/3600.0-0.0000000001;
         Tm2=H2+M2/60.0+S2/3600.0+0.0000000001;
         if(Tm1<=TimeL)
         {
           CSE_Sutki1->Value=2;
           CSE_Sutki2->Value=2;

         }else if(Tm2>=TimeF)
         {
           CSE_Sutki1->Value=1;
           CSE_Sutki2->Value=1;
         }else{
//           Tm1>TimeL � Tm2<TimeF � Tm1<Tm2
             CSE_Sutki1->Value=1;
             CSE_Sutki2->Value=1;
         }
      }
    }


/*��������� ������ � �������*/
      Ret=Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                               (int)H1,(int)M1,(int)S1,
                               CSE_Sutki2->Value-1,
                               (int)H2,(int)M2,(int)S2);
    if(Ret!=1)
    {
      MessageBox(NULL,"The time interval is not set properly!", "Error!",MB_OK);
      GlobalState=1;
      SetGlobalState();
      return;
    }
  }

//�������� �������� �������� ��������� ��������� ������
  if(Obz1.IsCheckVys)
  {
/*�������� �������� �����*/
    int Chislo1,Chislo2;
    Ret=sscanf(EVysota1->Text.c_str(),"%d",&Chislo1);
    if(Ret!=1)
    {
      MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
      EVysota1->SetFocus();
      GlobalState=1;
      SetGlobalState();
      return;
    }
    Ret=sscanf(EVysota2->Text.c_str(),"%d",&Chislo2);
    if(Ret!=1)
    {
      MessageBox(NULL,"The altitude value should be an integer", "Error!", MB_OK);
      EVysota2->SetFocus();
      GlobalState=1;
      SetGlobalState();
      return;
    }


    if(Chislo1>Chislo2)
    {
      MessageBox(NULL,"Interval atlitude is incorrect", "Error!",MB_OK);
      GlobalState=1;
      SetGlobalState();

      return;
    }
    Obz1.Vys1=Chislo1;
    Obz1.Vys2=Chislo2;
  }


//������������� ������. (Traekt.cpp)
  Obz1.MakeFilter();

//��������� �����
   RisovPanel->Color=Obz1.OblastCol;
   SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
          SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
          SetkaRad30Col=Obz1.Rad30Col;
               Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
          Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
          Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
          Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
          Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;

//������� ��  ����� ����� (������ ���������� � ���������� ������)
//VyvodSetkaANLSS (RisovatSetku.cpp)
  VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
  CGauge1->Progress=0;
  CGauge1->MaxValue=100;
  CGauge1->Visible=true;

  Application->ProcessMessages();      

  Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);

//������ ���������� �������
  Application->ProcessMessages();
  VyzovPosleVydeleniaOblasti();
  Application->ProcessMessages();
  GlobalState=1;
  SetGlobalState();
  CGauge1->Visible=false;




}
//---------------------------------------------------------------------------




/*������� ���������!!!*/
void __fastcall  TFGlavOkno::FreeNotPriamOblast(void)
{
  VidOblasti=0;
  if(GlobalState)
  {
    SVydel->Visible=true;
  }else{
     SVydel->Visible=false;
  }
  N_NotPriam=0;
  XNotPriam=(double*)realloc(XNotPriam,sizeof(double)*N_NotPriam);
  YNotPriam=(double*)realloc(YNotPriam,sizeof(double)*N_NotPriam);

}

/*��������*/
void __fastcall  TFGlavOkno::RisovatNotPriamOblast(void)
{
  int i;
  long Xv,Yv;
  double KoefX,KoefY;
  if(VidOblasti==0)
  {
    return;
  }

  KoefX=((double)(Image1->Width-1))/(RightKm-LeftKm);
  KoefY=((double)(Image1->Height-1))/(BottomKm-TopKm);


//�������� �������
  Image1->Picture->Bitmap->Canvas->Pen->Mode=pmXor;
  Image1->Picture->Bitmap->Canvas->Pen->Color=(TColor)RGB(255,200,0);
  Image1->Picture->Bitmap->Canvas->Pen->Style=psSolid;

  Xv=Okrugl(KoefX*(XNotPriam[0]-LeftKm));
  Yv=Okrugl(KoefY*(YNotPriam[0]-TopKm));

//������� �����...

//  Image1->Picture->Bitmap->Canvas->Pixels[Xv][Yv]=RGB(255,200,0);

  Image1->Picture->Bitmap->Canvas->MoveTo(Xv,Yv);
  for(i=1;i<N_NotPriam; i++)
  {
      Xv=Okrugl(KoefX*(XNotPriam[i]-LeftKm));
      Yv=Okrugl(KoefY*(YNotPriam[i]-TopKm));
      Image1->Picture->Bitmap->Canvas->LineTo(Xv,Yv);
  }

  if(VidOblasti==2)
  {
     Xv=Okrugl(KoefX*(XNotPriam[0]-LeftKm));
     Yv=Okrugl(KoefY*(YNotPriam[0]-TopKm));
     Image1->Picture->Bitmap->Canvas->LineTo(Xv,Yv);
  }

}


//�������� ����� ��� �������������� �������
void __fastcall  TFGlavOkno::PechatOblast(TCanvas *Canvas,   //
                                          int Wdth,int Hght, //������� ��� ������� ������
                                          int Lft,int Tp
                                          )
{
  int i;
  long Xv,Yv;
  double KoefX,KoefY;
  if(VidOblasti==0)
  {
    KoefX=((double)(Wdth-1))/(RightKm-LeftKm);
    KoefY=((double)(Hght-1))/(BottomKm-TopKm);
    Canvas->Pen->Mode=pmNotXor;
    Canvas->Pen->Color=(TColor)RGB(180,180,200);
    Canvas->Pen->Style=psDash;
    Xv=Okrugl(KoefX*(Xramki[0]-LeftKm));
    Yv=Okrugl(KoefY*(Yramki[0]-TopKm));
    Canvas->MoveTo(Xv+Lft,Yv+Tp);

    Xv=Okrugl(KoefX*(Xramki[1]-LeftKm));
    Yv=Okrugl(KoefY*(Yramki[1]-TopKm));
    Canvas->LineTo(Xv+Lft,Yv+Tp);

    Xv=Okrugl(KoefX*(Xramki[2]-LeftKm));
    Yv=Okrugl(KoefY*(Yramki[2]-TopKm));
    Canvas->LineTo(Xv+Lft,Yv+Tp);

    Xv=Okrugl(KoefX*(Xramki[3]-LeftKm));
    Yv=Okrugl(KoefY*(Yramki[3]-TopKm));
    Canvas->LineTo(Xv+Lft,Yv+Tp);

    Xv=Okrugl(KoefX*(Xramki[0]-LeftKm));
    Yv=Okrugl(KoefY*(Yramki[0]-TopKm));
    Canvas->LineTo(Xv+Lft,Yv+Tp);


    return;
  }

  KoefX=((double)(Wdth-1))/(RightKm-LeftKm);
  KoefY=((double)(Hght-1))/(BottomKm-TopKm);


//�������� �������
  Canvas->Pen->Mode=pmNotXor;
  Canvas->Pen->Color=(TColor)RGB(180,180,200);
  Canvas->Pen->Style=psSolid;

  Xv=Okrugl(KoefX*(XNotPriam[0]-LeftKm));
  Yv=Okrugl(KoefY*(YNotPriam[0]-TopKm));

//������� �����...

//  Image1->Picture->Bitmap->Canvas->Pixels[Xv][Yv]=RGB(255,200,0);

  Canvas->MoveTo(Xv+Lft,Yv+Tp);
  for(i=1;i<N_NotPriam; i++)
  {
      Xv=Okrugl(KoefX*(XNotPriam[i]-LeftKm));
      Yv=Okrugl(KoefY*(YNotPriam[i]-TopKm));
      Canvas->LineTo(Xv+Lft,Yv+Tp);
  }

  if(VidOblasti==2)
  {
     Xv=Okrugl(KoefX*(XNotPriam[0]-LeftKm));
     Yv=Okrugl(KoefY*(YNotPriam[0]-TopKm));
     Canvas->LineTo(Xv+Lft,Yv+Tp);
  }
}



/*�������  -  �������� ��������������� �������*/
void __fastcall TFGlavOkno::mnuNotPriamClick(TObject *Sender)
{
//������ ��������� ��������������� �������
//��������� ����� �� ������� Obz1
   RisovPanel->Color=Obz1.OblastCol;
   SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
          SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
          SetkaRad30Col=Obz1.Rad30Col;
               Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
          Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
          Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
          Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
          Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;

//������� ����� (������ ���������, ���������� �����)   ������: RisovatSetku
   VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);

//������� ������ �� ����� ����������: Traekt.cpp
   Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
   FreeNotPriamOblast(); //�� ������ ������ �������� ��� ������ � ��������������� �������
   SVydel->Visible=false; //������ ����������� ������������� ������� ���������
   VidOblasti=1;      //����������, ��� ����� ����� ��������� ��������������� �������
   GlobalState=2;     //�� �� �����, ������ ����������� �����
   SetGlobalState();  //���������� ��������� ����������� ���� � ������������ �
                      //GlobalState
}
//---------------------------------------------------------------------------















void __fastcall TFGlavOkno::VyzovPosleVydeleniaOblasti(void)
{
  double Xpr[4],Ypr[4];
  double KoefX,KoefY;
  if(VidOblasti==2)
  {
    Obz1.VydelennayaOblast(XNotPriam,YNotPriam,N_NotPriam);
  }else{
   KoefX=((double)(RightKm-LeftKm))/(Image1->Width-1);
   KoefY=((double)(BottomKm-TopKm))/(Image1->Height-1);

    Xpr[0]=KoefX*(SVydel->Left)+LeftKm-0.01;
    Ypr[0]=KoefY*(SVydel->Top+SVydel->Height-1)+TopKm-0.01;
    Xpr[1]=KoefX*(SVydel->Left)+LeftKm-0.01;
    Ypr[1]=KoefY*(SVydel->Top)+TopKm+0.01;
    Xpr[2]=KoefX*(SVydel->Left+SVydel->Width-1)+LeftKm+0.01;
    Ypr[2]=KoefY*(SVydel->Top)+TopKm+0.01;
    Xpr[3]=KoefX*(SVydel->Left+SVydel->Width-1)+LeftKm+0.01;
    Ypr[3]=KoefY*(SVydel->Top+SVydel->Height-1)+TopKm-0.01;
    Obz1.VydelennayaOblast(Xpr,Ypr,4);
  }
}






















/*"����" - "������ ���� � ������������". ��������� ������ � ��� ������������ �
��� �������������*/
void __fastcall TFGlavOkno::mnuSetUserInfoClick(TObject *Sender)
{
   FUserInfo->Edit1->Text=Obz1.FIO_User.Trim();
   FUserInfo->Edit2->Text=Obz1.Podr_User.Trim();
   FUserInfo->Tag=0;
   FUserInfo->ShowModal();
   if(FUserInfo->Tag)
   {
     Obz1.FIO_User=FUserInfo->Edit1->Text.Trim();
     Obz1.Podr_User=FUserInfo->Edit2->Text.Trim();
   }
}
//---------------------------------------------------------------------------
/*"����" - "������ ���� � ���� ����������������".*/
void __fastcall TFGlavOkno::mnuSetDocDateClick(TObject *Sender)
{

   Obz1.Date_Input=InputBox("Date documentation","Enter the date documentation of files",
                Obz1.Date_Input);

}
//---------------------------------------------------------------------------
/*"����" - "������ ����������� ���������� � ������".*/
void __fastcall TFGlavOkno::mnuSetDannyeInfoClick(TObject *Sender)
{
     Obz1.Dop_Info=InputBox("Enter the required information",
        "Enter the required information about the data",
                Obz1.Dop_Info);

}
//---------------------------------------------------------------------------


/*���� - �������� ���������� ������*/
void __fastcall TFGlavOkno::mnuShowDatesClick(TObject *Sender)
{
    unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
    extern TCGauge *GlobalCGauge1;
    GlobalCGauge1=CGauge1;

    DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
    DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
    Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

   if(FViewVydelData==NULL)
   {
      FViewVydelData=new TPreviewForm(NULL);
   }

   FViewVydelData->Hide();
   FViewVydelData->Caption=AnsiString("Table of selected data");
   if(GlobalState==3)
   {
      //���� ������ ����� ��������, �� ������� ��������������� ��������� ���
      //����������� � ��������� ����, ������� ����������� � TraektAnim.cpp
      Obz1.ShowDatasAnim(FViewVydelData->RichEdit1->Lines,NazadAnim,
       SVydel->Left,SVydel->Top,
       SVydel->Left+SVydel->Width, SVydel->Top+SVydel->Height);
   }else{
//���������� ������ ��������� ����� OBZORY_1, ������������� � Traekt2.cpp
      Obz1.ShowDatas(FViewVydelData->RichEdit1->Lines);
   }

//���������� ����
   FViewVydelData->WindowState=wsNormal;
   FViewVydelData->HelpFile=HelpFileName;
   FViewVydelData->HelpContext=10102;
   FViewVydelData->WindowState=wsMaximized;
   FViewVydelData->Show();



}
//---------------------------------------------------------------------------
/*���� - �������� ������������ ������*/
void __fastcall TFGlavOkno::mnuShowDates1Click(TObject *Sender)
{
    unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
    extern TCGauge *GlobalCGauge1;
    GlobalCGauge1=CGauge1;

    DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
    DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
    Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

   if(FViewData==NULL)
   {
      FViewData=new TPreviewForm(NULL);
   }
   FViewData->Hide();
   FViewData->Caption="Table of data";
   if(GlobalState==3)
   {
     //���� ������ ����� ��������, �� ������� ��������������� ��������� ���
     //����������� � ��������� ����, ������� ����������� � TraektAnim.cpp
      Obz1.ShowDatasAnim1(FViewData->RichEdit1->Lines,NazadAnim);
   }else{
//���������� ������ ��������� ����� OBZORY_1, ������������� � Traekt2.cpp
      Obz1.ShowDatas1(FViewData->RichEdit1->Lines);
   }
   FViewData->WindowState=wsNormal;
   FViewData->HelpFile=HelpFileName;
   FViewData->HelpContext=10101;
   FViewData->WindowState=wsMaximized;
   FViewData->Show();

}
//---------------------------------------------------------------------------

/*���� - ������������ ���������� ������� P.*/
void __fastcall TFGlavOkno::mnuOtmetkiMejduNordClick(TObject *Sender)
{

  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  int Ret;
  if(Obz1.CLSutkiCur!=0)
  {

    double TmPr1,TmPr2;
    TmPr1=Obz1.CLSutkiFirst*24.0+Obz1.CLChasyFirst+Obz1.CLMinutyFirst/60.0+Obz1.CLSekundyFirst/3600.0;
    TmPr2=Obz1.CLSutkiCur*24.0+Obz1.CLChasyCur+Obz1.CLMinutyCur/60.0+Obz1.CLSekundyCur/3600.0;
    if(TmPr2-TmPr1>23+59/60.0+58.9/3600.0)
    {

       MessageBox(NULL,"Here the data do not lie within a day - can not perform the operation!",
                       "Error",MB_OK);
       return;
    }
  }

  if(Obz1.IsCheckTime)
  {
    DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
    DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
    Ret=Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);
    if(Ret!=1)
    {
      MessageBox(NULL,"The time interval is incorrect!", "Error!",MB_OK);
      return;
    }
  }


   if(FMaxPOtmetok==NULL)
   {
      FMaxPOtmetok=new TPreviewForm(NULL);
   }
   FMaxPOtmetok->Hide();
   FMaxPOtmetok->Caption="Maximum number of P marks between Norths";

//��������� ���� FMaxPOtmetok->RichEdit1->Lines
//����������: Traekt2.cpp
   Obz1.MaxPOtmetokZaObzor(FMaxPOtmetok->RichEdit1->Lines,
                           FSpisokFiles->FileNames);
   FMaxPOtmetok->WindowState=wsNormal;
   FMaxPOtmetok->HelpFile=HelpFileName;
   FMaxPOtmetok->HelpContext=10103;
   FMaxPOtmetok->WindowState=wsMaximized;
   FMaxPOtmetok->Show();

}
//---------------------------------------------------------------------------


/*���� - �����*/
void __fastcall TFGlavOkno::mnuExitClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::mnuOtmetkiMejduNordSredClick(TObject *Sender)
{
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  int Ret;
  if(Obz1.CLSutkiCur!=0)
  {
       double TmPr1,TmPr2;
       TmPr1=Obz1.CLSutkiFirst*24.0+Obz1.CLChasyFirst+Obz1.CLMinutyFirst/60.0+Obz1.CLSekundyFirst/3600.0;
       TmPr2=Obz1.CLSutkiCur*24.0+Obz1.CLChasyCur+Obz1.CLMinutyCur/60.0+Obz1.CLSekundyCur/3600.0;
       if(TmPr2-TmPr1>23+59/60.0+58.9/3600.0)
       {

          MessageBox(NULL,"These lie within a day!",
                    "Error!",MB_OK);
          return;
        }  
  }


  if(Obz1.IsCheckTime)
  {
    DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
    DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
    Ret=Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);
    if(Ret!=1)
    {
      MessageBox(NULL,"�������� ������� ����� �� �����!", "������!!!",MB_OK);
      return;
    }
  }


   if(FMaxSredPOtmetok==NULL)
   {
      FMaxSredPOtmetok=new TPreviewForm(NULL);
   }
   FMaxSredPOtmetok->Hide();
   FMaxSredPOtmetok->Caption="������������ ������� ����� P ������� �� �����";

//��������� ���� FMaxPOtmetok->RichEdit1->Lines
//����������: Traekt2.cpp
   Obz1.MaxSredPOtmetokZaObzor(FMaxSredPOtmetok->RichEdit1->Lines,
                           FSpisokFiles->FileNames);
   FMaxSredPOtmetok->WindowState=wsNormal;
   FMaxSredPOtmetok->HelpFile=HelpFileName;
   FMaxSredPOtmetok->HelpContext=10104;
   FMaxSredPOtmetok->WindowState=wsMaximized;
   FMaxSredPOtmetok->Show();


}
//---------------------------------------------------------------------------




/*������� - �������� ���������� �������*/
void __fastcall TFGlavOkno::mnuClearSelectionClick(TObject *Sender)
{
   Obz1.ClearSelection();  //�������� ��������� �������. ����������: Traekt.cpp
   SetGlobalState();     //�������� ��������� ����������� � ����������� �� ���������� �������
   FormResize(NULL); //������������
}
//---------------------------------------------------------------------------

/*��������� "����" - "��������� ���� ������". ��������� ������ ������������ ��
�����*/
void __fastcall TFGlavOkno::mnuSaveAsClick(TObject *Sender)
{
  bool RetB;
  int Ret;
  int RetPerezapis;
  int i;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;


  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);


  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);


  SaveDialog1->Title="���������� ������ � �����";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");

  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();

//��������, � �� �������� �� ��� ����� �� �������� ������
  AnsiString FS1,FS2;
//  bool IsWasToJeImya=false;


  if(RetB)
  {
    FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.SaveAs(FileNamePerezapis);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }
               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }

    Ret=Obz1.SaveAs(SaveDialog1->FileName);
    if(Ret!=1)
    {
      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
    }

  }

}
//---------------------------------------------------------------------------

//���������� �� ����� ������ ���.
void __fastcall TFGlavOkno::mnuIzvlechUVDClick(TObject *Sender)
{
  AnsiString FS1,FS2;
  int RetPerezapis;
  bool RetB;
  int Ret;
  int i;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;

  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

  SaveDialog1->Title="���������� ��� ��������!";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");
  SaveDialog1->FileName="uvd.txt";
  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {
    FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.IzvlechDataVFile(FileNamePerezapis,
                                        1);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }
               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }



     Ret=Obz1.IzvlechDataVFile(SaveDialog1->FileName,
                           1);
     if(Ret!=1)
     {
      MessageBox(NULL,"�� ������� ������� ������, ��� ��� �� ������� ������� ���� �� ������!", "������!", MB_OK);

     }
  }

}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::mnuIzvlechRBSClick(TObject *Sender)
{
  AnsiString FS1,FS2;
  int RetPerezapis;

  bool RetB;
  int Ret;
  int i;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;


  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

  SaveDialog1->Title="���������� RBS ��������!";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");
  SaveDialog1->FileName="rbs.txt";
  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {

    FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.IzvlechDataVFile(FileNamePerezapis,
                                        2);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }
               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }

     Ret=Obz1.IzvlechDataVFile(SaveDialog1->FileName,
                           2);
     if(Ret!=1)
     {
      MessageBox(NULL,"�� ������� ������� ������, ��� ��� �� ������� ������� ���� �� ������!", "������!", MB_OK);

     }

  }


}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::mnuIzvlechPS_SClick(TObject *Sender)
{
  AnsiString FS1,FS2;
  int RetPerezapis;
  bool RetB;
  int Ret;
  int i;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;

  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

  SaveDialog1->Title="���������� PS � S ��������!";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");
  SaveDialog1->FileName="ps_s.txt";
  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {

      FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.IzvlechDataVFile(FileNamePerezapis,
                                        3);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }
               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }


     Ret=Obz1.IzvlechDataVFile(SaveDialog1->FileName,
                           3);
     if(Ret!=1)
     {
      MessageBox(NULL,"�� ������� ������� ������, ��� ��� �� ������� ������� ���� �� ������!", "������!", MB_OK);

     }

  }


}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::mnuIzvlechSClick(TObject *Sender)
{
  AnsiString FS1,FS2;
  int RetPerezapis;
  bool RetB;
  int Ret;
  int i;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;

  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

  SaveDialog1->Title="���������� S ��������!";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");
  SaveDialog1->FileName="s.txt";
  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {

      FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.IzvlechDataVFile(FileNamePerezapis,
                                        4);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }
               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }


     Ret=Obz1.IzvlechDataVFile(SaveDialog1->FileName,
                           4);
     if(Ret!=1)
     {
      MessageBox(NULL,"�� ������� ������� ������, ��� ��� �� ������� ������� ���� �� ������!", "������!", MB_OK);

     }

  }



}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::mnuIzvlechPClick(TObject *Sender)
{
  AnsiString FS1,FS2;
  int RetPerezapis;
  bool RetB;
  int Ret;
  int i;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;

  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

  SaveDialog1->Title="���������� P ��������!";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");
  SaveDialog1->FileName="p.txt";
  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {
    FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);
      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.IzvlechDataVFile(FileNamePerezapis,
                                        5);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }
               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }



     Ret=Obz1.IzvlechDataVFile(SaveDialog1->FileName,
                           5);

     if(Ret!=1)
     {
      MessageBox(NULL,"�� ������� ������� ������, ��� ��� �� ������� ������� ���� �� ������!", "������!", MB_OK);

     }

  }



}
//---------------------------------------------------------------------------









void __fastcall TFGlavOkno::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{

  switch(Key)
  {
    case 0x2E:  //������ ������� Delete
      if(mnuClearSelection->Enabled)
      {
         mnuClearSelectionClick(NULL);
      }
    break;
    case 0x08:  //������ ������ BackSpace
      if(Shift.Contains(ssAlt)) //������ �������
      {

      };
    break;




  };


}
//---------------------------------------------------------------------------
/*���� - ������ - ��������(����������). ��������� �������.*/
void __fastcall TFGlavOkno::mnuPrintPictureClick(TObject *Sender)
{
  TPrintDialog *PrintDialog1;
  char PromStrka[2000],PromStrka2[200];
  GetCurrentDirectory(1999,PromStrka);

  int W,H,W1,H1,dW;
  int Tp,Lft;
  int SizeOblst;
  int TekRiad;  //������� ���
  int TekStlbz; //�������
  AnsiString StrkaOut;  //������������� ���������� - ��� ������ ������
  int i,Indx;                //��� ������� ������ ������� �����
  int Stk1,Hr1,Mn1; double Skn1; //����� ���������
  int Stk2,Hr2,Mn2; double Skn2; //����� ���������
  int KStrk=0;                   //����� ����� � �������� ������
  double Xpr[4],Ypr[4];
  double KoefX,KoefY,KoefX1,KoefY1;
  long Xpr1,Ypr1;
  bool  Ret;
  int Shirina;

  TPrinter *Prntr;
//��������� ������� ���� �� ������ ��������
  Prntr=Printer();

  if(!Prntr)
  {
    MessageBox(NULL, "�� ���� ����� �������!", "������!", MB_OK);
    return;
  }


  if(Prntr->Printers->Count==0)
  {
    MessageBox(NULL, "�� ���� ����� �������!", "������!", MB_OK);
    return;

  }
  PrintDialog1=new TPrintDialog(this);
  PrintDialog1->Collate=false;
  PrintDialog1->Copies=1;
  PrintDialog1->Ctl3D=false;
  PrintDialog1->FromPage=0;
  PrintDialog1->HelpContext=0;
  PrintDialog1->MaxPage=0;
  PrintDialog1->MinPage=0;
  PrintDialog1->PrintRange=prAllPages;
  PrintDialog1->PrintToFile=false;
  PrintDialog1->ToPage=0;



  Ret=PrintDialog1->Execute();
  if(!Ret)
  {
    SetCurrentDirectory(PromStrka);
    return;
  }

//��������� ������� ������
  W=Prntr->PageWidth;
  H=Prntr->PageHeight;
  dW=W/10;
  W1=W-dW;
  H1=H-dW;

  SizeOblst=W1;
  Tp=(H1-W1)*2/3+dW*3/4;
  Lft=dW*3/4;
  Prntr->Orientation=poPortrait;	
  Prntr->BeginDoc();
  VyvodSetkaPrinter(Prntr->Canvas,
                    SizeOblst,SizeOblst,
                    Lft,Tp,
                    LeftKm,BottomKm,
                    RightKm,TopKm);

//�������� ������� ���������, ���� ����� ����

   KoefX=((double)(RightKm-LeftKm))/(Image1->Width-1);
   KoefY=((double)(BottomKm-TopKm))/(Image1->Height-1);
   KoefX1=(SizeOblst-1)/((double)(RightKm-LeftKm));
   KoefY1=(SizeOblst-1)/((double)(BottomKm-TopKm));


   Xpr[0]=KoefX*(SVydel->Left)+LeftKm-0.01;
   Ypr[0]=KoefY*(SVydel->Top+SVydel->Height-1)+TopKm-0.01;
   Xpr[1]=KoefX*(SVydel->Left)+LeftKm-0.01;
   Ypr[1]=KoefY*(SVydel->Top)+TopKm+0.01;
   Xpr[2]=KoefX*(SVydel->Left+SVydel->Width-1)+LeftKm+0.01;
   Ypr[2]=KoefY*(SVydel->Top)+TopKm+0.01;
   Xpr[3]=KoefX*(SVydel->Left+SVydel->Width-1)+LeftKm+0.01;
   Ypr[3]=KoefY*(SVydel->Top+SVydel->Height-1)+TopKm-0.01;

   Prntr->Canvas->Pen->Style=psDash;
   Prntr->Canvas->Pen->Color=clBlack;

   if(VidOblasti==0)
   {
//�������� �����
     Xpr1=Okrugl(KoefX1*(Xpr[3]-LeftKm))+Lft;
     Ypr1=Okrugl(KoefY1*(Ypr[3]-TopKm))+Tp;
     Prntr->Canvas->MoveTo(Xpr1,Ypr1);
     for(i=0;i<4;i++)
     {
       Xpr1=Okrugl(KoefX1*(Xpr[i]-LeftKm))+Lft;
       Ypr1=Okrugl(KoefY1*(Ypr[i]-TopKm))+Tp;
       Prntr->Canvas->LineTo(Xpr1,Ypr1);
     }
   }else if(VidOblasti==2)
   {
     if(N_NotPriam>=4)
     {
       Xpr1=Okrugl(KoefX1*(XNotPriam[N_NotPriam-1]-LeftKm))+Lft;
       Ypr1=Okrugl(KoefY1*(YNotPriam[N_NotPriam-1]-TopKm))+Tp;
       Prntr->Canvas->MoveTo(Xpr1,Ypr1);
       for(i=0;i<N_NotPriam;i++)
       {
         Xpr1=Okrugl(KoefX1*(XNotPriam[i]-LeftKm))+Lft;
         Ypr1=Okrugl(KoefY1*(YNotPriam[i]-TopKm))+Tp;
         Prntr->Canvas->LineTo(Xpr1,Ypr1);
       }
     }
   }





//������� ������ �� �������
  Obz1.VyvodDataNaPrinter(Prntr->Canvas,
                    SizeOblst,SizeOblst,
                    Lft,Tp,
                    LeftKm,BottomKm,
                    RightKm,TopKm);

  PechatOblast(Prntr->Canvas,
               SizeOblst,SizeOblst,
               Lft,Tp);
//�������� ��� ������� �������
  Prntr->Canvas->Brush->Style=bsSolid;
  Prntr->Canvas->Pen->Style=psSolid;
  Prntr->Canvas->Brush->Color=clWhite;
  Prntr->Canvas->Pen->Color=clWhite;
  Prntr->Canvas->Pen->Mode=pmCopy;
  Prntr->Canvas->Rectangle(0,0,W,Tp-1);
  Prntr->Canvas->Rectangle(0,0,Lft-1,H);
  Prntr->Canvas->Rectangle(W-dW/4+1,0,W,H);
  Prntr->Canvas->Rectangle(0,Tp+W1+1,W,H);



//���� ����� ����������
  Prntr->Canvas->Pen->Style=psSolid;
  Prntr->Canvas->Pen->Color=clBlack;
  Prntr->Canvas->Brush->Style=bsClear;
  Prntr->Canvas->Rectangle(Lft,Tp,Lft+SizeOblst,Tp+SizeOblst);

//������ ����� �������� ��� ������
//������� ���� ����������������
  Prntr->Canvas->Font->Color=clBlack;
//  Prntr->Canvas->Font->Style=;
  Prntr->Canvas->Font->Name="MS Sans Serif";
  Prntr->Canvas->Font->Size=9;
  TekRiad=dW*3/4;

  StrkaOut=String("���� ����������������:")+Obz1.Date_Input+String("   ");
  Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
  TekStlbz=dW*3/4+Prntr->Canvas->TextWidth(StrkaOut+String("   ."));

//������� ���������� � ��������� �������
//������� ������� ��

sprintf(PromStrka2,"������ - %02d:%02d:%02d, ��������� -  %02d:%02d:%02d",
Obz1.CLChasyFirst,Obz1.CLMinutyFirst,Obz1.CLSekundyFirst,
Obz1.CLChasyCur,Obz1.CLMinutyCur,Obz1.CLSekundyCur);

//  Prntr->Canvas->TextOut(TekStlbz,TekRiad,PromStrka2); ���� ������ ���������..
  TekRiad+=Prntr->Canvas->TextHeight(StrkaOut+String(PromStrka2));
  StrkaOut=String("����������: ")+Obz1.Dop_Info;
  Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
  TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
  TekStlbz=dW*3/4+W1/2-Prntr->Canvas->TextWidth(Obz1.MestoIspytaniy)/2;
  Prntr->Canvas->TextOut(TekStlbz,TekRiad,Obz1.MestoIspytaniy);
  TekRiad+=1.3*Prntr->Canvas->TextHeight(Obz1.MestoIspytaniy);
  Prntr->Canvas->Pen->Style=psSolid;
  Prntr->Canvas->Pen->Color=clBlack;
  Prntr->Canvas->Pen->Mode=pmCopy;
  Prntr->Canvas->MoveTo(dW*3/4,TekRiad);
  Prntr->Canvas->LineTo(W-dW/4,TekRiad);

//������� �������� ������ �������
  StrkaOut="�������� �������: ";
  if(Obz1.IsCheckNumber)StrkaOut+=String("������ ������|");
  if(Obz1.IsCheckTime)StrkaOut+=String("�����|");
  if(Obz1.IsCheckVys)
  {
    if(Obz1.IskluchVys)StrkaOut+=String("������(����.)|");
    else StrkaOut+=String("������(���.)|");
  }
  if(Obz1.IsCheckAzmt)StrkaOut+=String("�������|");
  if(Obz1.IsCheckDlnst)StrkaOut+=String("���������|");
  if(Obz1.KanalP)StrkaOut+=String("P-����.|");
  if(Obz1.KanalS)StrkaOut+=String("S-����.|");
  if(Obz1.KanalPS)StrkaOut+=String("PS-����.");
  if(StrkaOut[StrkaOut.Length()]=='|')StrkaOut[StrkaOut.Length()]=' ';
  Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
  TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);

//������ ������� ������
  if(Obz1.IsCheckNumber)
  {
    StrkaOut="������ ������: ";
    for(i=0;i<Obz1.GetNumberOfSortedBorts();i++)
    {
      if(Obz1.GetSelectedInSortedBorts(i))
      {
         sprintf(PromStrka2,"%ld,",(long)(Obz1.GetNomerSelectedBort(i)));

         if(Prntr->Canvas->TextWidth(StrkaOut+String(PromStrka2))>=W1)
         {
             Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
             TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
             StrkaOut=String(PromStrka2);
             KStrk++;
             if(KStrk==4)break;
         }else{
             StrkaOut=StrkaOut+String(PromStrka2);
         }
      }
    }

    if(KStrk<4)
    {
      StrkaOut[StrkaOut.Length()]='.';
      Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
      TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
    }else{
      StrkaOut="������� ����� ������� ������ - �� ���� �� ���������!!!!";
      Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
      TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
    }
  }
//������ ���������� ������� ��� ��������� �������� �������
  StrkaOut="";
  if(Obz1.IsCheckTime)
  {
    GetTimeFromDouble(Obz1.Time1+0.00001,Stk1,Hr1,Mn1,Skn1);
    GetTimeFromDouble(Obz1.Time2-0.00001,Stk2,Hr2,Mn2,Skn2);
    sprintf(PromStrka2,"����� ��  %02d:%02d:%02d ��   %02d:%02d:%02d. ",
           Hr1,Mn1,(int)(Skn1),Hr2,Mn2,(int)(Skn2));
    StrkaOut+=String(PromStrka2);
  }
  if(Obz1.IsCheckVys)
  {
    if(Obz1.IskluchVys)
    {
      sprintf(PromStrka2, "������ ���� %ld ��� ���� %ld � . ",Obz1.Vys1,Obz1.Vys2);
    }else{
      sprintf(PromStrka2, "������ �� %ld �� %ld � . ",Obz1.Vys1,Obz1.Vys2);
    }
    StrkaOut=StrkaOut+String(PromStrka2);
  }
  if(StrkaOut.Length())
  {
    Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
    TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
  }

  StrkaOut="";
  if(Obz1.IsCheckAzmt)
  {
    if(Obz1.Azmt11>-1)
    {
      if(Obz1.Azmt21>-1)
      {
        sprintf(PromStrka2, "�������: �� %.02lf �� %.02lf ��. ��� �� %.02lf �� %.02lf ��, ",
                Obz1.Azmt11,Obz1.Azmt12,Obz1.Azmt21,Obz1.Azmt22);
        StrkaOut=PromStrka2;
      }else{
        sprintf(PromStrka2, "�������: �� %.02lf �� %.02lf ��. ",
               Obz1.Azmt11,Obz1.Azmt12);
        StrkaOut=PromStrka2;
      }
    }else{
      if(Obz1.Azmt21>-1)
      {
        sprintf(PromStrka2, "�������: �� %.02lf �� %.02lf ��.  ",
        Obz1.Azmt21,Obz1.Azmt22);
        StrkaOut=PromStrka2;
      }
    }
  }
  if(Obz1.IsCheckDlnst)
  {
     sprintf(PromStrka2, "���������: �� %.02lf �� %.02lf ��. ",
               Obz1.Dlnst1,Obz1.Dlnst2);
     StrkaOut+=PromStrka2;
  }
  if(StrkaOut.Length()!=0)
  {
     Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);
     TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
  }

//������� ������ � ������������
  if(Obz1.FIO_User.Length()==0)
  {
     if(Obz1.Podr_User.Length()==0)
     {
       StrkaOut=String("   ");
     }else{
       StrkaOut=String("���������: ________________________(")+
            String(Obz1.Podr_User)+String(")");
     }
  }else{
     if(Obz1.Podr_User.Length()==0)
     {
       StrkaOut=String("���������: ")+Obz1.FIO_User+
                String(" (_____)");

     }else{
        StrkaOut=String("���������: ")+Obz1.FIO_User+String(" (")+
        String(Obz1.Podr_User)+String(")");
     }
  }
  TekRiad=Tp-Prntr->Canvas->TextHeight(StrkaOut)*2;
  Prntr->Canvas->TextOut(dW*3/4,TekRiad,StrkaOut);


  Prntr->Canvas->Font->Color=clBlack;
 //  Prntr->Canvas->Font->Style=;
  Prntr->Canvas->Font->Name="MS Sans Serif";
  Prntr->Canvas->Font->Size=9;
  TekRiad=Tp+SizeOblst+Prntr->Canvas->TextHeight("9");

//������ ���������� ������ ���
  if(IzFindTochnost)
  {
//������� ������
     if(StepenPolyot<7)
     {
        sprintf(PromStrka2,"���������� ������ ��������. ������� ������������: %d",StepenPolyot);
     }else{
        sprintf(PromStrka2,"���������� ������ ��������. ������ �� �������� ���������");
     }
     Prntr->Canvas->TextOut(Lft,TekRiad,PromStrka2);
     TekRiad+=Prntr->Canvas->TextHeight(PromStrka2);
     StrkaOut=String("������� ����:");
     if(IsWasP_T)StrkaOut=StrkaOut+String(" P");
     if(IsWasS_T)StrkaOut=StrkaOut+String(" S");
     if(IsWasPS_T)StrkaOut=StrkaOut+String(" PS");
     StrkaOut=StrkaOut+String("      ����:");
     for(i=0;i<N_of_NBT;i++)StrkaOut=StrkaOut+String(" ")+String((int)NBT[i]);
     Prntr->Canvas->TextOut(Lft,TekRiad,StrkaOut);
     TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
     sprintf(PromStrka2,"������� %d ��������. ���_�= %.0lf �, ���_�=%.0lf ���.",
             Obz1.ChisloVydelOtschetov, T_SKO_D, T_SKO_A);
     Prntr->Canvas->TextOut(Lft,TekRiad,PromStrka2);
     TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);
     sprintf(PromStrka2,"����: %.01lf ��, ������� ��������: %.0lf ��/�.",
             KursPolyota,SredSkorostPolyota);
     Prntr->Canvas->TextOut(Lft,TekRiad,PromStrka2);
     TekRiad+=Prntr->Canvas->TextHeight(StrkaOut);

     if(StepenPolyot==0)
     {
//������� ������� �������� �������� � ����������
       sprintf(PromStrka2,"������� ��������: �������=%.02lf ��, ���������=%.02lf ��",
          SredA,SredD);
       Prntr->Canvas->TextOut(Lft,TekRiad,PromStrka2);
       TekRiad+=Prntr->Canvas->TextHeight(PromStrka2);
     }
  }

  TekRiad+=Prntr->Canvas->TextHeight("1");
  Shirina=SizeOblst*20/768;
  Prntr->Canvas->Pen->Style=psSolid;
  Prntr->Canvas->Pen->Color=(TColor)RGB(Obz1.cPechatPK,Obz1.cPechatPK,Obz1.cPechatPK);
  Prntr->Canvas->Brush->Color=(TColor)RGB(Obz1.cPechatPK,Obz1.cPechatPK,Obz1.cPechatPK);
  Prntr->Canvas->Brush->Style=bsSolid;
  if(Obz1.KanalP)
    Prntr->Canvas->Ellipse(Lft,TekRiad,Lft+Shirina,TekRiad+Shirina);
  TekStlbz=Lft+Shirina;
  Prntr->Canvas->Brush->Style=bsClear;

  if(Obz1.KanalP)
    Prntr->Canvas->TextOut(TekStlbz,TekRiad," - ������� �� ");
  TekStlbz+=Prntr->Canvas->TextWidth(" - ������� �� ")+Shirina;
    Prntr->Canvas->Pen->Width=5;
  Prntr->Canvas->Brush->Color=(TColor)RGB(Obz1.cPechatVK,Obz1.cPechatVK,Obz1.cPechatVK);
  Prntr->Canvas->Pen->Color=(TColor)RGB(Obz1.cPechatVK,Obz1.cPechatVK,Obz1.cPechatVK);
  Prntr->Canvas->Brush->Style=bsSolid;
  if(Obz1.KanalS)
  Prntr->Canvas->Ellipse(TekStlbz,TekRiad,TekStlbz+Shirina,TekRiad+Shirina);

  TekStlbz+=Shirina;
  Prntr->Canvas->Brush->Style=bsClear;
  if(Obz1.KanalS)
    Prntr->Canvas->TextOut(TekStlbz,TekRiad," - ������� �� ");
  TekStlbz+=Prntr->Canvas->TextWidth(" - ������� �� ")+Shirina;
  Prntr->Canvas->Brush->Color=(TColor)RGB(Obz1.cPechatObjed,Obz1.cPechatObjed,Obz1.cPechatObjed);
  Prntr->Canvas->Pen->Color=(TColor)RGB(Obz1.cPechatObjed,Obz1.cPechatObjed,Obz1.cPechatObjed);
  Prntr->Canvas->Brush->Style=bsSolid;
  if(Obz1.KanalPS)
    Prntr->Canvas->Ellipse(TekStlbz,TekRiad,TekStlbz+Shirina,TekRiad+Shirina);
  Prntr->Canvas->Pen->Width=1;
  TekStlbz+=Shirina;
  Prntr->Canvas->Brush->Style=bsClear;
  if(Obz1.KanalPS)
  Prntr->Canvas->TextOut(TekStlbz,TekRiad," - ������������ ��+��");

  Prntr->EndDoc();

  SetCurrentDirectory(PromStrka);
  delete PrintDialog1;
}
//---------------------------------------------------------------------------

/*������� ���� ���������.*/
void __fastcall TFGlavOkno::mnuOptionClick(TObject *Sender)
{
/*��������� ���� �������� "���������".
����� FNastroiki ����������� � ������ Nastroiki.
 ������� �� Obz1 ���� �������� ���� ����������� ������� ��������.
 ����� Obz1 ���������� ����� � �����.
 ����� ���� ��������� �������� ���� FNastroiki.
 ����� ���� �� ���� ������ ��� �������� FNastroiki, ��
 ���������� �������� ���������� ������� Obz1 � ����� FGlavOkno.
 ���� ���� � ���� FNastroiki ���������, �� ���������� ������ ����������� �������
 ����������� ���������������� ������
*/
   char Strka[50];
   int i;

  int Chislo1,Chislo2,Ret;

//�������� �������� �����
    Ret=sscanf(EVysota1->Text.c_str(),"%d",&Chislo1);
    if(Ret!=1)
    {
      MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
      EVysota1->SetFocus();
      return;
    }
    Ret=sscanf(EVysota2->Text.c_str(),"%d",&Chislo2);
    if(Ret!=1)
    {
      MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
      EVysota2->SetFocus();
      return;
    }



   FNastroiki->HelpFile=HelpFileName;
   FNastroiki->HelpContext=4040;

/*�������� ������ �� Obz1 � ���������� ���� FNastroiki*/
   FNastroiki->IsCheckTime->Enabled=true;
 //������� ��������� ��� ��������� �� OBZORY_1
//   FNastroiki->IsAORL_1AS->Checked=Obz1.IsAORL_1AS;
   FNastroiki->IsVisNumber->Checked=Obz1.IsVisNumber;
   FNastroiki->IsVisTime->Checked=Obz1.IsVisTime;
   FNastroiki->IsVisTime->Enabled=Obz1.IsVisNumber;

 //������ ��� �������� �������
   FNastroiki->IsCheckNumber->Checked=Obz1.IsCheckNumber;
   FNastroiki->IsCheckTime->Checked=Obz1.IsCheckTime;
   FNastroiki->IsCheckVys->Checked=Obz1.IsCheckVys;
   FNastroiki->IskluchVys->Checked=Obz1.IskluchVys;
   if(FNastroiki->IsCheckVys->Checked)
   {
      FNastroiki->IskluchVys->Enabled=true;
   }else{
      FNastroiki->IskluchVys->Enabled=false;
   }

   FNastroiki->IsCheckDlnst->Checked=Obz1.IsCheckDlnst;
   if(FNastroiki->IsCheckDlnst->Checked)
   {
      FNastroiki->Dlnst1->Enabled=true; FNastroiki->Dlnst2->Enabled=true;
      FNastroiki->Dlnst1->Color=clWindow;       FNastroiki->Dlnst2->Color=clWindow;
   }else{
      FNastroiki->Dlnst1->Enabled=false; FNastroiki->Dlnst2->Enabled=false;
      FNastroiki->Dlnst1->Color=clBtnFace;       FNastroiki->Dlnst2->Color=clBtnFace;

   }

   if(Obz1.Dlnst1<-0.002||Obz1.Dlnst2<-0.002)
   {
     FNastroiki->Dlnst1->Text="";
     FNastroiki->Dlnst2->Text="";
   }else{
     if(Obz1.Dlnst1<0)Obz1.Dlnst1=0;
     if(Obz1.Dlnst2<0)Obz1.Dlnst2=0;
     sprintf(Strka,"%.02lf",Obz1.Dlnst1);
     FNastroiki->Dlnst1->Text=Strka;
     sprintf(Strka,"%.02lf",Obz1.Dlnst2);
     FNastroiki->Dlnst2->Text=Strka;
   }

   FNastroiki->IsCheckAzmt->Checked=Obz1.IsCheckAzmt;
   if(FNastroiki->IsCheckAzmt->Checked)
   {
      FNastroiki->Azmt11->Enabled=true; FNastroiki->Azmt12->Enabled=true;
      FNastroiki->Azmt11->Color=clWindow;       FNastroiki->Azmt12->Color=clWindow;
      FNastroiki->Azmt21->Enabled=true; FNastroiki->Azmt22->Enabled=true;
      FNastroiki->Azmt21->Color=clWindow;       FNastroiki->Azmt22->Color=clWindow;
   }else{
      FNastroiki->Azmt11->Enabled=false; FNastroiki->Azmt12->Enabled=false;
      FNastroiki->Azmt11->Color=clBtnFace;       FNastroiki->Azmt12->Color=clBtnFace;
      FNastroiki->Azmt21->Enabled=false; FNastroiki->Azmt22->Enabled=false;
      FNastroiki->Azmt21->Color=clBtnFace;       FNastroiki->Azmt22->Color=clBtnFace;

   }

   if(Obz1.Azmt11<-0.9||Obz1.Azmt12<-0.9)
   {
     FNastroiki->Azmt11->Text="";
     FNastroiki->Azmt12->Text="";
   }else{
     if(Obz1.Azmt11<0)Obz1.Azmt11=0;
     if(Obz1.Azmt12<0)Obz1.Azmt12=0;

     sprintf(Strka,"%.02lf",Obz1.Azmt11);
     FNastroiki->Azmt11->Text=Strka;
     sprintf(Strka,"%.02lf",Obz1.Azmt12);
     FNastroiki->Azmt12->Text=Strka;
   }

   if(Obz1.Azmt21<-0.9||Obz1.Azmt22<-0.9)
   {
     FNastroiki->Azmt21->Text="";
     FNastroiki->Azmt22->Text="";
   }else{
     if(Obz1.Azmt21<0)Obz1.Azmt21=0;
     if(Obz1.Azmt22<0)Obz1.Azmt22=0;

     sprintf(Strka,"%.02lf",Obz1.Azmt21);
     FNastroiki->Azmt21->Text=Strka;
     sprintf(Strka,"%.02lf",Obz1.Azmt22);
     FNastroiki->Azmt22->Text=Strka;
   }
   FNastroiki->KanalP->Checked=Obz1.KanalP;
   FNastroiki->KanalS->Checked=Obz1.KanalS;
   FNastroiki->KanalPS->Checked=Obz1.KanalPS;

//��������� ������
   if(Obz1.RejimUVD)
   {
     if(Obz1.RejimRBS)
     {
       FNastroiki->RGRejim->ItemIndex=0;
     }else{
       FNastroiki->RGRejim->ItemIndex=1;
     }
   }else{
     FNastroiki->RGRejim->ItemIndex=2;
   }


   FNastroiki->cbIsCheckSignalPK->Checked=Obz1.IsCheckSignalPK;
   sprintf(Strka, "%le",Obz1.dfMinSignalPK);
   FNastroiki->eMinSignalPK->Text=Strka;

//�������� �� ������
   FNastroiki->VydelyatSevera->Checked=Obz1.VydelyatSevera;
   FNastroiki->DiametrMetok->Value=Obz1.DiametrMetok;
   FNastroiki->IsAnimNomer->Checked=Obz1.IsAnimNomer;
   FNastroiki->IsAnimVysota->Checked=Obz1.IsAnimVysota;
   FNastroiki->IsAnimTime->Checked=Obz1.IsAnimTime;
   FNastroiki->IsHvost->Checked=Obz1.IsHvost;
   FNastroiki->DlinaHvosta->Value=Obz1.DlinaHvosta;
   if(FNastroiki->IsHvost->Checked)
   {
     FNastroiki->DlinaHvosta->Enabled=true;
     FNastroiki->DlinaHvosta->Color=clWindow;
   }else{
     FNastroiki->DlinaHvosta->Enabled=false;
     FNastroiki->DlinaHvosta->Color=clBtnFace;
   }


   FNastroiki->A11=Obz1.Azmt11;
   FNastroiki->A12=Obz1.Azmt12;
   FNastroiki->A21=Obz1.Azmt21;
   FNastroiki->A22=Obz1.Azmt22;
   FNastroiki->D1=Obz1.Dlnst1;
   FNastroiki->D2=Obz1.Dlnst2;


   FNastroiki->VysotaRLS->Value=Obz1.VysotaRLS;

//�� �������� ����
   if(Obz1.CLSutkiCur==0)
   {
     FNastroiki->IsCheckTime->Enabled=true;
   }else{
  //�������� ��������� �������
     double TmPr1,TmPr2;
     TmPr1=Obz1.CLSutkiFirst*24.0+Obz1.CLChasyFirst+Obz1.CLMinutyFirst/60.0+Obz1.CLSekundyFirst/3600.0;
     TmPr2=Obz1.CLSutkiCur*24.0+Obz1.CLChasyCur+Obz1.CLMinutyCur/60.0+Obz1.CLSekundyCur/3600.0;
     if(TmPr2-TmPr1>23+59/60.0+58.9/3600.0)
     {
       FNastroiki->IsCheckTime->Enabled=false;
     }else{
       FNastroiki->IsCheckTime->Enabled=true;
     }
   }

     FNastroiki->RBNapravlenia->Enabled=Obz1.IsWasPrivyazka;
     FNastroiki->RBNapravlenia->ItemIndex=Obz1.Napravlenie;
     if(Obz1.IsWasPrivyazka)
     {
       FNastroiki->GBMinMaxAndMaxMin->Enabled=true;
       FNastroiki->FindRadialTraekt->Enabled=true;
       FNastroiki->FindForP_PS->Enabled=true;
       FNastroiki->FindForS_PS->Enabled=true;
       FNastroiki->MaxMinD->Color=clWindow;
       FNastroiki->MinMaxD->Color=clWindow;
     }else{
       FNastroiki->GBMinMaxAndMaxMin->Enabled=false;
       FNastroiki->FindRadialTraekt->Checked=false;
       FNastroiki->FindRadialTraekt->Enabled=false;
       FNastroiki->FindForP_PS->Enabled=false;
       FNastroiki->FindForS_PS->Enabled=false;
       FNastroiki->MaxMinD->Color=clBtnFace;
       FNastroiki->MinMaxD->Color=clBtnFace;
     }
     FNastroiki->FindRadialTraekt->Checked=Obz1.FindRadialTraekt;
     if(Obz1.FindForP_PS)FNastroiki->FindForP_PS->Checked=true;
       else FNastroiki->FindForS_PS->Checked=true;

     sprintf(Strka,"%.2lf",Obz1.MaxMinD);
     FNastroiki->MaxMinD->Text=Strka;
     sprintf(Strka,"%.2lf",Obz1.MinMaxD);
     FNastroiki->MinMaxD->Text=Strka;



     FNastroiki->IsIstinnyRightDopData->Checked=Obz1.IsIstinnyRightDopData;
     FNastroiki->IsLojnyyInfoVyvod->Checked=Obz1.IsLojnyyInfoVyvod;


//��������� ������ � ������
     FNastroiki->SOblastCol->Brush->Color=Obz1.OblastCol;
     FNastroiki->SKolca1Col->Brush->Color=Obz1.Kolca1Col;
     FNastroiki->SKolca2Col->Brush->Color=Obz1.Kolca2Col;
     FNastroiki->SRad90Col->Brush->Color=Obz1.Rad90Col;
     FNastroiki->SRad30Col->Brush->Color=Obz1.Rad30Col;
     FNastroiki->SRamkaCol->Brush->Color=Obz1.RamkaCol;
     FNastroiki->S_P_Col->Brush->Color=Obz1.P_Col;
     FNastroiki->S_S_Col->Brush->Color=Obz1.S_Col;
     FNastroiki->S_PS_Col->Brush->Color=Obz1.PS_Col;
     FNastroiki->S_RS_UVD_Col->Brush->Color=Obz1.RS_UVD_Col;
     FNastroiki->S_RS_RBS_Col->Brush->Color=Obz1.RS_RBS_Col;
     FNastroiki->S_RS_P_Col->Brush->Color=Obz1.RS_P_Col;
     FNastroiki->S_Text_Col->Brush->Color=Obz1.Text_Col;

//��������� ����� ���������
     FNastroiki->MestoIspytaniy->Text=Trim((AnsiString)(Obz1.MestoIspytaniy));

//��������� ������ ����������� ���������� ��� �������������� ��������
     FNastroiki->KO_UVD->Text=""; FNastroiki->KO_RBS->Text="";

     for(i=0;i<Obz1.N_of_NomBortKO_UVD;i++)
     {
       if(i==0)
       {
         FNastroiki->KO_UVD->Text=String((int)(Obz1.NomBortKO_UVD[i]));
       }else{
         FNastroiki->KO_UVD->Text=FNastroiki->KO_UVD->Text+String(", ")+
                  String((int)(Obz1.NomBortKO_UVD[i]));
       }
     }
     for(i=0;i<Obz1.N_of_NomBortKO_RBS;i++)
     {
       if(i==0)
       {
         FNastroiki->KO_RBS->Text=String((int)(Obz1.NomBortKO_RBS[i]));
       }else{
         FNastroiki->KO_RBS->Text=FNastroiki->KO_RBS->Text+String(", ")+
                  String((int)(Obz1.NomBortKO_RBS[i]));
       }
     }

//������ ����������� ���� ��� ������ ���� ������
     sprintf(Strka,"%.2lf",Obz1.dRazmerOknaDlyaZonyObzora);
     FNastroiki->E_RazmerOkna->Text=String(Strka);

//��������� 10.09.2008
//���� ������
     FNastroiki->CSEPrintPK->Value=Obz1.cPechatPK+1;
     FNastroiki->CSEPrintVK->Value=Obz1.cPechatVK+1;
     FNastroiki->CSEPrintO->Value=Obz1.cPechatObjed+1;
     FNastroiki->CBSetMaxDalnGOST->Checked=Obz1.isSetMaxDalnGOST;

/*��������� ����������� ���� FNastroiki*/

//��������� ���� ���������
   FNastroiki->ShowModal();
   if(FNastroiki->Canceled)
   {
     return;
   }


//������ ����� ����, ��� ���� ������� - ������� ��� �������� �������
//  Obz1.IsAORL_1AS=FNastroiki->IsAORL_1AS->Checked;
  Obz1.IsVisNumber=FNastroiki->IsVisNumber->Checked;
  Obz1.IsVisTime=FNastroiki->IsVisTime->Checked;
  Obz1.IsCheckNumber=FNastroiki->IsCheckNumber->Checked;
  Obz1.IsCheckTime=FNastroiki->IsCheckTime->Checked;
  Obz1.IsCheckVys=FNastroiki->IsCheckVys->Checked;
  Obz1.IskluchVys=FNastroiki->IskluchVys->Checked;
  Obz1.IsCheckDlnst=FNastroiki->IsCheckDlnst->Checked;
  Obz1.IsCheckAzmt=FNastroiki->IsCheckAzmt->Checked;
  Obz1.KanalP=FNastroiki->KanalP->Checked;
  Obz1.KanalS=FNastroiki->KanalS->Checked;
  Obz1.KanalPS=FNastroiki->KanalPS->Checked;
  Obz1.IsCheckSignalPK=FNastroiki->cbIsCheckSignalPK->Checked;
  sscanf(FNastroiki->eMinSignalPK->Text.c_str(),"%lf",&(Obz1.dfMinSignalPK));


  Obz1.VydelyatSevera=FNastroiki->VydelyatSevera->Checked;
  Obz1.DiametrMetok=FNastroiki->DiametrMetok->Value;
  Obz1.IsAnimNomer=FNastroiki->IsAnimNomer->Checked;
  Obz1.IsAnimVysota=FNastroiki->IsAnimVysota->Checked;
  Obz1.IsAnimTime=FNastroiki->IsAnimTime->Checked;
  Obz1.IsHvost=FNastroiki->IsHvost->Checked;
  Obz1.DlinaHvosta=FNastroiki->DlinaHvosta->Value;
//������� ������ �������� �������
  Obz1.Dlnst1=FNastroiki->D1;  Obz1.Dlnst2=FNastroiki->D2;
  Obz1.Azmt11=FNastroiki->A11;  Obz1.Azmt12=FNastroiki->A12;
  Obz1.Azmt21=FNastroiki->A21;  Obz1.Azmt22=FNastroiki->A22;

  Obz1.VysotaRLS=FNastroiki->VysotaRLS->Value;

  if(FNastroiki->RGRejim->ItemIndex==0)
  {
    Obz1.RejimUVD=Obz1.RejimRBS=true;
  }else if(FNastroiki->RGRejim->ItemIndex==1)
  {
    Obz1.RejimUVD=true;Obz1.RejimRBS=false;
  }else{
    Obz1.RejimUVD=false;Obz1.RejimRBS=true;
  }


//��������� ����������� �������� ����
//��� ������� ������� ������ � ����������� �� ��������
  if(Obz1.IsCheckNumber)
  {
     BSortSpisokBortov->Enabled=true;
     CBSpisokBortov->Enabled=true;
     CBSpisokBortov->Color=clWindow;
  }else{
     BSortSpisokBortov->Enabled=false;
     CBSpisokBortov->Enabled=false;
     CBSpisokBortov->Color=clBtnFace;
  }

//��������� ����������� �������� ����
//��� ������� ������� � ����������� �� ��������
  if(Obz1.IsCheckTime)
  {
    DTP1->Enabled=true;    DTP2->Enabled=true;
    CSE_Sutki1->Enabled=true; CSE_Sutki2->Enabled=true;
    DTP1->Color=clWindow;    DTP2->Color=clWindow;
    CSE_Sutki1->Color=clWindow; CSE_Sutki2->Color=clWindow;
  }else{
    DTP1->Enabled=false;    DTP2->Enabled=false;
    CSE_Sutki1->Enabled=false; CSE_Sutki2->Enabled=false;
    DTP1->Color=clBtnFace;    DTP2->Color=clBtnFace;
    CSE_Sutki1->Color=clBtnFace; CSE_Sutki2->Color=clBtnFace;
  }

//��������� ����������� �������� ����
//��� ������� ������ � ����������� �� ��������
  if(Obz1.IsCheckVys)
  {
    EVysota1->Enabled=true;    EVysota2->Enabled=true;
    EVysota1->TabStop=true;   EVysota2->TabStop=true;
    EVysota1->ReadOnly=false;   EVysota2->ReadOnly=false;
    EVysota1->Color=clWhite;  EVysota2->Color=clWhite;
    EVysota1->Font->Color=clBlack;    EVysota2->Font->Color=clBlack;

  }else{
    EVysota1->Enabled=true;    EVysota2->Enabled=true;
    EVysota1->TabStop=false;   EVysota2->TabStop=false;
    EVysota1->ReadOnly=true;   EVysota2->ReadOnly=true;
    EVysota1->Color=clBtnFace;  EVysota2->Color=clBtnFace;
    EVysota1->Font->Color=clBtnFace;    EVysota2->Font->Color=clBtnFace;

  }

  Obz1.Napravlenie=FNastroiki->RBNapravlenia->ItemIndex;

  Obz1.IsIstinnyRightDopData=FNastroiki->IsIstinnyRightDopData->Checked;
  Obz1.IsLojnyyInfoVyvod=FNastroiki->IsLojnyyInfoVyvod->Checked;

  Obz1.FindRadialTraekt=FNastroiki->FindRadialTraekt->Checked;
  Obz1.FindForP_PS=FNastroiki->FindForP_PS->Checked;
  Obz1.MaxMinD=FNastroiki->MaxMinD1;
  Obz1.MinMaxD=FNastroiki->MinMaxD1;

  Obz1.OblastCol=FNastroiki->SOblastCol->Brush->Color;
  Obz1.Kolca1Col=FNastroiki->SKolca1Col->Brush->Color;
  Obz1.Kolca2Col=FNastroiki->SKolca2Col->Brush->Color;
  Obz1.Rad90Col=FNastroiki->SRad90Col->Brush->Color;
  Obz1.Rad30Col=FNastroiki->SRad30Col->Brush->Color;
  Obz1.RamkaCol=FNastroiki->SRamkaCol->Brush->Color;
  Obz1.P_Col=FNastroiki->S_P_Col->Brush->Color;
  Obz1.S_Col=FNastroiki->S_S_Col->Brush->Color;
  Obz1.PS_Col=FNastroiki->S_PS_Col->Brush->Color;
  Obz1.RS_UVD_Col=FNastroiki->S_RS_UVD_Col->Brush->Color;
  Obz1.RS_RBS_Col=FNastroiki->S_RS_RBS_Col->Brush->Color;
  Obz1.RS_P_Col=FNastroiki->S_RS_P_Col->Brush->Color;
  Obz1.Text_Col=FNastroiki->S_Text_Col->Brush->Color;



  Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
  Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
  Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
  Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
  Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;

  strcpy(Obz1.MestoIspytaniy,FNastroiki->MestoIspytaniy->Text.c_str());


//��������� ����������� �������
  GetMassivZelyhChiselFromStrka(FNastroiki->KO_UVD->Text.c_str(),
       Obz1.NomBortKO_UVD,&Obz1.N_of_NomBortKO_UVD,10);
  GetMassivZelyhChiselFromStrka(FNastroiki->KO_RBS->Text.c_str(),
       Obz1.NomBortKO_RBS,&Obz1.N_of_NomBortKO_RBS,10);


//  GetTickCount
//��������� ������� ����������� ����
   if(1!=sscanf(FNastroiki->E_RazmerOkna->Text.c_str(),"%lf",
       &(Obz1.dRazmerOknaDlyaZonyObzora)))
   {
     Obz1.dRazmerOknaDlyaZonyObzora=10.0;
   }

     Obz1.cPechatPK=FNastroiki->CSEPrintPK->Value-1;
     Obz1.cPechatVK=FNastroiki->CSEPrintVK->Value-1;
     Obz1.cPechatObjed=FNastroiki->CSEPrintO->Value-1;
     Obz1.isSetMaxDalnGOST=FNastroiki->CBSetMaxDalnGOST->Checked;   

/*
//�������� ������, ���� ������ ����� ���������
  if(OldRadius!=Obz1.DiametrMetok)
  {
    VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
    Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
  }
  */


//    VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
//    Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);



/*���� ���-�� ���������, �� �������� ������� B_OKClick*/
   if(B_OK->Enabled)
   {
     B_OKClick(NULL);
   }else{
      RisovPanel->Color=Obz1.OblastCol;
      SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
          SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
          SetkaRad30Col=Obz1.Rad30Col;
     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);

   }
}
//---------------------------------------------------------------------------





void __fastcall TFGlavOkno::BZonaSClick(TObject *Sender)
{
  int i;
  int iRet;
  long Tek;
  ODIN_BORT_1 ob1;
  char Strka[1000];
  char *HlpFileNameC;
  long nomerBortaIndxBrt;       //����� ����� + ������ ����� * 100000
  int ON_of_S=0;

  HlpFileNameC=(char*)malloc(sizeof(char)*HelpFileName.Length()+1);
  strcpy(HlpFileNameC,HelpFileName.c_str());
  SetHelpFileName(HlpFileNameC);
  free(HlpFileNameC);

  //��������� ������ ����������� ����
  SetRazmerOknaDlyaZony(Obz1.dRazmerOknaDlyaZonyObzora);


  //��������� ������� ��� ������ � ������
  if(Obz1.Date_Input.Trim().Length()==0)
  {
    SetDataDoc("1");
  }else{
    strcpy(Strka,Obz1.Date_Input.c_str());
    SetDataDoc(Strka);
  }

  if(Obz1.Dop_Info.Trim().Length()==0)
  {
    SetDannyeDoc("1");
  }else{
    strcpy(Strka,Obz1.Dop_Info.c_str());
    SetDannyeDoc(Strka);
  }



//�������� �������, ��� �� ������ � �������� �������
//������� ���������� �������� � ������
  if(Obz1.ChisloVydelOtschetov==0)
  {
       MessageBox(NULL,"��� ���������� �������� ���������� ������",
                       "������!",MB_OK);
       return;
  }else   if(Obz1.ChisloVydelOtschetov<10)
  {
       MessageBox(NULL,"� ��������� ������� ����� ���� �����",
                       "������!",MB_OK);
       return;
  }



  Tek=Obz1.tnGetFirstOtschetKanalAndRejim(2,0,2);
  while(Tek>=0)
  {
    iRet=Obz1.GetOdin_Bort(Tek,&ob1, &nomerBortaIndxBrt);
    if(iRet<1)
    {
       MessageBox(NULL,"������� �������, �� ������������� ������. ������ ���� ������ ���������� ",
                       "������!",MB_OK);
       return;
    }
    ON_of_S++;
    Tek=Obz1.tnGetNextOtschetKanalAndRejim(Tek,2,0,2);

  }


  if(ON_of_S<=0)
  {
       MessageBox(NULL,"��� ���������� �������� ���������� ������",
                       "������!",MB_OK);
       return;
  }
  if(ON_of_S<10)
  {
       MessageBox(NULL,"� ��������� ������� ����� ���� �����",
                       "������!",MB_OK);
       return;
  }

  if(Obz1.isSetMaxDalnGOST)
  {
        FMaxDalnostGOST->CSpinEdit1->Value=Obz1.iMaxDalnGOST_VK;
        FMaxDalnostGOST->ShowModal();
        Obz1.iMaxDalnGOST_VK=FMaxDalnostGOST->CSpinEdit1->Value;
        InitializeM(Obz1.TO,2,Obz1.iMaxDalnGOST_VK);
   }else{
        InitializeM(Obz1.TO,2,450.0);
   }

  Tek=Obz1.tnGetFirstOtschetKanalAndRejim(2,0,2);

  while(Tek>=0)
  {

    if(Tek==360)
    {
      Tek=Obz1.GetNextFilterOB(Tek,true);
      continue;
    }
    Obz1.GetOdin_Bort(Tek,&ob1, &nomerBortaIndxBrt);
    if(ob1.Status&KANAL_S_STATUS)
    {
      if(Obz1.IsWasPrivyazka)
      {
        AddOtchet(nomerBortaIndxBrt,ob1.Azmt,ob1.Dlnst,ob1.Time,
        ob1.IstVysota);
      }else{
        if(ob1.NomBort>0)
        {
           AddOtchet(ob1.NomBort,ob1.Azmt,ob1.Dlnst,ob1.Time, ob1.OprVysota);
        }else{
           MessageBox(NULL,"� ��������� ������� ���� ������� � �����.������� �����",
                       "������!",MB_OK);
           return;

        }
      }
    }
    Tek=Obz1.tnGetNextOtschetKanalAndRejim(Tek,2,0,2);
  }

//��������� ���������
  ZakonchitObrabotku(); //������ ��������� ���������� �� ������ matv_dovesok.dll


}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::BZonaPClick(TObject *Sender)
{
  int i;
  long Tek;
  ODIN_BORT_1 ob1;
  char Strka[1000];
  int ON_of_P=0;
  char *HlpFileNameC;
  long nomerBortaIndxBrt;
  int iRet;

//��������� ���� � ����� ������
  HlpFileNameC=(char*)malloc(sizeof(char)*HelpFileName.Length()+1);
  strcpy(HlpFileNameC,HelpFileName.c_str());
  SetHelpFileName(HlpFileNameC);
  free(HlpFileNameC);

//��������� ������ ����������� ����
  SetRazmerOknaDlyaZony(Obz1.dRazmerOknaDlyaZonyObzora);

//��������� ������� ��� ������ � ������
  if(Obz1.Date_Input.Trim().Length()==0)
  {
    SetDataDoc("1");
  }else{
    strcpy(Strka,Obz1.Date_Input.c_str());
    SetDataDoc(Strka);
  }

  if(Obz1.Dop_Info.Trim().Length()==0)
  {
    SetDannyeDoc("1");
  }else{
    strcpy(Strka,Obz1.Dop_Info.c_str());
    SetDannyeDoc(Strka);
  }


//������� ���������� �������� � ������
  if(Obz1.ChisloVydelOtschetov==0)
  {
       MessageBox(NULL,"��� ���������� �������� ���������� ������",
                       "������!",MB_OK);
       return;
  }else   if(Obz1.ChisloVydelOtschetov<10)
  {
       MessageBox(NULL,"� ��������� ������� ����� ���� �����",
                       "������!",MB_OK);
       return;
  }

  Tek=Obz1.tnGetFirstOtschetKanalAndRejim(1,0,2);
  while(Tek>=0)
  {
    iRet=Obz1.GetOdin_Bort(Tek,&ob1);
    if(iRet<=0)
    {
      MessageBox(NULL,"������� �������, �� ������������� ������. ������ ���� ������ ���������� ",
                       "������!",MB_OK);
       return;
    }
    ON_of_P++;
    Tek=Obz1.tnGetNextOtschetKanalAndRejim(Tek,1,0,2);
  }



  if(ON_of_P<=0)
  {
       MessageBox(NULL,"��� ���������� �������� ���������� ������",
                       "������!",MB_OK);
       return;
  }
  if(ON_of_P<10)
  {
       MessageBox(NULL,"� ��������� ������� ����� ���� �����",
                       "������!",MB_OK);
       return;
  }

  if(Obz1.isSetMaxDalnGOST)
  {
        FMaxDalnostGOST->CSpinEdit1->Value=Obz1.iMaxDalnGOST_PK;
        FMaxDalnostGOST->ShowModal();
        Obz1.iMaxDalnGOST_PK=FMaxDalnostGOST->CSpinEdit1->Value;
        InitializeM(Obz1.TO,1,Obz1.iMaxDalnGOST_PK);

  }else{
        InitializeM(Obz1.TO,1,450.0);
  }

  Tek=Obz1.tnGetFirstOtschetKanalAndRejim(1,0,2);
  while(Tek>=0)
  {

    Obz1.GetOdin_Bort(Tek,&ob1,&nomerBortaIndxBrt);

    if(ob1.Status&KANAL_P_STATUS)
    {
       if(Obz1.IsWasPrivyazka)
       {
          AddOtchet(nomerBortaIndxBrt,ob1.Azmt,ob1.Dlnst,ob1.Time,
                    ob1.IstVysota);
       }else{
          if(ob1.NomBort>0)
          {
             if(ob1.Status&3==1)
             {
               AddOtchet(ob1.NomBort,ob1.Azmt,ob1.Dlnst,ob1.Time,0);
             }else{
               AddOtchet(ob1.NomBort,ob1.Azmt,ob1.Dlnst,ob1.Time,ob1.OprVysota);
             }
          }else{
             MessageBox(NULL,"� ��������� ������� ���� ������� � �����.������� �����",
                       "������!",MB_OK);
             return;
          }
       }
    }else if(ob1.Status&KANAL_S_STATUS)
    {
//       AddOtchetPropusk(Obz1.Number[ob1.IndxBrt]&0x00FFFFFF,ob1.Azmt,ob1.Dlnst,ob1.Time);
    }
    Tek=Obz1.tnGetNextOtschetKanalAndRejim(Tek,1,0,2);
  }

     ZakonchitObrabotku();   //��������� ����������� � ������ matv_dovesok.dll
}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::Timer2Timer(TObject *Sender)
{
 long X1,Y1,X2,Y2;
   double KoefX,KoefY;

   Timer2->Enabled=false;
   if(CommandTimer2==1)
   {
      Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);

//������ ���������� �������
      VyzovPosleVydeleniaOblasti();
      GlobalState=1;
      SetGlobalState();
      CGauge1->Visible=false;

   }

 //��� ���� ������
   if(CommandTimer2==2)
   {
   //��������� �����

     RisovPanel->Color=Obz1.OblastCol;
     SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
     SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
     SetkaRad30Col=Obz1.Rad30Col;
//������� �����
     Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
     Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
     Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
     Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
     Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;


     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
     Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
     RisovatNotPriamOblast();
     if(VidOblasti==0)
     {
        KoefX=(Image1->Width-1)/((double)(RightKm-LeftKm));
        KoefY=(Image1->Height-1)/((double)(BottomKm-TopKm));
        X1=Okrugl(KoefX*(Xramki[3]-LeftKm));
        Y1=Okrugl(KoefY*(Yramki[3]-TopKm));
        X2=Okrugl(KoefX*(Xramki[1]-LeftKm));
        Y2=Okrugl(KoefY*(Yramki[1]-TopKm));
        SVydel->Left=X1;SVydel->Top=Y1;
        SVydel->Width=X2-X1+1;        SVydel->Height=Y2-Y1+1;
     }
     CGauge1->Visible=false;
     SetGlobalState();
   }


   if(CommandTimer2==3) //������ ��������� ��������
   {
     TAllParallel *AllParallel;
     CGauge1->Progress=0;
     CGauge1->Visible=true;
     extern TCGauge *GlobalCGauge1;
     GlobalCGauge1=CGauge1;
     NachaloPrivyazki();         //������ ��������...

     AllParallel=new TAllParallel(3);  //��� ���������� ��������� SamaPrivyazka();

   }



}
//---------------------------------------------------------------------------







void __fastcall TFGlavOkno::BTochnostClick(TObject *Sender)
{
  long Tek;
  long i,Naid;
  ODIN_BORT_1 ob1;
  long *NomBort=NULL;     //����� �����
  long *Vysota=NULL;      //�������� ������
  long *Xk=NULL,*Yk=NULL;           //���������� X,Y
  double *Azmts=NULL;
  double *Dlnsts=NULL;
  double *Times=NULL,DT;
  double *AzmtsRet=NULL;
  double *DlnstsRet=NULL;
  double X1,Y1,X2,Y2,dX,dY;
  double TempObzoraVChasah=Obz1.TO;
  int MaxN_of_Otsch=0;
  int N_of_Otsch=0;
  double *dAzmt, *dDaln;
  double Sred_dAzmt,Sred_dDaln;
  double SumKvAzmt,SumKvDaln;
  long dObzor;   //
/*
  int ApproximatAzimDalnost_TFC(
          double *AzmtIn,  //������� ������ ��������
          double *DlnstIn, //������� ������ ����������
          double *Times,   //����� � �����
          int N_of_Otch,   //����� ��������
          long Stepen,     //������� ���������� �� 0 �� 6
          double *AzmtOut, //�������� ������ ��������, �� ����� 0
          double *DlnstOut,//�������� ������ ����������, �� ����� 0
          double *SKO_Azmt,//��� �������, �� ����� 0
          double *SKO_Dlnst//��� ���������, �� ����� 0
          );

*/

//��������� �� ��������, ������� �������� � ���������� �������
  Tek=Obz1.GetFirstFilterOB(true);
  IsWasP_T=false; IsWasS_T=false; IsWasPS_T=false;
  N_of_NBT=0;
  while(Tek>=0)
  {
    if(Tek%362==360)
    {
      Tek=Obz1.GetNextFilterOB(Tek,true);
      continue;
    }
    Obz1.GetOdin_Bort(Tek,&ob1);
    if(N_of_Otsch==MaxN_of_Otsch)
    {
      MaxN_of_Otsch+=10;
      Azmts=(double*)realloc(Azmts,sizeof(double)*MaxN_of_Otsch);
      Dlnsts=(double*)realloc(Dlnsts,sizeof(double)*MaxN_of_Otsch);
      Times=(double*)realloc(Times,sizeof(double)*MaxN_of_Otsch);
      NomBort=(long*)realloc(NomBort,sizeof(long)*MaxN_of_Otsch);
      Vysota=(long*)realloc(Vysota,sizeof(long)*MaxN_of_Otsch);
      Xk=(long*)realloc(Xk,sizeof(long)*MaxN_of_Otsch);
      Yk=(long*)realloc(Yk,sizeof(long)*MaxN_of_Otsch);

    }
//      Azmts[N_of_Otsch]=ob1.Azmt; Dlnsts[N_of_Otsch]=ob1.Dlnst; ���� �������������



//�������� �������� �������� � ���������
    if(CSE_Poryadok->Text=="��������")
    {
//��������� ������
      Dlnsts[N_of_Otsch]=ob1.Dlnst;
      Azmts[N_of_Otsch]=ob1.Azmt;
    }else{

//��������� ������ ��� �������������
      X1=ob1.X/1000.0; Y1=ob1.Y/1000.0;
      Xk[N_of_Otsch]=ob1.X; Yk[N_of_Otsch]=ob1.Y;
      NomBort[N_of_Otsch]=ob1.NomBort;
      Vysota[N_of_Otsch]=ob1.OprVysota;
      if(ABS(X1)<0.001&&ABS(Y1)<0.001)
      {
        Azmts[N_of_Otsch]=0;
        Dlnsts[N_of_Otsch]=0;
      }else{
        Dlnsts[N_of_Otsch]=sqrt(X1*X1+Y1*Y1);
        Azmts[N_of_Otsch]=atan2(X1,Y1)/M_PI*180;
        if(Azmts[N_of_Otsch]<0)Azmts[N_of_Otsch]+=360.0;
      }
    }

    if((ob1.Status&3)==1)IsWasP_T=true;
    if((ob1.Status&3)==2)IsWasS_T=true;
    if((ob1.Status&3)==3)IsWasPS_T=true;

    Naid=0;
    for(i=0;i<N_of_NBT;i++)
    {
      if(ob1.NomBort==NBT[i])
      {
        Naid=1;
        break;
      }
    }

    if(Naid==0)
    {
      if(N_of_NBT<20)
      {
        NBT[N_of_NBT]=ob1.NomBort;
      }
      N_of_NBT++;
    }


    Times[N_of_Otsch]=ob1.Time; N_of_Otsch++;



    Tek=Obz1.GetNextFilterOB(Tek,true);
  }
  AzmtsRet=(double*)malloc(sizeof(double)*N_of_Otsch);
  DlnstsRet=(double*)malloc(sizeof(double)*N_of_Otsch);
//  StepenPolyot=CSE_Poryadok->Value;
  Sred_dAzmt=0;
  Sred_dDaln=0;
  if(CSE_Poryadok->Text=="��������")
  {
     StepenPolyot=7;
     dAzmt=(double*)malloc((N_of_Otsch-1)*sizeof(double));
     dDaln=(double*)malloc((N_of_Otsch-1)*sizeof(double));
     for(i=0;i<N_of_Otsch-1;i++)
     {
        dObzor=Okrugl((Times[i+1]-Times[i])/TempObzoraVChasah);
        if(dObzor==0)
        {
          MessageBox(NULL, "��� ������� � ����� ������!", "������������ ������!",MB_OK);
          return;
        }
        if(dObzor>1)
        {
          dAzmt[i]=(Azmts[i+1]-Azmts[i])/dObzor;
          dDaln[i]=(Dlnsts[i+1]-Dlnsts[i])/dObzor;
        }else{
          dAzmt[i]=Azmts[i+1]-Azmts[i];
          dDaln[i]=Dlnsts[i+1]-Dlnsts[i];
        }
        Sred_dAzmt+=dAzmt[i];
        Sred_dDaln+=dDaln[i];
     }
     Sred_dAzmt/=(N_of_Otsch-1);
     Sred_dDaln/=(N_of_Otsch-1);

//������ ����� ��������� ���������
     SumKvAzmt=0;
     SumKvDaln=0;
     for(i=0;i<N_of_Otsch-1;i++)
     {
       SumKvAzmt+=(Sred_dAzmt-dAzmt[i])*(Sred_dAzmt-dAzmt[i]);
       SumKvDaln+=(Sred_dDaln-dDaln[i])*(Sred_dDaln-dDaln[i]);
     }

//������ ������� �������������� ������
     T_SKO_A=sqrt(SumKvAzmt/(N_of_Otsch-2));
     T_SKO_D=sqrt(SumKvDaln/(N_of_Otsch-2));

  }else{
     StepenPolyot=CSE_Poryadok->Text.ToInt();
//������ �� ������������ ��������� Azmts, Dlnsts ������������������ ��������
//AzmtRet, DlnstsRet �������������� �������� � ����������
//��������� ApproximatAzimDalnost_TFC (approx.cpp)
     ApproximatAzimDalnost_TFC(Azmts,Dlnsts,Times,N_of_Otsch,StepenPolyot,
                                AzmtsRet,DlnstsRet,&T_SKO_A,&T_SKO_D);
  }

  T_SKO_A=T_SKO_A*60.0;
  T_SKO_D=T_SKO_D*1000.0;



//��������� ����...



  IzFindTochnost=true;
  X2=Dlnsts[N_of_Otsch-1]*sin(Azmts[N_of_Otsch-1]/180*M_PI);
  Y2=Dlnsts[N_of_Otsch-1]*cos(Azmts[N_of_Otsch-1]/180*M_PI);
  X1=Dlnsts[0]*sin(Azmts[0]/180*M_PI);
  Y1=Dlnsts[0]*cos(Azmts[0]/180*M_PI);
  DT=Times[N_of_Otsch-1]-Times[0];
  dX=X2-X1; dY=Y2-Y1;
  if(dX<0.0001&&dY<0.0001&&dX>-0.0001&&dY>-0.0001||DT<1/36000.0)
  {
  //���� ������ �����������
    KursPolyota=-1;
    SredSkorostPolyota=0;
  }else{
    KursPolyota=atan2(dX,dY)/M_PI*180.0;
    if(KursPolyota<0)KursPolyota+=360;
    SredSkorostPolyota=sqrt(dX*dX+dY*dY)/DT;
  }
//������� �������
  dTimePolyot=DT*3600.0;  //� ��������

  if(StepenPolyot==0)
  {
     SredA=AzmtsRet[0];
     SredD=DlnstsRet[0];
  }


  if(N_of_DToch)
  {
    N_of_DToch=0;
    free(DToch);DToch=NULL;
  }

  if(StepenPolyot<7)
  {
    N_of_DToch=N_of_Otsch;
    DToch=(DANNYE_TOCHNOST *)malloc(sizeof(DANNYE_TOCHNOST)*N_of_DToch);

    for(i=0;i<N_of_DToch;i++)
    {
      DToch[i].NomBort=NomBort[i]; DToch[i].OprVysota=Vysota[i];
      DToch[i].Azmt=Azmts[i]; DToch[i].Dlnst=Dlnsts[i];
      DToch[i].X=Xk[i];DToch[i].Y=Yk[i]; DToch[i].Time=Times[i];
      DToch[i].AzmtA=AzmtsRet[i]; if(DToch[i].AzmtA<0)DToch[i].AzmtA+=360.0;
      DToch[i].DlnstA=DlnstsRet[i];
      DToch[i].XA=DlnstsRet[i]*sin(AzmtsRet[i]/180.0*M_PI)*1000;
      DToch[i].YA=DlnstsRet[i]*cos(AzmtsRet[i]/180.0*M_PI)*1000;
      if(i==0)
      {
        DToch[i].NomerObzora=1;
      }else{
        if(TempObzoraVChasah>0)
        {
          DToch[i].NomerObzora=DToch[i-1].NomerObzora+
            Okrugl((DToch[i].Time-DToch[i-1].Time)/TempObzoraVChasah);
        }else{
          DToch[i].NomerObzora=DToch[i-1].NomerObzora;
        }
      }
    }
  }


  SetGlobalState();
  free(Azmts); free(Dlnsts); free(DlnstsRet); free(AzmtsRet); free(Times);
  free(Xk); free(Yk); free(Vysota); free(NomBort);



}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::FormCloseQuery(TObject *Sender, bool &CanClose)
{

   FExitAsk->ShowModal();
   if(FExitAsk->ExitStatus==2)
   {
     if(GlobalState==3)
     {
       BEndClick(NULL);
     }
     Obz1.SaveOption();
     CanClose=true;
   }else
   if(FExitAsk->ExitStatus==1)
   {
     if(GlobalState==3)
     {
       BEndClick(NULL);
     }
     CanClose=true;
   }else{
     CanClose=false;
   }
}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::RisovPanelResize(TObject *Sender)
{
  Shape1->Top=RisovPanel->Height-20;
  Label7->Top=RisovPanel->Height-20;
  Shape2->Top=RisovPanel->Height-20;
  Label8->Top=RisovPanel->Height-20;
  Shape3->Top=RisovPanel->Height-20;
  Shape4->Top=RisovPanel->Height-20;
  Shape5->Top=RisovPanel->Height-20;
  Label9->Top=RisovPanel->Height-20;
  Shape6->Left=RisovPanel->Width-35;
  Shape6->Top=RisovPanel->Height-20;
  Label10->Left=RisovPanel->Width-20;
  Label10->Top=RisovPanel->Height-20;

}
//---------------------------------------------------------------------------
/*������ - �������� ��������*/
void __fastcall TFGlavOkno::mnuProvestiPrivyazkuClick(TObject *Sender)
{
//  FMesOkno=new TFMesOkno(NULL);
//  FMesOkno->Label1->Caption="���������! ���� �������� ������!";
//  FMesOkno->Show();
  LPanelStatus->Caption="���������! ���� �������� ������!";

  PanelStatus->Left=0;
  PanelStatus->Top=0;
  PanelStatus->Width=Image1->Width;
  PanelStatus->Height=Image1->Height;

  PanelStatus->Visible=true;


  CommandTimer2=3;
  Application->Title="Analisis. ���� ��������!";


/*���������� ���������� Timer2Click. ��� � ���� ������� �������� �������
�����, � ������� ��������������� ���������� ������ ������� FGlavOkno:
NachaloPrivyazki, SamaPrivyazka, KonecPrivyazki

*/
  Timer2->Enabled=true;
}
//---------------------------------------------------------------------------







/*��������� ������� ������ ������� "����" - "�������". ���������
����������� ���� ������� ����. ������ ������ ������.
*/
void __fastcall TFGlavOkno::N1Click(TObject *Sender)
{

extern AnsiString CurPathCfg;  //���� �� ����� �����������

extern AnsiString CurPath;    //������� ����


  char Strka[2001],*RetC;
  int Dlina;
  FILE *fp;
  fp=fopen(CurPathCfg.c_str(),"r");
  if(fp)
  {
//������� ������
    RetC=fgets(Strka,2000,fp);
    if(RetC)
    {
       Dlina=strlen(Strka);
       if(Strka[Dlina-1]=='\n')Strka[Dlina-1]=0;
       CurPath=Strka;

    }
    fclose(fp);
  }
  FSpisokFiles->TempObzoraVSekundah=Obz1.TO*3600.0;
  sprintf(Strka,"%.02lf",FSpisokFiles->TempObzoraVSekundah);
  FSpisokFiles->ETempObzora->Text=Strka;

//������� ������ ������
  FSpisokFiles->BClearListClick(NULL);
  FSpisokFiles->BAddFilesClick(NULL);
  if(FSpisokFiles->CancelAdd)
  {
    return;
  }

  FSpisokFiles->BOpenFileClick(NULL);

  if(!FSpisokFiles->Canceled)
  {
      FGlavOkno->Nachalo1();  
      FGlavOkno->LoadFiles();
      FGlavOkno->Konec1();
      GlobalState=1;
      SetGlobalState();
  }
  IzFindTochnost=false;
  if(N_of_DToch)
  {
    N_of_DToch=0;
    free(DToch);DToch=NULL;
  }


}
//---------------------------------------------------------------------------




void __fastcall TFGlavOkno::mnuIzvlechP_PSClick(TObject *Sender)
{
  AnsiString FS1,FS2;
  int RetPerezapis;
  int Ret;
  int i;
  bool RetB;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;

  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);

  SaveDialog1->Title="���������� P � PS ��������!";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");
  SaveDialog1->FileName="p_ps.txt";
  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {
    FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.IzvlechDataVFile(FileNamePerezapis,
                                        6);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }
               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }
     Ret=Obz1.IzvlechDataVFile(SaveDialog1->FileName,    6);
     if(Ret!=1)
     {
      MessageBox(NULL,"�� ������� ������� ������, ��� ��� �� ������� ������� ���� �� ������!", "������!", MB_OK);

     }
                           
  }

}
//---------------------------------------------------------------------------

/*���� - ����� ���������� �������� ���������� ������*/
void __fastcall TFGlavOkno::mnuDroblVKClick(TObject *Sender)
{
//����� ��������� �������� ���������� ������ �� ���������� ������
   if(FDublirVK==NULL)
   {
      FDublirVK=new TPreviewForm(this);
   }
   FDublirVK->Hide();
   FDublirVK->Caption="��������� ������� ���������� ������";

//��������� ���� FMaxPOtmetok->RichEdit1->Lines
//����������: Traekt2.cpp
   Obz1.FindDroblOtschet(FDublirVK->RichEdit1->Lines,
                           FSpisokFiles->FileNames);
   FDublirVK->WindowState=wsNormal;
   FDublirVK->HelpFile=HelpFileName;
   FDublirVK->HelpContext=10105;
   FDublirVK->WindowState=wsMaximized;
   FDublirVK->Show();



}
//---------------------------------------------------------------------------


/*����� �� - �� �� ���������� ������ -  ���������*/
void __fastcall TFGlavOkno::mnuRS_PK_OptionClick(TObject *Sender)
{
/*������� �������� ������� ��� ���������� ������.
 F_Param_P - ���������� � ������ ParamForP
*/
/*�������� ���������� �� ������ ������ Obz1*/
  F_Param_P->RSA=Obz1.RSA_P2;
  F_Param_P->RSD=Obz1.RSD_P2;
  F_Param_P->HelpFile=HelpFileName;        //��� ����� ������
  F_Param_P->HelpContext=4173; //����� ��������� ������
  F_Param_P->ShowModal();
  if(F_Param_P->Canceled==false)
  {
    Obz1.RSA_P2=F_Param_P->RSA; //������� ����������
    Obz1.RSD_P2=F_Param_P->RSD; //������� ����������
    Obz1.FreeBlizkieForP();  //�������� ������ ��� ��������, �����������
                             //� ���������������� ��������
    if(B_OK->Enabled)B_OKClick(NULL); //���������� ������� ������ OK
    SetGlobalState();
  }
}
//---------------------------------------------------------------------------

/*����� �� - �� �� ���������� ������ -  ���������*/
void __fastcall TFGlavOkno::mnuRS_VK_OptionClick(TObject *Sender)
{
/*������� �������� ������� ��� ���������� ������.
 F_Param_S - ���������� � ������ ParamForS.
*/
//��� ��� ���������
  F_Param_S->RSA_uvd=Obz1.RSA_UVD2;
  F_Param_S->RSD_uvd=Obz1.RSD_UVD2;
//��� RBS ���������
  F_Param_S->RSA_rbs=Obz1.RSA_RBS2;
  F_Param_S->RSD_rbs=Obz1.RSD_RBS2;
  F_Param_S->HelpContext=4191;
  F_Param_S->HelpFile=HelpFileName;
  F_Param_S->ShowModal();
  if(F_Param_S->Canceled==false) //���� � ���� ������ �� �� ���� ������.
  {
    Obz1.RSA_UVD2=F_Param_S->RSA_uvd;
    Obz1.RSD_UVD2=F_Param_S->RSD_uvd;
    Obz1.RSA_RBS2=F_Param_S->RSA_rbs;
    Obz1.RSD_RBS2=F_Param_S->RSD_rbs;
    if(B_OK->Enabled)B_OKClick(NULL); //������������ ������� ����������� ������
    SetGlobalState();     //��������� ��������� ��������� ����.
  }
}
//---------------------------------------------------------------------------
/*����� �� - �� �� ���������� ������ - ����� ������� �������*/
void __fastcall TFGlavOkno::mnuRS_PK_FindBlizkieClick(TObject *Sender)
{
//����� �������� ���������� ������, ����������� � ���������������� ��������
  Obz1.FindBlizkieForP(); //����������� � ������ Traekt2.cpp
  if(B_OK->Enabled)B_OKClick(NULL);
  SetGlobalState();
}
//---------------------------------------------------------------------------

/*����� �� - �� �� ���������� ������ - ����� ������� �������*/
void __fastcall TFGlavOkno::mnuRS_VK_FindBlizkieClick(TObject *Sender)
{
//����� �������� ���������� ������, ����������� � ���������������� ��������
//�������� ���, �������� RBS
    Obz1.FindBlizkieForVK();  //���������� � ������ Traekt2.cpp
    if(B_OK->Enabled)B_OKClick(NULL);
    SetGlobalState();
}
//---------------------------------------------------------------------------

/*����� �� - �� �� ���������� ������ - �������� ������ ���*/
void __fastcall TFGlavOkno::mnuRS_PK_ClearClick(TObject *Sender)
{
//���������� ������, ����������������� ��� ������ ���
  Obz1.FreeBlizkieForP();  //Traekt2.cpp
  if(B_OK->Enabled)B_OKClick(NULL);
  SetGlobalState();
}
//---------------------------------------------------------------------------

/*����� �� - �� �� ���������� ������ - �������� ������ ���*/
void __fastcall TFGlavOkno::mnuRS_VK_ClearClick(TObject *Sender)
{
//���������� ������, ����������������� ��� ������ ���
    Obz1.FreeBlizkieForVK();  //Traekt2.cpp
    if(B_OK->Enabled)B_OKClick(NULL);
    SetGlobalState();
}
//---------------------------------------------------------------------------

/*�������� �������, ����� ������� ���� ��, ������� ����������� � ���������������� ��������*/
/*����� �� - �� �� ���������� ������ - �������� ����� � ������� �������������� ���������*/
void __fastcall TFGlavOkno::mnuRS_PK_VklBortaClick(TObject *Sender)
{
  int i;
/*�������� ����� ���������� ������ (���� �� ����� ��������, �� ������
���� 99999).
*/
  Obz1.VklBlzkieBortaP(); //�������� ��������� ��������� ������ � ������. Traekt2.cpp

/*�������� � ������ ���������� ������ ������*/
  for(i=0;i<CBSpisokBortov->Items->Count;i++)
  {
       CBSpisokBortov->Selected[i]=Obz1.GetSelectedInSortedBorts(i);
  }
  if(B_OK->Enabled)B_OKClick(NULL);   //������������ ����
  SetGlobalState();
}
//---------------------------------------------------------------------------
/*�������� ������� S, ����� ������� ���� ��, ������� ����������� � ���������������� ��������*/
/*����� �� - �� �� ���������� ������ - �������� ����� � ������� �������������� ��������� - ���*/
void __fastcall TFGlavOkno::mnuRS_UVD_Add_BrtClick(TObject *Sender)
{
  int i;
  Obz1.VklBlzkieBortaUVD(); //�������� ����� ��� � ������ ������, �������
   //����� �������, ����������� � ���������������� �������� ���� �� �����. Traekt2.cpp


  for(i=0;i<CBSpisokBortov->Items->Count;i++)
  {
       CBSpisokBortov->Selected[i]=Obz1.GetSelectedInSortedBorts(i);
  }
  if(B_OK->Enabled)B_OKClick(NULL);
  SetGlobalState();

}
//---------------------------------------------------------------------------
/*�������� ������� S, ����� ������� ���� ��, ������� ����������� � ���������������� ��������*/
/*����� �� - �� �� ���������� ������ - �������� ����� � ������� �������������� ��������� - RBS*/
void __fastcall TFGlavOkno::mnuRS_RBS_Add_BrtClick(TObject *Sender)
{
    int i;
  Obz1.VklBlzkieBortaRBS(); //�������� ����� RBS � ������ ������, �������
   //����� �������, ����������� � ���������������� �������� ���� �� �����. Traekt2.cpp

  for(i=0;i<CBSpisokBortov->Items->Count;i++)
  {
       CBSpisokBortov->Selected[i]=Obz1.GetSelectedInSortedBorts(i);
  }
  if(B_OK->Enabled)B_OKClick(NULL);
  SetGlobalState();
}
//---------------------------------------------------------------------------


/*����� �� - �� �� ���������� ������ - ���������� � ���������� �������*/
void __fastcall TFGlavOkno::mnuRS_PK_ViewVydelClick(TObject *Sender)
{
  if(FRS_P==NULL)
  {
//�������� �����, � ������� ����� ���������� � ���������� �������  
    FRS_P=new TPreviewForm(this);
  }
  FRS_P->Hide();
  FRS_P->Caption="�������, ����������� � ���������������� ��������. ��.";

//��������� ������� FRS_P->RichEdit1 � ������� �����������
//Traekt2.cpp
  Obz1.VyvodVydelRS_P(FRS_P->RichEdit1->Lines,
                           FSpisokFiles->FileNames);
  FRS_P->WindowState=wsNormal;
  FRS_P->HelpFile=HelpFileName;
  FRS_P->HelpContext=10106;
  FRS_P->Show();
}
//---------------------------------------------------------------------------

/*����� �� - �� �� ���������� ������ - ���������� � ���������� ������� - ���*/
void __fastcall TFGlavOkno::mnuRS_VK_View_UVDClick(TObject *Sender)
{
  if(FRS_UVD==NULL)
  {
//�������� �����, � ������� ����� ���������� � ���������� ������� ���
    FRS_UVD=new TPreviewForm(this);
  }
  FRS_UVD->Hide();
  FRS_UVD->Caption="�������, ����������� � ���������������� ��������. ���";

  //��������� ������� FRS_UVD->RichEdit1 � ������� �����������
//Traekt2.cpp
  Obz1.VyvodVydelRS_UVD(FRS_UVD->RichEdit1->Lines,
                           FSpisokFiles->FileNames);
  FRS_UVD->WindowState=wsNormal;
  FRS_UVD->HelpFile=HelpFileName;
  FRS_UVD->HelpContext=10107;
  FRS_UVD->Show();

}
//---------------------------------------------------------------------------
/*����� �� - �� �� ���������� ������ - ���������� � ���������� ������� - RBS*/
void __fastcall TFGlavOkno::mnuRS_VK_View_RBSClick(TObject *Sender)
{
  if(FRS_RBS==NULL)
  {
//�������� �����, � ������� ����� ���������� � ���������� ������� RBS
    FRS_RBS=new TPreviewForm(this);
  }
  FRS_RBS->Hide();
  FRS_RBS->Caption="�������, ����������� � ���������������� ��������. RBS";

  //��������� ������� FRS_RBS->RichEdit1 � ������� �����������
//Traekt2.cpp
  Obz1.VyvodVydelRS_RBS(FRS_RBS->RichEdit1->Lines,
                           FSpisokFiles->FileNames);
  FRS_RBS->WindowState=wsNormal;
  FRS_RBS->HelpFile=HelpFileName;
  FRS_RBS->HelpContext=10108;
  FRS_RBS->Show();

}
//---------------------------------------------------------------------------

/*���� - ��������� �� ������� � �������*/
void __fastcall TFGlavOkno::mnuDopClick(TObject *Sender)
{


 char *HelpFileNameC;   //��� ����� ������
 int SizeOfHelpFileNameC;
 char **FileNamesPlots;
 long N_of_FileNamesPlots=0;
 AnsiString RetStr;
 long i;

 extern TCGauge *GlobalCGauge1;
 GlobalCGauge1=CGauge1;


 char *ImyaFaila;
 long *NomeraBortov,N_of_NomeraBortov=0;

  N_of_FileNamesPlots=FSpisokFiles->N_of_fps;
  FileNamesPlots=(char**)malloc(sizeof(char*)*N_of_FileNamesPlots);

  for(i=0;i<N_of_FileNamesPlots;i++)
  {
    FileNamesPlots[i]=
      (char*)malloc(sizeof(char)*(FSpisokFiles->FileNames[i].Length()+1));
    strcpy(FileNamesPlots[i],FSpisokFiles->FileNames[i].c_str());
  }



  GlobalCGauge1->Progress=0;
  GlobalCGauge1->MinValue=0;
  GlobalCGauge1->MaxValue=2*(Obz1.CLSutkiCur*24+Obz1.CLChasyCur-
                             Obz1.CLSutkiFirst*24-Obz1.CLChasyFirst)+24;
  GlobalCGauge1->Visible=true;


//�������� ������ � ������������� ����
//RetStr - ��� ������� �������������� �����. ���������� � Traekt2.cpp
  RetStr=Obz1.SaveDataForFindIskaj(FSpisokFiles->FileNames[0]);

//������� ������ �� ���� ����� ���������� ������.  ���������� � Traekt2.cpp
  Obz1.BackupDataForFindIskaj();

//ImyaFaila - ����� ������ ���������� ���� � ������, ������� ������������ ���
//������� ���.����
  ImyaFaila=(char*)malloc(sizeof(char)*(RetStr.Length()+1));
  strcpy(ImyaFaila,RetStr.c_str());
  NomeraBortov=(long*)malloc(sizeof(long)*Obz1.GetNumberOfSortedBorts());
  for(i=0;i<Obz1.GetNumberOfSortedBorts();i++)
  {
    if(Obz1.GetSelectedInSortedBorts(i))
    {
      NomeraBortov[N_of_NomeraBortov]=Obz1.GetNomerSelectedBort(i);
      N_of_NomeraBortov++;
    }
  }

  SizeOfHelpFileNameC=HelpFileName.Length()+1;
  HelpFileNameC=(char*)malloc(SizeOfHelpFileNameC*sizeof(char));
  strcpy(HelpFileNameC,HelpFileName.c_str());
//����������� ���� "A����� �������������� ����������"
    FindIskaj631(Obz1.TO*3600.0,ImyaFaila,true,HelpFileNameC,NomeraBortov,N_of_NomeraBortov,
                GlobalCGauge1,FileNamesPlots,N_of_FileNamesPlots);
  free(HelpFileNameC);

//������������ ������ � ����� ����� �������� ����.
//���������� � Traekt2.cpp
  Obz1.RestoreAllAfterFindIskaj();
  if(FileExists(ImyaFaila))
  {
     DeleteFile(ImyaFaila);   //�������� ���������� �����
  }
  free(ImyaFaila);
  free(NomeraBortov);
  for(i=0;i<N_of_FileNamesPlots;i++)
  {
    free(FileNamesPlots[i]);
  }
  free(FileNamesPlots);
  GlobalCGauge1->Visible=false;


}
//---------------------------------------------------------------------------




void __fastcall TFGlavOkno::N9Click(TObject *Sender)
{
  int Ret;
  bool RetB;
  SaveDialog1->Title="���������� ������ ���������� ������";
  SaveDialog1->Filter="��������� �����(*.LST)|*.lst|��� �����|*.*";
  SaveDialog1->DefaultExt=String("lst");

  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {
     Ret=Obz1.SaveSpisokBortov(SaveDialog1->FileName);
     if(Ret!=1)
     {
       MessageBox(NULL,"�� ������� ��������� ������ ������","������!",MB_OK);
     }
  }


}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::mnu_Load_SelectionClick(TObject *Sender)
{
  int Ret;
  bool RetB;
  long i;
  OpenDialog1->Title="�������� ������ ���������� ������";
  OpenDialog1->Filter="����� ������(*.LST)|*.lst|��� �����|*.*";
  OpenDialog1->DefaultExt=String("lst");
  OpenDialog1->InitialDir=CurPath;
  RetB=OpenDialog1->Execute();
  if(RetB)
  {
     Ret=Obz1.LoadSpisokBortov(OpenDialog1->FileName);
     if(Ret!=1)
     {
       MessageBox(NULL,"�� ������� ������� ������ ������","������!",MB_OK);
       return;
     }
     for(i=0;i<CBSpisokBortov->Items->Count;i++)
     {
       CBSpisokBortov->Selected[i]=Obz1.GetSelectedInSortedBorts(i);
     }

     if(B_OK->Enabled)B_OKClick(NULL);
     SetGlobalState();
  }

}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  int i;
  if(N_of_FilesForDelete>0)
  {
    for(i=0;i<N_of_FilesForDelete;i++)
    {
      if(FileExists(FilesForDelete[i]))DeleteFile(FilesForDelete[i]);
    }
    N_of_FilesForDelete=0;
  }

  if(hMatv_Dovesok)
  {
    FreeLibrary(hMatv_Dovesok);
  }

  if(FViewVydelData)delete FViewVydelData;
  if(FViewData)delete FViewData;
  if(FMaxPOtmetok)delete FMaxPOtmetok;
  if(FMaxSredPOtmetok)delete FMaxSredPOtmetok;
  if(FDublirVK)delete FDublirVK;
  if(FRS_P)delete FRS_P;
  if(FRS_UVD)delete FRS_UVD;
  if(FRS_RBS)delete FRS_RBS;

extern int N_OF_MATRIX;

extern int D_ALLOC_FREE;
//  MessageBox(NULL,String(N_OF_MATRIX).c_str(),String(D_ALLOC_FREE).c_str(),MB_OK);

}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::mnuViewVisualBortsClick(TObject *Sender)
{
   int i;
   Obz1.VydelSVisualSpisokBortov();
   SetGlobalState();
   for(i=0;i<CBSpisokBortov->Items->Count;i++)
   {
       CBSpisokBortov->Selected[i]=Obz1.GetSelectedInSortedBorts(i);
   }
   CBSpisokBortov->Enabled=true;
   
}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::BStartClick(TObject *Sender)
{



  OnResize=NULL;
extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;



  NazadAnim=false;
  CBNazad->Checked=false;
  TBDTimeAnimChange(NULL);
  Obz1.InizialAnim(Image1,LeftKm,BottomKm,RightKm,TopKm);
  GlobalState=3;

  SetGlobalState();
  TimerAnimate->Enabled=true;
  PBPause->Caption="�����";
  NajataPBPause=false;
  PBPause->BevelOuter=bvRaised;

  OnResize=FormResize;
}

//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::TBDTimeAnimChange(TObject *Sender)
{
  switch(TBDTimeAnim->Position)
  {
     case 1: //����� �������
     case 2:
     case 3:
     case 4:
     case 5: //����� �������
     case 6:
     case 7:
     case 8:
  TimerAnimate->Interval=50; DSektorAnim=4*(9-TBDTimeAnim->Position); break;
     case 9: //����� �������
     case 10:
     case 11:
     case 12:
     case 13: //����� �������
     case 14:
     case 15:
     case 16:
  TimerAnimate->Interval=50+5*(TBDTimeAnim->Position-8);DSektorAnim=4; break;
     case 17:
  TimerAnimate->Interval=100; DSektorAnim=3; break;
     case 18:
  TimerAnimate->Interval=100; DSektorAnim=2; break;
     case 19:
  TimerAnimate->Interval=100; DSektorAnim=1; break;
     case 20:
  TimerAnimate->Interval=200; DSektorAnim=1; break;
     case 21:
  TimerAnimate->Interval=300; DSektorAnim=1; break;
     case 22:
  TimerAnimate->Interval=400; DSektorAnim=1; break;
     case 23:
  TimerAnimate->Interval=500; DSektorAnim=1; break;
     case 24:
  TimerAnimate->Interval=750; DSektorAnim=1; break;
     case 25:
  TimerAnimate->Interval=1000; DSektorAnim=1; break;




  };

}
//---------------------------------------------------------------------------










void __fastcall TFGlavOkno::TimerAnimateTimer(TObject *Sender)
{

    if(GlobalState==3)
    {
    //  CGauge1->Visible=true;
      RisovPanel->Color=Obz1.OblastCol;
      SetkaOblastCol=Obz1.OblastCol; SetkaKolca1Col=Obz1.Kolca1Col;
      SetkaKolca2Col=Obz1.Kolca2Col; SetkaRad90Col=Obz1.Rad90Col;
      SetkaRad30Col=Obz1.Rad30Col;
//������� �����
      Shape1->Brush->Color=Obz1.P_Col;Shape2->Brush->Color=Obz1.S_Col;
      Shape6->Brush->Color=Obz1.PS_Col;Shape3->Brush->Color=Obz1.RS_P_Col;
      Shape4->Brush->Color=Obz1.RS_UVD_Col;Shape5->Brush->Color=Obz1.RS_RBS_Col;
      Label7->Font->Color=Obz1.Text_Col;          Label8->Font->Color=Obz1.Text_Col;
      Label9->Font->Color=Obz1.Text_Col;          Label10->Font->Color=Obz1.Text_Col;

      Obz1.VychislenieKoordinatAnim(Image1,LeftKm,BottomKm,RightKm,TopKm);
      VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
      if(NazadAnim)
      {
        Obz1.VyvodAllDataAnimTekSektorNazad(Image1,LeftKm,BottomKm,RightKm,TopKm);
        Obz1.PrevNSAnim(DSektorAnim);
      }else{
        Obz1.VyvodAllDataAnimTekSektor(Image1,LeftKm,BottomKm,RightKm,TopKm);
        Obz1.NextNSAnim(DSektorAnim);
      }
      if(Obz1.cCurTimeAnim[0]!=0)
      {
        LCurTime->Caption=Obz1.cCurTimeAnim;
      }


      return;
    }

}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::BEndClick(TObject *Sender)
{
  TimerAnimate->Enabled=false;
  GlobalState=1;
  Obz1.StopAnim();
  SetGlobalState();
  FormResize(NULL);
}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::CBNazadClick(TObject *Sender)
{
  NazadAnim= CBNazad->Checked;
  FormResize(NULL);
}
//---------------------------------------------------------------------------



















void __fastcall TFGlavOkno::PM_CopyToBufferClick(TObject *Sender)
{
   int i,j;
   AnsiString Strka,Strka1;
   TClipboard *CP=Clipboard();
 
//�������� ������ CP
   CP->Clear();

   for(i=0;i<CBSpisokBortov->Items->Count;i++)
   {
     if(CBSpisokBortov->Selected[i])
     {
       Strka1=CBSpisokBortov->Items->Strings[i];
       for(j=1;j<=Strka1.Length();j++)
       {
         if(Strka1[j]==':')
         {
           Strka1=Strka1.SubString(1,j-1);
           break;
         }

       }
       if(Strka!="")
       {
         Strka=Strka+String(", ")+Strka1;
       }else{
         Strka=Strka1;
       }
     }
   }







}
//---------------------------------------------------------------------------




/*������� - �������� ��, ������ ���������� �������*/
void __fastcall TFGlavOkno::mnuClearNotSelectionClick(TObject *Sender)
{
   Obz1.ClearNotSelection(); //�������� ��� ����������� �������. ����������: Traekt.cpp
   SetGlobalState();  //�������� ��������� ���������
   FormResize(NULL);  //������������ �����������
}
//---------------------------------------------------------------------------


/*��������� "����" - "��������� ���� ������". ��������� ������ ������������ ��
����� � �����*/
void __fastcall TFGlavOkno::mnuSaveVydelAsClick(TObject *Sender)
{
  int RetPerezapis;
  bool RetB;
  int Ret;
  int i;
  unsigned short H1,M1,S1,mS1,H2,M2,S2,mS2;
  extern TCGauge *GlobalCGauge1;
  GlobalCGauge1=CGauge1;


  DTP1->DateTime.DecodeTime(&H1,&M1,&S1,&mS1);
  DTP2->DateTime.DecodeTime(&H2,&M2,&S2,&mS2);
  Obz1.GetTimeAndSutki(CSE_Sutki1->Value-1,
                             (int)H1,(int)M1,(int)S1,
                             CSE_Sutki2->Value-1,
                             (int)H2,(int)M2,(int)S2);


  SaveDialog1->Title="���������� ���������� ������ � �����";
  SaveDialog1->Filter="��������� �����(*.TXT)|*.txt|��� �����|*.*";
  SaveDialog1->DefaultExt=String("txt");

  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();

//��������, � �� �������� �� ��� ����� �� �������� ������
  AnsiString FS1,FS2;
//  bool IsWasToJeImya=false;


  if(RetB)
  {
    FS2=SaveDialog1->FileName.Trim();
    FS2=LowerCase(FS2);
    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ��� ������ Analisis! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FileNamePerezapis=SaveDialog1->FileName+String("1");
               Ret=Obz1.SaveVydelAs(FileNamePerezapis);
               if(Ret!=1)
               {
                      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
                      return;
               }


               TimerPerezapis->Enabled=true;

        }
        return;
      }
    }
//��������� ���� ���������� ������
    Ret=Obz1.SaveVydelAs(SaveDialog1->FileName);
    if(Ret!=1)
    {
      MessageBox(NULL,"�� ������� ��������� ����", "������!", MB_OK);
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::mnuSaveVydelListClick(TObject *Sender)
{
  int Ret;
  bool RetB;
  SaveDialog1->Title="���������� ������ ������ � ������� ���������";
  SaveDialog1->Filter="��������� �����(*.LST)|*.lst|��� �����|*.*";
  SaveDialog1->DefaultExt=String("lst");

  SaveDialog1->InitialDir=CurPath;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {
     Ret=Obz1.SaveSpisokVydelBortov(SaveDialog1->FileName);
     if(Ret!=1)
     {
       MessageBox(NULL,"�� ������� ��������� ������ ������","������!",MB_OK);
     }
  }
        
}
//---------------------------------------------------------------------------






void __fastcall TFGlavOkno::CSE_PoryadokChange(TObject *Sender)
{
  int i;
  CSE_Poryadok->Text=CSE_Poryadok->Text.Trim().UpperCase();
  for(i=0;i<CSE_Poryadok->Items->Count;i++)
  {
    if(CSE_Poryadok->Text==CSE_Poryadok->Items->Strings[i])
    {
       return;
    }
  }
  CSE_Poryadok->Text="1";

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::TimerPerezapisTimer(TObject *Sender)
{

extern AnsiString CurPathCfg;  //���� �� ����� �����������

extern AnsiString CurPath;    //������� ����


  char Strka[2001],*RetC;
  int Dlina;
  TAllParallel *AllParallel;
  FILE *fp;

  int i;
  TimerPerezapis->Enabled=false;
  AnsiString FileNamePerezapis1;
//������ ���������� � ������������

//��������� �������� ���� ������
  for(i=0; i<FSpisokFiles->N_of_fps;i++)
  {
//    fclose(FSpisokFiles->fps[i]);
  }


//������ ���������� ������������� ����������� ����
  FileNamePerezapis1=FileNamePerezapis.SubString(1,FileNamePerezapis.Length()-1);

  if(FileExists(FileNamePerezapis1))
  {
    DeleteFile(FileNamePerezapis1);
  }

  RenameFile(FileNamePerezapis,FileNamePerezapis1);


  //������� ����� ������
  fp=fopen(CurPathCfg.c_str(),"r");
  if(fp)
  {
//������� ������
    RetC=fgets(Strka,2000,fp);
    if(RetC)
    {
       Dlina=strlen(Strka);
       if(Strka[Dlina-1]=='\n')Strka[Dlina-1]=0;
       CurPath=Strka;

    }
    fclose(fp);
  }
  FSpisokFiles->TempObzoraVSekundah=Obz1.TO*3600.0;
  sprintf(Strka,"%.02lf",FSpisokFiles->TempObzoraVSekundah);
  FSpisokFiles->ETempObzora->Text=Strka;

  FSpisokFiles->BOpenFileClick(NULL);

  if(!FSpisokFiles->Canceled)
  {
    AllParallel=new TAllParallel(1);
    GlobalState=1;
    SetGlobalState();
//    Obz1.Date_Input=InputBox("���� ����������������","������� ���� ���������������� ������",
//                "");
//    Obz1.Dop_Info="";
  }
  IzFindTochnost=false;
  if(N_of_DToch)
  {
    N_of_DToch=0;
    free(DToch);DToch=NULL;
  }






}
//---------------------------------------------------------------------------








/*������� - ���������� �������*/
void __fastcall TFGlavOkno::mnuPredyshMashtabClick(TObject *Sender)
{
  if(N_of_PrevKm<=0)return;
  N_of_PrevKm--;

  if(PrevLeftKm[N_of_PrevKm]<-99&&PrevLeftKm[N_of_PrevKm]>-101&&
     PrevBottomKm[N_of_PrevKm]<-99&&PrevBottomKm[N_of_PrevKm]>-101&&
     PrevRightKm[N_of_PrevKm]>99&&PrevRightKm[N_of_PrevKm]<101&&
     PrevTopKm[N_of_PrevKm]>99&&PrevTopKm[N_of_PrevKm]<101)
  {
  //����������� ������� ��� -100, -100, 100, 100 ��
     RB_100km->OnClick=NULL;
     RB_100km->Checked=true;
     RB_100km->OnClick=RB_100kmClick; //���������� ��������� ����� �� 100 ��
     RB_100kmClick(NULL);
  }else if(PrevLeftKm[N_of_PrevKm]<-199&&PrevLeftKm[N_of_PrevKm]>-201&&
     PrevBottomKm[N_of_PrevKm]<-199&&PrevBottomKm[N_of_PrevKm]>-201&&
     PrevRightKm[N_of_PrevKm]>199&&PrevRightKm[N_of_PrevKm]<201&&
     PrevTopKm[N_of_PrevKm]>199&&PrevTopKm[N_of_PrevKm]<201)
  {
  //����������� ������� ��� -200, -200, 200, 200 ��
     RB_200km->OnClick=NULL;
     RB_200km->Checked=true;
     RB_200km->OnClick=RB_200kmClick; //���������� ��������� ����� �� 200 ��
     RB_200kmClick(NULL);
  }else if(PrevLeftKm[N_of_PrevKm]<-399&&PrevLeftKm[N_of_PrevKm]>-401&&
     PrevBottomKm[N_of_PrevKm]<-399&&PrevBottomKm[N_of_PrevKm]>-401&&
     PrevRightKm[N_of_PrevKm]>399&&PrevRightKm[N_of_PrevKm]<401&&
     PrevTopKm[N_of_PrevKm]>399&&PrevTopKm[N_of_PrevKm]<401)
  {
  //����������� ������� ��� -400, -400, 400, 400 ��
     RB_400km->OnClick=NULL;
     RB_400km->Checked=true;
     RB_400km->OnClick=RB_400kmClick;
     RB_400kmClick(NULL);  //���������� ��������� ����� �� 400 ��
  }else if(PrevLeftKm[N_of_PrevKm]<-49&&PrevLeftKm[N_of_PrevKm]>-51&&
//����������� ������� -50, -50, 50, 50 ��

     PrevBottomKm[N_of_PrevKm]<-49&&PrevBottomKm[N_of_PrevKm]>-51&&
     PrevRightKm[N_of_PrevKm]>49&&PrevRightKm[N_of_PrevKm]<51&&
     PrevTopKm[N_of_PrevKm]>49&&PrevTopKm[N_of_PrevKm]<51)
  {
     RB_50km->OnClick=NULL;
     RB_50km->Checked=true;
     RB_50km->OnClick=RB_50kmClick;
     RB_50kmClick(NULL);  //���������� ��������� ����� �� 50 ���
  }else{

//���������� ������� ��� �� �����������
     LeftKm=PrevLeftKm[N_of_PrevKm];
     RightKm=PrevRightKm[N_of_PrevKm];
     TopKm=PrevTopKm[N_of_PrevKm];
     BottomKm=PrevBottomKm[N_of_PrevKm];
     RB_50km->Checked=false;
     RB_100km->Checked=false;
     RB_200km->Checked=false;
     RB_400km->Checked=false;
     RB_Uvelichenie->Checked=true;
     RB_Uvelichenie->Visible=true;
     RB_Uvelichenie->SetFocus();
     FormResize(NULL);
  }



}
//---------------------------------------------------------------------------







void __fastcall TFGlavOkno::N4Click(TObject *Sender)
{
  if(!FileExists(HelpFile))
  {
    AnsiString Err=String("�� ������� ����� ������: ")+HelpFile;
    MessageBox(Handle,Err.c_str(),"������!",MB_OK);
    return;
  }
  WinHelp(Handle,HelpFile.c_str(),HELP_CONTEXT, HelpContext);

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::N2Click(TObject *Sender)
{
    if(!FileExists(HelpFile))
  {
    AnsiString Err=String("�� ������� ����� ������: ")+HelpFile;
    MessageBox(Handle,Err.c_str(),"������!",MB_OK);
    return;
  }
  WinHelp(Handle,HelpFile.c_str(),HELP_CONTENTS, 0);

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::N3Click(TObject *Sender)
{
 if(!FileExists(HelpFile))
  {
    AnsiString Err=String("�� ������� ����� ������: ")+HelpFile;
    MessageBox(Handle,Err.c_str(),"������!",MB_OK);
    return;
  }
  WinHelp(Handle,HelpFile.c_str(),HELP_FINDER, 0);
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::N8Click(TObject *Sender)
{
  FAbout->ShowModal();
}
//---------------------------------------------------------------------------





void __fastcall TFGlavOkno::EVysota1Exit(TObject *Sender)
{
  int Ret,Chislo;
  //���� ��������� � ����������, ��� ��� �����
  Ret=sscanf(EVysota1->Text.c_str(),"%d",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
    EVysota1->SetFocus();
    return;
  }


}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::CSBVysota1DownClick(TObject *Sender)
{
  int Ret,Chislo;
  //���� ��������� � ����������, ��� ��� �����
  Ret=sscanf(EVysota1->Text.c_str(),"%d",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
    EVysota1->SetFocus();
    return;
  }
  Chislo-=100;
  if(Chislo<0)Chislo=0;
  EVysota1->Text=IntToStr(Chislo);
  EVysota1->SetFocus();
}
//---------------------------------------------------------------------------


void __fastcall TFGlavOkno::CSBVysota1UpClick(TObject *Sender)
{
  int Ret,Chislo;
  //���� ��������� � ����������, ��� ��� �����
  Ret=sscanf(EVysota1->Text.c_str(),"%d",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
    EVysota1->SetFocus();
    return;
  }
  Chislo+=100;
  if(Chislo<0)Chislo=0;
  EVysota1->Text=IntToStr(Chislo);
  EVysota1->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::CSBVysota2UpClick(TObject *Sender)
{
  int Ret,Chislo;
  //���� ��������� � ����������, ��� ��� �����
  Ret=sscanf(EVysota2->Text.c_str(),"%d",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
    EVysota2->SetFocus();
    return;
  }
  Chislo+=100;
  if(Chislo<0)Chislo=0;
  EVysota2->Text=IntToStr(Chislo);
  EVysota2->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::CSBVysota2DownClick(TObject *Sender)
{
 int Ret,Chislo;
  //���� ��������� � ����������, ��� ��� �����
  Ret=sscanf(EVysota2->Text.c_str(),"%d",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
    EVysota2->SetFocus();
    return;
  }
  Chislo-=100;
  if(Chislo<0)Chislo=0;
  EVysota2->Text=IntToStr(Chislo);
  EVysota2->SetFocus();
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::EVysota2Exit(TObject *Sender)
{
 int Ret,Chislo;
  //���� ��������� � ����������, ��� ��� �����
  Ret=sscanf(EVysota2->Text.c_str(),"%d",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"������ ������ ���� ����� ������!", "������!", MB_OK);
    EVysota2->SetFocus();
    return;
  }

}
//---------------------------------------------------------------------------

/*���� - ����������� ����������� �������� �� � ��*/
void __fastcall TFGlavOkno::mnuFindPObjedClick(TObject *Sender)
{
//����� ������������ ������������� ��������
   int Ret;
   long N_of_PS,N_of_P_S;
   double P;
   char Strka[1000];

/*���������� � ����� ������� � ��������� ���������� � ���������� �������.
  ������� ����������� � ������ Traekt2.cpp
*/
   Ret=Obz1.FindVeroyatnostObjedinenie(&N_of_P_S, &N_of_PS);
   if(Ret==0)return;

   if(N_of_P_S==0)
   {
     MessageBox(Handle,"������ ������� ����������� \n \
- �� ������� ������� c ��������� �� � �� ������ �����!",
           "����������� ������� �� � ��!",MB_OK);
   }else{
     P=(double(N_of_PS))/N_of_P_S;
     sprintf(Strka,"������� c �� � �� ������ �����=%d, \n \
������������ �������=%d, \n \
����������� �����������=%5.03lf",N_of_P_S,N_of_PS,P);
     MessageBox(Handle,Strka, "����������� ������� �� � ��!",MB_OK);

   }



}
//---------------------------------------------------------------------------



void __fastcall TFGlavOkno::EVysota1KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==0x0d)
  {
    ActiveControl=EVysota2;

  }
}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::EVysota2KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==0x0d)
  {
    ActiveControl=CSE_Poryadok;;

  }

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::DTP1KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==0x0d)
  {
    ActiveControl=DTP2;
  }

}
//---------------------------------------------------------------------------

void __fastcall TFGlavOkno::DTP2KeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==0x0d)
  {
     if(Obz1.IsCheckVys)
     {
        ActiveControl=EVysota1;
     }else{
        ActiveControl=CSE_Poryadok;
     }

  }
}




















































































































































































































































































































































void __fastcall TFGlavOkno::RadioButton1Click(TObject *Sender)
{
//     VyvodSetkaANLSS(Image1,LeftKm,BottomKm,RightKm,TopKm);
 //    Obz1.VyvodNaEkran(Image1,LeftKm,BottomKm,RightKm,TopKm,CGauge1);
 //    FreeNotPriamOblast();

    LeftKm=-450;
    RightKm=450;
    BottomKm=-450;
    TopKm=450;
    RB_Uvelichenie->Visible=false;

    FormResize(NULL);        
}
//---------------------------------------------------------------------------








