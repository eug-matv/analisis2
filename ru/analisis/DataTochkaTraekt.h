/*���� ���������  ������ �����, ������� ���������� �� ���� ���������� ���� (VydelZona)
� ������ ���� ������������ ���������� � ��������� �����, ����� ������� � ����������
����� ����� ���������� ������.
*/

//---------------------------------------------------------------------------
#ifndef DataTochkaTraektH
#define DataTochkaTraektH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFDataTraekt : public TForm
{
__published:	// IDE-managed Components
        TGroupBox *GroupBox1;
        TLabel *LBort;
        TLabel *LVysota;
        TLabel *L_X;
        TLabel *L_Y;
        TLabel *LAzmt;
        TLabel *LDlnst;
        TLabel *LTime;
        TGroupBox *GroupBox2;
        TLabel *L_XA;
        TLabel *L_YA;
        TLabel *LAzmtA;
        TLabel *LDlnstA;
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
         struct DANNYE_TOCHNOST *DT1;
        int __fastcall MyShow(struct DANNYE_TOCHNOST *DT);
        __fastcall TFDataTraekt(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFDataTraekt *FDataTraekt;
//---------------------------------------------------------------------------
#endif
