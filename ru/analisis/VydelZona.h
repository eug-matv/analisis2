/*���� ���������� ����. ������ ���� ���������� �����, ������� ���� �����������
�� �������� (���). �� �������� ���� ��������� � ���������� ������� ���� �����������
������� ������ ������� ���� � ������� ���������
*/
//---------------------------------------------------------------------------
#ifndef VydelZonaH
#define VydelZonaH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFVydelZona : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TMenuItem *Dt1;
        TMenuItem *t1;
        TMenuItem *N1;
        TStaticText *StaticText1;
        TStaticText *StaticText2;
        TShape *ShOkrujnost;
        void __fastcall FormPaint(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall FormCanResize(TObject *Sender, int &NewWidth,
          int &NewHeight, bool &Resize);
        void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall FormMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall ShOkrujnostMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ShOkrujnostMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall ShOkrujnostMouseMove(TObject *Sender,
          TShiftState Shift, int X, int Y);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall FormHide(TObject *Sender);
        void __fastcall Dt1Click(TObject *Sender);
        void __fastcall t1Click(TObject *Sender);
        void __fastcall N1Click(TObject *Sender);
private:	// User declarations
        TShape **Shapes1;
        TShape **Shapes2;
        int N_of_Shapes;
        int ClientHeight1;
        int ClientWidth1;
        double X_k_Y;


        struct DANNYE_TOCHNOST *DT1;
        double LeftKm1, BottomKm1, RightKm1,TopKm1;

        int DopDWidth,DopDHeight;

        double Kurs1;

public:		// User declarations
        __fastcall TFVydelZona(TComponent* Owner);
       void __fastcall FreeAllShapes(void);




       int __fastcall VydelZonaMake(double LeftKm, double BottomKm,
                                     double RightKm, double TopKm,
                                     struct DANNYE_TOCHNOST *DT,
                                     int N_of_DT, double Kurs);

};
//---------------------------------------------------------------------------
extern PACKAGE TFVydelZona *FVydelZona;
//---------------------------------------------------------------------------
#endif
