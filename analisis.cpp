//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "AnalisisMain.h"
//---------------------------------------------------------------------------
USEFORM("zona_obzora\UDebug.cpp", FDebug);
USEFORM("en\analisis\About.cpp", FAbout);
USEFORM("en\analisis\DataTochkaTraekt.cpp", FDataTraekt);
USEFORM("en\analisis\ExitQuest.cpp", FExitAsk);
USEFORM("en\analisis\FUser_Info.cpp", FUserInfo);
USEFORM("en\analisis\GraphDOrA.cpp", FGraphDOrA);
USEFORM("en\analisis\max_dalnost_gost.cpp", FMaxDalnostGOST);
USEFORM("en\analisis\Nastroiki.cpp", FNastroiki);
USEFORM("en\analisis\Preview.cpp", PreviewForm);
USEFORM("en\analisis\ParamForP.cpp", F_Param_P);
USEFORM("en\analisis\ParamForS.cpp", F_Param_S);
USEFORM("en\analisis\SpisokFilis.cpp", FSpisokFiles);
USEFORM("en\analisis\GlavOkno.cpp", FGlavOkno);
USEFORM("en\analisis\VydelZona.cpp", FVydelZona);
USEFORM("en\zona_obzora\OptionForZone.cpp", FOption);
USEFORM("en\zona_obzora\Unit7.cpp", FSpisokBortov);
USEFORM("en\zona_obzora\Unit3.cpp", Form3);
USEFORM("en\zona_obzora\Unit5.cpp", FDataSVer);
USEFORM("en\zona_obzora\Unit6.cpp", FDataSOtch);
USEFORM("en\iskaj\DanOdinBort.cpp", FDanOdinBort);
USEFORM("en\iskaj\GlavFFor631.cpp", FGlav631);
USEFORM("en\iskaj\IskajNomView.cpp", FIskajNomView);
USEFORM("en\iskaj\IskajVysotaData.cpp", FIskajVysotaData);
USEFORM("en\iskaj\NewNomer.cpp", FNewNomer);
USEFORM("en\iskaj\PreviewIskaj.cpp", PreviewFormIskaj);
USEFORM("en\iskaj\VyvodData.cpp", FOData);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{


        AnalisisMain();
        try
        {
                 Application->Initialize();
                 Application->CreateForm(__classid(TFGlavOkno), &FGlavOkno);
                 Application->CreateForm(__classid(TFDebug), &FDebug);
                 Application->CreateForm(__classid(TFAbout), &FAbout);
                 Application->CreateForm(__classid(TFDataTraekt), &FDataTraekt);
                 Application->CreateForm(__classid(TFExitAsk), &FExitAsk);
                 Application->CreateForm(__classid(TFUserInfo), &FUserInfo);
                 Application->CreateForm(__classid(TFMaxDalnostGOST), &FMaxDalnostGOST);
                 Application->CreateForm(__classid(TFNastroiki), &FNastroiki);
                 Application->CreateForm(__classid(TF_Param_P), &F_Param_P);
                 Application->CreateForm(__classid(TF_Param_S), &F_Param_S);
                 Application->CreateForm(__classid(TFSpisokFiles), &FSpisokFiles);
                 Application->CreateForm(__classid(TForm3), &Form3);
                 Application->CreateForm(__classid(TFDataSVer), &FDataSVer);
                 Application->CreateForm(__classid(TFVydelZona), &FVydelZona);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
