//---------------------------------------------------------------------------
#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
struct OTCHET
{
  long NomerBorta;
  long Vysota;
  double Dalnost;
  double Azimut;
  double Time;
  bool Exists;      
  bool Bokovoy;  //���� Exist == false, � Bokovye=true -- �� ��� ������� �������
                 //����������� ������ ��� �����, �� ���� ���������� ����������
  long IsSNomerForP;   //��� ���������� ������
//  long  TypeKanala;   //��� ������ - 00  //�� PS, 1 - S, 2 - P, 3 - PS.
  long Znack;


};


extern "C" __declspec(dllexport)
void __stdcall SetHelpFileName(
                   char *HlpName  //��� ����� ������
                           );

extern "C" __declspec(dllexport)
void __stdcall SetDataDoc(char *DataStr);

extern "C" __declspec(dllexport)
void __stdcall SetDannyeDoc(char *DataStr);



extern "C" __declspec(dllexport)
void __stdcall InitializeM(
                 double TempObzor1, //� �����
                 long Kanal,        //�����
                 int max_daln_km    //������������ ��������� � ��
                );


extern "C" __declspec(dllexport)
void __stdcall AddOtchet(long NomerBort, //����� �����
                         double Azimut,   //������
                         double Dalnost,  //���������
                         double Time,     //�����
                         long Vysota      //������ 
                         );


extern "C" __declspec(dllexport)
void __stdcall AddOtchetPropusk(long NomerBort, //����� �����
    //                    long Vysota,
                        double Azimut,
                        double Dalnost,
                        double Time);



extern "C" __declspec(dllexport)
void __stdcall ZakonchitObrabotku(void);

extern "C" __declspec(dllexport)
void __stdcall ZakonchitObrabotkuP(void);




extern "C" __declspec(dllexport)
void __stdcall InitializePZone(void);


extern "C" __declspec(dllexport)
void __stdcall AddPZone(double KM, double N_of_P, double N_of_All);


extern "C" __declspec(dllexport)
void __stdcall AddPOtchet(
                          double Azimut,
                          double Dalnost,
                          long PS,
                          double Time,
                          long NB);


extern "C" __declspec(dllexport)
void __stdcall PodschetPVeroyatnosti(void);



extern "C" __declspec(dllexport)
void __stdcall VyvodGrafikPZone(void);


extern "C" __declspec(dllexport)
void __stdcall SetRazmerOknaDlyaZony(double sz_km);


void __stdcall FreeAll(void);

int MakeDataP_S(void);

//������� ������ �� �������
int MakeDataO_S(void);
int MakeDataO_S_N(void);


int MakeDataP_P(void);


int MakeDataO_P(void);

void __stdcall VeroyatnostiBortov(void);

void __stdcall SliyaniePovtorno(bool Proverka);
void __stdcall PereschitatVeroyatnost(bool Proverka088,bool Prolet);
void __stdcall PereschitatVeroyatnostP(bool Proverka088);


void __stdcall  PereschitatVeroyatnostForTrass(bool IsProlet);


#endif
