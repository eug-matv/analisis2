//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop


#include <alloc.h>
#include <math.h>
#include <mem.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "NotPriamZone.h"
#include "ToolsForZonaObz.h"
#include "Unit2.h"
#include "Unit3.h"
#include "UDebug.h"
#include "Unit5.h"
#include "Unit6.h"
#include "Unit7.h"



#define MAX_OTCHBORTS 1500
#define MAX_OTCHBORTS_S 500
#define ABS(X) (((X) > 0) ? (X) : (-(X)))
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))
#define SIGN(X) (((X) > 0) ? (1) : (-1))


//---------------------------------------------------------------------------
#pragma package(smart_init)

AnsiString HelpFileNameZO; //��� ����� ������
AnsiString DateStrka; //������ ����������� ������
AnsiString DannyeStrka; //������ ����������� ������


/*���������� WasErrorAddOtchet ������������� � ������ ������������� ������*/
bool WasErrorAddOtchet=false;



static long KnlD;           //��������� ��� ��������� ����� ���������
static OTCHET **Otch=NULL; //������ �������� � ������ ������
static OTCHET *LastOtch=NULL; //������ �������� ������ N_of_Otch

static int N_of_Otch=0;       //����� ������ (������)
static int *N_of_Otch2=NULL;  //������ ��������� ��������
static int *MaxN_of_Otch2=NULL; //������ ���� �������� ��������� ��������
static int *N_of_Otch3=NULL; //������ ��������� �������� ����� ����������
static int *Vsyo=NULL;       //���� 1, �� ����� ������ �� ���������
static double TempObzor;      //���� ������
static OTCHET *ObshOtch=NULL; //����� ������ ��������
static OTCHET *ObshOtch1=NULL;
static bool IskluchitBadBorta=false;  //��������� ������ �����
static bool ProletayushieTrassy=false;

//��������� �
static double dMaxDalnGOST;             //������������ ��������� �� ����������



//������������ ��������� - ������ ������� ������� �� �������������
static double dMaxDalnostOfExist;




int N_of_ObshOtch=0;

int     *N_of_All=NULL;
int     *N_of_S=NULL;
int     *N_of_P=NULL;
double  *PO=NULL;  //����������� �����������


//������� ����������� ����������� �� ���� ��������� ����� ��� ������� �� ������
long *NomeraBortov=NULL;  //������ ������
double *Psred=NULL;        //������� ����������� ��� ������� �����
long N_of_Borts=0;          //����� ������
double PsredAll;         //������� ����������� �� ���� ������



double DD1;


double MaxZone, MinZone;
double ZonaForS=0.9;               //��� ����������� ����
double BlijOtschUgol;     //���� ������ �������� �������
long BlijVysota;          //������
double BlijPOtschD, DalnPOtschD;     //��������� ������ �������� � ���������� ��������
double FirstSpad;        //��������� 2008.07.17 - ������ �������� ��������� �����
double SecondPodjem;     //��������� 2008.07.17 - ������ �������� �������

static double SredSkorostObzor;



//�������� ������� ������
double SrednyayaVysota=0.0;


bool IskluchDad=false;

/*� ��� ����� ������ ��������*/
bool IsVyvodMaxD=true,IsVyvodMinD=true, IsVyvodMaxUgol=true, IsVyvodFirstSpad=true;

/*������ ����������� ����. ������� 28.08.2008*/
double sizeOfOkno_km=10.0;




/*������ ��� ��������� ������*/
class TExcepAddOtchet
{
  public:
   TExcepAddOtchet(int Index)
   {
     Index1=Index;
   };
  int Index1;

};


/*����� ��� ��������� ���������� ������ � �������*/
class TExceptionUnit2
{
  int D;
  char Comment[100];
  public:
   TExceptionUnit2(int D1,char *Comment1)
   {
     D=D1;
     strcpy(Comment, Comment1);
   };
   void MessageBox(void)
   {
     ::MessageBox(NULL,Comment,String(D).c_str(),MB_OK);
   };

};



//�������� ����� ����� ������
void __stdcall SetHelpFileName(char *HlpName)
{
  HelpFileNameZO=String(HlpName);


}


//���� ������������
void __stdcall SetDataDoc(char *DataStr)
{
  DateStrka=String(DataStr).Trim();
  if(DateStrka.Length()==1&&DateStrka[1]=='1')
  {
    DateStrka="";
  }
}

//���� ������������
void __stdcall SetDannyeDoc(char *DataStr)
{
  DannyeStrka=String(DataStr).Trim();
  if(DannyeStrka.Length()==1&&DannyeStrka[1]=='1')
  {

   DannyeStrka="";
  }

}







void __stdcall InitializeM(
                 double TempObzor1, //� �����
                 long Kanal,        //�����
                 int max_daln_km    //������������ ��������� � ��
                )
{

  FreeAll();
  TempObzor=TempObzor1;
  KnlD=Kanal;
  WasErrorAddOtchet=false;
  IskluchitBadBorta=false;
  dMaxDalnGOST=max_daln_km;
  dMaxDalnostOfExist=-1.0;


}




/*�������� ������ ���������� ������*/
void __stdcall AddOtchet(long NomerBort, //����� �����
                         double Azimut,   //������
                         double Dalnost,  //���������
                         double Time,     //�����
                         long Vysota      //������
                         )
{
  int i,j;
  int Zn;
  double DD,DT;
  double X1,Y1,X2,Y2,Xp,Yp,dX,dY,Ap,Dp,Tp,dR;
  AnsiString Strka;
  long DObzor;

  if(WasErrorAddOtchet)
  {
     return;
  }
  Strka=FloatToStr(Dalnost);


  __try
  {

//��������� ����� ������ ������
  if(N_of_Otch==0) //����� ������ ����� -
  {
    Otch=(OTCHET**)malloc(sizeof(OTCHET*));
    if(Otch==NULL)
    {

    }

    Otch[0]=(OTCHET*)malloc(sizeof(OTCHET)*MAX_OTCHBORTS_S);
    N_of_Otch=1;
    N_of_Otch2=(int*)malloc(sizeof(int));
    MaxN_of_Otch2=(int*)malloc(sizeof(int));
    Vsyo=(int*)malloc(sizeof(int));
    N_of_Otch2[0]=1;
    MaxN_of_Otch2[0]=MAX_OTCHBORTS_S;
    Vsyo[0]=0;
    Otch[0][0].NomerBorta=NomerBort;
    Otch[0][0].Azimut=Azimut;
    Otch[0][0].Dalnost=Dalnost;
    Otch[0][0].Time=Time;
    Otch[0][0].Exists=true;
    Otch[0][0].Znack=0;
    Otch[0][0].IsSNomerForP=0;
    Otch[0][0].Vysota=Vysota;
    dMaxDalnostOfExist=Otch[0][0].Dalnost;

    return;
  }


  if(Dalnost>dMaxDalnostOfExist)
  {
        dMaxDalnostOfExist=Dalnost;
  }

//������ ��������, � ��� �� ������ ������� ����� ��������
  for(i=0;i<N_of_Otch;i++)
  {

  //����� ��� �����
    if(Otch[i][N_of_Otch2[i]-1].NomerBorta==NomerBort) //���� ��������� ����� ��� ������
    {
      if(Vsyo[i])continue;   //���� ������ ����� �� ����������� - ������ �����



      DD=Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost; //�� ������� ���������� ���������
      if(DD>0)Zn=1;   //��� ������� ��������� - ������ �������� �� ���
        else
      if(DD<0)Zn=-1;  //������ ����������� � ���
        else  Zn=0;



      DT=Time-Otch[i][N_of_Otch2[i]-1].Time;  //��������� ������� � �

      X2=Dalnost*SinG(Azimut);
      Y2=Dalnost*CosG(Azimut);
      X1=Otch[i][N_of_Otch2[i]-1].Dalnost*
           SinG(Otch[i][N_of_Otch2[i]-1].Azimut);
      Y1=Otch[i][N_of_Otch2[i]-1].Dalnost*
           CosG(Otch[i][N_of_Otch2[i]-1].Azimut);
      dR=sqrt((X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2));
      DObzor=Okrugl(DT/TempObzor);
      if(ABS(DT)>=0.1)
      {
         Vsyo[i]=1;
         continue;
      }


      if(DObzor>1)
      {
 //����� ��������� ������� ���� ��� ��������
 //����� ������� ��� ���� �������� �� ������
         dX=(X2-X1)/DObzor;
         dY=(Y2-Y1)/DObzor;
         Xp=X1;
         Yp=Y1;
         Tp=Otch[i][N_of_Otch2[i]-1].Time;
         for(j=1;j<DObzor;j++)
         {
           if(N_of_Otch2[i]>=MaxN_of_Otch2[i])
           {
             MaxN_of_Otch2[i]+=MAX_OTCHBORTS_S;
             Otch[i]=(OTCHET*)realloc(Otch[i],MaxN_of_Otch2[i]*sizeof(OTCHET));
           }
           Xp+=dX;
           Yp+=dY;
           Tp+=TempObzor;
 //��������� ���������� �������� ���������� �� ����������
           DekartToPolyar(Yp,Xp,&Ap,&Dp);
           Otch[i][N_of_Otch2[i]].NomerBorta=NomerBort;
           Otch[i][N_of_Otch2[i]].Azimut=OkruglSot(Ap);
           Otch[i][N_of_Otch2[i]].Dalnost=OkruglSot(Dp);
           Otch[i][N_of_Otch2[i]].Time=Tp;
           Otch[i][N_of_Otch2[i]].Exists=false;
           Otch[i][N_of_Otch2[i]].Bokovoy=false;
           Otch[i][N_of_Otch2[i]].Znack=Zn;
           Otch[i][N_of_Otch2[i]].IsSNomerForP=0;
           Otch[i][N_of_Otch2[i]].Vysota=0;
           N_of_Otch2[i]++;
         }
      }else if(DObzor==0)
      {
        if(KnlD==2)
        {

          return;
        }else{
           if(N_of_Otch2[i]-1>=0)
           {
         //��������, � �� PS �� ����� �����
              if(Otch[i][N_of_Otch2[i]-1].IsSNomerForP==1)
              {
           //����� ���� �������� �����
                 Otch[i][N_of_Otch2[i]-1].NomerBorta=NomerBort;
                 Otch[i][N_of_Otch2[i]-1].Azimut=OkruglSot(Azimut);
                 Otch[i][N_of_Otch2[i]-1].Dalnost=OkruglSot(Dalnost);
                 Otch[i][N_of_Otch2[i]-1].Time=Time;
                 Otch[i][N_of_Otch2[i]-1].Exists=true;
                 Otch[i][N_of_Otch2[i]-1].Znack=Zn;
                 Otch[i][N_of_Otch2[i]-1].IsSNomerForP=0;
                 Otch[i][N_of_Otch2[i]-1].Vysota=Vysota;

              }
           }
           return;
        }
        return;
      }


//������� ����� �����
      if(N_of_Otch2[i]>=MaxN_of_Otch2[i])
      {
           MaxN_of_Otch2[i]+=MAX_OTCHBORTS_S;
           Otch[i]=(OTCHET*)realloc(Otch[i],MaxN_of_Otch2[i]*sizeof(OTCHET));
      }
      Otch[i][N_of_Otch2[i]].NomerBorta=NomerBort;
      Otch[i][N_of_Otch2[i]].Azimut=OkruglSot(Azimut);
      Otch[i][N_of_Otch2[i]].Dalnost=OkruglSot(Dalnost);
      Otch[i][N_of_Otch2[i]].Time=Time;
      Otch[i][N_of_Otch2[i]].Exists=true;
      Otch[i][N_of_Otch2[i]].Znack=Zn;
      Otch[i][N_of_Otch2[i]].IsSNomerForP=0;
      Otch[i][N_of_Otch2[i]].Vysota=Vysota;

      N_of_Otch2[i]++;
      if(N_of_Otch2[i]==2)
      {
        Otch[i][0].Znack=Zn;
      }
      return;
    }
  }

//� ���, ���� ����� ����� �� ���� �������
  Otch=(OTCHET**)realloc(Otch,sizeof(OTCHET*)*(N_of_Otch+1));
  N_of_Otch2=(int*)realloc(N_of_Otch2,sizeof(int)*(N_of_Otch+1));
  MaxN_of_Otch2=(int*)realloc(MaxN_of_Otch2,sizeof(int)*(N_of_Otch+1));
  Vsyo      =(int*)realloc(Vsyo,sizeof(int)*(N_of_Otch+1));

//���� �� ������ ����� � ����� ������� �� ���� �������, �� �������� ����� �����
// � ����� �������

  Otch[N_of_Otch]=(OTCHET*)malloc(sizeof(OTCHET)*MAX_OTCHBORTS_S);
  N_of_Otch2[N_of_Otch]=1;
  MaxN_of_Otch2[N_of_Otch]=MAX_OTCHBORTS_S;
  Vsyo[N_of_Otch]=0;
  Otch[N_of_Otch][0].NomerBorta=NomerBort;
  Otch[N_of_Otch][0].Azimut=Azimut;
  Otch[N_of_Otch][0].Dalnost=Dalnost;
  Otch[N_of_Otch][0].Time=Time;
  Otch[N_of_Otch][0].Exists=true;
  Otch[N_of_Otch][0].Bokovoy=false;
  Otch[N_of_Otch][0].Znack=0;
  Otch[N_of_Otch][0].IsSNomerForP=0;
  Otch[N_of_Otch][0].Vysota=Vysota;

  N_of_Otch++;
  }
  catch(TExcepAddOtchet &e)
  {
     if(e.Index1==1)
     {
       FreeAll();
       WasErrorAddOtchet=true;
       MessageBox(NULL,"Error function AddOtchet",
                       "Memory error!", MB_OK);
       return;
     }
  };

}




/*�������� ������ ���������� ������ � �������� ��� ��� �����������*/
void __stdcall AddOtchetPropusk(long NomerBort, //����� �����
                        double Azimut,   //������
                        double Dalnost,  //���������
                        double Time)     //�����
{
  int i,j;
  int Zn;
  double DD,DT;
  double X1,Y1,X2,Y2,Xp,Yp,dX,dY,Ap,Dp,Tp;
  AnsiString Strka;


  long DObzor;
  Strka=FloatToStr(Dalnost);


//��������� ����� ������ ������
  if(N_of_Otch==0) //����� ������ ����� -
  {
    Otch=(OTCHET**)malloc(sizeof(OTCHET*));
    Otch[0]=(OTCHET*)malloc(sizeof(OTCHET)*MAX_OTCHBORTS_S);
    N_of_Otch=1;
    N_of_Otch2=(int*)malloc(sizeof(int));
    MaxN_of_Otch2=(int*)malloc(sizeof(int));
    Vsyo=(int*)malloc(sizeof(int));
    N_of_Otch2[0]=1;
    MaxN_of_Otch2[0]=MAX_OTCHBORTS_S;
    Vsyo[0]=0;
    Otch[0][0].NomerBorta=NomerBort;
    Otch[0][0].Azimut=Azimut;
    Otch[0][0].Dalnost=Dalnost;
    Otch[0][0].Time=Time;
    Otch[0][0].Exists=false;
    Otch[0][0].Bokovoy=false;
    Otch[0][0].Znack=0;
    Otch[0][0].IsSNomerForP=1;

    return;
  }


//������ ��������, � ��� �� ������ ������� ����� ��������
  for(i=0;i<N_of_Otch;i++)
  {

  //����� ��� �����
    if(Otch[i][N_of_Otch2[i]-1].NomerBorta==NomerBort) //���� ��������� ����� ��� ������
    {
      if(Vsyo[i])continue;   //���� ������ ����� �� ����������� - ������ �����




      DD=Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost; //�� ������� ���������� ���������
      if(DD>0)Zn=1;   //��� ������� ��������� - ������ �������� �� ���
        else
      if(DD<0)Zn=-1;  //������ ����������� � ���
        else  Zn=0;



      DT=Time-Otch[i][N_of_Otch2[i]-1].Time;  //��������� ������� � �

      X2=Dalnost*SinG(Azimut);
      Y2=Dalnost*CosG(Azimut);
      X1=Otch[i][N_of_Otch2[i]-1].Dalnost*
           SinG(Otch[i][N_of_Otch2[i]-1].Azimut);
      Y1=Otch[i][N_of_Otch2[i]-1].Dalnost*
           CosG(Otch[i][N_of_Otch2[i]-1].Azimut);
      DObzor=Okrugl(DT/TempObzor);
      if(ABS(DT)>=0.1)
      {
         Vsyo[i]=1;
         continue;
      }


      if(DObzor>1)
      {
 //����� ��������� ������� ���� ��� ��������
 //����� ������� ��� ���� �������� �� ������
         dX=(X2-X1)/DObzor;
         dY=(Y2-Y1)/DObzor;
         Xp=X1;
         Yp=Y1;
         Tp=Otch[i][N_of_Otch2[i]-1].Time;
         for(j=1;j<DObzor;j++)
         {
           if(N_of_Otch2[i]>=MaxN_of_Otch2[i])
           {
             MaxN_of_Otch2[i]+=MAX_OTCHBORTS_S;
             Otch[i]=(OTCHET*)realloc(Otch[i],MaxN_of_Otch2[i]*sizeof(OTCHET));
           }
           Xp+=dX;
           Yp+=dY;
           Tp+=TempObzor;
 //��������� ���������� �������� ���������� �� ����������
           DekartToPolyar(Yp,Xp,&Ap,&Dp);
           Otch[i][N_of_Otch2[i]].NomerBorta=NomerBort;
           Otch[i][N_of_Otch2[i]].Azimut=Ap;
           Otch[i][N_of_Otch2[i]].Dalnost=Dp;
           Otch[i][N_of_Otch2[i]].Time=Tp;
           Otch[i][N_of_Otch2[i]].Exists=false;
           Otch[i][N_of_Otch2[i]].Bokovoy=false;
           Otch[i][N_of_Otch2[i]].Znack=Zn;
           Otch[i][N_of_Otch2[i]].IsSNomerForP=1;

           N_of_Otch2[i]++;
         }
      }else if(DObzor==0)
      {
         return;
      }


//������� ����� �����
      if(N_of_Otch2[i]>=MaxN_of_Otch2[i])
      {
           MaxN_of_Otch2[i]+=MAX_OTCHBORTS_S;
           Otch[i]=(OTCHET*)realloc(Otch[i],MaxN_of_Otch2[i]*sizeof(OTCHET));
      }
      Otch[i][N_of_Otch2[i]].NomerBorta=NomerBort;
      Otch[i][N_of_Otch2[i]].Azimut=Azimut;
      Otch[i][N_of_Otch2[i]].Dalnost=Dalnost;
      Otch[i][N_of_Otch2[i]].Time=Time;
      Otch[i][N_of_Otch2[i]].Exists=false;
      Otch[i][N_of_Otch2[i]].Bokovoy=false;
      Otch[i][N_of_Otch2[i]].Znack=Zn;
      Otch[i][N_of_Otch2[i]].IsSNomerForP=1;

      N_of_Otch2[i]++;
      if(N_of_Otch2[i]==2)
      {
        Otch[i][0].Znack=Zn;
      }
      return;
    }
  }
//� ���, ���� ����� ����� �� ���� �������
  Otch=(OTCHET**)realloc(Otch,sizeof(OTCHET*)*(N_of_Otch+1));
  N_of_Otch2=(int*)realloc(N_of_Otch2,sizeof(int)*(N_of_Otch+1));
  MaxN_of_Otch2=(int*)realloc(MaxN_of_Otch2,sizeof(int)*(N_of_Otch+1));
  Vsyo      =(int*)realloc(Vsyo,sizeof(int)*(N_of_Otch+1));

//���� �� ������ ����� � ����� ������� �� ���� �������, �� �������� ����� �����
// � ����� �������

  Otch[N_of_Otch]=(OTCHET*)malloc(sizeof(OTCHET)*MAX_OTCHBORTS_S);
  N_of_Otch2[N_of_Otch]=1;
  MaxN_of_Otch2[N_of_Otch]=MAX_OTCHBORTS_S;
  Vsyo[N_of_Otch]=0;
  Otch[N_of_Otch][0].NomerBorta=NomerBort;
  Otch[N_of_Otch][0].Azimut=Azimut;
  Otch[N_of_Otch][0].Dalnost=Dalnost;
  Otch[N_of_Otch][0].Time=Time;
  Otch[N_of_Otch][0].Exists=false;
  Otch[N_of_Otch][0].Bokovoy=false;
  Otch[N_of_Otch][0].Znack=0;
  Otch[N_of_Otch][0].IsSNomerForP=1;
  N_of_Otch++;
}





int sort_function1( const void *a, const void *b)
{
  OTCHET *o1,*o2;
  o1=(OTCHET*)a;
  o2=(OTCHET*)b;

  if(o1->Dalnost>o2->Dalnost)return 1;
  if(o1->Dalnost<o2->Dalnost)return (-1);
  return 0;
}


void SortOtchetDaln(OTCHET *Otchs,int N_of_Otchs)
{
  qsort(Otchs,N_of_Otchs, sizeof(OTCHET), sort_function1);
}


/*��� ��������� ��������� ������ �������� ������������ ���������*/
void __stdcall Dopolnenie(void)
{
  int i,j,k;
  bool RetB;
  double DTime,DDaln,DDaln2;
  double MinDaln;
  AnsiString Strka;




  long DObz;           //�� ������� ��������� �����
  double TO1;    //���� ������������ ���� � ���, �� TO1=-TO, ���� ��������� TO1=TO
  double Dlnst,Azmt;  //�������� ������� � ���������
  double PrevDlnst;   //���������� �������� ���������
  double Tm;       //����� ������������ ��� ������ ����� ����� �������
  double X1,Y1,X2,Y2;
  double VX,VY;   //�������� ��������� X � Y
  double Skorost,Do1,DT;  // ��� ��������
  int *DeletedVetvi;
  double *NewD=NULL,*NewA=NULL;
  double MaxDaln;
  int IndexMinDaln;
  int State=3;
  double MaxDalnForDaln; //������������ �������� ���������
  double MinDalnForDaln; //����������� �������� ���������
//  int DObzor;
 // OTCHET PromOtchs[10];
//  int N_of_PromOtchs=0;

/*
  for (i=0;i<N_of_Otch;i++)
  {
    for(j=1;j<N_of_Otch2[i];j++)
    {
       DObzor=Okrugl((Otch[i][j].Time-Otch[i][j-1].Time)/TempObzora);
       if(DObzor==0)
       {
     //��������� ����� ��������� �������� ��� ���������

          for(k=j;k<N_of_Otch2[i];k++)
          {

          }
       }
    }
  }
  */

  N_of_Otch3=(int*)malloc(sizeof(int)*N_of_Otch);
  if(N_of_Otch3==NULL)throw TExceptionUnit2(1,"Function Dopolnenie, error: malloc N_of_Otch3");
  for(i=0;i<N_of_Otch;i++)
  {
     N_of_Otch3[i]=N_of_Otch2[i];
  }

//�������� ������ ����� �� ��������
//������� ����������� ������������ ����������� ��� �������� �� ���
  for(i=0;i<N_of_Otch;i++)
  {

     Otch[i][0].Znack=0;
     for(j=1;j<N_of_Otch3[i];j++)
     {
       if(Otch[i][j].Dalnost>Otch[i][j-1].Dalnost)
       {
          if(Otch[i][j-1].Znack>-1)
          {
             Otch[i][j].Znack=1;
          }else{
             Otch[i][j].Znack=0;
          }
       }else if(Otch[i][j].Dalnost<Otch[i][j-1].Dalnost)
       {
          if(Otch[i][j-1].Znack<1)
          {
             Otch[i][j].Znack=-1;
          }else{
             Otch[i][j].Znack=0;
          }
       }else{
          Otch[i][j].Znack=0;
       }
     }
  }



  k=0;
  for(i=0;i<N_of_Otch;i++)
  {
     MinDaln=10000;
     IndexMinDaln=-1;
     for(j=0;j<N_of_Otch3[i+k];j++)
     {
        if(Otch[i+k][j].Dalnost<MinDaln)
        {
           MinDaln=Otch[i+k][j].Dalnost;
           IndexMinDaln=j;
        }
     }
     if(IndexMinDaln==0||IndexMinDaln==N_of_Otch3[i+k]-1)
     {
        continue;
     }

 //� ��� ���������� ����� ��� ��������� ��� �����

//Excpt2
     Otch=(OTCHET**)realloc(Otch,sizeof(OTCHET*)*(N_of_Otch+k+1));
     if(Otch==NULL)throw TExceptionUnit2(2,"Dopolnenie,  realloc, Otch");
     N_of_Otch2=(int*)realloc(N_of_Otch2,sizeof(int)*(N_of_Otch+k+1));
     if(N_of_Otch2==NULL)throw TExceptionUnit2(2,"Dopolnenie,  realloc, N_of_Otch2");
     N_of_Otch3=(int*)realloc(N_of_Otch3,sizeof(int)*(N_of_Otch+k+1));
     MaxN_of_Otch2=(int*)realloc(MaxN_of_Otch2,sizeof(int)*(N_of_Otch+k+1));
     if(MaxN_of_Otch2==NULL)throw TExceptionUnit2(2,"Dopolnenie,  realloc, MaxN_of_Otch2");

     for(j=N_of_Otch+k;j>i+k+1;j--)
     {
       Otch[j]=Otch[j-1];
       N_of_Otch2[j]=N_of_Otch2[j-1];
       N_of_Otch3[j]=N_of_Otch3[j-1];
       MaxN_of_Otch2[j]=MaxN_of_Otch2[j-1];
     }

     MaxN_of_Otch2[i+k+1]=MaxN_of_Otch2[i+k];
     Otch[i+k+1]=(OTCHET*)malloc(sizeof(OTCHET)*MaxN_of_Otch2[i+k+1]);
     memcpy(Otch[i+k+1],
             Otch[i+k]+IndexMinDaln,
             sizeof(OTCHET)*(N_of_Otch3[i+k]-IndexMinDaln));
     N_of_Otch3[i+k+1]=N_of_Otch3[i+k]-IndexMinDaln;
     N_of_Otch3[i+k]=IndexMinDaln;
     k++;
  }
  N_of_Otch+=k;

  for(i=0;i<N_of_Otch;i++)
  {
    N_of_Otch2[i]=0;
    for(j=0;j<N_of_Otch3[i];j++)
    {
      if(Otch[i][j].Exists)
      {
         N_of_Otch2[i]++;
      }
    }
  }



  for(i=0;i<N_of_Otch;i++)
  {

    SortOtchetDaln(Otch[i],N_of_Otch3[i]);
  }


    MaxDalnForDaln=0;
    MinDalnForDaln=1000000;
    for(i=0;i<N_of_Otch;i++)
    {
      if(N_of_Otch2[i]<15)continue;
      if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;


      if(Otch[i][N_of_Otch3[i]-1].Dalnost>MaxDalnForDaln)
      {
        MaxDalnForDaln=Otch[i][N_of_Otch3[i]-1].Dalnost;
      }

      if(Otch[i][0].Dalnost<MinDalnForDaln)
      {
        MinDalnForDaln=Otch[i][0].Dalnost;
      }
    }
    for(i=0;i<N_of_Otch;i++)
    {

      //MaxDaln=MIN(MaxDalnForDaln+sizeOfOkno_km,dMaxDalnGOST);
      MaxDaln=dMaxDalnostOfExist;

      if(N_of_Otch2[i]<15)continue;
      if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;
  //������ DDaln
      X1=Otch[i][0].Dalnost*SinG(Otch[i][0].Azimut);
      Y1=Otch[i][0].Dalnost*CosG(Otch[i][0].Azimut);
      X2=Otch[i][N_of_Otch3[i]-1].Dalnost*SinG(Otch[i][N_of_Otch3[i]-1].Azimut);
      Y2=Otch[i][N_of_Otch3[i]-1].Dalnost*CosG(Otch[i][N_of_Otch3[i]-1].Azimut);
      Do1=sqrt((X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2));
      DT=ABS(Otch[i][0].Time-Otch[i][N_of_Otch3[i]-1].Time);
      if(DT<TempObzor)
      {
        continue;
      }

      Skorost=Do1/DT*TempObzor;
      SredSkorostObzor=Skorost;
      DDaln=0.0;
      k=N_of_Otch3[i]-1;
      while(ABS(DDaln)<0.1)
      {
        if(k<1)break;

        DDaln=Otch[i][k].Dalnost-Otch[i][k-1].Dalnost;



      //  Form3->Label3->Caption=FloatToStr(DDaln);
        k--;
      }



      if(ABS(DDaln)<0.1)continue;

      Strka=FloatToStr(DDaln)+String(" ")+
            FloatToStr(Otch[i][N_of_Otch3[i]-2].Dalnost)+String(" ")+
            FloatToStr(Otch[i][N_of_Otch3[i]-1].Dalnost);

      double fx,fy,Dt,NewDaln,NewAzmt/*,NewTime*/;



  //��������� ���� �� ������ ����� MaxDaln ��
      if(Otch[i][1].Time>Otch[i][2].Time)
      {
        Dt=-TempObzor;
      }else{
        Dt=TempObzor;
      }
      do
      {

          if(N_of_Otch3[i]>=MaxN_of_Otch2[i]-1)
          {
             MaxN_of_Otch2[i]+=MAX_OTCHBORTS_S;
             Otch[i]=(OTCHET*)realloc(Otch[i],sizeof(OTCHET)*MaxN_of_Otch2[i]);
          }

         if(DDaln<0.100)break;

         NewDaln=Otch[i][N_of_Otch3[i]-1].Dalnost+Skorost;
         if(NewDaln>MaxDaln)break;
         NewAzmt=Otch[i][N_of_Otch3[i]-1].Azimut;
         X1=NewDaln*SinG(NewAzmt);
         Y1=NewDaln*CosG(NewAzmt);
         if(!IsVnutriNotPriamOblast(X1,-Y1))
         {
//          break;
         }
         Otch[i][N_of_Otch3[i]].Dalnost=
               Otch[i][N_of_Otch3[i]-1].Dalnost+Skorost;

         Otch[i][N_of_Otch3[i]].Time=
               Otch[i][N_of_Otch3[i]-1].Time+Dt;

         Otch[i][N_of_Otch3[i]].NomerBorta=
               Otch[i][N_of_Otch3[i]-1].NomerBorta;

         Otch[i][N_of_Otch3[i]].Azimut=
               Otch[i][N_of_Otch3[i]-1].Azimut;


         Otch[i][N_of_Otch3[i]].Exists=false;
         Otch[i][N_of_Otch3[i]].Bokovoy=true;
         Otch[i][N_of_Otch3[i]].Vysota=0;

         N_of_Otch3[i]++;

//    }while(Otch[i][N_of_Otch3[i]-1].Dalnost<=405.0);
        }while(Otch[i][N_of_Otch3[i]-1].Dalnost<=MaxDaln);





 //� ������ �������� ����� �������
    //������ ��������
/*
      X1=Otch[i][0].Dalnost*sin(Otch[i][0].Azimut/180*M_PI);
      Y1=Otch[i][0].Dalnost*cos(Otch[i][0].Azimut/180*M_PI);
      X2=Otch[i][5].Dalnost*sin(Otch[i][5].Azimut/180*M_PI);
      Y2=Otch[i][5].Dalnost*cos(Otch[i][5].Azimut/180*M_PI);
      DObz=Okrugl((Otch[i][5].Time-Otch[i][0].Time)/TempObzor);


//������ �������� ��������� X � Y
      VX=(X2-X1)/DObz;
      VY=(Y2-Y1)/DObz;
      X1+=VX;
      Y1+=VY;
      if(DObz<0)
      {
        TO1=-TempObzor;
      }else{
        TO1=TempObzor;
      }
      Tm=Otch[i][0].Time-TO1;
      if(Tm<0)Tm=Tm+24;


      DekartToPolyar(Y1,X1,&Azmt,&Dlnst);
      PrevDlnst=Otch[i][0].Dalnost;
      while(Dlnst<=PrevDlnst)
      {
          if(N_of_Otch3[i]>=MaxN_of_Otch2[i]-1)
          {
             MaxN_of_Otch2[i]+=MAX_OTCHBORTS_S;
             Otch[i]=(OTCHET*)realloc(Otch[i],sizeof(OTCHET)*MaxN_of_Otch2[i]);
          }
          Otch[i][N_of_Otch3[i]].NomerBorta=Otch[i][0].NomerBorta;
          Otch[i][N_of_Otch3[i]].Vysota=Otch[i][0].Vysota;
          Otch[i][N_of_Otch3[i]].Dalnost=Dlnst;
          Otch[i][N_of_Otch3[i]].Time=Tm;
          Otch[i][N_of_Otch3[i]].Exists=false;
          Otch[i][N_of_Otch3[i]].Bokovoy=true;
          Otch[i][N_of_Otch3[i]].IsSNomerForP=0;
          Otch[i][N_of_Otch3[i]].Azimut=Azmt;
          N_of_Otch3[i]++;
          if(Dlnst<=MinDalnForDaln)break;
          PrevDlnst=Dlnst;
          X1+=VX;
          Y1+=VY;
          Tm=Tm-TO1;
          if(Tm<0)Tm=Tm+24;
          DekartToPolyar(Y1,X1,&Azmt,&Dlnst);
      }

  */

     if(SrednyayaVysota>0.9)
     {
//����� �������� �����
       int i1;
       double *PDaln;  //������������ ���������
       double PX,PY;
       int N_of_PDaln;
       double *NewPDaln;
       double *NewPAzmt;
       for(i1=0;i1<N_of_Otch3[i];i1++)
       {
         if(Otch[i][i1].Dalnost>50.0)
         {
            N_of_PDaln=i1+1;
            break;
         }
         if(i1==N_of_Otch3[i]-1)
         {
           N_of_PDaln=i1+1;
           break;
         }
       }

     }





//� ������ �������� �����
//��� ����������� ����� ������
//��������� ���� �� ����� ��������� �������, ��� ����������� ��������� ������������
/*
    int k1,k2;




   //������� ��� ���� ����� �� ������ ����� �� ���. ���� ��� �� ���, �� � �� �������
    NewDaln=Otch[i][0].Dalnost-Skorost;
    NewTime=Otch[i][0].Time-Dt;
    while(NewDaln>=0.001)
    {
      if(N_of_Otch3[i]>=MaxN_of_Otch2[i]-1)
      {
           MaxN_of_Otch2[i]+=MAX_OTCHBORTS_S;
           Otch[i]=(OTCHET*)realloc(Otch[i],sizeof(OTCHET)*MaxN_of_Otch2[i]);
      }
      Otch[i][N_of_Otch3[i]].Dalnost=NewDaln;
      Otch[i][N_of_Otch3[i]].Azimut=Otch[i][0].Azimut;
      Otch[i][N_of_Otch3[i]].NomerBorta=Otch[i][0].NomerBorta;
      Otch[i][N_of_Otch3[i]].Exists=false;
      Otch[i][N_of_Otch3[i]].Time=NewTime;
      N_of_Otch3[i]++;
      NewDaln-=Skorost;
      NewTime-=Dt;
    }
*/

      SortOtchetDaln(Otch[i],N_of_Otch3[i]);

    }
  return;

}




//������� � ����������
void __stdcall Sliyanie(void)
{
  int i;
  OTCHET *Prom;
  N_of_ObshOtch=0;

  for(i=0;i<N_of_Otch;i++)
  {
    if((N_of_Otch2[i]<15||ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)
          &&ProletayushieTrassy==false)
    {
      continue;
    }
    N_of_ObshOtch+=N_of_Otch3[i];
  }


  if(N_of_ObshOtch)
  {
    ObshOtch=(OTCHET*)malloc(sizeof(OTCHET)*N_of_ObshOtch);
    Prom=ObshOtch;
    for(i=0;i<N_of_Otch;i++)
    {
      if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;
      if(N_of_Otch2[i]<15)
      {
        continue;
      }
      memcpy(Prom,Otch[i],sizeof(OTCHET)*N_of_Otch3[i]);
      Prom=Prom+N_of_Otch3[i];
    }
  }




//����������� ������
  SortOtchetDaln(ObshOtch,N_of_ObshOtch);


//������� ������ �����  ������� ������ �����
  for(i=0;i<N_of_Otch;i++)
  {
    Otch[i]=(OTCHET*)realloc(Otch[i],sizeof(OTCHET)*N_of_Otch3[i]);
  }
}





void __stdcall FindVeroyatnost(void)
{
  int i,j,k=0,i1;
  double Left,Right,First,Second,First1,i_d;
  double Step;

  int L_i,R_i;

  if(N_of_ObshOtch==0)
  {
    return;
  }

  N_of_All=(int*)realloc(N_of_All,sizeof(int)*N_of_ObshOtch);
  N_of_S=(int*)realloc(N_of_S,sizeof(int)*N_of_ObshOtch);
  PO=(double*)realloc(PO,sizeof(double)*N_of_ObshOtch);





  for(i=i1;i<N_of_ObshOtch;i++)
  {
     N_of_All[i]=0;
     N_of_S[i]=0;
     SchitatChislaOtschetovUnit6(ObshOtch,N_of_ObshOtch,i,
                dMaxDalnostOfExist,
                &(N_of_All[i]),&(N_of_S[i]),&(PO[i]),
                ProletayushieTrassy);

  }
}



//��������� ����������
void __stdcall SliyaniePovtorno(bool Proverka)
{

  int i,j;
  long NomBort;
  double Psred1;
  OTCHET *Prom;
  N_of_ObshOtch=0;

  if(Proverka)
  {
     for(i=0;i<N_of_Otch;i++)
     {

       if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;
       if(N_of_Otch2[i]<15)
       {
         continue;
       }

  //������ �������� �����������
       NomBort=Otch[i][0].NomerBorta;
       for(j=0;j<N_of_Borts;j++)
       {
         if(NomBort==NomeraBortov[j])
         {
            Psred1=Psred[j];
         }
       }
       if(Psred1<PsredAll*0.88)continue;
       N_of_ObshOtch+=N_of_Otch3[i];
     }

     if(N_of_ObshOtch)
     {
       ObshOtch=(OTCHET*)realloc(ObshOtch,sizeof(OTCHET)*N_of_ObshOtch);
       Prom=ObshOtch;
       for(i=0;i<N_of_Otch;i++)
       {
         if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;
         if(N_of_Otch2[i]<15)
         {
           continue;
         }
         NomBort=Otch[i][0].NomerBorta;
         for(j=0;j<N_of_Borts;j++)
         {
           if(NomBort==NomeraBortov[j])
           {
              Psred1=Psred[j];
           }
         }
         if(Psred1<PsredAll*0.88)continue;

         memcpy(Prom,Otch[i],sizeof(OTCHET)*N_of_Otch3[i]);
         Prom=Prom+N_of_Otch3[i];
       }

     }

  }else{
     for(i=0;i<N_of_Otch;i++)
     {
       if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;
       if(N_of_Otch2[i]<15)
       {
         continue;
       }
       N_of_ObshOtch+=N_of_Otch3[i];
     }


     if(N_of_ObshOtch)
     {
       ObshOtch=(OTCHET*)realloc(ObshOtch,sizeof(OTCHET)*N_of_ObshOtch);
       Prom=ObshOtch;
       for(i=0;i<N_of_Otch;i++)
       {
         if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;
         if(N_of_Otch2[i]<15)
         {
           continue;
         }
         memcpy(Prom,Otch[i],sizeof(OTCHET)*N_of_Otch3[i]);
         Prom=Prom+N_of_Otch3[i];
       }
     }
  }



//����������� ������
   SortOtchetDaln(ObshOtch,N_of_ObshOtch);


//������� ������ �����  ������� ������ �����


}




void MakeGraphik(void)
{
  int i,j,k;
  long NBs[600],NewNB;
  int N_of_NBs=0;
  AnsiString StrkaBorts,StrkaBorts1,Strka1Brt;
  AnsiString NijnyayaStroka="";

 char Strka[220];
  Form3->Chart1->OnResize=NULL;

  if(KnlD==2)
  {
    Form3->Caption="Secondary channel coverage area";
    Form3->Kanal=2;
  }else{
    Form3->Caption="Primary channel coverage area";
    Form3->Kanal=1;
  }

  Form3->MDan2->Visible=true;

  Form3->Series1->Clear();
  Form3->Series2->Clear();
  Form3->Series3->Clear();
  Form3->Series4->Clear();
  Form3->Series5->Clear();
  Form3->Series6->Clear();
  Form3->Series7->Clear();
  Form3->Series8->Clear();

  Form3->Chart1->Title->Text->Clear();
  if(KnlD==2)
  {
    Form3->Chart1->Title->Text->Add("Secondary channel coverage area");
  }else{
    Form3->Chart1->Title->Text->Add("Primary channel coverage area");
  }
  if(DateStrka.Length())
  {
    Form3->Chart1->Title->Text->Add(String("Date(s): ")+DateStrka);
  }

  if(DannyeStrka.Length())
  {
    Form3->Chart1->Title->Text->Add(DannyeStrka);
  }


//������� ������ ������
  if(N_of_Otch>0)
  {
     for(i=0;i<N_of_Otch;i++)
     {
       NewNB=Otch[i][0].NomerBorta;
       if(ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)continue;
       if(N_of_Otch2[i]<15)continue;

//���� ����� ���� ��������� �� �������� ������ ����
       if(IskluchitBadBorta)
       {
          long NomBort;
          double Psred1;
          NomBort=Otch[i][0].NomerBorta;

          for(j=0;j<N_of_Borts;j++)
          {
            if(NomBort==NomeraBortov[j])
            {
               Psred1=Psred[j];
            }
          }
          if(Psred1<PsredAll*0.88)
          {
            NewNB=-NewNB;
          }
      }

       for(j=0;j<N_of_NBs;j++)
       {

          if(ABS(NewNB)==ABS(NBs[j]))break;  //���� ��� ��� �������� � ������
          if(ABS(NewNB)<ABS(NBs[j]))
          {
         //������� ������
            for(k=N_of_NBs;k>j;k--)
            {
              NBs[k]=NBs[k-1];
            }
            NBs[j]=NewNB;
            N_of_NBs++;
            break;
          }
       }

       if(j==N_of_NBs) //�� ���� ��������� ������ �����
       {
          NBs[N_of_NBs]=NewNB;
          N_of_NBs++;
       }
     }

     if(NBs[0]<0)
     {
          if((ABS(NBs[0])%100000)==0||(ABS(NBs[0])%100000)==2000||
             (ABS(NBs[0])%100000)==99999)
          {
              StrkaBorts=String("Aircrafts: 1-w.n.-excl");
          }else{
              StrkaBorts=String("Aircrafts: ")+String("1-")+
              String(((int)(-NBs[0]))%100000)+String("-excl");
          }
     }else{
           if((ABS(NBs[0])%100000)==0||(ABS(NBs[0])%100000)==2000||
             (ABS(NBs[0])%100000)==99999)
          {
               StrkaBorts=String("Aircrafts: 1-w.n.");
          }else{
               StrkaBorts=String("Aircrafts: ")+String("1-")+
                String(((int)NBs[0])%100000);
          }
     }
     Form3->Chart1->Canvas->Font->Name="Courier New";
     Form3->Chart1->Canvas->Font->Size=8;
     for(i=1;i<N_of_NBs;i++)
     {
       if(NBs[i]<0)
       {
          if((ABS(NBs[i])%100000)==0||(ABS(NBs[i])%100000)==2000||
             (ABS(NBs[i])%100000)==99999)
          {
              Strka1Brt=String(i+1)+String("-w.n.-excl");
          }else{
              Strka1Brt=String(i+1)+String("-")+
                      String(((int)(-NBs[i]))%100000)+String("-excl");
          }
       }else{
          if((ABS(NBs[i])%100000)==0||(ABS(NBs[i])%100000)==2000||
             (ABS(NBs[i])%100000)==99999)
          {
              Strka1Brt=String(i+1)+String("-w.n.");
          }else{
              Strka1Brt=String(i+1)+String("-")+
                      String(((int)(NBs[i]))%100000);
          }
       }
       StrkaBorts1=StrkaBorts+String(", ")+Strka1Brt;
       if(Form3->Chart1->Canvas->TextWidth(StrkaBorts1)>
          Form3->Chart1->ClientWidth)
       {
             Form3->Chart1->Title->Text->Add(StrkaBorts+String(","));
             StrkaBorts1=Strka1Brt;
       }
       StrkaBorts=StrkaBorts1;

     }
     Form3->Chart1->Title->Text->Add(StrkaBorts);
  }


  if(KnlD==2)
  {
    Form3->Chart1->BottomAxis->Maximum=ObshOtch[N_of_ObshOtch-1].Dalnost+2*sizeOfOkno_km;
  }else{
     Form3->Chart1->BottomAxis->Maximum=ObshOtch[N_of_ObshOtch-1].Dalnost+2*sizeOfOkno_km;
  }

  Form3->Series3->AddXY(-2,0.9,"", clTeeColor);
  Form3->Series3->AddXY(1405,0.9,"", clTeeColor);
  Form3->Series7->AddXY(-2,0.8,"", clTeeColor);
  Form3->Series7->AddXY(1405,0.8,"", clTeeColor);
  Form3->Series8->AddXY(-2,0.7,"", clTeeColor);
  Form3->Series8->AddXY(1405,0.7,"", clTeeColor);

  Form3->Series4->AddXY(-2,40,"", clTeeColor);
  Form3->Series4->AddXY(1405,40,"", clTeeColor);

  if(N_of_ObshOtch)
  {
    Form3->Series1->AddXY(ObshOtch[0].Dalnost,0,"",clTeeColor);
    Form3->Series2->AddXY(ObshOtch[0].Dalnost,0,"",clTeeColor);
    Form3->Series6->AddXY(ObshOtch[0].Dalnost,0,"",clTeeColor);
  }else{
//    Form3->Series1->AddXY(0,0,"",clTeeColor);
//    Form3->Series2->AddXY(0,0,"",clTeeColor);
//    Form3->Series6->AddXY(0,0,"",clTeeColor);

  }

  for(i=0;i<N_of_ObshOtch;i++)
  {
    Form3->Series1->AddXY(ObshOtch[i].Dalnost,PO[i],"",clTeeColor);
    Form3->Series5->AddXY(ObshOtch[i].Dalnost,PO[i],"",clTeeColor);
    Form3->Series2->AddXY(ObshOtch[i].Dalnost,N_of_S[i],"",clTeeColor);
    Form3->Series6->AddXY(ObshOtch[i].Dalnost,N_of_All[i],"",clTeeColor);
  }
  if(N_of_ObshOtch>0)
  {
          Form3->Series1->AddXY(ObshOtch[N_of_ObshOtch-1].Dalnost,0,"",clTeeColor);
          Form3->Series5->AddXY(ObshOtch[N_of_ObshOtch-1].Dalnost,0,"",clTeeColor);
          Form3->Series2->AddXY(ObshOtch[N_of_ObshOtch-1].Dalnost,0,"",clTeeColor);
          Form3->Series6->AddXY(ObshOtch[N_of_ObshOtch-1].Dalnost,0,"",clTeeColor);
  }

  Form3->Chart1->Foot->Text->Clear();

  if(IsVyvodMaxD)
  {
    if(MaxZone>=0)
    {
      sprintf(Strka," Maximum range Dmax=%.2lf km, ",MaxZone);
    }else{
      sprintf(Strka," Maximum range Dmax not defined,");
    }
//  NijnyayaStroka=NijnyayaStroka+Strka;
    Form3->Chart1->Foot->Text->Add(Strka);
    NijnyayaStroka="";
  }
  if(IsVyvodMinD)
  {
    if(MinZone>=0)
    {
      sprintf(Strka," Close edge=%.2lf km,",MinZone);
    }else{
      sprintf(Strka, " Close edge not defined,");
    }
//  NijnyayaStroka=NijnyayaStroka+Strka;
    Form3->Chart1->Foot->Text->Add(Strka);
    NijnyayaStroka="";
  }
  if(IsVyvodMaxUgol)
  {
    if(BlijOtschUgol>=0)
    {
      sprintf(Strka," Maximum detection angle=%.2lf � for altitude = %d m ",BlijOtschUgol, BlijVysota);
    }else{
      sprintf(Strka," Maximum detection angle not defined");
    }
//  NijnyayaStroka=NijnyayaStroka+Strka;

    Form3->Chart1->Foot->Text->Add(Strka);
  }

  if(IsVyvodFirstSpad)
  {
    if(FirstSpad>0)
    {
      if(FirstSpad<MaxZone-0.01&&
         SecondPodjem>0)
      {

        sprintf(Strka,"  First dip = %.2lf km",
           FirstSpad);
      }else{
       //  sprintf(Strka,"  No dip ",
       //     FirstSpad);
           Strka[0]=0;
      }
    }else{
      sprintf(Strka,"",FirstSpad);
    }    
    Form3->Chart1->Foot->Text->Add(Strka);
  }

//������� ������ ������� �������������� ����� ���������������� �����������
//, � ��������� ������� �������
  if(ZonaForS==0.7)
  {
    Form3->ComboBox1->Text="0.7";
    Form3->Series3->LinePen->Width=1;
    Form3->Series7->LinePen->Width=1;
    Form3->Series8->LinePen->Width=2;
  }
  if(ZonaForS==0.8)
  {
    Form3->ComboBox1->Text="0.8";
    Form3->Series3->LinePen->Width=1;
    Form3->Series7->LinePen->Width=2;
    Form3->Series8->LinePen->Width=1;
  }
  if(ZonaForS==0.9)
  {
    Form3->ComboBox1->Text="0.9";
    Form3->Series3->LinePen->Width=2;
    Form3->Series7->LinePen->Width=1;
    Form3->Series8->LinePen->Width=1;
  }

  Form3->ComboBox1->OnChange=NULL;
  if(KnlD==2)
  {
//    Form3->ComboBox1->Text="0.9";
//    Form3->Series3->LinePen->Width=2;
//    Form3->Series7->LinePen->Width=1;
//    Form3->Series8->LinePen->Width=1;
    Form3->CheckBox1->Visible=true;
  }else{
//    Form3->ComboBox1->Text="0.8";
//    Form3->Series3->LinePen->Width=1;
//    Form3->Series7->LinePen->Width=2;
//    Form3->Series8->LinePen->Width=1;
    Form3->CheckBox1->Visible=false;
  }


  Form3->ComboBox1->OnChange=Form3->ComboBox1Change;
}


//����� �������� � �������� �����...
int FindMaxAndMin(void)
{
  int i;
  bool IsFindMinimum=false;
  MaxZone=-1;
  MinZone=-1;
  double MinZone1,MaxZone1;

  double MaxDlina=0;

  for(i=0;i<N_of_ObshOtch-1;i++)
  {
    if(PO[i]>=ZonaForS&&ObshOtch[i].Exists&&!IsFindMinimum)
    {
       MinZone=ObshOtch[i].Dalnost;
       break;
    }
  }

  if(N_of_ObshOtch>0&&PO[N_of_ObshOtch-1]>=ZonaForS)
  {
    MaxZone=ObshOtch[N_of_ObshOtch-1].Dalnost;
  }else{
    for(i=N_of_ObshOtch-2;i>=0;i--)
    {
      if(PO[i]>=ZonaForS && PO[i+1]<ZonaForS)
      {
         MaxZone=(ObshOtch[i+1].Dalnost-
                ObshOtch[i].Dalnost)*
                (ZonaForS-PO[i])/(PO[i+1]-PO[i])+ObshOtch[i].Dalnost;
         break;
      }
    }
  }


  for(i=0;i<N_of_ObshOtch;i++)
  {
    if(ObshOtch[i].Exists)
    {
      if(ObshOtch[i].Vysota>0&&ObshOtch[i].Dalnost>0)

        if(ObshOtch[i].Vysota/ObshOtch[i].Dalnost/1000.0>=1)
        {
            continue;
            BlijOtschUgol=-1;
            BlijVysota=ObshOtch[i].Vysota;
        }else{
           BlijOtschUgol=asin(ObshOtch[i].Vysota/ObshOtch[i].Dalnost/1000.0)/M_PI*180.0;
           BlijVysota=ObshOtch[i].Vysota;
        }
        break;
    }else{
        BlijOtschUgol=-1;
        BlijVysota=ObshOtch[i].Vysota;
    }
  }


//��������� 2008.07.17
//����� ������� ������������  �����
  FirstSpad=-2.1;
  SecondPodjem=-1;
  for(i=0;i<N_of_ObshOtch-1;i++)
  {
     if(PO[i]>=ZonaForS)
     {
        if(FirstSpad<-2)
        {
          FirstSpad=-1.1;
          continue;
        }
        if(FirstSpad>=0)
        {
          SecondPodjem=ObshOtch[i].Dalnost;
          break;
        }
     }else{
        if(FirstSpad>-2&&FirstSpad<-1)
        {
           FirstSpad=ObshOtch[i].Dalnost;

        }
     }
  }


  return 1;
}


/*��������� ��������� �������� ��� ����������*/
void __stdcall ZakonchitObrabotku(void)
{
  if(WasErrorAddOtchet)
  {
     return;
  }

  Dopolnenie();  //��������� ������ ������������ ��������� � �����
  Sliyanie();    //����� �������
//��������� ������� �����������
  if(KnlD==2)
  {
     Form3->CHMHtml="sc_cov_area.htm";
     ZonaForS=0.9;
  }else{
     Form3->CHMHtml="pc_cov_area.htm";
     ZonaForS=0.8;
  }
  VeroyatnostiBortov();
  FindVeroyatnost();
  FindMaxAndMin();
  MakeGraphik();
  Form3->CHMHelpFile=HelpFileNameZO;
  Form3->CheckBox1->Checked=false;
  Form3->CheckBox2->Checked=false;
  Form3->Show();
  Form3->Chart1->OnResize=Form3->Chart1Resize;


}


//��������� ������������� ������
void __stdcall FreeAll(void)
{
  int i;
  Form3->Close();

  if(Otch)
  {
    for(i=0;i<N_of_Otch;i++)
    {
       if(Otch[i])free(Otch[i]);
    }
    free(Otch);
    Otch=NULL;
  }
  if(LastOtch)
  {
    free(LastOtch);
    LastOtch=NULL;
  }
  if(N_of_Otch2)
  {
    free(N_of_Otch2);
    N_of_Otch2=NULL;
  }

  if(MaxN_of_Otch2)
  {
    free(MaxN_of_Otch2);
    MaxN_of_Otch2=NULL;
  }


  if(N_of_Otch3)
  {
    free(N_of_Otch3);
    N_of_Otch3=NULL;
  }

  if(Vsyo)
  {
    free(Vsyo);
    Vsyo=NULL;
  }


  if(ObshOtch)
  {
     if(ObshOtch1==ObshOtch)
     {
       free(ObshOtch);
       ObshOtch=NULL;
       ObshOtch1=NULL;
     }else{
       free(ObshOtch);
       ObshOtch=NULL;
       if(ObshOtch1)
       {
          free(ObshOtch1);
          ObshOtch1=NULL;
       }
     }
  }else{
     if(ObshOtch1)
     {
        free(ObshOtch1);
        ObshOtch1=NULL;
     }
  }



  if(N_of_All)
  {
    free(N_of_All);
    N_of_All=NULL;
  }

  if(N_of_S)
  {
    free(N_of_S);
    N_of_S=NULL;
  }

  if(N_of_P)
  {
    free(N_of_P);
    N_of_P=NULL;
  }

  if(PO)
  {
    free(PO);
    PO=NULL;
  }
  if(NomeraBortov)
  {
    free(NomeraBortov);
    NomeraBortov=NULL;
  }
  if(Psred)
  {
    free(Psred);
    Psred=NULL;
  }

  N_of_Otch=0;
  N_of_ObshOtch=0;
  N_of_Borts=0;
}



void __stdcall SetRazmerOknaDlyaZony(double sz_km)
{
   if(sz_km<=50&&sz_km>=0.1)
   {
     sizeOfOkno_km=sz_km;
   }
}


int MakeDataO_S(void)
{
  int Ret;

  if(!FDataSOtchObsh)
  {
        FDataSOtchObsh=new TFDataSOtch(NULL);
  }
  FDataSOtchObsh->Hide();
  Ret=FDataSOtchObsh->LoadOtchet(ObshOtch,N_of_ObshOtch,0,dMaxDalnostOfExist);
  if(Ret==0)return 0;
  FDataSOtchObsh->CHMHelpFile=HelpFileNameZO;
  FDataSOtchObsh->Caption= String("Plots. All branches");
  if(KnlD==1)
  {
       FDataSOtchObsh->CHMHtml="w_plots_all_br_pc_cov_area.htm";
  }else{
       FDataSOtchObsh->CHMHtml="w_plots_all_br_sc_cov_area.htm";
  }
  FDataSOtchObsh->Show();
  return 1;
}



int MakeDataO_S_N(void)
{
   int i;
   int RetNB,Ret;
   AnsiString Strka;
  //��������� ������ ������
   FSpisokBortov=new TFSpisokBortov(NULL);
   FSpisokBortov->CHMHelpFile=HelpFileNameZO;
   if(KnlD==1)
   {
     FSpisokBortov->CHMHtml="w_br_list_pc_cov_area.htm";
   }else{
     FSpisokBortov->CHMHtml="w_br_list_sc_cov_area.htm";;
   }
   FSpisokBortov->ListBox1->Clear();
   for(i=0;i<N_of_Otch;i++)
   {
       Strka=String("Branch � ")+String(i+1)+
             String(". Aircraft � ")+String((Otch[i][0].NomerBorta)%100000)+
             String(". All plots = ")+String(N_of_Otch3[i])+
             String(". Det.plots = ")+String(N_of_Otch2[i]);

       if(N_of_Otch2[i]<15||
          ABS(Otch[i][0].Dalnost-Otch[i][N_of_Otch2[i]-1].Dalnost)<10)
       {
         Strka=Strka+String(". Not used.");
       }else{
         Strka=Strka+String(". Used.");
       }
       FSpisokBortov->ListBox1->Items->Add(Strka);
   }
   FSpisokBortov->Button1->Caption="Select branch for consideration";
   FSpisokBortov->Caption="Branch list";
   RetNB=FSpisokBortov->VybratBort();
   delete FSpisokBortov;
   if(RetNB<0)return 0;

   if(!FDataSOtch)
   {
        FDataSOtch=new TFDataSOtch(NULL);
   }
   FDataSOtch->Hide();
   FDataSOtch->Caption=
        String("Plots. Aircraft branch �")+String((Otch[RetNB][0].NomerBorta)%100000);
   Ret=FDataSOtch->LoadOtchet(Otch[RetNB],N_of_Otch3[RetNB],1,dMaxDalnostOfExist);
   if(Ret==0)return 0;
   FDataSOtch->SredSkorostObzor=SredSkorostObzor;
   FDataSOtch->CHMHelpFile=HelpFileNameZO;
   if(KnlD==1)
   {
      FDataSOtch->CHMHtml="w_plots_air_b_nom_pc_cov_area.htm";
   }else{
      FDataSOtch->CHMHtml="w_plots_air_b_nom_sc_cov_area.htm";
   }
   FDataSOtch->Show();
   return 1;
}









//��������� ����������� ��� ������� �����
void __stdcall VeroyatnostiBortov(void)
{
  int i,j,k;
  int Naid=0;
  int Mb,Nb;
  int Frst,Lst;
  int N_O=0;
  double Pprm;
  long N_of_ObnarujOtsch;   //����� ������������ ��������
  long N_of_VseOtsch;   //����� ���� ��������

//������� ������� ������
  if(N_of_ObshOtch==0)return;
  for(i=0;i<N_of_ObshOtch;i++)
  {
     Naid=0;
     for(j=0;j<N_of_Borts;j++)
     {
        if(ObshOtch[i].NomerBorta==NomeraBortov[j])
        {
          Naid=1;
          break;
        }
     }
     if(!Naid)
     {
       NomeraBortov=(long*)realloc(NomeraBortov,sizeof(long)*(N_of_Borts+1));
       NomeraBortov[N_of_Borts]=ObshOtch[i].NomerBorta;
       N_of_Borts++;
     }
  }

//������ ������ ������� ��������� ����������� ����������� �����
  Psred=(double*)malloc(sizeof(double)*N_of_Borts);
  for(i=0;i<N_of_Borts;i++)
  {
    N_O=0;
    N_of_ObnarujOtsch=0;
    N_of_VseOtsch=0;

//���������� ����� ���������� �������� ��� ������� �����
    for(j=0;j<N_of_ObshOtch;j++)
    {
      if(ObshOtch[j].NomerBorta==NomeraBortov[i])
      {
        N_of_VseOtsch++;
        if(ObshOtch[j].Exists)
        {
          N_of_ObnarujOtsch++;
        }
      }
    }
    Psred[i]=N_of_ObnarujOtsch/((double)(N_of_VseOtsch));
  }

//������ ��������� ������� ����������� �� ���� �����
  N_of_ObnarujOtsch=0;
  N_of_VseOtsch=0;
  for(j=0;j<N_of_ObshOtch;j++)
  {
        N_of_VseOtsch++;
        if(ObshOtch[j].Exists)
        {
          N_of_ObnarujOtsch++;
        }
  }

  PsredAll=N_of_ObnarujOtsch/((double)(N_of_VseOtsch));

}






void __stdcall PereschitatVeroyatnost(bool Proverka088,
                                      bool Prolet
                                      )
{

  IskluchitBadBorta=Proverka088;
  ProletayushieTrassy=Prolet;
  SliyaniePovtorno(Proverka088);

  FindVeroyatnost();
  if(KnlD==2)
  {
//     ZonaForS=0.9;
  }else{
//     ZonaForS=0.8;
  }
  FindMaxAndMin();
  MakeGraphik();

  Form3->Show();

}


