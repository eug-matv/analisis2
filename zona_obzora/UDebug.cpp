//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "UDebug.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFDebug *FDebug;
//---------------------------------------------------------------------------
__fastcall TFDebug::TFDebug(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void DebugMsg(char *Strk)
{
  FDebug->Memo1->Lines->Add(Strk);
}