//---------------------------------------------------------------------------
#ifndef ToolsForZonaObzH
#define ToolsForZonaObzH
#define SinG(X) (sin((X)/180.0*M_PI))
#define CosG(X) (cos((X)/180.0*M_PI))

//---------------------------------------------------------------------------
int Okrugl(double X);
double OkruglSot(double X);

int Polinom2(double *A, //���������� ������������
             double *t, //���������
             double *X);

double PolinomRez(int Stepen,
                  double *Koef,
                  double t);


void GetTimeFromDouble(double FloatTime, int &Sutki,
                      int &H, int &M, double &S);

AnsiString   GetTimeFromDoubleToString(double FloatTime);
//��������� ���� � ��������� �� ���������� ���������
//���������� 1 ���� ����� ����� 0
int DekartToPolyar(double X, double Y,
                   double *A,   //� �������� �� 0 �� 360
                   double *D);


double PowI(double Chislo, int N);

//����������� � ������ �� ���� ����� ������� ��������� � ���
bool BylBlizkoKRLS(double A1,
                   double D1,
                   double A2,
                   double D2);


#endif
