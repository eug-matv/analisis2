//---------------------------------------------------------------------------
#ifndef UDebugH
#define UDebugH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFDebug : public TForm
{
__published:	// IDE-managed Components
        TMemo *Memo1;
private:	// User declarations
public:		// User declarations
        __fastcall TFDebug(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFDebug *FDebug;
//---------------------------------------------------------------------------
void DebugMsg(char *Strk);
#endif
