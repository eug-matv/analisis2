//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include <math.h>
#include "ToolsForZonaObz.h"
#define ABS(X) (((X) > 0) ? (X) : (-(X)))
//---------------------------------------------------------------------------
#pragma package(smart_init)
//���������� �����
int Okrugl(double X)
{
 int Y;
 Y=X;
 if(X-Y<0.5)return ((int)Y);
 return ((int)Y+1);
}

double OkruglSot(double X)
{
  int Ret=Okrugl(X*100.0);
  return (Ret/100.0);
}


//������� ������� ������� �� 4 ������
int Polinom3(double *A, //���������� ������������
             double *t, //���������
             double *X)
{

  return 1;
}



//������� ������ ������� �� 3 ������
int Polinom2(double *A, //���������� ������������
             double *t, //���������
             double *X)
{
   if(t[0]==t[1]|| t[1]==t[2]|| t[0]==t[2])return 0;   //�����

//������� ������ �����������
   A[0]=(X[2]*t[0]*t[0]*t[1]-X[1]*t[0]*t[0]*t[2]-
         X[2]*t[0]*t[1]*t[1]+t[2]*t[2]*t[0]*X[1]+
         t[1]*t[1]*X[0]*t[2]-t[2]*t[2]*X[0]*t[1])/
         ((-t[2]+t[1])*(-t[1]*t[0]+t[1]*t[2]+t[0]*t[0]-t[2]*t[0]));


   A[1]=(-t[1]*t[1]*X[0]+X[2]*t[1]*t[1]+t[2]*t[2]*X[0]-
          X[2]*t[0]*t[0]+X[1]*t[0]*t[0]-X[1]*t[2]*t[2])/
         ((-t[2]+t[1])*(-t[1]*t[0]+t[1]*t[2]+t[0]*t[0]-t[2]*t[0]));


   A[2]=(-X[0]*t[2]+X[0]*t[1]+t[0]*X[2]-t[0]*X[1]+X[1]*t[2]-t[1]*X[2])/
         (-t[0]*t[0]*t[2]+t[0]*t[0]*t[1]+t[0]*t[2]*t[2]-
           t[0]*t[1]*t[1]+t[1]*t[1]*t[2]-t[1]*t[2]*t[2]);

   return 1;
}



double PolinomRez(int Stepen,
                  double *Koef,
                  double t)
{
 double Rez;
 int i;
 Rez=Koef[Stepen];

 for(i=Stepen-1;i>=0;i--)
 {
   Rez=Rez*t+Koef[i];
 }
 return Rez;

}




void GetTimeFromDouble(double FloatTime, int &Sutki,
                      int &H, int &M, double &S)
{
  double fM,fS;
  Sutki=0;
  while(FloatTime<0)
  {
    FloatTime+=24.0;
    Sutki--;
  }
  H=(int)FloatTime;
  fM=FloatTime-H;
  while(H>=24)
  {
    Sutki++;
    H-=24;
  }
  fM=fM*60;
  M=(int)(fM);
  fS=fM-M;
  S=fS*60.0;
}



AnsiString   GetTimeFromDoubleToString(double FloatTime)
{
  int Sutki,H,M;
  double S;
  char  Strka[50];
  AnsiString RetS;
  GetTimeFromDouble(FloatTime,Sutki,H,M,S);
  sprintf(Strka,"%02d:%02d:%06.03lf",H,M,S);
  RetS=Strka;
  return RetS;
}



//��������� ���� � ��������� �� ���������� ���������
//���������� 1 ���� ����� ����� 0
int DekartToPolyar(double X, double Y,
                   double *A,   //� �������� �� 0 �� 360
                   double *D)
{
  double A1;
  (*D)=sqrt(X*X+Y*Y);
  if((*D)<0.0001)
  {
    *A=0;
    *D=0;
    return 0;
  }
  A1=asin(Y/(*D))*180.0/M_PI;
  if(X<0)
  {
    A1=180-A1;
  }else{
     if(Y<0)
     {
       A1=360+A1;
     }

  }
  *A=A1;
  return 1;
}



double PowI(double Chislo, int N)
{
  int i;
  double RetCh=1.0;
  if(N==0)return 1;
  if(N<0)
  {
    for(i=0;i>N;i--)
    {
      RetCh/=Chislo;
    }
    return RetCh;
  }

//� ��� N ������ 0
  for(i=0;i<N;i++)
  {
    RetCh*=Chislo;
  }
  return RetCh;
}

//����������� � ������ �� ���� ����� ������� ��������� � ���
bool BylBlizkoKRLS(double A1,
                   double D1,
                   double A2,
                   double D2)
{
  double X1,Y1,X2,Y2;
  double DX,DY,dR;
  double Cos1,Cos2;
  double Przvd,SklPrzvd;

//������� ������������� ����������
  X1=D1*cos(A1*M_PI/180.0);
  Y1=D1*sin(A1*M_PI/180.0);
  X2=D2*cos(A2*M_PI/180.0);
  Y2=D2*sin(A2*M_PI/180.0);

  DX=X2-X1;
  DY=Y2-Y1;

//������ ���� ����� ��������� (X1,Y1) � (DX,DY)
  dR=sqrt(DX*DX+DY*DY);
  Przvd=dR*D1;
  SklPrzvd=X1*DX+Y1*DY;
  if(Przvd<=0.0000000000001)return true;
  Cos1=SklPrzvd/Przvd;


  DX=X1-X2;
  DY=Y1-Y2;

//������ ���� ����� ��������� (X1,Y1) � (DX,DY)
  Przvd=dR*D2;
  SklPrzvd=X2*DX+Y2*DY;
  if(Przvd<=0.0000000000001)return true;
  Cos2=SklPrzvd/Przvd;

  if(Cos1>=0.0&&Cos2>=0.0)return true;

  return false;
}



/*����������� ������*/

