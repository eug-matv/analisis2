//---------------------------------------------------------------------------
#ifndef GlobalData631H
#define GlobalData631H
//---------------------------------------------------------------------------
int OpenFDOB_UVD(void *Otch631,
                 int IndexB);

int OpenFDOB_UVD_Indx(void *Otch631,
                      int IndexB,
                      int Indx);
int IzmenFalseVysotaFDOB_UVD(void);

int CloseFDOB_UVD(int IndexB);
int OpenFDOB_RBS(void *Otch631,
                 int IndexB);
int OpenFDOB_RBS_Indx(void *Otch631,
                      int IndexB,
                      int Indx);
int IzmenFalseVysotaFDOB_RBS(void);
int CloseFDOB_RBS(int IndexB);

int OpenFIVDataUVD(void *Otch631);
int IzmenFalseVysotaFIVData_UVD(void);
int CloseFIVDataUVD(void );

int OpenFIVDataRBS(void *Otch631);
int IzmenFalseVysotaFIVData_RBS(void);
int CloseFIVDataRBS(void );

int  OpenFODataUVD(void *Otch631);
int  OpenFODataUVD_Indx(void *Otch631,
                        int Indx);
int IzmenFalseVysotaFOData_UVD(void);                        
int CloseFODataUVD(void);

/**/
int  OpenFODataRBS(void *Otch631);
int OpenFODataRBS_Indx(void *Otch631,
                        int Indx);
int IzmenFalseVysotaFOData_RBS(void);                        
int CloseFODataRBS(void);


//���������� �������� ���� �������� ����, ��� ��� ���, ��� � ��� RBS
int GlobalCloseWindows(void);


#endif
