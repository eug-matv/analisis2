//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdlib.h>
#include "For631.h"
#include "For631_2.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)


int DopO_S_B_631_Nomer(const void *a,
                       const void *b)
{
  ONE_S_BORT_631 *osb_a=(ONE_S_BORT_631 *)a,
                 *osb_b=(ONE_S_BORT_631 *)b;

  OTSCHETS631 *o_a,*o_b;
  o_a=(OTSCHETS631*)(osb_a->Otkuda);
  o_b=(OTSCHETS631*)(osb_b->Otkuda);

  if(o_a->NomBort[osb_a->IndexBort]>o_b->NomBort[osb_b->IndexBort])return 1;
  if(o_a->NomBort[osb_a->IndexBort]<o_b->NomBort[osb_b->IndexBort])return (-1);
  return 0;
}


int SortO_S_B_631_Nomer(ONE_S_BORT_631 *OSB, int N_of_OSB)
{
  if(N_of_OSB<2)return 0;
  if(OSB==NULL)return 0;
  qsort(OSB,N_of_OSB,sizeof(ONE_S_BORT_631),DopO_S_B_631_Nomer);

  return 1;
}


int DopO_S_B_631_FTime(const void *a,
                       const void *b)
{
  ONE_S_BORT_631 *osb_a=(ONE_S_BORT_631 *)a,
                 *osb_b=(ONE_S_BORT_631 *)b;

  OTSCHETS631 *o_a,*o_b;
  double Tm1,Tm2;
  int P1,P2;
  o_a=(OTSCHETS631*)(osb_a->Otkuda);
  o_b=(OTSCHETS631*)(osb_b->Otkuda);
  P1=o_a->FirstOtschet(osb_a->IndexBort, true, false, true,false);
  P2=o_b->FirstOtschet(osb_b->IndexBort, true, false, true,false);

  if(P1<0)
  {
    if(P2<0)return 0;
    return 1;
  }

  if(P2<0)
  {
    return (-1);
  }
  Tm1=o_a->OO[P1].Time;
  Tm2=o_b->OO[P2].Time;
  if(Tm1>Tm2)return 1;
  if(Tm1<Tm2)return (-1);
  return 0;
}


int SortO_S_B_631_FTime(ONE_S_BORT_631 *OSB, int N_of_OSB)
{
  if(N_of_OSB<2)return 0;
  if(OSB==NULL)return 0;
  qsort(OSB,N_of_OSB,sizeof(ONE_S_BORT_631),DopO_S_B_631_FTime);

  return 1;
}


static int N_of_KolvoFor631_2;
int DopO_S_B_631_FKolvo(const void *a,
                       const void *b)
{
  int N_of_Oa;
  int N_of_Ob;
  ONE_S_BORT_631 *osb_a=(ONE_S_BORT_631 *)a,
                 *osb_b=(ONE_S_BORT_631 *)b;

  OTSCHETS631 *o_a,*o_b;
  o_a=(OTSCHETS631*)(osb_a->Otkuda);
  o_b=(OTSCHETS631*)(osb_b->Otkuda);

  N_of_Oa=o_a->ChisloOtschetov(osb_a->IndexBort,false,false);
  N_of_Ob=o_b->ChisloOtschetov(osb_b->IndexBort,false,false);
  if(N_of_Oa==N_of_KolvoFor631_2)
  {
    if(N_of_Ob==N_of_KolvoFor631_2)return 0;
    return (-1);
  }

  if(N_of_Ob==N_of_KolvoFor631_2)
  {
    return 1;
  }
  
  if(o_a->NomBort[osb_a->IndexBort]>o_b->NomBort[osb_b->IndexBort])return 1;
  if(o_a->NomBort[osb_a->IndexBort]<o_b->NomBort[osb_b->IndexBort])return (-1);
  return 0;
}

int SortO_S_B_631_FKolvo(ONE_S_BORT_631 *OSB, int N_of_OSB, int Dop_Otsch)
{
  N_of_KolvoFor631_2=Dop_Otsch;
  if(N_of_OSB<2)return 0;
  if(OSB==NULL)return 0;
  qsort(OSB,N_of_OSB,sizeof(ONE_S_BORT_631),DopO_S_B_631_FKolvo);
  return 1;
}

