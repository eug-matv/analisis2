//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "For631.h"
#include "DanOdinBort.h"
#include "GlobalData631.h"
#include "IskajVysotaData.h"
#include "VyvodData.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
AnsiString GlobalPathDLL="";

//��� ���������� ������ � ������� ��������� ������
TFDanOdinBort *GlobalFDOB_UVD[100];  //����
int N_of_GlobalFDOB_UVD=0;           //����� ������

TFDanOdinBort *GlobalFDOB_RBS[100];  //����
int N_of_GlobalFDOB_RBS=0;           //����� ������

TFIskajVysotaData *FIVDataUVD=NULL;
TFIskajVysotaData *FIVDataRBS=NULL;

TFOData *FODataUVD=NULL;
TFOData *FODataRBS=NULL;

//��� ����������� ������
bool IsMoveUVD=false;
int MoveOtkudaUVD;

bool IsMoveRBS=false;
int MoveOtkudaRBS;


AnsiString CHMHelpFileGlobal631;


//LastNomerFormUVD,LastNomerFormRBS - ������� ���� � �������
//��� ����� ����� ����������� OpenFDOB_UVD � OpenFDOB_UVD_Indx
//� OpenFDOB_RBS � OpenFDOB_RBS_Indx
static int LastNomerFormUVD,LastNomerFormRBS;


//������� ����
int OpenFDOB_UVD(void *Otch631,
                 int IndexB)
{
  int i;
  int Naid=-1;
  int FNull=-1;
  OTSCHETS631 *O631=(OTSCHETS631 *)Otch631;
  if(IndexB<0||IndexB>=O631->N_of_Borts)return 0;
  for(i=0;i<N_of_GlobalFDOB_UVD;i++)
  {
    if(GlobalFDOB_UVD[i]==NULL)
    {
      FNull=i;
      break;
    }
    if(GlobalFDOB_UVD[i]->IndexBort==IndexB)
    {
      Naid=i;
      break;
    }
  }

  if(Naid==-1)
  {
     if(FNull==-1)
     {
        if(N_of_GlobalFDOB_UVD==100)return 0;
        GlobalFDOB_UVD[N_of_GlobalFDOB_UVD]=new TFDanOdinBort(NULL);
        GlobalFDOB_UVD[N_of_GlobalFDOB_UVD]->IndexBort=IndexB;
        GlobalFDOB_UVD[N_of_GlobalFDOB_UVD]->Otchs631=Otch631;
        GlobalFDOB_UVD[N_of_GlobalFDOB_UVD]->Type=1;
        LastNomerFormUVD=N_of_GlobalFDOB_UVD;
        GlobalFDOB_UVD[N_of_GlobalFDOB_UVD]->CHMHelpFile=CHMHelpFileGlobal631;
        GlobalFDOB_UVD[N_of_GlobalFDOB_UVD]->CHMHtml="plots_of_one_air.htm";
        GlobalFDOB_UVD[N_of_GlobalFDOB_UVD]->Show();
//        LastNomerFormUVD=N_of_GlobalFDOB_UVD;
        N_of_GlobalFDOB_UVD++;

     }else{
       GlobalFDOB_UVD[FNull]=new TFDanOdinBort(NULL);
       GlobalFDOB_UVD[FNull]->IndexBort=IndexB;
       GlobalFDOB_UVD[FNull]->Otchs631=Otch631;
       GlobalFDOB_UVD[FNull]->Type=1;
       LastNomerFormUVD=FNull;
       GlobalFDOB_UVD[FNull]->CHMHelpFile=CHMHelpFileGlobal631;
       GlobalFDOB_UVD[FNull]->CHMHtml="plots_of_one_air.htm";
       GlobalFDOB_UVD[FNull]->Show();

     }
  }else{
     LastNomerFormUVD=Naid;
     GlobalFDOB_UVD[Naid]->CHMHelpFile=CHMHelpFileGlobal631;
     GlobalFDOB_UVD[Naid]->CHMHtml="plots_of_one_air.htm";
     GlobalFDOB_UVD[Naid]->Show();
  }
  return 1;
}

int OpenFDOB_UVD_Indx(void *Otch631,
                      int IndexB,
                      int Indx)
{
  int Ret;
  Ret=OpenFDOB_UVD(Otch631,IndexB);
  if(Ret!=1)return -1;
  Ret=GlobalFDOB_UVD[LastNomerFormUVD]->FindOtschetIndx(Indx);
  if(Ret!=1)return 0;
  return 1;
}


/*���������� ������ �� ���������� ������*/
int IzmenFalseVysotaFDOB_UVD(void)
{
  int i;
  for(i=0;i<N_of_GlobalFDOB_UVD;i++)
  {
     if(GlobalFDOB_UVD[i])GlobalFDOB_UVD[i]->ObnovIskajVys();
  }

  return 1;
}


//�������� ����
int CloseFDOB_UVD(int IndexB)
{
  int i;
  int Ret=0;
  for(i=0;i<N_of_GlobalFDOB_UVD;i++)
  {
    if(GlobalFDOB_UVD[i]==NULL)continue;
    if(IndexB==GlobalFDOB_UVD[i]->IndexBort)
    {

//      delete GlobalFDOB_UVD[i];
//      GlobalFDOB_UVD[i]=NULL;
       Ret=1;
       continue;
    }
    if(!GlobalFDOB_UVD[i]->Visible)
    {
       delete GlobalFDOB_UVD[i];
       GlobalFDOB_UVD[i]=NULL;
    }
  }
  return Ret;
}


int OpenFDOB_RBS(void *Otch631,
                 int IndexB)
{
  int i;
  int Naid=-1;
  int FNull=-1;
  OTSCHETS631 *O631=(OTSCHETS631 *)Otch631;
  if(IndexB<0||IndexB>=O631->N_of_Borts)return 0;
  for(i=0;i<N_of_GlobalFDOB_RBS;i++)
  {
    if(GlobalFDOB_RBS[i]==NULL)
    {
      FNull=i;
      break;
    }
    if(GlobalFDOB_RBS[i]->IndexBort==IndexB)
    {
      Naid=i;
      break;
    }
  }

  if(Naid==-1)
  {
     if(FNull==-1)
     {
        if(N_of_GlobalFDOB_RBS==100)return 0;
        GlobalFDOB_RBS[N_of_GlobalFDOB_RBS]=new TFDanOdinBort(NULL);
        GlobalFDOB_RBS[N_of_GlobalFDOB_RBS]->IndexBort=IndexB;
        GlobalFDOB_RBS[N_of_GlobalFDOB_RBS]->Otchs631=Otch631;
        GlobalFDOB_RBS[N_of_GlobalFDOB_RBS]->Type=2;
        LastNomerFormRBS=N_of_GlobalFDOB_RBS;
        GlobalFDOB_RBS[N_of_GlobalFDOB_RBS]->CHMHelpFile=CHMHelpFileGlobal631;
        GlobalFDOB_RBS[N_of_GlobalFDOB_RBS]->CHMHtml="plots_of_one_air.htm";
        GlobalFDOB_RBS[N_of_GlobalFDOB_RBS]->Show();
        N_of_GlobalFDOB_RBS++;

     }else{
       GlobalFDOB_RBS[FNull]=new TFDanOdinBort(NULL);
       GlobalFDOB_RBS[FNull]->IndexBort=IndexB;
       GlobalFDOB_RBS[FNull]->Otchs631=Otch631;
       GlobalFDOB_RBS[FNull]->Type=2;
       LastNomerFormRBS=FNull;
       GlobalFDOB_RBS[FNull]->CHMHelpFile=CHMHelpFileGlobal631;
       GlobalFDOB_RBS[FNull]->CHMHtml="plots_of_one_air.htm";
       GlobalFDOB_RBS[FNull]->Show();
     }
  }else{
     LastNomerFormRBS=Naid;
     GlobalFDOB_RBS[Naid]->CHMHelpFile=CHMHelpFileGlobal631;
     GlobalFDOB_RBS[Naid]->CHMHtml="plots_of_one_air.htm";
     GlobalFDOB_RBS[Naid]->Show();
  }
  return 1;
}


int OpenFDOB_RBS_Indx(void *Otch631,
                      int IndexB,
                      int Indx)
{
  int Ret;
  Ret=OpenFDOB_RBS(Otch631,IndexB);
  if(Ret!=1)return -1;
  Ret=GlobalFDOB_RBS[LastNomerFormRBS]->FindOtschetIndx(Indx);
  if(Ret!=1)return 0;
  return 1;
}


/*���������� ������ �� ���������� ������*/
int IzmenFalseVysotaFDOB_RBS(void)
{
  int i;
  for(i=0;i<N_of_GlobalFDOB_RBS;i++)
  {
     if(GlobalFDOB_RBS[i])GlobalFDOB_RBS[i]->ObnovIskajVys();
  }
  return 1;
}


//�������� ����
int CloseFDOB_RBS(int IndexB)
{
  int i;
  int Ret=0;
  for(i=0;i<N_of_GlobalFDOB_RBS;i++)
  {
    if(GlobalFDOB_RBS[i]==NULL)continue;
    if(IndexB==GlobalFDOB_RBS[i]->IndexBort)
    {

//      delete GlobalFDOB_UVD[i];
//      GlobalFDOB_UVD[i]=NULL;
       Ret=1;
       continue;
    }
    if(!GlobalFDOB_RBS[i]->Visible)
    {
       delete GlobalFDOB_RBS[i];
       GlobalFDOB_RBS[i]=NULL;
    }
  }
  return Ret;
}


int OpenFIVDataUVD(void *Otch631)
{
  if(FIVDataUVD)
  {
     FIVDataUVD->Otschs631=Otch631;
     FIVDataUVD->Type=1;
     FIVDataUVD->CHMHelpFile=CHMHelpFileGlobal631;
     FIVDataUVD->Show();
  }else{
     FIVDataUVD=new TFIskajVysotaData(NULL);
     FIVDataUVD->Type=1;
     FIVDataUVD->Otschs631=Otch631;
     FIVDataUVD->CHMHelpFile=CHMHelpFileGlobal631;
     FIVDataUVD->Show();
  }
  return 1;
}

int IzmenFalseVysotaFIVData_UVD(void)
{
  if(FIVDataUVD)
  {
    FIVDataUVD->ObnovIskajVys();
  }
  return 1;
}


int CloseFIVDataUVD(void)
{

  if(FIVDataUVD)
  {
    delete FIVDataUVD;
    FIVDataUVD=NULL;
    return 1;
  }
  return 0;
}


int OpenFIVDataRBS(void *Otch631)
{
  if(FIVDataRBS)
  {
     FIVDataRBS->Otschs631=Otch631;
     FIVDataRBS->Type=2;
     FIVDataRBS->CHMHelpFile=CHMHelpFileGlobal631;
     FIVDataRBS->Show();
  }else{
     FIVDataRBS=new TFIskajVysotaData(NULL);
     FIVDataRBS->Type=2;
     FIVDataRBS->Otschs631=Otch631;
     FIVDataRBS->CHMHelpFile=CHMHelpFileGlobal631;
     FIVDataRBS->Show();
  }
  return 1;
}

int IzmenFalseVysotaFIVData_RBS(void)
{
  if(FIVDataRBS)
  {
    FIVDataRBS->ObnovIskajVys();
  }
  return 1;
}


int CloseFIVDataRBS(void)
{

  if(FIVDataRBS)
  {
    delete FIVDataRBS;
    FIVDataRBS=NULL;
    return 1;
  }
  return 0;
}

int OpenFODataUVD(void *Otch631)
{
  if(FODataUVD)
  {
     FODataUVD->Otschs631=Otch631;
     FODataUVD->Type=1;
     FODataUVD->CHMHelpFile=CHMHelpFileGlobal631;
     FODataUVD->Show();
  }else{
     FODataUVD=new TFOData(NULL);
     FODataUVD->Type=1;
     FODataUVD->Otschs631=Otch631;
     FODataUVD->CHMHelpFile=CHMHelpFileGlobal631;
     FODataUVD->Show();
  }
  return 1;
}

int  OpenFODataUVD_Indx(void *Otch631,
                        int Indx
                        )
{
  int Ret;
  OpenFODataUVD(Otch631);
  Ret=FODataUVD->FindOtschetIndx(Indx);
  if(Ret!=1)return 0;
  return 1;
}

int IzmenFalseVysotaFOData_UVD(void)
{
  if(FODataUVD)
  {
    FODataUVD->ObnovIskajVys();
  }
  return 1;
}


int CloseFODataUVD(void)
{

  if(FODataUVD)
  {
    delete FODataUVD;
    FODataUVD=NULL;
    return 1;
  }
  return 0;
}



int OpenFODataRBS(void *Otch631)
{
  if(FODataRBS)
  {
     FODataRBS->Otschs631=Otch631;
     FODataRBS->Type=2;
     FODataRBS->CHMHelpFile=CHMHelpFileGlobal631;
     FODataRBS->Show();
  }else{
     FODataRBS=new TFOData(NULL);
     FODataRBS->Type=2;
     FODataRBS->Otschs631=Otch631;
     FODataRBS->CHMHelpFile=CHMHelpFileGlobal631;
     FODataRBS->Show();
  }
  return 1;
}


int OpenFODataRBS_Indx(void *Otch631,
                        int Indx)
{
  int Ret;
  OpenFODataRBS(Otch631);
  Ret=FODataRBS->FindOtschetIndx(Indx);
  if(Ret!=1)return 0;
  return 1;
}

int IzmenFalseVysotaFOData_RBS(void)
{
  if(FODataRBS)
  {
    FODataRBS->ObnovIskajVys();
  }
  return 1;

}


int CloseFODataRBS(void)
{

  if(FODataRBS)
  {
    delete FODataRBS;
    FODataRBS=NULL;
    return 1;
  }
  return 0;
}



//���������� �������� ���� �������� ����, ��� ��� ���, ��� � ��� RBS
int GlobalCloseWindows(void)
{
  int i;
  int Indx;
  for(i=0;i<N_of_GlobalFDOB_UVD;i++)
  {
     if(GlobalFDOB_UVD[i])
     {
       delete GlobalFDOB_UVD[i];
       GlobalFDOB_UVD[i]=NULL;
     }
  }
  N_of_GlobalFDOB_UVD=0;
  for(i=0;i<N_of_GlobalFDOB_RBS;i++)
  {
     if(GlobalFDOB_RBS[i])
     {
        delete GlobalFDOB_RBS[i];
        GlobalFDOB_RBS[i]=NULL;
     }
  }
  N_of_GlobalFDOB_RBS=0;
  CloseFIVDataUVD();
  CloseFIVDataRBS();
  CloseFODataUVD();
  CloseFODataRBS();
  return 1;
}


