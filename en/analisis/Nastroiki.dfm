�
 TFNASTROIKI 0�>  TPF0TFNastroiki
FNastroikiLeftkTopCWidth�HeightNHelpType	htKeywordCaptionSettingsColor	clBtnFaceConstraints.MaxHeightNConstraints.MaxWidth�Constraints.MinHeightNConstraints.MinWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	Menu	MainMenu1OldCreateOrderPositionpoScreenCenterOnCreate
FormCreate	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel12Left�TopWidthHHeightCaptionMarks diameter  TLabelLabel13LeftXTopWidthHeightCaptionpix  TShapeShape5LeftiTop9WidthHeightBrush.Color@ @   TLabelLabel41LeftToptWidth� HeightCaption"Acceptable differences by altitude  TLabelLabel42LeftTopsWidthHeightCaptionm  TLabelLabel43Left4TopWidth=Height	AlignmenttaRightJustifyCaption	Test siteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel44LeftTop�Width� HeightCaption$Monitoring transponders UVD, numbers  TLabelLabel45LeftTop�Width� HeightCaption$Monitoring transponders RBS, numbers  TLabelLabel47LeftaTop�WidthHeightCaption   Visible  	TGroupBox	GroupBox8LeftHTop�Width1HeightICaptionGrayscale PrintTabOrderVisible TLabelLabel48Left$TopWidth&Height	AlignmenttaRightJustifyCaptionP marks  TLabelLabel53Left}TopWidth&Height	AlignmenttaRightJustifyCaptionS marks  TLabelLabel54Left� TopWidth-Height	AlignmenttaRightJustifyCaptionPS marks  
TCSpinEdit	CSEPrintOLeft� Top'Width9HeightMaxValue� MinValueTabOrder   
TCSpinEdit
CSEPrintVKLeftwTop'Width9HeightMaxValue� MinValueTabOrder  
TCSpinEdit
CSEPrintPKLeftTop(Width9HeightMaxValue� MinValueTabOrder   	TCheckBoxIsVisNumberLeftTopWidth� HeightCaptionShow tail number (and time)TabOrder OnClickIsVisNumberClick  	TCheckBox	IsVisTimeLeftTop'Width� HeightCaption%Show time of the origin of trajectoryTabOrderOnClickIsVisTimeClick  	TGroupBox	GroupBox1LeftTop7Width� Height2CaptionRecords selectionFont.CharsetDEFAULT_CHARSET
Font.Color� Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel3Left� TopZWidth	HeightCaptiontoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4Left� Top\WidthHeightCaptionkmFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftZTopxWidthHeightCaptionfromFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6Left� TopyWidth	HeightCaptiontoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7LeftXTopYWidthHeightCaptionfromFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel8Left� ToppWidthHeightCaptionoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel9LeftZTop� WidthHeightCaptionfromFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel10Left� Top� Width	HeightCaptiontoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel11Left� Top� WidthHeightCaptionoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel51LeftTopWidthHeightCaption)   Мин.уровень сигнала ПКFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  	TCheckBoxIsCheckNumberLeftTopWidth� HeightCaptionBy tail numbersChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder OnClickIsCheckNumberClick  	TCheckBox
IsCheckVysLeftTop6WidthIHeightCaptionBy altitudeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckVysClick  	TCheckBoxIsCheckTimeLeftTop#WidthyHeightCaptionBy timeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckTimeClick  	TCheckBox
IskluchVysLeftUTop5Width� HeightCaptionExclude altitude range Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIskluchVysClick  	TCheckBoxIsCheckDlnstLeft	TopMWidth@HeightCaption	By range Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckDlnstClick  TEditDlnst1LeftpTopPWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextDlnst1OnChangeDlnst1Change  TEditDlnst2Left� TopPWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextDlnst2OnChangeDlnst2Change  	TCheckBoxIsCheckAzmtLeft	TopWidthPHeightCaptionBy azimuth Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickIsCheckAzmtClick  TEditAzmt11LeftpToprWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextAzmt11OnChangeAzmt11Change  TEditAzmt12Left� ToppWidth-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	TextAzmt12OnChangeAzmt12Change  TEditAzmt21LeftpTop� Width-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
TextAzmt21  TEditAzmt22Left� Top� Width-HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextAzmt22  	TCheckBoxKanalPLeft
Top� Width?HeightCaption	P channelChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKanalPClick  	TCheckBoxKanalSLeftNTop� WidthCHeightCaption	S channelChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKanalSClick  	TCheckBoxKanalPSLeft� Top� WidthQHeightCaptionCombined PSChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrderOnClickKanalPSClick  TRadioGroupRBNapravleniaLeftTop� WidthwHeightICaption
 DirectionFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 	ItemIndex Items.StringsAll readoutsTo radar stationFrom radar station 
ParentFontTabOrderOnClickRBNapravleniaClick  TRadioGroupRGRejimLeft� Top� WidthlHeightICaptionModes (for S and PS)Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style Items.StringsUVD and RBSUVDRBS 
ParentFontTabOrderOnClickRGRejimClick  	TCheckBoxcbIsCheckSignalPKLeftTopWidth� HeightCaption5   Проверять уровень сигнала ПКColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFontTabOrderVisible  TEditeMinSignalPKLeft� TopWidthdHeightTabOrderText1e+1Visible   
TCSpinEditDiametrMetokLeft"Top Width1HeightMaxValue
MinValueTabOrderValue  TButtonB_OKLeft�Top�WidthaHeightCaptionOKTabOrderOnClick	B_OKClick  TButtonBCancelLeft�Top�WidthaHeightCaptionCancelTabOrderOnClickBCancelClick  	TGroupBox	GroupBox6LeftTop.Width�HeightFCaption(Animation options (valid only for SCh)- TabOrder 	TCheckBoxIsAnimNomerLeftTopWidth� HeightCaptionDisplay tail numbersTabOrder   	TCheckBoxIsAnimVysotaLeftTop$WidthyHeightCaptionDisplay altitudeTabOrder  	TCheckBox
IsAnimTimeLeftTop7WidthyHeightCaptionDisplay timeTabOrder  	TGroupBoxgbSledLeft� TopWidth� Height5AlignalRightCaptionTrackTabOrder TLabelLabel1LeftTop#Width<HeightCaptionTrack length  	TCheckBoxIsHvostLeftTopWidthiHeightCaptionOutput trackTabOrder OnClickIsHvostClick  
TCSpinEditDlinaHvostaLeftSTopWidth=HeightMaxValue;MinValueTabOrderValue    TButtonB_PoUmolchaniuLeftTop�WidthaHeightCaption
By defaultTabOrderOnClickB_PoUmolchaniuClick  	TGroupBox	GroupBox7LeftTop� Width�Height3Caption(Parameters for data output and selectionTabOrder 	TCheckBoxVydelyatSeveraLeftTopWidth� HeightCaptionSelect also NorthTabOrder   	TCheckBoxIsIstinnyRightDopDataLeft� TopWidth� HeightCaption%Output correct additional informationTabOrder  	TCheckBoxIsLojnyyInfoVyvodLeftTopWidth9HeightCaption+Output information on incorrect plots data TabOrder   	TGroupBoxGBMinMaxAndMaxMinLeftTopWidth�HeightQCaption$For search of routes close to radialTabOrder	 TLabelLabel26LeftTop:Width`HeightCaptionMax. minimum range  TLabelLabel27Left� Top:Width`HeightCaptionMin. maximum range  TEditMaxMinDLeftpTop5Width1HeightTabOrder TextMaxMinDOnChangeMaxMinDChange  TEditMinMaxDLeft Top5Width1HeightTabOrderTextMinMaxDOnChangeMinMaxDChange  	TCheckBoxFindRadialTraektLeftTopWidthHeightCaption#Select trajectories close to radialTabOrderOnClickFindRadialTraektClick  TRadioButtonFindForP_PSLeftTop$WidthyHeightCaptionP or PS marksTabOrderOnClickFindForP_PSClick  TRadioButtonFindForS_PSLeft� Top$Width� HeightCaptionS or PS marksChecked	TabOrderTabStop	OnClickFindForS_PSClick   	TGroupBoxGBColorsLeftTophWidth� Height� CaptionColorsTabOrder
 TLabelLabel28LeftTopWidthHeightCaptionArea  TShape
SOblastColLeftiTopWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShape
SKolca1ColLeftiTop!WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel30LeftTop Width8HeightCaptionMain circles  TShape
SKolca2ColLeftiTop1WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel29LeftTop0WidthOHeightCaptionAdditional circles  TLabelLabel31LeftTopAWidthZHeightCaptionRadial lines 90-180  TShape	SRad90ColLeftiTopBWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShape	SRad30ColLeftiTopSWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel32LeftTopRWidthWHeightCaptionRadial lines 30, 60  TShapeS_P_ColLeft� TopWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel33Left� TopWidth&HeightCaptionP marks  TLabelLabel34Left� Top Width&HeightCaptionS marks  TShapeS_S_ColLeft� Top!WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShapeS_PS_ColLeft� Top1WidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel35Left� Top0Width-HeightCaptionPS marks  TLabelLabel36Left� TopAWidth4HeightCaption	Close UVD  TShapeS_RS_UVD_ColLeft� TopBWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TShapeS_RS_RBS_ColLeft� TopSWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel37Left� TopRWidth3HeightCaption	Close RBS  TLabelLabel38Left� TopcWidth$HeightCaptionClose P  TShape
S_RS_P_ColLeft� TopdWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel39LeftTopcWidth@HeightCaptionSelection box  TShape	SRamkaColLeftiTopdWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour  TLabelLabel40LeftHTopsWidth/HeightCaption
Text color  TShape
S_Text_ColLeft� ToptWidthHeightBrush.Color@ @ OnMouseDownMouseDownSetColour   
TCSpinEditDopDVysLeft�TopnWidthIHeightMaxValue�MinValue
TabOrderValue�   TEditMestoIspytaniyLeft�TopWidthHeightTabOrderTextMestoIspytaniy  TEditKO_UVDLeft�Top�Width� HeightTabOrderTextKO_UVD  TEditKO_RBSLeft�Top�Width� HeightTabOrderTextKO_RBS  TEditE_RazmerOknaLeftTop�WidthKHeightTabOrderTextE_RazmerOknaVisible  	TCheckBoxCBSetMaxDalnGOSTLeftTop�WidthWHeightCaptionf   Задавать максимальную дальность при поиске зоны обзораTabOrderVisible  	TCheckBoxcbPlotInLeftTopWidth� HeightCaptionThe input plotTabOrder  	TGroupBox	GroupBox2LeftTopxWidth�HeightYCaption. Strobes for bindingTabOrder 	TGroupBox	GroupBox3Left
TopWidth}HeightBCaptionUVDTabOrder  TLabelLabel2LeftTopWidth2Height	AlignmenttaRightJustifyCaption
by azimuth  TLabelLabel14LeftTop(Width)Height	AlignmenttaRightJustifyCaptionby range  TLabelLabel19LeftoTop
WidthHeightCaptiono  TLabelLabel22LeftkTop(WidthHeightCaptionkm  TEdit	EAStrbUVDLeftCTop
Width)HeightTabOrder Text	EAStrbUVD  TEdit	ERStrbUVDLeftBTop$Width)HeightTabOrderText	ERStrbUVD   	TGroupBox	GroupBox4Left� TopWidth� HeightBCaptionRBSTabOrder TLabelLabel15LeftTopWidth2Height	AlignmenttaRightJustifyCaption
by azimuth  TLabelLabel16LeftTop(Width)Height	AlignmenttaRightJustifyCaptionby range  TLabelLabel20LefttTop
WidthHeightCaptiono  TLabelLabel23LeftrTop(WidthHeightCaptionkm  TEdit	EAStrbRBSLeftITop
Width)HeightTabOrder Text	EAStrbRBS  TEdit	ERStrbRBSLeftITop$Width)HeightTabOrderText	ERStrbRBS   	TGroupBox	GroupBox5LeftTopWidth{HeightBCaptionPrimary channelTabOrder TLabelLabel17LeftTopWidth2Height	AlignmenttaRightJustifyCaption
by azimuth  TLabelLabel18LeftTop+Width)Height	AlignmenttaRightJustifyCaptionby range  TLabelLabel21LeftjTopWidthHeightCaptiono  TLabelLabel24LeftjTop(WidthHeightCaptionkm  TEditEAStrbPLeft=TopWidth)HeightTabOrder TextEAStrbP  TEditERStrbPLeft<Top'Width)HeightTabOrderTextERStrbP    	TMainMenu	MainMenu1LeftHToph 	TMenuItemmnuHelpCaptionHelpOnClickmnuHelpClick   TColorDialogColorDialog1Ctl3D	CustomColors.StringsColorA=000000ColorB=FFFFFFColorC=00F000ColorD=008000ColorE=FFFF00ColorF=00FFFFColorG=00FF00ColorH=FF00FFColorI=0000C0ColorJ=0000FFColorK=111111ColorL=AABBCCColorM=DDFF11ColorN=ABCDEFColorO=123456ColorP=888888  Left@Top0   