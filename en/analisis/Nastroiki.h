//---------------------------------------------------------------------------
#ifndef NastroikiH
#define NastroikiH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
#include <Menus.hpp>
#include <ExtCtrls.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TFNastroiki : public TForm
{
__published:	// IDE-managed Components
        TCheckBox *IsVisNumber;
        TCheckBox *IsVisTime;
        TGroupBox *GroupBox1;
        TCheckBox *IsCheckNumber;
        TCheckBox *IsCheckVys;
        TCheckBox *IsCheckTime;
        TCheckBox *IskluchVys;
        TCheckBox *IsCheckDlnst;
        TEdit *Dlnst1;
        TLabel *Label3;
        TEdit *Dlnst2;
        TLabel *Label4;
        TCheckBox *IsCheckAzmt;
        TLabel *Label5;
        TEdit *Azmt11;
        TLabel *Label6;
        TEdit *Azmt12;
        TLabel *Label7;
        TLabel *Label8;
        TEdit *Azmt21;
        TEdit *Azmt22;
        TLabel *Label9;
        TLabel *Label10;
        TCheckBox *KanalP;
        TCheckBox *KanalS;
        TCheckBox *KanalPS;
        TLabel *Label11;
        TCSpinEdit *DiametrMetok;
        TLabel *Label12;
        TLabel *Label13;
        TButton *B_OK;
        TButton *BCancel;
        TGroupBox *GroupBox6;
        TCheckBox *IsAnimNomer;
        TCheckBox *IsAnimVysota;
        TCheckBox *IsAnimTime;
        TCheckBox *IsHvost;
        TLabel *Label1;
        TCSpinEdit *DlinaHvosta;
        TMainMenu *MainMenu1;
        TMenuItem *mnuHelp;
        TButton *B_PoUmolchaniu;
        TRadioGroup *RBNapravlenia;
        TGroupBox *GroupBox7;
        TCheckBox *VydelyatSevera;
        TCheckBox *IsIstinnyRightDopData;
        TCheckBox *IsLojnyyInfoVyvod;
        TGroupBox *GBMinMaxAndMaxMin;
        TLabel *Label26;
        TEdit *MaxMinD;
        TLabel *Label27;
        TEdit *MinMaxD;
        TCheckBox *FindRadialTraekt;
        TRadioButton *FindForP_PS;
        TRadioButton *FindForS_PS;
        TRadioGroup *RGRejim;
        TGroupBox *GBColors;
        TLabel *Label28;
        TShape *SOblastCol;
        TShape *SKolca1Col;
        TLabel *Label30;
        TShape *SKolca2Col;
        TLabel *Label29;
        TLabel *Label31;
        TShape *SRad90Col;
        TShape *Shape5;
        TShape *SRad30Col;
        TLabel *Label32;
        TShape *S_P_Col;
        TLabel *Label33;
        TLabel *Label34;
        TShape *S_S_Col;
        TShape *S_PS_Col;
        TLabel *Label35;
        TLabel *Label36;
        TShape *S_RS_UVD_Col;
        TShape *S_RS_RBS_Col;
        TLabel *Label37;
        TLabel *Label38;
        TShape *S_RS_P_Col;
        TLabel *Label39;
        TShape *SRamkaCol;
        TLabel *Label40;
        TShape *S_Text_Col;
        TColorDialog *ColorDialog1;
        TLabel *Label41;
        TCSpinEdit *DopDVys;
        TLabel *Label42;
        TLabel *Label43;
        TEdit *MestoIspytaniy;
        TLabel *Label44;
        TEdit *KO_UVD;
        TLabel *Label45;
        TEdit *KO_RBS;
        TEdit *E_RazmerOkna;
        TLabel *Label47;
        TGroupBox *GroupBox8;
        TLabel *Label48;
        TCSpinEdit *CSEPrintO;
        TCSpinEdit *CSEPrintVK;
        TCSpinEdit *CSEPrintPK;
        TCheckBox *CBSetMaxDalnGOST;
        TCheckBox *cbIsCheckSignalPK;
        TLabel *Label51;
        TEdit *eMinSignalPK;
        TGroupBox *gbSled;
        TCheckBox *cbPlotIn;
        TGroupBox *GroupBox2;
        TGroupBox *GroupBox3;
        TGroupBox *GroupBox4;
        TGroupBox *GroupBox5;
        TLabel *Label2;
        TLabel *Label14;
        TEdit *EAStrbUVD;
        TEdit *ERStrbUVD;
        TLabel *Label15;
        TEdit *EAStrbRBS;
        TEdit *ERStrbRBS;
        TLabel *Label16;
        TLabel *Label17;
        TEdit *EAStrbP;
        TEdit *ERStrbP;
        TLabel *Label18;
        TLabel *Label19;
        TLabel *Label20;
        TLabel *Label21;
        TLabel *Label22;
        TLabel *Label23;
        TLabel *Label24;
        TLabel *Label53;
        TLabel *Label54;
        void __fastcall FormShow(TObject *Sender);
        void __fastcall B_OKClick(TObject *Sender);
        void __fastcall BCancelClick(TObject *Sender);
        void __fastcall IsCheckVysClick(TObject *Sender);
        void __fastcall IsCheckDlnstClick(TObject *Sender);
        void __fastcall IsCheckAzmtClick(TObject *Sender);
        void __fastcall IsHvostClick(TObject *Sender);
        void __fastcall IsVisNumberClick(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall mnuHelpClick(TObject *Sender);
        void __fastcall B_PoUmolchaniuClick(TObject *Sender);
        void __fastcall IsVisTimeClick(TObject *Sender);
        void __fastcall IsCheckNumberClick(TObject *Sender);
        void __fastcall IsCheckTimeClick(TObject *Sender);
        void __fastcall IskluchVysClick(TObject *Sender);
        void __fastcall Dlnst1Change(TObject *Sender);
        void __fastcall Dlnst2Change(TObject *Sender);
        void __fastcall Azmt11Change(TObject *Sender);
        void __fastcall Azmt12Change(TObject *Sender);
        void __fastcall KanalPClick(TObject *Sender);
        void __fastcall KanalSClick(TObject *Sender);
        void __fastcall KanalPSClick(TObject *Sender);
        void __fastcall RBNapravleniaClick(TObject *Sender);
        void __fastcall RGRejimClick(TObject *Sender);
        void __fastcall FindRadialTraektClick(TObject *Sender);
        void __fastcall FindForP_PSClick(TObject *Sender);
        void __fastcall FindForS_PSClick(TObject *Sender);
        void __fastcall MaxMinDChange(TObject *Sender);
        void __fastcall MinMaxDChange(TObject *Sender);
        void __fastcall MouseDownSetColour(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y);
        bool __fastcall FormHelp(WORD Command, int Data, bool &CallHelp);
        void __fastcall FormCreate(TObject *Sender);


private:	// User declarations

public:		// User declarations
        __fastcall TFNastroiki(TComponent* Owner);



        double D1,D2;
        double A11,A12,A21,A22;
        double MinMaxD1,MaxMinD1;
        bool Canceled;
        bool Modified;
        AnsiString CHMHelpFile;
        AnsiString CHMHtml;

};
//---------------------------------------------------------------------------
extern PACKAGE TFNastroiki *FNastroiki;
//---------------------------------------------------------------------------
#endif
