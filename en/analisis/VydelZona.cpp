/*��. ��������� VydelZona.h*/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>

#include <stdio.h>
#include "tools.h"
#include "VydelZona.h"
#include "GlavOkno.h"
#include "DataTochkaTraekt.h"
#include "GraphDOrA.h"
#include "work_ini_form.h"




//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
#define ABS(X) (((X) > 0) ? (X) : (-(X)))

TFVydelZona *FVydelZona;
//---------------------------------------------------------------------------
__fastcall TFVydelZona::TFVydelZona(TComponent* Owner)
        : TForm(Owner)
{
 N_of_Shapes=0;

 DT1=NULL;
 CHMHtml="vydel_zona.htm";

}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::FreeAllShapes(void)
{
  if(N_of_Shapes)
  {
    delete []DT1;
    DT1=NULL;

    N_of_Shapes=0;
  }

}


int __fastcall TFVydelZona::VydelZonaMake(double LeftKm, double BottomKm,
                                     double RightKm, double TopKm,
                                     struct DANNYE_TOCHNOST *DT,
                                     int N_of_DT, double Kurs)
{
  extern AnsiString PutKProgramme;
  AnsiString Ikonka;
  long i;
  int HS=750,WS=1000; //������� ������
  Hide();  //������ ���������� ���� ���� ������ �������

//��������� ������� ������

  WS=GetSystemMetrics(SM_CXSCREEN);
  HS=GetSystemMetrics(SM_CYSCREEN);

  if(WS<100)WS=500;
  if(HS<100)HS=500;


  LeftKm1=LeftKm; BottomKm1=BottomKm; RightKm1=RightKm; TopKm1=TopKm;

  FreeAllShapes();
//������� �������
  if(TopKm-BottomKm>RightKm-LeftKm)
  {
    ClientHeight1=HS-90;
    ClientWidth1=Okrugl(ClientHeight*((RightKm-LeftKm)/(TopKm-BottomKm)));
    ClientHeight=ClientHeight1+40;
    ClientWidth=ClientWidth1+20;
  }else{
    ClientWidth1=WS-50;
    ClientHeight1=Okrugl(ClientWidth*((TopKm-BottomKm)/(RightKm-LeftKm)));
    ClientHeight=ClientHeight1+40;
    ClientWidth=ClientWidth1+20;

  }

  X_k_Y=((double)(ClientWidth1))/ClientHeight1;

//������� ������
  DT1=new DANNYE_TOCHNOST[N_of_DT];


//������� ������ ��� ����

  N_of_Shapes=N_of_DT;

  memcpy(DT1,DT,sizeof(DANNYE_TOCHNOST)*N_of_Shapes);

//��������� ������� ���� TShape ��� ������� �� �� �
  DopDWidth=Width-ClientWidth;
  DopDHeight=Height-ClientHeight;

  if(Kurs<45||Kurs>=315)
  {
     Ikonka=PutKProgramme+String("\\ARW02UP.ICO");
  }else if(Kurs>=45&&Kurs<135)
  {
     Ikonka=PutKProgramme+String("\\ARW02RT.ICO");
  }else if(Kurs>=135&&Kurs<225)
  {
     Ikonka=PutKProgramme+String("\\ARW02DN.ICO");
  }else{
     Ikonka=PutKProgramme+String("\\ARW02LT.ICO");
  }
  if(FileExists(Ikonka))
  {
    Icon->LoadFromFile(Ikonka);
  }

  Left=WS/2-Width/2;
  Top=0;

  Show();
  return 1;
}

void __fastcall TFVydelZona::FormPaint(TObject *Sender)
{
  int i;
  double KoefX,KoefY;
  int X1,Y1,X2,Y2;

  ClientWidth1=ClientWidth-20;
  ClientHeight1=ClientHeight-40;

  //�������� ����
  KoefX=(ClientWidth1-1)/((double)(RightKm1-LeftKm1));
  KoefY=(ClientHeight1-1)/((double)(BottomKm1-TopKm1));




  Canvas->Pen->Style=psSolid; Canvas->Brush->Style=bsSolid;
  Canvas->Brush->Color=clGray;
  Canvas->Pen->Color=clGray;
  Canvas->Rectangle(0,0,ClientWidth,ClientHeight);
  Canvas->Pen->Color=clBlue;
  for(i=0;i<N_of_Shapes;i++)
  {
     X1=Okrugl((DT1[i].X/1000.0-LeftKm1)*KoefX)+10;
     Y1=Okrugl((DT1[i].Y/1000.0-TopKm1)*KoefY)+30;
     X2=Okrugl((DT1[i].XA/1000.0-LeftKm1)*KoefX)+10;
     Y2=Okrugl((DT1[i].YA/1000.0-TopKm1)*KoefY)+30;
     Canvas->Brush->Color=clWhite;
     Canvas->Ellipse(X1-3,Y1-3,X1+3,Y1+3);
     Canvas->Brush->Color=clRed;
     Canvas->Ellipse(X2-3,Y2-3,X2+3,Y2+3);
     Canvas->MoveTo(X1,Y1);
     Canvas->LineTo(X2,Y2);
  }



}
//---------------------------------------------------------------------------



void __fastcall TFVydelZona::FormDestroy(TObject *Sender)
{
  FreeAllShapes();
}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::FormResize(TObject *Sender)
{
   ShOkrujnost->Visible=false;
  

  Repaint();
}
//---------------------------------------------------------------------------




void __fastcall TFVydelZona::FormCanResize(TObject *Sender, int &NewWidth,
      int &NewHeight, bool &Resize)
{
   int ClientWidth2,ClientHeight2;

   if(N_of_Shapes<=0)return;


   ClientWidth2=NewWidth-DopDWidth;
   ClientHeight2=NewHeight-DopDHeight;

   if(NewWidth==Width)
   {
     ClientHeight1=ClientHeight2-40;
     ClientWidth1=Okrugl(ClientHeight1*X_k_Y);
     ClientWidth2=ClientWidth1+20;

   }else{
     ClientWidth1=ClientWidth2-20;
     ClientHeight1=Okrugl(ClientWidth1/X_k_Y);
     ClientHeight2=ClientHeight1+40;
   }

   NewWidth=ClientWidth2+DopDWidth;
   NewHeight=ClientHeight2+DopDHeight;
   Resize=true;
}
//---------------------------------------------------------------------------




void __fastcall TFVydelZona::FormMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   int i,i_n;
   long X1,Y1,X1n=-100,Y1n=-100;
   double KoefX,KoefY;
   long KvadratD,MinKvadratD=1000;
   ClientWidth1=ClientWidth-20;
    ClientHeight1=ClientHeight-40;

  //�������� ����
  KoefX=(ClientWidth1-1)/((double)(RightKm1-LeftKm1));
  KoefY=(ClientHeight1-1)/((double)(BottomKm1-TopKm1));

//���� ������ ������ ������ ����
   if(Button==mbRight)
   {
     for(i=0;i<N_of_Shapes;i++)
     {
       X1=Okrugl((DT1[i].X/1000.0-LeftKm1)*KoefX)+10;
       Y1=Okrugl((DT1[i].Y/1000.0-TopKm1)*KoefY)+30;
       if(ABS(X-X1)<=20&&ABS(Y-Y1)<=20)
       {
         KvadratD=(X-X1)*(X-X1)+(Y-Y1)*(Y-Y1);
         if(KvadratD<=400&&KvadratD<MinKvadratD)
         {
            MinKvadratD=KvadratD;
            X1n=X1;Y1n=Y1;
            i_n=i;
         }
       }
     }
   }

   if(X1n>=0&&Y1n>=0)
   {
     ShOkrujnost->Left=X1n-10;     ShOkrujnost->Top=Y1n-10;
     FDataTraekt->MyShow(DT1+i_n);

     ShOkrujnost->Visible=true;

   }

}

//---------------------------------------------------------------------------


void __fastcall TFVydelZona::FormCreate(TObject *Sender)
{
  //iffWorkWithIniFile(this, String("analis_lan.ini"));
  ShOkrujnost->Visible=false;  //�� �����
  FDataTraekt->Hide();
}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::FormMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{

    if(Button==mbRight)
    {
        ShOkrujnost->Visible=false;
        FDataTraekt->Hide();
    }
}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::FormMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
   if(X<0||X>=ClientWidth||Y<0||Y>=ClientHeight)
   {
       ShOkrujnost->Visible=false;
       FDataTraekt->Hide();
   }
}
//---------------------------------------------------------------------------


void __fastcall TFVydelZona::ShOkrujnostMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  FormMouseDown(NULL,Button,Shift,X+ShOkrujnost->Left,Y+ShOkrujnost->Top);
}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::ShOkrujnostMouseUp(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    FormMouseUp(NULL,Button,Shift,X+ShOkrujnost->Left,Y+ShOkrujnost->Top);
        
}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::ShOkrujnostMouseMove(TObject *Sender,
      TShiftState Shift, int X, int Y)
{
  FormMouseMove(NULL,Shift,X+ShOkrujnost->Left,Y+ShOkrujnost->Top);        
}
//---------------------------------------------------------------------------



void __fastcall TFVydelZona::FormShow(TObject *Sender)
{
   ShOkrujnost->Visible=false;

}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::FormHide(TObject *Sender)
{
   FDataTraekt->Hide();
   if(FGraphDOrA_Daln)
   {
     FGraphDOrA_Daln->Hide();
   }
   if(FGraphDOrA_Azmt)
   {
     FGraphDOrA_Azmt->Hide();
   }
}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::Dt1Click(TObject *Sender)
{
  if(FGraphDOrA_Daln==NULL)
  {
     FGraphDOrA_Daln=new TFGraphDOrA(this);
  }
  FGraphDOrA_Daln->CHMHelpFile=CHMHelpFile;
  
  FGraphDOrA_Daln->ShowGraphicDlnst(DT1,N_of_Shapes);
}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::t1Click(TObject *Sender)
{
    if(FGraphDOrA_Azmt==NULL)
    {
      FGraphDOrA_Azmt=new TFGraphDOrA(this);
    }

    FGraphDOrA_Azmt->CHMHelpFile=CHMHelpFile;
    FGraphDOrA_Azmt->ShowGraphicAzmt(DT1,N_of_Shapes);
}
//---------------------------------------------------------------------------


void __fastcall TFVydelZona::N1Click(TObject *Sender)
{
 AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);

}
//---------------------------------------------------------------------------

void __fastcall TFVydelZona::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
 switch(Key)
  {

    case VK_F1:
         N1Click(NULL);
    break;
  };
}
//---------------------------------------------------------------------------

