//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>

#include <stdio.h>
#include "ParamForP.h"
#include "work_ini_form.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TF_Param_P *F_Param_P;
//---------------------------------------------------------------------------
__fastcall TF_Param_P::TF_Param_P(TComponent* Owner)
        : TForm(Owner)
{
        CHMHtml="";
}
//---------------------------------------------------------------------------
void __fastcall TF_Param_P::FormShow(TObject *Sender)
{
   char Strka[100];
   Canceled=true;
   sprintf(Strka,"%.02lf",RSA);
   E_RSA->Text=Strka;
   sprintf(Strka,"%.0lf",RSD*1000);
   E_RSD->Text=Strka;


}
//---------------------------------------------------------------------------
void __fastcall TF_Param_P::Button1Click(TObject *Sender)
{
  int Ret;
  double Chislo;
  Ret=sscanf(E_RSA->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"Azimuth strobe value is set incorrectly!", "Error!",MB_OK);
    return;
  }
  if(Chislo<=0.01)
  {
    MessageBox(NULL,"Azimuth strobe value is too small!", "Error!",MB_OK);
    return;
  }
  if(Chislo>=20.01)
  {
    MessageBox(NULL,"Azimuth strobe value should be less than 20 �","Error!",MB_OK);
    return;
  }
  RSA=Chislo;

  Ret=sscanf(E_RSD->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"Range strobe value is set incorrectly!","Error!",MB_OK);
    return;
  }
  if(Chislo<=1)
  {
    MessageBox(NULL,"Range strobe value is too small!","Error!",MB_OK);
    return;
  }
  if(Chislo>=5000)
  {
    MessageBox(NULL,"Range strobe value should be less than 5000 m!","Error!",MB_OK);
    return;
  }
  RSD=Chislo/1000.0;
  Canceled=false;
  Close();

}
//---------------------------------------------------------------------------
void __fastcall TF_Param_P::Button2Click(TObject *Sender)
{
    Close();        
}
//---------------------------------------------------------------------------

void __fastcall TF_Param_P::MHelpClick(TObject *Sender)
{
 AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);
}
//---------------------------------------------------------------------------

void __fastcall TF_Param_P::FormCreate(TObject *Sender)
{
 //iffWorkWithIniFile(this, String("analis_lan.ini"));
}
//---------------------------------------------------------------------------

void __fastcall TF_Param_P::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
     if(Key==VK_F1)
     {
         MHelpClick(NULL);
     }        
}
//---------------------------------------------------------------------------

