//---------------------------------------------------------------------------

#ifndef max_dalnost_gostH
#define max_dalnost_gostH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "CSPIN.h"
//---------------------------------------------------------------------------
class TFMaxDalnostGOST : public TForm
{
__published:	// IDE-managed Components
        TCSpinEdit *CSpinEdit1;
        TLabel *Label1;
        TButton *BInput;
        void __fastcall BInputClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TFMaxDalnostGOST(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFMaxDalnostGOST *FMaxDalnostGOST;
//---------------------------------------------------------------------------
#endif
