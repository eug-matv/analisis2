//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <stdio.h>

//for Microsoft Help
#include <htmlhelp.h>

#include "ParamForS.h"
#include "work_ini_form.h"

 
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TF_Param_S *F_Param_S;
//---------------------------------------------------------------------------
__fastcall TF_Param_S::TF_Param_S(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TF_Param_S::Button1Click(TObject *Sender)
{
  int Ret;
  double Chislo;
  Ret=sscanf(E_RSA_UVD->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"Azimuth strobe value for UVD is set incorrectly!","Error!",MB_OK);
    return;
  }
  if(Chislo<=0.01)
  {
    MessageBox(NULL,"Azimuth strobe value for UVD is too small!","Error!",MB_OK);
    return;
  }
  if(Chislo>=20.01)
  {
    MessageBox(NULL,"Azimuth strobe value should be less 20 � for UVD!","Error!",MB_OK);
    return;
  }
  RSA_uvd=Chislo;

  Ret=sscanf(E_RSD_UVD->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"Range strobe value for UVD is set incorrectly!","Error!",MB_OK);
    return;
  }
  if(Chislo<=1)
  {
    MessageBox(NULL,"Range strobe value for UVD is too small!","Error!",MB_OK);
    return;
  }
  if(Chislo>=5000)
  {
    MessageBox(NULL,"Range strobe value should be less 5000 m for UVD","Error!",MB_OK);
    return;
  }
  RSD_uvd=Chislo/1000.0;


  Ret=sscanf(E_RSA_RBS->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"Azimuth strobe value for RBS is set incorrectly!","Error!",MB_OK);
    return;
  }
  if(Chislo<=0.01)
  {
    MessageBox(NULL,"Azimuth strobe value for RBS is too small!","Error!",MB_OK);
    return;
  }
  if(Chislo>=20.01)
  {
    MessageBox(NULL,"Azimuth strobe value should be less 20 � for RBS!","Error!",MB_OK);
    return;
  }
  RSA_rbs=Chislo;

  Ret=sscanf(E_RSD_RBS->Text.c_str(),"%lf",&Chislo);
  if(Ret!=1)
  {
    MessageBox(NULL,"Range strobe value for RBS is set incorrectly!","Error!",MB_OK);
    return;
  }
  if(Chislo<=1)
  {
    MessageBox(NULL,"Range strobe value for RBS is too small!","Error!",MB_OK);
    return;
  }
  if(Chislo>=5000)
  {
    MessageBox(NULL,"Range strobe value should be less 5000 m for RBS","Error!",MB_OK);
    return;
  }
  RSD_rbs=Chislo/1000.0;



  Canceled=false;
  Close();


}
//---------------------------------------------------------------------------
void __fastcall TF_Param_S::FormShow(TObject *Sender)
{
   char Strka[100];
   Canceled=true;
   sprintf(Strka,"%.02lf",RSA_uvd);
   E_RSA_UVD->Text=Strka;
   sprintf(Strka,"%.0lf",RSD_uvd*1000);
   E_RSD_UVD->Text=Strka;
   sprintf(Strka,"%.02lf",RSA_rbs);
   E_RSA_RBS->Text=Strka;
   sprintf(Strka,"%.0lf",RSD_rbs*1000);
   E_RSD_RBS->Text=Strka;


}
//---------------------------------------------------------------------------
void __fastcall TF_Param_S::Button2Click(TObject *Sender)
{
  Close();        
}
//---------------------------------------------------------------------------
void __fastcall TF_Param_S::MHelpClick(TObject *Sender)
{
AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);
        
}
//---------------------------------------------------------------------------

void __fastcall TF_Param_S::FormCreate(TObject *Sender)
{
//iffWorkWithIniFile(this, String("analis_lan.ini"));
}
//---------------------------------------------------------------------------

void __fastcall TF_Param_S::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if(Key==VK_F1)
     {
         MHelpClick(NULL);
     }                
}
//---------------------------------------------------------------------------

