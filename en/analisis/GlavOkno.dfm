object FGlavOkno: TFGlavOkno
  Left = 196
  Top = 68
  Width = 675
  Height = 664
  Caption = 'Analisis.Version 2'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  ObjectMenuItem = mnuClearSelection
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object RightPanel: TPanel
    Left = 476
    Top = 0
    Width = 191
    Height = 559
    Align = alRight
    BevelOuter = bvNone
    Color = 16770239
    TabOrder = 0
    object GBMashtab: TGroupBox
      Left = 0
      Top = 0
      Width = 191
      Height = 65
      Align = alTop
      Caption = 'Scale'
      TabOrder = 0
      TabStop = True
      object RB_50km: TRadioButton
        Left = 27
        Top = 13
        Width = 49
        Height = 17
        Caption = '50 km'
        TabOrder = 0
        TabStop = True
        OnClick = RB_50kmClick
      end
      object RB_100km: TRadioButton
        Left = 27
        Top = 30
        Width = 56
        Height = 17
        Caption = '100 km'
        TabOrder = 1
        TabStop = True
        OnClick = RB_100kmClick
      end
      object RB_400km: TRadioButton
        Left = 91
        Top = 30
        Width = 56
        Height = 17
        Caption = '400 km'
        Checked = True
        TabOrder = 3
        TabStop = True
        OnClick = RB_400kmClick
      end
      object RB_200km: TRadioButton
        Left = 91
        Top = 13
        Width = 62
        Height = 17
        Caption = '200 km'
        TabOrder = 2
        TabStop = True
        OnClick = RB_200kmClick
      end
      object RadioButton1: TRadioButton
        Left = 27
        Top = 46
        Width = 56
        Height = 17
        Caption = '450 km'
        TabOrder = 5
        Visible = False
        OnClick = RadioButton1Click
      end
      object RB_Uvelichenie: TRadioButton
        Left = 27
        Top = 48
        Width = 105
        Height = 15
        Caption = 'Zoom in'
        PopupMenu = PM_SpisokBortov
        TabOrder = 4
        TabStop = True
        Visible = False
      end
    end
    object StaticText1: TStaticText
      Left = 6
      Top = 69
      Width = 140
      Height = 20
      Alignment = taCenter
      BorderStyle = sbsSingle
      Caption = 'Analysis of data file'
      Color = 8421631
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
    object MVolDataSel: TMemo
      Left = 6
      Top = 99
      Width = 181
      Height = 34
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MVolDataSel')
      ReadOnly = True
      TabOrder = 2
    end
    object MVolVydelData: TMemo
      Left = 6
      Top = 134
      Width = 181
      Height = 33
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MVolVydelData')
      ReadOnly = True
      TabOrder = 3
    end
    object MVydelenoSek: TMemo
      Left = 6
      Top = 169
      Width = 181
      Height = 25
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MVydelenoSek')
      ReadOnly = True
      TabOrder = 4
    end
    object MKurs: TMemo
      Left = 6
      Top = 196
      Width = 181
      Height = 25
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MKurs')
      ReadOnly = True
      TabOrder = 5
    end
    object MSredSkorost: TMemo
      Left = 6
      Top = 223
      Width = 181
      Height = 27
      TabStop = False
      Alignment = taCenter
      Color = 14540253
      Lines.Strings = (
        'MSredSkorost')
      ReadOnly = True
      TabOrder = 6
    end
    object Panel2: TPanel
      Left = 0
      Top = 307
      Width = 191
      Height = 252
      Align = alBottom
      BevelOuter = bvNone
      Color = 16770239
      TabOrder = 7
      DesignSize = (
        191
        252)
      object Label6: TLabel
        Left = 145
        Top = 156
        Width = 32
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = 'Slower'
      end
      object M_SKO_D: TMemo
        Left = 6
        Top = 2
        Width = 167
        Height = 39
        TabStop = False
        Alignment = taCenter
        Color = 14540253
        Lines.Strings = (
          'M_SKO_D')
        ReadOnly = True
        TabOrder = 0
      end
      object M_SKO_A: TMemo
        Left = 6
        Top = 44
        Width = 167
        Height = 37
        TabStop = False
        Alignment = taCenter
        Color = 14540253
        Lines.Strings = (
          'M_SKO_A')
        ReadOnly = True
        TabOrder = 1
      end
      object BTochnost: TButton
        Left = 7
        Top = 93
        Width = 64
        Height = 20
        Caption = 'Accuracy'
        TabOrder = 2
        OnClick = BTochnostClick
      end
      object GroupBox4: TGroupBox
        Left = 8
        Top = 120
        Width = 105
        Height = 40
        Caption = 'Order'
        TabOrder = 3
        object CSE_Poryadok: TComboBox
          Left = 8
          Top = 13
          Width = 89
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = '1'
          OnChange = CSE_PoryadokChange
          OnKeyPress = CSE_PoryadokKeyPress
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            'DIFFERENCE')
        end
      end
      object BZonaP: TButton
        Left = 133
        Top = 96
        Width = 55
        Height = 20
        Caption = 'P area'
        TabOrder = 4
        OnClick = BZonaPClick
      end
      object BZonaS: TButton
        Left = 133
        Top = 120
        Width = 55
        Height = 18
        Caption = 'S area'
        TabOrder = 5
        OnClick = BZonaSClick
      end
      object BStart: TButton
        Left = 9
        Top = 193
        Width = 44
        Height = 27
        HelpContext = 4110
        Anchors = [akRight, akBottom]
        Caption = 'Start'
        TabOrder = 6
        OnClick = BStartClick
      end
      object PBPause: TPanel
        Left = 53
        Top = 193
        Width = 83
        Height = 27
        HelpContext = 4110
        BevelWidth = 2
        Caption = 'Pause'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        OnMouseDown = PBPauseMouseDown
        OnMouseUp = PBPauseMouseUp
      end
      object BEnd: TButton
        Left = 137
        Top = 192
        Width = 47
        Height = 27
        HelpContext = 4110
        Caption = 'Stop'
        PopupMenu = PM_SpisokBortov
        TabOrder = 8
        OnClick = BEndClick
      end
      object TBDTimeAnim: TTrackBar
        Left = 12
        Top = 168
        Width = 177
        Height = 20
        Anchors = [akRight, akBottom]
        Max = 25
        Min = 1
        Orientation = trHorizontal
        Frequency = 1
        Position = 12
        SelEnd = 0
        SelStart = 0
        TabOrder = 9
        ThumbLength = 15
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TBDTimeAnimChange
      end
      object CBNazad: TCheckBox
        Left = 14
        Top = 224
        Width = 161
        Height = 15
        HelpContext = 4110
        Anchors = [akRight]
        Caption = 'Backward motion'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
        OnClick = CBNazadClick
      end
    end
  end
  object NizPanel: TPanel
    Left = 0
    Top = 559
    Width = 667
    Height = 47
    Align = alBottom
    BevelOuter = bvNone
    Color = 16770239
    TabOrder = 1
    DesignSize = (
      667
      47)
    object CGauge1: TCGauge
      Left = 8
      Top = 7
      Width = 649
      Height = 18
      Anchors = [akLeft, akTop, akRight]
      ShowText = False
      ForeColor = clPurple
      Progress = 50
      Visible = False
    end
    object Label11: TLabel
      Left = 176
      Top = 32
      Width = 38
      Height = 13
      Caption = 'Label11'
    end
    object PB_Animate: TProgressBar
      Left = 831
      Top = 26
      Width = 174
      Height = 13
      Anchors = [akRight]
      Min = 0
      Max = 100000
      TabOrder = 0
    end
    object StatusBar1: TStatusBar
      Left = 0
      Top = 25
      Width = 667
      Height = 22
      Color = 16770239
      Panels = <
        item
          Width = 50
        end>
      SimplePanel = False
    end
  end
  object MainPanel: TPanel
    Left = 0
    Top = 0
    Width = 476
    Height = 559
    Align = alClient
    BevelOuter = bvNone
    Caption = 'MainPanel'
    TabOrder = 2
    object VerhPanel: TPanel
      Left = 0
      Top = 0
      Width = 476
      Height = 65
      Align = alTop
      BevelOuter = bvNone
      Color = 16565571
      TabOrder = 0
      object GBVremya: TGroupBox
        Left = 97
        Top = 0
        Width = 128
        Height = 65
        Align = alLeft
        Caption = 'Time'
        TabOrder = 2
        object LCurTime: TLabel
          Left = 8
          Top = 24
          Width = 45
          Height = 13
          Caption = 'LCurTime'
        end
        object Label1: TLabel
          Left = 9
          Top = 15
          Width = 26
          Height = 16
          Caption = 'from'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 11
          Top = 39
          Width = 11
          Height = 16
          Caption = 'to'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DTP1: TDateTimePicker
          Left = 43
          Top = 14
          Width = 71
          Height = 21
          CalAlignment = dtaLeft
          Date = 38691.3821059144
          Time = 38691.3821059144
          Color = clBtnFace
          DateFormat = dfShort
          DateMode = dmUpDown
          Enabled = False
          Kind = dtkTime
          ParseInput = False
          TabOrder = 0
        end
        object DTP2: TDateTimePicker
          Left = 43
          Top = 37
          Width = 70
          Height = 21
          CalAlignment = dtaLeft
          Date = 0.382105914351996
          Time = 0.382105914351996
          Color = clBtnFace
          DateFormat = dfShort
          DateMode = dmUpDown
          Enabled = False
          Kind = dtkTime
          ParseInput = False
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 225
        Top = 0
        Width = 49
        Height = 65
        Align = alLeft
        Caption = #1057#1091#1090#1082#1080
        TabOrder = 3
        Visible = False
        object CSE_Sutki1: TCSpinEdit
          Left = 4
          Top = 13
          Width = 41
          Height = 22
          Color = clBtnFace
          Enabled = False
          MaxValue = 5
          MinValue = 1
          TabOrder = 0
          Value = 1
        end
        object CSE_Sutki2: TCSpinEdit
          Left = 4
          Top = 38
          Width = 41
          Height = 22
          Color = clBtnFace
          Enabled = False
          MaxValue = 5
          MinValue = 1
          PopupMenu = PM_SpisokBortov
          TabOrder = 1
          Value = 1
        end
      end
      object GroupBox3: TGroupBox
        Left = 274
        Top = 0
        Width = 151
        Height = 65
        Align = alLeft
        Caption = 'Altitude'
        TabOrder = 4
        object Label3: TLabel
          Left = 17
          Top = 16
          Width = 26
          Height = 16
          Caption = 'from'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 18
          Top = 40
          Width = 11
          Height = 16
          Caption = 'to'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 128
          Top = 27
          Width = 11
          Height = 16
          Caption = 'm'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object EVysota1: TEdit
          Left = 51
          Top = 15
          Width = 44
          Height = 21
          TabOrder = 0
          Text = 'EVysota1'
          OnExit = EVysota1Exit
          OnKeyDown = EVysota1KeyDown
        end
        object CSBVysota1: TCSpinButton
          Left = 96
          Top = 14
          Width = 20
          Height = 22
          DownGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000808000808000
            8080000000008080008080008080008080000080800080800080800000000000
            0000000000808000808000808000008080008080000000000000000000000000
            0000000080800080800000808000000000000000000000000000000000000000
            0000008080000080800080800080800080800080800080800080800080800080
            8000}
          TabOrder = 1
          UpGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000000000000000
            0000000000000000000000000000008080000080800080800000000000000000
            0000000000000000808000808000008080008080008080000000000000000000
            0080800080800080800000808000808000808000808000000000808000808000
            8080008080000080800080800080800080800080800080800080800080800080
            8000}
          OnDownClick = CSBVysota1DownClick
          OnUpClick = CSBVysota1UpClick
        end
        object EVysota2: TEdit
          Left = 51
          Top = 39
          Width = 44
          Height = 21
          TabOrder = 2
          Text = 'EVysota1'
          OnExit = EVysota2Exit
          OnKeyDown = EVysota2KeyDown
        end
        object CSBVysota2: TCSpinButton
          Left = 96
          Top = 38
          Width = 20
          Height = 22
          DownGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000808000808000
            8080000000008080008080008080008080000080800080800080800000000000
            0000000000808000808000808000008080008080000000000000000000000000
            0000000080800080800000808000000000000000000000000000000000000000
            0000008080000080800080800080800080800080800080800080800080800080
            8000}
          TabOrder = 3
          UpGlyph.Data = {
            DE000000424DDE00000000000000360000002800000009000000060000000100
            180000000000A800000000000000000000000000000000000000008080008080
            0080800080800080800080800080800080800080800000808000000000000000
            0000000000000000000000000000008080000080800080800000000000000000
            0000000000000000808000808000008080008080008080000000000000000000
            0080800080800080800000808000808000808000808000000000808000808000
            8080008080000080800080800080800080800080800080800080800080800080
            8000}
          OnDownClick = CSBVysota2DownClick
          OnUpClick = CSBVysota2UpClick
        end
      end
      object Panel1: TPanel
        Left = 433
        Top = 0
        Width = 43
        Height = 65
        Align = alRight
        BevelInner = bvLowered
        BevelOuter = bvNone
        Color = 16565571
        TabOrder = 1
        object B_OK: TButton
          Left = 6
          Top = 24
          Width = 33
          Height = 20
          Caption = 'OK'
          TabOrder = 0
          OnClick = B_OKClick
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 97
        Height = 65
        Align = alLeft
        Caption = 'Aircraft list'
        TabOrder = 0
        object BSortSpisokBortov: TButton
          Left = 32
          Top = 13
          Width = 50
          Height = 19
          Caption = '1...9'
          TabOrder = 0
          OnClick = BSortSpisokBortovClick
        end
        object Button1: TButton
          Left = 4
          Top = 34
          Width = 90
          Height = 26
          Caption = 'View aircrafts'
          TabOrder = 1
          OnClick = Button1Click
        end
      end
    end
    object RisovPanel: TPanel
      Left = 4
      Top = 65
      Width = 472
      Height = 494
      Align = alClient
      AutoSize = True
      BevelOuter = bvNone
      Color = clBlack
      TabOrder = 1
      DesignSize = (
        472
        494)
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 472
        Height = 494
        Align = alClient
        ParentShowHint = False
        ShowHint = False
        OnMouseDown = Image1MouseDown
        OnMouseMove = Image1MouseMove
        OnMouseUp = Image1MouseUp
      end
      object Label9: TLabel
        Left = 119
        Top = 491
        Width = 156
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = '- In close proximity(P, UVD, RBS)'
        Color = clNone
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape5: TShape
        Left = 103
        Top = 491
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = 192
      end
      object Shape4: TShape
        Left = 89
        Top = 491
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clFuchsia
      end
      object Shape3: TShape
        Left = 75
        Top = 491
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clRed
      end
      object Label8: TLabel
        Left = 55
        Top = 492
        Width = 13
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = '- S'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape2: TShape
        Left = 38
        Top = 491
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clYellow
      end
      object Label7: TLabel
        Left = 18
        Top = 492
        Width = 13
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = '- P'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape1: TShape
        Left = 2
        Top = 491
        Width = 15
        Height = 14
        Anchors = [akLeft, akBottom]
        Brush.Color = clBlue
      end
      object Label10: TLabel
        Left = 790
        Top = 492
        Width = 20
        Height = 13
        Anchors = [akRight, akBottom]
        Caption = '- PS'
        Color = clBlack
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = True
      end
      object Shape6: TShape
        Left = 775
        Top = 491
        Width = 15
        Height = 14
        Anchors = [akRight, akBottom]
        Brush.Color = clLime
      end
      object SVydel: TShape
        Left = 120
        Top = 96
        Width = 605
        Height = 277
        Anchors = [akLeft, akTop, akRight, akBottom]
        Brush.Style = bsClear
        Pen.Color = clWhite
        Pen.Mode = pmXor
        Pen.Style = psDashDot
        OnMouseDown = SVydelMouseDown
        OnMouseMove = SVydelMouseMove
        OnMouseUp = SVydelMouseUp
      end
      object PanelStatus: TPanel
        Left = 64
        Top = 120
        Width = 723
        Height = 300
        Anchors = [akLeft, akTop, akRight, akBottom]
        TabOrder = 0
        Visible = False
        object LPanelStatus: TLabel
          Left = 1
          Top = 1
          Width = 721
          Height = 298
          Align = alClient
          Alignment = taCenter
          Caption = 'LPanelStatus'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
    end
    object LeftPanel: TPanel
      Left = 0
      Top = 65
      Width = 4
      Height = 494
      Align = alLeft
      TabOrder = 2
    end
  end
  object CBSpisokBortov: TListBox
    Left = 0
    Top = 60
    Width = 84
    Height = 21
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ItemHeight = 14
    MultiSelect = True
    ParentFont = False
    PopupMenu = PM_SpisokBortov
    TabOrder = 3
    Visible = False
  end
  object TimerPerezapis: TTimer
    Enabled = False
    OnTimer = TimerPerezapisTimer
    Left = 292
    Top = 561
  end
  object TimerAnimate: TTimer
    Enabled = False
    Interval = 100
    OnTimer = TimerAnimateTimer
    Left = 52
    Top = 312
  end
  object Timer2: TTimer
    OnTimer = Timer2Timer
    Left = 188
    Top = 465
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 204
    Top = 113
  end
  object SaveDialog1: TSaveDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 76
    Top = 105
  end
  object PM_SpisokBortov: TPopupMenu
    Left = 24
    Top = 48
    object PM_CopyToBuffer: TMenuItem
      Caption = 'Copy selected aircrafts information to buffer'
    end
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 20
    Top = 185
  end
  object MainMenu1: TMainMenu
    Left = 84
    Top = 328
    object mnuFile: TMenuItem
      Caption = 'File'
      object MListOfFiles: TMenuItem
        Caption = 'File list'
        Enabled = False
        OnClick = MListOfFilesClick
      end
      object N1: TMenuItem
        Caption = 'Open'
        OnClick = N1Click
      end
      object mnuSaveAs: TMenuItem
        Caption = 'Save data file'
        OnClick = mnuSaveAsClick
      end
      object mnuSaveVydelAs: TMenuItem
        Caption = 'Save selected data file'
        OnClick = mnuSaveVydelAsClick
      end
      object mnuSetUserInfo: TMenuItem
        Caption = 'Enter user information '
        OnClick = mnuSetUserInfoClick
      end
      object mnuSetDocDate: TMenuItem
        Caption = 'Enter information on documentation date'
        OnClick = mnuSetDocDateClick
      end
      object mnuSetDannyeInfo: TMenuItem
        Caption = 'Enter relevant information on data '
        OnClick = mnuSetDannyeInfoClick
      end
      object mnuIzvlech: TMenuItem
        Caption = 'Extract from file'
        object mnuIzvlechUVD: TMenuItem
          Caption = 'UVD'
          OnClick = mnuIzvlechUVDClick
        end
        object mnuIzvlechRBS: TMenuItem
          Caption = 'RBS'
          OnClick = mnuIzvlechRBSClick
        end
        object mnuIzvlechPS_S: TMenuItem
          Caption = 'PS and S'
          OnClick = mnuIzvlechPS_SClick
        end
        object mnuIzvlechS: TMenuItem
          Caption = 'S'
          OnClick = mnuIzvlechSClick
        end
        object mnuIzvlechP: TMenuItem
          Caption = 'P'
          OnClick = mnuIzvlechPClick
        end
        object mnuIzvlechP_PS: TMenuItem
          Caption = 'P and PS'
          OnClick = mnuIzvlechP_PSClick
        end
      end
      object N7: TMenuItem
        Caption = '-'
      end
      object mnuSpiskiBort: TMenuItem
        Caption = 'Lists of selected aircrafts'
        object mnu_Load_Selection: TMenuItem
          Caption = 'Open list of selected aircrafts'
          Enabled = False
        end
        object N9: TMenuItem
          Caption = 'Save list of selected aircrafts'
        end
        object mnuViewVisualBorts: TMenuItem
          Caption = 'Show list of displayed aircrafts'
          OnClick = mnuViewVisualBortsClick
        end
        object mnuSaveVydelList: TMenuItem
          Caption = 'Save list of aircrafts in selected area'
        end
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object mnuPrint: TMenuItem
        Caption = 'Printing'
        object mnuPrintPicture: TMenuItem
          Caption = 'Picture (Picture (printing data display area)'
          OnClick = mnuPrintPictureClick
        end
        object mnuPrintData1: TMenuItem
          Caption = 'Data table'
          Visible = False
        end
        object mnuPrintData: TMenuItem
          Caption = 'Selected data table'
          Visible = False
        end
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object mnuExit: TMenuItem
        Caption = 'Exit'
        OnClick = mnuExitClick
      end
    end
    object mnuDates: TMenuItem
      Caption = 'Data'
      object mnuShowDates: TMenuItem
        Caption = 'Show selected data'
        OnClick = mnuShowDatesClick
      end
      object mnuShowDates1: TMenuItem
        Caption = 'Show displayed data'
        OnClick = mnuShowDates1Click
      end
      object mnuStat: TMenuItem
        Caption = #1057#1090#1072#1090#1080#1089#1090#1080#1082#1072
        Visible = False
      end
      object mnuDop: TMenuItem
        Caption = 'Altitude and number distortion'
        OnClick = mnuDopClick
      end
      object mnuOtmetkiMejduNord: TMenuItem
        Caption = 'Maximum number of P marks between North '
        OnClick = mnuOtmetkiMejduNordClick
      end
      object mnuOtmetkiMejduNordSred: TMenuItem
        Caption = 'Maximum average number of P marks between North   '
        ImageIndex = 7
        OnClick = mnuOtmetkiMejduNordSredClick
      end
      object mnuDroblVK: TMenuItem
        Caption = 'Search for split plots of the secondary channel '
        OnClick = mnuDroblVKClick
      end
      object mnuFindPObjed: TMenuItem
        Caption = 'Probability of joining the primary and secondary channels '
        OnClick = mnuFindPObjedClick
      end
      object mnuProvestiPrivyazku: TMenuItem
        Caption = 'Perform binding '
        OnClick = mnuProvestiPrivyazkuClick
      end
      object mnuUbratPrivyazku: TMenuItem
        Caption = 'Remove binding'
        OnClick = mnuUbratPrivyazkuClick
      end
    end
    object mnuFindRS: TMenuItem
      Caption = 'Resolution search'
      object mnuRS_PK: TMenuItem
        Caption = 'Primary channel'
        object mnuRS_PK_Option: TMenuItem
          Caption = 'Parameters '
          OnClick = mnuRS_PK_OptionClick
        end
        object mnuRS_PK_FindBlizkie: TMenuItem
          Caption = 'Find close plots'
          OnClick = mnuRS_PK_FindBlizkieClick
        end
        object mnuRS_PK_VklBorta: TMenuItem
          Caption = 'View aircrafts with close plots'
          OnClick = mnuRS_PK_VklBortaClick
        end
        object mnuRS_PK_ViewVydel: TMenuItem
          Caption = 'View in selected area'
          OnClick = mnuRS_PK_ViewVydelClick
        end
        object mnuRS_PK_Clear: TMenuItem
          Caption = 'Clear parameters array'
          OnClick = mnuRS_PK_ClearClick
        end
      end
      object mnuRS_VK: TMenuItem
        Caption = 'Secondary channel '
        object mnuRS_VK_Option: TMenuItem
          Caption = 'Parameters'
          OnClick = mnuRS_VK_OptionClick
        end
        object mnuRS_VK_FindBlizkie: TMenuItem
          Caption = 'View aircrafts with close plots'
          OnClick = mnuRS_VK_FindBlizkieClick
        end
        object mnuRS_VK_VklBorta: TMenuItem
          Caption = 'Find close plots'
          object mnuRS_UVD_Add_Brt: TMenuItem
            Caption = 'UVD'
            OnClick = mnuRS_UVD_Add_BrtClick
          end
          object mnuRS_RBS_Add_Brt: TMenuItem
            Caption = 'RBS'
            OnClick = mnuRS_RBS_Add_BrtClick
          end
        end
        object mnuRS_VK_ViewVydel: TMenuItem
          Caption = 'View in selected area'
          object mnuRS_VK_View_UVD: TMenuItem
            Caption = 'UVD'
            OnClick = mnuRS_VK_View_UVDClick
          end
          object mnuRS_VK_View_RBS: TMenuItem
            Caption = 'RBS'
            OnClick = mnuRS_VK_View_RBSClick
          end
        end
        object mnuRS_VK_Clear: TMenuItem
          Caption = 'Clear parameters array'
          OnClick = mnuRS_VK_ClearClick
        end
      end
    end
    object mnuOblast: TMenuItem
      Caption = 'Area'
      object mnuVnutriOblasti: TMenuItem
        Caption = 'Inside rectangular area '
        ShortCut = 16457
        OnClick = mnuVnutriOblastiClick
      end
      object mnuPredyshMashtab: TMenuItem
        Caption = 'Previous scale '
        ShortCut = 32776
        OnClick = mnuPredyshMashtabClick
      end
      object mnuNotPriam: TMenuItem
        Caption = 'Select nonrectangular area '
        ShortCut = 16463
        OnClick = mnuNotPriamClick
      end
      object mnuClearSelection: TMenuItem
        Caption = 'Clear selected area '
        Enabled = False
        ShortCut = 46
        OnClick = mnuClearSelectionClick
      end
      object mnuClearNotSelection: TMenuItem
        Caption = 'Clear all except selected area '
        Enabled = False
        ShortCut = 8238
        OnClick = mnuClearNotSelectionClick
      end
    end
    object mnuOption: TMenuItem
      Caption = 'Settings'
      OnClick = mnuOptionClick
    end
    object mnuHelp: TMenuItem
      Caption = 'Help '
      object N4: TMenuItem
        Caption = 'Main menu'
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object N2: TMenuItem
        Caption = 'Contents '
        OnClick = N2Click
      end
      object N3: TMenuItem
        Caption = 'Index of help file'
        OnClick = N3Click
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object N8: TMenuItem
        Caption = 'About Program'
        OnClick = N8Click
      end
    end
  end
end
