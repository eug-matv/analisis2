//---------------------------------------------------------------------------
#ifndef SpisokFilisH
#define SpisokFilisH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <CheckLst.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include "CSPIN.h"
#include <Dialogs.hpp>
#include "CGAUGES.h"
//---------------------------------------------------------------------------
class TFSpisokFiles : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TPanel *Panel1;
        TButton *BAddFiles;
        TButton *BDelFiles;
        TButton *BClearList;
        TMainMenu *MainMenu1;
        TMenuItem *N1;
        TMenuItem *N2;
        TMenuItem *N3;
        TMenuItem *N4;
        TMenuItem *N5;
        TMenuItem *N6;
        TMenuItem *N7;
        TMenuItem *N8;
        TMenuItem *N9;
        TMenuItem *N10;
        TMenuItem *N11;
        TCSpinButton *CSpinButton1;
        TButton *BSortFiles;
        TLabel *Label2;
        TMenuItem *N12;
        TMenuItem *N13;
        TButton *BObjedinit;
        TOpenDialog *OpenDialog1;
        TListBox *CBListOfFiles;
        TSaveDialog *SaveDialog1;
        TButton *BOpenFile;
        TButton *BCancel;
        TMenuItem *N14;
        TLabel *Label3;
        TEdit *ETempObzora;
        TButton *BFindTO;
        TMenuItem *mnuHelp;
        TCGauge *CGauge1;
        TTimer *Timer1;
        TCheckBox *CBIs180;
        TLabel *Label5;
        TGroupBox *GroupBox1;
        TLabel *Label4;
        TEdit *EAzimuth;
        TLabel *Label6;
        TEdit *EDalnost;
        TLabel *Label7;
        TLabel *Label8;
        TCheckBox *CBIsInPlot;
        TCheckBox *CBNeedToPrivyazka;
        void __fastcall BAddFilesClick(TObject *Sender);
        void __fastcall BDelFilesClick(TObject *Sender);
        void __fastcall BClearListClick(TObject *Sender);
        void __fastcall BSortFilesClick(TObject *Sender);
        void __fastcall CSpinButton1DownClick(TObject *Sender);
        void __fastcall CSpinButton1UpClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall BOpenFileClick(TObject *Sender);
        void __fastcall BCancelClick(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall BFindTOClick(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall BObjedinitClick(TObject *Sender);
        void __fastcall CBListOfFilesEnter(TObject *Sender);
        void __fastcall CBListOfFilesClick(TObject *Sender);
        void __fastcall CBListOfFilesDblClick(TObject *Sender);
        void __fastcall mnuHelpClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
        void SetGlobalState(void);
public:		// User declarations
        bool Canceled;
        AnsiString *FileNames;
    //    FILE **fps;               //������ - ������ �� ������������
        int N_of_fps;
        double TempObzoraVSekundah;
        double dfAzmtStrob;     //����� ������� ��� ��������  � ��������
        double dfDlnstStrob;    //����� ��������� ��� �������� � ��
        bool is180;
        bool isInPlot;

        bool CancelAdd;   //������ ����������
        AnsiString CHMHelpFile;
        AnsiString CHMHtml;



        __fastcall TFSpisokFiles(TComponent* Owner);

};
//---------------------------------------------------------------------------
extern PACKAGE TFSpisokFiles *FSpisokFiles;
//---------------------------------------------------------------------------
#endif
