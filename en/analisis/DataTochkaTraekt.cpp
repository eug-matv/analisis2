/*�������� �������� � DataTochkaTraekt.h*/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "GlavOkno.h"
#include "DataTochkaTraekt.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFDataTraekt *FDataTraekt;
//---------------------------------------------------------------------------
__fastcall TFDataTraekt::TFDataTraekt(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


int __fastcall TFDataTraekt::MyShow(struct DANNYE_TOCHNOST *DT)
{

  char Strka[20];
  int Ch1,Ch,Mn;
  double Sek,Mn1;

  DT1=DT;
  sprintf(Strka,"Tail number: %d",DT->NomBort);
  LBort->Caption=Strka;
  sprintf(Strka,"Altitude: %d m",DT->OprVysota);
  LVysota->Caption=Strka;
  sprintf(Strka,"X: %d m",DT->X);
  L_X->Caption=Strka;
  sprintf(Strka,"Y: %d m",DT->Y);
  L_Y->Caption=Strka;
  sprintf(Strka,"Azimuth: %.02lf �",DT->Azmt);
  LAzmt->Caption=Strka;
  sprintf(Strka,"Range: %.02lf km",DT->Dlnst);
  LDlnst->Caption=Strka;

//��������� �����
  Ch1=(long)(DT->Time);
  Ch=Ch1%24;
  Mn1=(DT->Time-Ch1)*60.0;
  Mn=(long)Mn1;
  Sek=(Mn1-Mn)*60.0;
  sprintf(Strka,"Time: %02d:%02d:%06.03lf",Ch,Mn,Sek);

//��������� ����������������� ������
  sprintf(Strka,"X: %d m",DT->XA);
  L_XA->Caption=Strka;
  sprintf(Strka,"Y: %d m",DT->YA);
  L_YA->Caption=Strka;
  sprintf(Strka,"Azimuth: %.02lf �",DT->AzmtA);
  LAzmtA->Caption=Strka;
  sprintf(Strka,"Range: %.02lf km",DT->DlnstA);
  LDlnstA->Caption=Strka;
  Top=0;Left=0;
  Show();
  return 1;
}


void __fastcall TFDataTraekt::FormCreate(TObject *Sender)
{
     //iffWorkWithIniFile(this, String("analis_lan.ini"));

}
//---------------------------------------------------------------------------


