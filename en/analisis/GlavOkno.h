//---------------------------------------------------------------------------

#ifndef GlavOknoH
#define GlavOknoH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ComCtrls.hpp>
#include "CSPIN.h"
#include <Buttons.hpp>
#include "Traekt.h"
#include <Dialogs.hpp>
#include <TeEngine.hpp>
#include "CGAUGES.h"
#include <Graphics.hpp>

//��������� ����������� ������ ���������, ������� ������ � ���������� ���� � ��������
//������� �������. ������������� ��� �������� �������� (������ ���)
struct DANNYE_TOCHNOST
{
  long NomBort;   //������������ ����
  long OprVysota; //������������ ������
  double Azmt;   //������ ������������
  double Dlnst;  //��������� ������������
  long X,Y;    //���������� � �
  double Time;   //����� ������� �������
  double AzmtA;  //�������� ������� ������������������
  double DlnstA;  //�������� ������� ������������������
  long XA,YA;    //���������� � �
  long NomerObzora;

};



//---------------------------------------------------------------------------
class TFGlavOkno : public TForm
{
__published:	// IDE-managed Components
        TPanel *MainPanel;
        TPanel *VerhPanel;
        TPanel *RisovPanel;
        TPanel *NizPanel;
        TPanel *LeftPanel;
        TMainMenu *MainMenu1;
        TMenuItem *mnuFile;
        TMenuItem *mnuDates;
        TMenuItem *mnuFindRS;
        TMenuItem *mnuOblast;
        TMenuItem *mnuOption;
        TMenuItem *mnuHelp;
        TGroupBox *GroupBox1;
        TButton *BSortSpisokBortov;
        TPanel *Panel1;
        TButton *B_OK;
        TGroupBox *GBVremya;
        TGroupBox *GroupBox2;
        TCSpinEdit *CSE_Sutki1;
        TCSpinEdit *CSE_Sutki2;
        TGroupBox *GroupBox3;
        TLabel *Label3;
        TLabel *Label4;
        TLabel *Label5;
        TGroupBox *GBMashtab;
        TRadioButton *RB_50km;
        TRadioButton *RB_100km;
        TRadioButton *RB_400km;
        TRadioButton *RB_200km;
        TStaticText *StaticText1;
        TMemo *MVolDataSel;
        TMemo *MVolVydelData;
        TMemo *MKurs;
        TMemo *MSredSkorost;
        TMemo *MVydelenoSek;
        TPanel *Panel2;
        TMemo *M_SKO_D;
        TMemo *M_SKO_A;
        TButton *BTochnost;
        TGroupBox *GroupBox4;
        TButton *BZonaP;
        TButton *BZonaS;
        TLabel *Label6;
        TButton *BStart;
        TPanel *PBPause;
        TButton *BEnd;
        TTrackBar *TBDTimeAnim;
        TPanel *RightPanel;
        TProgressBar *PB_Animate;
        TMenuItem *MListOfFiles;
        TShape *Shape1;
        TLabel *Label7;
        TShape *Shape2;
        TLabel *Label8;
        TShape *Shape3;
        TShape *Shape4;
        TShape *Shape5;
        TLabel *Label9;
        TShape *Shape6;
        TLabel *Label10;
        TImage *Image1;
        TShape *SVydel;
        TMenuItem *mnuVnutriOblasti;
        TRadioButton *RB_Uvelichenie;
        TStatusBar *StatusBar1;
        TListBox *CBSpisokBortov;
        TButton *Button1;
        TTimer *Timer1;
        TMenuItem *mnuShowDates;
        TMenuItem *mnuShowDates1;
        TMenuItem *mnuStat;
        TMenuItem *mnuDop;
        TMenuItem *mnuOtmetkiMejduNord;
        TMenuItem *mnuOtmetkiMejduNordSred;
        TMenuItem *mnuDroblVK;
        TMenuItem *mnuNotPriam;
        TMenuItem *mnuClearSelection;
        TMenuItem *mnuSaveAs;
        TMenuItem *mnuSetUserInfo;
        TMenuItem *mnuSetDocDate;
        TMenuItem *mnuSetDannyeInfo;
        TMenuItem *mnuIzvlech;
        TMenuItem *mnuIzvlechUVD;
        TMenuItem *mnuIzvlechRBS;
        TMenuItem *mnuIzvlechPS_S;
        TMenuItem *mnuIzvlechS;
        TMenuItem *mnuIzvlechP;
        TMenuItem *N7;
        TMenuItem *mnuSpiskiBort;
        TMenuItem *mnu_Load_Selection;
        TMenuItem *N9;
        TMenuItem *N10;
        TMenuItem *mnuPrint;
        TMenuItem *mnuPrintPicture;
        TMenuItem *mnuPrintData1;
        TMenuItem *mnuPrintData;
        TMenuItem *N14;
        TMenuItem *mnuExit;
        TMenuItem *mnuRS_PK;
        TMenuItem *mnuRS_VK;
        TMenuItem *mnuRS_PK_Option;
        TMenuItem *mnuRS_PK_FindBlizkie;
        TMenuItem *mnuRS_PK_ViewVydel;
        TMenuItem *mnuRS_PK_Clear;
        TMenuItem *mnuRS_VK_Option;
        TMenuItem *mnuRS_VK_VklBorta;
        TMenuItem *mnuRS_VK_FindBlizkie;
        TMenuItem *mnuRS_VK_ViewVydel;
        TMenuItem *mnuRS_VK_Clear;
        TMenuItem *mnuRS_VK_View_UVD;
        TMenuItem *mnuRS_VK_View_RBS;
        TSaveDialog *SaveDialog1;

        TMenuItem *N1;
        TCGauge *CGauge1;
        TTimer *Timer2;
        TMenuItem *mnuProvestiPrivyazku;
        TMenuItem *mnuUbratPrivyazku;
        TMenuItem *mnuIzvlechP_PS;
        TMenuItem *mnuRS_PK_VklBorta;
        TMenuItem *mnuRS_UVD_Add_Brt;
        TMenuItem *mnuRS_RBS_Add_Brt;
        TOpenDialog *OpenDialog1;
        TMenuItem *mnuViewVisualBorts;
        TTimer *TimerAnimate;
        TLabel *Label11;
        TLabel *LCurTime;
        TDateTimePicker *DTP1;
        TLabel *Label1;
        TLabel *Label2;
        TDateTimePicker *DTP2;
        TPopupMenu *PM_SpisokBortov;
        TMenuItem *PM_CopyToBuffer;
        TMenuItem *mnuClearNotSelection;
        TPanel *PanelStatus;
        TLabel *LPanelStatus;
        TMenuItem *mnuSaveVydelAs;
        TMenuItem *mnuSaveVydelList;
        TComboBox *CSE_Poryadok;
        TTimer *TimerPerezapis;
        TMenuItem *mnuPredyshMashtab;
        TMenuItem *N2;
        TMenuItem *N3;
        TMenuItem *N4;
        TMenuItem *N5;
        TMenuItem *N6;
        TMenuItem *N8;
        TEdit *EVysota1;
        TCSpinButton *CSBVysota1;
        TEdit *EVysota2;
        TCSpinButton *CSBVysota2;
        TMenuItem *mnuFindPObjed;
        TRadioButton *RadioButton1;
        TCheckBox *CBNazad;
        void __fastcall FormCanResize(TObject *Sender, int &NewWidth,
          int &NewHeight, bool &Resize);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall CSE_PoryadokKeyPress(TObject *Sender, char &Key);
        void __fastcall PBPauseMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall PBPauseMouseUp(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall MListOfFilesClick(TObject *Sender);
        void __fastcall Image1MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall Image1MouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall Image1MouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall SVydelMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall SVydelMouseMove(TObject *Sender, TShiftState Shift,
          int X, int Y);
        void __fastcall SVydelMouseUp(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
        void __fastcall mnuVnutriOblastiClick(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall RB_50kmClick(TObject *Sender);
        void __fastcall RB_200kmClick(TObject *Sender);
        void __fastcall RB_100kmClick(TObject *Sender);
        void __fastcall RB_400kmClick(TObject *Sender);
        void __fastcall CBSpisokBortovEnter(TObject *Sender);
        void __fastcall CBSpisokBortovExit(TObject *Sender);
        void __fastcall CBSpisokBortovKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall GBVremyaMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall GroupBox2MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall GroupBox3MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall VerhPanelMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall RightPanelMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall StatusBar1MouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall NizPanelMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall BSortSpisokBortovClick(TObject *Sender);
        void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall B_OKClick(TObject *Sender);
        void __fastcall mnuNotPriamClick(TObject *Sender);
        void __fastcall mnuSetUserInfoClick(TObject *Sender);
        void __fastcall mnuSetDocDateClick(TObject *Sender);
        void __fastcall mnuSetDannyeInfoClick(TObject *Sender);
        void __fastcall mnuShowDatesClick(TObject *Sender);
        void __fastcall mnuShowDates1Click(TObject *Sender);
        void __fastcall mnuOtmetkiMejduNordClick(TObject *Sender);
        void __fastcall mnuExitClick(TObject *Sender);
        void __fastcall mnuOtmetkiMejduNordSredClick(TObject *Sender);
        void __fastcall mnuClearSelectionClick(TObject *Sender);
        void __fastcall mnuSaveAsClick(TObject *Sender);
        void __fastcall mnuIzvlechUVDClick(TObject *Sender);
        void __fastcall mnuIzvlechRBSClick(TObject *Sender);
        void __fastcall mnuIzvlechPS_SClick(TObject *Sender);
        void __fastcall mnuIzvlechSClick(TObject *Sender);
        void __fastcall mnuIzvlechPClick(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall mnuPrintPictureClick(TObject *Sender);
        void __fastcall mnuOptionClick(TObject *Sender);
        void __fastcall BZonaSClick(TObject *Sender);
        void __fastcall BZonaPClick(TObject *Sender);
        void __fastcall Timer2Timer(TObject *Sender);
        void __fastcall BTochnostClick(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall RisovPanelResize(TObject *Sender);
        void __fastcall mnuProvestiPrivyazkuClick(TObject *Sender);
        void __fastcall N1Click(TObject *Sender);
        void __fastcall mnuIzvlechP_PSClick(TObject *Sender);
        void __fastcall mnuDroblVKClick(TObject *Sender);
        void __fastcall mnuRS_PK_OptionClick(TObject *Sender);
        void __fastcall mnuRS_VK_OptionClick(TObject *Sender);
        void __fastcall mnuRS_PK_FindBlizkieClick(TObject *Sender);
        void __fastcall mnuRS_VK_FindBlizkieClick(TObject *Sender);
        void __fastcall mnuRS_PK_ClearClick(TObject *Sender);
        void __fastcall mnuRS_VK_ClearClick(TObject *Sender);
        void __fastcall mnuRS_PK_VklBortaClick(TObject *Sender);
        void __fastcall mnuRS_UVD_Add_BrtClick(TObject *Sender);
        void __fastcall mnuRS_RBS_Add_BrtClick(TObject *Sender);
        void __fastcall mnuRS_PK_ViewVydelClick(TObject *Sender);
        void __fastcall mnuRS_VK_View_UVDClick(TObject *Sender);
        void __fastcall mnuRS_VK_View_RBSClick(TObject *Sender);
        void __fastcall mnuDopClick(TObject *Sender);
        void __fastcall N9Click(TObject *Sender);
        void __fastcall mnu_Load_SelectionClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall mnuViewVisualBortsClick(TObject *Sender);
        void __fastcall BStartClick(TObject *Sender);
        void __fastcall TBDTimeAnimChange(TObject *Sender);
        void __fastcall TimerAnimateTimer(TObject *Sender);
        void __fastcall BEndClick(TObject *Sender);
        void __fastcall CBNazadClick(TObject *Sender);
        void __fastcall PM_CopyToBufferClick(TObject *Sender);
        void __fastcall mnuClearNotSelectionClick(TObject *Sender);
        void __fastcall mnuSaveVydelAsClick(TObject *Sender);
        void __fastcall mnuSaveVydelListClick(TObject *Sender);
        void __fastcall CSE_PoryadokChange(TObject *Sender);
        void __fastcall TimerPerezapisTimer(TObject *Sender);
        void __fastcall mnuPredyshMashtabClick(TObject *Sender);
        void __fastcall N4Click(TObject *Sender);
        void __fastcall N2Click(TObject *Sender);
        void __fastcall N3Click(TObject *Sender);
        void __fastcall N8Click(TObject *Sender);
        void __fastcall EVysota1Exit(TObject *Sender);
        void __fastcall CSBVysota1DownClick(TObject *Sender);
        void __fastcall CSBVysota1UpClick(TObject *Sender);
        void __fastcall CSBVysota2UpClick(TObject *Sender);
        void __fastcall CSBVysota2DownClick(TObject *Sender);
        void __fastcall EVysota2Exit(TObject *Sender);
        void __fastcall mnuFindPObjedClick(TObject *Sender);
        void __fastcall EVysota1KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall EVysota2KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall DTP1KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall DTP2KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall RadioButton1Click(TObject *Sender);
        void __fastcall mnuUbratPrivyazkuClick(TObject *Sender);
private:	// User declarations
        int GlobalState;  //���������� ��������� ���� ���������
        double Xramki[4],Yramki[4];     //���������� ����� � ��




//���������
        bool NajataPBPause;
        int NachatoVydelenie;
        double LeftKm,RightKm, BottomKm, TopKm;
        OBZORY_1 Obz1;

//������ ��� ������ � ���������������� ���������
        int VidOblasti;       //0 - �������� ������������� �������
                              //1 - ������ �������� �������������� �������
                              //2 - �������� ��������������� �������

        double *XNotPriam;  //������ ��� ��������������� � ��, ������������ ���
        double *YNotPriam;  //������ ��� ��������������� � ��, ������������ ���
        int N_NotPriam;     //����� ����� ��������������� �������



//������������� ������ ��� ���������
        int Xpnp,Ypnp;


//������� ��� ������� Timer2
        int CommandTimer2;  //1 - �� ��� ����������� �������

                            //3 - ������ ��������� ��������

//����� ��������
        bool IzFindTochnost; //���� �� ����� ���
                             //��� ��������� ������� ���������,
                             //�������� ������ �� �����,
                             //����������� IzFindTochnost=false;

        double T_SKO_A;   //�������� ��� ������� � �������
        double T_SKO_D;   //�������� C�� ��������� � ������
        double KursPolyota; //���� ������ � ��������
        double SredSkorostPolyota; //������� �������� ������
        double SredA,SredD;    //������� �������� �������� � ����������
        double dTimePolyot;    //��������� ������� ������
        int StepenPolyot;      //�������
        bool IsWasP_T, IsWasS_T, IsWasPS_T; //���� �� ������� ������� ��� ������ ��������
        long NBT[20]; //������ ������
        long N_of_NBT; //������

/*������ � �������� ��� ������ ���. ��������� DToch ���� �� ����� ����������
������� ��������. ��� ��������� ������� ���������,
�������� ������ �� �����,
����������� DToch ���������;

*/
        DANNYE_TOCHNOST *DToch;
        int N_of_DToch;

/*���� ��� ����� ����� ����� ��� �����...
*/
        AnsiString FilesForDelete[20]; //
        int N_of_FilesForDelete;

//������ � �������� ������� � �������
        void __fastcall FreeNotPriamOblast(void);
        void __fastcall RisovatNotPriamOblast(void);
        void __fastcall PechatOblast(TCanvas *Canvas,   //
                                          int Wdth,int Hght, //������� ��� ������� ������
                                          int Lft,int Tp
                                          );



//��������� ������� ���������� � �� ��� ����� ��������
       void  __fastcall  SetGlobalState(void);

//���������, ���������� ����� ����������� ���������� �������
       void __fastcall VyzovPosleVydeleniaOblasti(void);




     AnsiString LastTextOut;


//��� �������� ������
     bool NazadAnim;
     long DTimeAnim; //������ ����������� ������..
     long DSektorAnim;   //�� ������� �������� ��������

//��� ���� ������
     bool ProletTrassy;



/*��� ����������� ���������� ���������*/
     double PrevLeftKm[20],PrevTopKm[20],PrevRightKm[20], PrevBottomKm[20];
     int N_of_PrevKm;  //���������� �������� ���������

public:		// User declarations
     AnsiString CHMHelpFile;
     AnsiString CHMHtml;   


//��� ����������
     AnsiString FileNamePerezapis;


        __fastcall TFGlavOkno(TComponent* Owner);

   //��������� �������� ������
       void __fastcall LoadFiles(void);


   //������� ��� ���������
       void __fastcall Nachalo1(void);

   //���������� ������
       void __fastcall Konec1(void);  //��� �������� �� �����
       void __fastcall Konec2(void);  //��� ������ ��


//������ ��������
       void __fastcall NachaloPrivyazki(void);
       void __fastcall VypolnitPrivyazku(void);
       void __fastcall UbratPrivyazku(void);
       void __fastcall KonecPrivyazki(void);
};

extern PACKAGE TFGlavOkno *FGlavOkno;

#endif
