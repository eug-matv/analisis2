//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>


#include "GlavOkno.h"
#include "GraphDOrA.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFGraphDOrA *FGraphDOrA_Daln=NULL;
TFGraphDOrA *FGraphDOrA_Azmt=NULL;
//---------------------------------------------------------------------------
__fastcall TFGraphDOrA::TFGraphDOrA(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------


void __stdcall TFGraphDOrA::MakeGraphicAzmt(void)  //��������� ������
{
  int i,j;
  int PrevObzor;
  Series1->Clear();
  for(i=0;i<N_of_DT1;i++)
  {
    if(i!=0)
    {
       for(j=PrevObzor+1;j<DT1[i].NomerObzora;j++)
       {
         Series1->AddXY(j,0,"",clBlue);
       }
    }
    Series1->AddXY(DT1[i].NomerObzora,DT1[i].Azmt,"",clTeeColor);
    PrevObzor=DT1[i].NomerObzora;
  }
  Chart1->Title->Text->Clear();
  Chart1->Title->Text->Add("Azimuth");
  Chart1->LeftAxis->Title->Caption="Azimuth, �";
  Caption="Azimuth graph";
  Chart1->LeftAxis->Maximum=360;
  Chart1->LeftAxis->Minimum=0;

}

void __stdcall TFGraphDOrA::MakeGraphicDlnst(void)  //��������� ������
{
  int i,j;
  int PrevObzor;
  Series1->Clear();
  for(i=0;i<N_of_DT1;i++)
  {
    if(i!=0)
    {
       for(j=PrevObzor+1;j<DT1[i].NomerObzora;j++)
       {
         Series1->AddXY(j,0,"",clBlue);
       }
    }
    Series1->AddXY(DT1[i].NomerObzora,DT1[i].Dlnst,"",clTeeColor);
    PrevObzor=DT1[i].NomerObzora;
  }
  Chart1->Title->Text->Clear();
  Chart1->Title->Text->Add("Range");
  Chart1->LeftAxis->Title->Caption="Range, km";
  Caption="Range graph";
  Chart1->LeftAxis->Maximum=400;
  Chart1->LeftAxis->Minimum=0;

}


void __stdcall TFGraphDOrA::ShowGraphicAzmt(struct DANNYE_TOCHNOST *DT,
                                       int N_of_DT)
{
  DT1=DT;
  N_of_DT1=N_of_DT;
  MakeGraphicAzmt();
  CHMHtml="vydel_zona_azim.htm";
  Show();
}

void __stdcall TFGraphDOrA::ShowGraphicDlnst(struct DANNYE_TOCHNOST *DT,
                                       int N_of_DT)
{

  DT1=DT;
  N_of_DT1=N_of_DT;
  MakeGraphicDlnst();
  CHMHtml="vydel_zona_rang.htm";
  Show();
}



void __fastcall TFGraphDOrA::N1Click(TObject *Sender)
{
  AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);
}
//---------------------------------------------------------------------------


void __fastcall TFGraphDOrA::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  switch(Key)
  {

    case VK_F1:
         N1Click(NULL);
    break;
  };        
}
//---------------------------------------------------------------------------

