//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>

//for Microsoft Help
#include <htmlhelp.h>

#include "Nastroiki.h"
#include "work_ini_form.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma resource "*.dfm"
TFNastroiki *FNastroiki;


//---------------------------------------------------------------------------
__fastcall TFNastroiki::TFNastroiki(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::FormShow(TObject *Sender)
{
  Modified=false;
  Canceled=true;
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::B_OKClick(TObject *Sender)
{
  int Ret;
  double Znch1,Znch2;
//������� �� ��������, � ����� ��� ����� ���������


//�������� ��������� ����������
  if(IsCheckDlnst->Checked)
  {
    Ret=sscanf(Dlnst1->Text.c_str(),"%lf",&Znch1);
    if(Ret!=1)
    {
      MessageBox(NULL,"Range's sets are not correct","Error!",MB_OK);
      Dlnst1->SetFocus();
      return;
    }
    D1=Znch1-0.001;
    Ret=sscanf(Dlnst2->Text.c_str(),"%lf",&Znch2);
    if(Ret!=1)
    {
      MessageBox(NULL,"Not all the data in-range","Error!",MB_OK);
      Dlnst2->SetFocus();
      return;
    }
    if(Znch1<-0.001||Znch2<-0.001)
    {
      MessageBox(NULL,"Range must be positive","Error!",MB_OK);
      Dlnst1->SetFocus();
      return;
    }
    if(Znch1>Znch2)
    {
      MessageBox(NULL,"The first value range must be smaller than the second range values","Error!",MB_OK);
      Dlnst1->SetFocus();
      return;
    }

    D2=Znch2+0.001;
  }

  if(IsCheckAzmt->Checked)
  {
    Ret=sscanf(Azmt11->Text.c_str(),"%lf",&Znch1);
    if(Ret!=1)
    {
      MessageBox(NULL,"The first range of the azimuth is not completely filled","Error!",MB_OK);
      Azmt11->SetFocus();
      return;
    }
    A11=Znch1-0.001;

    Ret=sscanf(Azmt12->Text.c_str(),"%lf",&Znch2);
    if(Ret!=1)
    {
      MessageBox(NULL,"The first range of the azimuth is not completely filled","Error!",MB_OK);
      Azmt12->SetFocus();
      return;
    }
    A12=Znch2+0.001;

    if(Znch1<-0.001||Znch2<-0.001||Znch1>360.001||Znch2>360.001)
    {
        MessageBox(NULL,"Values first range of azimuths must be in the range from 0 to 360","Error!",MB_OK);
        Azmt12->SetFocus();
        return;
    }




    Ret=sscanf(Azmt21->Text.c_str(),"%lf",&Znch1);
    if(Ret!=1)
    {
        Ret=sscanf(Azmt22->Text.c_str(),"%lf",&Znch2);
        if(Ret!=1)
        {
           A22=-1;
           A21=-1;
        }else{
           MessageBox(NULL,"The second range of the azimuth is not completely filled","Error!",MB_OK);
           Azmt21->SetFocus();
           return;
        }
    }else{
        Ret=sscanf(Azmt22->Text.c_str(),"%lf",&Znch2);
        if(Ret!=1)
        {
           MessageBox(NULL,"The second range of the azimuth is not completely filled","Error!",MB_OK);
           Azmt22->SetFocus();
           return;
        }else{
          if(Znch1<-0.001||Znch2<-0.001||Znch1>360.001||Znch2>360.001)
          {
             MessageBox(NULL,"Values second range of azimuths must be in the range from 0 to 360","Error!",MB_OK);
             Azmt12->SetFocus();
             return;
          }

          A21=Znch1-0.001;
          A22=Znch2+0.001;
        }
    }
  }

  Ret=sscanf(MaxMinD->Text.c_str(),"%lf",&MaxMinD1);
  if(Ret!=1)MaxMinD1=0;
  Ret=sscanf(MinMaxD->Text.c_str(),"%lf",&MinMaxD1);
  if(Ret!=1)MinMaxD1=0;

  Ret=sscanf(eMinSignalPK->Text.c_str(),"%df",&Znch1);
  if(Ret!=1||Znch1<0.0)
  {
//��� ����� ������
     MessageBox(NULL,"Value for PC signal needs to be entered and is greater than 0","Error!",MB_OK);
     eMinSignalPK->SetFocus();
     return;
  }






  Canceled=false;
  Close();
}
//---------------------------------------------------------------------------


void __fastcall TFNastroiki::BCancelClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IsCheckVysClick(TObject *Sender)
{
   if(IsCheckVys->Checked)
   {
      IskluchVys->Enabled=true;
   }else{
      IskluchVys->Enabled=false;
   }
      Modified=true;
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IsCheckDlnstClick(TObject *Sender)
{
   if(IsCheckDlnst->Checked)
   {
     Dlnst1->Enabled=true;
     Dlnst2->Enabled=true;
     Dlnst1->Color=clWindow;
     Dlnst2->Color=clWindow;
     if(Visible)
     {
       Dlnst1->SetFocus();
     }  
   }else{
     Dlnst1->Enabled=false;
     Dlnst2->Enabled=false;
     Dlnst1->Color=clBtnFace;
     Dlnst2->Color=clBtnFace;
   }
   Modified=true;
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IsCheckAzmtClick(TObject *Sender)
{
   if(IsCheckAzmt->Checked)
   {
     Azmt11->Enabled=true;
     Azmt12->Enabled=true;
     Azmt21->Enabled=true;
     Azmt22->Enabled=true;
     Azmt11->Color=clWindow;
     Azmt12->Color=clWindow;
     Azmt21->Color=clWindow;
     Azmt22->Color=clWindow;
     if(Visible)
     {
       Azmt11->SetFocus();
     }
   }else{
     Azmt11->Enabled=false;
     Azmt12->Enabled=false;
     Azmt21->Enabled=false;
     Azmt22->Enabled=false;
     Azmt11->Color=clBtnFace;
     Azmt12->Color=clBtnFace;
     Azmt21->Color=clBtnFace;
     Azmt22->Color=clBtnFace;
   }
   Modified=true;
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IsHvostClick(TObject *Sender)
{
   if(IsHvost->Checked)
   {
      DlinaHvosta->Enabled=true;
      DlinaHvosta->Color=clWindow;
   }else{
      DlinaHvosta->Enabled=false;
      DlinaHvosta->Color=clBtnFace;
   }
   
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IsVisNumberClick(TObject *Sender)
{
   if(IsVisNumber->Checked)
   {
     IsVisTime->Enabled=true;
   }else{
     IsVisTime->Enabled=false;
   }
   Modified=true;
}
//---------------------------------------------------------------------------








void __fastcall TFNastroiki::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if(Key==VK_F1)
   {
     mnuHelpClick(NULL);
   }
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::mnuHelpClick(TObject *Sender)
{
  AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }
  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);

}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::B_PoUmolchaniuClick(TObject *Sender)
{

   IsVisNumber->Checked=true;
   IsVisTime->Checked=false;
   IsCheckNumber->Checked=true;
   IsCheckTime->Checked=false;
   IsCheckVys->Checked=false;
   IskluchVys->Checked=false;
   IsCheckDlnst->Checked=false;
   IsCheckAzmt->Checked=false;
   KanalP->Checked=true;   KanalS->Checked=true;   KanalPS->Checked=true;
   VydelyatSevera->Checked=true;  DiametrMetok->Value=1;
   IsAnimNomer->Checked=true;     IsAnimVysota->Checked=true;
   IsAnimTime->Checked=false;   IsHvost->Checked=false;
   IsIstinnyRightDopData->Checked=false;IsLojnyyInfoVyvod->Checked=false;
   FindRadialTraekt->Checked=false;   RGRejim->ItemIndex=0;
   cbIsCheckSignalPK->Checked=false;
   eMinSignalPK->Text="1.0e+3";
   RBNapravlenia->ItemIndex=0;
 //�����
   SOblastCol->Brush->Color=clBlack;
   SKolca1Col->Brush->Color=(TColor)RGB(0,0xF0,0);
   SKolca2Col->Brush->Color=(TColor)RGB(0,0x80,0);
   SRad90Col->Brush->Color=(TColor)RGB(0,0xF0,0);
   SRad30Col->Brush->Color=(TColor)RGB(0,0x80,0);
   SRamkaCol->Brush->Color=clWhite;
   S_P_Col->Brush->Color=clBlue;   S_S_Col->Brush->Color=clYellow;
   S_PS_Col->Brush->Color=clLime; S_RS_UVD_Col->Brush->Color=clFuchsia;
   S_RS_RBS_Col->Brush->Color=(TColor)0x000000C0;  S_RS_P_Col->Brush->Color=clRed;
   KO_UVD->Text="10001, 55555";
   KO_RBS->Text="1, 5555";
   Modified=true;
   DopDVys->Value=150;
   CSEPrintPK->Value=1;
   CSEPrintVK->Value=226;
   CSEPrintO->Value=141;



}
//---------------------------------------------------------------------------






void __fastcall TFNastroiki::IsVisTimeClick(TObject *Sender)
{
     Modified=true;
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IsCheckNumberClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IsCheckTimeClick(TObject *Sender)
{
    Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::IskluchVysClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::Dlnst1Change(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::Dlnst2Change(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::Azmt11Change(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::Azmt12Change(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::KanalPClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::KanalSClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::KanalPSClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::RBNapravleniaClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::RGRejimClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------






void __fastcall TFNastroiki::FindRadialTraektClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::FindForP_PSClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::FindForS_PSClick(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::MaxMinDChange(TObject *Sender)
{
     Modified=true;        
}
//---------------------------------------------------------------------------

void __fastcall TFNastroiki::MinMaxDChange(TObject *Sender)
{
     Modified=true;
}
//---------------------------------------------------------------------------


void __fastcall TFNastroiki::MouseDownSetColour(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
  bool Ret;
  TShape *Shape=(TShape*)Sender;

  ColorDialog1->Color=Shape->Brush->Color;
  Ret=ColorDialog1->Execute();
  if(Ret)
  {
    Shape->Brush->Color=ColorDialog1->Color;
    Modified=true;
  }
}




//---------------------------------------------------------------------------






bool __fastcall TFNastroiki::FormHelp(WORD Command, int Data,
      bool &CallHelp)
{
     mnuHelpClick(NULL);
    return true;    
}
//---------------------------------------------------------------------------











void __fastcall TFNastroiki::FormCreate(TObject *Sender)
{
   //iffWorkWithIniFile(this, String("analis_lan.ini"));        
}
//---------------------------------------------------------------------------





