//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>


#include "FUser_Info.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFUserInfo *FUserInfo;
//---------------------------------------------------------------------------
__fastcall TFUserInfo::TFUserInfo(TComponent* Owner)
        : TForm(Owner)
{
        CHMHtml="userinfo.htm";
        
}
//---------------------------------------------------------------------------

void __fastcall TFUserInfo::Button1Click(TObject *Sender)
{
  Tag=1;
  Close();
}
//---------------------------------------------------------------------------
void __fastcall TFUserInfo::Button2Click(TObject *Sender)
{
   Tag=0;
   Close();
}
//---------------------------------------------------------------------------
void __fastcall TFUserInfo::FormCreate(TObject *Sender)
{
        //iffWorkWithIniFile(this, String("analis_lan.ini"));        
}
//---------------------------------------------------------------------------


void __fastcall TFUserInfo::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
        if(Key==VK_F1)
        {
                  AnsiString HhpStr;
                  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
                  HtmlHelp(GetDesktopWindow(),
                        HhpStr.c_str(),
                        HH_DISPLAY_TOPIC,NULL);

        }        
}
//---------------------------------------------------------------------------

