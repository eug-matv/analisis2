//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <alloc.h>

#include <stdio.h>

//for Microsoft Help
#include <htmlhelp.h>


#include "tools.h"

#include "RabsBort.h"


#include "SpisokFilis.h"
#include "work_ini_form.h"


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "CGAUGES"
#pragma resource "*.dfm"


TFSpisokFiles *FSpisokFiles;
//---------------------------------------------------------------------------
__fastcall TFSpisokFiles::TFSpisokFiles(TComponent* Owner)
        : TForm(Owner)
{
  N_of_fps=0;
//  fps=NULL;
  TempObzoraVSekundah=4.58;  //���� ������ � ��������
  CHMHtml="fileslist.htm";

}
//---------------------------------------------------------------------------





void __fastcall TFSpisokFiles::BAddFilesClick(TObject *Sender)
{
  bool RetB;
  int i,j;
  char Strka[2001];
  double tempTOs[1000],ntto;
  int isRaznyeTOs=0;
  double problemTO1, problemTO2;
  extern AnsiString CurPath;    //������� ����
  AnsiString  MinStrka,MinStrka1;
  FILE *fp;
  char strka[100];

  SetCurrentDirectory(CurPath.c_str());
  OpenDialog1->InitialDir=CurPath;
  RetB=OpenDialog1->Execute();
  if(RetB)
  {
       MinStrka=OpenDialog1->Files->Strings[0];
       for(j=1;j<OpenDialog1->Files->Count;j++)
       {
          if(MinStrka>OpenDialog1->Files->Strings[j])
          {
            MinStrka=OpenDialog1->Files->Strings[j];
          }
       }


       CBListOfFiles->Items->Add(MinStrka);


       MinStrka1=MinStrka;
       for(i=1;i<OpenDialog1->Files->Count;i++)
       {
         MinStrka="";
         for(j=0;j<OpenDialog1->Files->Count;j++)
         {
           if(MinStrka==""&&MinStrka1<OpenDialog1->Files->Strings[j])
           {
             MinStrka=OpenDialog1->Files->Strings[j];
             continue;
           }
           if(MinStrka>OpenDialog1->Files->Strings[j]&&
              MinStrka1<OpenDialog1->Files->Strings[j])
           {
             MinStrka=OpenDialog1->Files->Strings[j];
           }
         }
         CBListOfFiles->Items->Add(MinStrka);
         MinStrka1=MinStrka;
       }
       CancelAdd=false;
  }else{
          CancelAdd=true;
  }
  GetCurrentDirectory(2000,Strka);
  CurPath=Strka;
  SetGlobalState();
}
//---------------------------------------------------------------------------


void __fastcall TFSpisokFiles::BDelFilesClick(TObject *Sender)
{
  int i;
//������� �����
  for(i=CBListOfFiles->Items->Count-1;i>=0;i--)
  {
    if(CBListOfFiles->Selected[i])
    {
       CBListOfFiles->Items->Delete(i);
    }
  }
    SetGlobalState();
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::BClearListClick(TObject *Sender)
{
  CBListOfFiles->Clear();
   SetGlobalState();
        
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::BSortFilesClick(TObject *Sender)
{
  int i,j;
  AnsiString Strka;
  for(i=0;i<CBListOfFiles->Items->Count;i++)
  {

    for(j=CBListOfFiles->Items->Count-1;j>=i+1;j--)
    {
      if(CBListOfFiles->Items->Strings[j]<CBListOfFiles->Items->Strings[j-1])
      {
         Strka=CBListOfFiles->Items->Strings[j];
         CBListOfFiles->Items->Strings[j]=CBListOfFiles->Items->Strings[j-1];
         CBListOfFiles->Items->Strings[j-1]=Strka;
      }
    }
  }
  SetGlobalState();
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::CSpinButton1DownClick(TObject *Sender)
{
  int i;
  AnsiString Strka;
  if(CBListOfFiles->Items->Count==0||
  CBListOfFiles->Selected[CBListOfFiles->Items->Count-1])return;
  for(i=CBListOfFiles->Items->Count-2;i>=0;i--)
  {
    if(CBListOfFiles->Selected[i])
    {
      Strka=CBListOfFiles->Items->Strings[i+1];
      CBListOfFiles->Items->Strings[i+1]=CBListOfFiles->Items->Strings[i];
      CBListOfFiles->Items->Strings[i]=Strka;
      CBListOfFiles->Selected[i]=false;
      CBListOfFiles->Selected[i+1]=true;
    }
  }
    SetGlobalState();

}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::CSpinButton1UpClick(TObject *Sender)
{
  int i;
  AnsiString Strka;
  if(CBListOfFiles->Items->Count==0||
  CBListOfFiles->Selected[0])return;
  for(i=1;i<CBListOfFiles->Items->Count;i++)
  {
    if(CBListOfFiles->Selected[i])
    {
      Strka=CBListOfFiles->Items->Strings[i-1];
      CBListOfFiles->Items->Strings[i-1]=CBListOfFiles->Items->Strings[i];
      CBListOfFiles->Items->Strings[i]=Strka;
      CBListOfFiles->Selected[i]=false;
      CBListOfFiles->Selected[i-1]=true;
    }
  }
    SetGlobalState();
}
//---------------------------------------------------------------------------







void __fastcall TFSpisokFiles::FormShow(TObject *Sender)
{
  int i;
  char Stroka[10];
  Canceled=true;
  CBListOfFiles->Items->Clear();
  for(i=0;i<N_of_fps;i++)
  {
    CBListOfFiles->Items->Add(FileNames[i]);
  }

  sprintf(Stroka,"%.02lf",TempObzoraVSekundah);
  ETempObzora->Text=Stroka;

  sprintf(Stroka,"%.02lf", dfAzmtStrob);
  EAzimuth->Text=String(Stroka);

  sprintf(Stroka,"%.02lf", dfDlnstStrob);
  EDalnost->Text=String(Stroka);

 // CBIs180->Checked=is180;


  SetGlobalState();


}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::BOpenFileClick(TObject *Sender)
{
  int i;
  int N_of_fps1=0;
  int Ret;
  double Znach;
  AnsiString Strka;
  Ret=sscanf(ETempObzora->Text.c_str(),"%lf",&Znach);
  if(Ret!=1||Znach<1.0||Znach>20)
  {
    MessageBox(NULL,"Scan rate is entered incorrectly!", "Error!",MB_OK);
    return;
  }
  FILE *MyFOpenR(AnsiString FileName);



//�������� ������ ������
  for(i=0;i<CBListOfFiles->Items->Count;i++)
  {
    Ret=GetFormatFile(CBListOfFiles->Items->Strings[i].c_str());
    if(Ret==0)
    {
      Strka=String("Cannot open a file: ")+CBListOfFiles->Items->Strings[i];
      MessageBox(Handle,Strka.c_str(),"Error!",MB_OK);
      return;
    }

    if(Ret==-1)
    {
      Strka=String("File ")+CBListOfFiles->Items->Strings[i]+
      String("\n does not have correct data");
      MessageBox(Handle,Strka.c_str(),"Error!",MB_OK);
      return;
    }
  }


  FILE *fps1;

  AnsiString *Strngs=new AnsiString[CBListOfFiles->Items->Count];
  for(i=0;i<N_of_fps;i++)
  {
//    fclose(fps[i]);
  }

  for(i=0;i<CBListOfFiles->Items->Count;i++)
  {
    fps1=MyFOpenR(CBListOfFiles->Items->Strings[i]);
    if(fps1)
    {
      Strngs[N_of_fps1]=CBListOfFiles->Items->Strings[i];
      N_of_fps1++;
      fclose(fps1);
    }
  }
  if(N_of_fps1==0)
  {
    MessageBox(NULL,"Files for opening are not selected!", "Error!",MB_OK);
    delete []Strngs;
    if(N_of_fps)
    {
      for(i=0;i<N_of_fps;i++)
      {
//        fps[i]=MyFOpenR(FileNames[i]);
      }
    }
    return;
  }

//����� �������

  N_of_fps=N_of_fps1;
//  fps=(FILE**)realloc(fps,N_of_fps*sizeof(FILE*));
  if(FileNames)delete []FileNames;
  FileNames=new AnsiString[N_of_fps];
  for(i=0;i<N_of_fps;i++)
  {
  //  fps[i]=fps1[i];
    FileNames[i]=Strngs[i];
  }

//��������� ���� ������
  TempObzoraVSekundah=Znach;


  Ret=sscanf(EAzimuth->Text.c_str(),"%lf", &Znach);
  if(Ret!=1&&Znach>10&&Znach<0.1)
  {
        dfAzmtStrob=3.5;
  }else{
        dfAzmtStrob=Znach;;
  }

  Ret=sscanf(EDalnost->Text.c_str(),"%lf", &Znach);
  if(Ret!=1&&Znach>2&&Znach<0.1)
  {
        dfDlnstStrob=0.4;
  }else{
        dfDlnstStrob=Znach;
  }

 // is180=CBIs180->Checked;
    is180=false;         //�������� ������

 // free(fps1);
  delete []Strngs;
  Canceled=false;
//


  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::BCancelClick(TObject *Sender)
{
  Close();
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::FormDestroy(TObject *Sender)
{
  int i;
  if(N_of_fps)
  {
    for(i=0;i<N_of_fps;i++)
    {
//      fclose(fps[i]);
    }
//    free(fps);
    delete []FileNames;
  }
}
//---------------------------------------------------------------------------



void __fastcall TFSpisokFiles::BFindTOClick(TObject *Sender)
{
  int i;
  double TO=-1;
  char Strka[10];
  FILE *MyFOpenR(AnsiString FileName);
  FILE *fp1=NULL;
 //������� ������� �����
  for(i=0;i<N_of_fps;i++)
  {
//    fclose(fps[i]);
  }

//����� ��������� ����� �� ������ ���� ������ ���� �� ���������
  for(i=0;i<CBListOfFiles->Items->Count;i++)
  {
    fp1=MyFOpenR(CBListOfFiles->Items->Strings[i]);
    if(fp1)break;
  }
  if(fp1==NULL)
  {
    MessageBox(NULL, "Cannot open any file","Error!",MB_OK);
  }else{
    TO=FindTempObzoraPoSeveram(fp1);
    fclose(fp1);

    if(TO<0)
    {
      MessageBox(NULL, "Cannot detect scan rate!" ,"Error!",MB_OK);
    }else{
      TO=TO*100;
      TO=Okrugl(TO);
      TO=TO/100;
      sprintf(Strka,"%.02lf",TO);
      ETempObzora->Text=Strka;
    }
  }

  for(i=0;i<N_of_fps;i++)
  {
//    fps[i]=MyFOpenR(FileNames[i]);
  }

}
//---------------------------------------------------------------------------


void __fastcall TFSpisokFiles::Timer1Timer(TObject *Sender)
{
  Timer1->Enabled=false;  //��������� ������� ������ �������
FILE *MyFOpenR(AnsiString FileName);
  int i;
  bool RetB;
  FILE *fpIn;   //��� ������� ������
  FILE *fpOut;  //��� ��������� �����
  long TekSmesh=0,TekSize;
  char *RetC;
  char PromStrka[1000];
  double ProcentNaFile,TekProc=0;
  HDC DC;
  //���������� �����
  CGauge1->Progress=0;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {
    for(i=0;i<N_of_fps;i++)
    {
//       fclose(fps[i]);
    }

//������� � ��������� ������
    fpOut=fopen(SaveDialog1->FileName.c_str(),"wt");
    if(!fpOut)return;
    ProcentNaFile=100.0/CBListOfFiles->Items->Count;
    for(i=0;i<CBListOfFiles->Items->Count;i++)
    {
//      fpIn=fopen(CBListOfFiles->Items->Strings[i].c_str(),"rb");

      fpIn=MyFOpenR(CBListOfFiles->Items->Strings[i]);
      fseek(fpIn,0,SEEK_END);
      TekSize=ftell(fpIn);
      fseek(fpIn,0,SEEK_SET);
      TekSmesh=0;
      while(!feof(fpIn))
      {
        DC=GetDC(Panel1->Handle);
//        Size=fread(Buffer,1,30000,fpIn);
        RetC=fgets(PromStrka,999,fpIn);
        TekSmesh=ftell(fpIn);
        if(RetC)
        {
          fputs(PromStrka,fpOut);
        }

        if(TekSmesh%1000==0)
        {
          TekProc=ProcentNaFile*i+TekSmesh/(1.0+TekSize)*ProcentNaFile;
          CGauge1->Progress=(long)TekProc;
        }
     //������� ������
        ReleaseDC(Handle,DC);
      }
      fclose(fpIn);
    }
    fclose(fpOut);
    for(i=0;i<N_of_fps;i++)
    {
//      fps[i]=MyFOpenR(FileNames[i]);
    }
  }

  CGauge1->Visible=false;

}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::BObjedinitClick(TObject *Sender)
{

  CGauge1->Progress=0;
  CGauge1->Visible=true;
  Timer1->Enabled=true;
}
//---------------------------------------------------------------------------







//�������
void TFSpisokFiles::SetGlobalState(void)
{
  if(CBListOfFiles->Items->Count>0)
  {
    if(CBListOfFiles->Items->Count==1)
    {
      BObjedinit->Enabled=false;  N12->Enabled=false;
      BSortFiles->Enabled=false;  N11->Enabled=false;
    }else{
      BObjedinit->Enabled=true;   N12->Enabled=true;
      BSortFiles->Enabled=true;   N11->Enabled=true;
    }
    if(CBListOfFiles->SelCount<=0)
    {
      BDelFiles->Enabled=false;  N6->Enabled=false;
      CSpinButton1->Enabled=false; N9->Enabled=false;N10->Enabled=false;
    }else{
      BDelFiles->Enabled=true;   N6->Enabled=true;
      CSpinButton1->Enabled=true; N9->Enabled=true;N10->Enabled=true;
    }
    BOpenFile->Enabled=true; N14->Enabled=true;
    BClearList->Enabled=true; N7->Enabled=true;
    BFindTO->Enabled=true;
  }else{
      BDelFiles->Enabled=false;  N6->Enabled=false;
      BObjedinit->Enabled=false;  N12->Enabled=false;
      CSpinButton1->Enabled=false; N9->Enabled=false;N10->Enabled=false;
      BOpenFile->Enabled=false; N14->Enabled=false;
      BClearList->Enabled=false;  N7->Enabled=false;
      BSortFiles->Enabled=false;  N11->Enabled=false;
      BFindTO->Enabled=false;
  }

}
void __fastcall TFSpisokFiles::CBListOfFilesEnter(TObject *Sender)
{
  SetGlobalState();        
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::CBListOfFilesClick(TObject *Sender)
{
    SetGlobalState();        
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::CBListOfFilesDblClick(TObject *Sender)
{
    SetGlobalState();        
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokFiles::mnuHelpClick(TObject *Sender)
{
  AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");;
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }
  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);


}
//---------------------------------------------------------------------------









void __fastcall TFSpisokFiles::FormCreate(TObject *Sender)
{
     //iffWorkWithIniFile(this, String("analis_lan.ini"));        
}
//---------------------------------------------------------------------------





void __fastcall TFSpisokFiles::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if(Key==VK_F1)
    {
         mnuHelpClick(NULL);
    }
}
//---------------------------------------------------------------------------

