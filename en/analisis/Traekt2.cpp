//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Traekt2.h"
#include "OcheredO.h"
#include <alloc.h>
#include <mem.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "RabsBort.h"
#include "Traekt.h"
#include "tools.h"
#include "NotPriamZone.h"


#define ABS(X) (((X) > 0) ? (X) : (-(X)))

//---------------------------------------------------------------------------
#pragma package(smart_init)
extern AnsiString PutKProgramme;
extern AnsiString PutKTemp;
static AnsiString TempFileForIskaj,BackupFileNameIskaj;  //��������� ���� ��� ������ ���������


/*� ������ ������ ����������� ��������� OBZORY_1, ��������� � ���������� ������*/

//��������� ������������� � ������ Traekt2.cpp
//��������� � ���� ������
int OBZORY_1::SaveAs(AnsiString FileNameOut)
{
   long TekSvr=0;  //������� ������ ������
   long RetIndxSvr; //������������ ������ ������
   long NomSektSvr=0;

   long i;
   long Indx,Nomer,Indx1;
   long TekVis;
   if(FirstVisual<0)return 0;  //��� ��������� ������
   long NomObz=0;
   FILE *fpVrem;

   long TekChas,PrevChas;
   extern TCGauge *GlobalCGauge1;
   GlobalCGauge1->MinValue=-2000000000;
   GlobalCGauge1->MaxValue=2000000000;


   TekChas=PrevChas=(long)(CLSutkiFirst*24+CLChasyFirst);
   GlobalCGauge1->MinValue=TekChas;
   GlobalCGauge1->MaxValue=(long)(CLSutkiCur*24+CLChasyCur);
   GlobalCGauge1->Progress=TekChas;

   GlobalCGauge1->Visible=true;

   fpVrem=fopen(FileNameOut.c_str(),"w");

   TekVis=GetFirstFilterOB(false);
    while(TekVis>=0)
    {
        Indx=TekVis/362;
        Nomer=TekVis%362;
        if(Nomer==360)
        {
            if(VydelyatSevera)
            {
              fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
            }
        }else{ //����� �� ����� 360
      //������� ��� ������
           fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
        }
        TekVis=GetNextFilterOB(TekVis,false);
    }


   fclose(fpVrem);

   GlobalCGauge1->Visible=false;
   return 1;
}


//��������� ������������� � ������ Traekt2.cpp
//��������� � ���� ���������� ������
int OBZORY_1::SaveVydelAs(AnsiString FileNameOut)
{
   long TekSvr=0;  //������� ������ ������
   long RetIndxSvr; //������������ ������ ������
   long NomSektSvr=0;


   long Indx,Nomer,Indx1;
   long TekVis;
   if(FirstVisual<0)return 0;  //��� ��������� ������
   long NomObz=0;
   FILE *fpVrem;

   long TekChas,PrevChas;
   extern TCGauge *GlobalCGauge1;
   GlobalCGauge1->MinValue=-2000000000;
   GlobalCGauge1->MaxValue=2000000000;


   TekChas=PrevChas=(long)(CLSutkiFirst*24+CLChasyFirst);
   GlobalCGauge1->MinValue=TekChas;
   GlobalCGauge1->MaxValue=(long)(CLSutkiCur*24+CLChasyCur);
   GlobalCGauge1->Progress=TekChas;

   GlobalCGauge1->Visible=true;

   fpVrem=fopen(FileNameOut.c_str(),"w");
   TekVis=GetFirstFilterOB(true);
    while(TekVis>=0)
    {
        Indx=TekVis/362;
        Nomer=TekVis%362;
        if(Nomer==360)
        {
            if(VydelyatSevera)
            {
              fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
            }
        }else{ //����� �� ����� 360
      //������� ��� ������
           fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
        }
        TekVis=GetNextFilterOB(TekVis,true);
    }


   fclose(fpVrem);
   GlobalCGauge1->Visible=false;
   return 1;
}




//���������� �� ����� ������
int OBZORY_1::IzvlechDataVFile(AnsiString FileNameOut,
                       int Chto  //1 - ���, 2 - RBS, 3 - PS � S, 4 - S, 5 - P
                                 //6 - P � PS
                       )
{
   int i,k,TekSektor;
   int Ret;

   long PrevSeverSmesh=-1,PrevDataSmesh=-1;
   long TekChas,PrevChas;
   long Nom, Indx, prev_i, next_i;
   extern TCGauge *GlobalCGauge1;
   GlobalCGauge1->MinValue=-2000000000;
   GlobalCGauge1->MaxValue=2000000000;
   TekChas=PrevChas=(long)(CLSutkiFirst*24+CLChasyFirst);
   GlobalCGauge1->MinValue=TekChas;
   GlobalCGauge1->MaxValue=(long)(CLSutkiCur*24+CLChasyCur);
   GlobalCGauge1->Progress=TekChas;

   GlobalCGauge1->Visible=true;



   bool *OldIsNumber;  //������ �������� sort_otis
   bool OldKanalP,OldKanalS,OldKanalPS,OldIsCheckNumber;
   if(Chto<1||Chto>6)return 0;
   FILE *fpVrem;
   fpVrem=fopen(FileNameOut.c_str(),"w");
   if(fpVrem==NULL)
   {
     return (-1);
   }

//�������� ������
   OldIsNumber=(bool*)malloc(n_of_sort_otis*sizeof(bool));
   for(i=0;i<n_of_sort_otis;i++)
   {
       OldIsNumber[i]=sort_otis[i]->isSelected;
   }
   OldKanalP=KanalP; OldKanalS=KanalS; OldKanalPS=KanalPS;
   OldIsCheckNumber=IsCheckNumber;

//����� ����� ��� - ��������� ��������
   switch(Chto)
   {
      case 1:   //���
        KanalP=false; KanalS=true;KanalPS=true;IsCheckNumber=true;
        for(i=0;i<n_of_sort_otis;i++)
        {
           if((sort_otis[i]->ulNumber&0xFF000000)==0x01000000)
           {
             sort_otis[i]->isSelected=1;
           }else{
             sort_otis[i]->isSelected=0;
           }
        }
      break;
      case 2:  //���
        KanalP=false; KanalS=true;KanalPS=true;IsCheckNumber=true;
        for(i=0;i<n_of_sort_otis;i++)
        {
           if((sort_otis[i]->ulNumber&0xFF000000)==0x02000000)
           {
             sort_otis[i]->isSelected=1;
           }else{
             sort_otis[i]->isSelected=0;
           }
        }
      break;

      case 3:  //PS � S
        KanalP=false; KanalS=true;KanalPS=true;IsCheckNumber=false;

        break;

      case 4: //S
        KanalP=false; KanalS=true;KanalPS=false;IsCheckNumber=false;

        break;

      case 5: //P
        KanalP=true; KanalS=false;KanalPS=false;IsCheckNumber=false;

        break;

      case 6: //P
        KanalP=true; KanalS=false;KanalPS=true;IsCheckNumber=false;

        break;

   };



//� ��� ��
   PrevSeverSmesh=-1;
   PrevDataSmesh=-1;
   TekSektor=0;


   i=FirstAbs;

   while(i>=0)
   {
        Nom=i%362;
        Indx=i/362;
        if(Nom==360)
        {
            if(o_b_1[Nom][Indx].AbsPrev>=0)
            {
                prev_i=o_b_1[Nom][Indx].AbsPrev;
                if(prev_i%362==360)
                {
                        next_i=o_b_1[Nom][Indx].AbsNext;
                        if(next_i>=0&&(next_i%362)==360)
                        {
                             i=next_i;
                             continue;
                        }
                }
            }
        }
        Ret=ProveritNastroikiOdinOtschet(Nom,Indx);
        if(Ret==1)
        {
                fputs(GetStrokaFrom(Nom,Indx).c_str(),fpVrem);
        }
        i=o_b_1[Nom][Indx].AbsNext;
   }
//���������� ���
   KanalP=OldKanalP;KanalS=OldKanalS;KanalPS=OldKanalPS;
   IsCheckNumber=OldIsCheckNumber;
   for(i=0;i<n_of_sort_otis;i++)
   {
        sort_otis[i]->isSelected=OldIsNumber[i];
   }

   free(OldIsNumber);
   fclose(fpVrem);
   GlobalCGauge1->Visible=false;

   return 1;
}


int OBZORY_1::VyvodDataNaPrinter(   TCanvas *Canvas,       //
      int Width,int Height,  //������� ��������
      int Xo1, int Yo1,         //������ �������� ������
      long LeftKm, long BottomKm,    //���������� ������� ������ ���� ������ ���
      long RightKm, long TopKm) //���������� ������� �������� ����
{
    int i;
  long TekIndx;          //������� �������

  long *PervNomBortIndx;   //������ ������� ��������� ����� �� ������ -
                      //��� ������ ������� ������ � �������
  long *IndxPervNomBortIndx;     //������ ������� ��������� ����� �� ������ -
  long N_of_IndxPervNomBortIndx=0; //��� ����������� ������� ���������

  long LeftM=LeftKm*1000,
       BottomM=BottomKm*1000,
       RightM=RightKm*1000,
       TopM=TopKm*1000;



  long Nomer,Index;  //����� � ������ �������� �������

  long XVis,YVis;
  double KoefX,KoefY;

  long IndxBrt;  //������ �����
  unsigned long NomBort; //����� �����
  double Xf,Yf;
  AnsiString TextNomBort;
  int Dlina;
  int Ch,Ch1,Mn,Sk;  //
  char TimeOut[10];
  int DiametrMetok1;
  int WidthLine;
  DiametrMetok1=Width*DiametrMetok/768;
  WidthLine=Width/768;

  if(ChisloOtschetov==0)return 0;


  PervNomBortIndx=(long*)malloc(n_of_otis*sizeof(long));

//������� ������� -1 ��� �������� ������� PervNomBortIndx
  for(i=0;i<n_of_otis;i++)
  {
    PervNomBortIndx[i]=-1;
  }

  KoefX=(Width-1)/((double)(RightKm-LeftKm));
  KoefY=(Height-1)/((double)(BottomKm-TopKm));


  TekIndx=GetFirstFilterOB(false);

  Canvas->Pen->Mode=pmCopy;
  Canvas->Pen->Style=psSolid;
  Canvas->Brush->Color=clWhite;
  Canvas->Brush->Style=bsSolid;
  while(TekIndx>=0)
  {
    Index=TekIndx/362;
    Nomer=TekIndx%362;

//��������, ����� �� ������� ��� �����
    if(o_b_1[Nomer][Index].X<RightM&&o_b_1[Nomer][Index].X>LeftM&&
       o_b_1[Nomer][Index].Y<TopM&&o_b_1[Nomer][Index].Y>BottomM)
    {
//������� �����
      Xf=o_b_1[Nomer][Index].X/1000.0;
      Yf=o_b_1[Nomer][Index].Y/1000.0;
      XVis=Okrugl((Xf-LeftKm)*KoefX);
      YVis=Okrugl((Yf-TopKm)*KoefY);

//�������� �������� �� ���� ������ ������� � ����������� ������ ��������

//��������� ����
      if(o_b_1[Nomer][Index].Status&KANAL_P_STATUS)
      {
        if(o_b_1[Nomer][Index].Status&KANAL_S_STATUS)
        {
//������������ ����� �������� ��� �������� ����
            Canvas->Pen->Color=(TColor)RGB(cPechatObjed,cPechatObjed,cPechatObjed);
            Canvas->Brush->Color=(TColor)RGB(cPechatObjed,cPechatObjed,cPechatObjed);
            Canvas->Brush->Style=bsSolid;

        }else{
 //���������� ��� ����� ����
            Canvas->Pen->Color=(TColor)RGB(cPechatPK,cPechatPK,cPechatPK);
            Canvas->Brush->Color=(TColor)RGB(cPechatPK,cPechatPK,cPechatPK);
            Canvas->Brush->Style=bsSolid;

        }
      }else{
        if(o_b_1[Nomer][Index].Status&KANAL_S_STATUS)
        {
 //���������� ������� ������
            Canvas->Pen->Color=(TColor)RGB(cPechatVK,cPechatVK,cPechatVK);
            Canvas->Brush->Color=(TColor)RGB(cPechatVK,cPechatVK,cPechatVK);
            Canvas->Brush->Style=bsSolid;

        }else{
           TekIndx=GetNextFilterOB(TekIndx,false);
           continue;
        }
      }


      if(DiametrMetok1==1)
      {
         Canvas->Pixels[XVis+Xo1][YVis+Yo1]=
         Canvas->Pen->Color;
      }else if(DiametrMetok1==2)
      {
          Canvas->Pixels[XVis+Xo1][YVis+Yo1]=Canvas->Pen->Color;
          Canvas->Pixels[XVis+Xo1][YVis+Yo1+1]=Canvas->Pen->Color;
          Canvas->Pixels[XVis+Xo1+1][YVis+Yo1]=Canvas->Pen->Color;
          Canvas->Pixels[XVis+Xo1+1][YVis+Yo1+1]=Canvas->Pen->Color;

      }else{
            if((o_b_1[Nomer][Index].Status&3)==2)Canvas->Pen->Width=WidthLine;
              Canvas->Ellipse(XVis-DiametrMetok1/2+Xo1,YVis-DiametrMetok1/2+Yo1,
                   XVis-DiametrMetok1/2+DiametrMetok1+Xo1,
                   YVis-DiametrMetok1/2+DiametrMetok1+Yo1);
            Canvas->Pen->Width=1;

      }


      if(IsVisNumber)
      {
//���������� � �������� ������
         IndxBrt=o_b_1[Nomer][Index].IndxBrt;
         if(PervNomBortIndx[IndxBrt]==-1)
         {
//����� ������� ���� �����
           PervNomBortIndx[IndxBrt]=Index*362+Nomer;
         }
      }
    }
    TekIndx=GetNextFilterOB(TekIndx,false);
  }



//������ �� ������� �������
  if(IsVisNumber)  //������������ �� ����� �����
  {
    Canvas->Brush->Style=bsClear;
    Canvas->Font->Color=clBlack;
    Canvas->Font->Name="MS Sans Serif";
    Canvas->Font->Size=7;

    for(i=0;i<n_of_otis;i++)
    {
      if(PervNomBortIndx[i]<0)continue;
      Nomer=PervNomBortIndx[i]%362;
      Index=PervNomBortIndx[i]/362;
      Xf=Okrugl(o_b_1[Nomer][Index].X/1000.0);
      Yf=Okrugl(o_b_1[Nomer][Index].Y/1000.0);
      XVis=Okrugl((Xf-LeftKm)*KoefX);
      YVis=Okrugl((Yf-TopKm)*KoefY);
//���������� ������ � �������
      NomBort=otis[i].ulNumber&0x00FFFFFF;
      if(otis[i].ulNumber&0xFF000000==0x02000000)
      {
          if(NomBort==0)
          {
            TextNomBort=String((int)NomBort)+String("(r)");
          }else{
            TextNomBort=String((int)NomBort);
          }
      }else
      if(otis[i].ulNumber&&0xFF000000==0x01000000)
      {
          if(NomBort<10000)
          {
            TextNomBort=String((int)NomBort)+String("(u)");
          }else{
            TextNomBort=String((int)NomBort);
          }
      }else
      if(otis[i].ulNumber&&0xFF000000==0)
      {
          TextNomBort=String("PC �")+String((int)otis[i].trassa_indx);
      }else
      if(otis[i].ulNumber&&0xFF000000==0x12000000)
      {
        TextNomBort=String((int)NomBort)+String("(RM_RBS)");
      }else
      if(otis[i].ulNumber&&0xFF000000==0x11000000)
      {
        TextNomBort=String((int)NomBort)+String("(RM_UVD)");
      }
      Dlina=Canvas->TextWidth(TextNomBort);
      if(XVis+Dlina>Width)
      {
        XVis=Width-Dlina;
      }
 //������� ������
      if(otis[i].ulNumber<0x14000000)
      {
        Canvas->TextOut(XVis+Xo1,YVis+Yo1,TextNomBort);

 //� ������ ������� �����, ���� ����������
        if(IsVisTime)
        {
                Ch1=(int)(o_b_1[Nomer][Index].Time);
                Ch=Ch1%24;
                Mn=(int)((o_b_1[Nomer][Index].Time-Ch1)*60);
                Sk=(int)((60*(o_b_1[Nomer][Index].Time-Ch1)-Mn)*60);
                sprintf(TimeOut,"%02d:%02d:%02d",Ch,Mn,Sk);
                Canvas->TextOut(XVis+Xo1,YVis+10+Yo1,TimeOut);
        }
      }
    }
  }







  if(PervNomBortIndx)free(PervNomBortIndx);
  if(IndxPervNomBortIndx)free(IndxPervNomBortIndx);
  return 1;
}







/*����� ���������� ��������� ������-�������� ���������� ������*/

int OBZORY_1::ShowDatas(TStrings *Strings)
{
   long Indx,Nomer,Indx1;
   long TekVis;
   long TekSvr=0;  //������� ������ ������
   long RetIndxSvr; //������������ ������ ������
   long NomSektSvr=0;

   if(FirstVisual<0)return 0;  //��� ��������� ������

   Strings->Clear();
   long NomObz=0;
   FILE *fpVrem;

   AnsiString TempFile=GetTempFileInTempPath();
   long TekChas,PrevChas;
   extern TCGauge *GlobalCGauge1;
   GlobalCGauge1->MinValue=-2000000000;
   GlobalCGauge1->MaxValue=2000000000;
   TekChas=PrevChas=(long)(CLSutkiFirst*24+CLChasyFirst);
   GlobalCGauge1->Progress=TekChas;
   GlobalCGauge1->MinValue=TekChas;
   GlobalCGauge1->MaxValue=(long)(CLSutkiCur*24+CLChasyCur);

   GlobalCGauge1->Visible=true;



   fpVrem=fopen(TempFile.c_str(),"w");

    TekVis=GetFirstFilterOB(true);
    while(TekVis>=0)
    {
        Indx=TekVis/362;
        Nomer=TekVis%362;
        if(Nomer==360)
        {
            if(VydelyatSevera)
            {
              fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
            }
        }else{ //����� �� ����� 360
      //������� ��� ������
           fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
        }
        TekVis=GetNextFilterOB(TekVis,true);
    }
    fclose(fpVrem);
    Strings->LoadFromFile(TempFile);
    DeleteFile(TempFile);
    GlobalCGauge1->Visible=false;
    return 1;
}


/*��������� ������� ������ - �������� ������������ ������.
*/
int OBZORY_1::ShowDatas1(TStrings *Strings)
{
   long Indx,Nomer,Indx1;
   long TekVis;
   long TekSvr=0;  //������� ������ ������
   long RetIndxSvr; //������������ ������ ������
   long NomSektSvr=0;

   if(FirstVisual<0)return 0;  //��� ��������� ������
   Strings->Clear();
   long NomObz=0;
   FILE *fpVrem;
   AnsiString TempFile=GetTempFileInTempPath();
   long TekChas,PrevChas;
   extern TCGauge *GlobalCGauge1;
   TekChas=PrevChas=(long)(CLSutkiFirst*24+CLChasyFirst);

   GlobalCGauge1->MinValue=-2000000000;
   GlobalCGauge1->MaxValue=2000000000;


   GlobalCGauge1->Progress=TekChas;
   GlobalCGauge1->MinValue=TekChas;
   GlobalCGauge1->MaxValue=(long)(CLSutkiCur*24+CLChasyCur);
   GlobalCGauge1->Visible=true;


   fpVrem=fopen(TempFile.c_str(),"w");

    TekVis=GetFirstFilterOB(false);
    while(TekVis>=0)
    {
        Indx=TekVis/362;
        Nomer=TekVis%362;
        if(Nomer==360)
        {
            if(VydelyatSevera)
            {
              fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
            }
        }else{ //����� �� ����� 360
      //������� ��� ������
           fputs(GetStrokaFrom(Nomer,Indx).c_str(),fpVrem);
        }
        TekVis=GetNextFilterOB(TekVis,false);
    }

   fclose(fpVrem);
   Strings->LoadFromFile(TempFile);
   DeleteFile(TempFile);
   GlobalCGauge1->Visible=false;
   return 1;
}




//���������� ������ ������ ��� ������ ���������
AnsiString OBZORY_1::GetStrokaForIskaj(long NomerA,long Indx)
{
  char Strka[1000];
  AnsiString RetStrka="";
  char StrkaNom[18];
  FILE *fp;


  if(NomerA<0||NomerA>360)return RetStrka;
  if(Indx<0||Indx>=N_of_o_b_1[NomerA])return RetStrka;

  if(curOpenF)
  {
        if(curIndxOfOpenF==o_b_1[NomerA][Indx].NFile)
        {
           fp=curOpenF;
        }else{
           fclose(curOpenF);
           curOpenF=NULL;
        }
  }
  if(!curOpenF)
  {
        fp=(curOpenF=MyFOpenR(fps[o_b_1[NomerA][Indx].NFile]));
        curIndxOfOpenF=o_b_1[NomerA][Indx].NFile;
  }


  fseek(fp,o_b_1[NomerA][Indx].Smesh,0);

//� ������ ������� ������
  fgets(Strka,499, fp);

  if(NomerA==360)
  {
    return "";
  }


  if(!(o_b_1[NomerA][Indx].Status2&PRIVYAZAN_OTSCHET_STATUS2)||
       (otis[o_b_1[NomerA][Indx].IndxBrt].ulNumber&0x03000000)==0||
       (otis[o_b_1[NomerA][Indx].IndxBrt].ulNumber)>=0x03000000||
      (o_b_1[NomerA][Indx].Status&DROBLENIE_STATUS)||
      (o_b_1[NomerA][Indx].Status&LOJNY_STATUS)||
      !(o_b_1[NomerA][Indx].Status&2)
    )
  {
    return String("");
  }



  if(Strka[2]=='3')
  {
      RetStrka=String(Strka).SubString(1,117);
      if(RetStrka[RetStrka.Length()]=='\n')
      {
        RetStrka[RetStrka.Length()]=' ';
      }
      while(RetStrka.Length()<117)
      {
        RetStrka=RetStrka+' ';
      }

  }else{
      RetStrka=String(Strka).SubString(1,103);
      if(RetStrka[RetStrka.Length()]=='\n')
      {
        RetStrka[RetStrka.Length()]=' ';
      }

      while(RetStrka.Length()<103)
      {
        RetStrka=RetStrka+' ';
      }
  }
  sprintf(StrkaNom,"%05d %05d",
          0xFFFFF&otis[o_b_1[NomerA][Indx].IndxBrt].ulNumber,
          o_b_1[NomerA][Indx].IstVysota);

  RetStrka=RetStrka+StrkaNom;
  return RetStrka;
}




AnsiString OBZORY_1::SaveDataForFindIskaj(AnsiString FN)
{
//����������
  int i;
  char TempFileName[13];
  long LastProgess;
  long Indexes[360],k,   TekSektor=0;
  FILE *fp;
  long FirstChas,
       LastChas=0; //��������� ���
  long Nom, Indx;

  AnsiString RetStrka;





  extern TCGauge *GlobalCGauge1;
  LastProgess=GlobalCGauge1->Progress;
  AnsiString FN1="";
  memset(Indexes,0,sizeof(long)*360);
//  Dlina=FN.Length();

  TempFileForIskaj="";
  for(i=0;i<100;i++)
  {
    sprintf(TempFileName,"AnlTmp%02d.txt",i);
    FN1=PutKTemp+String("\\")+String(TempFileName);
    if(!FileExists(FN1))
    {
      TempFileForIskaj=FN1;
      break;
    }
  }

  if(TempFileForIskaj.Length()==0)
  {
    return "";
  }

  fp=fopen(FN1.c_str(),"w");
  if(fp==NULL)
  {

    return "";
  }
  VremFN0=FN1;  //��������, ����� ����� ������� ���� ����
  LastChas=FirstChas=CLSutkiFirst*24+CLChasyFirst;
  i=FirstAbs;
  while(i>=0)
  {
       Nom=i%362;
       Indx=i/362;
       if(IsCheckTime&&
         (o_b_1[Nom][Indx].Time>Time2||o_b_1[Nom][Indx].Time<Time1))
       {
                i=o_b_1[Nom][Indx].AbsNext;
                continue;
       }
       if(Nom==360)
       {
              i=o_b_1[Nom][Indx].AbsNext;
              continue;
       }
       if(IsInPlot)
       {
           if((o_b_1[Nom][Indx].Status&3)==2)
           {
             RetStrka=GetStrokaForIskaj(Nom,Indx);
             if(RetStrka!="")
             {
               fputs(RetStrka.c_str(),fp);
               fputs("\n",fp);
             }
           }
       }else{
           if((o_b_1[Nom][Indx].Status&3)>=2)
           {
             RetStrka=GetStrokaForIskaj(Nom,Indx);
             if(RetStrka!="")
             {
               fputs(RetStrka.c_str(),fp);
               fputs("\n",fp);
             }
           }
       }
       i=o_b_1[Nom][Indx].AbsNext;

  }

   fclose(fp);
  return VremFN0;
}



int OBZORY_1::BackupDataForFindIskaj(void)
{
 AnsiString GetTempFileInTempPath(void);
  BackupFileNameIskaj=GetTempFileInTempPath();;
  fpDataBack=fopen(BackupFileNameIskaj.c_str(),"wb");
  TempSaveAllDataAndFreeMemory(fpDataBack);
  return 1;
}


int OBZORY_1::RestoreAllAfterFindIskaj(void)
{
  fclose(fpDataBack);
  fpDataBack=fopen(BackupFileNameIskaj.c_str(),"rb");
  TempRestoreAllData(fpDataBack);
  fclose(fpDataBack);

//������ ����
  DeleteFile(BackupFileNameIskaj);
//  DeleteFile(VremFN0);
  return 1;
}






int OBZORY_1::MaxPOtmetokZaObzor(TStrings *Strings,
                                 AnsiString *FN)
{
  long *N_of_P;  //����� ������� P
  long *PervSmesh;
  long *LastSmesh;
  int i,k;
  int N_of_Obzorov=LastNomerObzora+1;
  double Tm1=-1,Tm2;
  long FirstRasmObzor=-1; //������ ��������������� �����
  long smesh;
  int index_p=-1;
  long Prom;

  if(LastNomerObzora==0)return 0;
  N_of_P=(long*)malloc(sizeof(long)*(LastNomerObzora+1));
  PervSmesh=(long*)malloc(sizeof(long)*(LastNomerObzora+1));
  LastSmesh=(long*)malloc(sizeof(long)*(LastNomerObzora+1));
//�������������� ������� ��������� ����� �������� ���������� ������ � ������ ������


//���� ��������� ������
  for(i=0;i<=LastNomerObzora;i++)
  {
     N_of_P[i]=0;
     PervSmesh[i]=-1;
  }

  for(i=1;i<n_of_otis;i++)
  {
        if(otis[i].ulNumber==0x14000000)
        {
                index_p=i;
                break;
        }
  }

  if(index_p<0)return 0;

  smesh=FirstAbs;
  while(smesh>=0)
  {
         if(smesh%362==360)
         {
                smesh=o_b_1[smesh%362][smesh/362].AbsNext;
                continue;
         }

         if(IsCheckTime&&
             (o_b_1[smesh%362][smesh/362].Time>Time2||
              o_b_1[smesh%362][smesh/362].Time<Time1))
        {
                smesh=tnGetNextMainOtshet(smesh);
                continue;
        }

         if(Tm1<0)
         {
                Tm1=o_b_1[smesh%362][smesh/362].Time;
         }
         Tm2=o_b_1[smesh%362][smesh/362].Time;

         if(o_b_1[smesh%362][smesh/362].IndxBrt==index_p)
         {
                N_of_P[o_b_1[smesh%362][smesh/362].nomer_obzora]++;
         }
        if(PervSmesh[o_b_1[smesh%362][smesh/362].nomer_obzora]<0)
        {
                PervSmesh[o_b_1[smesh%362][smesh/362].nomer_obzora]=smesh;
        }
        LastSmesh[o_b_1[smesh%362][smesh/362].nomer_obzora]=smesh;
        smesh=o_b_1[smesh%362][smesh/362].AbsNext;
  };






//������ ���� ������� ������ 20 �������, � ������������ ����� ������� P
  N_of_Obzorov=20;
  for(i=0;i<20;i++)
  {
    for(k=LastNomerObzora;k>i;k--)
    {
      if(N_of_P[k]>N_of_P[k-1])
      {
//�������� �������
         Prom=N_of_P[k];
         N_of_P[k]=N_of_P[k-1];
         N_of_P[k-1]=Prom;
         Prom=PervSmesh[k];
         PervSmesh[k]=PervSmesh[k-1];
         PervSmesh[k-1]=Prom;
         Prom=LastSmesh[k];
         LastSmesh[k]=LastSmesh[k-1];
         LastSmesh[k-1]=Prom;

      }
    }
    if(N_of_P[i]==0)
    {
      N_of_Obzorov=i;
      break;
    }
  }

   FILE *fpVrem;
   AnsiString TempFile=GetTempFileInTempPath();
   fpVrem=fopen(TempFile.c_str(),"w");
   if(fpVrem==NULL)
   {
     free(N_of_P);
     free(PervSmesh);
     return 0;
   }

//������ ������������ ����� � ����
   fprintf(fpVrem,"Scans with maximum number of unbound P marks\n");

   AnsiString StrkString;
   if(FIO_User.Length())
   {
     StrkString=String("Made: ")+FIO_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }
   if(Podr_User.Length())
   {
     StrkString=String("Department: ")+Podr_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }

   StrkString=String("Date: ")+Date_Input;
   fprintf(fpVrem,StrkString.c_str());
   fprintf(fpVrem,"\n");

   if(Dop_Info.Length())
   {
      fprintf(fpVrem,Dop_Info.c_str());
      fprintf(fpVrem,"\n");
   }

//������� ������ �� ������
   if(FN)
   {
     fprintf(fpVrem,"Files have been examined:  ");
     fprintf(fpVrem,"\n");
     for(i=0;i<N_of_fps;i++)
     {
       fprintf(fpVrem,"%s\n",FN[i].c_str());
     }
   }


//������� ������

  long Stk1,Stk2,Chs1,Chs2,Mn1,Mn2,Skn1,Skn2;
  Stk1=(long)(Tm1/24.0);   Stk2=(long)(Tm2/24.0);
  Tm1=Tm1-Stk1*24;         Tm2=Tm2-Stk2*24;
  Chs1=(long)Tm1;          Chs2=(long)Tm2;
  Tm1=Tm1-Chs1;            Tm2=Tm2-Chs2;
  Mn1=(long)(Tm1*60);      Mn2=(long)(Tm2*60);
  Tm1=Tm1*60-Mn1;          Tm2=Tm2*60-Mn2;
  Skn1=(long)(Tm1*60);     Skn2=(long)(Tm2*60);

  fprintf(fpVrem," Examined time interval: %02d:%02d:%02d - %02d:%02d:%02d\n",
  Chs1,Mn1,Skn1,   Chs2,Mn2,Skn2);


   if(N_of_Obzorov==0)
   {
     fprintf(fpVrem,"No unbound P marks\n");
   }else{
//������� � ���� � ��� �������
     for(k=0;k<N_of_Obzorov;k++)
     {
       fprintf(fpVrem,"\nScan � %ld\n",k+1);
       fprintf(fpVrem," Number of unbound P marks = %ld\n",N_of_P[k]);
       VyvodObzor(PervSmesh[k],fpVrem);
     }

   }


  fclose(fpVrem);
  free(N_of_P);
  free(PervSmesh);

  Strings->Clear();
  Strings->LoadFromFile(TempFile);
  DeleteFile(TempFile);


  return 1;
}

/*�������������� ��������� � */
int OBZORY_1::VyvodObzor(long FirstPerv,
                 FILE *fpOut)  //�������� ����
{
  long Indx,Nomer;
  long LastNomer,FirstNomer;
  int i,j;
  int isWasNord=0;
  double dfTime1, dfTime2;
  long Smesh,PrevSmesh,NextSmesh;
//������ ����� ������ ������
  dfTime1=o_b_1[FirstPerv%362][FirstPerv/362].Time-
        (o_b_1[FirstPerv%362][FirstPerv/362].Azmt/360.0)*TO-TO/16;

  if(dfTime1<0)dfTime1+=24.0;

  dfTime2=dfTime1+9.0/8.0*TO;
  if(dfTime2>=24.0)dfTime2-=24.0;
  Smesh=FirstPerv;
  PrevSmesh=o_b_1[Smesh%362][Smesh/362].AbsPrev;
  while(PrevSmesh>=0&&
    toolsGetDTime(dfTime1,o_b_1[PrevSmesh%362][PrevSmesh/362].Time)>=0
  )
  {
        if((PrevSmesh%362)==360)
        {
                Smesh=PrevSmesh;
                break;
        }
        if((o_b_1[PrevSmesh%362][PrevSmesh/362].Status&3)==1)
        {
             if(o_b_1[PrevSmesh%362][PrevSmesh/362].Azmt-
                o_b_1[Smesh%362][Smesh/362].Azmt>=45)
             {
                  break;
             }
        }
        Smesh=PrevSmesh;
        PrevSmesh=o_b_1[Smesh%362][Smesh/362].AbsPrev;

  }


  while(Smesh>=0&&
    toolsGetDTime(o_b_1[Smesh%362][Smesh/362].Time,dfTime2)>=0)
  {
       i=Smesh%362;
       j=Smesh/362;
       if((Smesh%362)==360)
       {
           fputs(GetStrokaFrom(i,j).c_str(),fpOut);
           if(isWasNord)break;
           isWasNord=1;
           Smesh=o_b_1[Smesh%362][Smesh/362].AbsNext;
           continue;
       }
       fputs(GetStrokaFrom(i,j).c_str(),fpOut);
       Smesh=o_b_1[Smesh%362][Smesh/362].AbsNext;
  }

  return 1;
}


/*�������������� ���������, ���������� �� ��������� MaxSredPOtmetokZaObzor*/
int OBZORY_1::GetTimesOfObzorFromSmesh(
                        long smesh,    //�������� ������� ��
                        int dObzors,   //��������� ������
                        double &time1,
                        double &time2)
{
 long Indx,Nomer;
  long LastNomer,FirstNomer;
  int i,j;
  int isWasNord=0;
  double dfTime1, dfTime2;
  double dfTime1_, dfTime2_;

  long Smesh,PrevSmesh,NextSmesh;
//������ ����� ������ ������
  dfTime1_=o_b_1[smesh%362][smesh/362].Time-
          (o_b_1[smesh%362][smesh/362].Azmt/360.0)*TO;
  if(dfTime1_<0)dfTime1_+=24.0;
  dfTime1=dfTime1_-TO/16.0;
  if(dfTime1<0)dfTime1+=24.0;



  Smesh=smesh;
  PrevSmesh=o_b_1[Smesh%362][Smesh/362].AbsPrev;
  while(PrevSmesh>=0&&
    toolsGetDTime(dfTime1,o_b_1[PrevSmesh%362][PrevSmesh/362].Time)>=0
  )
  {
        if((PrevSmesh%362)==360)
        {
                if(ABS(toolsGetDTime(dfTime1_,
                   o_b_1[PrevSmesh%362][PrevSmesh/362].Time))<TO/8.0)
                {
                        Smesh=PrevSmesh;
                        isWasNord=1;
                        break;
                }
        }
        if((o_b_1[PrevSmesh%362][PrevSmesh/362].Status&3)==1)
        {
             if(o_b_1[PrevSmesh%362][PrevSmesh/362].Azmt-
                o_b_1[Smesh%362][Smesh/362].Azmt>=45)
             {
                  break;
             }
        }
        Smesh=PrevSmesh;
        PrevSmesh=o_b_1[Smesh%362][Smesh/362].AbsPrev;

  }
  if(isWasNord)
  {
       time1=o_b_1[Smesh%362][Smesh/362].Time;
  }else{
       time1=dfTime1_;
  }

  dfTime2_=time1+TO*dObzors;
  if(dfTime2_>=24.0)dfTime2_-=24.0;
  dfTime2=dfTime2_+TO/16.0;
  if(dfTime2>=24.0)dfTime2-=24.0;

  Smesh=o_b_1[smesh%362][smesh/362].AbsNext;
  isWasNord=0;
  while(Smesh>=0&&
    toolsGetDTime(o_b_1[Smesh%362][Smesh/362].Time,dfTime2)>=0)
  {
       if((Smesh%362)==360)
       {
                if(ABS(toolsGetDTime(dfTime1_,
                   o_b_1[Smesh%362][Smesh/362].Time))<TO/8.0)
                {
                        isWasNord=1;
                        break;
                }
       }
       Smesh=o_b_1[Smesh%362][Smesh/362].AbsNext;
  }

  if(isWasNord)
  {
       time2=o_b_1[Smesh%362][Smesh/362].Time;
  }else{
       time2=dfTime2_;
  }


  return 1;
}



/*������ ��������� ���� ������������ ����, ����� ���� ������� ����������*/

int OBZORY_1::MaxSredPOtmetokZaObzor(TStrings *Strings, AnsiString *FN)
{
  long *N_of_P,ObshN_of_P;  //����� ������� P
  long *PervSmesh;
  long *LastSmesh;
  int i,k;
  long lFirstSmesh=-1, lLastSmesh=-1;
  long TekSektor=0;
  long Indexes[362];
  double Tm1=-1,Tm2;
  FILE *fpVrem;
  double T5min;   //�������� 5 ����� � �
  long ChisloObzorov5min;
  AnsiString TempFile=GetTempFileInTempPath();
  long FirstRasmObzor=-1; //������ ��������������� �����
  long LastRasmObzor=-1;  //��������� ��������������� �����
  long Nomer,Indx;
  int index_p=-1;
  long smesh;

  if(LastNomerObzora==0)return 0;
  N_of_P=(long*)malloc(sizeof(long)*(LastNomerObzora+1));
  PervSmesh=(long*)malloc(sizeof(long)*(LastNomerObzora+1));
  LastSmesh=(long*)malloc(sizeof(long)*(LastNomerObzora+1));
//�������������� ������� ��������� ����� �������� ���������� ������ � ������ ������


//���� ��������� ������
  for(i=0;i<=LastNomerObzora;i++)
  {
     N_of_P[i]=0;
     PervSmesh[i]=-1;
  }

  for(i=1;i<n_of_otis;i++)
  {
        if(otis[i].ulNumber==0x14000000)
        {
                index_p=i;
                break;
        }
  }

  if(index_p<0)return 0;

  smesh=FirstAbs;
  while(smesh>=0)
  {
        if(smesh%362==360)
        {
                smesh=o_b_1[smesh%362][smesh/362].AbsNext;
                continue;
        }

        if(IsCheckTime&&
             (o_b_1[smesh%362][smesh/362].Time>Time2||
              o_b_1[smesh%362][smesh/362].Time<Time1))
        {
                smesh=o_b_1[smesh%362][smesh/362].AbsNext;
                continue;
        }

        if(Tm1<0)
        {
                Tm1=o_b_1[smesh%362][smesh/362].Time;
        }
        Tm2=o_b_1[smesh%362][smesh/362].Time;


        if(FirstRasmObzor<0)
        {
             FirstRasmObzor=o_b_1[smesh%362][smesh/362].nomer_obzora;
        }
        if(o_b_1[smesh%362][smesh/362].IndxBrt==index_p)
        {
                N_of_P[o_b_1[smesh%362][smesh/362].nomer_obzora]++;
        }
        if(PervSmesh[o_b_1[smesh%362][smesh/362].nomer_obzora]<0)
        {
                PervSmesh[o_b_1[smesh%362][smesh/362].nomer_obzora]=smesh;
        }

        LastRasmObzor=o_b_1[smesh%362][smesh/362].nomer_obzora;
        LastSmesh[o_b_1[smesh%362][smesh/362].nomer_obzora]=smesh;
        smesh=o_b_1[smesh%362][smesh/362].AbsNext;
  };


  if(FirstRasmObzor<0)
  {
        free(N_of_P);
        free(PervSmesh);
        free(LastSmesh);
        return 0;
  }


//���������� ����� ������� ��� ���� �����
  T5min=5.0/60;  //����� ����� ��� 5 �����
  ChisloObzorov5min=(long)(T5min/TO)+1;  //���������� ����� ������� �� 5 �����
                   //� �� ������ ������ ������� 1

  ObshN_of_P=0;






  double SredPOtmetok;  //������� ����� ������� �� �����
  long OkruglSredPOtmetok; //����������� ����� P ������� �� �����
  long MaxN_of_P=0;     //������������ ����� ������� ���� P �� 5 �����
  long i1_max=-1,i2_max=-1;   //�������, ��� ������������ ����� �������
  long MaxN_of_Obz;     //����� ������������� ��������������� �������, ��� ����

  long N_of_Obz=0,N_of_Obz1=0;
  long MaxN_of_PObzor=0;
  double MaxFirstTime;
  long j;

  ObshN_of_P=0;

  if(LastRasmObzor-FirstRasmObzor+1<=ChisloObzorov5min)
  {
     for(j=FirstRasmObzor;j<=LastRasmObzor;j++)
     {
        ObshN_of_P+=N_of_P[j];
     }
     i1_max=FirstRasmObzor;
     i2_max=LastRasmObzor;
  }else{
     for(i=FirstRasmObzor;i<=LastRasmObzor-ChisloObzorov5min+1;i++)
     {
        ObshN_of_P=0;
        for(j=i;j<i+ChisloObzorov5min;j++)
        {
              ObshN_of_P+=N_of_P[j];
        }
        if(MaxN_of_P<ObshN_of_P)
        {
              MaxN_of_P=ObshN_of_P;
              i1_max=i;
              i2_max=i+ChisloObzorov5min-1;
        }
     }
   }

   if(MaxN_of_P>0)
   {
        MaxN_of_Obz=LastRasmObzor-FirstRasmObzor+1;
        SredPOtmetok=ObshN_of_P/(double)(MaxN_of_Obz);
        OkruglSredPOtmetok=Okrugl(SredPOtmetok);

   }

//������ ������� ��� ������
  if((i1_max>=0)&&(i2_max>=0))
  {
    for(i=i1_max;i<=i2_max;i++)
    {
      if(N_of_P[i]>MaxN_of_PObzor)
      {
        MaxN_of_PObzor=N_of_P[i];
        Nomer=PervSmesh[i]%362;
        Indx=PervSmesh[i]/362;
        MaxFirstTime=o_b_1[Nomer][Indx].Time;
      }
    }
  }
//������ ����� �������� � ���� ��� ������
   fpVrem=fopen(TempFile.c_str(),"w");
   if(fpVrem==NULL)
   {
     free(N_of_P);
     free(PervSmesh);
     free(LastSmesh);
     return 0;
   }

//������ ������������ ����� � ����
   fprintf(fpVrem,"Maximum average number of unbound P marks in scan\n");
   AnsiString StrkString;
   if(FIO_User.Length())
   {
     StrkString=String("Made: ")+FIO_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }
   if(Podr_User.Length())
   {
     StrkString=String("Department: ")+Podr_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }

   StrkString=String("Date: ")+Date_Input;
   fprintf(fpVrem,StrkString.c_str());
   fprintf(fpVrem,"\n");

   if(Dop_Info.Length())
   {
      fprintf(fpVrem,Dop_Info.c_str());
      fprintf(fpVrem,"\n");
   }

//������� ������ �� ������
   if(FN)
   {
     fprintf(fpVrem,"Files have been examined:  ");
     fprintf(fpVrem,"\n");
     for(i=0;i<N_of_fps;i++)
     {
       fprintf(fpVrem,"%s\n",FN[i].c_str());
     }
   }

  long Stk1,Stk2,Chs1,Chs2,Mn1,Mn2,Skn1,Skn2;


//������� ������ � ��������������� �������
//�������������� ������
//������ ������ ����� �������
  Stk1=(long)(Tm1/24.0);   Stk2=(long)(Tm2/24.0);
  Tm1=Tm1-Stk1*24;         Tm2=Tm2-Stk2*24;
  Chs1=(long)Tm1;          Chs2=(long)Tm2;
  Tm1=Tm1-Chs1;            Tm2=Tm2-Chs2;
  Mn1=(long)(Tm1*60);      Mn2=(long)(Tm2*60);
  Tm1=Tm1*60-Mn1;          Tm2=Tm2*60-Mn2;
  Skn1=(long)(Tm1*60);     Skn2=(long)(Tm2*60);

  fprintf(fpVrem," Examined time interval:  %02d:%02d:%02d - %02d:%02d:%02d\n",
  Chs1,Mn1,Skn1,   Chs2,Mn2,Skn2);


//������� ������ �� ������� ����������� ������������ ����� P �������
  if(MaxN_of_P==0)
  {
    fprintf(fpVrem,"No found unbound P marks \n");
  }else if(FirstRasmObzor==LastRasmObzor)
  {
    fprintf(fpVrem,"Time interval is too small \n");
  }else{
  //������� ��� ������
  //��������� ����� Tm1
    while(PervSmesh[i1_max]<0)i1_max++;
    while(PervSmesh[i2_max]<0)i1_max--;

    Nomer=PervSmesh[i1_max]%362;
    Indx=PervSmesh[i1_max]/362;
    Tm1=o_b_1[Nomer][Indx].Time;
    Tm2=o_b_1[Nomer][Indx].Time;
    Stk1=(long)(Tm1/24.0);   Stk2=(long)(Tm2/24.0);
    Tm1=Tm1-Stk1*24;         Tm2=Tm2-Stk2*24;
    Chs1=(long)Tm1;          Chs2=(long)Tm2;
    Tm1=Tm1-Chs1;            Tm2=Tm2-Chs2;
    Mn1=(long)(Tm1*60);      Mn2=(long)(Tm2*60);
    Tm1=Tm1*60-Mn1;          Tm2=Tm2*60-Mn2;
    Skn1=(long)(Tm1*60);     Skn2=(long)(Tm2*60);

     fprintf(fpVrem,"Period where clean marks on the scan of the maximum: \n \
   %02d:%02d:%02d - %02d:%02d:%02d\n",
     Chs1,Mn1,Skn1,   Chs2,Mn2,Skn2);

     fprintf(fpVrem,"Number of marks Np of the primary channel in the course of found time interval: %ld\n",
        MaxN_of_P);
     fprintf(fpVrem,"Number of scans No in the course of found time interval: %ld\n",
     MaxN_of_Obz);
     fprintf(fpVrem,"Average number of false clutters over this period: %lf\n",
        SredPOtmetok);
     fprintf(fpVrem,"Average number of false clutters over this period (Rounded value): %ld\n",
        OkruglSredPOtmetok);


     fprintf(fpVrem,"Maximum number of P marks per scan in this interva: %d",
          MaxN_of_PObzor);


    Stk1=(long)(MaxFirstTime/24.0);
    MaxFirstTime=MaxFirstTime-Stk1*24;
    Chs1=(long)MaxFirstTime;
    MaxFirstTime=MaxFirstTime-Chs1;
    Mn1=(long)(MaxFirstTime*60);
    MaxFirstTime=MaxFirstTime*60-Mn1;
    Skn1=(long)(MaxFirstTime*60);


      fprintf(fpVrem, " in the scan from %02d:%02d:%02d",
        Chs1,Mn1,Skn1);

  }


  fclose(fpVrem);
  free(N_of_P);
  free(PervSmesh);
  free(LastSmesh);

  Strings->Clear();
  Strings->LoadFromFile(TempFile);
  DeleteFile(TempFile);


  return 1;

}




int OBZORY_1::FindDroblOtschet(TStrings *Strings, AnsiString *FN)
{

   int i,j;
   int *n_of_vis_o;  //����� ������� ����
   int *n_of_drobl;
   int **smeshs_drubl;
   int **smeshs_istin;
   long smesh;

   n_of_vis_o=(int*)malloc(sizeof(int)*(n_of_otis));
   n_of_drobl=(int*)malloc(sizeof(int)*(n_of_otis));
   smeshs_drubl=(int**)malloc(sizeof(int*)*(n_of_otis));
   smeshs_istin=(int**)malloc(sizeof(int*)*(n_of_otis));
   for(i=0;i<n_of_otis;i++)
   {
        n_of_vis_o[i]=0;
        n_of_drobl[i]=0;
        smeshs_drubl[i]=NULL;
        smeshs_istin[i]=NULL;
   }

   smesh=FirstAbs;
   while(smesh>=0)
   {
       if(smesh==71078)
       {
               int debug=1; 
       }
       if((smesh%362)==360)
       {
            smesh=o_b_1[smesh%362][smesh/362].AbsNext;
            continue;
       }
       if(!(o_b_1[smesh%362][smesh/362].Status&VISIBLE_STATUS))
       {
            smesh=o_b_1[smesh%362][smesh/362].AbsNext;
            continue;
       }

       if((o_b_1[smesh%362][smesh/362].Status&3)==1)
       {
           smesh=o_b_1[smesh%362][smesh/362].AbsNext;
           continue;
       }

       if((o_b_1[smesh%362][smesh/362].Status&3)==3)
       {
           if(o_b_1[smesh%362][smesh/362].ParnOtschet1>=0)
           {
                smesh=o_b_1[smesh%362][smesh/362].AbsNext;
                continue;
           }
       }
       if((o_b_1[smesh%362][smesh/362].IndxBrt==indx_otis_rbs_not_priv)||
          (o_b_1[smesh%362][smesh/362].IndxBrt==indx_otis_uvd_not_priv)||
          (o_b_1[smesh%362][smesh/362].IndxBrt==indx_otis_pk_not_priv))
       {
           smesh=o_b_1[smesh%362][smesh/362].AbsNext;
           continue;
       }

       n_of_vis_o[o_b_1[smesh%362][smesh/362].IndxBrt]++;

       if(o_b_1[smesh%362][smesh/362].ParnForDubl>=0)
       {
                smeshs_drubl[o_b_1[smesh%362][smesh/362].IndxBrt]=
                        (int*)realloc(smeshs_drubl[o_b_1[smesh%362][smesh/362].IndxBrt],
                          (n_of_drobl[o_b_1[smesh%362][smesh/362].IndxBrt]+1)*sizeof(int));

                smeshs_istin[o_b_1[smesh%362][smesh/362].IndxBrt]=
                        (int*)realloc(smeshs_istin[o_b_1[smesh%362][smesh/362].IndxBrt],
                          (n_of_drobl[o_b_1[smesh%362][smesh/362].IndxBrt]+1)*sizeof(int));

                smeshs_drubl[o_b_1[smesh%362][smesh/362].IndxBrt][n_of_drobl[o_b_1[smesh%362][smesh/362].IndxBrt]]=
                                                     o_b_1[smesh%362][smesh/362].ParnForDubl;
                smeshs_istin[o_b_1[smesh%362][smesh/362].IndxBrt][n_of_drobl[o_b_1[smesh%362][smesh/362].IndxBrt]]=
                                                     smesh;
                n_of_drobl[o_b_1[smesh%362][smesh/362].IndxBrt]++;

       }
       smesh=o_b_1[smesh%362][smesh/362].AbsNext;
   }

  AnsiString TempFile=GetTempFileInTempPath();
  FILE *fpVrem;
  fpVrem=fopen(TempFile.c_str(),"w");
  if(fpVrem)
  {
        fprintf(fpVrem,"Split secondary channel marks\n");
        AnsiString StrkString;
        if(FIO_User.Length())
        {
                StrkString=String("Made: ")+FIO_User;
                fprintf(fpVrem,StrkString.c_str());
                fprintf(fpVrem,"\n");
        }
        if(Podr_User.Length())
        {
                StrkString=String("Department: ")+Podr_User;
                fprintf(fpVrem,StrkString.c_str());
                fprintf(fpVrem,"\n");
        }

        StrkString=String("Date: ")+Date_Input;
        fprintf(fpVrem,StrkString.c_str());
        fprintf(fpVrem,"\n");

        if(Dop_Info.Length())
        {
                 fprintf(fpVrem,Dop_Info.c_str());
                fprintf(fpVrem,"\n");
        }

//������� ������ �� ������
        if(FN)
        {
                fprintf(fpVrem,"Files have been examined:");
                fprintf(fpVrem,"\n");
                for(i=0;i<N_of_fps;i++)
                {
                        fprintf(fpVrem,"%s\n",FN[i].c_str());
                }
        }

        for(i=0;i<n_of_otis;i++)
        {
                if(n_of_drobl[i]>0)
                {
                        if((otis[i].ulNumber&0xFF000000)==0x01000000)
                        {
                                fprintf(fpVrem, "Aircraft of UVD mode. Tail number: %d \n",
                                     (int)(otis[i].ulNumber&0x00FFFFFF));
                        }else
                        if((otis[i].ulNumber&0xFF000000)==0x02000000)
                        {
                                fprintf(fpVrem, "Aircraft of RBS mode. Tail number: %d \n",
                                     (int)(otis[i].ulNumber&0x00FFFFFF));
                        }else{
                             continue;
                        }
                        fprintf(fpVrem, "Number of scans: %d, \n", n_of_vis_o[i]);
                        fprintf(fpVrem, "There were %d splitting among them \n", n_of_drobl[i]);

                        for(j=0;j<n_of_drobl[i];j++)
                        {
                                fprintf(fpVrem, "Aircraft plot: \n");
                                fputs(GetStrokaFrom(smeshs_istin[i][j]%362,
                                                    smeshs_istin[i][j]/362).c_str(),
                                       fpVrem);

                                fprintf(fpVrem, "Duplicated plots: \n");
                                fputs(GetStrokaFrom(smeshs_drubl[i][j]%362,
                                                    smeshs_drubl[i][j]/362).c_str(),
                                       fpVrem);
                                fprintf(fpVrem, "\n");
                        }
                        fprintf(fpVrem, "\n");
                }
        }

        fclose(fpVrem);

  }
  for(i=0;i<n_of_otis;i++)
  {
        if(smeshs_drubl[i])
        {
                free(smeshs_drubl[i]);
        }
        if(smeshs_istin[i])
        {
                free(smeshs_istin[i]);
        }
  }

  free(smeshs_drubl);
  free(smeshs_istin);
  free(n_of_vis_o);
  free(n_of_drobl);
  Strings->Clear();
  Strings->LoadFromFile(TempFile);
  DeleteFile(TempFile);
 //����� ���������� �������� ���������� ������ �� ���������� ������
/*
  int Ret;
  long *NumberN_of_O;      //����� �������� �� ����
  long *NumberN_of_Drobl;  //����� ��������, ������� ����������
  long **SmeshsDrobl;       //������ �������� ������
  long **SmeshsDrobl2;      //������ �������� ��������������� ����������
  long i;
  long *N_of_SmeshsDrobl;  //����� ������ ��������� ������
  bool OldKanalP=KanalP,OldKanalS=KanalS,OldKanalPS=KanalPS;
  long IndxBrt;   //������ �����

  bool RetB;
  KanalP=false;KanalS=true;

  if(IsInPlot)KanalPS=false; else KanalPS=true;

  long Nom,Indx,Nom1,Indx1;
  long j,j1;
  long SektorNomer1,SektorNomer2;
  double Azmt1,Azmt2,Dlnst1,Dlnst2;

  NumberN_of_O=(long*)malloc(N_of_Number*sizeof(long));
  NumberN_of_Drobl=(long*)malloc(N_of_Number*sizeof(long));
  N_of_SmeshsDrobl=(long*)malloc(N_of_Number*sizeof(long));
  SmeshsDrobl=(long**)malloc(N_of_Number*sizeof(long*));
  SmeshsDrobl2=(long**)malloc(N_of_Number*sizeof(long*));
  memset(NumberN_of_O,0,sizeof(long)*N_of_Number);
  memset(NumberN_of_Drobl,0,sizeof(long)*N_of_Number);
  memset(SmeshsDrobl,0,sizeof(long*)*N_of_Number);
  memset(SmeshsDrobl2,0,sizeof(long*)*N_of_Number);
  memset(N_of_SmeshsDrobl,0,sizeof(long)*N_of_Number);
  j=FirstVisual;
  while(j>=0)
  {
    Nom=j%362; Indx=j/362;
    if(Nom==360)
    {
      j=o_b_1[Nom][Indx].NextVisual;
      continue;
    }

    Ret=ProveritNastroikiOdinOtschet(Nom,Indx);
    if(Ret!=1)
    {
      j=o_b_1[Nom][Indx].NextVisual;
      continue;
    }
    if(o_b_1[Nom][Indx].IndxBrt>=N_of_Number-6)
    {
      j=o_b_1[Nom][Indx].NextVisual;
      continue;
    }
    IndxBrt=o_b_1[Nom][Indx].IndxBrt;
    NumberN_of_O[IndxBrt]++;
    if(o_b_1[Nom][Indx].Status&DROBLENIE_STATUS)
    {

//������ ��� ������� ���������� � ����� � ������� ��� �� ����� �����, � ������� ��������, ���
//�������������
      if(Number[IndxBrt]&0x01000000)
      {
//������ ��� �������, �������
        Azmt1=o_b_1[Nom][Indx].Azmt-RSA_RBS1; if(Azmt1<0)Azmt1+=360.0;
        Azmt2=o_b_1[Nom][Indx].Azmt+RSA_RBS1; if(Azmt2>=360)Azmt2-=360.0;
        Dlnst1=o_b_1[Nom][Indx].Dlnst-RSD_RBS1; Dlnst2=o_b_1[Nom][Indx].Dlnst+RSD_RBS1;
        SektorNomer1=o_b_1[Nom][Indx].SektorNomer-1;
        SektorNomer2=o_b_1[Nom][Indx].SektorNomer+1;
//        j1=FirstOtschetBort(N_of_Number-5,KanalP,KanalS,KanalPS);
        while(j1>=0)
        {
           Nom1=j1%362;Indx1=j1/362;
           if(o_b_1[Nom1][Indx1].SektorNomer>=SektorNomer1&&
              o_b_1[Nom1][Indx1].SektorNomer<=SektorNomer2)
           {
              RetB=TestForFindBlizkieOtschety(Azmt1,Azmt2,Dlnst1,Dlnst2,
                   KanalP,KanalS,KanalPS,false,true,o_b_1[Nom][Indx].NomBort,Nom1,Indx1);
              if(RetB)
              {
//���� �������� ���� ������
                 SmeshsDrobl[IndxBrt]=(long*)realloc(SmeshsDrobl[IndxBrt],
                                  sizeof(long)*(N_of_SmeshsDrobl[IndxBrt]+1));

                  SmeshsDrobl2[IndxBrt]=(long*)realloc(SmeshsDrobl2[IndxBrt],
                                  sizeof(long)*(N_of_SmeshsDrobl[IndxBrt]+1));
                  SmeshsDrobl[IndxBrt][N_of_SmeshsDrobl[IndxBrt]]=j;
                  SmeshsDrobl2[IndxBrt][N_of_SmeshsDrobl[IndxBrt]]=j1;
                  N_of_SmeshsDrobl[IndxBrt]++;
              }

           }
           if(o_b_1[Nom1][Indx1].SektorNomer>SektorNomer2)break;
           j1=NextOtschetBort(Nom1,Indx1,KanalP,KanalS,KanalPS);
        }
        NumberN_of_Drobl[IndxBrt]++;
      }else{
        Azmt1=o_b_1[Nom][Indx].Azmt-RSA_UVD1; if(Azmt1<0)Azmt1+=360.0;
        Azmt2=o_b_1[Nom][Indx].Azmt+RSA_UVD1; if(Azmt2>=360)Azmt2-=360.0;
        Dlnst1=o_b_1[Nom][Indx].Dlnst-RSD_UVD1; Dlnst2=o_b_1[Nom][Indx].Dlnst+RSD_UVD1;
        SektorNomer1=o_b_1[Nom][Indx].SektorNomer-1;
        SektorNomer2=o_b_1[Nom][Indx].SektorNomer+1;
//        j1=FirstOtschetBort(N_of_Number-3,KanalP,KanalS,KanalPS);
        while(j1>=0)
        {
           Nom1=j1%362;Indx1=j1/362;
           if(o_b_1[Nom1][Indx1].SektorNomer>=SektorNomer1&&
              o_b_1[Nom1][Indx1].SektorNomer<=SektorNomer2)
           {
              RetB=TestForFindBlizkieOtschety(Azmt1,Azmt2,Dlnst1,Dlnst2,
                   KanalP,KanalS,KanalPS,true,false,o_b_1[Nom][Indx].NomBort,Nom1,Indx1);
              if(RetB)
              {
//���� �������� ���� ������
                 SmeshsDrobl[IndxBrt]=(long*)realloc(SmeshsDrobl[IndxBrt],
                                        sizeof(long)*(N_of_SmeshsDrobl[IndxBrt]+1));

                  SmeshsDrobl2[IndxBrt]=(long*)realloc(SmeshsDrobl2[IndxBrt],
                                       sizeof(long)*(N_of_SmeshsDrobl[IndxBrt]+1));
                  SmeshsDrobl[IndxBrt][N_of_SmeshsDrobl[IndxBrt]]=j;
                  SmeshsDrobl2[IndxBrt][N_of_SmeshsDrobl[IndxBrt]]=j1;
                  N_of_SmeshsDrobl[IndxBrt]++;
              }

           }
           if(o_b_1[Nom1][Indx1].SektorNomer>SektorNomer2)break;
           j1=NextOtschetBort(Nom1,Indx1,KanalP,KanalS,KanalPS);
        }


        NumberN_of_Drobl[IndxBrt]++;
      }
    }

    j=o_b_1[Nom][Indx].NextVisual;
  };
  KanalP=OldKanalP;KanalS=OldKanalS; KanalPS=OldKanalPS;


//������� ��� ��� ������ � ����
  AnsiString TempFile=GetTempFileInTempPath();
  FILE *fpVrem;
  fpVrem=fopen(TempFile.c_str(),"w");
  if(!fpVrem)
  {
      free(NumberN_of_O);
      free(NumberN_of_Drobl);
      free(N_of_SmeshsDrobl);
      for(i=0;i<N_of_Number;i++)
      {
        if(SmeshsDrobl[i])free(SmeshsDrobl[i]);
        if(SmeshsDrobl2[i])free(SmeshsDrobl2[i]);
      }
      free(SmeshsDrobl);
      free(SmeshsDrobl2);
  }

  // ���� fprintf(fpVrem,"������ �� ������������� ��������\n");
    AnsiString StrkString;
   if(FIO_User.Length())
   {
     StrkString=String("Made: ")+FIO_User;
     fp1rintf(fpVrem,StrkString.c_str());
     fpr1intf(fpVrem,"\n");
   }
   if(Podr_User.Length())
   {
     StrkString=String("Department: ")+Podr_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }

   StrkString=String("Date: ")+Date_Input;
   fprintf(fpVrem,StrkString.c_str());
   fprintf(fpVrem,"\n");

   if(Dop_Info.Length())
   {
      fprintf(fpVrem,Dop_Info.c_str());
      fprintf(fpVrem,"\n");
   }

//������� ������ �� ������
   if(FN)
   {
    //���� fprintf(fpVrem,"��������������� �����:");
     fprintf(fpVrem,"\n");
     for(i=0;i<N_of_fps;i++)
     {
       fprintf(fpVrem,"%s\n",FN[i].c_str());
     }
   }



   for(i=0;i<N_of_Number-6;i++)
   {
     if(NumberN_of_O[i]==0)continue;
     fprintf(fpVrem,"\n\n");
     if(Number[i]&0x02000000)
     {
       //���� fprintf(fpVrem,"���� ������ ���. ����� �����: %d\n",Number[i]&0x00FFFFFF);
     }else{
       // ���� fprintf(fpVrem,"���� ������ RBS. ����� �����: %d\n",Number[i]&0x00FFFFFF);
     }
     //���� fprintf(fpVrem, "����� �������: %d,\n",NumberN_of_O[i]);
     //���� fprintf(fpVrem, "����� ��� ���� %d ���������.\n",NumberN_of_Drobl[i]);



     for(j=0;j<N_of_SmeshsDrobl[i];)
     {
       Nom=SmeshsDrobl[i][j]%362; Indx=SmeshsDrobl[i][j]/362;
       //���� fprintf(fpVrem,"������ �����:\n");
       fputs(GetStrokaFrom(Nom,Indx).c_str(),fpVrem);
       //���� fprintf(fpVrem,"\n������� �������������:\n");


       for(int k=j;k<=N_of_SmeshsDrobl[i];k++)
       {
         if(k==N_of_SmeshsDrobl[i]||SmeshsDrobl[i][j]!=SmeshsDrobl[i][k])
         {
           j=k-1;
           break;
         }
         Nom1=SmeshsDrobl2[i][k]%362;Indx1=SmeshsDrobl2[i][k]/362;
         fputs(GetStrokaFrom(Nom1,Indx1).c_str(),fpVrem);
       }
       fprintf(fpVrem,"\n\n");
       j++;
     }
     fprintf(fpVrem,"\n\n");
   }

  fclose(fpVrem);

  free(NumberN_of_O);
  free(NumberN_of_Drobl);
  free(N_of_SmeshsDrobl);
  for(i=0;i<N_of_Number;i++)
  {
    if(SmeshsDrobl[i])free(SmeshsDrobl[i]);
    if(SmeshsDrobl2[i])free(SmeshsDrobl2[i]);
  }
  free(SmeshsDrobl);
  free(SmeshsDrobl2);


  Strings->Clear();
  Strings->LoadFromFile(TempFile);
  DeleteFile(TempFile);
  */
  return 1;
}

//������ ����������� �����������
int OBZORY_1::FindVeroyatnostObjedinenie(long *N_of_P_S,
                                 long *N_of_PS)
{
  long TekSmesh,Smesh1;
  long Nomer,  //����� �������
       Indx;   //������ �� �������
  long Nomer1,  //����� �������
       Indx1;   //������ �� �������

  bool NaidP_S;

  if(!IsWasPrivyazka||!IsInPlot)       //��������, ����� ���� �������� ������, ����� ��� �������� �����
  {
    return 0;
  }


  *N_of_P_S=0;
  *N_of_PS=0;

//������� �� ������ ������� ����� ���������� ���������
  TekSmesh=FirstVisual;

//���� ������ �� P ��������
  while(TekSmesh>=0)
  {
    Nomer=TekSmesh%362; Indx=TekSmesh/362;
//�������� �� ��������� �������, ���� ��
    if((o_b_1[Nomer][Indx].Status&3)!=1)
    {
       TekSmesh=o_b_1[Nomer][Indx].NextVisual;
       continue;
    }

//��������, ���������� �� ���������� � S �������
   NaidP_S=false;
   Smesh1=NextOtschetBort(Nomer,Indx,false,true,false);
   if(Smesh1>=0)
   {
      Nomer1=Smesh1%362; Indx1=Smesh1/362;
      if(ABS(o_b_1[Nomer1][Indx1].SektorNomer-o_b_1[Nomer][Indx].SektorNomer)<=1)
      {
        NaidP_S=true;
      }
   }
   if(!NaidP_S)
   {
     Smesh1=PrevOtschetBort(Nomer,Indx,false,true,false);
     if(Smesh1>=0)
     {
      Nomer1=Smesh1%362; Indx1=Smesh1/362;
      if(ABS(o_b_1[Nomer1][Indx1].SektorNomer-o_b_1[Nomer][Indx].SektorNomer)<=1)
      {
        NaidP_S=true;
      }
     }
   }

   if(NaidP_S) //��� ������ ������ ���������� ������ � ����� ������
   {
      (*N_of_P_S)++;
      NaidP_S=false;
      Smesh1=NextOtschetBort(Nomer,Indx,false,false,true);
      if(Smesh1>=0)
      {
         Nomer1=Smesh1%362; Indx1=Smesh1/362;
         if(ABS(o_b_1[Nomer1][Indx1].SektorNomer-o_b_1[Nomer][Indx].SektorNomer)<=1)
         {
           NaidP_S=true;
         }
      }
      if(!NaidP_S)
      {
        Smesh1=PrevOtschetBort(Nomer,Indx,false,false,true);
        if(Smesh1>=0)
        {
         Nomer1=Smesh1%362; Indx1=Smesh1/362;
         if(ABS(o_b_1[Nomer1][Indx1].SektorNomer-o_b_1[Nomer][Indx].SektorNomer)<=1)
         {
           NaidP_S=true;
         }
        }
      }
      if(NaidP_S)
      {
        (*N_of_PS)++;
      }
   }
   TekSmesh=o_b_1[Nomer][Indx].NextVisual;

  };



  return 1;
}





int OBZORY_1::FindBlizkieForP(void)
{
  long i;
  long k,TekSektor;
  long Indexes[362];
  OCHERED_O1 OO1(RSA_P2,RSD_P2);

  for(i=0;i<362;i++)
  {
    Indexes[i]=0;
  }


//����������� ������ ��� ������� ��������
  FreeBlizkieForP();


  TekSektor=0;
//� ������ ������� ����� � ������
  for(k=0;k<=LastNomerObzora;k++)
  {
  //������������

     for(i=0;i<90;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
         o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {
           if((o_b_1[i][Indexes[i]].Status&3)==1||
            ((o_b_1[i][Indexes[i]].Status&3)==3&&
            o_b_1[i][Indexes[i]].ParnOtschet1<0&&
            o_b_1[i][Indexes[i]].ParnOtschet2<0))
           {
               OO1.AddP(o_b_1[i]+Indexes[i], &POOB1_p, &N_of_POOB1_p);
           }
           Indexes[i]++;
       }
     }

     TekSektor++;
     for(i=90;i<180;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
         o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {
           if((o_b_1[i][Indexes[i]].Status&3)==1||
            ((o_b_1[i][Indexes[i]].Status&3)==3&&
            o_b_1[i][Indexes[i]].ParnOtschet1<0&&
            o_b_1[i][Indexes[i]].ParnOtschet2<0))
           {
               OO1.AddP(o_b_1[i]+Indexes[i], &POOB1_p, &N_of_POOB1_p);
           }
           Indexes[i]++;
       }
     }

     TekSektor++;
     for(i=180;i<270;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
          o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {

            if((o_b_1[i][Indexes[i]].Status&3)==1||
            ((o_b_1[i][Indexes[i]].Status&3)==3&&
            o_b_1[i][Indexes[i]].ParnOtschet1<0&&
            o_b_1[i][Indexes[i]].ParnOtschet2<0))
           {
               OO1.AddP(o_b_1[i]+Indexes[i], &POOB1_p, &N_of_POOB1_p);
           }
           Indexes[i]++;
       }
     }

     TekSektor++;
     for(i=270;i<360;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
          o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {
            if((o_b_1[i][Indexes[i]].Status&3)==1||
            ((o_b_1[i][Indexes[i]].Status&3)==3&&
            o_b_1[i][Indexes[i]].ParnOtschet1<0&&
            o_b_1[i][Indexes[i]].ParnOtschet2<0))
           {
               OO1.AddP(o_b_1[i]+Indexes[i], &POOB1_p, &N_of_POOB1_p);
           }
           Indexes[i]++;
       }
     }
     TekSektor++;
  }
  IsWasSearchingRS_P=true;
  return 1;
}



int OBZORY_1::FreeBlizkieForP(void)
{
  long i;
  for(i=0;i<N_of_POOB1_p;i++)
  {
    POOB1_p[i].OB1_1->Status&=~BLIZKO_STATUS;
    POOB1_p[i].OB1_2->Status&=~BLIZKO_STATUS;
  }

  if(POOB1_p)
  {
    free(POOB1_p);
    N_of_POOB1_p=0;
    POOB1_p=NULL;
  }
  IsWasSearchingRS_P=false;
  return 1;
}

int OBZORY_1::VklBlzkieBortaP(void)
{
  long i;
  if(!IsWasSearchingRS_P||!IsCheckNumber)return 0;
  for(i=0;i<n_of_sort_otis;i++)
  {
      sort_otis[i]->isSelected=0;
  }
  for(i=0;i<N_of_POOB1_p;i++)
  {
    otis[POOB1_p[i].OB1_1->IndxBrt].isSelected=1;
    otis[POOB1_p[i].OB1_2->IndxBrt].isSelected=1;
  }
  return 1;
}

 //����� �������� ���������� ������ � ���������� ��������
int OBZORY_1::VyvodVydelRS_P(TStrings *Strings, AnsiString *FN)
{
//������� ��� ��� ������ � ����
  long i;
  AnsiString TempFile=GetTempFileInTempPath();
  AnsiString StrkString;
  FILE *fpVrem;
  double dA,dD;
  if(!IsWasSearchingRS_P)return 0;

  fpVrem=fopen(TempFile.c_str(),"w");
  if(!fpVrem)
  {
    return 0;
  }
  fprintf(fpVrem,"Primary channel plots that are in close proximity\n");
  fprintf(fpVrem,"Maximum azimuth difference %.02lf �\n",RSA_P2);
  fprintf(fpVrem,"Maximum range difference %.0lf m\n\n",RSD_P2*1000);

    if(FIO_User.Length())
   {
     StrkString=String("Made: ")+FIO_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }
   if(Podr_User.Length())
   {
     StrkString=String("Department: ")+Podr_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }

   StrkString=String("Date: ")+Date_Input;
   fprintf(fpVrem,StrkString.c_str());
   fprintf(fpVrem,"\n");

   if(Dop_Info.Length())
   {
      fprintf(fpVrem,Dop_Info.c_str());
      fprintf(fpVrem,"\n");
   }

//������� ������ �� ������
   if(FN)
   {
     fprintf(fpVrem,"Files have been examined: ");
     fprintf(fpVrem,"\n");
     for(i=0;i<N_of_fps;i++)
     {
       fprintf(fpVrem,"%s\n",FN[i].c_str());
     }
   }
   fprintf(fpVrem,"\n\n");

//������� ������� ����. ������
   for(i=0;i<N_of_POOB1_p;i++)
   {
//��������, ����� ��� ������� ���� ������ � � ���������� �������
     if((POOB1_p[i].OB1_1->Status&VISIBLE_STATUS)&&
        (POOB1_p[i].OB1_1->Status&V_OBLASTI_VYDELENIA_STATUS)&&
        (POOB1_p[i].OB1_2->Status&VISIBLE_STATUS)&&
        (POOB1_p[i].OB1_2->Status&V_OBLASTI_VYDELENIA_STATUS))
     {
       dA=ABS(POOB1_p[i].OB1_1->Azmt-POOB1_p[i].OB1_2->Azmt);
       if(dA>180)dA=360-dA;
       dD=ABS(POOB1_p[i].OB1_1->Dlnst-POOB1_p[i].OB1_2->Dlnst);
         fprintf(fpVrem,"Azimuth difference: %.02lf �. Range difference: %.0lf m.\n",
         dA,dD*1000);

//������� �������
       fputs(GetStrokaFrom2(POOB1_p[i].OB1_1).c_str(),fpVrem);
       fputs(GetStrokaFrom2(POOB1_p[i].OB1_2).c_str(),fpVrem);
       fprintf(fpVrem,"\n\n");
     }
   }

  fclose(fpVrem);
  Strings->Clear();
  Strings->LoadFromFile(TempFile);
  DeleteFile(TempFile);
  return 1;
}



int OBZORY_1::FindBlizkieForVK(void)
{
  long i;
  long k,TekSektor;
  long Indexes[362];
  OCHERED_O1 OO1uvd(RSA_UVD2,RSD_UVD2);
  OCHERED_O1 OO1rbs(RSA_RBS2,RSD_RBS2);


  for(i=0;i<362;i++)
  {
    Indexes[i]=0;
  }


//����������� ������ ��� ������� ��������
  FreeBlizkieForVK();

  if(is180)
  {
        TekSektor=-2;
  }else{
        TekSektor=0;
  }      

//� ������ ������� ����� � ������
  for(k=0;k<=LastNomerObzora;k++)
  {
  //������������

     for(i=0;i<90;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
         o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {
           if(IsWasPrivyazka&&IsInPlot)  //������� �������� ��� �������� �����
           {
     //��������� ������ ��, ������� ����� ��� ������ S
              if((o_b_1[i][Indexes[i]].Status&3)==2)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }else{
     //������� ��� ���� S ��� PS
              if(o_b_1[i][Indexes[i]].Status&KANAL_S_STATUS)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }
           Indexes[i]++;
       }
     }

     TekSektor++;
     for(i=90;i<180;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
         o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {
           if(IsWasPrivyazka&&IsInPlot)  //������� �������� ��� �������� �����
           {
     //��������� ������ ��, ������� ����� ��� ������ S
              if((o_b_1[i][Indexes[i]].Status&3)==2)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }else{
     //������� ��� ���� S ��� PS
              if(o_b_1[i][Indexes[i]].Status&KANAL_S_STATUS)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }
           Indexes[i]++;

       }
     }

     TekSektor++;
     for(i=180;i<270;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
          o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {

           if(IsWasPrivyazka&&IsInPlot)  //������� �������� ��� �������� �����
           {
     //��������� ������ ��, ������� ����� ��� ������ S
              if((o_b_1[i][Indexes[i]].Status&3)==2)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }else{
     //������� ��� ���� S ��� PS
              if(o_b_1[i][Indexes[i]].Status&KANAL_S_STATUS)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }
           Indexes[i]++;
       }
     }

     TekSektor++;
     for(i=270;i<360;i++)
     {
       while(Indexes[i]<N_of_o_b_1[i]&&
          o_b_1[i][Indexes[i]].SektorNomer==TekSektor)
       {
           if(IsWasPrivyazka&&IsInPlot)  //������� �������� ��� �������� �����
           {
     //��������� ������ ��, ������� ����� ��� ������ S
              if((o_b_1[i][Indexes[i]].Status&3)==2)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }else{
     //������� ��� ���� S ��� PS
              if(o_b_1[i][Indexes[i]].Status&KANAL_S_STATUS)
              {
                if(o_b_1[i][Indexes[i]].Status&REJIM_UVD_STATUS)
                {
                  OO1uvd.AddS(o_b_1[i]+Indexes[i],&POOB1_uvd,&N_of_POOB1_uvd);
                }else if(o_b_1[i][Indexes[i]].Status&REJIM_RBS_STATUS)
                {
                  OO1rbs.AddS(o_b_1[i]+Indexes[i],&POOB1_rbs,&N_of_POOB1_rbs);
                }
              }
           }
           Indexes[i]++;
       }
     }
     TekSektor++;
  }
  IsWasSearchingRS_VK=true;
  return 1;
}



int OBZORY_1::FreeBlizkieForVK(void)
{
  int i;
  for(i=0;i<N_of_POOB1_uvd;i++)
  {
    POOB1_uvd[i].OB1_1->Status&=~BLIZKO_STATUS;
    POOB1_uvd[i].OB1_2->Status&=~BLIZKO_STATUS;
  }


  if(POOB1_uvd)
  {
    free(POOB1_uvd);
    N_of_POOB1_uvd=0;
    POOB1_uvd=NULL;
  }

  for(i=0;i<N_of_POOB1_rbs;i++)
  {
    POOB1_rbs[i].OB1_1->Status&=~BLIZKO_STATUS;
    POOB1_rbs[i].OB1_2->Status&=~BLIZKO_STATUS;
  }

  if(POOB1_rbs)
  {
    free(POOB1_rbs);
    N_of_POOB1_rbs=0;
    POOB1_rbs=NULL;
  }



  IsWasSearchingRS_VK=false;
  return 1;
}



int OBZORY_1::VklBlzkieBortaUVD(void)
{
  long i;
  if(!IsWasSearchingRS_VK||!IsCheckNumber)return 0;
  for(i=0;i<n_of_sort_otis;i++)
  {
         sort_otis[i]->isSelected=0;
  }

  for(i=0;i<N_of_POOB1_uvd;i++)
  {
    otis[POOB1_uvd[i].OB1_1->IndxBrt].isSelected=1;
    otis[POOB1_uvd[i].OB1_2->IndxBrt].isSelected=1;
  }
  return 1;
}


int OBZORY_1::VklBlzkieBortaRBS(void)
{
  long i;
  if(!IsWasSearchingRS_VK||!IsCheckNumber)return 0;
  for(i=0;i<n_of_sort_otis;i++)
  {
      sort_otis[i]->isSelected=0;
  }

  for(i=0;i<N_of_POOB1_rbs;i++)
  {
    otis[POOB1_rbs[i].OB1_1->IndxBrt].isSelected=1;
    otis[POOB1_rbs[i].OB1_2->IndxBrt].isSelected=1;
  }
  return 1;
}



 //����� �������� ���������� ������ � ���������� ��������
int OBZORY_1::VyvodVydelRS_UVD(TStrings *Strings, AnsiString *FN)
{
//������� ��� ��� ������ � ����
  long i;
  AnsiString TempFile=GetTempFileInTempPath();
  AnsiString StrkString;
  FILE *fpVrem;
  double dA,dD;
  if(!IsWasSearchingRS_VK)return 0;

  fpVrem=fopen(TempFile.c_str(),"w");
  if(!fpVrem)
  {
    return 0;
  }
  fprintf(fpVrem,"Plots in close proximity. UVD .\n");
  fprintf(fpVrem,"Maximum azimuth difference: %.02lf �\n",RSA_UVD2);
  fprintf(fpVrem,"Maximum range difference: %.0lf m\n\n",RSD_UVD2*1000);

    if(FIO_User.Length())
   {
     StrkString=String("Made: ")+FIO_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }
   if(Podr_User.Length())
   {
     StrkString=String("Department: ")+Podr_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }

   StrkString=String("Date: ")+Date_Input;
   fprintf(fpVrem,StrkString.c_str());
   fprintf(fpVrem,"\n");

   if(Dop_Info.Length())
   {
      fprintf(fpVrem,Dop_Info.c_str());
      fprintf(fpVrem,"\n");
   }

//������� ������ �� ������
   if(FN)
   {
     fprintf(fpVrem,"Files have been examined:");
     fprintf(fpVrem,"\n");
     for(i=0;i<N_of_fps;i++)
     {
       fprintf(fpVrem,"%s\n",FN[i].c_str());
     }
   }
   fprintf(fpVrem,"\n\n");

//������� ������� ����. ������
   for(i=0;i<N_of_POOB1_uvd;i++)
   {
//��������, ����� ��� ������� ���� ������ � � ���������� �������
     if((POOB1_uvd[i].OB1_1->Status&VISIBLE_STATUS)&&
        (POOB1_uvd[i].OB1_1->Status&V_OBLASTI_VYDELENIA_STATUS)&&
        (POOB1_uvd[i].OB1_2->Status&VISIBLE_STATUS)&&
        (POOB1_uvd[i].OB1_2->Status&V_OBLASTI_VYDELENIA_STATUS))
     {
       dA=ABS(POOB1_uvd[i].OB1_1->Azmt-POOB1_uvd[i].OB1_2->Azmt);
       if(dA>180)dA=360-dA;
       dD=ABS(POOB1_uvd[i].OB1_1->Dlnst-POOB1_uvd[i].OB1_2->Dlnst);
       fprintf(fpVrem,"Azimuth difference: %.02lf �. Range difference: %.0lf m.\n",
         dA,dD*1000);

//������� �������
       fputs(GetStrokaFrom2(POOB1_uvd[i].OB1_1).c_str(),fpVrem);
       fputs(GetStrokaFrom2(POOB1_uvd[i].OB1_2).c_str(),fpVrem);
       fprintf(fpVrem,"\n\n");
     }
   }

  fclose(fpVrem);
  Strings->Clear();
  Strings->LoadFromFile(TempFile);
  DeleteFile(TempFile);
  return 1;
}

 //����� �������� ���������� (RBS) ������ � ���������� ��������
int OBZORY_1::VyvodVydelRS_RBS(TStrings *Strings, AnsiString *FN)
{
//������� ��� ��� ������ � ����
  long i;
  AnsiString TempFile=GetTempFileInTempPath();
  AnsiString StrkString;
  FILE *fpVrem;
  double dA,dD;
  if(!IsWasSearchingRS_VK)return 0;

  fpVrem=fopen(TempFile.c_str(),"w");
  if(!fpVrem)
  {
    return 0;
  }

  fprintf(fpVrem,"Plots in close proximity. RBS.\n");
  fprintf(fpVrem,"Maximum azimuth difference: %.02lf �\n",RSA_RBS2);
  fprintf(fpVrem,"Maximum range difference: %.0lf m\n\n",RSD_RBS2*1000);

    if(FIO_User.Length())
   {
     StrkString=String("Made: ")+FIO_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }
   if(Podr_User.Length())
   {
     StrkString=String("Department: ")+Podr_User;
     fprintf(fpVrem,StrkString.c_str());
     fprintf(fpVrem,"\n");
   }

   StrkString=String("Date: ")+Date_Input;
   fprintf(fpVrem,StrkString.c_str());
   fprintf(fpVrem,"\n");

   if(Dop_Info.Length())
   {
      fprintf(fpVrem,Dop_Info.c_str());
      fprintf(fpVrem,"\n");
   }

//������� ������ �� ������
   if(FN)
   {
     fprintf(fpVrem,"Files have been examined:");
     fprintf(fpVrem,"\n");
     for(i=0;i<N_of_fps;i++)
     {
       fprintf(fpVrem,"%s\n",FN[i].c_str());
     }
   }
   fprintf(fpVrem,"\n\n");

//������� ������� ����. ������
   for(i=0;i<N_of_POOB1_rbs;i++)
   {
//��������, ����� ��� ������� ���� ������ � � ���������� �������
     if((POOB1_rbs[i].OB1_1->Status&VISIBLE_STATUS)&&
        (POOB1_rbs[i].OB1_1->Status&V_OBLASTI_VYDELENIA_STATUS)&&
        (POOB1_rbs[i].OB1_2->Status&VISIBLE_STATUS)&&
        (POOB1_rbs[i].OB1_2->Status&V_OBLASTI_VYDELENIA_STATUS))
     {
       dA=ABS(POOB1_rbs[i].OB1_1->Azmt-POOB1_rbs[i].OB1_2->Azmt);
       if(dA>180)dA=360-dA;
       dD=ABS(POOB1_rbs[i].OB1_1->Dlnst-POOB1_rbs[i].OB1_2->Dlnst);
       fprintf(fpVrem,"Azimuth difference: %.02lf �. Range difference: %.0lf m.\n",
         dA,dD*1000);

//������� �������
       fputs(GetStrokaFrom2(POOB1_rbs[i].OB1_1).c_str(),fpVrem);
       fputs(GetStrokaFrom2(POOB1_rbs[i].OB1_2).c_str(),fpVrem);
       fprintf(fpVrem,"\n\n");
     }
   }

  fclose(fpVrem);
  Strings->Clear();
  Strings->LoadFromFile(TempFile);
  DeleteFile(TempFile);
  return 1;
}

