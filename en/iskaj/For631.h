//---------------------------------------------------------------------------
#ifndef For631H
#define For631H
#include <alloc.h>
//---------------------------------------------------------------------------
/*�����: ��������� �.�.

  � ������ For631
 ����������� ������ ��� ������ ��������� ��� ������������� ��������������
 ���������� ���������� ������ �� ������ ��� ������ �����. � ����� ��������
 � ����� ��������� �������� �������� ���������� ������.

*/



/*�����: ODIN_OTSCHET631
 �������� �������� ������ �������. ������� ��������� ODIN_OTSCHET631 ������
 � ��������� OTSCHETS631
*/
struct ODIN_OTSCHET631
{
  long Smesh;      //�������� � �����
  long Nomer;      //����� �����, ������� �����������
  int  IndexNomer; //������ ������ �����
  int Vysota;      //������ �����
  unsigned short EVysota;    //�������������� ������
  short Zona;       //���� �����

                   //1 - ���� ������������ � ������
                   //2 - ���� ������ ������ � ������ �� ������� �� MaxRast
                   //3 - ���� ������ ����� � ������ �� ������� �� �� MinRast �� MaxRast
                   //4 - ���� ����� ����� ��� MinRast
                   //�� -  �� ����������
  bool IsklOtch;       //������� ������ ������, ������ ���������������
  bool IsFalseVysota;  //������������ ������
  bool IsNotTraekt;    //������ ������ ���������������� �� ����������� ����������
  short KudaRLS;       //-1 - �� ���, 1 - �� ���, 0 - ���� �� ����������




  double Time;    //����� � �, ������ � ��������, ��������� � ��
  float Azmt, Dlnst;   //������������ �������� ������� � ���������
//  float EAzmt, EDlnst; //����������������� �������� ������� � ���������
//  long BlijObject;       //��������� ������ �����



/*��������� ��������� ��� ����������� ������*/
  long Next;         //��������� ������
  long Prev;         //����������

/*��������� ������� ��� ����������� ������*/
  long GlobNext;         //��������� ������
  long GlobPrev;         //����������

};

//���� �����
struct ODIN_SEVER631
{
  long Smesh;      //�������� � �����

  long GlobNext;         //��������� ������
  long GlobPrev;         //����������

};




//������ � ���������� ������ �����
struct ISKAJENIA631
{
  struct OTSCHETS631 * Otkuda;
  int IndexNomer; //������ ����� � �������� ��� ���������
  int N_of_O;     //����� �������� ����� ������
  int N_of_O1;    //����� �������� � ���� ���� � ������
  int N_of_O2;    //����� �������� � ���� ���� � ������
  int N_of_O3;    //����� �������� � ���� ���� � ������
  int N_of_O4;    //����� �������� � ���� ���� � ������
  int N_of_O1_2;

  int N_of_PO1;    //����� ���������� �������� � ���� ���� � ������
  int N_of_PO2;    //����� ��. �������� � ���� ��� � ������
  int N_of_PO3;    //����� ��. �������� � ���� ���� � ������
  int N_of_PO4;    //����� ��. ��������� � ���� ���� � ������
  int N_of_PO1_2;

  int N_of_NotNull1;    //����� �������� �� ������� � ���� ���� � ������
  int N_of_NotNull2;    //����� ��. �������� � ���� ��� � ������
  int N_of_NotNull3;    //����� ��. �������� � ���� ���� � ������
  int N_of_NotNull4;    //����� ��. ��������� � ���� ���� � ������
  int N_of_NotNull1_2;


  int N_of_V1;    //����� ��������� �� ������
  int N_of_V2;    //����� ��������� �� ������
  int N_of_V3;    //����� ��������� �� ������
  int N_of_V4;    //����� ��������� �� ������
  int N_of_V1_2;

  int N_of_N1;    //����� ��������� �� ������
  int N_of_N2;    //����� ��������� �� ������
  int N_of_N3;    //����� ��������� �� ������
  int N_of_N4;    //����� ��������� �� ������
  int N_of_N1_2;

  int N_of_Nul1;    //����� ������� �����
  int N_of_Nul2;    //����� ������� �����
  int N_of_Nul3;    //����� ������� �����
  int N_of_Nul4;    //����� ������� �����
  int N_of_Nul1_2;

  int N_of_NulN1;  //����� ������� �������
  int N_of_NulN2;  //����� ������� �������
  int N_of_NulN3;  //����� ������� �������
  int N_of_NulN4;  //����� ������� �������
  int N_of_NulN1_2;



  double P1;         //����������� ����, ��� ������ ���������� �� ������
  double P2;
  double P3;
  double P4;
  double P1_2;

  double PN1;         //����������� ����, ��� ������ ���������� �� ������
  double PN2;
  double PN3;
  double PN4;
  double PN1_2;


  double PI1;   //����������� ���������
  double PI2;   //����������� ���������
  double PI3;   //����������� ���������
  double PI4;   //����������� ���������
  double PI1_2; //����������� ���������




  int ReturnN_of_O(int Z)
  {
    switch(Z)
    {
      case 1: return N_of_O1;
      case 2: return N_of_O2;
      case 3: return N_of_O3;
      case 4: return N_of_O4;
    };
    return (N_of_O1_2);
  };

  int ReturnN_of_PO(int Z)
  {
    switch(Z)
    {
      case 1: return N_of_PO1;
      case 2: return N_of_PO2;
      case 3: return N_of_PO3;
      case 4: return N_of_PO4;
    };
    return (N_of_PO1_2);
  };

  int ReturnN_of_V(int Z)
  {
    switch(Z)
    {
      case 1: return N_of_V1;
      case 2: return N_of_V2;
      case 3: return N_of_V3;
      case 4: return N_of_V4;
    };
    return (N_of_V1_2);
  };

  int ReturnN_of_N(int Z)
  {
    switch(Z)
    {
      case 1: return N_of_N1;
      case 2: return N_of_N2;
      case 3: return N_of_N3;
      case 4: return N_of_N4;
    };
    return (N_of_N1_2);
  };

  int ReturnN_of_Nul(int Z)
  {
    switch(Z)
    {
      case 1: return N_of_Nul1;
      case 2: return N_of_Nul2;
      case 3: return N_of_Nul3;
      case 4: return N_of_Nul4;
    };
    return (N_of_Nul1_2);
  };


  int ReturnN_of_NulN(int Z)
  {
    switch(Z)
    {
      case 1: return N_of_NulN1;
      case 2: return N_of_NulN2;
      case 3: return N_of_NulN3;
      case 4: return N_of_NulN4;
    };
    return (N_of_NulN1_2);
 };

  int ReturnN_of_NotNull(int Z)
  {
    switch(Z)
    {
      case 1: return N_of_NotNull1;
      case 2: return N_of_NotNull2;
      case 3: return N_of_NotNull3;
      case 4: return N_of_NotNull4;
    };
    return (N_of_NotNull1_2);
 };

  double ReturnP(int Z)
  {
    switch(Z)
    {
      case 1: return P1;
      case 2: return P2;
      case 3: return P3;
      case 4: return P4;
    };
    return P1_2;
  }

  double ReturnPN(int Z)
  {
    switch(Z)
    {
      case 1: return PN1;
      case 2: return PN2;
      case 3: return PN3;
      case 4: return PN4;
    };
    return PN1_2;
  }


  double ReturnPI(int Z)
  {
    switch(Z)
    {
      case 1: return PI1;
      case 2: return PI2;
      case 3: return PI3;
      case 4: return PI4;
    };
    return PI1_2;
  }



};

/*��� ������� ���������� �����*/
//������ ���������� �����
struct OTCHET_ISKAJ_VYSOT
{
  int Ind1;    //���������� ������
  int Ind2;    //��� ���� ������ ��������� �� ������, ������� ������������� �� ���������...
  int Ind3;    //��������� �� ������� ������
  int Ind4;
};

struct MASSIV_ISKAJ_VYSOT
{
  OTCHET_ISKAJ_VYSOT *OIV;
  int N_of_OIV;
  int MaxN_of_OIV;
  MASSIV_ISKAJ_VYSOT()
  {
    OIV=NULL;
    N_of_OIV=0;
    MaxN_of_OIV=0;
  };
  ~MASSIV_ISKAJ_VYSOT()
  {
    FreeAll();
  }
  int Add(int I1, int I2, int I3, int I4) //�������� �������
  {
    int i;
    for(i=0;i<N_of_OIV;i++)
    {
      if(OIV[i].Ind2==I2)return 0;
      if(OIV[i].Ind2>I2)
      {
      //������� �������
        if(N_of_OIV==MaxN_of_OIV)
        {
          MaxN_of_OIV+=100;
          if(OIV)
          {
                OIV=(OTCHET_ISKAJ_VYSOT *)realloc(OIV,MaxN_of_OIV*
                                                sizeof(OTCHET_ISKAJ_VYSOT));
           }else{
                OIV=(OTCHET_ISKAJ_VYSOT *)malloc(MaxN_of_OIV*
                                                sizeof(OTCHET_ISKAJ_VYSOT));
           }
        }
        memmove(&(OIV[i+1]),&(OIV[i]),sizeof(OTCHET_ISKAJ_VYSOT)*(N_of_OIV-i));
        OIV[i].Ind1=I1; OIV[i].Ind2=I2; OIV[i].Ind3=I3; OIV[i].Ind4=I4;
        N_of_OIV++;
        return 1;
      }
    }
 //��������� ������� � �����
    if(N_of_OIV==MaxN_of_OIV)
    {
          MaxN_of_OIV+=100;
          if(OIV)
          {
                OIV=(OTCHET_ISKAJ_VYSOT *)realloc(OIV,MaxN_of_OIV*
                                              sizeof(OTCHET_ISKAJ_VYSOT));
          }else{
                 OIV=(OTCHET_ISKAJ_VYSOT *)malloc(MaxN_of_OIV*
                                              sizeof(OTCHET_ISKAJ_VYSOT));
          }
    }

    OIV[N_of_OIV].Ind1=I1; OIV[N_of_OIV].Ind2=I2;
    OIV[N_of_OIV].Ind3=I3; OIV[N_of_OIV].Ind4=I4;
    N_of_OIV++;
    return 1;
  }

  int Delete(int I2)                      //�������
  {
    int i;
    for(i=0;i<N_of_OIV;i++)
    {
      if(OIV[i].Ind2==I2)
      {
        DeleteIndex(i);
        return 1;
      }
    }
    return 0;
  }
  int DeleteIndex(int I)                      //�������
  {
    if(I<0||I>=N_of_OIV)
    {
      return 0;
    }
    if(I<N_of_OIV-1)
    {
      memmove(&(OIV[I]),&(OIV[I+1]),sizeof(OTCHET_ISKAJ_VYSOT)*(N_of_OIV-I-1));
    }
    N_of_OIV--;
    if(N_of_OIV==0)
    {
      FreeAll();
    }
    return 1;
  }
  void FreeAll(void)
  {
     if(OIV)
     {
        free(OIV);
        OIV=NULL;
     }
     N_of_OIV=0;
     MaxN_of_OIV=0;
  }

};




/*���� �������� - ��� ������� ���������� ������ ���� S ��� PS
  ��������� OTSCHETS631 ��������� ����������, �� ���� ������� ����������� � ������ �����

*/
struct OTSCHETS631
{
  char *FileName;      //����
  bool CS;             //�������� ���� ��� �����
  double TempObzora;   //���� ������ � �����

  double Strob;        //����� � ��������
  double MinDD,MaxDD;

  long MaxPerepadVysoty;  //���������� ������� ������

//��� ������ ��������� ��� ������������� ���.����
  double A11, A12, A21, A22;  //������� ��������
  bool IsA1, IsA2;
  double MinDlnst, MaxDlnst;  //��������� �������� ����������
  bool IsDlnst;
  long Vysota1, Vysota2;      //������� ������
  bool IsVysota;
  int MinBN_of_Otsch;      //����������� ���������� ����� �������� ��� ������ ���������


  ODIN_OTSCHET631 *OO;   //������ ��������
  int N_of_OO;          //�� tc��� ��������
  int MaxN_of_OO;       //������������ ����� �������� - ��� ��� ������������ ������
  int N_of_ExistingOO;  //����� ������������ �������� - ��� ��������


   ODIN_SEVER631 *OS;   //������ �������
   int N_of_OS;         //����� �������
   int MaxN_of_OS;      //������������ ����� �������

   
  long *NomBort;        //����� ����� ������ -1, ��� �������, ������� ��������� ��
  long *FIBort;
  long *LIBort;
  bool *IsklBort;       //��������� ����

  bool *IsklBortPercV1;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercV2;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercV3;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercV4;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercV1_2;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercN1;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercN2;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercN3;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercN4;  //��������� ����, ��� ��� ��� ����������� ��������� ������
  bool *IsklBortPercN1_2;  //��������� ����, ��� ��� ��� ����������� ��������� ������





  int N_of_Borts;


//��� ���������
  MASSIV_ISKAJ_VYSOT MIV;


//��� �������� ������������
  ISKAJENIA631 *IskForNotFilter;
  ISKAJENIA631 *IskForWithFilter;
  ISKAJENIA631 ObshRezultNotFilter;
  ISKAJENIA631 ObshRezultWithFilterV;
  ISKAJENIA631 ObshRezultWithFilterN;


  int *IndexVys;       //�������������� ������ ������� �� ������
  int N_of_IndexVys;

  struct ONE_S_BORT_631 *OSB;  //������ ��� ������ ��� ���������� �������� � ������


//������, ����������� ��� ������� ����������
  double MinV1,MaxV1;       //����������� � ������������ ��������


//������ ������ � ���������� ������
  int GlobalF,GlobalL;


//�������������������� ������
  double SKO_A;   //��� ������� � ��������
  double SKO_A_dop; //���������������� SKO ������� � ��������
  double SKO_D;   //��� ��������� � ��

//������ �������������� ������ ���������
  double SKO_Usk_X; //������ ��������� �� X � ��/c^2
  double SKO_Usk_Y; //������ ��������� �� Y � ��/c^2

//�������� ������ ��� �������
  double MaxPsi;


//�������� ��� ��������� ����������� ��������
  double TimePropusk;         //����� ������� ����� ���������
  int    MinN_of_Otch;        //����� 1, 2 ��� 3


  OTSCHETS631()
  {
    FileName=NULL;
    Strob=MinDD=MaxDD=0;
    MaxN_of_OO=N_of_OO=N_of_ExistingOO=N_of_OS=MaxN_of_OS=0;

    NomBort=FIBort=LIBort=NULL;
    OO=NULL;
    OS=NULL;
    IsklBort=IsklBortPercV1=IsklBortPercV2=IsklBortPercV3=
       IsklBortPercV4=IsklBortPercV1_2=
       IsklBortPercN1=IsklBortPercN2=IsklBortPercN3=
       IsklBortPercN4=IsklBortPercN1_2=NULL;
    IndexVys=NULL;
    IskForNotFilter=IskForWithFilter=NULL;
    N_of_IndexVys=0;
    OSB=NULL;
  
  
  
    GlobalF=-1;
    GlobalL=-1;

//������ �������� ���� ������ ����� ��� ��������� �����
    N_of_Borts=1;

    NomBort=(long*)malloc(sizeof(long)*(N_of_Borts));
    FIBort=(long*)malloc(sizeof(long)*(N_of_Borts));
    LIBort=(long*)malloc(sizeof(long)*(N_of_Borts));
    IsklBort=(bool*)malloc(sizeof(bool)*(N_of_Borts));
    NomBort[0]=0;
    FIBort[0]=-1;
    LIBort[0]=-1;
    IsklBort[0]=true;



  };

  void FreeAll(void);


  ~OTSCHETS631()
  {
     FreeAll();
  };

  void SdelatVseBortaIsklKromeMasiva(long *NBs,  //������ ������
                                     int N_of_NBs);






  void AddOtschet(long Smesh,
                 long Nomer,
                 long Vysota,
                 double Azmt,
                 double Dlnst,
                 double Time,
                 bool Iskl,
                 long IstNomer,
                 long IstVysota);





//����� ������ ������� - ��������� ������ ��� ��������� ������ �� ������ �����
  int InsertGlobOtschet(int Indx,         //������ ����� �������� ���� �������� ������ ������
                   long Smesh,
                   long Nomer,
                   long Vysota,
                   double Azmt,
                   double Dlnst,
                   double Time,
                   bool Iskl);

//����� ������ ������� - ��������� ������ ��� ��������� ������ �� ������ �����
  int InsertOtschet(int Indx,         //������ ����� �������� ���� �������� ������ ������
                   long Smesh,
                   long Nomer,
                   long Vysota,
                   double Azmt,
                   double Dlnst,
                   double Time,
                   bool Iskl,
                   bool IsPropusk);


//������������ ��������� ������ �� ������, ���������, ���������� �������
//����������: 1 ���� ������ ������� � ������������� ��������� �������
//����������: 101, ���� ������ ��������, �� �� ������� � ������ ������������� ���������
  int IsSootvetOption(int Indx,           //���������� ������
                   bool NullVysotaVkl);    //�������� ������� ������





  int ChisloOtschetov(int IndexNomer,  //����� �����
                      bool VklIskl,     //�������� �� ����������� �����
                      bool VklOptOtsch  //��������� ������������ ������
                     );



/*���������� ��������� �������� ������������ �������� ������ �������*/
  void OtnosKZone(void);



//������ ������
  int FirstOtschet(int IndexNomerB,
                   bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                   bool IsNotNullVysOnly,  //������ � ��������� �������
                   bool IsExistingOnly,
                   bool IsOptionOnly);

//������ ������
  int LastOtschet( int IndexNomerB,
                   bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                   bool IsNotNullVysOnly,  //������ � ��������� �������
                   bool IsExistingOnly,
                   bool IsOptionOnly);


//�������� ��������� ������
  int NextOtschet(int  IndexOtsch,
           bool IsNotIsklOnly,     //������ �� ������������ �� �����������
           bool IsNotNullVysOnly,  //������ � ��������� �������
           bool IsExistingOnly,
           bool IsOptionOnly);

//���������� ������
  int PrevOtschet(int  IndsexOtsch,
                  bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                  bool IsNotNullVysOnly,  //������ � ��������� �������
                  bool IsExistingOnly,
                  bool IsOptionOnly);

//������ ������
  int GlobFirstOtschet(
                  bool IsNotIsklBortOnly, //�� �������� ����� �����
                  bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                  bool IsNotNullVysOnly,  //������ � ��������� �������
                  bool IsExistingOnly,
                  bool IsOptionOnly);

//������ ������
  int GlobLastOtschet(
                  bool IsNotIsklBortOnly, //�� �������� ����� �����
                  bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                  bool IsNotNullVysOnly,  //������ � ��������� �������
                  bool IsExistingOnly,
                  bool IsOptionOnly
                   );


//�������� ��������� ������
  int GlobNextOtschet(int  IndexOtsch,
                      bool IsNotIsklBortOnly, //�� �������� ����� �����
                      bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                      bool IsNotNullVysOnly,  //������ � ��������� �������
                      bool IsExistingOnly,
                      bool IsOptionOnly
                     );

//���������� ������
  int GlobPrevOtschet(int  IndexOtsch,
                      bool IsNotIsklBortOnly, //�� �������� ����� �����
                      bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                      bool IsNotNullVysOnly,  //������ � ��������� �������
                      bool IsExistingOnly,
                      bool IsOptionOnly
                     );



  int  AllDanIskajForNomer(int INomer);  //�������� ������ � ���� ����������, ������� ��������
             //��� ����� � �������� INomer. ���������� �� AllDanIskaj
  void AllDanIskaj(void);  //��������� ������ � ����������, ��� �������������� ��� � ���
  void ZapolnObshRezIskaj(void);


//���������� ������ �����
  int SmenaNomera(int KodOO,           //���
                  int NewKodBort);      //������;






//����� ���������� �������  ������� ������ ������
 int ZapolnitStringGrid(void *SG,        //������ ���� TStringGrid
                        int IndexNomer,  //������ ����������� �� ����� �����
                        bool VklIskl,     //�������� �� ����������� �������
                        bool IsOptionOnly //������ ��������������� ���������
                        );


//����� ���������� �������  ������� ������ ������
 int ZapolnitStringGridIskNom(void *SG,        //������ ���� TStringGrid
                        bool VklIsklBort,     //�������� �� ����������� �����
                        bool VklIskl,     //�������� �� ����������� �������
                        bool IsOptionOnly //������ ��������������� ���������
                        );




//����� ������ ������ ����� ������ ������ ������ ������ ������� ��� � RBS (��������)
 int ZapolnitStringGridObzor(void *SG,        //������ ���� TStringGrid
                        int IndexNomerOtch,  //������ ����������� �� ������ �������...
                        bool VklIskl     //�������� �� ����������� �������
                        );



//��������� ������ ������ OSB ���� ONE_S_BORT_631
  int SetDataOSB(int Kak,  //��� ���������,
                            //���� 1 - ������������� �� ������� ������
                            //     2 - �� �������
                            //     3 - ���, ��� ����� �������� ����� N_of_O1
                int N_of_O1=1);  //����� ��������

//���������  TCheckListBox
  int ZapolnitCheckListBox(void *CLB);


//���������� ��������� ������� ��� �������������� ��� ���
  int SetIskl(int IndxOtch, bool NewIsIskl);


//���������� ��������� ������� ��� �������������� ��� ��� ��� ���������� ��������
  int SetIsklMulti(int *IndxOtch, int N_of_Otch, bool NewIsIskl);



  int SetIsklBort(int IndxBort, bool NewIsIskl);


//������� ������� �� ������ ������
  int UdalitElementIzSpiska(int KodOO);

//�������� ������� � ������
  int DobavitElementVSpisok(int KodOO,
                            int NewKodBort);





//���������� ������ �� ���������� �������
  int ZapolnMIV(void);

//��������� ������� StringGrid
  int ZapolnitMIV_SG(
                     void *SG,        //������ ���� TStringGrid
                     int Zona1,
                     bool OptionOnly,  //������ ������������� ������
  bool NotVyvodEsliMalo //�� ��������, ���� ���� �������� ��������������� ������

                    );



//����� ������� ������������ ������
  int GetNomerNachalo(
                       int Indx,         //������ �������
                       bool VklIsklBort, //�������� ����������� ������ ������
                       bool VklIsklOtsch, //������� ����������� �� ������������ �������
                       bool IsOptionOnly  //������ ��������������� ���������
                                          //������ �� ��������, ��������� � ������
                      );

//����� �������� ����� ������� �������
   int GetChisloOtschetovPosle(
                       int Indx,         //������ �������
                       bool VklIsklBort, //�������� ����������� ������ ������
                       bool VklIsklOtsch, //������� ����������� �� ������������ �������
                       bool IsOptionOnly  //������ ��������������� ���������
                                          //������ �� ��������, ��������� � ������
                      );

//��������� ������ � ���������� �� ����� �������
//�����.0, ���� �� �������, ����� 1
   int FindBlijOtschetAndRastoyanie(
                       int Indx,
                       int *BlijIndex,
                       double *BlijRast);





//��������� �������� ��������
  double GetRaznizaAzimutov(int Indx1,
                            int Indx2);




//����������� � ����� For631_a.h
//����� ������ � ���������� � Strings
   int VyvodToStringIskaj(TStrings *Strings,
                          int Zona,   //1, 2 ,3, 4
                          int Type,    //1 -���, 2 - RBS
                          AnsiString FN,
                          AnsiString *FN_Plots,int N_of_FN_Plots
                          );

  int VyvodToStringIskaj0001(TStrings *Strings,
                          int Type,    //1 -���, 2 - RBS
                          AnsiString FN,
                          AnsiString *FN_Plots,int N_of_FN_Plots
                          );




private:



//����� ������� � ������ ��������, �� ���� ����� ����� ���� �����

  int FindBlizkieKoVremeniBorta(
                                int OtkudaIskatIndx,
                                int IndexBort,
                                bool IsNotIsklBortOnly, //�� ����������� �����
                                bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                                bool IsNotNullVysOnly,  //������ � ��������� �������
                                bool IsExistingOnly,     //��
                                int *Indx1,
                                int *Indx2);





//����� �� �������� ���������� � ������������..






long Index0;

};

int LoadUVDFromFile( void *fp1,  //������������� �����
                                long Vysota1,long Vysota2, bool IsVysota,       //��������� ������
                                double MinAzmt1, double MaxAzmt1, bool IsAzmt1, //�������
                                double MinAzmt2, double MaxAzmt2, bool IsAzmt2,
                                double MinDlnst, double MaxDlnst, bool IsDlnst,
                                OTSCHETS631 *OtchUVD,OTSCHETS631 *OtchRBS,
                                bool CS
                    );

#endif
