//---------------------------------------------------------------------------
#ifndef tools631H
#define tools631H
#include "RabsBort.h"
//---------------------------------------------------------------------------
/*��������� ���������� ������ �� RBS � UVD*/
/*���������� 1, ���� ������ ���; 2, ���� ������ RBS; 0, ���� ������ ���������� */
int TipBorta(DANN Dan, //������ ��������� ��������� � ����� RBort631.h
             bool CS); //��� �������� ���� ��� ���


/*���������� �������� ��������*/
bool AsimutDiapazon(DANN Dan,double Asmt1,double Asmt2);
bool AsimutDiapazon(double Azmt,double Asmt1,double Asmt2);



int ProverkaVys(int Vys1,int Obs1,
                int Vys2,int Obs2,
                int Vys3,int Obs3,
                int MaxDVys);

int GetTypeVStrobe631(
                       double A1, double D1, double T1,
                       double A2, double D2, double T2,
                       double TO,   //���� ������
                       double Strob, double MinDD, double MaxDD);


#endif
