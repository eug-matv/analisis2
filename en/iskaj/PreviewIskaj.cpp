//---------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <stdio.h>
#include <alloc.h>
#include <Printers.hpp>

//for Microsoft Help
#include <htmlhelp.h>

//#include "SpisokFilis.h"
//#include "GlavOkno.h"

#include "PreviewIskaj.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma resource "*.dfm"


TPreviewFormIskaj *PF_UVD[6]={NULL,NULL,NULL,NULL,NULL,NULL};
TPreviewFormIskaj *PF_RBS[6]={NULL,NULL,NULL,NULL,NULL,NULL};



//---------------------------------------------------------------------------

__fastcall TPreviewFormIskaj::TPreviewFormIskaj(TComponent *Owner)
	: TForm(Owner)
{
        
}
//---------------------------------------------------------------------------




void __fastcall TPreviewFormIskaj::FileSaveAs1Execute(TObject *Sender)
{

 FILE *fp;
 int RetPerezapis;
 int i;
 AnsiString FS1,FS2;
 SaveDialog->Filter="Text Files(*.TXT)|*.txt|All Files|*.*";
 SaveDialog->DefaultExt=String("txt");
 if (SaveDialog->Execute())
 {
/*
    FS2=SaveDialog->FileName.Trim();

    FS2=LowerCase(FS2);

    for(i=0;i<FSpisokFiles->N_of_fps;i++)
    {
      FS1=Trim(FSpisokFiles->FileNames[i]);
      FS1=LowerCase(FS1);

      if(FS2==FS1)
      {
        RetPerezapis=MessageBox(NULL,
        "������ ���� � �������� ������ ������! �� ����� ������������?",
        "������!",MB_YESNO);
        if(RetPerezapis==IDYES)
        {
//���������� ��� �����
               FGlavOkno->FileNamePerezapis=SaveDialog->FileName+String("1");
               RichEdit1->Lines->SaveToFile(FGlavOkno->FileNamePerezapis);
               FGlavOkno->TimerPerezapis->Enabled=true;

        }
        return;
      }
    }
*/  
    RichEdit1->Lines->SaveToFile(SaveDialog->FileName);


 }

}
//---------------------------------------------------------------------------


void __fastcall TPreviewFormIskaj::FileExit1Execute(TObject *Sender)
{
 Close();
}
//---------------------------------------------------------------------------


void __fastcall TPreviewFormIskaj::FontItemClick(TObject *Sender)
{
 bool Ret;
//������� �����
 Ret=FontDialog->Execute();
 if(Ret)
 {
  RichEdit1->Font=FontDialog->Font;
 }

}
//---------------------------------------------------------------------------


void __fastcall TPreviewFormIskaj::FormCreate(TObject *Sender)
{
    FontDialog->Font=RichEdit1->Font;
    iffWorkWithIniFile(this, String("iskaj_vis.ini"));

}
//---------------------------------------------------------------------------












void __fastcall TPreviewFormIskaj::FormHide(TObject *Sender)
{
   RichEdit1->Clear();        
}
//---------------------------------------------------------------------------

void __fastcall TPreviewFormIskaj::N4Click(TObject *Sender)
{
  bool Ret;
  long i;
  int MsgRet;
  long Lft,Rght,Tp,Bttm,Shrna;
  long TekRyad,NextRyad;  //������� ���
  long ShrnaStrki,ShrnaSym;   //������ ������, ������ �������
  long NomerStrn;  //����� ��������
  AnsiString Strka,Strka1;
  long Temp1;

  TPrintDialog *PrintDialog1;
  TPrinter *Prntr;
//��������� ������� ���� �� ������ ��������
  Prntr=Printer();
  if(!Prntr)
  {
    MessageBox(NULL, "Printer not found", "Error!", MB_OK);
    return;
  }


  if(Prntr->Printers->Count==0)
  {
    MessageBox(NULL, "Printer not found", "Error!", MB_OK);
    return;

  }
  PrintDialog1=new TPrintDialog(this);
  PrintDialog1->Collate=false;
  PrintDialog1->Copies=1;
  PrintDialog1->Ctl3D=false;
  PrintDialog1->FromPage=0;
  PrintDialog1->HelpContext=0;
  PrintDialog1->MaxPage=0;
  PrintDialog1->MinPage=0;
  PrintDialog1->PrintRange=prAllPages;
  PrintDialog1->PrintToFile=false;
  PrintDialog1->ToPage=0;
  Ret=PrintDialog1->Execute();
  if(!Ret)return;

  MsgRet=MessageBox(Handle,"Horizontally there is page?", "Attention!",MB_YESNO);
  if(MsgRet==IDYES)
  {
    Prntr->Orientation=poLandscape;
  }else{
    Prntr->Orientation=poPortrait;
  }


  Prntr->BeginDoc();

//��������� ����� Courer New � ������ ������ 8, � ��� �� ������
  Prntr->Canvas->Font->Name="Courier New";
  Prntr->Canvas->Font->Color=clBlack;
  if(MsgRet==IDYES)
  {
    Prntr->Canvas->Font->Style=Prntr->Canvas->Font->Style>>fsBold;
    Prntr->Canvas->Font->Size=10;
  }else{
    Prntr->Canvas->Font->Style=Prntr->Canvas->Font->Style<<fsBold;
    Prntr->Canvas->Font->Size=8;
  }
  Lft=Prntr->PageWidth/15;
  Rght=0.98*Prntr->PageWidth;
  Tp=Prntr->PageHeight/15;
  Bttm=0.98*Prntr->PageHeight;
  Shrna=Rght-Lft;
  TekRyad=Tp;
  NextRyad=TekRyad+Prntr->Canvas->TextHeight("1");
  for(i=0;i<RichEdit1->Lines->Count;i++)
  {
    Strka=RichEdit1->Lines->Strings[i].TrimRight();
    ShrnaStrki=Prntr->Canvas->TextWidth(Strka);
    if(NextRyad>Bttm)
    {
       Prntr->NewPage();
       TekRyad=Tp;
       NextRyad=TekRyad+Prntr->Canvas->TextHeight("1");
    }
    if(ShrnaStrki>Shrna)
    {
      ShrnaSym=Prntr->Canvas->TextWidth("A");
      Temp1=ShrnaStrki-Shrna;
      Temp1/=ShrnaSym;
      Temp1++;
      Strka=Strka.SubString(1,Strka.Length()-Temp1);
    }

//� ������ ������� ������
    Prntr->Canvas->TextOut(Lft,TekRyad,Strka);

    TekRyad=NextRyad;
    NextRyad=NextRyad+Prntr->Canvas->TextHeight("1");
  }

  Prntr->Canvas->Font->Style=Prntr->Canvas->Font->Style>>fsBold;

  Prntr->EndDoc();


}
//---------------------------------------------------------------------------

























void __fastcall TPreviewFormIskaj::MDeleteClick(TObject *Sender)
{
  RichEdit1->ClearSelection();        
}
//---------------------------------------------------------------------------

void __fastcall TPreviewFormIskaj::MSelectAllClick(TObject *Sender)
{
      RichEdit1->SelectAll();    
}
//---------------------------------------------------------------------------

void __fastcall TPreviewFormIskaj::Help1Click(TObject *Sender)
{
  AnsiString HhpStr;

  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }

    HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);

}
//---------------------------------------------------------------------------




void __fastcall TPreviewFormIskaj::CutItemClick(TObject *Sender)
{
 RichEdit1->CutToClipboard();        
}
//---------------------------------------------------------------------------

void __fastcall TPreviewFormIskaj::CopyItemClick(TObject *Sender)
{
   RichEdit1->CopyToClipboard();        
}
//---------------------------------------------------------------------------

void __fastcall TPreviewFormIskaj::PasteItemClick(TObject *Sender)
{
     RichEdit1->PasteFromClipboard();        

}
//---------------------------------------------------------------------------



void __fastcall TPreviewFormIskaj::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==VK_F1)
        {
           Help1Click(NULL);   
        }
}
//---------------------------------------------------------------------------

