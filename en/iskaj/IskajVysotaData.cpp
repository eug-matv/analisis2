//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>

#include <stdio.h>
#include "For631.h"
#include "GlavFFor631.h"
#include "GlobalData631.h"
#include "IskajVysotaData.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFIskajVysotaData *FIskajVysotaData;
//---------------------------------------------------------------------------
__fastcall TFIskajVysotaData::TFIskajVysotaData(TComponent* Owner)
	: TForm(Owner)
{
        CHMHtml="altitude_distortion.htm";
}
//---------------------------------------------------------------------------
void __fastcall TFIskajVysotaData::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  if(Type==1)
  {
    CloseFIVDataUVD();
  }else{
    CloseFIVDataRBS();
  }
}
//---------------------------------------------------------------------------


void __fastcall TFIskajVysotaData::VisualizeDataOfBort(void)
{
   OTSCHETS631 *O631;
   O631=(OTSCHETS631*)Otschs631;
// �������� �����  O631->ZapolnitMIV_SG(StringGrid1,Zona, CBVyvodOnlyOption->Checked,
//                        CBNotVyvodEsliMalo->Checked);

O631->ZapolnitMIV_SG(StringGrid1,Zona, true,
                        false);

}

void __fastcall TFIskajVysotaData::FormShow(TObject *Sender)
{
   char Strka[100];
   OTSCHETS631 *O631;
   O631=(OTSCHETS631*)Otschs631;


   Zona=0;
//�������� ����������
   CBVidyOtschetov->Items->Clear();

   sprintf(Strka, "All marks suspected of an error by altitude");
   CBVidyOtschetov->Items->Add(Strka);


   sprintf(Strka, "Single in the strobe: %.3lf �",
            O631->Strob);
   CBVidyOtschetov->Items->Add(Strka);

   sprintf(Strka, "Different plots in the strobe: %.3lf �,\
distance more %.2lf km",
            O631->Strob, O631->MaxDD);
   CBVidyOtschetov->Items->Add(Strka);

    sprintf(Strka, "Different plots in the strobe: %.3lf �,\
distance from %.2lf to %.2lf km",
            O631->Strob, O631->MinDD,O631->MaxDD);
   CBVidyOtschetov->Items->Add(Strka);


    sprintf(Strka, "Different plots in the strobe: %.3lf �,\
distance less %.2lf km",
            O631->Strob, O631->MinDD);
   CBVidyOtschetov->Items->Add(Strka);
   
   CBVidyOtschetov->Text=CBVidyOtschetov->Items->Strings[0];

   VisualizeDataOfBort();
}
//---------------------------------------------------------------------------

void __fastcall TFIskajVysotaData::CBVidyOtschetovChange(TObject *Sender)
{
  int i;
  AnsiString Strka=CBVidyOtschetov->Text.Trim();
  Zona=-1;
  for(i=0;i<CBVidyOtschetov->Items->Count;i++)
  {
    if(CBVidyOtschetov->Items->Strings[i].Trim()==Strka)
    {
      Zona=i;
      break;
    }
  }
  if(Zona==-1)
  {
    Zona=0;
    CBVidyOtschetov->Text=CBVidyOtschetov->Items->Strings[0];
  }
  VisualizeDataOfBort();
}
//---------------------------------------------------------------------------

int __fastcall TFIskajVysotaData::ObnovIskajVys(void)
{
  int i;
  int Indx;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  for(i=1;i<StringGrid1->RowCount;i++)
  {
    Indx=StringGrid1->Cells[0][i].ToInt();
    if(Indx<0)continue;
    if(O631->OO[Indx].IsFalseVysota)
    {
       StringGrid1->Cells[8][i]="False";

    }else if(O631->OO[Indx].Vysota==0)
    {
       StringGrid1->Cells[8][i]="Nil";
    }else{
       StringGrid1->Cells[8][i]="True";
    }

  //������ ������� �� ���� ��� �������� �� ������������
    if(O631->OO[Indx].IsklOtch)
    {
      StringGrid1->Cells[7][i]="Excluded";
    }else{
      StringGrid1->Cells[7][i]="Included";
    }


  }
  return 1;
}


void __fastcall TFIskajVysotaData::N1Click(TObject *Sender)
{
  int IndxBort, Indx;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)
  {
    return;
  }
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  if(Indx<0)return;
  if(Type==1)
  {
    OpenFODataUVD_Indx(Otschs631,Indx);
  }else{
    OpenFODataRBS_Indx(Otschs631,Indx);
  }
}
//---------------------------------------------------------------------------


void __fastcall TFIskajVysotaData::N2Click(TObject *Sender)
{
  int IndxBort, Indx;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)
  {
    return;
  }
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  if(Indx<0)return;
  IndxBort=O631->OO[Indx].IndexNomer;
  if(Type==1)
  {
 //���
     OpenFDOB_UVD_Indx(Otschs631,IndxBort,Indx);
  }else{
 //RBS
     OpenFDOB_RBS_Indx(Otschs631,IndxBort,Indx);
  }
}
//---------------------------------------------------------------------------

void __fastcall TFIskajVysotaData::PopupMenu1Popup(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr->Checked=O631->OO[Indx].IsFalseVysota;
    }else{
      MVysErr->Checked=false;
    }
  }else{
    MVysErr->Checked=false;
  }


}
//---------------------------------------------------------------------------

void __fastcall TFIskajVysotaData::MVysErrClick(TObject *Sender)
{
  int i;
  MVysErr->Checked=!MVysErr->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      if(O631->OO[Indx].Vysota==0)return;
      O631->OO[Indx].IsFalseVysota=MVysErr->Checked;
      if(Type==1)
      {
         IzmenFalseVysotaFDOB_UVD();
         IzmenFalseVysotaFIVData_UVD();
         IzmenFalseVysotaFOData_UVD();
      }else{
         IzmenFalseVysotaFDOB_RBS();
         IzmenFalseVysotaFIVData_RBS();
         IzmenFalseVysotaFOData_RBS();
      }
      O631->ZapolnMIV();
    }

  }

}
//---------------------------------------------------------------------------


void __fastcall TFIskajVysotaData::N4Click(TObject *Sender)
{
   OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr2->Checked=O631->OO[Indx].IsFalseVysota;
    }else{
      MVysErr2->Checked=false;
    }
  }else{
    MVysErr2->Checked=false;
  }

 	
}
//---------------------------------------------------------------------------

void __fastcall TFIskajVysotaData::MVysErr2Click(TObject *Sender)
{
  int i;
  MVysErr2->Checked=!MVysErr2->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otschs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      if(O631->OO[Indx].Vysota==0)return;
      O631->OO[Indx].IsFalseVysota=MVysErr2->Checked;
      if(Type==1)
      {
         IzmenFalseVysotaFDOB_UVD();
         IzmenFalseVysotaFIVData_UVD();
         IzmenFalseVysotaFOData_UVD();
      }else{
         IzmenFalseVysotaFDOB_RBS();
         IzmenFalseVysotaFIVData_RBS();
         IzmenFalseVysotaFOData_RBS();
      }
      O631->ZapolnMIV();
    }

  }
	

}
//---------------------------------------------------------------------------



void __fastcall TFIskajVysotaData::CBVyvodOnlyOptionClick(TObject *Sender)
{
   VisualizeDataOfBort();
}
//---------------------------------------------------------------------------


void __fastcall TFIskajVysotaData::CBNotVyvodEsliMaloClick(TObject *Sender)
{
  VisualizeDataOfBort();	
}
//---------------------------------------------------------------------------

void __fastcall TFIskajVysotaData::N7Click(TObject *Sender)
{
  AnsiString HhpStr;

  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }

    HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);

}
//---------------------------------------------------------------------------


void __fastcall TFIskajVysotaData::FormCreate(TObject *Sender)
{
     iffWorkWithIniFile(this, String("iskaj_vis.ini"));        
}
//---------------------------------------------------------------------------





void __fastcall TFIskajVysotaData::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
        if(Key==VK_F1)
        {
            N7Click(NULL);   
        }
}
//---------------------------------------------------------------------------


