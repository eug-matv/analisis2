//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <math.h>
#include "tools631.h"
#include "tools.h"
#include "RabsBort.h"
#include "string.h"


#define ABS(X) (((X) > 0) ? (X) : (-(X)))
#define ZNAK_CHISLA1(X) (((X) > 0) ? (1) : (-1))
#define ZNAK_CHISLA(X) (((X) == 0) ? (0) : (ZNAK_CHISLA1(X)))

//---------------------------------------------------------------------------
#pragma package(smart_init)
/*��������� ���������� ������ �� RBS � UVD*/
/*���������� 1, ���� ������ ���; 2, ���� ������ RBS; 0, ���� ������ ���������� */

int TipBorta(DANN Dan, //������ ��������� ��������� � ����� RBort631.h
             bool CS)  //��� �������� ���� ��� ���
{
 int Dlina;

 CS=false;
//�������� ��������� �� ��� �����
 if(Dan.T_K[0]!='S'&&Dan.T_K[1]!='S')return 0;
 Dlina=strlen(Dan.NomBort);

// if(Dlina==1)
 {
//  return 0;
 }

/*����� �������� 2000*/
 if(0==strcmp("2000",Dan.NomBort))
 {
  return 0;
 }
 if(Dan.NomBort[0]=='9'&&Dan.NomBort[1]=='9'&&Dan.NomBort[2]=='9'&&Dan.NomBort[3]=='9'&&Dan.NomBort[4]=='9')
 {
   Dlina=5;
   return 0;

 }

 if(CS)//�������� ����
 {
 //��������, ������� �������� � ������ �����
 //���� 5 ��������, �� ��� ���,
  if(Dlina==5)//��� ���
  {
   return 1;
  }else{
   return 2;
  }

 }else{
 //�������� ��� �� ���
   if(Dan.T_Bort[0]=='U'||Dan.T_Bort[1]=='U')
   {
     return 1;
   }else if(Dan.T_Bort[1]=='I'||Dan.T_Bort[0]=='I')
   {
     return 2;
   }
 }
 return 0;
}


/*���������� �������� ��������*/
bool AsimutDiapazon(DANN Dan,double Asmt1,double Asmt2)
{
//
 if(Asmt1>Asmt2)
 {
  if(Dan.Asimut<Asmt1-0.00001&&Dan.Asimut>Asmt2+0.00001)
  {
   return false;
  }
  return true;
 }else{
  if(Dan.Asimut<=Asmt2+0.00001&&Dan.Asimut>=Asmt1-0.00001)
  {
   return true;
  }
 }
 return false;
}

/*���������� �������� ��������*/
bool AsimutDiapazon(double Azmt,double Asmt1,double Asmt2)
{
//
 if(Asmt1>Asmt2)
 {
  if(Azmt<Asmt1-0.00001&&Azmt>Asmt2+0.00001)
  {
   return false;
  }
  return true;
 }else{
  if(Azmt<=Asmt2+0.00001&&Azmt>=Asmt1-0.00001)
  {
   return true;
  }
 }
 return false;
}


//�������� ������
//�����. 1 - �� ��������� ���� �������,
//2 - �� ��������� ������� ���,
//3 - �������� ������� ����,
//4 - �������� ������� ���
int ProverkaVys(int Vys1,int Obs1,
                int Vys2,int Obs2,
                int Vys3,int Obs3,
                int MaxDVys)
{
 int DV1,DV2,dDV;
 DV1=Vys2-Vys1;
 DV2=Vys3-Vys2;

/*
 if(DV>0)  //��� �����
 {
 //��������, ����� ����� ��� �� ����� MaxDVys
  if(DV<=MaxDVys)return  1;
 //��������, ����� ������ ���� ������
  if(Vys3>=Vys2)return 1;
 //��� ��� �������� ������
 //��������� ������ �������
  if(Obs3-Obs1<=10)return 4;
 }else{   //�������
 //��������, ����� ��������� ������ ���� �� �����
  if(DV>=-MaxDVys)return 1;
 //��������, ����� ������ ���� �� ������
  if(Vys3<=Vys2)return 1;
  //��� ��� �������� ������
  //�������� ������ �������
  if(Obs3-Obs1<=10)return 4;
 }
 */

 if(Obs3-Obs1>10)return 1;
 dDV=DV1-DV2;

//  if((ABS(DV1)>MaxDVys)&& (dDV<-MaxDVys||dDV>MaxDVys)&&ZNAK_CHISLA(DV1)*ZNAK_CHISLA(DV2)<=0)
 if((dDV<-MaxDVys||dDV>MaxDVys)&&(ABS(DV1)>MaxDVys||ABS(DV2)>MaxDVys))
 {
   return 4;
 }

 return 1;
}


//����������� ��������� ������� � ������
int GetTypeVStrobe631(
                       double A1, double D1, double T1,
                       double A2, double D2, double T2,
                       double TO,   //���� ������
                       double Strob, double MinDD, double MaxDD)
{
  double DT;
  double DD;
  double DX,DY;
  long Chislo1,Chislo2,Chislo3;
  long lDD,lMinDD,lMaxDD;
  DT=ABS(T1-T2);
  if(DT>=TO/10.0)return 1;
  if(A2<A1)
  {
    if(A1>350&&A2<10)
    {
      A2+=360;
      Chislo1=Okrugl(A1*100);
      Chislo2=Okrugl(A2*100);
      Chislo3=Okrugl(Strob*100);
      if(Chislo2-Chislo1>Chislo3)return 1;
    }else{
      Chislo1=Okrugl(A1*100);
      Chislo2=Okrugl(A2*100);
      Chislo3=Okrugl(Strob*100);
      if(Chislo1-Chislo2>Chislo3)return 1;
    }

  }else{
    Chislo1=Okrugl(A1*100);
    Chislo2=Okrugl(A2*100);
    Chislo3=Okrugl(Strob*100);
    if(Chislo2-Chislo1>Chislo3)return 1;
  }

  Chislo1=Okrugl(D1*100);
  Chislo2=Okrugl(D2*100);
  lDD=ABS(Chislo2-Chislo1);
  lMinDD=Okrugl(MinDD*100);
  lMaxDD=Okrugl(MaxDD*100);
  if(lDD<lMinDD)
  {
    DT=1;
    return 4;
  }
  if(lDD<=lMaxDD)
  {
    DT=2;
    return 3;
  }
  return 2;
}



