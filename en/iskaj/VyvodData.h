//---------------------------------------------------------------------------
#ifndef VyvodDataH
#define VyvodDataH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include "CSPIN.h"
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TFOData : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TStringGrid *StringGrid1;
	TScrollBar *ScrollBar1;
	TGroupBox *GroupBox1;
	TCheckBox *CBIsklBort;
	TCheckBox *CBIsklOtsch;
	TTimer *Timer1;
	TTimer *Timer2;
	TGroupBox *GroupBox2;
	TCSpinEdit *CSEMinN_of_O;
	TLabel *Label1;
	TLabel *Label2;
	TComboBox *ComboBox1;
	TButton *Button1;
	TPopupMenu *PopupMenu1;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *MVysErr;
	TMainMenu *MainMenu1;
	TMenuItem *N3;
	TMenuItem *N4;
	TMenuItem *MVysErr2;
	TMenuItem *N5;
	TCheckBox *CBFilterOnly;
	TMenuItem *MRasmatrivat;
        TMenuItem *MRasmatrivat2;
	TMenuItem *N6;
	void __fastcall ScrollBar1Change(TObject *Sender);
	void __fastcall CBIsklBortClick(TObject *Sender);
	void __fastcall CBIsklOtschClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
	void __fastcall Timer1Timer(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
	void __fastcall StringGrid1KeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
	void __fastcall PopupMenu1Popup(TObject *Sender);
	void __fastcall MVysErrClick(TObject *Sender);
	void __fastcall N1Click(TObject *Sender);
	void __fastcall N3Click(TObject *Sender);
	void __fastcall MVysErr2Click(TObject *Sender);
	void __fastcall N2Click(TObject *Sender);
	void __fastcall MRasmatrivatClick(TObject *Sender);
        void __fastcall MRasmatrivat2Click(TObject *Sender);
	void __fastcall CBFilterOnlyClick(TObject *Sender);
	void __fastcall N6Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
        int PrevScrollBar1Position;
        bool SmenaScrollBar1Position;
        bool IsVnizuKursorScrollBar1;

public:		// User declarations
    void *Otschs631;      //������ ��������
    int Type;         //1 - ���, 2 - RBS
    int PervIndx;     //������ �� ������ ������� - �� ��������

    AnsiString CHMHelpFile;
    AnsiString CHMHtml;


	__fastcall TFOData(TComponent* Owner);

 //������� ����� � ������ �������
    void __fastcall MakeShapkaAndRazmery(void);


 //���������� StringGrid1
    void __fastcall ZapolnStringGrid1(void);


//����� ����� ���� �� ������� ��
    void __fastcall SdvigUpDownStringGrid1(int Sdvg);


//���������� ������ ������
    int __fastcall FindOtschetIndx(int Indx);

//��������, �� ������� ������ ������
    int __fastcall ShowIndx(int Indx); 

//�������� ��� ���� ��������� �� ������
    int __fastcall ObnovIskajVys(void);    
};
//---------------------------------------------------------------------------
extern PACKAGE TFOData *FOData;
//---------------------------------------------------------------------------
#endif
