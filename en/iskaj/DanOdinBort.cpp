//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>

#include "GlavFFor631.h"
#include "DanOdinBort.h"
#include "For631.h"
#include "For631_2.h"
#include "GlobalData631.h"
#include "work_ini_form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFDanOdinBort *FDanOdinBort;
//---------------------------------------------------------------------------
__fastcall TFDanOdinBort::TFDanOdinBort(TComponent* Owner)
	: TForm(Owner)
{

}
//---------------------------------------------------------------------------




void __fastcall TFDanOdinBort::VisualizeDataOfBort(void)
{
  OTSCHETS631 *O631;
  O631=(OTSCHETS631*)Otchs631;

  O631->ZapolnitStringGrid(StringGrid1,IndexBort,CB_VklIskl->Checked,
                           CB_FilterOnly->Checked);
  if(Type==1)
  {
     LNomer->Caption=String("Aircraft UVD No.")+
                     IntToStr((int)(O631->NomBort[IndexBort]));
  }else{
     LNomer->Caption=String("Aircraft RBS No.")+
                     IntToStr((int)(O631->NomBort[IndexBort]));
  }

  int i,j;
  for(i=0;i<StringGrid1->ColCount;i++)
  {
    for(j=0;j<StringGrid1->RowCount;j++)
    {
//      StringGrid1->Cells[i][j]="";
    }
  }

}
void __fastcall TFDanOdinBort::CB_VklIsklClick(TObject *Sender)
{
   VisualizeDataOfBort();
}
//---------------------------------------------------------------------------


void __fastcall TFDanOdinBort::FormShow(TObject *Sender)
{

  VisualizeDataOfBort();
}
//---------------------------------------------------------------------------
int __fastcall TFDanOdinBort::FindOtschetIndx(int Indx)
{
  int i;
  int Chislo, Chislo2;
  TGridRect MyRect;

  for(i=1;i<StringGrid1->RowCount;i++)
  {
    Chislo=StringGrid1->Cells[0][i].ToInt();
    if(Chislo<0)break;
    if(Chislo==Indx)
    {
//������� ������
       Chislo2=i-StringGrid1->VisibleRowCount/2;
       if(Chislo2<1)Chislo2=1;
       StringGrid1->TopRow=Chislo2;
       MyRect.Left=1;
       MyRect.Right=13;
       MyRect.Top=i;
       MyRect.Bottom=i;
       StringGrid1->Selection=MyRect;
       return 1;
    }
  }
  return 0;
}

int __fastcall TFDanOdinBort::FindOtschetRow(int Row)
{
  int Chislo2;
  TGridRect MyRect;
  if(Row<1||Row>StringGrid1->RowCount)
  {
    return 0;
  }

  Chislo2=Row-StringGrid1->VisibleRowCount/2;
  if(Chislo2<1)Chislo2=1;
  StringGrid1->TopRow=Chislo2;
  MyRect.Left=1;
  MyRect.Right=13;
  MyRect.Top=Row;
  MyRect.Bottom=Row;
  StringGrid1->Selection=MyRect;
  return 1;      
}



//---------------------------------------------------------------------------
int __fastcall TFDanOdinBort::MoveOtchet(int NumLine)
{
  int N;
  extern bool IsMoveUVD;
  extern int MoveOtkudaUVD;
  extern bool IsMoveRBS;
  extern int MoveOtkudaRBS;
  if(NumLine>=StringGrid1->RowCount||NumLine<1)
  {
    return 0;
  }
  if(Type==1)
  {
     MoveOtkudaUVD=StrToIntDef(StringGrid1->Cells[NumLine][0],-1);
     if(MoveOtkudaUVD<0)
     {
       IsMoveUVD=false;
       return 0;
     }
     IsMoveUVD=true;
  }else{
     MoveOtkudaRBS=StrToIntDef(StringGrid1->Cells[NumLine][0],-1);
     if(MoveOtkudaRBS<0)
     {
       IsMoveRBS=false;
       return 0;
     }
     IsMoveRBS=true;
  }

  return 1;
}





void __fastcall TFDanOdinBort::PopupMenu1Popup(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr->Checked=O631->OO[Indx].IsFalseVysota;
      MRasmatrivat->Checked=!O631->OO[Indx].IsklOtch;
    }else{
      MVysErr->Checked=false;
      MRasmatrivat->Checked=false;
    }
  }else{
    MVysErr->Checked=false;
    MRasmatrivat->Checked=false;
  }

}
//---------------------------------------------------------------------------


void __fastcall TFDanOdinBort::MVysErrClick(TObject *Sender)
{
  int i;
  MVysErr->Checked=!MVysErr->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
     if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
     {
       Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
       if(Indx>=0)
       {
         if(O631->OO[Indx].Vysota==0)return;
         O631->OO[Indx].IsFalseVysota=MVysErr->Checked;
         if(MVysErr->Checked)
         {
           StringGrid1->Cells[10][StringGrid1->Row]="False";
         }else{
           StringGrid1->Cells[10][StringGrid1->Row]="True";
         }
         if(Type==1)
         {
           IzmenFalseVysotaFOData_UVD();
           IzmenFalseVysotaFIVData_UVD();
         }else{
           IzmenFalseVysotaFOData_RBS();
           IzmenFalseVysotaFIVData_RBS();
         }
         O631->ZapolnMIV();
       }
     }
}
//---------------------------------------------------------------------------


void __fastcall TFDanOdinBort::N1Click(TObject *Sender)
{
  int IndxBort, Indx;
  if(StringGrid1->Row<1||StringGrid1->Row>=StringGrid1->RowCount)
  {
    return;
  }
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
  if(Indx<0)return;
  if(Type==1)
  {
    OpenFODataUVD_Indx(Otchs631,Indx);
  }else{
    OpenFODataRBS_Indx(Otchs631,Indx);
  }
}
//---------------------------------------------------------------------------
int __fastcall TFDanOdinBort::ObnovIskajVys(void)
{
  int i;
  int Indx;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  for(i=1;i<StringGrid1->RowCount;i++)
  {
    Indx=StringGrid1->Cells[0][i].ToInt();
    if(Indx<0)continue;
    if(O631->OO[Indx].IsFalseVysota)
    {
       StringGrid1->Cells[10][i]="False";

    }else if(O631->OO[Indx].Vysota==0)
    {
       StringGrid1->Cells[10][i]="Nil";
    }else{
       StringGrid1->Cells[10][i]="True";
    }

    if(O631->OO[Indx].Nomer!=O631->NomBort[O631->OO[Indx].IndexNomer])
    {
      StringGrid1->Cells[11][i]="False";
    }else{
      StringGrid1->Cells[11][i]="True";
    }
//������� �����
    StringGrid1->Cells[2][i]=IntToStr((int)O631->NomBort[O631->OO[Indx].IndexNomer]);

//������ ������� �� ���� ��� �������� �� ������������
    if(O631->OO[Indx].IsklOtch)
    {
      StringGrid1->Cells[9][i]="Excluded";
    }else{
      StringGrid1->Cells[9][i]="Included";
    }
  }
}



void __fastcall TFDanOdinBort::MVysErr2Click(TObject *Sender)
{
  int i;
  MVysErr2->Checked=!MVysErr2->Checked;
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      if(O631->OO[Indx].Vysota==0)return;
      O631->OO[Indx].IsFalseVysota=MVysErr2->Checked;
      if(MVysErr2->Checked)
      {
        StringGrid1->Cells[10][StringGrid1->Row]="False";
      }else{
        StringGrid1->Cells[10][StringGrid1->Row]="True";
      }
      if(Type==1)
      {
        IzmenFalseVysotaFOData_UVD();
        IzmenFalseVysotaFIVData_UVD();
      }else{
        IzmenFalseVysotaFOData_RBS();
        IzmenFalseVysotaFIVData_RBS();
      }
      O631->ZapolnMIV();
    }
  }
}
//---------------------------------------------------------------------------

void __fastcall TFDanOdinBort::N3Click(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
      MVysErr2->Checked=O631->OO[Indx].IsFalseVysota;
      MRasmatrivat2->Checked=!O631->OO[Indx].IsklOtch;
    }else{
      MVysErr2->Checked=false;
      MRasmatrivat2->Checked=false;
    }
  }else{
    MVysErr2->Checked=false;
    MRasmatrivat2->Checked=false;
  }
}
//---------------------------------------------------------------------------





void __fastcall TFDanOdinBort::MRasmatrivatClick(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
       O631->OO[Indx].IsklOtch=MRasmatrivat->Checked;
       if(O631->OO[Indx].IsklOtch)
       {
          StringGrid1->Cells[9][StringGrid1->Row]="Excluded";
       }else{
          StringGrid1->Cells[9][StringGrid1->Row]="Included";
       }
       if(Type==1)
       {
          IzmenFalseVysotaFDOB_UVD();
          IzmenFalseVysotaFIVData_UVD();
          FGlav631->O631_UVD->ZapolnitCheckListBox(FGlav631->CLB_UVD_Borta);
       }else{
          IzmenFalseVysotaFDOB_RBS();
          IzmenFalseVysotaFIVData_RBS();
          FGlav631->O631_RBS->ZapolnitCheckListBox(FGlav631->CLB_RBS_Borta);
       }
    }
  }
}
//---------------------------------------------------------------------------


void __fastcall TFDanOdinBort::MRasmatrivat2Click(TObject *Sender)
{
  OTSCHETS631* O631;
  O631=(OTSCHETS631*)Otchs631;
  int Indx;
  if(StringGrid1->Row<StringGrid1->RowCount&&StringGrid1->Row>0)
  {
    Indx=StringGrid1->Cells[0][StringGrid1->Row].ToInt();
    if(Indx>=0)
    {
       O631->OO[Indx].IsklOtch=MRasmatrivat2->Checked;
       if(O631->OO[Indx].IsklOtch)
       {
          StringGrid1->Cells[9][StringGrid1->Row]="Excluded";
       }else{
          StringGrid1->Cells[9][StringGrid1->Row]="Included";
       }
       if(Type==1)
       {
          IzmenFalseVysotaFDOB_UVD();
          IzmenFalseVysotaFIVData_UVD();
          FGlav631->O631_UVD->ZapolnitCheckListBox(FGlav631->CLB_UVD_Borta);
       }else{
          IzmenFalseVysotaFDOB_RBS();
          IzmenFalseVysotaFIVData_RBS();
          FGlav631->O631_RBS->ZapolnitCheckListBox(FGlav631->CLB_RBS_Borta);
       }
    }
  }
}
//---------------------------------------------------------------------------


void __fastcall TFDanOdinBort::FormHide(TObject *Sender)
{
  StringGrid1->RowCount=2;
  if(Type==1)
  {
    CloseFDOB_UVD(IndexBort);

  }else{
    CloseFDOB_RBS(IndexBort);
  }


}
//---------------------------------------------------------------------------

void __fastcall TFDanOdinBort::CB_FilterOnlyClick(TObject *Sender)
{
    VisualizeDataOfBort();
}
//---------------------------------------------------------------------------

void __fastcall TFDanOdinBort::N5Click(TObject *Sender)
{
  AnsiString HhpStr;

  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }

    HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);

}
//---------------------------------------------------------------------------




void __fastcall TFDanOdinBort::FormCreate(TObject *Sender)
{
        iffWorkWithIniFile(this, String("iskaj_vis.ini"));        
}
//---------------------------------------------------------------------------









void __fastcall TFDanOdinBort::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==VK_F1)
  {
            N5Click(NULL);
  }        
}
//---------------------------------------------------------------------------

