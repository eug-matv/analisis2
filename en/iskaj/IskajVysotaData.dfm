�
 TFISKAJVYSOTADATA 0K  TPF0TFIskajVysotaDataFIskajVysotaDataLeftETop� Width�HeightCaptionAltitude distortionColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	Menu	MainMenu1OldCreateOrderOnClose	FormCloseOnCreate
FormCreate	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�HeightUAlignalTopTabOrder  TLabelLabel1LeftTopWidthqHeightCaptionTypes of displayed plotsFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxCBVidyOtschetovLeftTopWidthHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrder TextCBVidyOtschetovOnChangeCBVidyOtschetovChange  	TCheckBoxCBVyvodOnlyOptionLeftTop0Width�HeightCaption'Display only selected by criteria plotsChecked	State	cbCheckedTabOrderVisibleOnClickCBVyvodOnlyOptionClick  	TCheckBoxCBNotVyvodEsliMaloLeftTopBWidthaHeightCaption3Do not display if plots of aircraft are not enough TabOrderVisibleOnClickCBNotVyvodEsliMaloClick   TStringGridStringGrid1Left TopUWidth�Height�AlignalClientDefaultRowHeight	FixedCols OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine 	PopupMenu
PopupMenu1TabOrder  
TPopupMenu
PopupMenu1OnPopupPopupMenu1PopupLeft� Top�  	TMenuItemN1CaptionView all dataOnClickN1Click  	TMenuItemN2CaptionView aircraft dataOnClickN2Click  	TMenuItemMVysErrCaptionAltitude errorOnClickMVysErrClick  	TMenuItemN3CaptionChange numberVisible   	TMainMenu	MainMenu1LeftxTop 	TMenuItemN4CaptionDataOnClickN4Click 	TMenuItemN5CaptionView all dataOnClickN1Click  	TMenuItemN6CaptionView aircraft dataOnClickN2Click  	TMenuItemMVysErr2CaptionAltitude errorOnClickMVysErr2Click  	TMenuItemN8CaptionChange numberVisible   	TMenuItemN7CaptionHelpOnClickN7Click    