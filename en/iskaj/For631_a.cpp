/*
  � ������ For631_a.cpp ����������� ������ ������ OTSCHETS631 ��� ���������
  ���������� � ����������� ����������� ��������
*/



//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <stdio.h>
#include "For631.h"
#include "For631_2.h"
#include "For631_a.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#define ABS(X) (((X) > 0) ? (X) : (-(X)))
//������������� ��������� ������ ������, ������� ���������� ������

int MyPutsFor631_a(char *Strka,FILE *fp)
{
  if(Strka[2]=='3')
  {
     Strka[117]='\n';
     Strka[118]='\0';

  }else{
     Strka[103]='\n';
     Strka[104]=0;
  }
  fputs(Strka,fp);
  return 1;
}

AnsiString MyStringFor631_a(AnsiString Strka)
{
  if(Strka[3]=='3')
  {
    return Strka.SubString(1,117);
  }
  return Strka.SubString(1,103);
}



int OTSCHETS631::VyvodToStringIskaj(
                          TStrings *Strings,
                          int Zona,   //1, 2 ,3, 4
                          int Type,    //1 -���, 2 - RBS
                          AnsiString FN,
                          AnsiString *FN_Plots,int N_of_FN_Plots
                                   )
{

  Strings->Clear();
  AnsiString Strka;
  char Strka_c[100],Strka_c1[20],Strka_c2[20];
  double P1,P2, Pisk;
  int N_of_O, N_of_PO, N_of_V, N_of_N, N_of_Nul,N_of_Err;
  long Tek1=0;
  int i;
  int IB; //������ �����
  AnsiString TempFN;
  FILE *fp_TempFN;
//��� �����
  TempFN=FN+String(".")+String(Tek1);
  while(FileExists(TempFN))
  {
    Tek1++;
    TempFN=FN+String(".")+String(Tek1);
  };

  fp_TempFN=fopen(TempFN.c_str(),"w");




//������� ����� ������
  for(i=0;i<N_of_FN_Plots;i++)
  {
    fputs(FN_Plots[i].c_str(),fp_TempFN);
    fputs("\n",fp_TempFN);
  }


  if(Type==1)
  {
    fputs("Data on aircrafts.UVD\n",fp_TempFN);
  }else{
    fputs("Data on aircrafts.RBS\n",fp_TempFN);
  }

//���������� � Zona
  if(Zona==1)
  {
    fprintf(fp_TempFN,"Single in strobe %.2lf �\n",Strob);
  }else if(Zona==2)
  {
    fprintf(fp_TempFN,"Other plots in strobe %.2lf � \n",Strob);
    fprintf(fp_TempFN,"from %.2lf km\n",MaxDD);
  }else if(Zona==3)
  {
    fprintf(fp_TempFN,"Other plots in strobe %.2lf � \n",Strob);
    fprintf(fp_TempFN,"from %.2lf and to %.2lf km\n",MinDD, MaxDD);
  }else if(Zona==4)
  {
    fprintf(fp_TempFN,"Other plots in strobe %.2lf �\n",Strob);
    fprintf(fp_TempFN,"to %.2lf km\n",MinDD);
  }else{
    fprintf(fp_TempFN,"Single in strobe %.2lf �,\n",Strob);
    fprintf(fp_TempFN,"or from %.2lf km\n",MaxDD);
  }

//�������� ������
  if(IsA1)
  {
      fprintf(fp_TempFN,"Azimuth:  %.2lf - %.2lf� \n", A11, A12);
      if(IsA2)
      {
        fprintf(fp_TempFN,"and: %.2lf - %.2lf� \n", A21, A22);
      }
  }
  if(IsDlnst)
  {
      fprintf(fp_TempFN,"Range: %.2lf - %.2lf km\n", MinDlnst, MaxDlnst);
  }
  if(IsVysota)
  {
      fprintf(fp_TempFN,"Altitude:   %d - %d m\n", Vysota1, Vysota2);
  }

  fprintf(fp_TempFN,"Number of plots: %d\n", ObshRezultNotFilter.ReturnN_of_O(Zona));
  fprintf(fp_TempFN,"Number of plots with number distortion: %d\n",
          ObshRezultNotFilter.ReturnN_of_V(Zona));


  fprintf(fp_TempFN,"Number of plots with altitude distortion: %d\n",
          ObshRezultNotFilter.ReturnN_of_N(Zona));

  fprintf(fp_TempFN,"Number of plots  unidentified information by altitude: %d\n",
          ObshRezultNotFilter.ReturnN_of_Nul(Zona));

  fprintf(fp_TempFN,"Number of plots  unidentified information by number: %d\n",
          ObshRezultNotFilter.ReturnN_of_NulN(Zona));

  P1=ObshRezultNotFilter.ReturnP(Zona);
  fprintf(fp_TempFN,"P(Plot has reliable information about altitude): %.5lf\n",
          P1);
  fprintf(fp_TempFN,"P(Plot has reliable information about altitude)*0.88=%.5lf\n",P1*0.88);
  P1=ObshRezultNotFilter.ReturnPN(Zona);
  fprintf(fp_TempFN,"P(Plot has reliable information about number): %.5lf\n",P1);
  fprintf(fp_TempFN,"P(Plot has reliable information about number)*0.88=%.5lf\n",P1*0.88);
  fprintf(fp_TempFN,
          "Number of plots with non-zero additional information = %ld\n",
          ObshRezultNotFilter.ReturnN_of_NotNull(Zona));
  fprintf(fp_TempFN,
          "Number of plots with distorted additional information=%ld\n",
          ObshRezultNotFilter.ReturnN_of_NotNull(Zona)-
          ObshRezultNotFilter.ReturnN_of_PO(Zona));

  if(ObshRezultNotFilter.ReturnPI(Zona)<0)
  {
    fprintf(fp_TempFN,"Probability of distorted additional information is not defined \n");
  }else{
    fprintf(fp_TempFN,"Probability of distorted additional information =%.5lf\n",
            ObshRezultNotFilter.ReturnPI(Zona));
  }

  fprintf(fp_TempFN,"Exclude aircrafts with conservative detection probability values. P*88\n");
  fprintf(fp_TempFN,"Number of plots: %d\n", ObshRezultWithFilterV.ReturnN_of_O(Zona));
  fprintf(fp_TempFN,"Number of plots with distorted altitude(0.88): %d\n",
          ObshRezultWithFilterV.ReturnN_of_V(Zona));
  fprintf(fp_TempFN,"Number of plots with undetected  altitude(0.88): %d\n",
            ObshRezultWithFilterV.ReturnN_of_Nul(Zona));
  P1=ObshRezultWithFilterV.ReturnP(Zona);
  fprintf(fp_TempFN,"P(Plot has reliable additional information by altitude)(0.88): %.5lf\n",
          P1);




  fprintf(fp_TempFN,"Number of plots for assessment distorted aircraft's numbers: %d\n", ObshRezultWithFilterN.ReturnN_of_O(Zona));

  fprintf(fp_TempFN,"Number of plots with distorted number(0.88): %d\n",
          ObshRezultWithFilterN.ReturnN_of_N(Zona));
  fprintf(fp_TempFN,"Number of plots(0.88): %d\n",
          ObshRezultWithFilterN.ReturnN_of_NulN(Zona));

  P1=ObshRezultWithFilterN.ReturnPN(Zona);
  fprintf(fp_TempFN,"P(Plot has reliable additional information by number)(0.88): %.5lf\n",
          P1);


  fprintf(fp_TempFN,"\n\nAircrafts information\n");

  fprintf(fp_TempFN," Air. � | M.count|ZeroAlt.|Err.Alt.|ZeroNum.|Err.Num.| P(Alt.)| P(Num.)| P(dis.)|\n");
  for(int i=0;i<N_of_Borts;i++)
  {
    IB=OSB[i].IndexBort;
    if(IsklBort[IB])continue;
    if(IskForNotFilter[IB].N_of_O<MinBN_of_Otsch)continue;
    if(IskForNotFilter[IB].ReturnN_of_O(Zona)<MinBN_of_Otsch)continue;
    if(NomBort[IB]==0)continue;

    if(IskForNotFilter[IB].ReturnPI(Zona)<0)
    {
       sprintf(Strka_c1,"not.def");
    }else{
       sprintf(Strka_c1,"%8.06lf",IskForNotFilter[IB].ReturnPI(Zona));
    }

    fprintf(fp_TempFN,"�%6d |%-8d|%-8d|%-8d|%-8d|%-8d|%8.06lf|%8.06lf|%s|\n",//%d",
            NomBort[IB],
            IskForNotFilter[IB].ReturnN_of_O(Zona),
            IskForNotFilter[IB].ReturnN_of_Nul(Zona),
            IskForNotFilter[IB].ReturnN_of_V(Zona),
            IskForNotFilter[IB].ReturnN_of_NulN(Zona),
            IskForNotFilter[IB].ReturnN_of_N(Zona),
            IskForNotFilter[IB].ReturnP(Zona),
            IskForNotFilter[IB].ReturnPN(Zona),
            Strka_c1//,
//            IskForNotFilter[IB].ReturnN_of_PO(Zona)
            );
  }



//������� ���������

  int kprom=0,Tek,DlinaStroki;
  char Strka1[500], *RetC;

  FILE *fp1;
  fp1=fopen(FN.c_str(),"r");


  if(!fp1)
  {
     return 0;
  }



  Tek=GlobFirstOtschet(true,true,false,false,true);
//��������� �� �������.
  fprintf(fp_TempFN,
    "\n\n\nNumber distortion. Second plot is distorted\n");
  kprom=0;
  while(Tek>=0)
  {
    if(OO[Tek].IsNotTraekt)
    {
       Tek=GlobNextOtschet(Tek,true,true,false,false,true);
       continue;
    }
    if(OO[Tek].Nomer!=NomBort[OO[Tek].IndexNomer]&&
       OO[Tek].Nomer!=0)
    {
//�������� ����� �������� � ����
        if(Zona>=1&&Zona<=4)
        {
           if(OO[Tek].Zona!=Zona)
           {
             Tek=GlobNextOtschet(Tek,true,true,false,false,true);
             continue;
           }
        }else{
           if(OO[Tek].Zona!=1&&OO[Tek].Zona!=2)
           {
             Tek=GlobNextOtschet(Tek,true,true,false,false,true);
             continue;
           }
        }

        IB=OO[Tek].IndexNomer;
        if(!IsklBort[IB]&&
           IskForNotFilter[IB].N_of_O>=MinBN_of_Otsch&&
           IskForNotFilter[IB].ReturnN_of_O(Zona)>=MinBN_of_Otsch&&
           NomBort[IB]!=0
           )
        {


           kprom++;
           fprintf(fp_TempFN,"\n%d\n",kprom);
           Tek1=PrevOtschet(Tek,true,false,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
              fprintf(fp_TempFN,"******************No prev plot****************\n");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             MyPutsFor631_a(Strka1,fp_TempFN);
           }
           fseek(fp1,OO[Tek].Smesh,0);
           fgets(Strka1,499,fp1);
           MyPutsFor631_a(Strka1,fp_TempFN);
           Tek1=NextOtschet(Tek,true,false,false,false);
           if(Tek1<0) //��� ���������� �������� �� ������
           {
              fprintf(fp_TempFN,"******************No next plot****************\n");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             MyPutsFor631_a(Strka1,fp_TempFN);
           }

        }

    }



    Tek=GlobNextOtschet(Tek,true,true,false,false,true);
  }


  kprom=0;
  Tek=GlobFirstOtschet(true,true,true,false,true);
//��������� �� ������.
  fprintf(fp_TempFN,"\n\n\n Altitude distortion. Second plot is distorted\n");

  while(Tek>=0)
  {
    if(OO[Tek].IsNotTraekt)
    {
       Tek=GlobNextOtschet(Tek,true,true,true,false,true);
       continue;
    }
    if(OO[Tek].IsFalseVysota)
    {
//�������� ����� �������� � ����
        if(Zona>=1&&Zona<=4)
        {
           if(OO[Tek].Zona!=Zona)
           {
             Tek=GlobNextOtschet(Tek,true,true,true,false,true);
             continue;
           }
        }else{
           if(OO[Tek].Zona!=1&&OO[Tek].Zona!=2)
           {
             Tek=GlobNextOtschet(Tek,true,true,true,false,true);
             continue;
           }
        }

   //��� ��� ������������� ������ ������
        IB=OO[Tek].IndexNomer;
        if(!IsklBort[IB]&&
           IskForNotFilter[IB].N_of_O>=MinBN_of_Otsch&&
           IskForNotFilter[IB].ReturnN_of_O(Zona)>=MinBN_of_Otsch&&
           NomBort[IB]!=0
           )
        {


           kprom++;
           fprintf(fp_TempFN,"\n%d\n",kprom);
           Tek1=PrevOtschet(Tek,true,true,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
              fprintf(fp_TempFN,"******************No prev plot****************\n");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             MyPutsFor631_a(Strka1,fp_TempFN);
           }
           fseek(fp1,OO[Tek].Smesh,0);
           fgets(Strka1,499,fp1);
           MyPutsFor631_a(Strka1,fp_TempFN);
           Tek1=NextOtschet(Tek,true,true,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
              fprintf(fp_TempFN,"******************No next plot****************\n");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             MyPutsFor631_a(Strka1,fp_TempFN);
           }

        }

    }



    Tek=GlobNextOtschet(Tek,true,true,true,false,true);
  }


  Tek=GlobFirstOtschet(true,true,false,false,true);
//������� ������.

  fprintf(fp_TempFN,
   "\n\n\nThere is aircraft plots with zero number. Second plot is unidentified by numbers\n");
  kprom=0;
  while(Tek>=0)
  {
    if(OO[Tek].IsNotTraekt)
    {
       Tek=GlobNextOtschet(Tek,true,true,false,false,true);
       continue;
    }
    if(OO[Tek].Nomer!=NomBort[OO[Tek].IndexNomer]&&
       OO[Tek].Nomer==0)
    {
//�������� ����� �������� � ����
        if(Zona>=1&&Zona<=4)
        {
           if(OO[Tek].Zona!=Zona)
           {
             Tek=GlobNextOtschet(Tek,true,true,false,false,true);
             continue;
           }
        }else{
           if(OO[Tek].Zona!=1&&OO[Tek].Zona!=2)
           {
             Tek=GlobNextOtschet(Tek,true,true,false,false,true);
             continue;
           }
        }
        IB=OO[Tek].IndexNomer;
        if(!IsklBort[IB]&&
           IskForNotFilter[IB].N_of_O>=MinBN_of_Otsch&&
           IskForNotFilter[IB].ReturnN_of_O(Zona)>=MinBN_of_Otsch&&
           NomBort[IB]!=0
           )
        {


           kprom++;
           fprintf(fp_TempFN,"\n%d\n",kprom);
           Tek1=PrevOtschet(Tek,true,false,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
             fprintf(fp_TempFN,"******************No prev plot****************\n");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             MyPutsFor631_a(Strka1,fp_TempFN);
           }
           fseek(fp1,OO[Tek].Smesh,0);
           fgets(Strka1,499,fp1);
           MyPutsFor631_a(Strka1,fp_TempFN);
           Tek1=NextOtschet(Tek,true,false,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
             fprintf(fp_TempFN,"******************No next plot****************\n");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             MyPutsFor631_a(Strka1,fp_TempFN);
           }

        }

    }
    Tek=GlobNextOtschet(Tek,true,true,false,false,true);
  }


  kprom=0;
  Tek=GlobFirstOtschet(true,true,false,false,true);

//������� � �������� ��������
  fputs("\n\n\nPlots unidentified by altitude.\n", fp_TempFN);
  while(Tek>=0)
  {
    if(OO[Tek].IsNotTraekt)
    {
       Tek=GlobNextOtschet(Tek,true,true,false,false,true);
       continue;
    }
    if(OO[Tek].Vysota==0)
    {
//�������� ����� �������� � ����
        if(Zona>=1&&Zona<=4)
        {
           if(OO[Tek].Zona!=Zona)
           {
             Tek=GlobNextOtschet(Tek,true,true,false,false,true);
             continue;
           }
        }else{
           if(OO[Tek].Zona!=1&&OO[Tek].Zona!=2)
           {
             Tek=GlobNextOtschet(Tek,true,true,false,false,true);
             continue;
           }
        }
        N_of_O=ChisloOtschetov(OO[Tek].IndexNomer,false,true);
        if(N_of_O<MinBN_of_Otsch)
        {
           Tek=GlobNextOtschet(Tek,true,true,false,false,true);
           continue;
        }

   //��� ��� ������������� ������ ������
        IB=OO[Tek].IndexNomer;
        if(!IsklBort[IB]&&
           IskForNotFilter[IB].N_of_O>=MinBN_of_Otsch&&
           IskForNotFilter[IB].ReturnN_of_O(Zona)>=MinBN_of_Otsch&&
           NomBort[IB]!=0
           )
        {


           kprom++;
           fprintf(fp_TempFN,"\n%d\n",kprom);
           fseek(fp1,OO[Tek].Smesh,0);
           fgets(Strka1,499,fp1);
           MyPutsFor631_a(Strka1,fp_TempFN);
        }

    }



    Tek=GlobNextOtschet(Tek,true,true,false,false,true);
  }
  fclose(fp1);
  fclose(fp_TempFN);

//�������� ������ �� ����� 
  Strings->LoadFromFile(TempFN);
  DeleteFile(TempFN);


  return 1;
}



int OTSCHETS631::VyvodToStringIskaj0001(
                          TStrings *Strings,
                          int Type,    //1 -���, 2 - RBS
                          AnsiString FN,
                          AnsiString *FN_Plots,int N_of_FN_Plots
                                   )
{

  Strings->Clear();
  AnsiString Strka;
  int i;
  char Strka_c[100],Strka_c1[20],Strka_c2[20];
  double P1,P2, Pisk;
  int N_of_O, N_of_PO, N_of_V, N_of_N, N_of_Nul,N_of_Err;
  long Tek1;
  int IB; //������ �����

  long ObshN_of_Otch=0;         //����� ����� ��������������� ��������
  long ObshN_of_OtchBezNull=0;  //����� ����� ��������, ��� ���������� � ������ � �����
  long ObshN_of_OtchN=0;        //����� �������� � ��������� �������
  long ObshN_of_OtchV=0;        //����� �������� � ��������� �������
  long ObshN_of_Iskaj=0;        //����� ����� ��������� �������� �� ������� � �������
  long ObshN_of_IskajN=0;       //����� ����� ��������� �� �������
  long ObshN_of_IskajV=0;
  long ObshN_of_NullV=0;
  long ObshN_of_NullN=0;
  double P;
//��� �����

//  Strings->Add(FN);
  for(int i=0;i<N_of_FN_Plots;i++)
  {
    Strings->Add(FN_Plots[i]);
  }


  if(Type==1)
  {
    Strings->Add("Aircraft data. UVD");
  }else{
    Strings->Add("Aircraft data. RBS");
  }

  //����������� ��������� ��� ������������� ����������
  Strings->Add("Check the probability of additional information distortion or confusion");

//�������� ������
  if(IsA1)
  {
      sprintf(Strka_c,"Azimuth from %.2lf to %.2lf �", A11, A12);
      Strings->Add(Strka_c);
      if(IsA2)
      {
        sprintf(Strka_c,"and from %.2lf to %.2lf �", A21, A22);
        Strings->Add(Strka_c);
      }
  }
  if(IsDlnst)
  {
      sprintf(Strka_c,"Range: %.2lf - %.2lf km", MinDlnst, MaxDlnst);
      Strings->Add(Strka_c);
  }
  if(IsVysota)
  {
      sprintf(Strka_c,"Altitude:  %d - %d m", Vysota1, Vysota2);
      Strings->Add(Strka_c);
  }

//���������� ����� ����� ��������


  for(int i=0;i<N_of_Borts;i++)
  {
    if(IsklBort[i])continue;
    ObshN_of_Otch+=IskForNotFilter[i].N_of_O1+IskForNotFilter[i].N_of_O2+
                   IskForNotFilter[i].N_of_O3+IskForNotFilter[i].N_of_O4;

    ObshN_of_OtchBezNull+=IskForNotFilter[i].N_of_NotNull1+IskForNotFilter[i].N_of_NotNull2+
     IskForNotFilter[i].N_of_NotNull3+IskForNotFilter[i].N_of_NotNull4;

    ObshN_of_NullN+=IskForNotFilter[i].N_of_NulN1+IskForNotFilter[i].N_of_NulN2+
                    IskForNotFilter[i].N_of_NulN3+IskForNotFilter[i].N_of_NulN4;

    ObshN_of_NullV+=IskForNotFilter[i].N_of_Nul1+IskForNotFilter[i].N_of_Nul2+
                    IskForNotFilter[i].N_of_Nul3+IskForNotFilter[i].N_of_Nul4;

    ObshN_of_IskajN+=IskForNotFilter[i].N_of_N1+IskForNotFilter[i].N_of_N2+
                     IskForNotFilter[i].N_of_N3+IskForNotFilter[i].N_of_N4;

    ObshN_of_IskajV+=IskForNotFilter[i].N_of_V1+IskForNotFilter[i].N_of_V2+
                     IskForNotFilter[i].N_of_V3+IskForNotFilter[i].N_of_V4;

    ObshN_of_Iskaj+=IskForNotFilter[i].N_of_NotNull1+
                    IskForNotFilter[i].N_of_NotNull2+
                    IskForNotFilter[i].N_of_NotNull3+
                    IskForNotFilter[i].N_of_NotNull4-
               IskForNotFilter[i].N_of_PO1-IskForNotFilter[i].N_of_PO2-
               IskForNotFilter[i].N_of_PO3-IskForNotFilter[i].N_of_PO4;


  }
  ObshN_of_OtchN=ObshN_of_Otch-ObshN_of_NullN;
  ObshN_of_OtchV=ObshN_of_Otch-ObshN_of_NullV;


  sprintf(Strka_c,"Number of plots: %d", ObshN_of_Otch);
  Strings->Add(Strka_c);

  sprintf(Strka_c,"Number of plots with non-zero number: %d",
          ObshN_of_OtchN);
  Strings->Add(Strka_c);

  sprintf(Strka_c,"Number of plots with distorted number: %d",
          ObshN_of_IskajN);
  Strings->Add(Strka_c);

  if(ObshN_of_OtchN)
  {
 sprintf(Strka_c,"The probability of number distortion: %.5lf",
            1.0*ObshN_of_IskajN/ObshN_of_OtchN );
  }else{
sprintf(Strka_c,"The probability of number distortion is unknown");
  }
  Strings->Add(Strka_c);


  sprintf(Strka_c,"Number of plots with non-zero altitude: %d",
          ObshN_of_OtchV);
  Strings->Add(Strka_c);

  sprintf(Strka_c,"Number of plots with distorted altitude: %d",
          ObshN_of_IskajV);
  Strings->Add(Strka_c);

  if(ObshN_of_OtchV)
  {
   sprintf(Strka_c,"The probability of altitude distortion: %.5lf",
            1.0*ObshN_of_IskajV/ObshN_of_OtchV );
  }else{
   sprintf(Strka_c,"The probability of altitude distortion is unknown");
  }
  Strings->Add(Strka_c);

  sprintf(Strka_c,"Number of plots with non-zero additional informationY: %d",
          ObshN_of_OtchBezNull);
  Strings->Add(Strka_c);

  sprintf(Strka_c,"Number of plots with distorted additional information: %d",
          ObshN_of_Iskaj);
  Strings->Add(Strka_c);

  if(ObshN_of_OtchBezNull)
  {
   sprintf(Strka_c,"The probability of additional information distortion or confusion: %.5lf",
            1.0*ObshN_of_Iskaj/ObshN_of_OtchBezNull );
  }else{
   sprintf(Strka_c,"The probability of additional information distortion or confusion is unknown");
  }
  Strings->Add(Strka_c);



  Strings->Add("");
  Strings->Add("");

  sprintf(Strka_c," Air. � |M.Count |Nil.Alt.|Err.Alt.|Nil.Num.|Err.Num.|");
  Strings->Add(Strka_c);

  for(int i=0;i<N_of_Borts;i++)
  {
    IB=OSB[i].IndexBort;
    if(IsklBort[IB])continue;
  //  if(IskForNotFilter[IB].N_of_O<MinBN_of_Otsch)continue;
 //   if(IskForNotFilter[IB].ReturnN_of_O(Zona)<MinBN_of_Otsch)continue;
    if(NomBort[IB]==0)continue;
    if(IskForNotFilter[IB].N_of_O1+IskForNotFilter[IB].N_of_O2+
       IskForNotFilter[IB].N_of_O3+IskForNotFilter[IB].N_of_O4==0)continue;

    sprintf(Strka_c,"�%6d |%-8d|%-8d|%-8d|%-8d|%-8d|",//%d",
            NomBort[IB],
 IskForNotFilter[IB].ReturnN_of_O(1)+IskForNotFilter[IB].ReturnN_of_O(2)+
 IskForNotFilter[IB].ReturnN_of_O(3)+IskForNotFilter[IB].ReturnN_of_O(4),
 IskForNotFilter[IB].ReturnN_of_Nul(1)+IskForNotFilter[IB].ReturnN_of_Nul(2)+
 IskForNotFilter[IB].ReturnN_of_Nul(3)+IskForNotFilter[IB].ReturnN_of_Nul(4),
 IskForNotFilter[IB].ReturnN_of_V(1)+IskForNotFilter[IB].ReturnN_of_V(2)+
 IskForNotFilter[IB].ReturnN_of_V(3)+IskForNotFilter[IB].ReturnN_of_V(4),
 IskForNotFilter[IB].ReturnN_of_NulN(1)+ IskForNotFilter[IB].ReturnN_of_NulN(2)+
 IskForNotFilter[IB].ReturnN_of_NulN(3)+ IskForNotFilter[IB].ReturnN_of_NulN(4),
 IskForNotFilter[IB].ReturnN_of_N(1)+IskForNotFilter[IB].ReturnN_of_N(2)+
 IskForNotFilter[IB].ReturnN_of_N(3)+IskForNotFilter[IB].ReturnN_of_N(4)
//            IskForNotFilter[IB].ReturnN_of_PO(Zona)
            );
    Strings->Add(Strka_c);
  }


  Strings->Add("");  Strings->Add("");  Strings->Add("");
//������� ���������

  int kprom=0,Tek,DlinaStroki;
  char Strka1[500], *RetC;

  FILE *fp1;
  fp1=fopen(FN.c_str(),"r");


  if(!fp1)
  {
     return 0;
  }



  Tek=GlobFirstOtschet(true,true,false,false,true);
//��������� �� �������.
  Strings->Add("Number distortions. Second plot is distorted by number");
  kprom=0;
  while(Tek>=0)
  {
    if(OO[Tek].IsNotTraekt)
    {
       Tek=GlobNextOtschet(Tek,true,true,false,false,true);
       continue;
    }
    if(OO[Tek].Nomer!=NomBort[OO[Tek].IndexNomer]&&
       OO[Tek].Nomer!=0)
    {

        IB=OO[Tek].IndexNomer;
        if(!IsklBort[IB]&&NomBort[IB]!=0)
        {

           kprom++;
           Strings->Add("");
           Strings->Add(String(kprom)+String(" Tail number:")+String(NomBort[IB]));
           Tek1=PrevOtschet(Tek,true,false,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
              Strings->Add("******************No previous plot****************");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             DlinaStroki=strlen(Strka1);
             if(Strka1[DlinaStroki-1]=='\n')Strka1[DlinaStroki-1]=0;
             Strings->Add(MyStringFor631_a(Strka1));
           }
           fseek(fp1,OO[Tek].Smesh,0);
           fgets(Strka1,499,fp1);
           DlinaStroki=strlen(Strka1);
           if(Strka1[DlinaStroki-1]=='\n')Strka1[DlinaStroki-1]=0;
           Strings->Add(MyStringFor631_a(Strka1));
           Tek1=NextOtschet(Tek,true,false,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
              Strings->Add("******************No next plot****************");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             DlinaStroki=strlen(Strka1);
             if(Strka1[DlinaStroki-1]=='\n')Strka1[DlinaStroki-1]=0;
             Strings->Add(MyStringFor631_a(Strka1));
           }

        }

    }
    Tek=GlobNextOtschet(Tek,true,true,false,false,true);
  }


  Strings->Add("");  Strings->Add("");  Strings->Add("");
  kprom=0;
  Tek=GlobFirstOtschet(true,true,true,false,true);
//��������� �� ������.
  Strings->Add("Altitude distortions. Second plot is distorted by altitude");

  while(Tek>=0)
  {
    if(OO[Tek].IsNotTraekt)
    {
       Tek=GlobNextOtschet(Tek,true,true,true,false,true);
       continue;
    }
    if(OO[Tek].IsFalseVysota)
    {
//�������� ����� �������� � ����

   //��� ��� ������������� ������ ������
        IB=OO[Tek].IndexNomer;
        if(!IsklBort[IB]&&NomBort[IB]!=0)
        {


           kprom++;
           Strings->Add("");
           Strings->Add(kprom);
           Tek1=PrevOtschet(Tek,true,true,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
              Strings->Add("******************No previous plot****************");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             DlinaStroki=strlen(Strka1);
             if(Strka1[DlinaStroki-1]=='\n')Strka1[DlinaStroki-1]=0;
             Strings->Add(MyStringFor631_a(Strka1));
           }
           fseek(fp1,OO[Tek].Smesh,0);
           fgets(Strka1,499,fp1);
           DlinaStroki=strlen(Strka1);
           if(Strka1[DlinaStroki-1]=='\n')Strka1[DlinaStroki-1]=0;
           Strings->Add(MyStringFor631_a(Strka1));
           Tek1=NextOtschet(Tek,true,true,false,false);
           if(Tek1<0) //��� ����������� �������� �� ������
           {
              Strings->Add("******************No next plot****************");
           }else{
             fseek(fp1,OO[Tek1].Smesh,0);
             fgets(Strka1,499,fp1);
             DlinaStroki=strlen(Strka1);
             if(Strka1[DlinaStroki-1]=='\n')Strka1[DlinaStroki-1]=0;
             Strings->Add(MyStringFor631_a(Strka1));
           }

        }

    }



    Tek=GlobNextOtschet(Tek,true,true,true,false,true);
  }

  fclose(fp1);
  return 1;
}




















