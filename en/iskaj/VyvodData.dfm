�
 TFODATA 0�
  TPF0TFODataFODataLeft� Top^WidthDHeight�Caption	All plotsColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	Menu	MainMenu1OldCreateOrderOnClose	FormCloseOnCreate
FormCreate	OnKeyDownFormKeyDownOnResize
FormResizeOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width<HeightQAlignalTopTabOrder  	TGroupBox	GroupBox1LeftTopWidth� HeightOAlignalLeftCaptionWhat plots to displayTabOrder  	TCheckBox
CBIsklBortLeftTopWidth� HeightCaptionExcluded aircraftsChecked	State	cbCheckedTabOrder OnClickCBIsklBortClick  	TCheckBoxCBIsklOtschLeftTop%Width� HeightCaptionExcluded plotsChecked	State	cbCheckedTabOrderOnClickCBIsklOtschClick  	TCheckBoxCBFilterOnlyLeftTop8Width� HeightCaptionOnly relevant criteriaTabOrderOnClickCBFilterOnlyClick   	TGroupBox	GroupBox2Left� TopWidth� HeightOAlignalLeftCaption!Finding aircrafts with rare plotsTabOrderVisible TLabelLabel1LeftTopWidthlHeightCaptionA suspicious n.of plots:  TLabelLabel2LeftTop"WidthiHeightCaptionList of suspicious plots  
TCSpinEditCSEMinN_of_OLeft� TopWidth/HeightTabOrder Value  	TComboBox	ComboBox1LeftTop2WidthkHeight
ItemHeightTabOrderText	ComboBox1  TButtonButton1Left{Top0Width1HeightCaptionFindTabOrder    TStringGridStringGrid1Left TopQWidth,HeightmAlignalClientBorderStylebsNoneDefaultRowHeight	FixedColsRowCount	PopupMenu
PopupMenu1
ScrollBarsssHorizontalTabOrder	OnKeyDownStringGrid1KeyDown
RowHeights   
TScrollBar
ScrollBar1Left,TopQWidthHeightmAlignalRightKind
sbVerticalMax'PageSize Position�TabOrderOnChangeScrollBar1Change  TTimerTimer1EnabledInterval2OnTimerTimer1TimerLeft� TopP  TTimerTimer2EnabledIntervaldLeft0Top`  
TPopupMenu
PopupMenu1OnPopupPopupMenu1PopupLeftPTop�  	TMenuItemN1CaptionView aircraft dataOnClickN1Click  	TMenuItemMVysErrCaptionAltitude errorOnClickMVysErrClick  	TMenuItemMRasmatrivatCaptionConsider plotOnClickMRasmatrivatClick  	TMenuItemN2CaptionChange numberOnClickN2Click   	TMainMenu	MainMenu1Left� Top  	TMenuItemN3CaptionDataOnClickN3Click 	TMenuItemN4CaptionView aircraft dataOnClickN1Click  	TMenuItemMVysErr2CaptionAltitude errorOnClickMVysErr2Click  	TMenuItemMRasmatrivat2CaptionConsider plotOnClickMRasmatrivat2Click  	TMenuItemN5CaptionChange numberOnClickN2Click   	TMenuItemN6CaptionHelpOnClickN6Click    