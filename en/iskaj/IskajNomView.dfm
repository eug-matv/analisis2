�
 TFISKAJNOMVIEW 0w  TPF0TFIskajNomViewFIskajNomViewLeft� Top� WidthHeight�CaptionPlots with distorted numbersColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	Menu	MainMenu1OldCreateOrderPositionpoDefaultPosOnlyOnCreate
FormCreate	OnKeyDownFormKeyDownOnShowFormShowPixelsPerInch`
TextHeight TLabelLNomerLeft Top WidthHeightAlignalTopFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TStringGridStringGrid1Left TopAWidthHeightiAlignalClientColCountDefaultColWidthPDefaultRowHeight	FixedCols 	PopupMenu
PopupMenu1TabOrder 	ColWidthsSPPPPPPP   TPanelPanel1Left TopWidthHeight-AlignalTop
BevelOuterbvNoneTabOrder 	TCheckBox
CB_VklIsklLeft TopWidth� HeightCaptionDisplay also excluded plotsChecked	State	cbCheckedTabOrder OnClickCB_VklIsklClick  	TCheckBoxCB_FilterOnlyLeft TopWidth0HeightCaption'Display only selected by criteria plotsTabOrderOnClickCB_FilterOnlyClick  	TCheckBoxCBVklIsklBortLeft Top�WidthaHeightCaption(Display also plots of excluded aircraftsChecked	State	cbCheckedTabOrderOnClickCB_VklIsklClick   
TPopupMenu
PopupMenu1OnPopupPopupMenu1PopupLeftTop  	TMenuItemN8CaptionView aircraft dataOnClickN8Click  	TMenuItemN1CaptionView all dataOnClickN1Click  	TMenuItemMVysErrCaptionAltitude errorOnClickMVysErrClick  	TMenuItemMRasmatrivatCaptionConsider plotOnClickMRasmatrivatClick  	TMenuItemN2CaptionChange numberVisible   	TMainMenu	MainMenu1Left� Top�  	TMenuItemN3CaptionDataOnClickN3Click 	TMenuItemN7CaptionView aircraft dataOnClickN7Click  	TMenuItemN4CaptionView all dataOnClickN1Click  	TMenuItemMVysErr2CaptionAltitude errorOnClickMVysErr2Click  	TMenuItemMRasmatrivat2CaptionConsider plotOnClickMRasmatrivat2Click  	TMenuItemN6CaptionChange numberVisible   	TMenuItemN5CaptionHelpOnClickN5Click   TActionListActionList1Left�   