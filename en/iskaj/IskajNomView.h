//---------------------------------------------------------------------------
#ifndef IskajNomViewH
#define IskajNomViewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
#include <ActnList.hpp>
//---------------------------------------------------------------------------
class TFIskajNomView : public TForm
{
__published:	// IDE-managed Components
	TLabel *LNomer;
	TStringGrid *StringGrid1;
	TPanel *Panel1;
	TCheckBox *CB_VklIskl;
	TPopupMenu *PopupMenu1;
	TMenuItem *N1;
	TMenuItem *N2;
	TMenuItem *MVysErr;
	TMainMenu *MainMenu1;
	TMenuItem *N3;
	TMenuItem *N4;
	TMenuItem *MVysErr2;
	TMenuItem *N6;
	TCheckBox *CB_FilterOnly;
        TMenuItem *MRasmatrivat;
        TMenuItem *MRasmatrivat2;
        TActionList *ActionList1;
        TCheckBox *CBVklIsklBort;
	TMenuItem *N5;
	TMenuItem *N7;
	TMenuItem *N8;
	void __fastcall CB_VklIsklClick(TObject *Sender);
	void __fastcall FormShow(TObject *Sender);
	void __fastcall PopupMenu1Popup(TObject *Sender);
	void __fastcall MVysErrClick(TObject *Sender);
	void __fastcall N1Click(TObject *Sender);
	void __fastcall MVysErr2Click(TObject *Sender);
	void __fastcall N3Click(TObject *Sender);
        void __fastcall MRasmatrivatClick(TObject *Sender);
        void __fastcall MRasmatrivat2Click(TObject *Sender);
	void __fastcall CB_FilterOnlyClick(TObject *Sender);
	void __fastcall N5Click(TObject *Sender);
	void __fastcall N7Click(TObject *Sender);
	void __fastcall N8Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);

    private:	// User declarations
public:		// User declarations
    void *Otchs631;
    int Type;               //1 - ���, ������ RBS

    AnsiString CHMHelpFile;
    AnsiString CHMHtml;


	__fastcall TFIskajNomView(TComponent* Owner);

//���������� ��� ������ ������ ������� �����
    void __fastcall VisualizeDataOfBort(void);

//���������� �������
    int __fastcall MoveOtchet(int NumLine);
    
//���������� ������ ������
    int __fastcall FindOtschetIndx(int Indx);

//���������� ������ ������ �� ������
    int __fastcall FindOtschetRow(int Row);

//�������� ��� ���� ��������� �� ������
    int __fastcall ObnovIskajVys(void);

};
//---------------------------------------------------------------------------
extern PACKAGE TFIskajNomView *FIskajNomView;
//---------------------------------------------------------------------------
#endif
