//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include <alloc.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <Grids.hpp>
#include <CheckLst.hpp>

//������� ������ ���� CGauges.h
#include "CGAUGES.h"


#include "For631.h"
#include "For631_2.h"
#include "RabsBort.h"
#include "tools631.h"
#include "tools.h"
#include "StringGridKT.h"


#define ABS(X) (((X) > 0) ? (X) : (-(X)))
#define MAX(a, b)  (((a) > (b)) ? (a) : (b))
#define MIN(a, b)  (((a) < (b)) ? (a) : (b))

//---------------------------------------------------------------------------
#pragma package(smart_init)




/*���������� ������ �������*/
void  OTSCHETS631::AddOtschet(
                            long Smesh,
                            long Nomer,
                            long Vysota,
                            double Azmt,
                            double Dlnst,
                            double Time,
                            bool Iskl,
                            long IstNomer,
                            long IstVysota
                            )
{

  int i,Naid=-1;
  if(N_of_OO==MaxN_of_OO)
  {
     MaxN_of_OO+=1000;
     if(OO)
     {
        OO=(ODIN_OTSCHET631*)realloc(OO,MaxN_of_OO*sizeof(ODIN_OTSCHET631));
     }else{
        OO=(ODIN_OTSCHET631*)malloc(MaxN_of_OO*sizeof(ODIN_OTSCHET631));
     }
  }

  OO[N_of_OO].Smesh=Smesh;
  OO[N_of_OO].Nomer=Nomer;
  OO[N_of_OO].Vysota=Vysota;
  OO[N_of_OO].EVysota=IstVysota;
  OO[N_of_OO].Azmt=Azmt;
  OO[N_of_OO].Dlnst=Dlnst;
  OO[N_of_OO].Time=Time;
  OO[N_of_OO].IsklOtch=Iskl;
  OO[N_of_OO].IsFalseVysota=false;

  OO[N_of_OO].Zona=0;

  if(GlobalF==-1)
  {
    GlobalF=0;
    GlobalL=0;
    OO[N_of_OO].GlobPrev=-1;
    OO[N_of_OO].GlobNext=-1;
  }else{
    OO[N_of_OO].GlobPrev=N_of_OO-1;
    OO[N_of_OO].GlobNext=-1;
    OO[N_of_OO-1].GlobNext=N_of_OO;

  }

//������ ���� ����� � ������ �������
  for(i=0;i<N_of_Borts;i++)
  {
    if(NomBort[i]==IstNomer)
    {
      Naid=i;
      break;
    }
  }

  if(Naid==-1)
  {

//���� ���� �� ������

    NomBort=(long*)realloc(NomBort,sizeof(long)*(N_of_Borts+1));
    FIBort=(long*)realloc(FIBort,sizeof(long)*(N_of_Borts+1));
    LIBort=(long*)realloc(LIBort,sizeof(long)*(N_of_Borts+1));
    IsklBort=(bool*)realloc(IsklBort,sizeof(bool)*(N_of_Borts+1));
    NomBort[N_of_Borts]=IstNomer;
    FIBort[N_of_Borts]=N_of_OO;
    LIBort[N_of_Borts]=N_of_OO;
    IsklBort[N_of_Borts]=false;
    OO[N_of_OO].Prev=-1;
    OO[N_of_OO].Next=-1;
    OO[N_of_OO].IndexNomer=N_of_Borts;
    N_of_Borts++;
  }else{
  //��� ����
    if(LIBort[Naid]==-1)
    {
      LIBort[Naid]=N_of_OO;
      FIBort[Naid]=N_of_OO;
      OO[N_of_OO].Next=-1;
      OO[N_of_OO].Prev=-1;
      OO[N_of_OO].IndexNomer=Naid;
    }else{
      OO[N_of_OO].Prev=LIBort[Naid];
      OO[LIBort[Naid]].Next=N_of_OO;
      OO[N_of_OO].Next=-1;
      OO[N_of_OO].IndexNomer=Naid;
      LIBort[Naid]=N_of_OO;
    }
  }
//������ ���� ��������� � ���������� ��� ����� �����
  if(OO[N_of_OO].Vysota>0&&OO[N_of_OO].Vysota!=(int)OO[N_of_OO].EVysota)
  {
    OO[N_of_OO].IsFalseVysota=true;
  }else{
    OO[N_of_OO].IsFalseVysota=false;
  }
  N_of_OO++;
  N_of_ExistingOO++;




}


int OTSCHETS631::ChisloOtschetov(
                      int IndexNomer,  //����� �����
                      bool VklIskl,     //�������� �� ����������� �����
                      bool VklOptOtsch  //��������� ������������ ������
                      )
{
  int N=0;
  int Tek;
  bool RetB;
  if(IndexNomer<0||IndexNomer>=N_of_Borts)return (-1);
  if(VklIskl)//������� �����������
  {
    Tek=FIBort[IndexNomer];
    while(Tek>=0)
    {
      if(VklOptOtsch)
      {
         RetB=IsSootvetOption(Tek,false);
         if(RetB==0)
         {
           Tek=OO[Tek].Next;
           continue;
         }
      }
      N++;
      Tek=OO[Tek].Next;
    };
  }else{
    Tek=FIBort[IndexNomer];
    while(Tek>=0)
    {
      if(VklOptOtsch)
      {
         RetB=IsSootvetOption(Tek,false);
         if(RetB==0)
         {
           Tek=OO[Tek].Next;
           continue;
         }
      }
      if(!OO[Tek].IsklOtch)N++;
      Tek=OO[Tek].Next;
    };
  }
  return N;
}

//������� ��� ����� ������������ ����� �������
  void OTSCHETS631::SdelatVseBortaIsklKromeMasiva(long *NBs,  //������ ������
                                     int N_of_NBs)
  {
     int i,j;
        for(j=0;j<N_of_Borts;j++)
        {
          IsklBort[j]=true;
        }


     for(i=0;i<N_of_NBs;i++)
     {
        if(NBs[i]==0||NBs[i]==1||NBs[i]==10001)continue;
        for(j=0;j<N_of_Borts;j++)
        {
           if(NomBort[j]==NBs[i])
           {
              IsklBort[j]=false;
           }
        }
     }


  }                                   











//��������� ������ � ���������� �� ����� �������
//�����.0, ���� �� �������, ����� 1
int OTSCHETS631::FindBlijOtschetAndRastoyanie(
                       int Indx,
                       int *BlijIndex,
                       double *BlijRast)
{
   double MinDR=405.0,DR;
   double DTO=TempObzora/10.0;   //������� ����� �=����� ������ �� 5
   int TekI1,MinI=-1;
   double A1,A2;
   if(Indx<0||Indx>=N_of_OO)
   {
     return (-1);
   }

//������������ ��������
   if(OO[Indx].IsklOtch)return 0;
   if(IsklBort[OO[Indx].IndexNomer])return 0;
   if(OO[Indx].Zona<=1)return 0;

   TekI1=OO[Indx].GlobPrev;
   while(TekI1>=0)
   {
       if(OO[Indx].Time-OO[TekI1].Time>DTO)
       {
         break;
       }


//       if(OO[TekI1].IsklOtch||IsklBort[OO[TekI1].IndexNomer])
       if(OO[TekI1].IsklOtch)
       {
         TekI1=OO[TekI1].GlobPrev;
         continue;
       }
       A1=OO[TekI1].Azmt;
       A2=OO[Indx].Azmt;
       if(A2<A1)
       {
         if(A1>350&&A2<10)
         {
           A2+=360;
         }
       }
       if(ABS(A1-A2)>Strob)
       {
         TekI1=OO[TekI1].GlobPrev;
         continue;
       }
       DR=ABS(OO[TekI1].Dlnst-OO[Indx].Dlnst);
       if(DR<MinDR)
       {
          MinI=TekI1;
          MinDR=DR;
       }

       TekI1=OO[TekI1].GlobPrev;
   };

//���� ��������� �������
  TekI1=OO[Indx].GlobNext;
  while(TekI1>=0)
  {
       if(OO[TekI1].Time-OO[Indx].Time>DTO)
       {
         break;
       }

//       if(OO[TekI1].IsklOtch||IsklBort[OO[TekI1].IndexNomer])
       if(OO[TekI1].IsklOtch)
       {
         TekI1=OO[TekI1].GlobNext;
         continue;
       }
       A1=OO[Indx].Azmt;
       A2=OO[TekI1].Azmt;
       if(A2<A1)
       {
         if(A1>350&&A2<10)
         {
           A2+=360;
         }
       }
       if(ABS(A1-A2)>=Strob)
       {
         TekI1=OO[TekI1].GlobNext;
         continue;
       }
       DR=ABS(OO[Indx].Dlnst-OO[TekI1].Dlnst);
       if(DR<MinDR)
       {
          MinI=TekI1;
          MinDR=DR;
       }
       TekI1=OO[TekI1].GlobNext;
  };

  if(*BlijIndex==MinI)
  {
     return 0;
  }
  *BlijIndex=MinI;
//  *BlijRast=sqrt(MinDR_2);
  *BlijRast=MinDR;
  return 1;
}





//��������� �������� ��������
double OTSCHETS631::GetRaznizaAzimutov(int Indx1,
                                       int Indx2)
{

  return 1;
}



//����������� ��� ������
void OTSCHETS631::FreeAll(void)
{
    if(OO)free(OO);
    if(OS)free(OS);

    if(NomBort)free(NomBort);
    if(FIBort)free(FIBort);
    if(LIBort)free(LIBort);
    if(IsklBort)free(IsklBort);
    if(IsklBortPercV1)free(IsklBortPercV1);
    if(IsklBortPercV2)free(IsklBortPercV2);
    if(IsklBortPercV3)free(IsklBortPercV3);
    if(IsklBortPercV4)free(IsklBortPercV4);
    if(IsklBortPercV1_2)free(IsklBortPercV1_2);
    if(IsklBortPercN1)free(IsklBortPercN1);
    if(IsklBortPercN2)free(IsklBortPercN2);
    if(IsklBortPercN3)free(IsklBortPercN3);
    if(IsklBortPercN4)free(IsklBortPercN4);
    if(IsklBortPercN1_2)free(IsklBortPercN1_2);


    if(IndexVys)free(IndexVys);
    if(IskForNotFilter)free(IskForNotFilter);
    if(IskForWithFilter)free(IskForWithFilter);
    if(OSB)free(OSB);
    if(FileName)free(FileName);


    MaxN_of_OO=N_of_OO=N_of_ExistingOO=N_of_Borts=0;
    GlobalF=GlobalL=-1;
    NomBort=FIBort=LIBort=NULL;
    OO=NULL;
    IsklBort=IsklBortPercV1=IsklBortPercV2=IsklBortPercV3=
       IsklBortPercV4=IsklBortPercV1_2=
       IsklBortPercN1=IsklBortPercN2=IsklBortPercN3=
       IsklBortPercN4=IsklBortPercN1_2=NULL;
    IskForNotFilter=IskForWithFilter=NULL;
    IndexVys=NULL;
    N_of_IndexVys=0;
    OSB=NULL;
    FileName=NULL;
    MIV.FreeAll();

//������ �������� ��� ������ �����
 //�������� ����� ����� ��� �������� ��������
    N_of_Borts=1;
    NomBort=(long*)realloc(NomBort,sizeof(long)*(N_of_Borts));
    FIBort=(long*)realloc(FIBort,sizeof(long)*(N_of_Borts));
    LIBort=(long*)realloc(LIBort,sizeof(long)*(N_of_Borts));
    IsklBort=(bool*)realloc(IsklBort,sizeof(bool)*(N_of_Borts));
    NomBort[0]=0;
    FIBort[0]=-1;
    LIBort[0]=-1;
    IsklBort[0]=true;

}

//����� ������� ������������ ������
int OTSCHETS631::GetNomerNachalo(
                       int Indx,         //������ �������
                       bool VklIsklBort, //�������� ����������� ������ ������
                       bool VklIsklOtsch, //������� ����������� �� ������������ �������
                       bool IsOptionOnly  //������ ��������������� ���������
                                          //������ �� ��������, ��������� � ������
                      )
{
  int N=0;
  int I;
  if(Indx<0||Indx>=N_of_OO)return (-1);
  if(OO[Indx].IsklOtch&&!VklIsklOtsch)
  {
    return (-2);
  }
  if(IsklBort[OO[Indx].IndexNomer]&&!VklIsklBort)
  {
    return (-2);
  }
  I=Indx;
  while(I>=0)
  {
    I=GlobPrevOtschet(I,!VklIsklBort,!VklIsklOtsch,false,false,IsOptionOnly);
    N++;
  };

  return N;
}

//����� �������� ����� ������� �������
int OTSCHETS631::GetChisloOtschetovPosle(
                       int Indx,         //������ �������
                       bool VklIsklBort, //�������� ����������� ������ ������
                       bool VklIsklOtsch, //������� ����������� �� ������������ �������
                       bool IsOptionOnly  //������ ��������������� ���������
                                          //������ �� ��������, ��������� � ������
                      )
{
  int N=0;
  int I;
  if(Indx<0||Indx>=N_of_OO)return (-1);
  if(OO[Indx].IsklOtch&&!VklIsklOtsch)
  {
    return (-2);
  }
  if(IsklBort[OO[Indx].IndexNomer]&&!VklIsklBort)
  {
    return (-2);
  }
  I=GlobNextOtschet(Indx,!VklIsklBort,!VklIsklOtsch,false,false,IsOptionOnly);
  while(I>=0)
  {
    I=GlobNextOtschet(I,!VklIsklBort,!VklIsklOtsch,false,false,IsOptionOnly);
    N++;
  };
  return N;
}







//����������, ���� �� ������ ������� � ������
void OTSCHETS631::OtnosKZone(void)
{
  int i,j;
  int TekI,TekI1;
  double DTO=TempObzora/5.0;   //������� ����� �=����� ������ �� 5
  TekI=GlobalF;

  while(TekI!=-1)
  {

  /*
    if(IsklBort[OO[TekI].IndexNomer])
    {
     // if(NomBort[OO[TekI].IndexNomer]!=0)
      {
        OO[TekI].Zona=0;
        TekI=OO[TekI].GlobNext;
        continue;
      }
    }
 */
/*
    if(OO[TekI].IndexNomer<1)
    {
      OO[TekI].Zona=0;
      TekI=OO[TekI].GlobNext;
      continue;

    }
*/

    if(OO[TekI].IsklOtch)
    {
      OO[TekI].Zona=0;
      TekI=OO[TekI].GlobNext;
      continue;
    }

    if(Strob<0.01)
    {
       OO[TekI].Zona=1;
       TekI=OO[TekI].GlobNext;
       continue;
    }

 //� ������ ���� ��������� ��� �������� �������, � ���������� ���� �� ������� ������
 //������
    OO[TekI].Zona=1;

 //�������� ��� �������, ������� ������
    TekI1=OO[TekI].GlobPrev;
    while(TekI1>=0)
    {
       if(OO[TekI].Time-OO[TekI1].Time>DTO)
       {
         break;
       }

       if(IsklBort[OO[TekI1].IndexNomer])
       {
//         TekI1=OO[TekI1].GlobPrev;
//         continue;
       }

/*
       if(OO[TekI].IndexNomer<1)
      {
         TekI1=OO[TekI1].GlobPrev;
         continue;
      }

*/
       if(OO[TekI1].IsklOtch)
       {
         TekI1=OO[TekI1].GlobPrev;
         continue;
       }
          OO[TekI].Zona=MAX(OO[TekI].Zona,
                        GetTypeVStrobe631(
                               OO[TekI1].Azmt,OO[TekI1].Dlnst, OO[TekI1].Time,
                               OO[TekI].Azmt,OO[TekI].Dlnst, OO[TekI].Time,
                               TempObzora,
                               Strob, MinDD, MaxDD)
                        );

         TekI1=OO[TekI1].GlobPrev;
    };

//���� ��������� �������
    TekI1=OO[TekI].GlobNext;
    while(TekI1>=0)
    {
       if(OO[TekI1].Time-OO[TekI].Time>DTO)
       {
         break;
       }

       if(IsklBort[OO[TekI1].IndexNomer])
       {
//         TekI1=OO[TekI1].GlobNext;
  //       continue;
       }

       if(OO[TekI1].IndexNomer<1)
       {
         TekI1=OO[TekI1].GlobNext;
         continue;
       }


       if(OO[TekI1].IsklOtch)
       {
         TekI1=OO[TekI1].GlobNext;
         continue;
       }
          OO[TekI].Zona=MAX(OO[TekI].Zona,
                        GetTypeVStrobe631(
                               OO[TekI].Azmt,OO[TekI].Dlnst, OO[TekI].Time,
                               OO[TekI1].Azmt,OO[TekI1].Dlnst, OO[TekI1].Time,
                               TempObzora,
                               Strob, MinDD, MaxDD)
                        );
         TekI1=OO[TekI1].GlobNext;
    };
    TekI=OO[TekI].GlobNext;
  };
}





//������ ������
int OTSCHETS631::FirstOtschet(
                   int IndexNomerB,        //������ �����
                   bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                   bool IsNotNullVysOnly,  //������ � ��������� �������
                   bool IsExistingOnly,
                   bool IsOptionOnly)   //������ ������������
{
  int TekOtsch, TekOtsch2;
  bool RetB;
  if(IndexNomerB<0||IndexNomerB>=N_of_Borts)return (-2);

 //������ ������
  TekOtsch2=FIBort[IndexNomerB];
  if(TekOtsch2<0)return (-1);
  while(TekOtsch2>=0)
  {
    TekOtsch=TekOtsch2;
    TekOtsch2=OO[TekOtsch2].Prev;
  }

  while(TekOtsch>=0)
  {
//��������� ������ �� �������������

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].Next;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].Next;
       continue;
     }
     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].Next;
         continue;
       }
     }
     break;
  }
  return TekOtsch;
}


//��������� ������
int OTSCHETS631::LastOtschet(
                   int IndexNomerB,        //������ �����
                   bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                   bool IsNotNullVysOnly,  //������ � ��������� �������
                   bool IsExistingOnly,
                   bool IsOptionOnly)   //������ ������������
{
  int TekOtsch, TekOtsch2;
  bool RetB;
  if(IndexNomerB<0||IndexNomerB>=N_of_Borts)return (-2);

 //������ ������
  TekOtsch2=LIBort[IndexNomerB];
  if(TekOtsch2<0)return (-1);
  while(TekOtsch2>=0)
  {
    TekOtsch=TekOtsch2;
    TekOtsch2=OO[TekOtsch2].Next;
  }

  while(TekOtsch>=0)
  {
//��������� ������ �� �������������

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].Prev;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].Prev;
       continue;
     }
     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].Prev;
         continue;
       }
     }
     break;
  }
  return TekOtsch;
}



//NextOtschet - ��������� ������
int OTSCHETS631::NextOtschet(
           int IndexOtsch,
           bool IsNotIsklOnly,    //������ �� ������������ �� �����������
           bool IsNotNullVysOnly, //������ � ��������� �������
           bool IsExistingOnly,
           bool IsOptionOnly)   //������ ������������
{
//���� ������ ������� ������������� ������ ���������
  int TekOtsch;
  bool RetB;
  if(IndexOtsch<0||IndexOtsch>=N_of_OO)return (-1);

  TekOtsch=OO[IndexOtsch].Next;
  while(TekOtsch>=0)
  {

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].Next;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].Next;
       continue;
     }

     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].Next;
         continue;
       }
     }

     break;
  }
  return TekOtsch;
}

//PrevOtschet - ��������� ������
int OTSCHETS631::PrevOtschet(
           int IndexOtsch,
           bool IsNotIsklOnly,    //������ �� ������������ �� �����������
           bool IsNotNullVysOnly, //������ � ��������� �������
           bool IsExistingOnly,
           bool IsOptionOnly)   //������ ������������
{
//���� ������ ������� ������������� ������ ���������
  int TekOtsch;
  bool RetB;
  if(IndexOtsch<0||IndexOtsch>=N_of_OO)return (-1);

  TekOtsch=OO[IndexOtsch].Prev;
  while(TekOtsch>=0)
  {

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].Prev;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].Prev;
       continue;
     }

     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].Prev;
         continue;
       }
     }

     break;
  }
  return TekOtsch;
}



//������ ������
int OTSCHETS631::GlobFirstOtschet(
           bool IsNotIsklBortOnly, //�� �������� ����� �����
           bool IsNotIsklOnly,    //������ �� ������������ �� �����������
           bool IsNotNullVysOnly, //������ � ��������� �������
           bool IsExistingOnly,
           bool IsOptionOnly)   //������ ������������
{
    int TekOtsch;

   bool RetB;
 //������ ������
  TekOtsch=GlobalF;
  if(TekOtsch<0)return (-1);

  while(TekOtsch>=0)
  {

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].GlobNext;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].GlobNext;
       continue;
     }
     if(IsNotIsklBortOnly&&IsklBort[OO[TekOtsch].IndexNomer])
     {
       TekOtsch=OO[TekOtsch].GlobNext;
       continue;
     }

     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].GlobNext;
         continue;
       }
     }

     break;
  }
  return TekOtsch;
}

//��������� ������
int OTSCHETS631::GlobLastOtschet(
           bool IsNotIsklBortOnly, //�� �������� ����� ����� 
           bool IsNotIsklOnly,    //������ �� ������������ �� �����������
           bool IsNotNullVysOnly, //������ � ��������� �������
           bool IsExistingOnly,
           bool IsOptionOnly)   //������ ������������
{
    int TekOtsch;
    bool RetB;
 //������ ������
  TekOtsch=GlobalL;
  if(TekOtsch<0)return (-1);

  while(TekOtsch>=0)
  {

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].GlobPrev;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].GlobPrev;
       continue;
     }
     if(IsNotIsklBortOnly&&IsklBort[OO[TekOtsch].IndexNomer])
     {
       TekOtsch=OO[TekOtsch].GlobPrev;
       continue;
     }

     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].GlobPrev;
         continue;
       }
     }

     break;
  }
  return TekOtsch;
}


//�������� ��������� ������
int OTSCHETS631::GlobNextOtschet(
           int  IndexOtsch,
           bool IsNotIsklBortOnly, //�� �������� ����� ����� 
           bool IsNotIsklOnly,    //������ �� ������������ �� �����������
           bool IsNotNullVysOnly, //������ � ��������� �������
           bool IsExistingOnly,
           bool IsOptionOnly)   //������ ������������
{
//���� ������ ������� ������������� ������ ���������
  int TekOtsch;
  bool RetB;
  if(IndexOtsch<0||IndexOtsch>=N_of_OO)return (-1);

  TekOtsch=OO[IndexOtsch].GlobNext;
  while(TekOtsch>=0)
  {

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].GlobNext;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].GlobNext;
       continue;
     }

  //��������� ���������� ������
     if(IsNotIsklBortOnly&&IsklBort[OO[TekOtsch].IndexNomer])
     {
       TekOtsch=OO[TekOtsch].GlobNext;
       continue;
     }

//������������ ������
     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].GlobNext;
         continue;
       }
     }
     break;
  }
  return TekOtsch;
}


//PrevOtschet - ��������� ������
int OTSCHETS631::GlobPrevOtschet(
           int IndexOtsch,
           bool IsNotIsklBortOnly, //�� �������� ����� �����
           bool IsNotIsklOnly,    //������ �� ������������ �� �����������
           bool IsNotNullVysOnly, //������ � ��������� �������
           bool IsExistingOnly,
           bool IsOptionOnly)   //������ ������������
{
//���� ������ ������� ������������� ������ ���������
  int TekOtsch;
  bool RetB;
  if(IndexOtsch<0||IndexOtsch>=N_of_OO)return (-1);

  TekOtsch=OO[IndexOtsch].GlobPrev;
  while(TekOtsch>=0)
  {

   //��������� ������ �� ��������� ������
     if(IsNotNullVysOnly&&OO[TekOtsch].Vysota==0)
     {
       TekOtsch=OO[TekOtsch].GlobPrev;
       continue;
     }

  //��������� ������ �� �� �������� �� ��� ���
     if(IsNotIsklOnly&&OO[TekOtsch].IsklOtch)
     {
       TekOtsch=OO[TekOtsch].GlobPrev;
       continue;
     }
     if(IsNotIsklBortOnly&&IsklBort[OO[TekOtsch].IndexNomer])
     {
       TekOtsch=OO[TekOtsch].GlobPrev;
       continue;
     }


   //������������ ������
     if(IsOptionOnly)
     {
   //�������� �� ������������ �����
       RetB=IsSootvetOption(TekOtsch,false);
       if(!RetB)
       {
         TekOtsch=OO[TekOtsch].GlobPrev;
         continue;
       }
     }

     break;
  }
  return TekOtsch;
}



//����������:
// 0 - ������ ������, �
// 1 - ������ ���������
// 2 - ��� ���������, �� ������ �� ������, ��� � ������ ��� ���� ������ ����
int OTSCHETS631::SmenaNomera(int KodOO,           //���
                            int NewKodBort        //����� ��� (������) �����
                           )      //������
{

  if(KodOO<0||KodOO>=N_of_OO)return 0;
  if(NewKodBort<0||NewKodBort>=N_of_Borts)return 0;


//������ ��� �������, ������� ������
  UdalitElementIzSpiska(KodOO);
  DobavitElementVSpisok(KodOO,NewKodBort);


  return 1;
}


//�������������� 2 ������� -- ������������ � SmenaNomera
int OTSCHETS631::UdalitElementIzSpiska(int KodOO)
{
  int Lft;
  int Rght;

  if(KodOO<0||KodOO>=N_of_OO)return 0;
  int IndxN=OO[KodOO].IndexNomer;
//����� ��� ������ �������
  if(OO[KodOO].Prev<0)
  {
   //������������ �������
     if(OO[KodOO].Next<0)
     {
      //������� ��� ���� � �������� ������
         FIBort[IndxN]=-1;
         LIBort[IndxN]=-1;

     }else{

      //��� ������ �������, �� �� ���������
         Rght=OO[KodOO].Next;
         OO[Rght].Prev=-1;
         FIBort[IndxN]=Rght;
     }
     OO[KodOO].Prev=OO[KodOO].Next=-1;
     return 1;
  }

 //��������� �������
  if(OO[KodOO].Next<0)
  {
     Lft=OO[KodOO].Prev;
     OO[Lft].Next=-1;
     LIBort[IndxN]=Lft;
     OO[KodOO].Prev=OO[KodOO].Next=-1;
     return 1;
  }

//������� �� ��������
  Lft=OO[KodOO].Prev;
  Rght=OO[KodOO].Next;
  OO[Lft].Next=Rght;
  OO[Rght].Prev=Lft;
  OO[KodOO].Prev=OO[KodOO].Next=-1;
  return 1;

}



int OTSCHETS631::DobavitElementVSpisok(int KodOO,
                                       int NewKodBort)
{
  int i,i1;
  double Tm;
  if(KodOO<0||KodOO>=N_of_OO)return 0;
  if(NewKodBort<0||NewKodBort>=N_of_Borts)return 0;
  Tm=OO[KodOO].Time;

  i=FIBort[NewKodBort];
  if(i<0)
  {
    OO[KodOO].Prev=-1;
    OO[KodOO].Next=-1;
    FIBort[NewKodBort]=KodOO;
    LIBort[NewKodBort]=KodOO;
    OO[KodOO].IndexNomer=NewKodBort;
    return 1;
  }

  if(Tm<OO[i].Time)//���� Tm<OO[i].Time
  {
    OO[KodOO].Prev=-1;
    OO[KodOO].Next=FIBort[NewKodBort];
    OO[FIBort[NewKodBort]].Prev=KodOO;
    FIBort[NewKodBort]=KodOO;
    OO[KodOO].IndexNomer=NewKodBort;
    return 1;
  }

  if(Tm>OO[LIBort[NewKodBort]].Time)
  {
    OO[KodOO].Next=-1;
    OO[KodOO].Prev=LIBort[NewKodBort];
    OO[LIBort[NewKodBort]].Next=KodOO;
    LIBort[NewKodBort]=KodOO;
    OO[KodOO].IndexNomer=NewKodBort;
    return 1;
  }


  i1=OO[i].Next;
  while(i1>=0)
  {
     if(Tm>=OO[i].Time&&Tm<=OO[i1].Time)
     {
        OO[KodOO].Next=i1;
        OO[KodOO].Prev=i;
        OO[i].Next=KodOO;
        OO[i1].Prev=KodOO;
        OO[KodOO].IndexNomer=NewKodBort;
        break;
     }
     i=i1;
     i1=OO[i1].Next;
  };
  return 1;
}


int OTSCHETS631::ZapolnitStringGrid(void *SG,
                        int IndexNomer,
                        bool VklIskl,
                        bool IsOptionOnly)
{
  int N_of_OO1,i;
  int Tek;
  int ProshloObzorov;
  double Xprv,Yprv,Xtek,Ytek;
  double V_Xprv,V_Yprv, Vprv, V_Xtek,V_Ytek, Vtek;
  char Strka[50];
  double DTime,PrevTime;

  if(IndexNomer<0||IndexNomer>=N_of_Borts)return 0;
  TStringGrid *MySG;
  MySG=(TStringGrid*)SG;
  MySG->ColCount=19;
  MySG->RowCount=2;
  MySG->FixedCols=2;
  MySG->FixedRows=1;
  MySG->DefaultRowHeight=20;
  MySG->ColWidths[0]=1;
  MySG->ColWidths[1]=35;
  MySG->ColWidths[2]=85;
  MySG->ColWidths[3]=85;
  MySG->ColWidths[4]=45;
  MySG->ColWidths[5]=45;
  MySG->ColWidths[6]=45;
  MySG->ColWidths[7]=70;
  MySG->ColWidths[8]=45;
  MySG->ColWidths[9]=45;
  MySG->ColWidths[10]=45;
  MySG->ColWidths[11]=45;
  MySG->ColWidths[12]=85;
  MySG->ColWidths[13]=65;
  MySG->ColWidths[14]=1;
  MySG->ColWidths[15]=1;
  MySG->ColWidths[16]=1;
  MySG->ColWidths[17]=1;
  MySG->ColWidths[18]=1;

  MySG->Options=MySG->Options>>goRowSelect;

//�������� �����
  MySG->Cells[1][0]="�";
  MySG->Cells[2][0]="True aircraft";
  MySG->Cells[3][0]="Detected aircraft";
  MySG->Cells[4][0]="Altitude";
  MySG->Cells[5][0]="Azimuth";
  MySG->Cells[6][0]="Range";
  MySG->Cells[7][0]="Time";
  MySG->Cells[8][0]="+Scans";
  MySG->Cells[9][0]="Status";
  MySG->Cells[10][0]="Altitude";
  MySG->Cells[11][0]="Number";
  MySG->Cells[12][0]="Another aircraft";
  MySG->Cells[13][0]="Altitude A";

  MySG->Cells[14][0]="Vx (km/h)";
  MySG->Cells[15][0]="Vy (km/h)";
  MySG->Cells[16][0]="V  (km/h)";
  MySG->Cells[17][0]="Delta time (s)";
  MySG->Cells[18][0]="Ay (km/h/s)";
  MySG->Cells[19][0]="A  (km/h/s)";

  N_of_OO1=ChisloOtschetov(IndexNomer,VklIskl,IsOptionOnly);
  DTime=0.0;
  if(N_of_OO1==0)
  {
     for(i=0;i<11;i++)MySG->Cells[i][1]="";
  }else{
     MySG->RowCount=N_of_OO1+1;
     Tek=FirstOtschet(IndexNomer,!VklIskl,false,false,IsOptionOnly);


     for(i=1;i<=N_of_OO1;i++)
     {

        if(Tek<0)break;

     //�������� ��� ������
        MySG->Cells[0][i]=IntToStr(Tek);

     //������ ����� �������
        MySG->Cells[1][i]=IntToStr(i);

     //�������� ����� �����
        MySG->Cells[2][i]=IntToStr((int)NomBort[IndexNomer]);

     //�������� ����� ����� ������������
        MySG->Cells[3][i]=IntToStr((int)OO[Tek].Nomer);

     //������� ������
        MySG->Cells[4][i]=IntToStr((int)OO[Tek].Vysota);

     //������� ������
        sprintf(Strka,"%.2lf",OO[Tek].Azmt);
        MySG->Cells[5][i]=Strka;

     //������� ���������
        sprintf(Strka,"%.2lf",OO[Tek].Dlnst);
        MySG->Cells[6][i]=Strka;

     //������� �����
        MySG->Cells[7][i]=GetTimeFromDoubleToString(OO[Tek].Time);
     //������� ������� ������� ������ � ����������
        if(Tek==FIBort[IndexNomer])
        {
           MySG->Cells[8][i]="";
           PrevTime=OO[Tek].Time;
        }else{
           DTime=OO[Tek].Time-PrevTime;
           ProshloObzorov=Okrugl(DTime/TempObzora);
           MySG->Cells[8][i]=IntToStr(ProshloObzorov);
           PrevTime=OO[Tek].Time;
        }

        if(OO[Tek].IsklOtch)
        {
          MySG->Cells[9][i]="Excluded";
        }else{
          MySG->Cells[9][i]="Included";
        }

        if(OO[Tek].IsFalseVysota)
        {
          MySG->Cells[10][i]="False";
        }else{
          if(OO[Tek].Vysota==0)
          {
            MySG->Cells[10][i]="Nil";
          }else{
            MySG->Cells[10][i]="True";
          }
        }


        if(OO[Tek].Nomer!=NomBort[OO[Tek].IndexNomer])
        {
          if(OO[Tek].Nomer)
          {
            MySG->Cells[11][i]="False";
          }else{
            MySG->Cells[11][i]="Nil";
          }
        }else{
          MySG->Cells[11][i]="True";
        }


        MySG->Cells[12][i]=IntToStr(OO[Tek].Zona);

        MySG->Cells[13][i]=IntToStr((int)OO[Tek].EVysota);


        Tek=NextOtschet(Tek,!VklIskl,false,false,IsOptionOnly);
     }
  }
  return 1;
}





//����� ����������
int OTSCHETS631::ZapolnitStringGridIskNom(void *SG,        //������ ���� TStringGrid
                        bool VklIsklBort,     //�������� �� ����������� �����
                        bool VklIskl,     //�������� �� ����������� �������
                        bool IsOptionOnly //������ ��������������� ���������
                        )
{
  int N_of_OO1,i;
  int Tek;
  int ProshloObzorov;
  double Xprv,Yprv,Xtek,Ytek;
  double V_Xprv,V_Yprv, Vprv, V_Xtek,V_Ytek, Vtek;
  char Strka[50];
  double DTime,PrevTime;

  TStringGrid *MySG;
  MySG=(TStringGrid*)SG;
  MySG->ColCount=17;
  MySG->RowCount=2;
  MySG->FixedCols=2;
  MySG->FixedRows=1;
  MySG->DefaultRowHeight=20;
  MySG->ColWidths[0]=1;
  MySG->ColWidths[1]=35;
  MySG->ColWidths[2]=85;
  MySG->ColWidths[3]=85;
  MySG->ColWidths[4]=45;
  MySG->ColWidths[5]=45;
  MySG->ColWidths[6]=45;
  MySG->ColWidths[7]=70;
  MySG->ColWidths[8]=45;
  MySG->ColWidths[9]=45;
  MySG->ColWidths[10]=45;
  MySG->ColWidths[11]=85;
  MySG->ColWidths[12]=1;
  MySG->ColWidths[13]=1;
  MySG->ColWidths[14]=1;
  MySG->ColWidths[15]=1;
  MySG->ColWidths[16]=1;

  MySG->Options=MySG->Options>>goRowSelect;

//�������� �����
  MySG->Cells[1][0]="�";
  MySG->Cells[2][0]="True aircraft";
  MySG->Cells[3][0]="Detected aircraft";
  MySG->Cells[4][0]="Altitude";
  MySG->Cells[5][0]="Azimuth";
  MySG->Cells[6][0]="Range";
  MySG->Cells[7][0]="Time";
  MySG->Cells[8][0]="Status";
  MySG->Cells[9][0]="Altitude";
  MySG->Cells[10][0]="Number";
  MySG->Cells[11][0]="Another aircraft";
  MySG->Cells[12][0]="Vx (km/h)";
  MySG->Cells[13][0]="Vy (km/h)";
  MySG->Cells[14][0]="V  (km/h)";
  MySG->Cells[15][0]="Delta Time (s)";
  MySG->Cells[16][0]="ChVy (km/h/s)";
  MySG->RowCount=2;
  for(i=0;i<11;i++)MySG->Cells[i][1]="";
  Tek=GlobFirstOtschet(!VklIsklBort,!VklIskl,false,false,IsOptionOnly);
  i=0;
  while(Tek>=0)
  {
    if(OO[Tek].Nomer!=NomBort[OO[Tek].IndexNomer])
    {
      if(i>0)
      {
        MySG->RowCount++;
      }
      i++;
//�������� ��� ������
        MySG->Cells[0][i]=IntToStr(Tek);

     //������ ����� �������
        MySG->Cells[1][i]=IntToStr(i);

     //�������� ����� �����
        MySG->Cells[2][i]=IntToStr((int)NomBort[OO[Tek].IndexNomer]);

     //�������� ����� ����� ������������
        MySG->Cells[3][i]=IntToStr((int)OO[Tek].Nomer);

     //������� ������
        MySG->Cells[4][i]=IntToStr((int)OO[Tek].Vysota);

     //������� ������
        sprintf(Strka,"%.2lf",OO[Tek].Azmt);
        MySG->Cells[5][i]=Strka;

     //������� ���������
        sprintf(Strka,"%.2lf",OO[Tek].Dlnst);
        MySG->Cells[6][i]=Strka;

     //������� �����
        MySG->Cells[7][i]=GetTimeFromDoubleToString(OO[Tek].Time);
     //������� ������� ������� ������ � ����������

        if(OO[Tek].IsklOtch)
        {
          MySG->Cells[8][i]="Excluded";
        }else{
          MySG->Cells[8][i]="Included";
        }

        if(OO[Tek].IsFalseVysota)
        {
          MySG->Cells[9][i]="False";
        }else{
          if(OO[Tek].Vysota==0)
          {
            MySG->Cells[9][i]="Nil";
          }else{
            MySG->Cells[9][i]="True";
          }
        }

        if(OO[Tek].Nomer!=NomBort[OO[Tek].IndexNomer])
        {
          if(OO[Tek].Nomer)
          {
           MySG->Cells[10][i]="False";
          }else{
           MySG->Cells[10][i]="Nil";
          }
        }else{
           MySG->Cells[10][i]="True";
        }

        MySG->Cells[11][i]=IntToStr(OO[Tek].Zona);
    }
    Tek=GlobNextOtschet(Tek,!VklIsklBort,!VklIskl,false,false,IsOptionOnly);
  };
  return 1;


}



//����� ������ ������ ����� ������ ������ ������ ������ ������� ��� � RBS (��������)
 int OTSCHETS631::ZapolnitStringGridObzor(void *SG,        //������ ���� TStringGrid
                                         int IndexNomerOtch,  //������ ����������� �� ������ �������...
                                         bool VklIskl     //�������� �� ����������� �������
                                         )
 {

    if(IndexNomerOtch>=N_of_OO||IndexNomerOtch<0)
    {
      return 0;
    }
    TStringGrid *MySG;
    MySG=(TStringGrid*)SG;
    MySG->ColCount=10;
    MySG->RowCount=2;
    MySG->FixedCols=0;
    MySG->FixedRows=1;
    MySG->DefaultRowHeight=20;
    MySG->ColWidths[0]=1;
    MySG->ColWidths[1]=80;
    MySG->ColWidths[2]=80;
    MySG->ColWidths[3]=80;
    MySG->ColWidths[4]=80;
    MySG->ColWidths[5]=80;
    MySG->ColWidths[6]=80;
    MySG->ColWidths[7]=80;
    MySG->ColWidths[8]=80;
    MySG->ColWidths[9]=80;
    MySG->Options=MySG->Options<<goRowSelect;


//�������� �����
  MySG->Cells[1][0]="True �";
  MySG->Cells[2][0]="Det. �";
  MySG->Cells[3][0]="Altitude";
  MySG->Cells[4][0]="Azimuth";
  MySG->Cells[5][0]="Range";
  MySG->Cells[6][0]="Time";
  MySG->Cells[7][0]="Status";
  MySG->Cells[8][0]="Altitude";
  MySG->Cells[9][0]="Number";


  return 1;

 }




//���������� ������ � ��
int  OTSCHETS631::AllDanIskajForNomer(int INomer)
{
  int Indx;
  if(INomer<0||INomer>=N_of_Borts)return 0;
  if(!IskForNotFilter)return 0;

    memset(&IskForNotFilter[INomer],0, sizeof(ISKAJENIA631));
    IskForNotFilter[INomer].Otkuda=this;
    IskForNotFilter[INomer].IndexNomer=INomer;
    Indx=FirstOtschet(INomer, true, false, true,true);

    while(Indx>=0)
    {
       IskForNotFilter[INomer].N_of_O++;
       switch(OO[Indx].Zona)
       {
          case 1:
             IskForNotFilter[INomer].N_of_O1++;
             if(!OO[Indx].IsFalseVysota&&
                OO[Indx].Nomer==NomBort[OO[Indx].IndexNomer]&&
                OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_PO1++;
             }
             if(OO[Indx].IsFalseVysota)
             {
               IskForNotFilter[INomer].N_of_V1++;
             }
             if(OO[Indx].Nomer!=NomBort[OO[Indx].IndexNomer]&&OO[Indx].Nomer!=0)
             {
               IskForNotFilter[INomer].N_of_N1++;
             }
             if(OO[Indx].Vysota==0)
             {
               IskForNotFilter[INomer].N_of_Nul1++;
             }
             if(OO[Indx].Nomer==0)
             {
               IskForNotFilter[INomer].N_of_NulN1++;
             }
             if(OO[Indx].Nomer!=0&&OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_NotNull1++;
             }

             break;

         case 2:
             IskForNotFilter[INomer].N_of_O2++;
             if(!OO[Indx].IsFalseVysota&&
                OO[Indx].Nomer==NomBort[OO[Indx].IndexNomer]&&
                OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_PO2++;
             }


             if(OO[Indx].IsFalseVysota)
             {
               IskForNotFilter[INomer].N_of_V2++;
             }
             if(OO[Indx].Nomer!=NomBort[OO[Indx].IndexNomer]&&OO[Indx].Nomer!=0)
             {
               IskForNotFilter[INomer].N_of_N2++;
             }
             if(OO[Indx].Vysota==0)
             {
               IskForNotFilter[INomer].N_of_Nul2++;
             }
             if(OO[Indx].Nomer==0)
             {
               IskForNotFilter[INomer].N_of_NulN2++;
             }

             if(OO[Indx].Nomer!=0&&OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_NotNull2++;
             }

             break;

         case 3:
             IskForNotFilter[INomer].N_of_O3++;
             if(!OO[Indx].IsFalseVysota&&
                OO[Indx].Nomer==NomBort[OO[Indx].IndexNomer]&&
                OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_PO3++;
             }

             if(OO[Indx].IsFalseVysota)
             {
               IskForNotFilter[INomer].N_of_V3++;
             }
             if(OO[Indx].Nomer!=NomBort[OO[Indx].IndexNomer]&&OO[Indx].Nomer!=0)
             {
               IskForNotFilter[INomer].N_of_N3++;
             }
             if(OO[Indx].Vysota==0)
             {
               IskForNotFilter[INomer].N_of_Nul3++;
             }
             if(OO[Indx].Nomer==0)
             {
               IskForNotFilter[INomer].N_of_NulN3++;
             }

             if(OO[Indx].Nomer!=0&&OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_NotNull3++;
             }

             break;

         case 4:
             IskForNotFilter[INomer].N_of_O4++;
             if(!OO[Indx].IsFalseVysota&&
                OO[Indx].Nomer==NomBort[OO[Indx].IndexNomer]&&
                OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_PO4++;
             }

             if(OO[Indx].IsFalseVysota)
             {
               IskForNotFilter[INomer].N_of_V4++;
             }
             if(OO[Indx].Nomer!=NomBort[OO[Indx].IndexNomer]&&OO[Indx].Nomer!=0)
             {
               IskForNotFilter[INomer].N_of_N4++;
             }
             if(OO[Indx].Vysota==0)
             {
               IskForNotFilter[INomer].N_of_Nul4++;
             }

             if(OO[Indx].Nomer==0)
             {
               IskForNotFilter[INomer].N_of_NulN4++;
             }

             if(OO[Indx].Nomer!=0&&OO[Indx].Vysota!=0)
             {
               IskForNotFilter[INomer].N_of_NotNull4++;
             }

             break;
       };
       Indx=NextOtschet(Indx, true,false,true,true);
    };
//����������� ����� �������� ������� ��������� � � 1 � 2
    IskForNotFilter[INomer].N_of_O1_2=IskForNotFilter[INomer].N_of_O1+
                                      IskForNotFilter[INomer].N_of_O2;

    IskForNotFilter[INomer].N_of_PO1_2=IskForNotFilter[INomer].N_of_PO1+
                                       IskForNotFilter[INomer].N_of_PO2;

    IskForNotFilter[INomer].N_of_V1_2=IskForNotFilter[INomer].N_of_V1+
                                      IskForNotFilter[INomer].N_of_V2;

    IskForNotFilter[INomer].N_of_N1_2=IskForNotFilter[INomer].N_of_N1+
                                      IskForNotFilter[INomer].N_of_N2;

    IskForNotFilter[INomer].N_of_Nul1_2=IskForNotFilter[INomer].N_of_Nul1+
                                        IskForNotFilter[INomer].N_of_Nul2;

    IskForNotFilter[INomer].N_of_NulN1_2=IskForNotFilter[INomer].N_of_NulN1+
                                         IskForNotFilter[INomer].N_of_NulN2;

    IskForNotFilter[INomer].N_of_NotNull1_2=
                      IskForNotFilter[INomer].N_of_NotNull1+
                      IskForNotFilter[INomer].N_of_NotNull2;


  //���������� �����������
    if(IskForNotFilter[INomer].N_of_O1)
    {
      IskForNotFilter[INomer].P1=((double)(IskForNotFilter[INomer].N_of_O1-
                                           IskForNotFilter[INomer].N_of_V1-
                                           IskForNotFilter[INomer].N_of_Nul1))/
                                 ((double)IskForNotFilter[INomer].N_of_O1);

      IskForNotFilter[INomer].PN1=((double)(IskForNotFilter[INomer].N_of_O1-
                                           IskForNotFilter[INomer].N_of_N1-
                                           IskForNotFilter[INomer].N_of_NulN1))/
                                 ((double)IskForNotFilter[INomer].N_of_O1);

    }else{
      IskForNotFilter[INomer].P1=0.0;
      IskForNotFilter[INomer].PN1=0.0;
    }
    if(IskForNotFilter[INomer].N_of_NotNull1)
    {
      IskForNotFilter[INomer].PI1=((double)(IskForNotFilter[INomer].N_of_NotNull1-
                                            IskForNotFilter[INomer].N_of_PO1))/
                                ((double)IskForNotFilter[INomer].N_of_NotNull1);
    }else{
      IskForNotFilter[INomer].PI1=-1.0;
    }

    if(IskForNotFilter[INomer].N_of_O2)
    {
      IskForNotFilter[INomer].P2=((double)(IskForNotFilter[INomer].N_of_O2-
                                           IskForNotFilter[INomer].N_of_V2-
                                           IskForNotFilter[INomer].N_of_Nul2))/
                                 ((double)IskForNotFilter[INomer].N_of_O2);

      IskForNotFilter[INomer].PN2=((double)(IskForNotFilter[INomer].N_of_O2-
                                            IskForNotFilter[INomer].N_of_N2-
                                            IskForNotFilter[INomer].N_of_NulN2))/
                                  ((double)IskForNotFilter[INomer].N_of_O2);
    }else{
      IskForNotFilter[INomer].P2=0.0;
      IskForNotFilter[INomer].PN2=0.0;
    }

    if(IskForNotFilter[INomer].N_of_NotNull2)
    {
      IskForNotFilter[INomer].PI2=((double)(IskForNotFilter[INomer].N_of_NotNull2-
                                            IskForNotFilter[INomer].N_of_PO2))/
                                ((double)IskForNotFilter[INomer].N_of_NotNull2);
    }else{
      IskForNotFilter[INomer].PI2=-1.0;
    }


    if(IskForNotFilter[INomer].N_of_O3)
    {
      IskForNotFilter[INomer].P3=((double)(IskForNotFilter[INomer].N_of_O3-
                                           IskForNotFilter[INomer].N_of_V3-
                                           IskForNotFilter[INomer].N_of_Nul3))/
                                 ((double)IskForNotFilter[INomer].N_of_O3);

      IskForNotFilter[INomer].PN3=((double)(IskForNotFilter[INomer].N_of_O3-
                                            IskForNotFilter[INomer].N_of_N3-
                                            IskForNotFilter[INomer].N_of_NulN3))/
                                  ((double)IskForNotFilter[INomer].N_of_O3);
    }else{
      IskForNotFilter[INomer].P3=0.0;
      IskForNotFilter[INomer].PN3=0.0;
    }

    if(IskForNotFilter[INomer].N_of_NotNull3)
    {
      IskForNotFilter[INomer].PI3=((double)(IskForNotFilter[INomer].N_of_NotNull3-
                                            IskForNotFilter[INomer].N_of_PO3))/
                                ((double)IskForNotFilter[INomer].N_of_NotNull3);
    }else{
      IskForNotFilter[INomer].PI3=-1.0;
    }


    if(IskForNotFilter[INomer].N_of_O4)
    {
      IskForNotFilter[INomer].P4=((double)(IskForNotFilter[INomer].N_of_O4-
                                           IskForNotFilter[INomer].N_of_V4-
                                           IskForNotFilter[INomer].N_of_Nul4))/
                                 ((double)IskForNotFilter[INomer].N_of_O4);

      IskForNotFilter[INomer].PN4=((double)(IskForNotFilter[INomer].N_of_O4-
                                            IskForNotFilter[INomer].N_of_N4-
                                            IskForNotFilter[INomer].N_of_NulN4))/
                                  ((double)IskForNotFilter[INomer].N_of_O4);
    }else{
      IskForNotFilter[INomer].P4=0.0;
      IskForNotFilter[INomer].PN4=0.0;
    }

    if(IskForNotFilter[INomer].N_of_NotNull4)
    {
      IskForNotFilter[INomer].PI4=((double)(IskForNotFilter[INomer].N_of_NotNull4-
                                            IskForNotFilter[INomer].N_of_PO4))/
                                ((double)IskForNotFilter[INomer].N_of_NotNull4);
    }else{
      IskForNotFilter[INomer].PI4=-1.0;
    }


  //���������� �����������
    if(IskForNotFilter[INomer].N_of_O1_2)
    {
      IskForNotFilter[INomer].P1_2=((double)(IskForNotFilter[INomer].N_of_O1_2-
                                           IskForNotFilter[INomer].N_of_V1_2-
                                           IskForNotFilter[INomer].N_of_Nul1_2))/
                                 ((double)IskForNotFilter[INomer].N_of_O1_2);

      IskForNotFilter[INomer].PN1_2=((double)(IskForNotFilter[INomer].N_of_O1_2-
                                           IskForNotFilter[INomer].N_of_N1_2-
                                           IskForNotFilter[INomer].N_of_NulN1_2))/
                                 ((double)IskForNotFilter[INomer].N_of_O1_2);

    }else{
      IskForNotFilter[INomer].P1_2=0.0;
      IskForNotFilter[INomer].PN1_2=0.0;
    }

    if(IskForNotFilter[INomer].N_of_NotNull1_2)
    {
      IskForNotFilter[INomer].PI1_2=
                             ((double)(IskForNotFilter[INomer].N_of_NotNull1_2-
                                            IskForNotFilter[INomer].N_of_PO1_2))/
                               ((double)IskForNotFilter[INomer].N_of_NotNull1_2);
    }else{
      IskForNotFilter[INomer].PI1_2=-1.0;
    }

  return 1;
}

void OTSCHETS631::AllDanIskaj(void)
{
  int i;

//������� ������� ������ ��� ���������
  IskForNotFilter=(ISKAJENIA631*)realloc(IskForNotFilter,N_of_Borts*sizeof(ISKAJENIA631));
  IsklBortPercV1=(bool*)realloc(IsklBortPercV1,N_of_Borts*sizeof(bool));
  IsklBortPercV1_2=(bool*)realloc(IsklBortPercV1_2,N_of_Borts*sizeof(bool));
  IsklBortPercV2=(bool*)realloc(IsklBortPercV2,N_of_Borts*sizeof(bool));
  IsklBortPercV3=(bool*)realloc(IsklBortPercV3,N_of_Borts*sizeof(bool));
  IsklBortPercV4=(bool*)realloc(IsklBortPercV4,N_of_Borts*sizeof(bool));
  IsklBortPercN1=(bool*)realloc(IsklBortPercN1,N_of_Borts*sizeof(bool));
  IsklBortPercN1_2=(bool*)realloc(IsklBortPercN1_2,N_of_Borts*sizeof(bool));
  IsklBortPercN2=(bool*)realloc(IsklBortPercN2,N_of_Borts*sizeof(bool));
  IsklBortPercN3=(bool*)realloc(IsklBortPercN3,N_of_Borts*sizeof(bool));
  IsklBortPercN4=(bool*)realloc(IsklBortPercN4,N_of_Borts*sizeof(bool));


//������ ��������� ��� ������ �����
  for(i=0;i<N_of_Borts;i++)
  {
    AllDanIskajForNomer(i);
  }

//�������� ����� ������ � ���� ������
  ZapolnObshRezIskaj();
}











int OTSCHETS631::SetDataOSB(int Kak,  //��� ���������,
                            //���� 1 - ������������� �� ������� ������
                            //     2 - �� �������
                            //     3 - ���, ��� ����� �������� ����� N_of_O1
                int N_of_O1)  //����� ��������

{
   int i;
   OSB=(ONE_S_BORT_631*)realloc(OSB,sizeof(ONE_S_BORT_631)*N_of_Borts);
   for(i=0;i<N_of_Borts;i++)
   {
     OSB[i].IndexBort=i;
     OSB[i].Otkuda=(void*)this;
   }
   switch(Kak)
   {
     case 1:
          SortO_S_B_631_Nomer(OSB,N_of_Borts);
          break;

     case 2:
          SortO_S_B_631_FTime(OSB,N_of_Borts);
          break;

     case 3:
          SortO_S_B_631_FKolvo(OSB,N_of_Borts,N_of_O1);
          break;

     default:
       break;
   };


   return 1;
}


int OTSCHETS631::SetIskl(int IndxOtch, bool NewIsIskl)
{
  int Prm1,Prm2,Prm1_1,Prm2_1;
  int Time1,Time2;
  double DTime;
  int Obz2,Obz3;
  if(IndxOtch<0||IndxOtch>=N_of_OO)
  {
     return 0;
  }
  if(OO[IndxOtch].IsklOtch==NewIsIskl)return 1;
  OO[IndxOtch].IsklOtch=NewIsIskl;
  OtnosKZone();
  return 1;
}


//���������� ��������� ������� ��� �������������� ��� ��� ��� ���������� ��������
int OTSCHETS631::SetIsklMulti(int *IndxOtch, int N_of_Otch, bool NewIsIskl)
{
  int i;
  for(i=0;i<N_of_Otch;i++)
  {
     if(IndxOtch[i]>=N_of_OO||IndxOtch[i]<0)continue;
     OO[IndxOtch[i]].IsklOtch=NewIsIskl;
  }
  OtnosKZone();
  return 1;
}





int OTSCHETS631::SetIsklBort(int IndxBort, bool NewIsIskl)
{
  if(IndxBort>=N_of_Borts||IndxBort<0)
  {
    return 0;
  }

  if(IndxBort==0||IndxBort==1)
  {
     IsklBort[IndxBort]=true;
  }

  if(IsklBort[IndxBort]==NewIsIskl)
  {
     return 1;
  }



  IsklBort[IndxBort]=NewIsIskl;
  OtnosKZone();
  ZapolnMIV();
  return 1;
}

//������������� ������ � ���������
int OTSCHETS631::ZapolnitCheckListBox(void *CLB)
{
  int i,N,N1;
  AnsiString Strka;
  int NmBort;
  TCheckListBox *CheckListBox=(TCheckListBox*)CLB;
  CheckListBox->Clear();
  if(!OSB)return 0;
  for(i=0;i<N_of_Borts;i++)
  {
    NmBort=NomBort[OSB[i].IndexBort];
    N=ChisloOtschetov(OSB[i].IndexBort,false,false);
    N1=ChisloOtschetov(OSB[i].IndexBort,false,true);

    if(NmBort==100000)
    {
      Strka=String("False.")+String(" Marks:")+IntToStr(N)+
      String(" Reviewed: ")+IntToStr(N1);
    }else if(NmBort==100001)
    {
      Strka=String("Splitting.")+String(" Marks:")+IntToStr(N)+
      String(" Reviewed: ")+IntToStr(N1);

    }else{
       Strka=String("Tail �")+IntToStr(NmBort)+String(" Marks:")+IntToStr(N)+
      String(" Reviewed: ")+IntToStr(N1);
    }
    CheckListBox->Items->Add(Strka);
    CheckListBox->Checked[i]=IsklBort[OSB[i].IndexBort];
  }

  return 1;
}

void OTSCHETS631::ZapolnObshRezIskaj(void)
{
  int i;
  double Prm1_2,Prm1,Prm2,Prm3,Prm4;
  double PrmN1_2,PrmN1,PrmN2,PrmN3,PrmN4;

  memset(&ObshRezultNotFilter,0,sizeof(ISKAJENIA631));
  for(i=0;i<N_of_Borts;i++)
  {
     if(IsklBort[i])continue;
     if(IskForNotFilter[i].N_of_O<MinBN_of_Otsch)continue;
     if(NomBort[i]==0)continue;
    if(IskForNotFilter[i].N_of_O1>=MinBN_of_Otsch)
    {
     ObshRezultNotFilter.N_of_O1+=IskForNotFilter[i].N_of_O1;
     ObshRezultNotFilter.N_of_PO1+=IskForNotFilter[i].N_of_PO1;
     ObshRezultNotFilter.N_of_N1+=IskForNotFilter[i].N_of_N1;
     ObshRezultNotFilter.N_of_V1+=IskForNotFilter[i].N_of_V1;
     ObshRezultNotFilter.N_of_Nul1+=IskForNotFilter[i].N_of_Nul1;
     ObshRezultNotFilter.N_of_NulN1+=IskForNotFilter[i].N_of_NulN1;
     ObshRezultNotFilter.N_of_NotNull1+=IskForNotFilter[i].N_of_NotNull1;
    }

    if(IskForNotFilter[i].N_of_O2>=MinBN_of_Otsch)
    {
     ObshRezultNotFilter.N_of_O2+=IskForNotFilter[i].N_of_O2;
     ObshRezultNotFilter.N_of_PO2+=IskForNotFilter[i].N_of_PO2;
     ObshRezultNotFilter.N_of_N2+=IskForNotFilter[i].N_of_N2;
     ObshRezultNotFilter.N_of_V2+=IskForNotFilter[i].N_of_V2;
     ObshRezultNotFilter.N_of_Nul2+=IskForNotFilter[i].N_of_Nul2;
     ObshRezultNotFilter.N_of_NulN2+=IskForNotFilter[i].N_of_NulN2;
     ObshRezultNotFilter.N_of_NotNull2+=IskForNotFilter[i].N_of_NotNull2;

    }


    if(IskForNotFilter[i].N_of_O3>=MinBN_of_Otsch)
    {
     ObshRezultNotFilter.N_of_O3+=IskForNotFilter[i].N_of_O3;
     ObshRezultNotFilter.N_of_PO3+=IskForNotFilter[i].N_of_PO3;
     ObshRezultNotFilter.N_of_N3+=IskForNotFilter[i].N_of_N3;
     ObshRezultNotFilter.N_of_V3+=IskForNotFilter[i].N_of_V3;
     ObshRezultNotFilter.N_of_Nul3+=IskForNotFilter[i].N_of_Nul3;
     ObshRezultNotFilter.N_of_NulN3+=IskForNotFilter[i].N_of_NulN3;
     ObshRezultNotFilter.N_of_NotNull3+=IskForNotFilter[i].N_of_NotNull3;

    }

    if(IskForNotFilter[i].N_of_O4>=MinBN_of_Otsch)
    {
     ObshRezultNotFilter.N_of_O4+=IskForNotFilter[i].N_of_O4;
     ObshRezultNotFilter.N_of_PO4+=IskForNotFilter[i].N_of_PO4;
     ObshRezultNotFilter.N_of_N4+=IskForNotFilter[i].N_of_N4;
     ObshRezultNotFilter.N_of_V4+=IskForNotFilter[i].N_of_V4;
     ObshRezultNotFilter.N_of_Nul4+=IskForNotFilter[i].N_of_Nul4;
     ObshRezultNotFilter.N_of_NulN4+=IskForNotFilter[i].N_of_NulN4;
     ObshRezultNotFilter.N_of_NotNull4+=IskForNotFilter[i].N_of_NotNull4;

    }

    if(IskForNotFilter[i].N_of_O1_2>=MinBN_of_Otsch)
    {
     ObshRezultNotFilter.N_of_O1_2+=IskForNotFilter[i].N_of_O1_2;
     ObshRezultNotFilter.N_of_PO1_2+=IskForNotFilter[i].N_of_PO1_2;
     ObshRezultNotFilter.N_of_N1_2+=IskForNotFilter[i].N_of_N1_2;
     ObshRezultNotFilter.N_of_V1_2+=IskForNotFilter[i].N_of_V1_2;
     ObshRezultNotFilter.N_of_Nul1_2+=IskForNotFilter[i].N_of_Nul1_2;
     ObshRezultNotFilter.N_of_NulN1_2+=IskForNotFilter[i].N_of_NulN1_2;
     ObshRezultNotFilter.N_of_NotNull1_2+=IskForNotFilter[i].N_of_NotNull1_2;

    }
  }

//
    if(ObshRezultNotFilter.N_of_O1>=MinBN_of_Otsch)
    {
      ObshRezultNotFilter.P1=((double)(ObshRezultNotFilter.N_of_O1-
                                       ObshRezultNotFilter.N_of_V1-
                                       ObshRezultNotFilter.N_of_Nul1))/
                                 ((double)ObshRezultNotFilter.N_of_O1);

      ObshRezultNotFilter.PN1=((double)(ObshRezultNotFilter.N_of_O1-
                                            ObshRezultNotFilter.N_of_N1-
                                            ObshRezultNotFilter.N_of_NulN1))/
                                  ((double)ObshRezultNotFilter.N_of_O1);

      if(ObshRezultNotFilter.N_of_NotNull1>0)
      {
        ObshRezultNotFilter.PI1=((double)(ObshRezultNotFilter.N_of_NotNull1-
                                 ObshRezultNotFilter.N_of_PO1))/
                                 ((double)(ObshRezultNotFilter.N_of_NotNull1));
      }else{
        ObshRezultNotFilter.PI1=-1.0;
      }

    }else{
      ObshRezultNotFilter.P1=0.0;
      ObshRezultNotFilter.PN1=0.0;
      ObshRezultNotFilter.PI1=-1.0;
    }


    if(ObshRezultNotFilter.N_of_O2>=MinBN_of_Otsch)
    {
      ObshRezultNotFilter.P2=((double)(ObshRezultNotFilter.N_of_O2-
                                       ObshRezultNotFilter.N_of_V2-
                                       ObshRezultNotFilter.N_of_Nul2))/
                                 ((double)ObshRezultNotFilter.N_of_O2);

      ObshRezultNotFilter.PN2=((double)(ObshRezultNotFilter.N_of_O2-
                                        ObshRezultNotFilter.N_of_N2-
                                        ObshRezultNotFilter.N_of_NulN2))/
                                  ((double)ObshRezultNotFilter.N_of_O2);

      if(ObshRezultNotFilter.N_of_NotNull2>0)
      {
        ObshRezultNotFilter.PI2=((double)(ObshRezultNotFilter.N_of_NotNull2-
                                 ObshRezultNotFilter.N_of_PO2))/
                                 ((double)(ObshRezultNotFilter.N_of_NotNull2));
      }else{
        ObshRezultNotFilter.PI2=-1.0;
      }

    }else{
      ObshRezultNotFilter.P2=0.0;
      ObshRezultNotFilter.PN2=0.0;
      ObshRezultNotFilter.PI2=-1.0;
    }

    if(ObshRezultNotFilter.N_of_O3>=MinBN_of_Otsch)
    {
      ObshRezultNotFilter.P3=((double)(ObshRezultNotFilter.N_of_O3-
                                       ObshRezultNotFilter.N_of_V3-
                                       ObshRezultNotFilter.N_of_Nul3))/
                                 ((double)ObshRezultNotFilter.N_of_O3);

      ObshRezultNotFilter.PN3=((double)(ObshRezultNotFilter.N_of_O3-
                                        ObshRezultNotFilter.N_of_N3-
                                        ObshRezultNotFilter.N_of_NulN3))/
                                  ((double)ObshRezultNotFilter.N_of_O3);

      if(ObshRezultNotFilter.N_of_NotNull3>0)
      {
        ObshRezultNotFilter.PI3=((double)(ObshRezultNotFilter.N_of_NotNull3-
                                 ObshRezultNotFilter.N_of_PO3))/
                                 ((double)(ObshRezultNotFilter.N_of_NotNull3));
      }else{
        ObshRezultNotFilter.PI3=-1.0;
      }


    }else{
      ObshRezultNotFilter.P3=0.0;
      ObshRezultNotFilter.PN3=0.0;
      ObshRezultNotFilter.PI3=-1.0;
    }

    if(ObshRezultNotFilter.N_of_O4>=MinBN_of_Otsch)
    {
      ObshRezultNotFilter.P4=((double)(ObshRezultNotFilter.N_of_O4-
                                       ObshRezultNotFilter.N_of_V4-
                                       ObshRezultNotFilter.N_of_Nul4))/
                                 ((double)ObshRezultNotFilter.N_of_O4);

      ObshRezultNotFilter.PN4=((double)(ObshRezultNotFilter.N_of_O4-
                                        ObshRezultNotFilter.N_of_N4-
                                        ObshRezultNotFilter.N_of_NulN4))/
                                  ((double)ObshRezultNotFilter.N_of_O4);

      if(ObshRezultNotFilter.N_of_NotNull4>0)
      {
        ObshRezultNotFilter.PI4=((double)(ObshRezultNotFilter.N_of_NotNull4-
                                 ObshRezultNotFilter.N_of_PO4))/
                                 ((double)(ObshRezultNotFilter.N_of_NotNull4));
      }else{
        ObshRezultNotFilter.PI4=-1.0;
      }

    }else{

      ObshRezultNotFilter.P4=0.0;
      ObshRezultNotFilter.PN4=0.0;
      ObshRezultNotFilter.PI4=-1.0;

    }

//
    if(ObshRezultNotFilter.N_of_O1_2>=MinBN_of_Otsch)
    {
      ObshRezultNotFilter.P1_2=((double)(ObshRezultNotFilter.N_of_O1_2-
                                       ObshRezultNotFilter.N_of_V1_2-
                                       ObshRezultNotFilter.N_of_Nul1_2))/
                                 ((double)ObshRezultNotFilter.N_of_O1_2);

      ObshRezultNotFilter.PN1_2=((double)(ObshRezultNotFilter.N_of_O1_2-
                                            ObshRezultNotFilter.N_of_N1_2-
                                            ObshRezultNotFilter.N_of_NulN1_2))/
                                ((double)ObshRezultNotFilter.N_of_O1_2);

      if(ObshRezultNotFilter.N_of_NotNull1_2>0)
      {
        ObshRezultNotFilter.PI1_2=((double)(ObshRezultNotFilter.N_of_NotNull1_2-
                                 ObshRezultNotFilter.N_of_PO1_2))/
                                 ((double)(ObshRezultNotFilter.N_of_NotNull1_2));
      }else{
        ObshRezultNotFilter.PI1_2=-1.0;
      }

    }else{
      ObshRezultNotFilter.P1_2=0.0;
      ObshRezultNotFilter.PN1_2=0.0;
      ObshRezultNotFilter.PI1_2=-1.0;

    }




//� ������ ���� �������������
  Prm1=ObshRezultNotFilter.P1*0.88;
  Prm2=ObshRezultNotFilter.P2*0.88;
  Prm3=ObshRezultNotFilter.P3*0.88;
  Prm4=ObshRezultNotFilter.P4*0.88;
  Prm1_2=ObshRezultNotFilter.P1_2*0.88;
  PrmN1=ObshRezultNotFilter.PN1*0.88;
  PrmN2=ObshRezultNotFilter.PN2*0.88;
  PrmN3=ObshRezultNotFilter.PN3*0.88;
  PrmN4=ObshRezultNotFilter.PN4*0.88;
  PrmN1_2=ObshRezultNotFilter.PN1_2*0.88;



  memset(&ObshRezultWithFilterV,0,sizeof(ISKAJENIA631));
  memset(&ObshRezultWithFilterN,0,sizeof(ISKAJENIA631));

  for(i=0;i<N_of_Borts;i++)
  {
     if(IsklBort[i])continue;
     if(IskForNotFilter[i].N_of_O<MinBN_of_Otsch)continue;
     if(NomBort[i]==0)continue;
     if(IskForNotFilter[i].P1<Prm1)
     {
        IsklBortPercV1[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O1>=MinBN_of_Otsch)
       {
         ObshRezultWithFilterV.N_of_O1+=IskForNotFilter[i].N_of_O1;
         ObshRezultWithFilterV.N_of_V1+=IskForNotFilter[i].N_of_V1;
         ObshRezultWithFilterV.N_of_Nul1+=IskForNotFilter[i].N_of_Nul1;
       }
         IsklBortPercV1[i]=false;

     }

     if(IskForNotFilter[i].P2<Prm2)
     {
        IsklBortPercV2[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O2>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterV.N_of_O2+=IskForNotFilter[i].N_of_O2;
        ObshRezultWithFilterV.N_of_V2+=IskForNotFilter[i].N_of_V2;
        ObshRezultWithFilterV.N_of_Nul2+=IskForNotFilter[i].N_of_Nul2;
       }
        IsklBortPercV2[i]=false;

     }

     if(IskForNotFilter[i].P3<Prm3)
     {
        IsklBortPercV3[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O3>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterV.N_of_O3+=IskForNotFilter[i].N_of_O3;
        ObshRezultWithFilterV.N_of_V3+=IskForNotFilter[i].N_of_V3;
        ObshRezultWithFilterV.N_of_Nul3+=IskForNotFilter[i].N_of_Nul3;
       }
        IsklBortPercV3[i]=false;
     }

     if(IskForNotFilter[i].P4<Prm4)
     {
        IsklBortPercV4[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O4>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterV.N_of_O4+=IskForNotFilter[i].N_of_O4;
        ObshRezultWithFilterV.N_of_V4+=IskForNotFilter[i].N_of_V4;
        ObshRezultWithFilterV.N_of_Nul4+=IskForNotFilter[i].N_of_Nul4;
       }
        IsklBortPercV4[i]=false;
     }

     if(IskForNotFilter[i].P1_2<Prm1_2)
     {
        IsklBortPercV1_2[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O1_2>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterV.N_of_O1_2+=IskForNotFilter[i].N_of_O1_2;
        ObshRezultWithFilterV.N_of_V1_2+=IskForNotFilter[i].N_of_V1_2;
        ObshRezultWithFilterV.N_of_Nul1_2+=IskForNotFilter[i].N_of_Nul1_2;
        IsklBortPercV1_2[i]=false;
       } 
     }

//� ������ ���������� � ��������
     if(IskForNotFilter[i].PN1<PrmN1)
     {
        IsklBortPercN1[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O1>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterN.N_of_O1+=IskForNotFilter[i].N_of_O1;
        ObshRezultWithFilterN.N_of_N1+=IskForNotFilter[i].N_of_N1;
        ObshRezultWithFilterN.N_of_NulN1+=IskForNotFilter[i].N_of_NulN1;
       }
        IsklBortPercN1[i]=false;
     }

     if(IskForNotFilter[i].PN2<PrmN2)
     {
        IsklBortPercN2[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O2>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterN.N_of_O2+=IskForNotFilter[i].N_of_O2;
        ObshRezultWithFilterN.N_of_N2+=IskForNotFilter[i].N_of_N2;
        ObshRezultWithFilterN.N_of_NulN2+=IskForNotFilter[i].N_of_NulN2;
       }
        IsklBortPercN2[i]=false;
     }

     if(IskForNotFilter[i].PN3<PrmN3)
     {
        IsklBortPercN3[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O3>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterN.N_of_O3+=IskForNotFilter[i].N_of_O3;
        ObshRezultWithFilterN.N_of_N3+=IskForNotFilter[i].N_of_N3;
        ObshRezultWithFilterN.N_of_NulN3+=IskForNotFilter[i].N_of_NulN3;
       }
        IsklBortPercN3[i]=false;
     }

     if(IskForNotFilter[i].PN4<PrmN4)
     {
        IsklBortPercN4[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O4>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterN.N_of_O4+=IskForNotFilter[i].N_of_O4;
        ObshRezultWithFilterN.N_of_N4+=IskForNotFilter[i].N_of_N4;
        ObshRezultWithFilterN.N_of_NulN4+=IskForNotFilter[i].N_of_NulN4;
       } 
        IsklBortPercN4[i]=false;
     }

     if(IskForNotFilter[i].PN1_2<PrmN1_2)
     {
        IsklBortPercN1_2[i]=true;
     }else{
       if(IskForNotFilter[i].N_of_O1_2>=MinBN_of_Otsch)
       {
        ObshRezultWithFilterN.N_of_O1_2+=IskForNotFilter[i].N_of_O1_2;
        ObshRezultWithFilterN.N_of_N1_2+=IskForNotFilter[i].N_of_N1_2;
        ObshRezultWithFilterN.N_of_NulN1_2+=IskForNotFilter[i].N_of_NulN1_2;
       } 
        IsklBortPercN1_2[i]=false;
     }
  }


    if(ObshRezultWithFilterV.N_of_O1)
    {
      ObshRezultWithFilterV.P1=((double)(ObshRezultWithFilterV.N_of_O1-
                                       ObshRezultWithFilterV.N_of_V1-
                                       ObshRezultWithFilterV.N_of_Nul1))/
                               ((double)ObshRezultWithFilterV.N_of_O1);
    }else{
      ObshRezultWithFilterV.P1=0.0;
    }

    if(ObshRezultWithFilterV.N_of_O2)
    {
      ObshRezultWithFilterV.P2=((double)(ObshRezultWithFilterV.N_of_O2-
                                       ObshRezultWithFilterV.N_of_V2-
                                       ObshRezultWithFilterV.N_of_Nul2))/
                                 ((double)ObshRezultWithFilterV.N_of_O2);

    }else{
      ObshRezultWithFilterV.P2=0.0;
    }

    if(ObshRezultWithFilterV.N_of_O3)
    {
      ObshRezultWithFilterV.P3=((double)(ObshRezultWithFilterV.N_of_O3-
                                       ObshRezultWithFilterV.N_of_V3-
                                       ObshRezultWithFilterV.N_of_Nul3))/
                                 ((double)ObshRezultWithFilterV.N_of_O3);
    }else{
      ObshRezultWithFilterV.P3=0.0;
    }

    if(ObshRezultWithFilterV.N_of_O4)
    {
      ObshRezultWithFilterV.P4=((double)(ObshRezultWithFilterV.N_of_O4-
                                       ObshRezultWithFilterV.N_of_V4-
                                       ObshRezultWithFilterV.N_of_Nul4))/
                                 ((double)ObshRezultWithFilterV.N_of_O4);
    }else{
      ObshRezultWithFilterV.P4=0.0;
    }

//
    if(ObshRezultWithFilterV.N_of_O1_2)
    {
      ObshRezultWithFilterV.P1_2=((double)(ObshRezultWithFilterV.N_of_O1_2-
                                       ObshRezultWithFilterV.N_of_V1_2-
                                       ObshRezultWithFilterV.N_of_Nul1_2))/
                                 ((double)ObshRezultWithFilterV.N_of_O1_2);
    }else{
      ObshRezultWithFilterV.P1_2=0.0;
    }

//� ������ ���������� � ��������
    if(ObshRezultWithFilterN.N_of_O1)
    {
       ObshRezultWithFilterN.PN1=((double)(ObshRezultWithFilterN.N_of_O1-
                                       ObshRezultWithFilterN.N_of_N1-
                                       ObshRezultWithFilterN.N_of_NulN1))/
                               ((double)ObshRezultWithFilterN.N_of_O1);
    }else{
      ObshRezultWithFilterN.PN1=0.0;
    }

    if(ObshRezultWithFilterN.N_of_O2)
    {
       ObshRezultWithFilterN.PN2=((double)(ObshRezultWithFilterN.N_of_O2-
                                       ObshRezultWithFilterN.N_of_N2-
                                       ObshRezultWithFilterN.N_of_NulN2))/
                               ((double)ObshRezultWithFilterN.N_of_O2);
    }else{
      ObshRezultWithFilterN.PN2=0.0;
    }

    if(ObshRezultWithFilterN.N_of_O3)
    {
       ObshRezultWithFilterN.PN3=((double)(ObshRezultWithFilterN.N_of_O3-
                                       ObshRezultWithFilterN.N_of_N3-
                                       ObshRezultWithFilterN.N_of_NulN3))/
                               ((double)ObshRezultWithFilterN.N_of_O3);
    }else{
       ObshRezultWithFilterN.PN3=0.0;
    }

    if(ObshRezultWithFilterN.N_of_O4)
    {
       ObshRezultWithFilterN.PN4=((double)(ObshRezultWithFilterN.N_of_O4-
                                       ObshRezultWithFilterN.N_of_N4-
                                       ObshRezultWithFilterN.N_of_NulN4))/
                               ((double)ObshRezultWithFilterN.N_of_O4);
    }else{
       ObshRezultWithFilterN.PN4=0.0;
    }

    if(ObshRezultWithFilterN.N_of_O1_2)
    {
       ObshRezultWithFilterN.PN1_2=((double)(ObshRezultWithFilterN.N_of_O1_2-
                                       ObshRezultWithFilterN.N_of_N1_2-
                                       ObshRezultWithFilterN.N_of_NulN1_2))/
                               ((double)ObshRezultWithFilterN.N_of_O1_2);
    }else{
      ObshRezultWithFilterN.PN1_2=0.0;
    }




}


//����� ������ �������-���� ������ ��������, ��
//�����
int OTSCHETS631::InsertGlobOtschet(int Indx,         //������ ����� �������� ��������� ���� ������
                                 long Smesh,
                                 long Nomer,
                                 long Vysota,
                                 double Azmt,
                                 double Dlnst,
                                 double Time,
                                 bool Iskl)
{
  int Indx1=-1, Indx2=-1,i1;
  int i;

//���� �����
  if(Indx>=N_of_OO||Indx<0)return -1;
  i1=Indx;

  if(OO[Indx].Time<=Time)
  {
    i1=Indx;
    while(1)
    {
      if(OO[i1].GlobNext<0)
      {
         Indx1=i1;
         Indx2=-1;
         break;
      }
      if(OO[i1].Time<=Time&&OO[OO[i1].GlobNext].Time>=Time)
      {
        Indx1=i1;
        Indx2=OO[i1].GlobNext;
        break;
      }
      i1=OO[i1].GlobNext;
    };
  }else{
   i1=Indx;
    while(1)
    {
      if(OO[i1].GlobPrev<0)
      {
         Indx1=-1;
         Indx2=i1;
         break;
      }
      if(OO[OO[i1].GlobPrev].Time<=Time&&OO[i1].Time>=Time)
      {
        Indx1=OO[i1].GlobPrev;
        Indx2=i1;
        break;
      }
      i1=OO[i1].GlobPrev;
    };
  }


  if(Indx1==-1&&Indx2==-1)return (-1);

//������� ������
  if(N_of_OO==MaxN_of_OO)
  {
     MaxN_of_OO+=1000;
     OO=(ODIN_OTSCHET631*)realloc(OO,MaxN_of_OO*sizeof(ODIN_OTSCHET631));
  }

  OO[N_of_OO].Smesh=Smesh;
  OO[N_of_OO].Nomer=Nomer;
  OO[N_of_OO].Vysota=Vysota;
  OO[N_of_OO].Azmt=Azmt;
  OO[N_of_OO].Dlnst=Dlnst;
  OO[N_of_OO].Time=Time;
  OO[N_of_OO].IsklOtch=Iskl;
  OO[N_of_OO].IsFalseVysota=false;
  OO[N_of_OO].Zona=0;
  OO[N_of_OO].Next=-1;
  OO[N_of_OO].Prev=-1;


  if(Indx1==-1)
  {
  //������� ��� ������
    OO[N_of_OO].GlobPrev=-1;
    GlobalF=N_of_OO;
    OO[N_of_OO].GlobNext=Indx2;
    OO[Indx2].GlobPrev=N_of_OO;
    N_of_OO++;
    return (N_of_OO-1);
  }

  if(Indx2==-1)
  {
    OO[N_of_OO].GlobPrev=Indx1;
    GlobalL=N_of_OO;
    OO[N_of_OO].GlobNext=-1;
    OO[Indx1].GlobNext=N_of_OO;
    N_of_OO++;
    return (N_of_OO-1);
  }

  OO[N_of_OO].GlobNext=Indx2;
  OO[Indx2].GlobPrev=N_of_OO;
  OO[N_of_OO].GlobPrev=Indx1;
  OO[Indx1].GlobNext=N_of_OO;
  N_of_OO++;
  return (N_of_OO-1);
}

//����� ������ ������� - ��������� ������ ��� ��������� ������ �� ������ �����
int OTSCHETS631::
      InsertOtschet(int Indx,         //������ ����� �������� ���� �������� ������ ������
                   long Smesh,
                   long Nomer,
                   long Vysota,
                   double Azmt,
                   double Dlnst,
                   double Time,
                   bool Iskl,
                   bool IsPropusk
                   )
{
  int NewIndx;
  int IndexBort;

  if(Indx<0||Indx>=N_of_OO)
  {
    return -1;
  }
  IndexBort=OO[Indx].IndexNomer;
  if(IndexBort<0)return (-2);


  NewIndx=InsertGlobOtschet(Indx,Smesh, Nomer, Vysota, Azmt, Dlnst, Time, Iskl);
  if(NewIndx<0)return NewIndx;

//������ ���� ���������� ����� � ������ �� �����
  if(OO[Indx].Time>Time)
  {
    OO[NewIndx].Next=Indx;
    OO[NewIndx].Prev=OO[Indx].Prev;
    OO[Indx].Prev=NewIndx;
    if(OO[NewIndx].Prev>=0)
    {
       OO[OO[NewIndx].Prev].Next=NewIndx;
    }else{
       FIBort[IndexBort]=NewIndx;
    }
  }else{
    OO[NewIndx].Prev=Indx;
    OO[NewIndx].Next=OO[Indx].Next;
    OO[Indx].Next=NewIndx;
    if(OO[NewIndx].Next>=0)
    {
      OO[OO[NewIndx].Next].Prev=NewIndx;
    }else{
      LIBort[IndexBort]=NewIndx;
    }
  }
  OO[NewIndx].IndexNomer=IndexBort;
  return NewIndx;
}




//������������ ��������� ������ �� ������, ���������, ���������� �������
//����������: 1 ���� ������ ������� � ������������� ��������� �������
//0 - �� �������������;
//-1 ��������� ������
int OTSCHETS631::IsSootvetOption(
                   int Indx,           //���������� ������
                   bool NullVysotaVkl)    //�������� ������� ������
{
  bool RetB;
  if(Indx<0||Indx>=N_of_OO)
  {
    return -1;
  }

  if(IsA1)
  {
      if(IsA2)
      {
          RetB=AsimutDiapazon(OO[Indx].Azmt,A11,A12)&&
               AsimutDiapazon(OO[Indx].Azmt,A21,A22);
      }else{
          RetB=AsimutDiapazon(OO[Indx].Azmt,A11,A12);
       }
   }else{
       if(IsA2)
       {
         RetB=AsimutDiapazon(OO[Indx].Azmt,A21,A22);
       }else{
         RetB=true;
       }
  }
  if(!RetB)return 0;
  if(IsDlnst)
  {
     if(OO[Indx].Dlnst<MinDlnst-0.00001||OO[Indx].Dlnst>MaxDlnst+0.00001)
     {
        return 0;
     }
  }

//�������� ������
  if(IsVysota)
  {
     if(OO[Indx].EVysota<Vysota1||OO[Indx].EVysota>Vysota2)
     {
        return 0;
     }
  }
  return 1;

}


/*���������� ������ � ���������� �� ������*/
int OTSCHETS631::ZapolnMIV(void)
{
  int i;
  int j0,j1,j2;
//���� �������, ��� ��� ������� ���� ������
  MIV.FreeAll();
  for(i=0;i<N_of_OO;i++)
  {
  //�������� ���� �� �����������, ���� �� ������� �� ���������������

    if(IsklBort[OO[i].IndexNomer])continue;
    if(OO[i].IsFalseVysota)
    {
//����� ���������� ������ � ��������� �������
       j0=PrevOtschet(i,true,true,false,false);
       j1=NextOtschet(i,true,true,false,false);
       j2=NextOtschet(j1,true,true,false,false);
       MIV.Add(j0,i,j1,j2);
    }
  }
  return 1;
}

int OTSCHETS631::ZapolnitMIV_SG(
                     void *SG,         //������ ���� TStringGrid
                     int Zona1,
                     bool OptionOnly,  //������ ������������� ������
                     bool NotVyvodEsliMalo //�� ��������, ���� ���� �������� ��������������� ������
                    )
{
  int Ind,Ind1;
  int i,j;
  int Zona2;


  #include "StringGridKT.h"


  TStringGrid *MySG;
  MySG=(TStringGrid*)SG;
  MySG->ColCount=12;
  MySG->RowCount=2;
  MySG->FixedCols=0;
  MySG->FixedRows=1;
  MySG->DefaultRowHeight=12;
  MySG->ColWidths[0]=1;
  MySG->ColWidths[1]=85;
  MySG->ColWidths[2]=85;
  MySG->ColWidths[3]=45;
  MySG->ColWidths[4]=45;
  MySG->ColWidths[5]=45;
  MySG->ColWidths[6]=80;
  MySG->ColWidths[7]=45;
  MySG->ColWidths[8]=45;
  MySG->ColWidths[9]=45;
  MySG->ColWidths[10]=85;
  MySG->ColWidths[11]=65;
  MySG->DefaultRowHeight=20;
  MySG->Cells[1][0]="True aircraft";
  MySG->Cells[2][0]="Detected aircraft";
  MySG->Cells[3][0]="Altitude";
  MySG->Cells[4][0]="Azimuth";
  MySG->Cells[5][0]="Range";
  MySG->Cells[6][0]="Time";
  MySG->Cells[7][0]="Status";
  MySG->Cells[8][0]="Altitude";
  MySG->Cells[9][0]="Number";
  MySG->Cells[10][0]="Another aircraft";
  MySG->Cells[11][0]="Altitude A";
  if(MIV.N_of_OIV==0)
  {
    MySG->RowCount=2;
  }else{
    MySG->RowCount=MIV.N_of_OIV*5+1;
  }

  for(i=0;i<MySG->RowCount;i++)
  {
    MySG->Cells[0][i]="-1";
  }

  Ind1=1;
  for(i=0;i<MIV.N_of_OIV;i++)
  {
    Ind=MIV.OIV[i].Ind1;
    Zona2=OO[MIV.OIV[i].Ind2].Zona;

    if(OptionOnly)
    {
       if(!IsSootvetOption(MIV.OIV[i].Ind2,false))
       {
          continue;
       }
    }

    if(NotVyvodEsliMalo)
    {
       if(ChisloOtschetov(OO[MIV.OIV[i].Ind2].IndexNomer,false,true)<MinBN_of_Otsch)
       {
          continue;
       }
    }

    if(Zona1!=0&&Zona2!=Zona1)
    {
      continue;
    }
    if(Ind<0)
    {
      VyvodSG1(MySG,Ind1,-1,0,0,0,0,0,0,0,0,0,0,0);
    }else{

      VyvodSG1(MySG,Ind1,Ind, NomBort[OO[Ind].IndexNomer], OO[Ind].Nomer,
        OO[Ind].Vysota,OO[Ind].Azmt, OO[Ind].Dlnst, OO[Ind].Time, OO[Ind].IsklOtch,
        OO[Ind].IsFalseVysota, OO[Ind].Nomer!=NomBort[OO[Ind].IndexNomer],
        OO[Ind].Zona, OO[Ind].EVysota);

    }
    Ind1++;

    Ind=MIV.OIV[i].Ind2;

    if(Ind<0)
    {
   //   VyvodSG1(MySG,Ind1,-1,0,0,0,0,0,0,0,0,0,0,0);
    }else{

      VyvodSG1(MySG,Ind1,Ind, NomBort[OO[Ind].IndexNomer], OO[Ind].Nomer,
        OO[Ind].Vysota,OO[Ind].Azmt, OO[Ind].Dlnst, OO[Ind].Time, OO[Ind].IsklOtch,
        OO[Ind].IsFalseVysota, OO[Ind].Nomer!=NomBort[OO[Ind].IndexNomer],
        OO[Ind].Zona, OO[Ind].EVysota);

    }
    Ind1++;

    Ind=MIV.OIV[i].Ind3;
    if(Ind<0)
    {
//      VyvodSG1(MySG,Ind1,-1,0,0,0,0,0,0,0,0,0,0,0);
    }else{
      VyvodSG1(MySG,Ind1,Ind, NomBort[OO[Ind].IndexNomer], OO[Ind].Nomer,
        OO[Ind].Vysota,OO[Ind].Azmt, OO[Ind].Dlnst, OO[Ind].Time, OO[Ind].IsklOtch,
        OO[Ind].IsFalseVysota, OO[Ind].Nomer!=NomBort[OO[Ind].IndexNomer],
        OO[Ind].Zona, OO[Ind].EVysota);
    }
    Ind1++;

    Ind=MIV.OIV[i].Ind4;
    if(Ind<0)
    {
      VyvodSG1(MySG,Ind1,-1,0,0,0,0,0,0,0,0,0,0,0);
    }else{
      VyvodSG1(MySG,Ind1,Ind, NomBort[OO[Ind].IndexNomer], OO[Ind].Nomer,
        OO[Ind].Vysota,OO[Ind].Azmt, OO[Ind].Dlnst, OO[Ind].Time, OO[Ind].IsklOtch,
        OO[Ind].IsFalseVysota, OO[Ind].Nomer!=NomBort[OO[Ind].IndexNomer],
        OO[Ind].Zona, OO[Ind].EVysota);
    }
    Ind1++;

    VyvodSG1(MySG,Ind1,-2,0,0,0,0,0,0,0,0,0,0,0);
    Ind1++;

  }

  if(Zona1>=0)
  {

    if(Ind1==1)
    {
      VyvodSG1(MySG,1,-1,0,0,0,0,0,0,0,0,0,0,0);
      MySG->RowCount=2;
    }
    else MySG->RowCount=Ind1;
  }
  
  return 1;
}







//������� ���������� �� ���� ������ ������ �����
//���������� 1, ���� ������� ���������
//
/*
int OTSCHETS631::ZavyazkaBort(int FIndex, //������ ������
                              int *I1,    //������ ������
                              int *I2,    //������ ������
                              int *I3)    //������ ������
{


}
*/

int OTSCHETS631::FindBlizkieKoVremeniBorta(
                                int OtkudaIskatIndx,
                                int IndexBort,
                                bool IsNotIsklBortOnly, //�� ����������� �����
                                bool IsNotIsklOnly,     //������ �� ������������ �� �����������
                                bool IsNotNullVysOnly,  //������ � ��������� �������
                                bool IsExistingOnly,     //��
                                int *Indx1,
                                int *Indx2)
{
  int Tek;
  if(OtkudaIskatIndx<0||OtkudaIskatIndx>=N_of_OO)return 0;
  Tek=OtkudaIskatIndx;
  while(Tek>=0)
  {
    if(OO[Tek].IndexNomer==IndexBort)
    {
      break;
    }
    Tek=GlobPrevOtschet(Tek,IsNotIsklBortOnly,IsNotIsklOnly,IsNotNullVysOnly,IsExistingOnly,false);
  }
  *Indx1=Tek;
  Tek=OtkudaIskatIndx;
  while(Tek>=0)
  {
    if(OO[Tek].IndexNomer==IndexBort)
    {
      break;
    }
    Tek=GlobNextOtschet(Tek,IsNotIsklBortOnly,IsNotIsklOnly,
                        IsNotNullVysOnly,IsExistingOnly,false);
  }
   *Indx2=Tek;
  return 1;

}












//���������� ��������
int LoadUVDFromFile( void *fp1,  //������������� �����
                     long Vysota1,long Vysota2, bool IsVysota,       //��������� ������
                     double MinAzmt1, double MaxAzmt1, bool IsAzmt1, //�������
                     double MinAzmt2, double MaxAzmt2, bool IsAzmt2,
                     double MinDlnst, double MaxDlnst, bool IsDlnst,
                     OTSCHETS631 *OtchUVD,OTSCHETS631 *OtchRBS,
                     bool CS
                    )
{
  FILE *fp=(FILE*)fp1;
  DANN Dan;
  int NomerSutok=0;
  int Ret;
  int LastChas=0,LastMinuts=-1;
  long LastChasV,FirstChasV=-1;
  bool RetB,IsklOtch;
  double Vremya;
  long Nomer;
  extern TCGauge *GlobalCGauge1Isk;
  long LastProgress=GlobalCGauge1Isk->Progress;

  OtchUVD->A11=MinAzmt1;
  OtchUVD->A12=MaxAzmt1;
  OtchUVD->IsA1=IsAzmt1;
  OtchUVD->A21=MinAzmt2;
  OtchUVD->A22=MaxAzmt2;
  OtchUVD->IsA2=IsAzmt2;
  OtchUVD->MinDlnst=MinDlnst;
  OtchUVD->MaxDlnst=MaxDlnst;
  OtchUVD->IsDlnst=IsDlnst;
  OtchUVD->Vysota1=Vysota1;
  OtchUVD->Vysota2=Vysota2;
  OtchUVD->IsVysota=IsVysota;
  OtchRBS->A11=MinAzmt1;
  OtchRBS->A12=MaxAzmt1;
  OtchRBS->IsA1=IsAzmt1;
  OtchRBS->A21=MinAzmt2;
  OtchRBS->A22=MaxAzmt2;
  OtchRBS->IsA2=IsAzmt2;
  OtchRBS->MinDlnst=MinDlnst;
  OtchRBS->MaxDlnst=MaxDlnst;
  OtchRBS->IsDlnst=IsDlnst;
  OtchRBS->Vysota1=Vysota1;
  OtchRBS->Vysota2=Vysota2;
  OtchRBS->IsVysota=IsVysota;



  Ret=fseek(fp,0,SEEK_SET);
  if(Ret)return 0;




  OtchUVD->FreeAll();
  OtchRBS->FreeAll();
  while(!feof(fp))
  {

    Ret=LoadDan(fp,&Dan);
    if(Ret!=1)continue;
    


    if(LastChas-Dan.H>=1)
    {
      NomerSutok++;
    }else if(LastChas==Dan.H)
    {
  //������� ������
      if(LastMinuts-Dan.M>=5)
      {
        NomerSutok++;
      }
    }



 //�������� ��������� �� ��� �����
    if(Dan.T_K[0]!='S'&&Dan.T_K[1]!='S')
    {
 //     continue;
       Dan.NomBort[0]='9';
       Dan.NomBort[1]='9';
       Dan.NomBort[2]='9';
       Dan.NomBort[3]='9';
       Dan.NomBort[4]='9';

    }


    if(Dan.T_K[0]=='P'&&Dan.T_K[1]!='S')
    {
      Ret=0;
    }else{
      Ret=TipBorta(Dan,CS);
    }
    if(!Ret)continue;

  //������ �����������
    IsklOtch=false;
    if(IsAzmt1)
    {
       if(IsAzmt2)
       {
          RetB=AsimutDiapazon(Dan,MinAzmt1,MaxAzmt1)&&
               AsimutDiapazon(Dan,MinAzmt2,MaxAzmt2);
       }else{
          RetB=AsimutDiapazon(Dan,MinAzmt1,MaxAzmt1);
       }
    }else{
       if(IsAzmt2)
       {
         RetB=AsimutDiapazon(Dan,MinAzmt1,MaxAzmt1);
       }else{
         RetB=true;
       }

    }

    if(!RetB)
    {
       IsklOtch=true;
    }

    if(IsDlnst)
    {
      if((Dan.Dalnost<MinDlnst||Dan.Dalnost>MaxDlnst))
      {
       IsklOtch=true;
      }
    }

    if(IsVysota)
    {
       if(Dan.Vysota)
       {
          if(Dan.Vysota<Vysota1||Dan.Vysota>Vysota2)
          {
            IsklOtch=true;
          }
       }
    }

 //��������� �����
    Nomer=atoi(Dan.NomBort);

 //��������� ����� � �����
    Vremya=NomerSutok*24.0+Dan.H+Dan.M/60.0+Dan.S/3600;
    if(FirstChasV<0)
    {
      FirstChasV=(long)Vremya;
      LastChasV=FirstChasV;
    }



 //� ������ ��������� ������ ���� ��� � ������ �����
    IsklOtch=false;
    if(Ret==1||Dan.T_K[0]=='P'&&Dan.T_K[1]!='S') //���
    {

      OtchUVD->AddOtschet(Dan.Smesh,Nomer,
                Dan.Vysota,Dan.Asimut,Dan.Dalnost, Vremya, IsklOtch,
                Dan.ObnarujNomBort,Dan.ObnarujVysota);
    }else{     //���
      OtchRBS->AddOtschet(Dan.Smesh,Nomer,
                Dan.Vysota,Dan.Asimut,Dan.Dalnost, Vremya, IsklOtch,
                Dan.ObnarujNomBort,Dan.ObnarujVysota);
    }
    if(LastChasV!=((long)Vremya))
    {
      LastChasV=(long)Vremya;
      GlobalCGauge1Isk->Progress=LastProgress+LastChasV-FirstChasV;
    }
    LastChas=Dan.H;
    LastMinuts=Dan.M;
  };
//
  for(int i=0;i<OtchUVD->N_of_Borts;i++)
  {
    if(OtchUVD->NomBort[i]==0)
    {
      OtchUVD->IsklBort[i]=true;
    }
  }

  for(int i=0;i<OtchRBS->N_of_Borts;i++)
  {
    if(OtchRBS->NomBort[i]==0)
    {
      OtchRBS->IsklBort[i]=true;
    }
  }



  return 1;
}








