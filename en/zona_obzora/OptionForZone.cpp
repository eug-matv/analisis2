//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>

#include "OptionForZone.h"
#include "work_ini_form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFOption *FOption;
//---------------------------------------------------------------------------
__fastcall TFOption::TFOption(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TFOption::FormCreate(TObject *Sender)
{
  IsWasVvod=false;
  iffWorkWithIniFile(this, String("zona_obz_vis.ini"));
}
//---------------------------------------------------------------------------
void __fastcall TFOption::Button1Click(TObject *Sender)
{
 IsWasVvod=true;
 Close();
}
//---------------------------------------------------------------------------
void __fastcall TFOption::Button2Click(TObject *Sender)
{
 Close();
}
//---------------------------------------------------------------------------

void __fastcall TFOption::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
  if(Key==VK_F1)
  {
        AnsiString HhpStr;
        if(!FileExists(CHMHelpFile))
        {
                AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
                MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
                return;
        }


        HhpStr=CHMHelpFile+String("::/")+CHMHtml;
        HtmlHelp(GetDesktopWindow(),
                HhpStr.c_str(),
                HH_DISPLAY_TOPIC,NULL);
  }
}
//---------------------------------------------------------------------------
