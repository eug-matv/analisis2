//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>


#include <stdlib.h>
#include "Unit5.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFDataSVer *FDataSVer;
//---------------------------------------------------------------------------
__fastcall TFDataSVer::TFDataSVer(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------



void __fastcall TFDataSVer::FormClose(TObject *Sender,
      TCloseAction &Action)
{
  StringGrid1->RowCount=2;
  StringGrid1->Cells[0][1]="";
  StringGrid1->Cells[1][1]="";
  StringGrid1->Cells[2][1]="";
  StringGrid1->Cells[3][1]="";
  StringGrid1->Cells[4][1]="";
  StringGrid1->Cells[5][1]="";
  StringGrid1->Cells[6][1]="";

}
//---------------------------------------------------------------------------


void __fastcall TFDataSVer::Button1Click(TObject *Sender)
{
  double D,D1,D2;
  int i;
  //����
  D=atof(Edit1->Text.c_str());
  for(i=1;i<StringGrid1->RowCount;i++)
  {
    D1=atof(StringGrid1->Cells[1][i].c_str());
    if(D<D1)
    {
       StringGrid1->Row=i;
       return;
    }
  }
  StringGrid1->Row=StringGrid1->RowCount-1;


}
//---------------------------------------------------------------------------

void __fastcall TFDataSVer::FormCreate(TObject *Sender)
{
  StringGrid1->Cells[0][0]="�";
  StringGrid1->Cells[1][0]="Range(km)";
  StringGrid1->Cells[2][0]="Range-halfwindow";
  StringGrid1->Cells[3][0]="Range+halfwindow";
  StringGrid1->Cells[4][0]="Det.marks";
  StringGrid1->Cells[5][0]="Avail.marks";
  StringGrid1->Cells[6][0]="Probability of det.";

  iffWorkWithIniFile(this, String("zona_obz_vis.ini"));        

}
//---------------------------------------------------------------------------

void __fastcall TFDataSVer::FormResize(TObject *Sender)
{
   int Wdth;
   //������� �������
   Wdth=StringGrid1->Width-StringGrid1->ColWidths[0]-4;
   Wdth=Wdth/6-1;
   StringGrid1->ColWidths[1]=Wdth;
   StringGrid1->ColWidths[2]=Wdth;
   StringGrid1->ColWidths[3]=Wdth;
   StringGrid1->ColWidths[4]=Wdth;
   StringGrid1->ColWidths[5]=Wdth;
   StringGrid1->ColWidths[6]=Wdth;

}
//---------------------------------------------------------------------------

void __fastcall TFDataSVer::N1Click(TObject *Sender)
{
 AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);
}
//---------------------------------------------------------------------------


void __fastcall TFDataSVer::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
     if(Key==VK_F1)
     {
         N1Click(NULL);
     }
}
//---------------------------------------------------------------------------

