//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <alloc.h>
#include "ToolsForZonaObz.h"
#include "Unit2.h"
#include "Unit6.h"
#include "work_ini_form.h"

#define ABS(X) (((X) > 0) ? (X) : (-(X)))

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFDataSOtch *FDataSOtch=NULL;
TFDataSOtch *FDataSOtchObsh=NULL;

extern double sizeOfOkno_km;  //���������� � ������ Unit2.cpp
//---------------------------------------------------------------------------
__fastcall TFDataSOtch::TFDataSOtch(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

//��������� ��������� ����� ��������
int SchitatChislaOtschetovUnit6(
                                 struct OTCHET* Otch,//������ ��������
                                 int N_of_Otch, //����� ��������
                                 int I_O,   //������ ��������
                                 double dfMaxDalnostOfExistO,  //����������� ��������� ������������ ������
/*������������ ��������*/
                                 int *N_of_VO,  //����� ���� �������� � ����
                                 int *N_of_OO, //����� ������������ ��������
                                 double *P,  //����������� �����������
                                 bool Prolet //����������� �����

                               )
{
  int i;

  double Left,Right;
  double PervD,P2,P0;
  int N_of_VO2,N_of_OO2;
  int PervI;
  int N_of_VO1=0,N_of_OO1=0;
  double K1;
  if(I_O<0||I_O>=N_of_Otch)
  {
    return 0;
  }


  *N_of_VO=0;
  *N_of_OO=0;
  if((Otch[I_O].Dalnost-Otch[0].Dalnost)<sizeOfOkno_km/2.0)
  {
     Left=Otch[0].Dalnost-0.0001;
     Right=Otch[0].Dalnost+sizeOfOkno_km+0.00001;
  }else
  if((dfMaxDalnostOfExistO-Otch[I_O].Dalnost)<sizeOfOkno_km/2.0)
  {
     Right=dfMaxDalnostOfExistO+0.00001;
     Left=dfMaxDalnostOfExistO-sizeOfOkno_km-0.00001;
  }else{
     Left=Otch[I_O].Dalnost-sizeOfOkno_km/2.0-0.0001;
     Right=Otch[I_O].Dalnost+sizeOfOkno_km/2.0+0.0001;;
  }
  PervD=-1;
  PervI=-1;




  for(i=I_O;i>=0;i--)
  {
     if(Otch[i].Dalnost<Left)break;
     if(Prolet&&!(Otch[i].Exists)&&Otch[i].Bokovoy)  //���� ���� �����������
     {
       continue;
     }
     (*N_of_VO)++;
     if(Otch[i].Exists)
     {
       (*N_of_OO)++;
     }
  }
  for(i=I_O+1;i<N_of_Otch;i++)
  {
     if(Otch[i].Dalnost>Right)break;
     if(Prolet&&!(Otch[i].Exists)&&Otch[i].Bokovoy)  //���� ���� �����������
     {
       continue;
     }
     (*N_of_VO)++;
     if(Otch[i].Exists)
     {
       (*N_of_OO)++;
     }
  }
  if((*N_of_VO))
  {
    *P=(double)(*N_of_OO)/(*N_of_VO);
  }else{
    *P=0;
  }
  return 1;
}





void __fastcall TFDataSOtch::FormCreate(TObject *Sender)
{
   //

  N_of_VnOtch=0;
  VnOtch=NULL;
  iffWorkWithIniFile(this, String("zona_obz_vis.ini"));        


}
//---------------------------------------------------------------------------
int __fastcall TFDataSOtch::LoadOtchet(struct OTCHET* Otch,
                                       int N_of_Otch,
                                       int VS,
                                       double dfMaxDalnostOfExist
                                       )
{
  int i,i1,i2,j,k;
  char Strka[200];
  double D,DT,Skorost;
  double X1,X2,Y1,Y2;

//��� ���������� ������ � �����������
  int N_of_VO,N_of_OO,Ret;
  double PO;

  if(VS)
  {
     StringGrid1->ColCount=9;
     StringGrid1->Cells[0][0]="�";
     StringGrid1->Cells[1][0]="Azimuth(�)";
     StringGrid1->Cells[2][0]="Range(km)";
     StringGrid1->Cells[3][0]="� Air.";
     StringGrid1->Cells[4][0]="Range-halfwindow";
     StringGrid1->Cells[5][0]="Range+halfwindow";
     StringGrid1->Cells[6][0]="Time";
     StringGrid1->Cells[7][0]="Avail.";
     StringGrid1->Cells[8][0]="Delta time(sec)";
  }else{
     StringGrid1->ColCount=11;
     StringGrid1->Cells[0][0]="�";
     StringGrid1->Cells[1][0]="Azimuth(�)";
     StringGrid1->Cells[2][0]="Range(km)";
     StringGrid1->Cells[3][0]="� Air.";
     StringGrid1->Cells[4][0]="Range-halfwindow";
     StringGrid1->Cells[5][0]="Range+halfwindow";
     StringGrid1->Cells[6][0]="Time";
     StringGrid1->Cells[7][0]="Avail.";
     StringGrid1->Cells[8][0]="Det.";
     StringGrid1->Cells[9][0]="Tolal ";
     StringGrid1->Cells[10][0]="Pr.of.Det.";

  }

  VisualBort=VS;

  if(N_of_Otch==0)return 0;




  StringGrid1->RowCount=N_of_Otch+1;

  for(i=0;i<N_of_Otch;i++)
  {
    StringGrid1->Cells[0][i+1]=i+1;

    sprintf(Strka,"%.2lf",Otch[i].Azimut);
    StringGrid1->Cells[1][i+1]=Strka;

    sprintf(Strka,"%.2lf",Otch[i].Dalnost);
    StringGrid1->Cells[2][i+1]=Strka;

    sprintf(Strka,"%d",Otch[i].NomerBorta%100000);
    StringGrid1->Cells[3][i+1]=Strka;

    D=Otch[i].Dalnost-sizeOfOkno_km/2.0;
    if(D<0.0)D=0.0;
    sprintf(Strka,"%.2lf",D);
    StringGrid1->Cells[4][i+1]=Strka;

    D=Otch[i].Dalnost+sizeOfOkno_km/2.0;
    sprintf(Strka,"%.2lf",D);
    StringGrid1->Cells[5][i+1]=Strka;

 //
    StringGrid1->Cells[6][i+1]=GetTimeFromDoubleToString(Otch[i].Time);

 //������ �������
    if(Otch[i].Exists)
    {
      StringGrid1->Cells[7][i+1]="+";
    }else{
      if(Otch[i].IsSNomerForP==1)
      {
        StringGrid1->Cells[7][i+1]="S";
      }else{
        StringGrid1->Cells[7][i+1]="-";
      }  
    }
  }

  if(VS)
  {
//     GroupBox1->Visible=true;
     GroupBox1->Visible=false;
     N_of_VnOtch=N_of_Otch;
     VnOtch=(OTCHET*)malloc(N_of_VnOtch*sizeof(OTCHET));
     memcpy(VnOtch,Otch,N_of_VnOtch*sizeof(OTCHET));
     Edit2->Text="0.0";
     Edit3->Text="400.0";
  }else{
     GroupBox1->Visible=false;
  }

/*
  if(VS)for(i=1;i<N_of_VnOtch;i++)
  {
     D=Otch[i].Dalnost-Otch[i-1].Dalnost;
     sprintf(Strka,"%.2lf",D);
     StringGrid1->Cells[9][i+1]=Strka;
  }
*/
//������� ������� �� �������
  if(VS)
  {
    if(Otch[1].Time>Otch[0].Time)
    {
      for(i=1;i<N_of_VnOtch;i++)
      {
         DT=Otch[i].Time-Otch[i-1].Time;
         DT*=3600;
         sprintf(Strka,"%.2lf",DT);
         StringGrid1->Cells[8][i+1]=Strka;
      }
    }else{
      for(i=0;i<N_of_VnOtch-1;i++)
      {
         DT=Otch[i].Time-Otch[i+1].Time;
         DT*=3600;
         sprintf(Strka,"%.2lf",DT);
         StringGrid1->Cells[8][i+1]=Strka;
      }
    }
  }else{
//���� ���������� ����� �������� � ������ ����
      for(i=0;i<N_of_Otch;i++)
      {
        Ret=SchitatChislaOtschetovUnit6(Otch,N_of_Otch,i,
         dfMaxDalnostOfExist,&N_of_VO,&N_of_OO,&PO,false);
        if(Ret==1)
        {
          StringGrid1->Cells[8][i+1]=String(N_of_OO);
          StringGrid1->Cells[9][i+1]=String(N_of_VO);
          sprintf(Strka,"%.3lf",PO);
          StringGrid1->Cells[10][i+1]=Strka;
        }
      }
  }


//  StringGrid1->Cells[9][1]="-";

  Time1=Otch[0].Time;
  Time2=Otch[N_of_Otch-1].Time;
  return 1;
}



void __fastcall TFDataSOtch::FormHide(TObject *Sender)
{
   StringGrid1->RowCount=2;
   if(VnOtch)
   {
     VnOtch=(OTCHET*)realloc(VnOtch,0);
     N_of_VnOtch=0;
   }
  if(VisualBort)
  {
     StringGrid1->ColCount=9;
     StringGrid1->Cells[0][0]="�";
     StringGrid1->Cells[1][0]="Azimuth(�)";
     StringGrid1->Cells[2][0]="Range(km)";
     StringGrid1->Cells[3][0]="� Air.";
     StringGrid1->Cells[4][0]="Range-halfwindow";
     StringGrid1->Cells[5][0]="Range+halfwindow";
     StringGrid1->Cells[6][0]="Time";
     StringGrid1->Cells[7][0]="Avail.";
     StringGrid1->Cells[8][0]="Delta time (sec)";
  }else{
     StringGrid1->ColCount=11;
     StringGrid1->Cells[0][0]="�";
     StringGrid1->Cells[1][0]="Azimuth(�)";
     StringGrid1->Cells[2][0]="Range(km)";
     StringGrid1->Cells[3][0]="� Air.";
     StringGrid1->Cells[4][0]="Range-halfwindow";
     StringGrid1->Cells[5][0]="Range+halfwindow";
     StringGrid1->Cells[6][0]="Time";
     StringGrid1->Cells[7][0]="Avail.";
     StringGrid1->Cells[8][0]="Total";
     StringGrid1->Cells[9][0]="Det.";
     StringGrid1->Cells[10][0]="Pr.of.Det.";

  }



}
//---------------------------------------------------------------------------

void __fastcall TFDataSOtch::Button1Click(TObject *Sender)
{
  double D,D1,D2;
  int i;
  //����
  D=atof(Edit1->Text.c_str());
  for(i=1;i<StringGrid1->RowCount;i++)
  {
    D1=atof(StringGrid1->Cells[2][i].c_str());
    if(D<D1)
    {
       StringGrid1->Row=i;
       return;
    }
  }
  StringGrid1->Row=StringGrid1->RowCount-1;

}
//---------------------------------------------------------------------------






void __fastcall TFDataSOtch::Timer1Timer(TObject *Sender)
{
   int i,N_of_Obn=0;

  TGridRect myRect;
  myRect=StringGrid1->Selection;
  LVydelStrok->Caption=IntToStr(myRect.Bottom-myRect.Top+1);
  for(i=myRect.Top;i<=myRect.Bottom;i++)
  {
    if(StringGrid1->Cells[7][i].Trim()==String("+"))
    {
      N_of_Obn++;
    }
  }
  LObnStrok->Caption=IntToStr(N_of_Obn);


  for( i=1;i<StringGrid1->ColCount;i++)
  {
     StringGrid1->ColWidths[i]=
     StringGrid1->Canvas->TextWidth(StringGrid1->Cells[i][0])+2;
  }
  StringGrid1->ColWidths[6]=
   StringGrid1->Canvas->TextWidth(StringGrid1->Cells[6][1])+2;




}
//---------------------------------------------------------------------------


void __fastcall TFDataSOtch::FormShow(TObject *Sender)
{
     Timer1->Enabled=true;

}
//---------------------------------------------------------------------------




void __fastcall TFDataSOtch::N1Click(TObject *Sender)
{
   AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);
}
//---------------------------------------------------------------------------



void __fastcall TFDataSOtch::N2Click(TObject *Sender)
{
  bool RetB;
  int i;
  FILE *fp;
  AnsiString Strka;
  RetB=SaveDialog1->Execute();
  if(RetB)
  {
    fp=fopen(SaveDialog1->FileName.c_str(),"w");
    if(!fp)return;
 //���������
    if(VisualBort==false)
    {
      fprintf(fp,"  �  | A(�) | R(km)|Numb.| R-pac| R+pac|\
    Time    |TK|Upd.o.|All   | Pro. |");

      for(i=1;i<StringGrid1->RowCount;i++)
      {
        fprintf(fp,"\n");
        Strka=StringGrid1->Cells[0][i].Trim();
        fprintf(fp,"%-5s|",Strka.c_str());
        Strka=StringGrid1->Cells[1][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[2][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[3][i].Trim();
        fprintf(fp,"%-5s|",Strka.c_str());
        Strka=StringGrid1->Cells[4][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[5][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[6][i].Trim();
        fprintf(fp,"%-12s|",Strka.c_str());
        Strka=StringGrid1->Cells[7][i].Trim();
        fprintf(fp,"%-2s|",Strka.c_str());
        Strka=StringGrid1->Cells[8][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[9][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[10][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
      }
    }else{
      fprintf(fp,"  �  | A(�) | R(km)|Numb.| R-pac| R+pac|\
    Time    |TK|TimeCh.|");

      for(i=1;i<StringGrid1->RowCount;i++)
      {
        fprintf(fp,"\n");
        Strka=StringGrid1->Cells[0][i].Trim();
        fprintf(fp,"%-5s|",Strka.c_str());
        Strka=StringGrid1->Cells[1][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[2][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[3][i].Trim();
        fprintf(fp,"%-5s|",Strka.c_str());
        Strka=StringGrid1->Cells[4][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[5][i].Trim();
        fprintf(fp,"%-6s|",Strka.c_str());
        Strka=StringGrid1->Cells[6][i].Trim();
        fprintf(fp,"%-12s|",Strka.c_str());
        Strka=StringGrid1->Cells[7][i].Trim();
        fprintf(fp,"%-2s|",Strka.c_str());
        Strka=StringGrid1->Cells[8][i].Trim();
        fprintf(fp,"%-7s|",Strka.c_str());
      }
    }
    fclose(fp);
  }

}
//---------------------------------------------------------------------------


void __fastcall TFDataSOtch::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
     if(Key==VK_F1)
     {
         N1Click(NULL);
     }        
}
//---------------------------------------------------------------------------

