//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>


#include "Unit7.h"
#include "work_ini_form.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFSpisokBortov *FSpisokBortov;
//---------------------------------------------------------------------------
__fastcall TFSpisokBortov::TFSpisokBortov(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
int __fastcall TFSpisokBortov::VybratBort(void)
{
  RetIndex=-1;
  ShowModal();
  return RetIndex;
}

void __fastcall TFSpisokBortov::Button1Click(TObject *Sender)
{
   RetIndex=ListBox1->ItemIndex;
   if(RetIndex<0)return;
   Close();
}
//---------------------------------------------------------------------------

void __fastcall TFSpisokBortov::N1Click(TObject *Sender)
{
  AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);
}
//---------------------------------------------------------------------------


void __fastcall TFSpisokBortov::FormCreate(TObject *Sender)
{
  iffWorkWithIniFile(this, String("zona_obz_vis.ini"));
}
//---------------------------------------------------------------------------


void __fastcall TFSpisokBortov::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
     if(Key==VK_F1)
     {
         N1Click(NULL);
     }
}
//---------------------------------------------------------------------------

