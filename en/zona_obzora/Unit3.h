//---------------------------------------------------------------------------
#ifndef Unit3H
#define Unit3H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Chart.hpp>
#include <ExtCtrls.hpp>
#include <Series.hpp>
#include <TeEngine.hpp>
#include <TeeProcs.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
//---------------------------------------------------------------------------
class TForm3 : public TForm
{
__published:	// IDE-managed Components
        TChart *Chart1;
        TLineSeries *Series1;
        TLineSeries *Series3;
        TLineSeries *Series4;
        TLineSeries *Series2;
        TPointSeries *Series5;
        TLineSeries *Series6;
        TMainMenu *MainMenu1;
        TMenuItem *N1;
        TMenuItem *N2;
	TMenuItem *MDan2;
        TMenuItem *N5;
        TMenuItem *N6;
        TMenuItem *N7;
	TLineSeries *Series7;
	TLineSeries *Series8;
        TPanel *Panel2;
        TCheckBox *CheckBox1;
        TCheckBox *CheckBox2;
        TComboBox *ComboBox1;
        TLabel *Label9;
        TMenuItem *N3;
        void __fastcall N5Click(TObject *Sender);
        void __fastcall N6Click(TObject *Sender);
        void __fastcall N7Click(TObject *Sender);
	void __fastcall ComboBox1Change(TObject *Sender);
	void __fastcall MDan1Click(TObject *Sender);
        void __fastcall N8Click(TObject *Sender);
        void __fastcall CheckBox1Click(TObject *Sender);
        void __fastcall Chart1Resize(TObject *Sender);
        void __fastcall N1Click(TObject *Sender);
        void __fastcall N3Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
     AnsiString CHMHelpFile;
     AnsiString CHMHtml;


    int Kanal;         //1 - первичный, 2 - вторичный

        __fastcall TForm3(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm3 *Form3;
//---------------------------------------------------------------------------
#endif
