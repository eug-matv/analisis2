//---------------------------------------------------------------------------
#ifndef OptionForZoneH
#define OptionForZoneH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TFOption : public TForm
{
__published:	// IDE-managed Components
        TCheckBox *CheckBox1;
        TCheckBox *CheckBox2;
        TCheckBox *CheckBox3;
        TButton *Button1;
        TButton *Button2;
        TCheckBox *CheckBox4;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall Button2Click(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
      bool IsWasVvod;   //��� �� ����

      AnsiString CHMHelpFile;
      AnsiString CHMHtml;



        __fastcall TFOption(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFOption *FOption;
//---------------------------------------------------------------------------
#endif
 