//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

//for Microsoft Help
#include <htmlhelp.h>

#include <stdio.h>
#include "ToolsForZonaObz.h"
#include "Unit3.h"
#include "Unit2.h"
#include "OptionForZone.h"
#include "work_ini_form.h"



//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm3 *Form3;

//---------------------------------------------------------------------------
__fastcall TForm3::TForm3(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------




void __fastcall TForm3::N5Click(TObject *Sender)
{
   int Ret;

 //����� ������� ��� ���� �������
   MakeDataO_S();
}
//---------------------------------------------------------------------------

void __fastcall TForm3::N6Click(TObject *Sender)
{
/*��������� MakeDataO_S_N ���������� � ������ Unit2. �������� ����*/

   MakeDataO_S_N();
}
//---------------------------------------------------------------------------



void __fastcall TForm3::N7Click(TObject *Sender)
{
  AnsiString HhpStr;
  if(!FileExists(CHMHelpFile))
  {
    AnsiString Err=String("Help file: ")+CHMHelpFile+String("  not found");
    MessageBox(Handle,Err.c_str(),"Error!",MB_OK);
    return;
  }


  HhpStr=CHMHelpFile+String("::/")+CHMHtml;
  HtmlHelp(GetDesktopWindow(),
    HhpStr.c_str(),
    HH_DISPLAY_TOPIC,NULL);
}
//---------------------------------------------------------------------------


void __fastcall TForm3::ComboBox1Change(TObject *Sender)
{
  AnsiString NijnyayaStroka="";
  char Strka[20];
  ComboBox1->Text=ComboBox1->Text.Trim();
  if(ComboBox1->Text!="0.7"&&ComboBox1->Text!="0.8"&&ComboBox1->Text!="0.9")
  {
    ComboBox1->Text="0.9";
  }
  extern double ZonaForS;
  if(ComboBox1->Text.Trim()==String("0.7"))
  {
    ZonaForS=0.7;
    Series3->LinePen->Width=1;
    Series7->LinePen->Width=1;
    Series8->LinePen->Width=2;

  }

  if(ComboBox1->Text.Trim()==String("0.8"))
  {
    ZonaForS=0.8;
    Series3->LinePen->Width=1;
    Series7->LinePen->Width=2;
    Series8->LinePen->Width=1;

  }

  if(ComboBox1->Text.Trim()==String("0.9"))
  {
    ZonaForS=0.9;
    Series3->LinePen->Width=2;
    Series7->LinePen->Width=1;
    Series8->LinePen->Width=1;

  }

//����������� �����������  
  PereschitatVeroyatnost(CheckBox1->Checked,CheckBox2->Checked);
}
//---------------------------------------------------------------------------




void __fastcall TForm3::MDan1Click(TObject *Sender)
{
  int Ret;

 //����� ������� ��� ���� �������
   MakeDataO_S();	
}
//---------------------------------------------------------------------------


void __fastcall TForm3::N8Click(TObject *Sender)
{
  TPrintDialog *PrintDialog1;
  bool  Ret;

  TPrinter *Prntr = Printer();
  if(!Prntr)
  {
    MessageBox(NULL, "Printer not found!", "Error!", MB_OK);
    return;
  }
  if(Prntr->Printers->Count==0)
  {
    MessageBox(NULL, "Printer not found!", "Error!", MB_OK);
    return;

  }
  PrintDialog1=new TPrintDialog(this);
  PrintDialog1->Collate=false;
  PrintDialog1->Copies=1;
  PrintDialog1->Ctl3D=false;
  PrintDialog1->FromPage=0;
  PrintDialog1->HelpContext=0;
  PrintDialog1->MaxPage=0;
  PrintDialog1->MinPage=0;
  PrintDialog1->PrintRange=prAllPages;
  PrintDialog1->PrintToFile=false;
  PrintDialog1->ToPage=0;

  Ret=PrintDialog1->Execute();
  if(!Ret)return;

  TRect Rect;
  Graphics::TBitmap *Bitmap=GetFormImage();
//  TPrinterOrientation *OldPrinterOrientation=Printer->Orientation;
  Prntr->Orientation=poLandscape;
  Rect.Left=Prntr->PageHeight/10;
  Rect.Top=Prntr->PageHeight/10;
  Rect.Bottom=Prntr->PageHeight-Prntr->PageHeight/10;
  Rect.Right=Prntr->PageWidth-Prntr->PageHeight/10;


  Prntr->BeginDoc();
  Prntr->Canvas->StretchDraw(Rect,Bitmap);

  Prntr->EndDoc();
  if(Bitmap)
  {
    delete Bitmap;
  }
  delete PrintDialog1;
}
//---------------------------------------------------------------------------

void __fastcall TForm3::CheckBox1Click(TObject *Sender)
{
//
  if(Kanal==1)
  {
//    PereschitatVeroyatnostP(CheckBox1->Checked,CheckBox2->Checked);
  }else{
    PereschitatVeroyatnost(CheckBox1->Checked,CheckBox2->Checked);
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm3::Chart1Resize(TObject *Sender)
{

  if(Kanal==1)
  {
//    PereschitatVeroyatnostP(CheckBox1->Checked,CheckBox2->Checked);
  }else{
//    PereschitatVeroyatnost(CheckBox1->Checked,CheckBox2->Checked);
  }

  PereschitatVeroyatnost(CheckBox1->Checked,CheckBox2->Checked);


}
//---------------------------------------------------------------------------






void __fastcall TForm3::N1Click(TObject *Sender)
{
  bool  RetB;
  TPrintDialog *PrintDialog1;
//�������� ������� ��������


  TPrinter *Prntr = Printer();
  if(!Prntr)
  {
    MessageBox(NULL, "Printer not found", "Error!", MB_OK);
    return;
  }
  if(Prntr->Printers->Count==0)
  {
    MessageBox(NULL, "Printer not found", "Error!", MB_OK);
    return;

  }


  PrintDialog1=new TPrintDialog(this);
  PrintDialog1->Collate=false;
  PrintDialog1->Copies=1;
  PrintDialog1->Ctl3D=false;
  PrintDialog1->FromPage=0;
  PrintDialog1->HelpContext=0;
  PrintDialog1->MaxPage=0;
  PrintDialog1->MinPage=0;
  PrintDialog1->PrintRange=prAllPages;
  PrintDialog1->PrintToFile=false;
  PrintDialog1->ToPage=0;



  RetB=PrintDialog1->Execute();
  if(!RetB)return;


  int Ret;

  Chart1->PrintOrientation(poLandscape);
 // Chart1->Print();
  delete PrintDialog1;
}
//---------------------------------------------------------------------------


void __fastcall TForm3::N3Click(TObject *Sender)
{

  extern bool IsVyvodMaxD,IsVyvodMinD, IsVyvodMaxUgol, IsVyvodFirstSpad;

   FOption=new TFOption(NULL);
   FOption->CheckBox1->Checked=IsVyvodMaxD;
   FOption->CheckBox2->Checked=IsVyvodMinD;
   FOption->CheckBox3->Checked=IsVyvodMaxUgol;
   FOption->CheckBox4->Checked=IsVyvodFirstSpad;
   FOption->CHMHelpFile=CHMHelpFile;
   if(Kanal==1)
   {
        FOption->CHMHtml="wind_opt_for_pc_cov_area.htm";
   }else{
        FOption->CHMHtml="wind_opt_for_sc_cov_area.htm";
   }
   FOption->ShowModal();
   if(FOption->IsWasVvod)
   {
     IsVyvodMaxD=FOption->CheckBox1->Checked;
     IsVyvodMinD=FOption->CheckBox2->Checked;
     IsVyvodMaxUgol=FOption->CheckBox3->Checked;
     IsVyvodFirstSpad=FOption->CheckBox4->Checked;

     PereschitatVeroyatnost(CheckBox1->Checked,CheckBox2->Checked);
   }
   delete FOption;


}
//---------------------------------------------------------------------------



void __fastcall TForm3::FormCreate(TObject *Sender)
{
        iffWorkWithIniFile(this, String("zona_obz_vis.ini"));
}
//---------------------------------------------------------------------------




void __fastcall TForm3::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if(Key==VK_F1)
     {
         N7Click(NULL);
     }          
}
//---------------------------------------------------------------------------

