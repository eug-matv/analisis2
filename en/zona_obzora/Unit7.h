//---------------------------------------------------------------------------
#ifndef Unit7H
#define Unit7H
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Menus.hpp>
//---------------------------------------------------------------------------
class TFSpisokBortov : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TButton *Button1;
        TListBox *ListBox1;
        TMainMenu *MainMenu1;
        TMenuItem *N1;
        void __fastcall Button1Click(TObject *Sender);
        void __fastcall N1Click(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
private:	// User declarations
        int RetIndex;
public:		// User declarations

        AnsiString CHMHelpFile;
        AnsiString CHMHtml;


        __fastcall TFSpisokBortov(TComponent* Owner);
        int __fastcall VybratBort(void);

};
//---------------------------------------------------------------------------
extern PACKAGE TFSpisokBortov *FSpisokBortov;
//---------------------------------------------------------------------------
#endif
